<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 推广计划控制器
 *
 * @author 林凯<linkai@anysdk.com>
 * @date   2016-11-15
 */
class Campaign extends MY_API_Controller {

	public function __construct() {
		parent::__construct();
		$this->check_login();
	}

	public function get_list() {

		$status = $this->input->get('status');  //0所以未删除 1启用中 2暂停
		$from_date = $this->input->get('from_date');
		$to_date = $this->input->get('to_date');
		$campaign_name = $this->input->get('campaign_name');
		$campaign_id = $this->input->get('campaign_id');
		$page = $this->input->get('page');
		$page_num = $this->input->get('page_num');

		$from_date = strtotime($from_date)?$from_date:date("Ymd", NOW_TIME);
		$to_date = strtotime($to_date)?$to_date:date("Ymd", NOW_TIME);
		$campaign_id = $campaign_id?intval($campaign_id):0;
		$page = $page?intval($page):1;
		$page_num = $page_num?intval($page_num):10;

		$this->load->model(array('st_dsp_daily_report_model', 'ad_campaign_model'));

		if (!empty($campaign_name)) {
			$c_where['like'] = array('campaignname', $campaign_name, 'both');
		}
		if (!empty($campaign_id)) {
			$c_where['campaignid'] = $campaign_id;
		}
		$c_where['userid'] = $this->session->userid;

		switch ($status) {
			case 1:
				$c_where['status'] = 2;
				break;
			case 2:
				$c_where['status'] = 3;
				break;
			default:
				$c_where['where_in'] = array('status', array(1,2,3,4,5));
				break;
		}
		$c_order['campaignid'] = 'DESC';
		$campaign_list = $this->ad_campaign_model->find_all($c_where, '*', $c_order, ($page-1)*$page_num, $page_num);
		$campaign_count = $this->ad_campaign_model->find_count($c_where);

		$campaigns = array();
		if (!empty($campaign_list)) {
			foreach ($campaign_list as $key => $value) {
				$campaign_ids[] = $value['campaignid'];
			}

			//获取统计数据
			$reports = array();
			$from_date = date("Y-m-d", strtotime($from_date));
			$to_date = date("Y-m-d", strtotime($to_date));

			$sql = "SELECT campaignid,sum(imp) AS imp_count,sum(click) AS click_count,sum(clickrate) AS clickrate_count,sum(cost) AS cost_count ";
			$sql .= "FROM ".$this->st_dsp_daily_report_model->_table." WHERE campaignid IN (".implode(',',$campaign_ids).") AND";
			$sql .= " logdate >= '".$from_date."' AND logdate <= '".$to_date."' GROUP BY campaignid";
			$report_query = $this->st_dsp_daily_report_model->execute_query($sql);
			foreach ($report_query->result_array() as $value) {
				$reports[$value['campaignid']] = $value;
			}

			foreach ($campaign_list as $key => $value) {
				$tmp = array();
				$report_tmp = $reports[$value['campaignid']];
				$tmp['imp'] = intval($report_tmp['imp_count']);
				$tmp['imp'] = $tmp['imp']?$tmp['imp']:0;
				$tmp['click'] = intval($report_tmp['click_count']);
				$tmp['click'] = $tmp['click']?$tmp['click']:0;
				$tmp['clickrate'] = floatval($report_tmp['clickrate_count']);
				$tmp['click'] = $tmp['clickrate']?$tmp['clickrate']:0.00;
				$tmp['cost'] = floatval($report_tmp['cost_count']);
				$tmp['cost'] = $tmp['cost']?$tmp['cost']:0.00;
				$tmp['clickcost_avg'] = $report_tmp['click_count']?($report_tmp['cost_count']/$report_tmp['click_count']):0;
				$tmp['clickcost_avg'] = floatval($tmp['clickcost_avg']);

				$tmp['campaign_id'] = intval($value['campaignid']);
				$tmp['campaign_status'] = intval($value['status']);
				$tmp['campaign_daybudget'] = floatval(formatmoney($value['daybudget'],'get',2,'.'));
				$tmp['campaign_name'] = $value['campaignname'];
				$campaigns[] = $tmp;
			}
		}

		$info = array();
		$info['items'] = $campaigns;
		$info['count'] = $campaign_count;

		$this->success_response($info);
	}

	public function get_info() {

		$campaign_id = $this->input->get('campaign_id');
		$campaign_id = $campaign_id?intval($campaign_id):0;

		if (empty($campaign_id)) {
			$this->error_response(MY_Controller::$error_no['param_error'], MY_Controller::$error_msg['param_error']);
		}

		$this->load->model('ad_campaign_model');
		$c_where['campaignid'] = $campaign_id;
		$c_where['userid'] = $this->session->userid;
		$campaign_info = $this->ad_campaign_model->find($c_where);

		if (empty($campaign_info)) {
			$this->error_response(MY_Controller::$error_no['data_empty'], MY_Controller::$error_msg['data_empty']);
		}

		$info = array();
		$info['campaign_id'] = intval($campaign_info['campaignid']);
		$info['campaign_name'] = $campaign_info['campaignname'];
		$info['campaign_status'] = intval($campaign_info['status']);
		$info['os_type'] = intval($campaign_info['ostypeid']);

		//定向配置
		$info['os_version_set'] = $campaign_info['osversionset'];
		$info['device_type_set'] = $campaign_info['devicetypeset'];
		$info['net_type_set'] = $campaign_info['nettypeset'];
		$info['operators_set'] = $campaign_info['operatorsset'];
		$info['app_type_set'] = $campaign_info['apptypeset'];
		$info['area_set'] = $campaign_info['areaset'];
		$info['dmp_set'] = $campaign_info['dmpset'];

		$info['day_budget'] = floatval(formatmoney($campaign_info['daybudget'],'get',2,'.'));
		$info['start_time'] = intval($campaign_info['starttime']);
		$info['end_time'] = intval($campaign_info['endtime']);
		$info['period_set'] = $campaign_info['periodset'];
		$this->success_response($info);
	}

	public function update_status() {

		$campaign_id = $this->input->get('campaign_id');
		$is_enable = $this->input->get('is_enable');

		$campaign_id = $campaign_id?intval($campaign_id):0;
		$is_enable = $is_enable?intval($is_enable):0;

		if (empty($campaign_id)) {
			$this->error_response(MY_Controller::$error_no['param_error'], MY_Controller::$error_msg['param_error']);
		}

		$this->load->model('ad_campaign_model');
		$c_where['campaignid'] = $campaign_id;
		$c_where['userid'] = $this->session->userid;
		$campaign_info = $this->ad_campaign_model->find($c_where);

		if (empty($campaign_info)) {
			$this->error_response(MY_Controller::$error_no['data_empty'], MY_Controller::$error_msg['data_empty']);
		}

		switch ($is_enable) {
			case 1:
				$update['status'] = 2;
				break;
			case 0:
				$update['status'] = 3;
				break;
		}
		$result = $this->ad_campaign_model->update($campaign_id, $update);

		if ($result !== FALSE) {
			$this->success_response();
		}
		$this->error_response(MY_Controller::$error_no['system_error'], MY_Controller::$error_msg['system_error']);

	}


	public function create() {

		$this->load->library('form_validation');
		$this->form_validation->set_rules('campaign_name', '推广计划名称', 'required');
		$this->form_validation->set_rules('os_version_set', '系统类型定向', 'required');
		$this->form_validation->set_rules('device_type_set', '设备类型定向', 'required');
		$this->form_validation->set_rules('net_type_set', '网络类型定向', 'required');
		$this->form_validation->set_rules('operators_set', '营运商定向', 'required');
		$this->form_validation->set_rules('app_type_set', '媒体类型定向', 'required');
		$this->form_validation->set_rules('area_set', '地域定向', 'required');
		$this->form_validation->set_rules('dmp_set', 'Dmp定向', 'required');
		$this->form_validation->set_rules('day_budget', '日预算', 'required');
		$this->form_validation->set_rules('start_time', '投放周期起始时间', 'required');
		$this->form_validation->set_rules('end_time', '投放周期结束时间', 'required');
		$this->form_validation->set_rules('period_set', '投放时段', 'required');

		if ($this->form_validation->run() == FALSE) {
			$this->error_response(MY_Controller::$error_no['param_error'], $this->form_validation->error_string());
		}

		$data['campaignname'] = $this->input->post('campaign_name');
		$data['osversionset'] = $this->input->post('os_version_set');
		$data['devicetypeset'] = $this->input->post('device_type_set');
		$data['nettypeset'] = $this->input->post('net_type_set');
		$data['operatorsset'] = $this->input->post('operators_set');
		$data['apptypeset'] = $this->input->post('app_type_set');
		$data['areaset'] = $this->input->post('area_set');
		$data['dmpset'] = $this->input->post('dmp_set');
		$data['periodset'] = $this->input->post('period_set');
		$data['daybudget'] = formatmoney($this->input->post('day_budget'), 'set');
		$data['starttime'] = strtotime($this->input->post('start_time'));
		$data['endtime'] = strtotime($this->input->post('end_time'));

		$data['targettype'] = 1;//App
		$data['status'] = 1;//待投放
		$data['selfappset'] = 0;//正常广告
		$data['userid'] = $this->session->userid;
		$data['createtime'] = NOW_TIME;
		$data['lastupdatetime'] = NOW_TIME;
		$this->load->model('ad_campaign_model');
		$result = $this->ad_campaign_model->insert($data);

		if ($result !== FALSE) {
			$info['campaign_id'] = $result;
			$this->success_response($info);
		}
		$this->error_response(MY_Controller::$error_no['system_error'], MY_Controller::$error_msg['system_error']);

	}

	public function update() {

		$this->load->library('form_validation');
		$this->form_validation->set_rules('campaign_id', '推广计划ID', 'required');
		$this->form_validation->set_rules('campaign_name', '推广计划名称', 'required');
		$this->form_validation->set_rules('os_version_set', '系统类型定向', 'required');
		$this->form_validation->set_rules('device_type_set', '设备类型定向', 'required');
		$this->form_validation->set_rules('net_type_set', '网络类型定向', 'required');
		$this->form_validation->set_rules('operators_set', '营运商定向', 'required');
		$this->form_validation->set_rules('app_type_set', '媒体类型定向', 'required');
		$this->form_validation->set_rules('area_set', '地域定向', 'required');
		$this->form_validation->set_rules('dmp_set', 'Dmp定向', 'required');
		$this->form_validation->set_rules('day_budget', '日预算', 'required');
		$this->form_validation->set_rules('start_time', '投放周期起始时间', 'required');
		$this->form_validation->set_rules('end_time', '投放周期结束时间', 'required');
		$this->form_validation->set_rules('period_set', '投放时段', 'required');

		if ($this->form_validation->run() == FALSE) {
			$this->error_response(MY_Controller::$error_no['param_error'], $this->form_validation->error_string());
		}

		$this->load->model('ad_campaign_model');
		$c_where['campaignid'] = $this->input->post('campaign_id');
		$c_where['userid'] = $this->session->userid;
		$campaign_info = $this->ad_campaign_model->find($c_where);

		if (empty($campaign_info)) {
			$this->error_response(MY_Controller::$error_no['data_empty'], MY_Controller::$error_msg['data_empty']);
		}

		$data['campaignname'] = $this->input->post('campaign_name');
		$data['osversionset'] = $this->input->post('os_version_set');
		$data['devicetypeset'] = $this->input->post('device_type_set');
		$data['nettypeset'] = $this->input->post('net_type_set');
		$data['operatorsset'] = $this->input->post('operators_set');
		$data['apptypeset'] = $this->input->post('app_type_set');
		$data['areaset'] = $this->input->post('area_set');
		$data['dmpset'] = $this->input->post('dmp_set');
		$data['periodset'] = $this->input->post('period_set');
		$data['daybudget'] = formatmoney($this->input->post('day_budget'), 'set');
		$data['starttime'] = strtotime($this->input->post('start_time'));
		$data['endtime'] = strtotime($this->input->post('end_time'));

		$data['lastupdatetime'] = NOW_TIME;
		$result = $this->ad_campaign_model->update($this->input->post('campaign_id'), $data);

		if ($result !== FALSE) {
			$this->success_response();
		}
		$this->error_response(MY_Controller::$error_no['system_error'], MY_Controller::$error_msg['system_error']);

	}
}

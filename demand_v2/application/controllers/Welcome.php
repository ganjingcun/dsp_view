<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_API_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$info['account']['balance'] = 100.00;
		$info['ads']['review'] = 100.00;
		$info['ads']['review1'] = 100.00;
		//$this->load->view('welcome_message', $info);
		$this->success_response($info);
	}
}

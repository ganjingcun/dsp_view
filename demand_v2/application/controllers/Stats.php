<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 统计控制器
 *
 * @author 林凯<linkai@anysdk.com>
 * @date   2016-11-14
 */
class Stats extends MY_API_Controller {

	public function __construct() {
		parent::__construct();
		$this->check_login();
	}

	public function get_money_and_ads() {
		$this->load->model(array('user_member_model', 'ad_group_model'));
		$user_info = $this->user_member_model->get($this->session->userid);

		if (empty($user_info)) {
			$this->error_response(MY_Controller::$error_no['user_no_exists'], MY_Controller::$error_msg['user_no_exists']);
		}

		$info = array();
		$info['account']['balance'] = intval($user_info['totalmoney']);

		$where = array();
		$where['userid'] = $this->session->userid;
		$where['status'] = 0;
		$verify_ing_count = $this->ad_group_model->find_count($where);

		$where['status'] = 2;
		$verify_fail_count = $this->ad_group_model->find_count($where);

		$where['status'] = 1;
		$verify_success_count = $this->ad_group_model->find_count($where);

		$where['starttime >='] = mktime(0, 0, 0, date("m")  , date("d"), date("Y"));
		$where['endtime <='] = mktime(23, 59, 59, date("m")  , date("d"), date("Y"));
		$where['status'] = 1;
		$today_update_count = $this->ad_group_model->find_count($where);

		$info['ads']['verify_ing_count'] = $verify_ing_count;
		$info['ads']['verify_fail_count'] = $verify_fail_count;
		$info['ads']['verify_success_count'] = $verify_success_count;
		$info['ads']['today_update_count'] = $today_update_count;

		$this->success_response($info);
	}

}

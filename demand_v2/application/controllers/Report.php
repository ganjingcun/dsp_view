<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 报表控制器
 *
 * @author 林凯<linkai@anysdk.com>
 * @date   2016-11-15
 */
class Report extends MY_API_Controller {

	public function __construct() {
		parent::__construct();
		$this->check_login();
	}

	public function ads() {
		$from_date = $this->input->get('from_date');
		$to_date = $this->input->get('to_date');

		$from_date = strtotime($from_date)?$from_date:date("Ymd", NOW_TIME);
		$to_date = strtotime($to_date)?$to_date:date("Ymd", NOW_TIME);

		if ($from_date > $to_date) {
			$this->error_response(MY_Controller::$error_no['param_error'], MY_Controller::$error_msg['param_error']);
		}

		$from_date = date("Y-m-d", strtotime($from_date));
		$to_date = date("Y-m-d", strtotime($to_date));

		$this->load->model(array('st_dsp_daily_report_model', 'ad_group_model'));
		$ad_where['userid'] = $this->session->userid;
		$ad_where['status'] = 1;
		$ads_list = $this->ad_group_model->find_all($ad_where, 'adgroupid');

		$report_list = array();
		if (!empty($ads_list)) {
			foreach ($ads_list as $value) {
				$adgroupid_array[] = $value['adgroupid'];
			}
			$sql = "SELECT logdate,hour,sum(imp) AS imp_count,sum(click) AS click_count,sum(clickrate) AS clickrate_count,sum(cost) AS cost_count ";
			$sql .= "FROM ".$this->st_dsp_daily_report_model->_table." WHERE groupid IN (".implode(',',$adgroupid_array).") AND";
			$sql .= " logdate >= '".$from_date."' AND logdate <= '".$to_date."' GROUP BY logdate,hour ORDER BY logdate ASC,hour ASC";
			$report_query = $this->st_dsp_daily_report_model->execute_query($sql);


			foreach ($report_query->result_array() as $value) {
				$tmp['logdate'] = $value['logdate'];
				$tmp['hour'] = intval($value['hour']);
				$tmp['imp'] = intval($value['imp_count']);
				$tmp['click'] = intval($value['click_count']);
				$tmp['clickrate'] = floatval($value['clickrate_count']);
				$tmp['cost'] = floatval($value['cost_count']);
				$tmp['clickcost_avg'] = $value['click_count']?($value['cost_count']/$value['click_count']):0;
				$tmp['clickcost_avg'] = floatval($tmp['clickcost_avg']);
				$report_list[] = $tmp;
			}

		}

		$info = array();
		$info['report'] = $report_list;

		$this->success_response($info);
	}

	public function get_top5() {

		$this->load->model(array('st_dsp_daily_report_model', 'ad_group_model'));
		$ad_where['userid'] = $this->session->userid;
		$ad_where['status'] = 1;
		$ads_list = $this->ad_group_model->find_all($ad_where, 'adgroupid,adgroupname');

		$imp = array();
		$click = array();
		$clickrate = array();
		if (!empty($ads_list)) {
			foreach ($ads_list as $value) {
				$adgroupid_array[] = $value['adgroupid'];
				$ad_name_array[$value['adgroupid']] = $value['adgroupname'];
			}
			//曝光量top5
			$r_where['where_in'] = array('groupid', $adgroupid_array);
			$r_order = array();
			$r_order['imp'] = 'DESC';
			$ads_list = $this->st_dsp_daily_report_model->find_all($r_where, 'groupid,imp', $r_order, 0, 5);
			foreach ($ads_list as $item) {
				$tmp = array();
				$tmp['ad_id'] = intval($item['groupid']);
				$tmp['imp'] = intval($item['imp']);
				$tmp['ad_name'] = $ad_name_array[$item['groupid']];
				$imp[] = $tmp;
			}

			//点击量top5
			$r_order = array();
			$r_order['click'] = 'DESC';
			$ads_list = $this->st_dsp_daily_report_model->find_all($r_where, 'groupid,click', $r_order, 0, 5);
			foreach ($ads_list as $item) {
				$tmp = array();
				$tmp['ad_id'] = intval($item['groupid']);
				$tmp['click'] = intval($item['click']);
				$tmp['ad_name'] = $ad_name_array[$item['groupid']];
				$click[] = $tmp;
			}

			//点击率top5
			$r_order = array();
			$r_order['clickrate'] = 'DESC';
			$ads_list = $this->st_dsp_daily_report_model->find_all($r_where, 'groupid,clickrate', $r_order, 0, 5);
			foreach ($ads_list as $item) {
				$tmp = array();
				$tmp['ad_id'] = intval($item['groupid']);
				$tmp['clickrate'] = intval($item['clickrate']);
				$tmp['ad_name'] = $ad_name_array[$item['groupid']];
				$clickrate[] = $tmp;
			}
		}

		$info = array();
		$info['imp'] = $imp;
		$info['click'] = $click;
		$info['clickrate'] = $clickrate;

		//$this->load->view('welcome_message', $info);
		$this->success_response($info);
	}

}

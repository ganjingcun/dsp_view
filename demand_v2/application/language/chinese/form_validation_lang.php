<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['form_validation_valid_mobile']		= 'The {field} field must contain a valid mobile number.';


<?php

/**
 * 数据库价格转化函数
 * @param $money
 * @param $type
 * @param $num
 * @param $replace
 */
function formatmoney($money, $type='get', $num = 2, $replace = '')
{
    if(!is_numeric($money))
    {
        return $money;
    }
    if($type == 'get')
    {
        if($replace == '')
        {
            $data = number_format($money / 1000000, $num);
        }
        else
        {
            $data = number_format($money / 1000000, $num, $replace,'');
        }
    }
    elseif($type == 'set')
    {
        $data = $money * 1000000;
    }
    return $data;
}
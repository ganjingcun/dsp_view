<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 广告表
 *
 * @author 林凯<linkai@anysdk.com>
 * @date   2016-11-14
 */
class Ad_group_model extends MY_Model {

	public $_table = 'ad_group';
	protected $return_type = 'array';

	public function __construct()
	{
		parent::__construct();
	}


}
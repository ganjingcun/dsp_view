<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 推广计划表
 *
 * @author 林凯<linkai@anysdk.com>
 * @date   2016-11-14
 */
class Ad_campaign_model extends MY_Model {

	public $_table = 'ad_campaign';
	protected $return_type = 'array';

	public function __construct()
	{
		parent::__construct();
	}


}
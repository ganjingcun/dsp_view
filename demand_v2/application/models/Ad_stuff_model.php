<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 素材表
 *
 * @author 林凯<linkai@anysdk.com>
 * @date   2016-11-14
 */
class Ad_stuff_model extends MY_Model {

	public $_table = 'ad_stuff';
	protected $return_type = 'array';

	public function __construct()
	{
		parent::__construct();
	}


}
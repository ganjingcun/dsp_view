<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * dsp报表数据表
 *
 * @author 林凯<linkai@anysdk.com>
 * @date   2016-11-14
 */
class St_dsp_daily_report_model extends MY_Model {

	public $_table = 'st_dsp_daily_report';
	protected $return_type = 'array';

	public function __construct()
	{
		parent::__construct();
	}


}
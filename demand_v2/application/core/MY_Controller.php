<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 控制器父类
 *
 * @author 林凯<linkai@anysdk.com>
 * @date   2016-11-14
 */
class MY_Controller extends CI_Controller {

	public static $error_no = array(
		'system_error' => 10001,
		'param_error' => 10002,
		'data_empty' => 10003,

		'user_no_login' => 20001,
		'user_no_exists' => 20002,
	);

	public static $error_msg = array(
		'system_error' => '系统错误',
		'param_error' => '参数错误',
		'data_empty' => '数据不存在',

		'user_no_login' => '请先登录',
		'user_no_exists' => '用户不存在',

	);

	public function __construct() {
		define('NOW_TIME', time());
		parent::__construct();

		//临时登录状态
		$user_info = array(
			'userid' =>  '4579',
			'username' =>  'CocosAds',
			'usertype' =>  '4',
			'totalmoney' =>  '0',
		);
		$this->session->set_userdata($user_info);
	}


}

/**
 * 控制器父类，适用于API
 *
 * @author 林凯<linkai@anysdk.com>
 * @date   2016-11-14
 */
class MY_API_Controller extends MY_Controller {

	public function __construct() {
		parent::__construct();
	}

	/**
	 * 检查是否登录
	 *
	 * @author 饭盒<linkai@anysdk.com>
	 * @date   2016-11-15
	 * @return json
	 */
	public function check_login() {
		if (empty($this->session->userid)) {
			$this->error_response(MY_Controller::$error_no['user_no_login'], MY_Controller::$error_msg['user_no_login']);
		}
	}

	/**
	 * API成功返回
	 *
	 * @author 饭盒<linkai@anysdk.com>
	 * @date   2016-11-14
	 * @param  array $info 返回数据
	 * @return json
	 */
	public function success_response($info = array()) {
		$response = array('status' => 0, 'msg' => 'success', 'data' => $info);

		$this->output->set_status_header(200)
			->set_header('Access-Control-Allow-Origin: *')
			->set_header('Access-Control-Allow-Headers: Content-Type')
			->set_header('Cache-Control: no-store, no-cache, must-revalidate')
			->set_header('Pragma: no-cache')
			->set_header('Expires: 0')
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response))
			->_display();

		log_message('debug', "API Success Response:" . uri_string() . " GET " . json_encode($this->input->get(), TRUE) . " POST " . json_encode($this->input->post(), TRUE) . " Return " . json_encode($info, TRUE));
		exit;
	}

	/**
	 * API失败返回
	 *
	 * @author 饭盒<linkai@anysdk.com>
	 * @date   2016-11-14
	 * @param  int $code 错误码
	 * @param  string $error_message 错误信息
	 * @return json
	 */
	public function error_response($code, $error_message = 'fail') {

		$response = array('status' => $code, 'msg' => $error_message, 'data' => array());

		$this->output->set_status_header(200)
			->set_header('Access-Control-Allow-Origin: *')
			->set_header('Access-Control-Allow-Headers: Content-Type')
			->set_header('Cache-Control: no-store, no-cache, must-revalidate')
			->set_header('Pragma: no-cache')
			->set_header('Expires: 0')
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response))
			->_display();

		log_message('debug', "API Error Response:" . uri_string() . " GET " . json_encode($this->input->get(), TRUE) . " POST " . json_encode($this->input->post(), TRUE) . " Msg " . $code . ":" . $error_message);
		exit;
	}
}

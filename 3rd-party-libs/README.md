* **后端框架**
    
    * PHP的后端均采用`CodeIgniter`框架
        * 中文官网：http://codeigniter.org.cn/
        * 源码地址：https://github.com/bcit-ci/CodeIgniter/tree/3.1.1

* **前端框架**
    
    * Supply和Demand后台采用`AdminLTE`（ _AdminLTE是基于Bootstrap 3.x的_ ）
        * 官方网站：https://almsaeedstudio.com/
        * 源码地址：https://github.com/almasaeed2010/AdminLTE/tree/v2.3.7
    
    * Admin和Monitor后台采用`Bootstrap`默认Theme。（ _其中畅思原Admin后台使用的是Bootstrap 2.3.2_ ）
        * 中文官网：http://www.bootcss.com/
   * 图表采用`Chart.js`
        * 官方网站：http://www.chartjs.org/
        * 中文文档：http://www.bootcss.com/p/chart.js/docs/


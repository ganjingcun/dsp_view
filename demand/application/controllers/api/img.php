<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * api图片处理接口
 *
 * @author wd
 *
 */
class Img extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->helper("tools");
        $this->load->helper(array('form', 'url'));
    }

    /**
     * 图片上传.
     *
     * @author wd
     */
    public function upload()
    {
		//图片
		$this->load->model('dsp_manage/AdStuffImgClass');
		$this->load->model('dsp_manage/AdStuffImgSizeClass');

		switch ($_POST['img_type']) {
			case 1:
			default:
				$path = 'stuff';
				break;

		}
		$this->load->library('MyNewUpload',$_FILES['img']);
		$handle = $this->mynewupload;
		if ($handle->uploaded) {
			$handle->allowed = array('image/*');
			$handle->file_new_name_body = md5($_FILES['img']['name'] . uniqid());
			$handle->image_convert = 'png';
			$handle->process(FCPATH.'public/upload/' . $path . '/img/' . date('Ym'));
			if ($handle->processed) {

				$img_path = str_replace(FCPATH.'public/upload', '', $handle->file_dst_pathname);
				$img_url = $this->config->item('pic_url').$img_path;
				$info = array('status' => 0, 'data'=> array('img_url' => $img_url));
				$img_name = $handle->file_dst_name_body;

				//翻转
				$this->load->library('MyNewUpload',$handle->file_dst_pathname);
				$handle = $this->mynewupload;
				//$handle->image_rotate = 90;
				$handle->file_new_name_body = $img_name."L";
				$handle->file_new_name_ext = 'png';
				$handle->process(FCPATH.'public/upload/' . $path . '/img/' . date('Ym'));

				echo json_encode($info);
				exit;
			}
		}
		$info = array('status' => 1, 'data'=> array(), 'error'=>$handle->error);
		echo json_encode($info);
		exit;
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
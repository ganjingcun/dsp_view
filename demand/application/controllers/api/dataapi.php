<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 网站登陆注册入口
 *
 * @author wd
 *
 */
class DataApi extends CI_Controller {

    private $secret = "046da2af2bb263b4c82b83b9fbb80533";
    function __construct()
    {
        parent::__construct();
        $this->load->helper("tools");
        $this->load->helper(array('form', 'url'));
        $this->load->model('/user/UserInfoClass');
        header("Content-type: text/html; charset=utf-8");
    }

    /**
     * 首页.
     *
     * @author wd
     */
    public function changepwd($from, $username, $sign)
    {
        $username = urldecode($username);
        if(empty($username))
        {
            $error['errorcode'] = 1001;
            $error['errormsg'] = '用户名不能为空';
            $error['status'] = 'error';
            echo json_encode($error);
            return ;
        }
        if($from != 'open')
        {
            $error['errorcode'] = 1002;
            $error['errormsg'] = '地址错误！';
            $error['status'] = 'error';
            echo json_encode($error);
            return ;
        }
        $signData['from'] = $from;
        $signData['username'] = $username;
        $getSign = cocoSignData($signData);
        if($sign != $getSign)
        {
            $error['errorcode'] = 1003;
            $error['errormsg'] = '验签错误！';
            $error['status'] = 'error';
            echo json_encode($error);
            return ;
        }
        $data['username'] = $this->input->post('username');
        $data['password'] = $this->input->post('password');
        $data['newpassword'] = $this->input->post('newpassword');
        echo json_encode($this->UserInfoClass->cocoEditPassword($data));
    }

    /**
     * 首页.
     *
     * @author wd
     */
    public function editapp($from, $username, $sign)
    {
        $this->load->model('manage/AppInfoClass');
        $postData = $this->input->post();
        $username = urldecode($username);
        if(empty($username))
        {
            $error['errorcode'] = 1001;
            $error['errormsg'] = '用户名不能为空';
            $error['status'] = 'error';
            echo json_encode($error);
            return ;
        }
        if($from != 'open')
        {
            $error['errorcode'] = 1002;
            $error['errormsg'] = '地址错误！';
            $error['status'] = 'error';
            echo json_encode($error);
            return ;
        }
        $signData['from'] = $from;
        $signData['username'] = $username;
        $getSign = cocoSignData($signData);
        if($sign != $getSign)
        {
            $error['errorcode'] = 1003;
            $error['errormsg'] = '验签错误！';
            $error['status'] = 'error';
            echo json_encode($error);
            return ;
        }
        $userInfo = $this->UserInfoClass->getUserInfo($username);
        if(empty($userInfo))
        {
            $error['errorcode'] = 1004;
            $error['errormsg'] = '用户不存在！';
            $error['status'] = 'error';
            echo json_encode($error);
            return ;
        }
        $data['appname'] = $postData['appname'];
        $data['appid'] = $postData['appid'];
        $data['packagename'] = $postData['packagename'];
        $data['appchildtypeid']= $postData['appchildtypeid'].',';
        $data['appdescription']= $postData['appdescription'];
        $appInfo = $this->AppInfoClass->cocodoEdit($data, $userInfo);
        if($appInfo['status'] == 0)
        {
            $error['errorcode'] = 1005;
            $error['errormsg'] = '更新错误！';
            $error['status'] = 'error';
            echo json_encode($error);
            return ;
        }
        $error['status'] = 'success';
        echo json_encode($error);
        return ;
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
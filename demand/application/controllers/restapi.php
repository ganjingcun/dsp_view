<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: chukong
 * Date: 3/25/16
 * Time: 10:47 AM
 */
class RestApi extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('RestapiClass');
        $this->load->model('manage/AppInfoClass');
    }

    /**
     * Cocos Ads for Creator插件 - PHP远程API
     * 获取应用列表
     */
    function getapplist()
    {
        if (isset($_POST['userid']))
        {
            $userid = $_POST['userid'];
        }

        if(!isset($userid) || !is_numeric($userid))
        {
            ajaxReturn('参数错误', 0, null);
        }

        if (isset($_POST['ostypeid']))
        {
            $data = $this->RestapiClass->getAppInfoList($userid, $_POST['ostypeid']);
        }
        else
        {
            $data = $this->RestapiClass->getAppInfoList($userid);
        }

        if(is_array($data))
        {
            ajaxReturn('', 1, $data);
        }

        ajaxReturn('未知错误', 0, null);
    }

    /**
     * Cocos Ads for Creator插件 - PHP远程API
     * 添加App
     */
    function createapp()
    {
        $postData = $this->input->post();

        if(!isset($postData['userid']) || !is_numeric($postData['userid']))
        {
            ajaxReturn('参数错误', 0, null);
        }

        $postData['appurl'] = '';
        $data = $this->AppInfoClass->doAdd($postData, array('userid'=>$postData['userid']));
        if($data['status'] == 0)
        {
            ajaxReturn($data['info'], 0, $data['data']);
        }

        $result = array();
        $result['appid'] = $data['appid'];
        $result['appname'] = trim($postData['appname']);
        $result['publisherID'] = $data['publisherID'];
        $result['channelname'] = '官方';
        ajaxReturn('', 1, $result);
    }
}
<?php
/**
 * 应用管理类
 * @filename	: appinfo.php
 * @author		: niejianhui
 * @datetime	: 2013-10-21
 * @Description  : 前台广告主收入理页面。
 */

class DetailReport extends MY_Controller{
    private  $userid = null;
    public function __construct()
    {
        parent::__construct();
        header("Cache-control:no-cache,no-store,must-revalidate");
        header("Pragma:no-cache");
        header("Expires:0");
        $this->load->model('manage/AuthClass');
        $this->load->model('manage/AppInfoClass');
        $this->load->model('report/AppReportClass');
        $this->load->model('manage/AppChannelClass');
        $this->load->model('manage/AppPositionClass');
        $this->load->helper('tools');
        $this->userid =  AuthClass::getUserId();
        $param = $this->input->get();
       	if(!isset($param['type']) || $param['type'] != 'exportreport'){
        	$this->loadHeader('统计分析', 3, 31);
       	}
    }

    /**

     * 媒体概况报表
     * Enter description here ...
     */
    function detailGather(){
        $data = $this->getCondition();
        $data['graphreport'] = $this->getAppGraphReport($data);
        $this->load->view('manage/appreport/detailgather', $data);
    }
    
    public function generateAppDetail($inputDate=''){
        $timeDate = explode('~', $inputDate);
        if(count($timeDate) == 2)
        {
            $sdate = $timeDate[0];
            $edate = $timeDate[1];
        }
        if(empty($sdate))
        {
            $sdate = date('Y-m-d',time()-6*86400);
        }
        if(empty($edate))
        {
            $edate = date('Y-m-d');
        }
        $originalAppList = $this->AppInfoClass->getAppInfoList('', $this->userInfo);
        $reportdata = array();
        if($originalAppList){
	        $appIDList = '';
	        $applist=array();
	        foreach($originalAppList as $v){
	        	$appIDList = $appIDList.','.$v['appid'];
	        	$applist[$v['appid']] = $v;
	        }
	        $appIDList=trim($appIDList, ',');
	        $reportdata = $this->AppReportClass->postAppReport($appIDList, $sdate,$edate, $applist);
        }
	    return $reportdata;
    }
    
    function appDetailGather(){
    	$inputDate = $this->input->get("date");
	    $report = $this->generateAppDetail($inputDate);
	    
		$filename=urlencode('详情统计').'_'.date('Y-m-d His');
	    header("Content-Disposition:attachment;filename={$filename}.csv");
	    $table = iconv("UTF-8","GBK","应用名称,广告形式,日期,请求量,展示量,点击量,填充率,点击率,收入").PHP_EOL;
	   
    	$reportdata = array();
    	if(!empty($report)){
    		foreach($report as $app){
    			foreach($app as $adform=>$data){
    				foreach($data['list'] as $date=>$v){
    	 				if($adform != 4) $ask =  $v['ask']; else $ask = '--';
    	 				if($adform == 1 || $adform == 2 ||$adform == 20) $imp = $v['imp']; else $imp = '--';
    	 				if($adform != 4) $click = $v['click']; else $click = '--';
    	 				if($adform == 1 || $adform == 2 ||$adform == 20) $imprate = $v['imprate']."%"; else $imprate = '--';
    	 				if($adform == 1 || $adform == 2 ||$adform == 20) $clickrate = $v['clickrate']."%"; else $clickrate = '--';
    					$reportdata[] = array('appname'=>$data['appname'], 'adform'=>$data['adform'], 'date'=> $date, 'ask'=>$ask,'imp'=>$imp, 'click'=>$click, 'imprate'=>$imprate, 'clickrate'=>$clickrate, 'income'=>$v['income']);
    				}
    			}
    		}
    	}
    	
		if(!empty($reportdata)){
			foreach($reportdata as $k=>$v){
				$table.=iconv("UTF-8","GBK",$v['appname'].",".$v['adform'].",".$v['date'].",".$v['ask'].",".$v['imp'].",".$v['click'].",".$v['imprate'].",".$v['clickrate'].",".'￥'.$v['income']).PHP_EOL;
			}
    	}
    	echo $table;
    }
  


    /**
     * 渠道维度报表
     * Enter description here ...
     */
    function ajaxChannelData(){
        $data = $this->getCondition();
        $data['graphreport'] = $this->getChannelGraphReport($data);
		if($data['incentiveswitch']!=1){
	        $ajaxData = array();
	        $askData = array();
	        $impData = array();
	        $clickData = array();
	        $incomeData = array();
	        $clickrateData = array();
	        $imprateData = array();
	        $coinsData = array();
	        $complateData = array();
	        foreach ($data['graphreport']['list'] as $k => $v)
	        {
	            $tmpData = array();
	            $tmpData[] = $k;
	            if($data['adform'] != 4)
	            {
	                $tmpData[] = $v['ask'];
	                $askData[$k] = $v['ask'];
	            }
	            if($data['adform'] == 1 || $data['adform'] == 2)
	            {
	                $tmpData[] = $v['imp'];
	                $impData[$k] = $v['imp'];
	            }
	            if($data['adform'] != 4)
	            {
	                $tmpData[] = $v['click'];
	                $clickData[$k] = $v['click'];
	            }
	            if($data['adform'] == 1 || $data['adform'] == 2)
	            {
	                $tmpData[] = $v['imprate'];
	                $imprateData[$k] = $v['imprate'];
	            }
	            if($data['adform'] == 1 || $data['adform'] == 2)
	            {
	                $tmpData[] = $v['clickrate'];
	                $clickrateData[$k] = $v['clickrate'];
	            }
	            if($data['adform'] == 4)
	            {
	                $tmpData[] = $v['complate'];
	                $complateData[$k] = $v['complate'];
	                $tmpData[] = $v['coins'];
	                $coinsData[$k] = $v['coins'];
	            }
	            $tmpData[] = $v['income'];
	            $incomeData[$k] = $v['income'];
	            array_push($ajaxData, $tmpData);
	        }
	        $sumData[] = '合计';
	        if($data['adform'] != 4)
	        {
	            $sumData[] = $data['graphreport']['sum']['ask'];
	        }
	        if($data['adform'] == 1 || $data['adform'] == 2)
	        {
	            $sumData[] = $data['graphreport']['sum']['imp'];
	        }
	        if($data['adform'] != 4)
	        {
	            $sumData[] = $data['graphreport']['sum']['click'];
	        }
	        if($data['adform'] == 1 || $data['adform'] == 2)
	        {
	            $sumData[] = $data['graphreport']['sum']['imprate'];
	        }
	        if($data['adform'] == 1 || $data['adform'] == 2)
	        {
	            $sumData[] = $data['graphreport']['sum']['clickrate'];
	        }
	        if($data['adform'] == 4)
	        {
	            $sumData[] = $data['graphreport']['sum']['complate'];
	            $sumData[] = $data['graphreport']['sum']['coins'];
	        }
	
	        $sumData[] = $data['graphreport']['sum']['income'];
	        array_push($ajaxData, $sumData);
	        $data['graphreport']['ajaxlist'] = array_reverse($ajaxData);
	        $data['graphreport']['ajaxgraph'][0] = array_values(($askData));
	        $data['graphreport']['ajaxgraph'][1] = array_values(($impData));
	        $data['graphreport']['ajaxgraph'][2] = array_values(($clickData));
	        $data['graphreport']['ajaxgraph'][3] = array_values(($imprateData));
	        $data['graphreport']['ajaxgraph'][4] = array_values(($clickrateData));
	        if($data['adform'] == 4)
	        {
	            $data['graphreport']['ajaxgraph'][6] = array_values(($coinsData));
	            $data['graphreport']['ajaxgraph'][7] = array_values(($complateData));
	        }
	        $data['graphreport']['ajaxgraph'][5] = array_values(($incomeData));
	        ajaxReturn('', 1, $data['graphreport']);
	        return;
		}
		else{
	        $ajaxData = array();
	        $uidclickData = array();
//	        $startdownloadData = array();
//	        $downloadfailedData = array();
//	        $downloadsuccessData = array();
//	        $startplayData = array();
//	        $playfailedData = array();
	        $playsuccessData = array();
	        $clickvideopicData = array();
	        $incomeData = array();
	        foreach ($data['graphreport']['list'] as $k => $v)
	        {
	            $tmpData = array();
	            $tmpData[] = $k;
	            $tmpData[] = $v['uidclick'];
//	            $tmpData[] = $v['startdownload'];
//	            $tmpData[] = $v['downloadfailed'];
//	            $tmpData[] = $v['downloadsuccess'];
//	            $tmpData[] = $v['startplay'];
//	            $tmpData[] = $v['playfailed'];
	            $tmpData[] = $v['playsuccess'];
	            $tmpData[] = $v['clickvideopic'];
	            $tmpData[] = $v['income'];
                $uidclickData[$k] = $v['uidclick'];
//                $startdownloadData[$k] = $v['startdownload'];
//                $downloadfailedData[$k] = $v['downloadfailed'];
//                $downloadsuccessData[$k] = $v['downloadsuccess'];
//                $startplayData[$k] = $v['startplay'];
//                $playfailedData[$k] = $v['playfailed'];
                $playsuccessData[$k] = $v['playsuccess'];
                $clickvideopicData[$k] = $v['clickvideopic'];
                $incomeData[$k] = $v['income'];
	            array_push($ajaxData, $tmpData);
	        }
	        $sumData[] = '合计';
	        $sumData[] = $data['graphreport']['sum']['uidclick'];
//	        $sumData[] = $data['graphreport']['sum']['startdownload'];
//	        $sumData[] = $data['graphreport']['sum']['downloadfailed'];
//	        $sumData[] = $data['graphreport']['sum']['downloadsuccess'];
//	        $sumData[] = $data['graphreport']['sum']['startplay'];
//	        $sumData[] = $data['graphreport']['sum']['playfailed'];
	        $sumData[] = $data['graphreport']['sum']['playsuccess'];
	        $sumData[] = $data['graphreport']['sum']['clickvideopic'];
	        $sumData[] = $data['graphreport']['sum']['income'];
	        array_push($ajaxData, $sumData);
	        $data['graphreport']['ajaxlist'] = array_reverse($ajaxData);
	        $data['graphreport']['ajaxgraph'][0] = array_values(($uidclickData));
//	        $data['graphreport']['ajaxgraph'][1] = array_values(($startdownloadData));
//	        $data['graphreport']['ajaxgraph'][2] = array_values(($downloadfailedData));
//	        $data['graphreport']['ajaxgraph'][3] = array_values(($downloadsuccessData));
//	        $data['graphreport']['ajaxgraph'][4] = array_values(($startplayData));
//	        $data['graphreport']['ajaxgraph'][5] = array_values(($playfailedData));
	        $data['graphreport']['ajaxgraph'][6] = array_values(($playsuccessData));
	        $data['graphreport']['ajaxgraph'][7] = array_values(($clickvideopicData));
	        $data['graphreport']['ajaxgraph'][8] = array_values(($incomeData));
	        ajaxReturn('', 1, $data['graphreport']);
	        return;
		}
    }

    /**
     * 广告位维度报表
     * Enter description here ...
     */
    function ajaxPositionData(){
        $data = $this->getCondition();
        $data['graphreport'] = $this->getPositionGraphReport($data);

        $ajaxData = array();
        $askData = array();
        $impData = array();
        $clickData = array();
        $incomeData = array();
        $clickrateData = array();
        $imprateData = array();
        foreach ($data['graphreport']['list'] as $k => $v)
        {
            $tmpData = array();
            $tmpData[] = $k;
            if($data['adform'] != 4)
            {
                $tmpData[] = $v['ask'];
                $askData[$k] = $v['ask'];
            }
            if($data['adform'] == 1 || $data['adform'] == 3)
            {
                $tmpData[] = $v['imp'];
                $impData[$k] = $v['imp'];
            }
            if($data['adform'] != 4)
            {
                $tmpData[] = $v['click'];
                $clickData[$k] = $v['click'];
            }
            if($data['adform'] == 1 || $data['adform'] == 3)
            {
                $tmpData[] = $v['imprate'];
                $imprateData[$k] = $v['imprate'];
            }
            if($data['adform'] == 1 || $data['adform'] == 3)
            {
                $tmpData[] = $v['clickrate'];
                $clickrateData[$k] = $v['clickrate'];
            }
            $tmpData[] = $v['income'];
            $incomeData[$k] = $v['income'];
            array_push($ajaxData, $tmpData);
        }
        $sumData[] = '合计';
        if($data['adform'] != 4)
        {
            $sumData[] = $data['graphreport']['sum']['ask'];
        }
        if($data['adform'] == 1 || $data['adform'] == 3)
        {
            $sumData[] = $data['graphreport']['sum']['imp'];
        }
        if($data['adform'] != 4)
        {
            $sumData[] = $data['graphreport']['sum']['click'];
        }
        if($data['adform'] == 1 || $data['adform'] == 3)
        {
            $sumData[] = $data['graphreport']['sum']['imprate'];
        }
        if($data['adform'] == 1 || $data['adform'] == 3)
        {
            $sumData[] = $data['graphreport']['sum']['clickrate'];
        }
        $sumData[] = $data['graphreport']['sum']['income'];
        array_push($ajaxData, $sumData);
        $data['graphreport']['ajaxlist'] = array_reverse($ajaxData);
        $data['graphreport']['ajaxgraph'][0] = array_values(($askData));
        $data['graphreport']['ajaxgraph'][1] = array_values(($impData));
        $data['graphreport']['ajaxgraph'][2] = array_values(($clickData));
        $data['graphreport']['ajaxgraph'][3] = array_values(($imprateData));
        $data['graphreport']['ajaxgraph'][4] = array_values(($clickrateData));
        $data['graphreport']['ajaxgraph'][5] = array_values(($incomeData));
        ajaxReturn('', 1, $data['graphreport']);
        return;
    }

    /**
     * 渠道维度报表
     * Enter description here ...
     */
    function ajaxChannelList(){
        $appid = $this->input->get('appid');
        $keys = $this->input->get('keys');
        if(empty($appid) || empty($keys))
        {
            return;
        }
        $channellist = $this->AppChannelClass->getSearchList($keys,$appid,$this->userInfo['userid']);
        $redata = array();
        foreach ($channellist as $v)
        {
            $redata[] = array('id'=>$v['publisherID'], 'name'=>$v['channelname']);
        }
        ajaxReturn('', 1, $redata);
        return;
    }

    /**
     * 渠道维度报表
     * Enter description here ...
     */
    function ajaxPositionList(){
        $appid = $this->input->get('appid');
        $keys = $this->input->get('keys');
        $adform = $this->input->get('adform');
        if(empty($appid) || empty($keys) || empty($adform))
        {
            return;
        }
        $channellist = $this->AppPositionClass->getSearchList($keys, $appid, $adform, $this->userInfo['userid']);
        $redata = array();
        foreach ($channellist as $v)
        {
            $redata[] = array('id'=>$v['positionid'], 'name'=>$v['positionname']);
        }
        ajaxReturn('', 1, $redata);
        return;
    }

    /**
     * 渠道维度报表
     * Enter description here ...
     */
    function detailChannel(){
        $data = $this->getCondition();
        $data['graphreport'] = $this->getChannelGraphReport($data);
        $this->load->view('manage/appreport/detailchannel', $data);
    }

    /**
     * 渠道维度报表
     * Enter description here ...
     */
    function detailPosition(){
        $data = $this->getCondition();
        $data['graphreport'] = $this->getPositionGraphReport($data);
        $this->load->view('manage/appreport/detailposition', $data);
    }

    /**
     * 广告位维度报表
     * Enter description here ...
     */
    function positionreport(){
        $data = $this->getCondition();
        $data['applist'] =  $this->AppInfoClass->getAppInfoList(array('ostypeid'=>$data['ostype']),$this->userInfo);
        $data['commonreport'] = $this->getCommonReport($data['applist'], $data['appid'], $data['sdate'], $data['edate'], $data['adform']);
        $data['reportlist'] = $this->getPositionReport($data['applist'], $data['appid'], $data['sdate'], $data['edate'], $data['adform'], $data['ostype']);
        if($data['sdate'] == $data['edate'])
        {
            $data['dateinfolist'] = mkhours();
        }
        else
        {
            $data['dateinfolist'] = mktimes($data['sdate'], $data['edate'], true, true);
        }
        array_multisort($data['dateinfolist'], SORT_DESC);
        $this->load->view('manage/appreport/positionreport', $data);
    }

    /**
     * 渠道数据
     * Enter description here ...
     * @param unknown_type $campaignId
     * @param unknown_type $adform
     */
    private function getChannelReport($appList, $appId, $sdate, $edate, $adform, $ostype)
    {
        $tmpAppIds = '';
        if(!empty($appList))
        {
            foreach ($appList as $v)
            {
                if($v['appid'] == $appId)
                {
                    $tmpAppIds = array($appId);
                    break;
                }
                else
                {
                    $tmpAppIds[] = $v['appid'];
                }
            }
        }
        if(!empty($tmpAppIds))
        {
            $appIds = implode(',', $tmpAppIds);
        }
        else
        {
            $appIds = '';
        }
        $dataClass = new AppReportClass();
        $data = $dataClass->getAppReport($appIds, $sdate, $edate, $adform, 'pkCanalId','pkPublisherId');
        //        $channelList = $this->AppChannelClass->getChannelTypeList($ostype, $this->userid);
        if(count($tmpAppIds) > 1)
        {
            $punchboxList = $this->AppChannelClass->getList('', $this->userid);
        }
        else
        {
            $punchboxList = $this->AppChannelClass->getList($appIds, $this->userid);
        }
        $tmpPboxList = null;
        if(!empty($punchboxList))
        foreach($punchboxList as $v)
        {
            $tmpPboxList[$v['publisherID']] = $v;
        }
        if(!empty($data))
        {
            foreach ($data as $k => $v)
            {
                $data[$k]['channelname'] = '未知渠道';
                if(isset($tmpPboxList[$v['pkPublisherId']]))
                {
                    $data[$k]['channelname'] = $tmpPboxList[$v['pkPublisherId']]['channelname'];
                }
            }
        }
        $redata['report'] = $data;
        $redata['sumreport'] = $dataClass->getSumData();
        return $redata;
    }

    /**
     * 广告位数据
     * Enter description here ...
     * @param unknown_type $campaignId
     * @param unknown_type $adform
     */
    private function getPositionReport($appList, $appId, $sdate, $edate, $adform, $ostype)
    {
        $tmpAppIds = '';
        $tmpAppListArrya = null;
        if(!empty($appList))
        foreach ($appList as $v)
        {
            if($v['appid'] == $appId)
            {
                $tmpAppIds = array($appId);
                break;
            }
            else
            {
                $tmpAppIds[] = $v['appid'];
            }
            $tmpAppListArrya[$v['appid']] = $v;
        }
        if(!empty($appList))
        {
            $appIds = implode(',', $tmpAppIds);
        }
        else
        {
            $appIds = '';
        }
        $dataClass = new AppReportClass();
        $data = $dataClass->getAppReportTow($appIds, $sdate, $edate, $adform, 'pkCanalId', 'pkCanalId', 'pkPlaceId');
        //        $channelList = $this->AppChannelClass->getChannelTypeList($ostype, $this->userid);

        $positionList = $this->AppPositionClass->getList(array('userid' => $this->userid, 'adform' => $adform,));

        $tmpPositionList = null;
        if(!empty($positionList))
        foreach($positionList as $v)
        {
            $tmpPositionList[$v['positionid']] = $v;
        }
        if(!empty($data))
        foreach ($data as $key => $value)
        {
            foreach ($value as $k => $v)
            {
                $data[$key][$k]['appname'] = '未知渠道';
                $data[$key][$k]['positionname'] = '默认广告位';
                if(isset($tmpAppListArrya[$key]))
                {
                    $data[$key][$k]['appname'] = $tmpAppListArrya[$key]['appname'];
                }
                if(isset($tmpPositionList[$v['pkPlaceId']]))
                {
                    $data[$key][$k]['positionname'] = $tmpPositionList[$v['pkPlaceId']]['positionname'];
                }
            }

        }
        $redata['report'] = $data;
        $redata['sumreport'] = $dataClass->getSumData();
        return $redata;
    }

    /**
     * App版本数据
     * Enter description here ...
     * @param unknown_type $campaignId
     * @param unknown_type $adform
     */
    private function getAppVerReport($appList, $appId, $sdate, $edate, $adform, $ostype)
    {
        $tmpAppIds = '';
        $tmpAppListArrya = null;
        if(!empty($appList))
        foreach ($appList as $v)
        {
            if($v['appid'] == $appId)
            {
                $tmpAppIds = array($appId);
                break;
            }
            else
            {
                $tmpAppIds[] = $v['appid'];
            }
            $tmpAppListArrya[$v['appid']] = $v;
        }
        if(!empty($tmpAppIds))
        {
            $appIds = implode(',', $tmpAppIds);
        }
        else
        {
            $appIds= '';
        }
        $dataClass = new AppReportClass();
        $data = $dataClass->getAppReportTow($appIds, $sdate, $edate, $adform, 'pkCanalId', 'pkCanalId', 'pkAppVer');
        if(!empty($data))
        foreach ($data as $key => $value)
        {
            foreach ($value as $k => $v)
            {
                $data[$key][$k]['appname'] = '未知渠道';
                if(isset($tmpAppListArrya[$key]))
                {
                    $data[$key][$k]['appname'] = $tmpAppListArrya[$key]['appname'];
                }
            }
        }
        $redata['report'] = $data;
        $redata['sumreport'] = $dataClass->getSumData();
        return $redata;
    }

    /**
     * 通用数据
     * Enter description here ...
     * @param unknown_type $campaignId
     * @param unknown_type $adform
     */
    private function getAppGraphReport($data)
    {
        $appIds = $data['appid'];
        if (!$appIds) return array();

        $sdate = $data['sdate'];
        $edate = $data['edate'];
        $adform = $data['adform'];
        $incentiveswitch = $data['incentiveswitch'];
        if($incentiveswitch == 1){
        	$data = $this->AppReportClass->getAppGraphReportVideo($appIds, $sdate, $edate, 'pkCanalId', $adform, 'pkDay');
        	$data['isincentiveswitch']=1;
        }
        else{
        	$data = $this->AppReportClass->getAppGraphReport2($appIds, $sdate, $edate, 'pkCanalId', $adform, 'pkDay');
        	$data['isincentiveswitch']=0;
        }
        return $data;
    }

    /**
     * 通用数据
     * Enter description here ...
     * @param unknown_type $campaignId
     * @param unknown_type $adform
     */
    private function getChannelGraphReport($data)
    {
        $publisherID = $data['publisherid'];
        $sdate = $data['sdate'];
        $edate = $data['edate'];
        $adform = $data['adform'];
        $appid = $data['appid'];
    	$incentiveswitch = $data['incentiveswitch'];
        if($incentiveswitch == 1){
        	$data = $this->AppReportClass->getAppGraphReportVideo($publisherID, $sdate, $edate, 'pkPublisherId', $adform, 'pkDay', $appid);
        	$data['isincentiveswitch']=1;
        }
        else{
        	$data = $this->AppReportClass->getAppGraphReport($publisherID, $sdate, $edate, 'pkPublisherId', $adform, 'pkDay', $appid);
        	$data['isincentiveswitch']=0;
        }
        
        return $data;
    }

    /**
     * 通用数据
     * Enter description here ...
     * @param unknown_type $campaignId
     * @param unknown_type $adform
     */
    private function getPositionGraphReport($data)
    {
        $positionid = $data['positionid'];
        $sdate = $data['sdate'];
        $edate = $data['edate'];
        $adform = $data['adform'];
        $appid = $data['appid'];
        $data = $this->AppReportClass->getAppGraphReport($positionid, $sdate, $edate, 'pkPlaceId', $adform, 'pkDay', $appid);
        return $data;
    }

    /**
     * 处理页面请求数据
     * Enter description here ...
     */
    function getCondition(){
        $condition = array();
        $osType = $this->input->post("ostype");
        $appid = $this->input->get_post("appid");
        $adforms = $this->input->post("adform");
        $adform = $adforms;
        $isnonincentive = 0;
        $adforms = explode('_', $adforms);
        if(count($adforms)== 2 && $adforms[0] == 4){
        	$adform = $adforms[0];
        	$isnonincentive = $adforms[1];
        }
        $inputDate = $this->input->post("inputDate");
        $publisherid = $this->input->post("publisherid");
        $positionid = $this->input->post("positionid");
        $timeDate = explode('~', $inputDate);
        if(count($timeDate) == 2)
        {
            $sdate = $timeDate[0];
            $edate = $timeDate[1];
        }
        if(empty($sdate) || empty($edate))
        {
            $sdate = date('Y-m-d', time() - 6*86400);
            $edate = date('Y-m-d');
        }

        $condition['leftlist'] = $this->AppInfoClass->getAppInfoList('', $this->userInfo);
        $condition['applist'] =  $condition['leftlist'];
        if(empty($appid))
        {
            if (is_array($condition['applist'])) {
                $appid = $condition['applist'][0]['appid'];
            }
        }
        $condition['appid'] = $appid;
        $condition['sdate'] = $sdate;
        $condition['edate'] = $edate;
        $condition['adform'] = empty($adform)?1:$adform;
        $myadform = $this->input->post("adform");
        $condition['adforms'] = empty($myadform)?1:$myadform;
        $condition['incentiveswitch'] = $isnonincentive;
        $condition['channellist'] =  $this->AppChannelClass->getRatioTopTenList($appid,$this->userInfo['userid']);
        $condition['publisherid'] =  isset($_POST['publisherid'])?$publisherid:$condition['channellist'][0]['publisherID'];
        $condition['positionlist'] =  $this->AppPositionClass->getList(array('userid' => $this->userid, 'adform' => $adform, 'appid'=>$appid));
        $condition['positionid'] =  isset($_POST['publisherid'])?$positionid:$condition['positionlist'][0]['positionid'];
        $condition['appinfo'] = $this->AppInfoClass->getDetailInfo($this->userInfo, $appid);
        return $condition;
    }
}
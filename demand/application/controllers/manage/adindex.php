<?php
/**
 * 应用管理类
 * @filename	: appinfo.php
 * @author		: wangdong
 * @datetime	: 2013-10-15
 * @Description  : 前台App管理页面。
 */

class AdIndex extends MY_Controller{
	private  $userid = null;
	private $urlmatch='/^(http:\/\/)?(https:\/\/)?([\w\d-]+\.)+[\w-]+(\/[\d\w-.\/\!\+?%&=]*)?$/Ui';
	public function __construct()
	{
		parent::__construct();
		header("Cache-control:no-cache,no-store,must-revalidate");
		header("Pragma:no-cache");
		header("Expires:0");
	}

	/**
	 * 开发者首页。
	 * @author wangdong
	 */
	function index(){
		$this->loadHeader('开发者管理', 0, 0);
		$this->load->view('manage/index');
	}
}
<?php
/**
 * 应用管理类
 * @filename	: appinfo.php
 * @author		: wangdong
 * @datetime	: 2013-10-15
 * @Description  : 前台App管理页面。
 */

class AppTag extends MY_Controller{
    private  $userid = null;
    public function __construct()
    {
        parent::__construct();
        header("Cache-control:no-cache,no-store,must-revalidate");
        header("Pragma:no-cache");
        header("Expires:0");
        $this->load->model('manage/AppInfoClass');
        $this->load->model('manage/AppTagClass');
        $this->load->model('manage/AppPositionClass');
        $this->load->model('manage/AppTagRelationClass');
    }

    /**
     * 显示标签。
     * @author wangdong
     */
    function showInfo(){
        $appId = $this->input->get('appid');
        if(empty($appId))
        {
            redirect("/manage/appinfo/showlist");
            exit;
        }
        $data = $this->AppInfoClass->getDetailInfo($this->userInfo, $appId);
        $data['apptaglist'] = $this->AppTagRelationClass->getList($appId, $this->userInfo['userid']);
        $allTagList = $this->AppTagClass->getList();
        $data['alltaglist'] = null;
        if($allTagList != false)
        {
            foreach ($allTagList as $value)
            {
                $data['alltaglist'][$value['tagid']] = $value;
            }
        }
        if(!empty($data['apptaglist']))
        {
            foreach($data['apptaglist'] as $v)
            {
                if(isset($data['alltaglist'][$v['tagid']]))
                {
                    unset($data['alltaglist'][$v['tagid']]);
                }
            }
        }
        $data['appid'] = $appId;
        $data['leftlist'] = $this->AppInfoClass->getAppInfoList('', $this->userInfo);
        $this->loadHeader('标签管理', 2, 21);
        $this->load->view('manage/apptag/showinfo', $data);
    }

    /**
     *  添加标签。
     * @author wangdong
     */
    function doAdd(){
        $postData = $this->input->post();
        $AppTagInfo = $this->AppTagClass->doAdd($postData, $this->userInfo);
        if($AppTagInfo['status'] == 0)
        {
            ajaxReturn($AppTagInfo['info'], 0, $AppTagInfo['data']);
        }
        ajaxReturn('', 1, $AppTagInfo['id']);
    }

    /**
     *  关联标签。
     * @author wangdong
     */
    function doAddAppTag(){
        $postData = $this->input->post();
        $appTagInfo = $this->AppTagRelationClass->doAdd($postData, $this->userInfo);
        if($appTagInfo['status'] == 0)
        {
            ajaxReturn($appTagInfo['info'], 0, $appTagInfo['data']);
        }
        ajaxReturn('', 1, '');
    }

    /**
     *  删除关联标签。
     * @author wangdong
     */
    function doDelAppTag(){
        $postData = $this->input->post();
        $appTagInfo = $this->AppTagRelationClass->doDel($postData, $this->userInfo);
        if($appTagInfo['status'] == 0)
        {
            ajaxReturn($appTagInfo['info'], 0, $appTagInfo['data']);
        }
        ajaxReturn('', 1, '');
    }
}
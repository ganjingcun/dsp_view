<?php
/**
 * 应用管理类
 * @filename	: appinfo.php
 * @author		: wangdong
 * @datetime	: 2013-10-15
 * @Description  : 前台App管理页面。
 */

class AppChannel extends MY_Controller{
    private  $userid = null;
    public function __construct()
    {
        parent::__construct();
        header("Cache-control:no-cache,no-store,must-revalidate");
        header("Pragma:no-cache");
        header("Expires:0");
        $this->load->model('manage/AppInfoClass');
        $this->load->model('manage/AppPositionClass');
        $this->load->model('manage/AppChannelClass');
    }

    /**
     * 添加应用渠道。
     * @author wangdong
     */
    function showAdd(){
        $appid = $this->input->get('appid');
        $channelId = $this->input->get('channelid');
        if(empty($appid) || empty($publisherID))
        {
            redirect("/manage/appinfo/showlist");
            exit;
        }
        $data['channelList'] = $this->AppPositionClass->getList(array('appid'=>$appid, 'userid' => $this->userInfo['userid'], 'status' => 0));
        $data['channelid'] = $this->AppPositionClass->createPositionId($appid);
        $data['channelInfo'] = '';
        if($data['channelList'])
        {
            foreach ($data['channelList'] as $v)
            {
                if($v['channelid'] == $channelId)
                {
                    $data['channelInfo'] = $v;
                    break;
                }
            }
        }
        $data['appid'] = $appid;
        $this->loadHeader('添加渠道', 2, 21);
        $this->load->view('manage/appchannel/showadd', $data);
    }


    /**
     * 渠道管理。
     * @author wangdong
     */
    function showInfo(){
        $appId = $this->input->get('appid');
        if(empty($appId))
        {
            redirect("/manage/appinfo/showlist");
            exit;
        }
        $data = $this->AppInfoClass->getDetailInfo($this->userInfo, $appId);
        $data['channellist'] = $this->AppChannelClass->getList($appId, $this->userInfo['userid']);
        $channelTypeList = $this->AppChannelClass->getChannelTypeList($data['ostypeid'], $this->userInfo['userid'], $this->userInfo['usertype']);
        $data['channeltype'] = null;
        if($channelTypeList != false)
        {
            foreach ($channelTypeList as $value)
            {
                $data['channeltype'][$value['channelid']] = $value;
            }
        }
        if($data['channellist'] != false){
	        foreach($data['channellist'] as $v)
	        {
	            if(isset($data['channeltype'][$v['channelid']]))
	            {
	                unset($data['channeltype'][$v['channelid']]);
	            }
	        }
        }
        //自有用户渠道处理
        $data['ownchanneltype'] = array();
        if($this->userInfo['usertype'] == 3)
        {
            foreach ($data['channeltype'] as $k => $v)
            {
                if($v['parentid'] == 0)
                {
                    $data['ownchanneltype'][$v['channelid']] = $v;
                }
                else 
                {
                    $data['ownchanneltype'][$v['parentid']]['childlist'][$v['channelid']] = $v;
                }
            }
        }
        $this->load->model('manage/MarketClass');
        $data['marketlist'] = $this->MarketClass->getList(array('status'=>0));
        $data['appid'] = $appId;
        $data['leftlist'] = $this->AppInfoClass->getAppInfoList('', $this->userInfo);
        $data['usertype'] = $this->userInfo['usertype'];
        $this->loadHeader('渠道管理', 2, 21);
        $this->load->view('manage/appchannel/showinfo', $data);
    }

    /**
     *  搜索渠道。
     * @author wangdong
     */
    function searchChannel(){
        $appId = $this->input->post('appid');
        $keys = trim($this->input->post('keys'));
        if(empty($appId) || empty($keys))
        {
            ajaxReturn('keyserr', 0, array('data'=>array('keysmsg'=>'请刷新页面后重试！')));
            return;
        }
        $data = $this->AppInfoClass->getDetailInfo($this->userInfo, $appId);
        $data['channellist'] = $this->AppChannelClass->getList($appId, $this->userInfo['userid']);
        $channelTypeList = $this->AppChannelClass->getChannelTypeList($data['ostypeid'], $this->userInfo['userid'], $this->userInfo['usertype']);
        $data['channeltype'] = null;
        if($channelTypeList != false)
        {
            foreach ($channelTypeList as $value)
            {
                $data['channeltype'][$value['channelid']] = $value;
            }
        }
        foreach($data['channellist'] as $v)
        {
            if(isset($data['channeltype'][$v['channelid']]))
            {
                unset($data['channeltype'][$v['channelid']]);
            }
        }
        $reData = array();
        foreach ($data['channeltype'] as $k => $v)
        {
            if(strpos(strtolower($v['channelname']), strtolower($keys)) !== false)
            {
                $tmp = array();
                $tmp['channelid'] = $v['channelid'];
                $tmp['channelname'] = $v['channelname'];
                $reData[] = $tmp;
            }
        }
        if(empty($reData))
        {
            ajaxReturn('', 2, '');
        }        
        ajaxReturn('', 1, $reData);
    }

    /**
     *  添加渠道。
     * @author wangdong
     */
    function doAdd(){
        $postData = $this->input->post();
        $channelInfo = $this->AppChannelClass->doAdd($postData, $this->userInfo);
        if($channelInfo['status'] == 0)
        {
            ajaxReturn($channelInfo['info'], 0, $channelInfo['data']);
        }
        ajaxReturn('', 1, $channelInfo['id']);
    }

    /**
     *  申请渠道PunchboxId。
     * @author wangdong
     */
    function doAddChannel(){
        $postData = $this->input->post();
        $channelInfo = $this->AppChannelClass->doAddChannel($postData, $this->userInfo);
        if($channelInfo['status'] == 0)
        {
            ajaxReturn($channelInfo['info'], 0, $channelInfo['data']);
        }
        ajaxReturn('', 1, '');
    }
    
    
	/**
     *  添加渠道。
     * @author wangdong
     */
    function appstore(){
        $this->load->model("manage/MarketClass");
        $marketlist = $this->MarketClass->getList(array('status'=>0));
        if($marketlist)
        {
            ajaxReturn("", 1, $marketlist);
        }
        ajaxReturn('', 0, "");
    }
}
<?php /**
* @filename	: apptypeclass.php
* @encoding	: UTF-8
* @author		: niejianhui
* @datetime	: 2012-12-17 17:19:24
* @Description  : 应用类型model
*/
class AppTagRelationClass extends MY_Model{

    /**
     * 初始化
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * 删除App标签
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    public function doDel($params, $userInfo)
    {
        $chk = $this->chkDelInfo($params, $userInfo);
        if($chk !== true)
        {
            $err['status'] = 0;
            $err = array_merge($err, $chk);
            return $err;
        }
        $this->db->where('appid', $params['appid']);
        $this->db->where('tagid', $params['tagid']);
        $this->db->where('userid', $userInfo['userid']);
        $this->db->update('app_tag_relation', array('status' => 0));
        $upNum = $this->db->affected_rows();
        if($upNum <= 0) return array('status' => 0, 'info' => 'deltagiderr', 'data' => '');
        return array('status' => 1);
        
    }

    /**
     * 增加App标签
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    public function doAdd($params, $userInfo)
    {
        $chk = $this->chkAddInfo($params, $userInfo);
        if($chk !== true)
        {
            $err['status'] = 0;
            $err = array_merge($err, $chk);
            return $err;
        }
        $tagId = explode(',', $params['tagid']);
        $this->load->model('manage/AppTagClass');
        $appTagList = $this->getList($params['appid'], $userInfo['userid']);
        if(!empty($appTagList))
        {
            foreach ($appTagList as $value) {
                $appTagIdArray[$value['tagid']] = $value['tagname'];
            }
        }
        $tagList = $this->AppTagClass->getList();
        foreach ($tagList as $value) {
            $tagIdArray[$value['tagid']] = $value['tagname'];
        }
        foreach ($tagId as $v)
        {
            if(!empty($v))
            {
                if(isset($appTagIdArray[$v]))
                {
                    continue;
                }
                $data['userid'] = $userInfo['userid'];
                $data['appid'] = $params['appid'];
                $data['tagid'] = $v;
                $data['status'] = 1;
                if(!isset($tagIdArray[$v]))
                {
                    continue;
                }
                $data['tagname'] = $tagIdArray[$v];
                $data['createtime'] = time();
            }
            else
            {
                continue;
            }
            $dataBatch[] = $data;
        }
        if(empty($dataBatch))
        {
            return array('status' => 0, 'info' => 'tagiderr', 'data' => array('tagidmsg', '未知错误。'));
        }
        $this->db->insert_batch('app_tag_relation', $dataBatch);
        $id = $this->db->insert_id();
        if($id <= 0) return array('status' => 0, 'info' => 'tagiderr', 'data' => array('tagidmsg', '未知错误。'));
        return array('status' => 1, 'id' => $id);
    }

    /**
     * 检测标签信息
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    private function chkAddInfo($params, $userInfo)
    {
        if(empty($params['appid']))
        {
            $error['data'] = array('tagidmsg', '未知错误。');
            $error['info'] = 'tagiderr';
            return $error;
        }
        if(empty($params['tagid']))
        {
            $error['data'] = array('tagidmsg', '请选择标签。');
            $error['info'] = 'tagiderr';
            return $error;
        }
        return true;
    }

    /**
     * 检测标签信息
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    private function chkDelInfo($params, $userInfo)
    {
        if(empty($params['appid']))
        {
            $error['data'] = '';
            $error['info'] = 'deltagiderr';
            return $error;
        }
        if(empty($params['tagid']))
        {
            $error['data'] = '';
            $error['info'] = 'deltagiderr';
            return $error;
        }
        return true;
    }

    /**
     * 获取标签列表
     */
    public function getList($appId, $userId)
    {
        $where['userid'] = $userId;
        $where['appid'] = $appId;
        $where['status'] = 1;
        $this->db->order_by('createtime', "desc");
        $query = $this->db->get_where('app_tag_relation', $where);
        if ($query->num_rows() > 0)
        {
            $data = $query->result_array();
        }
        if(empty($data))
        {
            return false;
        }
        return $data;
    }
}
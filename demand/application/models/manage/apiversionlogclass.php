<?php /**
* @filename	: apiversionlogclass.php
* @encoding	: UTF-8
* @author		: wf
* @datetime	: 2015-05-28
* @Description  : 应用类型model
*/
class ApiVersionLogClass extends MY_Model{

    /**
     * 初始化
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * 获取标签列表
     */
    public function getRow()
    {
    	$where=array();
        $this->db->order_by('id', "desc");
        $query = $this->db->get_where('apiversion_log', $where);
        if ($query->num_rows() > 0)
        {
            $data = $query->row_array();
        }
        if(empty($data))
        {
            return false;
        }
        return $data;
    }
    
	/**
     * 获取标签列表
     */
    public function getList()
    {
    	$where=array();
        $this->db->order_by('id', "desc");
        $query = $this->db->get_where('apiversion_log', $where);
        if ($query->num_rows() > 0)
        {
            $data = $query->result_array();
        }
        if(empty($data))
        {
            return false;
        }
        return $data;
    }

}
<?php /**
* @filename	: apptypeclass.php
* @encoding	: UTF-8
* @author		: niejianhui
* @datetime	: 2012-12-17 17:19:24
* @Description  : 应用类型model
*/
define('UPLOAD_APP_PATH',UPLOAD_DIR.'/app/'.date('Ym'));
class AppInfoClass extends MY_Model{
    private $urlmatch='/^(https?:\/\/).?([\w\d-]+\.)+[\w-]+(\/[\d\w-.\/\!\+?%&=]*)?$/Ui';
    /**
     * 初始化
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('tools');
    }


    /**
     * 新建应用
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    public function doAdd($params, $userInfo)
    {
        $error['status'] = 0;
        if (isset($params['appname']))
        {
            $params['appname'] = trim($params['appname']);
        }
        if (isset($params['appdescription']))
        {
            $params['appdescription'] = trim($params['appdescription']);
        }
        $chk = $this->chkAddInfo($params);
        if($chk !== true)
        {
            $err['status'] = 0;
            $err = array_merge($err, $chk);
            return $err;
        }

        //        $cocodata['username'] = $userInfo['username'];
        //        $cocodata['app_name'] = $params['appname'];
        //        //        if($params['apptypeid'] == 1)
        //        //        {//格式 1-9-10
        //        //            $cocodata['app_type[]'] = 101;
        //        //        }
        //        //        else
        //        //        {
        //        //            $cocodata['app_type[]'] = 1;
        //        //        }
        //        $cocotype = trim($params['apptypeid'], ',');
        //        $cocodata['app_type[]'] = str_replace(',', '-', $cocotype);
        //        if($params['ostypeid']==1)
        //        {
        //            $cocodata['app_equip'] = 'android';
        //        }
        //        else
        //        {
        //            if($params['devicetypeid'] == 2)
        //            {
        //                $cocodata['app_equip'] = 'iphone';
        //            }
        //            if($params['devicetypeid'] == 3)
        //            {
        //                $cocodata['app_equip'] = 'ipad';
        //            }
        //            if($params['devicetypeid'] == 4)
        //            {
        //                $cocodata['app_equip'] = 'universal';
        //            }
        //        }
        //        $cocodata['app_des'] = $params['appdescription'];
        //        $signData['from'] = 'ads';
        //        $signData['username'] = $userInfo['username'];
        //        $sign = cocoSignData($signData);
        //        $username = urlencode($userInfo['username']);
        //        $cocoUrl = "{$this->config->item('openurl')}/api/add_app/ads/{$username}/{$sign}";
        //        $postdata = http_build_query($cocodata);
        //        $reg_status = curlPost($cocoUrl, $postdata);
        //        $getData = json_decode($reg_status, true);
        //        if(empty($getData))
        //        {
        //            $error['data'] = array('appnamemsg', '未知错误，请刷新重试！');
        //            $error['info'] = 'appnameerr';
        //            return $error;
        //            break;
        //        }
        //        if($getData['status'] == 'error')
        //        {
        //            switch ($getData['errorcode'])
        //            {
        //                case 1001:
        //                    $error['data'] = array('appnamemsg', '未知错误，请刷新重试！');
        //                    $error['info'] = 'appnameerr';
        //                    return $error;
        //                    break;
        //                case 1002:
        //                    $error['data'] = array('appnamemsg', '未知错误，请刷新重试！');
        //                    $error['info'] = 'appnameerr';
        //                    return $error;
        //                    break;
        //                case 1003:
        //                    $error['data'] = array('appnamemsg', '应用名称不能为空。');
        //                    $error['info'] = 'appnameerr';
        //                    return $error;
        //                    break;
        //                case 1004:
        //                    //                    $error['data'] = array('apptypeidmsg', '应用分类不能超过3个。');
        //                    //                    $error['info'] = 'apptypeiderr';
        //                    //                    return $error;
        //                    break;
        //                case 1005:
        //                    $error['data'] = '';
        //                    $error['info'] = 'apptypeiderr';
        //                    return $error;
        //                    break;
        //                case 1006:
        //                    $error['data'] = array('appdescriptionmsg', '应用介绍不能为空，最多不超过1000字。');
        //                    $error['info'] = 'appdescriptionerr';
        //                    return $error;
        //                    break;
        //                case 1007:
        //                    $error['data'] = array('appdescriptionmsg', '应用介绍不能为空，最多不超过1000字。');
        //                    $error['info'] = 'appdescriptionerr';
        //                    return $error;
        //                    break;
        //            }
        //        }
        //        if($getData['status'] == 'success' && !empty($getData['app_id']))
        //        {
        //            $appid = $getData['app_id'];
        //        }
        //        else
        //        {
        //            $error['data'] = array('appnamemsg', '未知错误，请刷新重试！');
        //            $error['info'] = 'appnameerr';
        //            return $error;
        //
        //        }
        //        $data['appid'] = $appid;
        $data['appid'] = $this->produceAppid();
        $data['appname'] = $params['appname'];
        $data['packagename'] = $params['packagename'];
        $data['appurl'] = $params['appurl'];
        $data['apptypeid'] = $params['apptypeid'];
        $data['ostypeid'] = $params['ostypeid'];
        if($data['ostypeid'] == 1)
        {
            $data['devicetypeid'] = 0;
        }
        else
        {
            $data['devicetypeid'] = isset($params['devicetypeid']) ? $params['devicetypeid'] : 0;
        }
        $data['appchildtypeid']= ',' . $params['appchildtypeid'];
        $data['appdescription']= $params['appdescription'];

        /*
        //同步映射到畅思DSP
        $postdata = array();
        $postdata['appname'] = $data['appname'];
        $postdata['thirdpartyid'] = $data['appid'];
        $postdata['packagename'] = $data['packagename'];
        $postdata['appurl'] = $data['appurl'];
        $postdata['ostypeid'] = $data['ostypeid'];
        $postdata['devicetypeid'] = $data['devicetypeid'];
        $postdata['apptypeid'] = $data['apptypeid'];
        $postdata['appchildtypeid']= $data['appchildtypeid'];
        $postdata['appdescription']= $data['appdescription'];
        $postdata = array('params' => $postdata, 'userinfo' => $this->config->item('dsp_api_userinfo'));
        $result = curlPost($this->config->item('dsp_api_server') . 'doaddapp', $postdata, 'POST', true);
        $result = json_decode($result, true);
        if($result['status'] == 1)
        {
            $data['chance_appid'] = $result['data']['appid'];
            $data['chance_publisherID'] = $result['data']['publisherID'];
            $data['chance_position'] = json_encode($result['data']['position']);
            $data['chance_secretkey'] = $result['data']['secretkey'];
        }
        else
        {
            return array('status' => 0, 'info' => '远程服务器错误。', 'data' => null);
//            return $result;
        }
        */

        if(isset($params['isintegral']) && $params['isintegral'] == 1)
        {
            $data['isintegral'] = 1;
            $data['integration'] = $params['integration'];
            $data['unit'] = $params['unit'];
        }

        $data['userid']= $userInfo['userid'];
        $data['createtime'] = time();
        $data['isupdate'] = 1;
        $data['secretkey'] = getAppSecret();
        $this->db->insert('app_info', $data);
        $appId = $this->db->insert_id();
        if($appId <= 0)
        {
            return array('status' => 0, 'info' => 'appnameerr', 'data' => array('appnamemsg', '未知错误。'));
        }
        else
        {
            //默认广告位
            $this->setDefaultPosition($appId, $data);
        }
        $this->load->library('UUID');
        $publisherID = $appId.'-'.UUID::getUUID();
        $pIdInfo = array('appid'=>$appId, 'publisherID' => $publisherID, 'userid' => $userInfo['userid'], 'channelid' => 0, 'channelname' => '官方', 'createtime' => time());
        $this->db->insert('app_channel',$pIdInfo);
        return array('status' => 1, 'appid' => $appId, 'publisherID'=>$publisherID);
    }

    /**
     * 新建应用
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    public function ccAdd($params, $userid)
    {
        foreach($params as $v)
        {
            $data['appname'] = trim($v['app_name']);
            $data['appid'] = $v['id'];
            $data['packagename'] = $v['app_apk'];
            $data['appurl'] = '';
            if(strtolower($v['app_equip']) == 'android' || empty($v['app_equip']))
            {
                $data['ostypeid'] = 1;
                $data['devicetypeid'] = 0;
            }
            else
            {
                $data['ostypeid'] = 2;
            }
            if(strtolower($v['app_equip']) == 'ipad')
            {
                $data['devicetypeid'] = 3;
            }
            if(strtolower($v['app_equip']) == 'iphone')
            {
                $data['devicetypeid'] = 2;
            }
            if(strtolower($v['app_equip']) == 'universal')
            {
                $data['devicetypeid'] = 4;
            }
            if(!empty($v['app_type']))
            {
                $tmpstr = str_replace('-', ',', $v['app_type']);

                $tmparray = explode('-', $v['app_type']);
                $apptypeinfo = $this->getAppTypeList($tmparray[0]);

                if(!empty($apptypeinfo))
                {
                    $parenttypeid = $apptypeinfo[0]['parentid'];
                }
                $data['apptypeid'] = $parenttypeid;
                $data['appchildtypeid']= ','.$tmpstr.',';
            }
            $data['appdescription']= $v['app_des'];
            $data['userid']= $userid;
            $data['createtime'] = strtotime($v['create_time']);
            $data['isupdate'] = 1;
            try
            {
                $this->db->insert('app_info', $data);
                $appId = @$this->db->insert_id();
                if($appId <= 0)
                {
                    continue;
                }
                else
                {
                    //默认广告位
                    $this->setDefaultPosition($appId, $data);
                }
                $this->load->library('UUID');
                $publisherID = $appId.'-'.UUID::getUUID();
                $pIdInfo = array('appid'=>$appId, 'publisherID' => $publisherID, 'userid' => $userid, 'channelid' => 0, 'channelname' => '官方', 'createtime' => time());
                $this->db->insert('app_channel',$pIdInfo);
            }
            catch (Exception $e)
            {
                continue;
            }
        }
        return array('status' => 1);
    }

    private function produceAppid(){
	    usleep(100000);
        $time_str = microtime();
        $app_id = '8' . substr($time_str, 14, 7) . substr($time_str, 2, 1);
        return  $app_id;
    }

    /**
     *
     */
    public function setDefaultPosition($appId, $data)
    {
    	$query = $this->db->get_where('ad_border', array('defaultborder'=>1, 'status'=>1));
    	$rst = $query->result_array();
    	$bordergroupid = null;
    	if($rst){
    		$bordergroupid = isset($rst[0]['bordergroupid'])?$rst[0]['bordergroupid']:null;
    	}
        $tmp = array();
        $tmp['positionname'] = '默认广告位';
        $tmp['positiondesc'] = '默认广告位';
        $tmp['adform'] = 1;
        $tmp['appid']= $appId;
        $tmp['trusteeshiptype'] = 0;
        $tmp['userid'] = $data['userid'];
        $tmp['createtime'] = time();
        $batchData[] = $tmp;
        $tmp_pop = array();
        $tmp_pop['positionname'] = '默认广告位';
        $tmp_pop['positiondesc'] = '默认广告位';
        $tmp_pop['adform'] = 2;
        $tmp_pop['appid']= $appId;
        $tmp_pop['trusteeshiptype'] = 0;
        $tmp_pop['userid'] = $data['userid'];
        if($bordergroupid){
	        $tmp_pop['adstyle'] = 1;
	        $tmp_pop['bordertype'] = 1;
	        $tmp_pop['borderpolicy'] = 1;
	        $tmp_pop['bordergroupid'] = $bordergroupid;
        }
        $tmp_pop['createtime'] = time();
        $tmp = array();
        $tmp['positionname'] = '默认广告位';
        $tmp['positiondesc'] = '默认广告位';
        $tmp['adform'] = 3;
        $tmp['appid']= $appId;
        $tmp['trusteeshiptype'] = 0;
        $tmp['userid'] = $data['userid'];
        $tmp['createtime'] = time();
        $batchData[] = $tmp;

        if(isset($data['isintegral']) && $data['isintegral'] == 1)
        {

            $tmp = array();
            $tmp['positionname'] = '默认广告位';
            $tmp['positiondesc'] = '默认广告位';
            $tmp['adform'] = 4;
            $tmp['appid']= $appId;
            $tmp['userid'] = $data['userid'];
            $tmp['createtime'] = time();
            $tmp['trusteeshiptype'] = 2;
            $batchData[] = $tmp;
        }
        $this->db->insert('app_position', $tmp_pop);
        $this->db->insert_batch('app_position', $batchData);
    }
    /**
     * 更改app的积分回调数据
     */
    function editAppwall($params,$userInfo){
        $data['appcampaignid'] = $params['appcampaignid'];
        $data['wallrechargetype'] = $params['wallrechargetype'];
        $this->db->where('appid', $params['appid']);
        $this->db->where('userid', $userInfo['userid']);
        $this->db->update('app_info', $data);
        $upNum = $this->db->affected_rows();
        if($upNum <= 0) return array('status' => 0, 'info' => 'appnameerr', 'data' => array('appnamemsg', '未知错误。'));
        return array('status' => 1, 'appid' => $params['appid']);
    }
    /**
     * 编辑应用
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    public function doEdit($params, $userInfo)
    {
        $error['status'] = 0;
        $params['appname'] = trim($params['appname']);
        $params['appdescription'] = trim($params['appdescription']);
        $chk = $this->chkEditInfo($params);
        if($chk !== true)
        {
            $err['status'] = 0;
            $err = array_merge($err, $chk);
            return $err;
        }

        $data['appname'] = $params['appname'];
        $data['packagename'] = $params['packagename'];
        $data['appurl'] = $params['appurl'];
        $data['appchildtypeid']= ',' . $params['appchildtypeid'];
        $data['appdescription']= $params['appdescription'];
        //app基本信息修改时不允许开启或关闭积分墙。
        //        if(isset($params['isintegral']) && $params['isintegral'] == 1)
        //        {
        //            $data['isintegral'] = 1;
        //            $data['integration'] = $params['integration'];
        //            $data['unit'] = $params['unit'];
        //        }
        //        else
        //        {
        //            $data['isintegral'] = 0;
        //            $data['integration'] = 0;
        //            $data['unit'] = '金币';
        //        }
        //    $cocoAppInfo = $this->getAppInfoRow($params['appid'], $userInfo['userid']);
        //    if(empty($cocoAppInfo))
        //    {
        //        $error['data'] = array('appnamemsg', '未知错误，请刷新重试1！');
        //        $error['info'] = 'appnameerr';
        //        return $error;
        //        break;
        //    }
        //    $cocodata['username'] = $userInfo['username'];
        //    $cocodata['app_name'] = $params['appname'];
        //    $cocodata['app_id'] = $params['appid'];
        //    $cocotype = trim($params['appchildtypeid'], ',');
        //    $cocodata['app_type[]'] = str_replace(',', '-', $cocotype);
        //    $cocodata['app_des'] = $params['appdescription'];
        //    $signData['from'] = 'ads';
        //    $signData['username'] = $userInfo['username'];
        //    $sign = cocoSignData($signData);
        //    $username = urlencode($userInfo['username']);
        //    $cocoUrl = "{$this->config->item('openurl')}/api/edit_app/ads/{$username}/{$sign}";
        //    $postdata = http_build_query($cocodata);
        //    $reg_status = curlPost($cocoUrl, $postdata);
        //    $getData = json_decode($reg_status, true);
        //    if(empty($getData))
        //    {
        //        return ;
        //    }
        //    if($getData['status'] == 'error')
        //    {
        //        switch ($getData['errorcode'])
        //        {
        //            case 1001:
        //                $error['data'] = array('appnamemsg', '未知错误，请刷新重试！');
        //                $error['info'] = 'appnameerr';
        //                return $error;
        //                break;
        //            case 1002:
        //                $error['data'] = array('appnamemsg', '未知错误，请刷新重试！');
        //                $error['info'] = 'appnameerr';
        //                return $error;
        //                break;
        //            case 1003:
        //                $error['data'] = array('appnamemsg', '应用名称不能为空。');
        //                $error['info'] = 'appnameerr';
        //                return $error;
        //                break;
        //            case 1004:
        //                //                    $error['data'] = array('apptypeidmsg', '应用分类不能超过3个。');
        //                //                    $error['info'] = 'apptypeiderr';
        //                //                    return $error;
        //                break;
        //            case 1005:
        //                $error['data'] = '';
        //                $error['info'] = 'apptypeiderr';
        //                return $error;
        //                break;
        //            case 1006:
        //                $error['data'] = array('appdescriptionmsg', '应用介绍不能为空，最多不超过1000字。');
            //                $error['info'] = 'appdescriptionerr';
            //                return $error;
            //                break;
            //            case 1007:
            //                $error['data'] = array('appdescriptionmsg', '应用介绍不能为空，最多不超过1000字。');
            //                $error['info'] = 'appdescriptionerr';
            //                return $error;
            //                break;
            //        }
            //    }
            //    if(!isset($getData['status']) || $getData['status'] != 'success')
            //    {
            //        $error['data'] = array('appnamemsg', '未知错误，请刷新重试！');
            //        $error['info'] = 'appnameerr';
            //        return $error;
            //
        //    }
        $data['uptime'] = time();
        $data['isupdate'] = 1;
        $this->db->where('appid', $params['appid']);
        $this->db->where('userid', $userInfo['userid']);
        $this->db->update('app_info', $data);
        $upNum = $this->db->affected_rows();
        if($upNum <= 0) return array('status' => 0, 'info' => 'appnameerr', 'data' => array('appnamemsg', '未知错误。'));

        $result = $this->syncAppEditToChance($params, $userInfo);
        if($result['status'] != 1)
        {
            return $result;
        }

        return array('status' => 1, 'appid' => $params['appid']);
    }

    private function syncAppEditToChance($params, $userInfo)
    {
        $data = $this->getAppInfoRow($params['appid'], $userInfo['userid']);
        if(!$data) return array('status' => 0, 'info' => 'appnameerr', 'data' => array('appnamemsg', '获取应用信息失败。'));

        $postdata = array();
        $postdata['appname'] = $data['appname'];
        $postdata['appid'] = $data['chance_appid'];
        $postdata['packagename'] = $data['packagename'];
        $postdata['appurl'] = $data['appurl'];
        // $postdata['ostypeid'] = $data['ostypeid'];
        // $postdata['devicetypeid'] = $data['devicetypeid'];
        // $postdata['apptypeid'] = $data['apptypeid'];
        $postdata['appchildtypeid']= ',' . $data['appchildtypeid'];
        $postdata['appdescription']= $data['appdescription'];
        // $postdata['isintegral']= $data['isintegral'];
        // $postdata['integration']= $data['integration'];
        // $postdata['unit']= $data['unit'];
        
        $postdata = array('params' => $postdata, 'userinfo' => $this->config->item('dsp_api_userinfo'));
        //var_dump($postdata);
        $result = curlPost($this->config->item('dsp_api_server') . 'doeditapp', $postdata, 'POST', true);
        $result = json_decode($result, true);
        return $result;
    }

    /**
     * 编辑应用
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    public function cocodoEdit($params, $userInfo)
    {
        $error['status'] = 0;
        $params['appname'] = trim($params['appname']);
        $params['appdescription'] = trim($params['appdescription']);
        $chk = $this->chkEditInfo($params);
        if($chk !== true)
        {
            $err['status'] = 0;
            $err = array_merge($err, $chk);
            return $err;
        }
        $data['appname'] = $params['appname'];
        $data['packagename'] = $params['packagename'];
        $data['appchildtypeid']= ',' . $params['appchildtypeid'];
        $data['appdescription']= $params['appdescription'];
        $cocoAppInfo = $this->getAppInfoRow($params['appid'], $userInfo['userid']);
        $data['uptime'] = time();
        $data['isupdate'] = 1;
        $this->db->where('appid', $params['appid']);
        $this->db->where('userid', $userInfo['userid']);
        $this->db->update('app_info', $data);
        $upNum = $this->db->affected_rows();
        if($upNum <= 0) return array('status' => 0, 'info' => 'appnameerr', 'data' => array('appnamemsg', '未知错误。'));

        return array('status' => 1, 'appid' => $params['appid']);
    }
    
    /**
     * 开启feeds
     */
    public function doFeeds($params, $userInfo)
    {
    	if(isset($params['isfeeds']) && $params['isfeeds'] == 1)
        {
            $data['isfeeds'] = 1;
        }
        $data['uptime'] = time();
        $data['isupdate'] = 1;
        $this->db->where('appid', $params['appid']);
        $this->db->where('userid', $userInfo['userid']);
        $this->db->update('app_info', $data);
        $upNum = $this->db->affected_rows();
        if($upNum <= 0) return array('status' => 0, 'info' => 'openfeedserr', 'data' => array('openfeedsmsg', '未知错误。'));

        //添加默认feeds广告位
        $tmp['positionname'] = '默认广告位';
        $tmp['positiondesc'] = '默认广告位';
        $tmp['adform'] = 20;
        $tmp['appid']= $params['appid'];
        $tmp['userid'] = $userInfo['userid'];
        $tmp['createtime'] = time();
        $this->db->insert('app_position', $tmp);

        return array('status' => 1, 'appid' => $params['appid']);
    }

    /**
     * 开启积分墙
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    public function doIntegral($params, $userInfo)
    {
        $params['unit'] = trim($params['unit']);
        $chk = $this->chkIntegralInfo($params);
        if($chk !== true)
        {
            $err['status'] = 0;
            $err = array_merge($err, $chk);
            return $err;
        }
        if(isset($params['isintegral']) && $params['isintegral'] == 1)
        {
            $data['isintegral'] = 1;
            $data['integration'] = $params['integration'];
            $data['unit'] = $params['unit'];
        }
        $data['uptime'] = time();
        $data['isupdate'] = 1;
        $this->db->where('appid', $params['appid']);
        $this->db->where('userid', $userInfo['userid']);
        $this->db->update('app_info', $data);
        $upNum = $this->db->affected_rows();
        if($upNum <= 0) return array('status' => 0, 'info' => 'appnameerr', 'data' => array('appnamemsg', '未知错误。'));

        //添加默认积分墙广告位
        $tmp['positionname'] = '默认广告位';
        $tmp['positiondesc'] = '默认广告位';
        $tmp['adform'] = 4;
        $tmp['appid']= $params['appid'];
        $tmp['trusteeshiptype'] = 2;
        $tmp['userid'] = $userInfo['userid'];
        $tmp['createtime'] = time();
        $this->db->insert('app_position', $tmp);

        $result = $this->syncAppEditToChance($params, $userInfo);
        if($result['status'] != 1)
        {
            return $result;
        }

        return array('status' => 1, 'appid' => $params['appid']);
    }

    /**
     * 更新开关状态
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    public function doSwitch($params, $userInfo)
    {
        $where['appid'] = $params['appid'];
        $where['userid']= $userInfo['userid'];
        $data['uptime'] = time();
        $data['isupdate'] = 1;
        $data['switch'] = $params['switch'];
        $this->db->where($where);
        $this->db->update('app_info', $data);
        $upNum = $this->db->affected_rows();
        if($upNum <= 0) return array('status' => 0, 'info' => 'appnameerr', 'data' => array('appnamemsg', '未知错误。'));

        return array('status' => 1);
    }

    /**
     * 更新提审
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    public function doAutit($appid, $url, $auditype = 1, $userInfo)
    {
        if($auditype == 2 && !preg_match($this->urlmatch, $url))
        {
            return array('status' => 0, 'info' => 'appnameerr', 'data' => '请输入正确的地址 !');
        }
        $where['appid'] = $appid;
        $where['userid']= $userInfo['userid'];
        $data['audisubtime'] = time();
        $data['auditstatus'] = 0;
        $data['audiurl'] = $url;
        $data['auditype'] = $auditype;
        $this->db->where($where);
        $this->db->update('app_info', $data);
        $upNum = $this->db->affected_rows();
        if($upNum <= 0) return array('status' => 0, 'info' => 'appnameerr', 'data' => '未知错误。');

        return array('status' => 1, 'data'=>'');
    }

    /**
     * 提交审核
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    public function doStatus($appid, $auditid, $userInfo)
    {

        $where['appid'] = $appid;
        $where['userid']= $userInfo['userid'];
        $data['auditid']= $auditid;
        $data['auditstatus'] = 1;
        $this->db->where($where);
        $this->db->update('app_info', $data);
        $upNum = $this->db->affected_rows();
        if($upNum <= 0) return array('status' => 0, 'info' => 'subauditerr', 'data' => '未知错误。');
        return array('status' => 1, 'data'=>'');
    }

    /**
     * 更改兑换比例与单位
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    public function doEditIntegration($params, $userInfo)
    {
        $params['integration'] = trim($params['integration']);
        $params['unit'] = trim($params['unit']);
        $params['appid'] = trim($params['appid']);
        $chk = $this->chkEditIntegration($params);
        if($chk !== true)
        {
            $err['status'] = 0;
            $err = array_merge($err, $chk);
            return $err;
        }
        $data['integration'] = $params['integration'];
        $data['unit'] = $params['unit'];
        $data['uptime'] = time();
        $data['isupdate'] = 1;
        $this->db->where('appid', $params['appid']);
        $this->db->where('userid', $userInfo['userid']);
        $this->db->update('app_info', $data);
        $upNum = $this->db->affected_rows();
        if($upNum <= 0) return array('status' => 0, 'info' => 'eintegrationmsg', 'data' => array('eintegrationerr', '未知错误。'));

        $result = $this->syncAppEditToChance($params, $userInfo);
        if($result['status'] != 1)
        {
            return $result;
        }

        return array('status' => 1, 'appid' => $params['appid']);
    }

    /**
     * 检测广告组信息
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    private function chkAddInfo($params)
    {
        if(empty($params['ostypeid']))
        {
            $error['data'] = '';
            $error['info'] = 'ostypeiderr';
            return $error;
        }
        if($params['ostypeid'] == 2 && empty($params['devicetypeid']))
        {
            $error['data'] = '';
            $error['info'] = 'devicetypeiderr';
            return $error;
        }
        if(empty($params['appname']))
        {
            $error['data'] = array('appnamemsg', '应用名称不能为空。');
            $error['info'] = 'appnameerr';
            return $error;
        }
        if(mb_strlen($params['appname']) > 50)
        {
            $error['data'] = array('appnamemsg', '应用名称不能超过50个字符。');
            $error['info'] = 'appnameerr';
            return $error;
        }
        if(empty($params['packagename']))
        {
            $error['data'] = array('packagenamemsg', '应用包名称不能为空。');
            $error['info'] = 'packagenameerr';
            return $error;
        }
        if(!empty($params['appurl']) && !preg_match($this->urlmatch, $params['appurl']))
        {
            $error['data'] = '';
            $error['info'] = 'appurlerr';
            $error['status'] = 0;
            return $error;
        }
        if(isset($params['isintegral']) && $params['isintegral'] == 1)
        {
            if(empty($params['integration']))
            {
                $error['data'] = array('integrationmsg', '请填写兑换比例。');
                $error['info'] = 'integrationerr';
                return $error;
            }
            if(!is_numeric($params['integration']))
            {
                $error['data'] = array('integrationmsg', '兑换比例必须是数字。');
                $error['info'] = 'integrationerr';
                return $error;
            }
            if($params['integration'] <= 0)
            {
                $error['data'] = array('sintegrationmsg', '兑换比例必须大于0。');
                $error['info'] = 'sintegrationerr';
                return $error;
            }
            if(empty($params['unit']))
            {
                $error['data'] = array('integrationmsg', '请填写兑换单位。');
                $error['info'] = 'integrationerr';
                return $error;
            }
        }
        if($params['apptypeid'] == 0)
        {
            $error['data'] = '';
            $error['info'] = 'apptypeiderr';
            return $error;
        }
        $appchildtypeid = trim($params['appchildtypeid'], ',');
        if(empty($appchildtypeid))
        {
            $error['data'] = array('appchildtypeidmsg', '请选择应用子分类。');
            $error['info'] = 'appchildtypeiderr';
            return $error;
        }
        $appchildtypeida = explode(',', $appchildtypeid);
        if(count($appchildtypeida) > 3)
        {
            $error['data'] = array('appchildtypeidmsg', '分类不能超过3个。');
            $error['info'] = 'appchildtypeiderr';
            return $error;
        }
        if(empty($params['appdescription']) || mb_strlen($params['appdescription']) > 1000)
        {
            $error['data'] = array('appdescriptionmsg', '应用介绍不能为空，最多不超过1000字。');
            $error['info'] = 'appdescriptionerr';
            return $error;
        }
        return true;
    }

    /**
     * 检测广告组信息
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    private function chkEditInfo($params)
    {

        if(empty($params['appid']) )
        {
            $error['data'] = array('appnamemsg', '未知的appid！');
            $error['info'] = 'appnameerr';
            return $error;
        }
        if(empty($params['appname']))
        {
            $error['data'] = array('appnamemsg', '应用名称不能为空。');
            $error['info'] = 'appnameerr';
            return $error;
        }
        if(mb_strlen($params['appname']) > 50)
        {
            $error['data'] = array('appnamemsg', '应用名称不能超过50个字符。');
            $error['info'] = 'appnameerr';
            return $error;
        }
        if(empty($params['packagename']))
        {
            $error['data'] = array('packagenamemsg', '应用包名称不能为空。');
            $error['info'] = 'packagenameerr';
            return $error;
        }
        if(!empty($params['appurl']) && !preg_match($this->urlmatch, $params['appurl']))
        {
            $error['data'] = '';
            $error['info'] = 'appurlerr';
            $error['status'] = 0;
            return $error;
        }
        $appchildtypeid = trim($params['appchildtypeid'], ',');
        if(empty($appchildtypeid))
        {
            $error['data'] = array('appchildtypeidmsg', '请选择应用子分类。');
            $error['info'] = 'appchildtypeiderr';
            return $error;
        }
        $appchildtypeida = explode(',', $appchildtypeid);
        if(count($appchildtypeida) > 3)
        {
            $error['data'] = array('appchildtypeidmsg', '应用分类不能超过3个。');
            $error['info'] = 'appchildtypeiderr';
            return $error;
        }
        if(empty($params['appdescription']) || mb_strlen($params['appdescription']) > 1000)
        {
            $error['data'] = array('appdescriptionmsg', '应用介绍不能为空，最多不超过1000字。');
            $error['info'] = 'appdescriptionerr';
            return $error;
        }
        return true;
    }

    /**
     * 检测广告组信息
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    private function chkIntegralInfo($params)
    {

        if(empty($params['appid']) )
        {
            $error['data'] = array('appnamemsg', '未知的appid！');
            $error['info'] = 'appnameerr';
            return $error;
        }
        if(isset($params['isintegral']) && $params['isintegral'] == 1)
        {
            if(empty($params['integration']))
            {
                $error['data'] = array('sintegrationmsg', '请填写兑换比例。');
                $error['info'] = 'sintegrationerr';
                return $error;
            }
            if(!is_numeric($params['integration']))
            {
                $error['data'] = array('sintegrationmsg', '兑换比例必须是数字。');
                $error['info'] = 'sintegrationerr';
                return $error;
            }
            if($params['integration'] <= 0)
            {
                $error['data'] = array('sintegrationmsg', '兑换比例必须大于0。');
                $error['info'] = 'sintegrationerr';
                return $error;
            }
            if(empty($params['unit']))
            {
                $error['data'] = array('sintegrationmsg', '请填写兑换单位。');
                $error['info'] = 'sintegrationerr';
                return $error;
            }
        }
        return true;
    }

    /**
     * 检测广告组信息
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    private function chkEditIntegration($params)
    {

        if(empty($params['appid']) )
        {
            $error['data'] = array('eintegrationmsg', '未知的appid！');
            $error['info'] = 'eintegrationerr';
            return $error;
        }
        if(empty($params['integration']))
        {
            $error['data'] = array('eintegrationmsg', '请填写兑换比例。');
            $error['info'] = 'eintegrationerr';
            return $error;
        }
        if(!is_numeric($params['integration']))
        {
            $error['data'] = array('eintegrationmsg', '兑换比例必须是数字。');
            $error['info'] = 'eintegrationerr';
            return $error;
        }
        if($params['integration'] <= 0)
        {
            $error['data'] = array('sintegrationmsg', '兑换比例必须大于0。');
            $error['info'] = 'sintegrationerr';
            return $error;
        }
        if(empty($params['unit']))
        {
            $error['data'] = array('eintegrationmsg', '请填写兑换单位。');
            $error['info'] = 'eintegrationerr';
            return $error;
        }
        return true;
    }

    /**
     * 获取app类别
     */
    public function getAppType()
    {
        $typeList = $this->getAppTypeList();
        if($typeList == false)
        {
            return false;
        }
        $data = array();
        foreach ($typeList as $v)
        {
            if($v['parentid'] == 0)
            {
                $data[$v['apptypeid']] = $v;
            }
        }
        foreach ($data as $k => $v)
        {
            foreach ($typeList as $pv)
            {
                if($pv['parentid'] == $k)
                {
                    $data[$k]['childtype'][] = $pv;
                }
            }
        }
        return $data;
    }

    /**
     * 获取app父类别
     */
    public function getAppTypeList($appTypeId = '', $parentId = '')
    {
        $where = array();
        if(!empty($appTypeId))
        {
            $where['apptypeid'] = $appTypeId;
        }
        if(!empty($parentId))
        {
            $where['parentid'] = $parentId;
        }
        $this->db->order_by('ordernum desc,apptypeid asc');
        $query = $this->db->get_where('app_type', $where);
        if ($query->num_rows() > 0)
        {
            $data = $query->result_array();
        }
        if(empty($data))
        {
            return false;
        }
        return $data;
    }

    /**
     * 获取应用列表
     */
    public function getAppList($params, $userInfo)
    {
        $data = $this->getAppInfoList($params,$userInfo);
        if(!$data) return false;
        $dataType = $this->getAppTypeList('', 0);
        $dataTypeTmp = array();
        foreach ($dataType as $value) {
            $dataTypeTmp[$value['apptypeid']] = $value['typename'];
        }
        foreach ($data as $k => $v)
        {
            if(isset($dataTypeTmp[$v['apptypeid']]))
            {
                $data[$k]['typename'] = $dataTypeTmp[$v['apptypeid']];
            }
            $punchbox = $this->getAppChannelRow($v['appid']);
            $data[$k]['publisherID'] = isset($punchbox['publisherID']) ? $punchbox['publisherID'] : '';
        }
        return $data;
    }

    /**
     * 获取应用列表
     */
    public function getCocoAppList($ids, $userid)
    {
        $this->db->where_in('appid', $ids);
        $this->db->where('userid', $userid);
        $this->db->select('appid');
        $query = $this->db->get('app_info');
        if ($query->num_rows() > 0)
        {
            $data = $query->result_array();
        }
        if(empty($data))
        {
            return false;
        }
        return $data;
    }

    /**
     * 获取应用详细信息
     */
    public function getDetailInfo($userInfo, $appId)
    {
        $data = $this->getAppInfoRow($appId, $userInfo['userid']);
        if(!$data) return false;
        $dataType = $this->getAppTypeList();
        foreach ($dataType as $value) {
            if($value['apptypeid'] == $data['apptypeid'])
            {
                $data['typename'] = $value['typename'];
                continue;
            }
            if(strrpos($data['appchildtypeid'], ','.$value['apptypeid'].',') !== false)
            {
                $data['childtypename'][] = $value['typename'];
            }
        }
        return $data;
    }
    /**
     * 获取App基本信息
     * Enter description here ...
     * @param $appId
     * @param $userId
     */
    public function getAppInfoRow($appId, $userId)
    {
        $query = $this->db->get_where('app_info', array('appid' => $appId, 'userid' => $userId));
        if ($query->num_rows() > 0)
        {
            $data = $query->row_array();
        }
        if(empty($data))
        {
            return false;
        }
        return $data;
    }

    /**
     * 获取App基本信息列表
     * Enter description here ...
     * @param unknown_type $userId
     */

    public function getAppInfoList($params, $userInfo)
    {
        if(empty($userInfo))
        {
            return ;
        }

        $this->db->where('userid', $userInfo['userid']);
        if(isset($params['ostypeid']) && $params['ostypeid'] > 0)
        {
            $this->db->where('ostypeid', $params['ostypeid']);
        }
        if(isset($params['devicetypeid']) && $params['devicetypeid'] != 9)
        {
            $this->db->where('devicetypeid', $params['devicetypeid']);
        }
        if(isset($params['auditstatus']) && $params['auditstatus'] != 9)
        {
            $this->db->where('auditstatus', $params['auditstatus']);
        }
        if(isset($params['keys']) && !empty($params['keys']))
        {
            $this->db->like('appname', $params['keys']);
        }
        $this->db->order_by('createtime', "desc");
        $query = $this->db->get('app_info');
        if ($query->num_rows() > 0)
        {
            $data = $query->result_array();
        }
        if(empty($data))
        {
            return false;
        }
        return $data;
    }

    public function getAppChannelRow($appId, $publisherID = '')
    {
        $where['appid'] = $appId;
        if(!empty($publisherID)) $where['publisherID'] = $publisherID;
        $query = $this->db->get_where('app_channel', $where);
        if ($query->num_rows() > 0)
        {
            $data = $query->row_array();
        }
        if(empty($data))
        {
            return false;
        }
        return $data;
    }

    /**
     * 上传应用
     * @return unknown_type
     */
    public function upLoad($type)
    {
        $this->load->library('MyUpload',$_FILES['uploadata']);
        $upload = new MyUpload($_FILES['uploadata']);
        $upload->setUploadPath(UPLOAD_APP_PATH);
        if($type == 1)
        {
            $fileExt = 'apk';
        }
        else
        {
            $fileExt = 'ipa';
        }
        $upload->setFileExt(array($fileExt));
        $upload->setMaxsize(50*1024*1024);

        $strError = '';

        if( !$upload->isAllowedTypes() )
        {
            $strError = '请上传后缀为'.$fileExt.'的文件 !';
        }
        elseif( $upload->isBigerThanMaxSize() )
        {
            $strError = '此处的文件最大不能超过 '.intval($upload->getMaxsize()/1024) .'KB';
        }

        if(empty($strError) and $upload->upload())
        {
            $imgPath = $upload->getUplodedFilePath();
            $data['data'] = $imgPath;
            $data['status'] = 1;
        }
        else
        {
            $data['data'] = $strError;
            $data['status'] = 0;
        }
        return $data;
    }
    
     /**
     * 批量添加App。
     * @author wf
     */
    
    function doBatchAdd($data,$userinfo){
    	$result=array();
    	if(isset($data) && count($data)>1){
    		for ($i=1;$i<count($data);$i++){
    			$params=array();
    			$params['appname']=$data[$i][0]?$data[$i][0]:"";
    			$params['packagename']=$data[$i][1]?$data[$i][1]:"";
    			$params['ostypeid']=$data[$i][2]?$data[$i][2]:"";
    			$params['devicetypeid']=$data[$i][3]?$data[$i][3]:"";
    			$params['apptypeid']=$data[$i][4]?$data[$i][4]:"";
    			$params['appchildtypeid']=str_replace('|', ',', $data[$i][5]?$data[$i][5]:"");
    			if(substr($params['appchildtypeid'], -1,1)!=','){
    				$params['appchildtypeid']= $params['appchildtypeid'].',';
    			}
    			$params['appurl']=$data[$i][6]?$data[$i][6]:"";
    			$params['appdescription']=$data[$i][7]?$data[$i][7]:"";
    			$params['isintegral']=$data[$i][8]?$data[$i][8]:"";
    			$params['integration']=$data[$i][9]?$data[$i][9]:"";
    			$params['unit']=$data[$i][10]?$data[$i][10]:"";
    			$insertRes=$this->doAdd($params, $userinfo);
    			array_push($result,$insertRes);
    		}
    	}
    	else{
    		 $res=array();
    		 $res['status']=0;
    		 $res['data'] = array("");
             $res['info'] = '您上传的文件中没有数据';
             array_push($result,$res);
    	}
    	return $result;
    }
}
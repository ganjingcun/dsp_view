<?php /**
* @filename	: apptypeclass.php
* @encoding	: UTF-8
* @author		: niejianhui
* @datetime	: 2012-12-17 17:19:24
* @Description  : 应用类型model
*/
class AppPositionClass extends MY_Model{

    /**
     * 初始化
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * 新建应用广告位
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    public function doAdd($params, $userInfo)
    {
        $params['positionname'] = trim($params['positionname']);
        $chk = $this->chkAddInfo($params, $userInfo);
        if($chk !== true)
        {
            $err['status'] = 0;
            $err = array_merge($err, $chk);
            return $err;
        }
        $data['positionid'] = $params['positionid'];
        $data['positionname'] = $params['positionname'];
        $data['positiondesc'] = $params['positiondesc'];
        $data['adform'] = $params['adform'];
        if($data['adform'] == 4)
        {
            //            $data['integration'] = $params['integration'];
            //            $data['unit'] = $params['unit'];
            $data['trusteeshiptype'] = $params['trusteeshiptype'];
            //            $data['rewardesc']= $params['rewardesc'];
        }
        elseif($data['adform'] == 2)
        {
            $data['adstyle'] = $params['adstyle'];
            if($params['adstyle'] == 1)
            {
                $data['adstylesize'] = $params['adstylesize'];
                $data['adstyleimgsize'] = $params['adstyleimgsize'];
                $data['adstylecolor'] = $params['adstylecolor'];
                $data['bordertype'] = $params['bordertype'];
                $bordergroupid=0;
                if($params['bordertype'] == 1)
                {
                	$data['borderpolicy'] = $params['borderpolicy'];
                	$bordergroupid = $params['sysBorderGroupId'];
                }
                elseif($params['bordertype'] == 2)
                {
	                $sql = "select max(bordergroupid) as maxgroupid from ad_border";
			    	$result = $this->db->query($sql);
			    	$re=array();
			        $re = $result->result_array();
			        $maxgroupid = is_null($re[0]['maxgroupid'])?0:$re[0]['maxgroupid'];
			        $bordergroupid = $maxgroupid + 1;
       				$createtime = time();
					  	
					$border[] = array('bordergroupid' => $bordergroupid, 'sizeid' => 12, 'type' => 2, 'path' => $params['img_border1'], 'createtime' => $createtime,'isicon'=>0);
					$border[] = array('bordergroupid' => $bordergroupid, 'sizeid' => 13, 'type' => 2, 'path' => $params['img_border2'], 'createtime' => $createtime,'isicon'=>0);
				    $border[] = array('bordergroupid' => $bordergroupid, 'sizeid' => 8, 'type' => 2, 'path' => $params['img_border3'], 'createtime' => $createtime,'isicon'=>0);
				    $border[] = array('bordergroupid' => $bordergroupid, 'sizeid' => 9, 'type' => 2,  'path' => $params['img_border4'], 'createtime' => $createtime,'isicon'=>0);
				    $border[] = array('bordergroupid' => $bordergroupid, 'sizeid' => 10, 'type' => 2,  'path' => $params['img_border5'], 'createtime' => $createtime,'isicon'=>0);
				    $border[] = array('bordergroupid' => $bordergroupid, 'sizeid' => 11, 'type' => 2,  'path' => $params['img_border6'], 'createtime' => $createtime,'isicon'=>0);
				    if(!empty($params['img_border7'])&&$params['img_border7']!=""){
				    	$border[] = array('bordergroupid' => $bordergroupid, 'sizeid' => null,'type' => 2,  'path' => $params['img_border7'], 'createtime' => $createtime, 'isicon'=>1);
				    }
				    $this->db->insert_batch('ad_border' ,$border);
                }
        		$data['bordergroupid'] = $bordergroupid;
            }
        }
        $data['appid']= $params['appid'];
        //$data['publisherID']= $params['publisherID'];
        $data['userid'] = $userInfo['userid'];
        $data['createtime'] = time();
        $this->db->insert('app_position', $data);
        $id = $this->db->insert_id();
        if($id <= 0) return array('status' => 0, 'info' => 'positionnameerr', 'data' => array('positionnamemsg', '未知错误。'));
        return array('status' => 1, 'id' => $id);
    }

    /**
     * 编辑应用广告位
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    public function doEdit($params, $userInfo)
    {
        $params['positionname'] = trim($params['positionname']);
        $chk = $this->chkEditInfo($params, $userInfo);
        if($chk !== true)
        {
            $err['status'] = 0;
            $err = array_merge($err, $chk);
            return $err;
        }
        $positionInfo = $this->getPositionInfo(array('positionid' => $params['positionid']));
        if($positionInfo['adform'] == 2)
        {
            $data['adstyle'] = $params['adstyle'];
            if($params['adstyle'] == 1)//广告形态 1 自定义
            {
                $data['adstylesize'] = $params['adstylesize'];
                $data['adstyleimgsize'] = $params['adstyleimgsize'];
                $data['adstylecolor'] = $params['adstylecolor'];
            	$data['bordertype'] = $params['bordertype'];
                if($params['bordertype'] == 0)//边框设置 0 无边框， 
                {
                	$data['borderpolicy'] = 0;
                	$data['bordergroupid'] = 0;
                	if($params['oldBorderType'] == 2){
                		$this->load->model('manage/AdBorderClass');
                		$this->AdBorderClass->delBorderGroup($params['oldBorderGroupId']);
                	}
                }
                elseif($params['bordertype'] == 1)//边框设置 1 系统边框，
                {
                	$data['borderpolicy'] = $params['borderpolicy'];
                	$data['bordergroupid'] = $params['sysBorderGroupId'];
               		if($params['oldBorderType'] == 2){
                		$this->load->model('manage/AdBorderClass');
                		$this->AdBorderClass->delBorderGroup($params['oldBorderGroupId']);
                	}
                }
                elseif($params['bordertype'] == 2)//边框设置 2 自定义边框
                {
                	if($params['oldBorderType'] != 2)
                	{
                		$sql = "select max(bordergroupid) as maxgroupid from ad_border";//旧边框设置不是2 自定义边框，
				    	$result = $this->db->query($sql);
				    	$re=array();
				        $re = $result->result_array();
				        $maxgroupid = is_null($re[0]['maxgroupid'])?0:$re[0]['maxgroupid'];
				        $bordergroupid = $maxgroupid + 1;
					  	$data['bordergroupid'] = $bordergroupid;
					  	$data['borderpolicy'] = 0;
					  	$createtime = time();
					  	$border[] = array('bordergroupid' => $bordergroupid, 'sizeid' => 12, 'type' => 2, 'path' => $params['img_border1'], 'createtime'=>$createtime, 'lastupdatetime' => $createtime,'isicon'=>'0');
					  	$border[] = array('bordergroupid' => $bordergroupid, 'sizeid' => 13, 'type' => 2, 'path' => $params['img_border2'], 'createtime'=>$createtime, 'lastupdatetime' => $createtime,'isicon'=>'0');
				        $border[] = array('bordergroupid' => $bordergroupid, 'sizeid' => 8, 'type' => 2, 'path' => $params['img_border3'], 'createtime'=>$createtime, 'lastupdatetime' => $createtime,'isicon'=>'0');
				        $border[] = array('bordergroupid' => $bordergroupid, 'sizeid' => 9, 'type' => 2,  'path' => $params['img_border4'], 'createtime'=>$createtime, 'lastupdatetime' => $createtime,'isicon'=>'0');
				        $border[] = array('bordergroupid' => $bordergroupid, 'sizeid' => 10, 'type' => 2,  'path' => $params['img_border5'], 'createtime'=>$createtime, 'lastupdatetime' => $createtime,'isicon'=>'0');
				        $border[] = array('bordergroupid' => $bordergroupid, 'sizeid' => 11, 'type' => 2, 'path' => $params['img_border6'], 'createtime'=>$createtime, 'lastupdatetime' => $createtime,'isicon'=>'0');
				        if(!empty($params['img_border7']) && $params['img_border7']!=""){
				        	$border[] = array('bordergroupid' => $bordergroupid, 'sizeid' => null,'type' => 2, 'path' => $params['img_border7'], 'createtime'=>$createtime,'lastupdatetime' => $createtime, 'isicon'=>'1');
				        }
				        $this->db->insert_batch('ad_border' ,$border);
				        $upNum = $this->db->affected_rows();
	        			if($upNum <= 0) 
	        				return array('status' => 0, 'info' => 'positionnameerr', 'data' => array('positionnamemsg', '未知错误。'));
                	}else{
                		$data['borderpolicy'] = 0;//旧边框设置是2 自定义边框，
	                	$borderUpdate = json_decode($params['updateBorderJson'], TRUE);
				        foreach($borderUpdate as $key=>$val)
				    	{
				    		$updateData = explode("*", $val);
				    		$where['borderid'] = $updateData[0];
				    		$updatePath['path'] = $updateData[1];
				    		$updatePath['lastupdatetime'] = time();
				    		if($updateData[0]!='undefined'){
								$this->db->where($where);
							    $this->db->update('ad_border' ,$updatePath);
							    $upNum = $this->db->affected_rows();
	        					if($upNum <= 0) 
	        						return array('status' => 0, 'info' => 'positionnameerr', 'data' => array('positionnamemsg', '未知错误。'));
				    		}
				    		else{
				    			if(!empty($params['img_border7']) && $params['img_border7']!=""){
					    			/*如果原来没有上传关闭按钮，现在又重新上传了，需要保存这条数据。
					    			 * 先查看数据库中是否有该groupid对应的可用关闭按钮的数据，如果有，则在原来基础尚更新，没有，则插入这条数据。
					    			 */
									$exsitwhere=array('bordergroupid' => $positionInfo['bordergroupid'],'type' => 2,'isicon'=>'1','status' => '1');
					    			$this->load->model('manage/AdBorderClass');
					    			$exsitdata=$this->AdBorderClass->getBorderList($exsitwhere);
					    			$time=time();
					    			if($exsitdata!=null && $exsitdata!=''){
					    				$this->db->where(array('bordergroupid' => $positionInfo['bordergroupid'],'type' => 2,'isicon'=>'1','status' => '1'));
								    	$this->db->update('ad_border',array('path' => $params['img_border7'],'lastupdatetime'=>$time));
								    	$exsitupNum = $this->db->affected_rows();
		        						if($exsitupNum <= 0) return array('status' => 0, 'info' => 'positionnameerr', 'data' => array('positionnamemsg', '未知错误。'));
					    			}
					    			else{
						        		$border= array('bordergroupid' => $positionInfo['bordergroupid'], 'sizeid' => null,'type' => 2, 'path' => $params['img_border7'], 'createtime'=>$time, 'lastupdatetime' => $time,'isicon'=>'1');
						        		$this->db->insert('ad_border' ,$border);
					    			}
					    		}
					    		elseif ($params['img_border7']==""){
					    			/*如果存在undefined,但img_border7的值为‘’，则目的是删除该关闭按钮。*/
					    			$lastupdatetime = time();
					    			$this->db->where(array('bordergroupid' => $positionInfo['bordergroupid'],'type' => 2,'isicon'=>'1'));
							    	$this->db->update('ad_border',array('status' => '2','lastupdatetime'=>$lastupdatetime));
							    	$upNum = $this->db->affected_rows();
	        						if($upNum <= 0) return array('status' => 0, 'info' => 'positionnameerr', 'data' => array('positionnamemsg', '未知错误。'));
					    		}
					    		else{
					    			return  array('status' => 0, 'info' => 'positionnameerr', 'data' => array('positionnamemsg', '未知错误。'));
					    		}
				    		}
				    	}
                	}
                	
                }
            }
            else 
            {
                $data['adstylesize'] = 90;//广告形态 0 全屏 
                $data['adstyleimgsize'] = 1;
                $data['adstylecolor'] = '';            
            }
        
        }
        $data['positionid'] = $params['positionid'];
        $data['positionname'] = $params['positionname'];
        $data['positiondesc'] = $params['positiondesc'];
        //目前只支持自定义积分。
        $data['trusteeshiptype'] = 2;
        $data['appid']= $params['appid'];
        $data['userid'] = $userInfo['userid'];
        $data['edittime'] = time();
        $this->db->where(array('positionid' => $params['positionid']));//, 'userid' => $userInfo['userid']
        $this->db->update('app_position', $data);
        $upNum = $this->db->affected_rows();
        if($upNum <= 0) return array('status' => 0, 'info' => 'positionnameerr', 'data' => array('positionnamemsg', '未知错误。'));
        return array('status' => 1);
    }

    /**
     * 编辑应用广告位
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    public function doDel($params, $userInfo)
    {
    	$this->load->model('manage/AdBorderClass');
        if($this->getPositionInfo(array('positionid'=>$params['positionid'], 'userid'=>$userInfo['userid'])) == false)
        {
            $error['data'] = array('positionidmsg', '广告位ID不存在，请刷新页面重试！');
            $error['info'] = 'postioniderr';
            return $error;
        }

        $data['positionid'] = $params['positionid'];
        $data['appid']= $params['appid'];
        $data['publisherID']= $params['publisherID'];
        $data['userid'] = $userInfo['userid'];
        $this->db->where($data);
        $this->db->update('app_position', array('status' => 1));
        $this->AdBorderClass->delBorderGroup($params['bordergroupid']);
        return array('status' => 1);
    }

    /**
     * 检测广告组信息
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    private function chkAddInfo($params, $userInfo)
    {
        if(empty($params['positionid']) || empty($params['appid']))
        {
            $error['data'] = array('positionnamemsg', '未知错误！');
            $error['info'] = 'positionnameerr';
            return $error;
        }
        if(empty($params['positionname']))
        {
            $error['data'] = array('positionnamemsg', '广告位名称不能为空。');
            $error['info'] = 'positionnameerr';
            return $error;
        }
        if(mb_strlen($params['positionname']) > 50)
        {
            $error['data'] = array('positionnamemsg', '广告位名称不能超过50个字符。');
            $error['info'] = 'positionnameerr';
            return $error;
        }

        if(empty($params['adform']))
        {
            $error['data'] = '';
            $error['info'] = 'adformerr';
            return $error;
        }
        if($params['adform'] == 4)
        {
            //            if(empty($params['integration']))
            //            {
            //                $error['data'] = array('integrationmsg', '请填写兑换比例。');
            //                $error['info'] = 'integrationerr';
            //                return $error;
            //            }
            //            if(empty($params['unit']))
            //            {
            //                $error['data'] = array('integrationmsg', '请填写兑换单位。');
            //                $error['info'] = 'integrationerr';
            //                return $error;
            //            }
            if(empty($params['trusteeshiptype']))
            {
                $error['data'] = '';
                $error['info'] = 'trusteeshiptypeerr';
                return $error;
            }
        }
        if($params['adform']==2 && $params['adstyle']==1){
	        if($params['bordertype'] == 1)
	        {
	        	if($params['borderpolicy'] == 0)
	        	{
	        		$error['data'] = array('sysBordermsg', '请选择边框设置。');
			        $error['info'] = 'sysBordererr';
			        return $error;
	        	}
	        	elseif ($params['borderpolicy'] == 1 && $params['sysBorderGroupId'] == 0)
	        	{
	        		$error['data'] = array('sysBordermsg', '请选择系统边框。');
			        $error['info'] = 'sysBordererr';
			        return $error;
	        	}
	        }
	        if($params['bordertype'] == 2)
	        {
	        	for($i=1; $i<=6; $i++){
	        		if(empty($params['img_border'.$i]))
	        		{
	        			$error['data'] = array('bordermsg'.$i, '请上传边框。');
			            $error['info'] = 'bordererr'.$i;
			            return $error;
	        		}
	        	}
	        }
        }
        $positionInfo = $this->getPositionInfo(array('positionname' => $params['positionname'], 'userid' => $userInfo['userid'], 'appid' => $params['appid']));
        if($positionInfo !== false)
        {
            $error['data'] = array('positionnamemsg', '广告位名称已存在。');
            $error['info'] = 'positionnameerr';
            return $error;
        }
        $positionInfo = $this->getPositionInfo(array('positionid' => $params['positionid'], 'userid' => $userInfo['userid'], 'appid' => $params['appid']));
        if($positionInfo !== false)
        {
            $error['data'] = array('positionidmsg', '广告位名称已存在。');
            $error['info'] = 'positioniderr';
            return $error;
        }
        return true;
    }

    /**
     * 检测广告组信息
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    private function chkEditInfo($params, $userInfo)
    {
    	if(empty($params['positionid']) || empty($params['appid']))
        {
        	$error['data'] = array('positionnamemsg', '未知错误！');
            $error['info'] = 'positionnameerr';
            return $error;
        }
        if(empty($params['positionname']))
        {
            $error['data'] = array('positionnamemsg', '广告位名称不能为空。');
            $error['info'] = 'positionnameerr';
            return $error;
        }
        if(mb_strlen($params['positionname']) > 50)
        {
            $error['data'] = array('positionnamemsg', '广告位名称不能超过50个字符。');
            $error['info'] = 'positionnameerr';
            return $error;
        }
    	if($params['bordertype'] == 1)
        {
        	if($params['borderpolicy'] == 0)
        	{
        		$error['data'] = array('sysBordermsg', '请选择边框设置。');
		        $error['info'] = 'sysBordererr';
		        return $error;
        	}
        	elseif ($params['borderpolicy'] == 1 && $params['sysBorderGroupId'] == 0)
        	{
        		$error['data'] = array('sysBordermsg', '请选择系统边框。');
		        $error['info'] = 'sysBordererr';
		        return $error;
        	}
        }
        if($params['bordertype'] == 2)
        {
        	for($i=1; $i<=6; $i++){
        		if(empty($params['img_border'.$i]))
        		{
        			$error['data'] = array('bordermsg'.$i, '请上传边框。');
		            $error['info'] = 'bordererr'.$i;
		            return $error;
        		}
        	}
        }
        
        $where['positionname'] = $params['positionname'];
        $where['userid'] = $userInfo['userid'];
        $where['positionid != '] = $params['positionid'];
        $where['appid'] = $params['appid'];
        $positionInfo = $this->getPositionInfo($where);
        if($positionInfo !== false)
        {
            $error['data'] = array('positionnamemsg', '广告位名称已存在。');
            $error['info'] = 'positionnameerr';
            return $error;
        }
        return true;
    }

    /**
     * 获取广告位列表
     */
    public function getList($where)
    {
        $this->db->order_by('createtime', "desc");
        $query = $this->db->get_where('app_position', $where);
        if ($query->num_rows() > 0)
        {
            $data = $query->result_array();
        }
        if(empty($data))
        {
            return false;
        }
        return $data;
    }

    /**
     * 获取广告位基本信息
     * Enter description here ...
     * @param $appId
     * @param $userId
     */
    public function getPositionInfo($where)
    {
        $query = $this->db->get_where('app_position', $where);
        if ($query->num_rows() > 0)
        {
            $data = $query->row_array();
        }
        if(empty($data))
        {
            return false;
        }
        return $data;
    }


    /**
     * 搜索渠道列表
     */
    public function getSearchList($keys, $appId, $adform, $userId)
    {
        if(empty($userId) || $userId <=0 || empty($keys) || empty($adform))
        {
            return false;
        }
        $where['userid'] = $userId;
        $where['appid'] = $appId;
        $where['adform'] = $adform;
        $this->db->like('positionname', $keys);
        $this->db->order_by('createtime desc');
        $this->db->limit(10);
        $query = $this->db->get_where('app_position', $where);
        if ($query->num_rows() > 0)
        {
            $data = $query->result_array();
        }
        if(empty($data))
        {
            return false;
        }
        return $data;
    }

    /**
     * 生成广告位id
     */
    public function createPositionId($appid)
    {
        //时间戳转换成36进制
        $hexadecimal = time();
        $tmp = base_convert($hexadecimal, 10, 36);
        $positionId = $appid.$tmp;

        return $positionId;
    }
    
	/**
     * 上传边框
     */
	public function upload($type='1280*720') {
		$this->load->library('MyUpload', $_FILES['Filedata']);
		$upload = new MyUpload($_FILES['Filedata']);
		$upload->setUploadPath(UPLOAD_DIR.'/stuff/border/img/'.date('Ym'));
		$upload->setFileExt(array('jpg', 'png'));

		$upload->setMaxsize(120 * 1024);

		$strError = '';
		$data = array('isReceived'=>false,'imgInfo'=>array(),'errorMessage'=>array());
		if (!$upload->isAllowedTypes()) {
			$strError = '您上传的文件格式错误 !';
		} elseif ($upload->isBigerThanMaxSize()) {
			$strError = '此处的文件最大不能超过 ' . intval($upload->getMaxsize() / 1024) . 'KB';
		}
		
        $imgSize = explode("*", $type);
        $width = $imgSize[0];
        $height = $imgSize[1];
        
		if(!$upload->isAllowedSize($width, $height)){
			$strError .= " 图片的尺寸必须为：".$width." X ".$height;
		}

		if (empty($strError) and $upload->upload()) {
			$imgPath = $upload->getUplodedFilePath();
			$data['imgInfo']['newImg'] = $imgPath;
			$data['newImg'] = $imgPath;
			$data['isReceived'] = 1;
		} else {
			$data['errorMessage'] = $strError;
			$data['isReceived'] = 0;
		}
		return $data;
	}
}
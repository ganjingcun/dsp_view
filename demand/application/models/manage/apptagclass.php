<?php /**
* @filename	: apptypeclass.php
* @encoding	: UTF-8
* @author		: niejianhui
* @datetime	: 2012-12-17 17:19:24
* @Description  : 应用类型model
*/
class AppTagClass extends MY_Model{

    /**
     * 初始化
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * 新建标签
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    public function doAdd($params, $userInfo)
    {
        $params['tagname'] = trim($params['tagname']);
        $chk = $this->chkAddInfo($params, $userInfo);
        if($chk !== true)
        {
            $err['status'] = 0;
            $err = array_merge($err, $chk);
            return $err;
        }
        $data['tagname'] = $params['tagname'];
        $data['status'] = 0;
        $data['userid'] = $userInfo['userid'];
        $data['createtime'] = time();
        $this->db->insert('app_tag', $data);
        $tagid = $this->db->insert_id();
        if($tagid <= 0) return array('status' => 0, 'info' => 'tagnameerr', 'data' => array('tagnamemsg', '未知错误。'));
        return array('status' => 1, 'id' => $tagid);
    }

    /**
     * 检测标签信息
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    private function chkAddInfo($params, $userInfo)
    {
        if(empty($params['tagname']))
        {
            $error['data'] = array('tagnamemsg', '标签名称不能为空。');
            $error['info'] = 'tagnameerr';
            return $error;
        }
        if(mb_strlen($params['tagname']) > 50)
        {
            $error['data'] = array('tagnamemsg', '标签名称不能超过50个字符。');
            $error['info'] = 'tagnameerr';
            return $error;
        }
        return true;
    }

    /**
     * 获取标签列表
     */
    public function getList()
    {
        $where['status'] = 1;
        $this->db->order_by('createtime', "desc");
        $query = $this->db->get_where('app_tag', $where);
        if ($query->num_rows() > 0)
        {
            $data = $query->result_array();
        }
        if(empty($data))
        {
            return false;
        }
        return $data;
    }
}
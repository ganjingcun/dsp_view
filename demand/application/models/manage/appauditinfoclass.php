<?php /**
* @filename	: apptypeclass.php
* @encoding	: UTF-8
* @author		: niejianhui
* @datetime	: 2012-12-17 17:19:24
* @Description  : 应用类型model
*/
class AppAuditInfoClass extends MY_Model{

    /**
     * 初始化
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * 新建标签
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    public function doAdd($params, $appid)
    {
        $data['auditinfo'] = json_encode($params);
        $data['appid'] = $appid;
        $data['createtime'] = time();
        $this->db->insert('app_auditinfo', $data);
        $auditid = $this->db->insert_id();
        if($auditid <= 0) return 0;
        return $auditid;
    }

    /**
     * 获取标签列表
     */
    public function getList()
    {
        $where['status'] = 1;
        $this->db->order_by('createtime', "desc");
        $query = $this->db->get_where('app_tag', $where);
        if ($query->num_rows() > 0)
        {
            $data = $query->result_array();
        }
        if(empty($data))
        {
            return false;
        }
        return $data;
    }
}
<?php /**
* @filename	: adgroupclass.php
* @encoding	: UTF-8
* @author		: funbox
* @datetime	: 2016-10-31 17:19:24
* @Description  : 广告model
*/

class AdStuffImgSizeClass extends MY_Model{
    /**
     * 初始化
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('tools');
		$this->load->helper('db_exchange');
    }

	public function add($data) {

		$this->db->insert('ad_stuff_img_size', $data);
		$id = $this->db->insert_id();
		return $id;
	}


}
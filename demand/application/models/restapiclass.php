<?php
/**
 * Created by PhpStorm.
 * User: chukong
 * Date: 3/25/16
 * Time: 10:47 AM
 */
class RestapiClass extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getAppInfoList($userid, $ostypeid=null)
    {
        $sql = "select a.appid, b.appname, a.publisherID, a.channelname
                from app_channel as a left join app_info as b on a.appid=b.appid
                where b.userid=$userid";
        if (isset($ostypeid))
        {
            $sql .= " and b.ostypeid=$ostypeid";
        }
        $data = $this->db->query($sql)->result_array();

        return $data;
    }
}
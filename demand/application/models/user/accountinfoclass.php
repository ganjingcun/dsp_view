<?php
/**
 * 用户信息操作
 *
 * @brief M层
 * @author 王栋
 * @date 2013.10.15
 * @note 详细说明及修改日志
 */
class AccountInfoClass extends MY_Model{

	/**
	 * 初始化
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library('Security');
		$this->load->database();
	}

	/**
	 * 注册用户接口
	 *
	 * @author wangdong
	 * @date 2013.10.15
	 * @note 详细说明及修改日志
	 */
	public function doAdd($params, $userInfo)
	{
		$error = array('status' => false);
		$error = $this->chkAddInfo($params);
		if($error['status'] == false)
		{
			return $error;
		}
		$data['infosource'] = $params['infosource'];
		$data['sourcename'] = $params['sourcename'];
		$data['accounttype'] = $params['accounttype'];
		$data['certtype'] = isset($params['certtype'])?$params['certtype']:null;
		$data['realname'] = trim($params['realname']);
		$data['idnumber'] = trim($params['idnumber']);
		$data['companyname'] = trim($params['companyname']);
		$data['companylicense'] = trim($params['companylicense']);
		$data['companyid'] = trim($params['companyid']);
		$data['individualname'] = trim($params['individualname']);
		$data['individualid'] = trim($params['individualid']);
		$data['ownername'] = trim($params['ownername']);
		$data['ownercerttype'] = isset($params['ownercerttype'])?$params['ownercerttype']:null;
		$data['ownerid'] = trim($params['ownerid']);
		$data['bankusername'] = trim($params['bankusername']);
		$data['province'] = $params['province'];
		$data['city'] = $params['city'];
		$data['currency'] = $params['currency'];
		$data['bank'] = $params['bank'];
		$data['bankaddress'] = trim($params['bankaddress']);
		$data['bankaccount'] = trim($params['bankaccount']);
		//$data['rebankaccount'] = $params['rebankaccount'];
		$data['contact'] = $params['contact'];
		$data['mobile'] = trim($params['mobile']);
		$data['address'] = trim($params['address']);
		$data['telephone'] = trim($params['telephone']);
		$data['qqnumber'] = trim($params['qqnumber']);
		$data['uploadify100'] = isset($params['uploadify100'])?$params['uploadify100']:'';
		$data['uploadify200'] = isset($params['uploadify200'])?$params['uploadify200']:'';
		$data['uploadify300'] = isset($params['uploadify300'])?$params['uploadify300']:'';
		$data['uploadify400'] = isset($params['uploadify400'])?$params['uploadify400']:'';
		$data['uploadify500'] = isset($params['uploadify500'])?$params['uploadify500']:'';
		$data['uploadify600'] = isset($params['uploadify600'])?$params['uploadify600']:'';
		$data['userid'] = $userInfo['userid'];
		$data['status'] = 1;
		$data['createtime'] = time();
		$id = $this->db->insert('account_info', $data);
		if($id > 0)
		{
			$where['userid'] = $data['userid'];
			$upData['accountstatus'] = 0;
			$upData['accounttype'] = $params['accounttype'];
			$upData['qq'] = $data['qqnumber'];
			$upData['telephone'] = $data['mobile'];
			$this->db->where($where);
			$this->db->update('user_member', $upData);

			$userInfo['isauth'] = $upData['accountstatus'];
			$userInfo['istype'] = $upData['accounttype'];
			$this->session->set_userdata($userInfo);
			$error['status'] == 1;
			return $error;
		}
		else
		{
			$error['status'] = 0;
			$error['data'] = array('accounttypemsg', '未知错误！');
			$error['info'] = 'accounttypeerr';
			return $error;
		}
	}




	/**
	 * 取消用户身份升级接口
	 *
	 * @author wangdong
	 * @date 2013.10.15
	 * @note 详细说明及修改日志
	 */
	public function doUpdateCancel($userInfo)
	{
		$error = array('status' => 0);
		$accounthistory = $this->getAccountHistory($userInfo['userid']);
		$where['status'] = 1;
        $where['accounttype'] = $userInfo['istype'];
        $where['userid'] = $userInfo['userid'];
        $currentaccount = $this->getAccountByStatus($where);
		
		if($accounthistory!=false && count($accounthistory)>1 && $accounthistory[0]['tag']!=3 && $accounthistory[0]['createtime']>$currentaccount['createtime'] ){
			$accountInfo=$accounthistory[0];
		}
		if(empty($accountInfo))
		{
			$error['data'] = '未知错误！';
			$error['info'] = '';
			return $error;
		}
		if($accountInfo['auditstatus'] == 1)
		{
			$error['data'] = '您的信息已经审核通过！';
			$error['info'] = '';
			return $error;
		}
		$data['tag'] = 3;
		$data['status'] = 2;
		$data['canceltime'] = time();
		$this->db->where('id', $accountInfo['id']);
		$id = $this->db->update('account_info', $data);
		if($id>0)
		{
			$error['data'] = '';
			$error['info'] = '';
			$error['status'] = 1;
		}
		else
		{
			$error['data'] = '未知错误！';
			$error['info'] = '';
		}
		return $error;
	}

	/**
	 * 用户身份升级内容编辑接口
	 */
	public function doEditUpAccount($params, $userInfo)
	{
		$error = array('status' => false);
		$where['status'] = 1;
        $where['accounttype'] = $userInfo['istype'];
        $where['userid'] = $userInfo['userid'];
        $currentaccount = $this->getAccountByStatus($where);
        $params['currency']=$currentaccount['currency'];
		$accounthistory = $this->getAccountHistory($userInfo['userid']);
		if($accounthistory != false && count($accounthistory) > 1 && $accounthistory[0]['tag'] != 1 && $accounthistory[0]['tag'] != 3  && $accounthistory[0]['auditstatus'] == 2 && $accounthistory[0]['createtime'] > $currentaccount['createtime']){
			$oldInfo = $accounthistory[0];
			if(empty($oldInfo))
			{
				$error['data'] = array('accounttypemsg', '未知错误！');
				$error['info'] = 'accounttypeerr';
				return $error;
			}
			$info = array('status'=>false);
			if($where['accounttype'] == 1 || ($where['accounttype'] == 3 && isset($params['accounttype']) && $params['accounttype'] == 2)){
        		$info = $this->doUpdateAccount($params,$userInfo);
	        }
	        elseif($where['accounttype'] == 2 || ($where['accounttype'] == 3 && isset($params['accounttype']) && $params['accounttype'] == 1)){
	        	$info = $this->doDownGradeAccount($params, $userInfo);
	        } 
			if($info['status'])
			{
				$this->db->where('id', $oldInfo['id']);
				$id = $this->db->update('account_info', array('status'=>2));
				$error['status'] = 1;
				return $error;
			}
			else
			{
				return $info;
			}
		}
		else{
				$error['data'] = array('accounttypemsg', '未知错误！');
				$error['info'] = 'accounttypeerr';
				return $error;
		}		
	}

	
/**
	 * 用户身份降级内容编辑接口
	 */
	public function doEditDownAccount($params, $userInfo)
	{
		$error = array('status' => false);
		$oldInfo = $this->getAccountByWhere(array('status'=>1, 'tag'=>4, 'auditstatus' => 2, 'userid'=>$userInfo['userid']));
		if(empty($oldInfo))
		{
			$error['data'] = array('accounttypemsg', '未知错误！');
			$error['info'] = 'accounttypeerr';
			return $error;
		}
		$info = $this->doDownGradeAccount($params, $userInfo);
		if($info['status'])
		{
			$this->db->where('id', $oldInfo['id']);
			$id = $this->db->update('account_info', array('status'=>2));
			$error['status'] = 1;
			return $error;
		}
		else
		{
			$error['data'] = array('accounttypemsg', '未知错误！');
			$error['info'] = 'accounttypeerr';
			return $info;
		}
	}
	
	/**
	 * 用户身份升级接口
	 *
	 * @author wangdong
	 * @date 2013.10.15
	 * @note 详细说明及修改日志
	 */
	public function doUpdateAccount($params, $userInfo)
	{
		$error = array('status' => false);
		$error = $this->chkAddInfo($params, true);
		if($error['status'] == false)
		{
			return $error;
		}
		$data['accounttype'] = $params['accounttype'];
		$data['certtype'] = isset($params['certtype'])?$params['certtype']:null;
		$data['realname'] = trim($params['realname']);
		$data['idnumber'] = trim($params['idnumber']);
		$data['companyname'] = trim($params['companyname']);
		$data['companylicense'] = trim($params['companylicense']);
		$data['companyid'] = trim($params['companyid']);
		$data['individualname'] = trim($params['individualname']);
		$data['individualid'] = trim($params['individualid']);
		$data['ownername'] = trim($params['ownername']);
		$data['ownercerttype'] = isset($params['ownercerttype'])?$params['ownercerttype']:null;
		$data['ownerid'] = trim($params['ownerid']);
		$data['bankusername'] = trim($params['bankusername']);
		$data['province'] = $params['province'];
		$data['city'] = $params['city'];
		$data['bank'] = $params['bank'];
		$data['bankaddress'] = trim($params['bankaddress']);
		$data['bankaccount'] = trim($params['bankaccount']);
		//$data['rebankaccount'] = $params['rebankaccount'];
		$data['contact'] = $params['contact'];
		$data['mobile'] = trim($params['mobile']);
		$data['address'] = trim($params['address']);
		$data['telephone'] = trim($params['telephone']);
		$data['qqnumber'] = trim($params['qqnumber']);
		$data['uploadify100'] = isset($params['uploadify100'])?$params['uploadify100']:'';
		$data['uploadify200'] = isset($params['uploadify200'])?$params['uploadify200']:'';
		$data['uploadify300'] = isset($params['uploadify300'])?$params['uploadify300']:'';
		$data['uploadify400'] = isset($params['uploadify400'])?$params['uploadify400']:'';
		$data['uploadify500'] = isset($params['uploadify500'])?$params['uploadify500']:'';
		$data['uploadify600'] = isset($params['uploadify600'])?$params['uploadify600']:'';
		$data['userid'] = $userInfo['userid'];
		$data['status'] = 1;
		$data['tag'] = 2;
		$data['currency'] = $params['currency'];
		$data['createtime'] = time();
		$id = $this->db->insert('account_info', $data);
		if($id > 0)
		{
			$where['userid'] = $userInfo['userid'];
			$upData['qq'] = $data['qqnumber'];
			$upData['telephone'] = $data['mobile'];
			$this->db->where($where);
			$this->db->update('user_member', $upData);
			$error['status'] == 1;
			return $error;
		}
		else
		{
			$error['status'] = 0;
			$error['data'] = array('accounttypemsg', '未知错误！');
			$error['info'] = 'accounttypeerr';
			return $error;
		}
	}

	/**
	 * 注册用户接口
	 *
	 * @author wangdong
	 * @date 2013.10.15
	 * @note 详细说明及修改日志
	 */
	public function doEdit($params, $userInfo)
	{
		$error = array('status' => false);
		$error = $this->chkAddInfo($params);
		if($error['status'] == false)
		{
			return $error;
		}
		$data = $this->getAccountByStatus(array('id'=>$params['id'], 'userid'=>$userInfo['userid']));
		if(!empty($data))
		{
			unset($data['id']);
		}
		else
		{
			$error['status'] = 0;
			$error['data'] = array('accounttypemsg', '未知错误！');
			$error['info'] = 'accounttypeerr';
			return $error;
		}
		$data['accounttype'] = $params['accounttype'];
		$data['certtype'] = isset($params['certtype'])?$params['certtype']:null;
		$data['realname'] = trim($params['realname']);
		$data['idnumber'] = trim($params['idnumber']);
		$data['companyname'] = trim($params['companyname']);
		$data['companylicense'] = trim($params['companylicense']);
		$data['companyid'] = trim($params['companyid']);
		$data['individualname'] = trim($params['individualname']);
		$data['individualid'] = trim($params['individualid']);
		$data['currency'] = $params['currency'];
		$data['ownername'] = trim($params['ownername']);
		$data['ownercerttype'] = isset($params['ownercerttype'])?$params['ownercerttype']:null;
		$data['ownerid'] = trim($params['ownerid']);
		$data['bankusername'] = trim($params['bankusername']);
		$data['province'] = $params['province'];
		$data['city'] = $params['city'];
		$data['bank'] = $params['bank'];
		$data['bankaddress'] = trim($params['bankaddress']);
		$data['bankaccount'] = trim($params['bankaccount']);
		//$data['rebankaccount'] = $params['rebankaccount'];
		$data['contact'] = trim($params['contact']);
		$data['mobile'] = trim($params['mobile']);
		$data['address'] = trim($params['address']);
		$data['telephone'] = trim($params['telephone']);
		$data['qqnumber'] = trim($params['qqnumber']);
		$data['infosource'] = $params['infosource'];
		$data['sourcename'] = trim($params['sourcename']);
		if(isset($params['uploadify100']))
		{
			$data['uploadify100'] = $params['uploadify100'];
		}
		if(isset($params['uploadify200']))
		{
			$data['uploadify200'] = $params['uploadify200'];
		}
		if(isset($params['uploadify300']))
		{
			$data['uploadify300'] = $params['uploadify300'];
		}
		if(isset($params['uploadify400']))
		{
			$data['uploadify400'] = $params['uploadify400'];
		}
		if(isset($params['uploadify500']))
		{
			$data['uploadify500'] = $params['uploadify500'];
		}
		if(isset($params['uploadify600']))
		{
			$data['uploadify600'] = $params['uploadify600'];
		}
		$data['userid'] = $userInfo['userid'];
		$data['status'] = 1;
		$data['auditstatus'] = 0;
		$data['createtime'] = time();
		$id = $this->db->insert('account_info', $data);
		if($id > 0)
		{
			$this->db->where(array('id'=>$params['id'],'userid'=>$userInfo['userid']));
			$num = $this->db->update('account_info', array('status'=>2));
			if($num)
			{
				$where['userid'] = $userInfo['userid'];
				$upData['accountstatus'] = 0;
				$upData['accounttype'] = $params['accounttype'];
				$upData['qq'] = $data['qqnumber'];
				$upData['telephone'] = $data['mobile'];
				$this->db->where($where);
				$this->db->update('user_member', $upData);
				$userInfo['isauth'] = $upData['accountstatus'];
				$userInfo['istype'] = $upData['accounttype'];
				$this->session->set_userdata($userInfo);
				$error['status'] == 1;
			}
			else
			{
				$error['status'] = 0;
				$error['data'] = array('accounttypemsg', '未知错误！');
				$error['info'] = 'accounttypeerr';
			}
			return $error;
		}
		else
		{
			$error['status'] = 0;
			$error['data'] = array('accounttypemsg', '未知错误！');
			$error['info'] = 'accounttypeerr';
			return $error;
		}
	}

	/**
	 * 检测广告组信息
	 *
	 * @author wangdong
	 * @date 2013.10.19
	 * @note 详细说明及修改日志
	 */
	private function chkAddInfo($params, $update=false)
	{
		$error = array('status' => false);
		if(empty($params['accounttype']))
		{
			$error['data'] = array('accounttypemsg', '请选择身份类型！');
			$error['info'] = 'accounttypeerr';
			return $error;
		}
		if($params['accounttype'] == 1)
		{
			if(empty($params['certtype']))
			{
				$error['data'] = array('certtypemsg', '请选择证件类型！');
				$error['info'] = 'certtypeerr';
				return $error;
			}
			if(empty($params['realname']))
			{
				$error['data'] = array('realnamemsg', '请填写证件姓名。');
				$error['info'] = 'realnameerr';
				return $error;
			}
			if(empty($params['idnumber']))
			{
				$error['data'] = array('idnumbermsg', '请填写证件号码。');
				$error['info'] = 'idnumbererr';
				return $error;
			}
			if(empty($params['uploadify100']))
			{
				$error['data'] = array('uploadify100msg', '请上传证件正面图片。');
				$error['info'] = 'uploadify100err';
				return $error;
			}
			if(empty($params['uploadify200']))
			{
				$error['data'] = array('uploadify200msg', '请上传证件背面图片。');
				$error['info'] = 'uploadify200err';
				return $error;
			}
			if(empty($params['bankusername']) || $params['bankusername']!= $params['realname'])
			{
				$error['data'] = array('bankusernamemsg', '请填开户人姓名并与证件姓名保持一致。');
				$error['info'] = 'bankusernameerr';
				return $error;
			}
			$data = array('certtype'=>$params['certtype'], 'idnumber'=>$params['idnumber'], 'accounttype'=>$params['accounttype'], 'status'=>1, );
			if(!empty($params['id']))
			{
				$data['id !='] = $params['id'];
			}
			//            $chkAccountInfo = $this->getAccountByWhere($data);
			//            if(!empty($chkAccountInfo))
			//            {
			//                $error['data'] = array('idnumbermsg', '此证件号码已经被使用。');
			//                $error['info'] = 'idnumbererr';
			//                return $error;
			//            }
		}
		elseif($params['accounttype'] == 2)
		{
			if(empty($params['companyname']))
			{
				$error['data'] = array('companynamemsg', '请填写公司全称。');
				$error['info'] = 'companynameerr';
				return $error;
			}
			if(empty($params['companylicense']))
			{
				$error['data'] = array('companylicensemsg', '请填写营业执照号。');
				$error['info'] = 'companylicenseerr';
				return $error;
			}
			if(empty($params['companyid']))
			{
				$error['data'] = array('companyidmsg', '请填写组织机构代码。');
				$error['info'] = 'companyiderr';
				return $error;
			}
			if(empty($params['uploadify300']))
			{
				$error['data'] = array('uploadify300msg', '请上传营业执照图片。');
				$error['info'] = 'uploadify300err';
				return $error;
			}
			if(empty($params['bankusername']) || $params['bankusername']!= $params['companyname'])
			{
				$error['data'] = array('bankusernamemsg', '请填开户人姓名并与公司全称保持一致。');
				$error['info'] = 'bankusernameerr';
				return $error;
			}
			$data = array('companylicense'=>$params['companylicense'], 'accounttype'=>$params['accounttype'], 'status'=>1, 'auditstatus !=' => 2 );
			if(!empty($params['id']))
			{
				$data['id !='] = $params['id'];
			}
			//            $chkAccountInfo = $this->getAccountByWhere($data);
			//            if(!empty($chkAccountInfo))
			//            {
			//                $error['data'] = array('companylicensemsg', '此营业执照号已被使用。');
			//                $error['info'] = 'companylicenseerr';
			//                return $error;
			//            }
		}
		elseif($params['accounttype'] == 3)
		{
			if(empty($params['individualid']))
			{
				$error['data'] = array('individualidmsg', '请填写营业执照号。');
				$error['info'] = 'individualiderr';
				return $error;
			}
			if(empty($params['uploadify400']))
			{
				$error['data'] = array('uploadify400msg', '请上传营业执照图片。');
				$error['info'] = 'uploadify400err';
				return $error;
			}
			if(empty($params['ownername']))
			{
				$error['data'] = array('ownernamemsg', '请填写业主姓名。');
				$error['info'] = 'ownernameerr';
				return $error;
			}
			if(empty($params['ownercerttype']))
			{
				$error['data'] = array('ownercerttypemsg', '请选择证件类型。');
				$error['info'] = 'ownercerttypeerr';
				return $error;
			}
			if(empty($params['ownerid']))
			{
				$error['data'] = array('owneridmsg', '请填写证件号码。');
				$error['info'] = 'owneriderr';
				return $error;
			}
			if(empty($params['uploadify500']))
			{
				$error['data'] = array('uploadify500msg', '请上传证件正面图片。');
				$error['info'] = 'uploadify500err';
				return $error;
			}
			if(empty($params['uploadify600']))
			{
				$error['data'] = array('uploadify600msg', '请上传证件背面图片。');
				$error['info'] = 'uploadify600err';
				return $error;
			}
            if(empty($params['bankusername']) || $params['bankusername']!= $params['ownername'])
            {
                $error['data'] = array('bankusernamemsg', '请填开户人姓名并与业主姓名保持一致。');
                $error['info'] = 'bankusernameerr';
                return $error;
            }
//			$data = array('individualid'=>$params['individualid'], 'accounttype'=>$params['accounttype'], 'status'=>1, );
//			if(!empty($params['id']))
//			{
//				$data['id !='] = $params['id'];
//			}
				//            $chkAccountInfo = $this->getAccountByWhere($data);
				//            if(!empty($chkAccountInfo))
				//            {
				//                $error['data'] = array('individualidmsg', '此营业执照号已被使用。');
				//                $error['info'] = 'individualiderr';
				//                return $error;
				//            }
		}

		if(empty($params['currency']))
		{
			$error['data'] = array('currencytypemsg', '请选择币种。');
			$error['info'] = 'currencytypeerr';
			return $error;
		}
		if(empty($params['province']) || $params['province'] == '请选择省份名')
		{
			$error['data'] = array('provincemsg', '请选择所在省份。');
			$error['info'] = 'provinceerr';
			return $error;
		}
		if(empty($params['city']) || $params['city'] == '请选择城市名' || $params['city'] == '请选择')
		{
			$error['data'] = array('provincemsg', '请选择所在城市。');
			$error['info'] = 'provinceerr';
			return $error;
		}
		if(empty($params['bankaddress']))
		{
			$error['data'] = array('bankaddressmsg', '请填写具体分行/支行地址。');
			$error['info'] = 'bankaddresserr';
			return $error;
		}
		if(empty($params['bankaccount']))
		{
			$error['data'] = array('bankaccountmsg', '请填写收款银行卡号。');
			$error['info'] = 'bankaccounterr';
			return $error;
		}
		if($params['rebankaccount'] != $params['bankaccount'])
		{
			$error['data'] = array('rebankaccountmsg', '两次输入银行卡卡号有误。');
			$error['info'] = 'rebankaccounterr';
			return $error;
		}
		if(empty($params['contact']))
		{
			$error['data'] = array('contactmsg', '请填写联系人姓名。');
			$error['info'] = 'contacterr';
			return $error;
		}
		if(empty($params['mobile']))
		{
			$error['data'] = array('mobilemsg', '请填写手机号码。');
			$error['info'] = 'mobileerr';
			return $error;
		}
		if(!preg_match("/^13[0-9]{1}[0-9]{8}$|15[0-9]{1}[0-9]{8}$|17[0-9]{1}[0-9]{8}$|18[0-9]{1}[0-9]{8}$/", $params['mobile'])){
			$error['data'] = array('mobilemsg', '请填写正确的手机号码。');
			$error['info'] = 'mobileerr';
			return $error;
		}
		if(empty($params['address']))
		{
			$error['data'] = array('addressmsg', '请填写联系地址。');
			$error['info'] = 'addresserr';
			return $error;
		}
		if(empty($params['qqnumber']))
		{
			$error['data'] = array('qqnumbermsg', '请填写您的QQ。');
			$error['info'] = 'qqnumbererr';
			return $error;
		}
		if(!$update){
			if(empty($params['infosource']))
			{
				$error['data'] = array('infosourcemsg', '请选择信息渠道。');
				$error['info'] = 'infosourceerr';
				$error['status'] = 0;
				return $error;
			}
			if(($params['infosource'] == 5 && empty($params['sourcename'])) || ($params['infosource'] == 5 && (mb_strlen($params['sourcename']) < 2 || mb_strlen($params['sourcename']) > 20 )))
			{
				$error['data'] = array('infosourcemsg', '请填写2-20字的信息渠道。');
				$error['info'] = 'infosourceerr';
				$error['status'] = 0;
				return $error;
			}
		}
		$error['status'] = true;
		return $error;
	}

	/**
	 * 获取账号基本信息
	 * Enter description here ...
	 * @param $appId
	 * @param $userId
	 */
	public function getAccountByStatus($where)
	{
		foreach($where as $k => $v)
		{
			$this->db->where_in($k, $v);
		}
		$this->db->order_by("createtime","desc");
		$query = $this->db->get('account_info');
		if ($query->num_rows() > 0)
		{
			$data = $query->row_array();
		}
		if(empty($data))
		{
			return false;
		}
		return $data;
	}

	/**
	 * 获取账号基本信息
	 * Enter description here ...
	 * @param $appId
	 * @param $userId
	 */
	public function getAccountByWhere($where)
	{
		if(empty($where))
		{
			return false;
		}
		$query = $this->db->get_where('account_info', $where);
		if ($query->num_rows() > 0)
		{
			$data = $query->row_array();
		}
		if(empty($data))
		{
			return false;
		}
		return $data;
	}

	/**
	 * 编辑联系人信息
	 */
	public function doEditContact($params, $userInfo)
	{
		$error = array('status' => false);
		$error = $this->chkContactInfo($params);
		if($error['status'] == false)
		{
			return $error;
		}
		$data['contact'] = trim($params['contact']);
		$data['mobile'] = trim($params['mobile']);
		$data['address'] = trim($params['address']);
		$data['telephone'] = trim($params['telephone']);
		$data['qqnumber'] = trim($params['qqnumber']);
		$where['status'] = 1;
		$where['accounttype'] = $userInfo['istype'];
		$where['auditstatus'] = $userInfo['isauth'];
		$where['userid'] = $userInfo['userid'];
		$info = $this->getAccountByStatus($where);
		if(empty($info))
		{
			$error['status'] = 0;
			$error['data'] = array('contactmsg', '未知错误！');
			$error['info'] = 'contacterr';
			return $error;
		}
		$this->db->where('id', $info['id']);
		$this->db->where('userid', $info['userid']);
		$id = $this->db->update('account_info', $data);
		if($id > 0)
		{
			$upData['qq'] = $data['qqnumber'];
			$upData['telephone'] = $data['mobile'];
			$this->db->where(array('userid' => $userInfo['userid']));
			$this->db->update('user_member', $upData);
			$error['status'] == 1;
			return $error;
		}
		else
		{
			$error['status'] = 0;
			$error['data'] = array('contactmsg', '未知错误！');
			$error['info'] = 'contacterr';
			return $error;
		}
	}

	/**
	 * 编辑联系人信息
	 */
	public function doEditFinance($params, $userInfo)
	{
		$error = array('status' => false);
		$error = $this->chkFinanceInfo($params);
		if($error['status'] == false)
		{
			return $error;
		}
		$data['province'] = $params['province'];
		$data['city'] = $params['city'];
		$data['bank'] = $params['bank'];
		$data['bankusername'] = $params['bankusername'];
		$data['bankaddress'] = trim($params['bankaddress']);
		$data['bankaccount'] = trim($params['bankaccount']);
		$where['status'] = 1;
		$where['accounttype'] = $userInfo['istype'];
		$where['auditstatus'] = $userInfo['isauth'];
		$where['userid'] = $userInfo['userid'];
		$info = $this->getAccountByStatus($where);
		if(empty($info))
		{
			$error['status'] = 0;
			$error['data'] = array('bankusernamemsg', '未知错误！');
			$error['info'] = 'bankusernameerr';
			return $error;
		}
		$this->db->where('id', $info['id']);
		$this->db->where('userid', $info['userid']);
		$id = $this->db->update('account_info', $data);
		if($id > 0)
		{
			$error['status'] == 1;
			return $error;
		}
		else
		{
			$error['status'] = 0;
			$error['data'] = array('bankusernamemsg', '未知错误！');
			$error['info'] = 'bankusernameerr';
			return $error;
		}
	}

	/**
	 * 检测联系人信息
	 *
	 * @author wangdong
	 * @date 2013.10.19
	 * @note 详细说明及修改日志
	 */
	private function chkFinanceInfo($params)
	{
		$error = array('status' => false);

		if(empty($params['bankusername']))
		{
			$error['data'] = array('bankusernamemsg', '请填写收款方名。');
			$error['info'] = 'bankusernameerr';
			return $error;
		}
		if(empty($params['province']) || $params['province'] == '请选择省份名')
		{
			$error['data'] = array('provincemsg', '请选择所在省份。');
			$error['info'] = 'provinceerr';
			return $error;
		}
		if(empty($params['city']) || $params['city'] == '请选择城市名' || $params['city'] == '请选择')
		{
			$error['data'] = array('provincemsg', '请选择所在城市。');
			$error['info'] = 'provinceerr';
			return $error;
		}
		if(empty($params['bankaddress']))
		{
			$error['data'] = array('bankaddressmsg', '请填写具体分行/支行地址。');
			$error['info'] = 'bankaddresserr';
			return $error;
		}
		if(empty($params['bankaccount']))
		{
			$error['data'] = array('bankaccountmsg', '请填写收款银行卡号。');
			$error['info'] = 'bankaccounterr';
			return $error;
		}
		if($params['rebankaccount'] != $params['bankaccount'])
		{
			$error['data'] = array('rebankaccountmsg', '两次输入银行卡卡号有误。');
			$error['info'] = 'rebankaccounterr';
			return $error;
		}
		$error['status'] = true;
		return $error;
	}

	/**
	 * 检测联系人信息
	 *
	 * @author wangdong
	 * @date 2013.10.19
	 * @note 详细说明及修改日志
	 */
	private function chkContactInfo($params)
	{
		$error = array('status' => false);
		if(empty($params['contact']))
		{
			$error['data'] = array('contactmsg', '请填写联系人姓名。');
			$error['info'] = 'contacterr';
			return $error;
		}
		if(empty($params['mobile']))
		{
			$error['data'] = array('mobilemsg', '请填写手机号码。');
			$error['info'] = 'mobileerr';
			return $error;
		}
		if(!preg_match("/^13[0-9]{1}[0-9]{8}$|15[0-9]{1}[0-9]{8}$|17[0-9]{1}[0-9]{8}$|18[0-9]{1}[0-9]{8}$/", $params['mobile'])){
			$error['data'] = array('mobilemsg', '请填写正确的手机号码。');
			$error['info'] = 'mobileerr';
			return $error;
		}
		if(empty($params['address']))
		{
			$error['data'] = array('addressmsg', '请填写联系地址。');
			$error['info'] = 'addresserr';
			return $error;
		}
		if(empty($params['qqnumber']))
		{
			$error['data'] = array('qqnumbermsg', '请填写您的QQ。');
			$error['info'] = 'qqnumbererr';
			return $error;
		}
		$error['status'] = true;
		return $error;
	}

	/**
	 * 修改个人用户身份认证信息---
	 * 2015-03-20
	 * wangfei
	 * $error['status']    1:right,0:error
	 */
	public function  doEditGroup($params){
		$error = array('status' => 0);
		$id = $params['id'];
		if(empty($id))
		{
			$error['data'] = array('certtypemsg', '未知错误！');
			$error['info'] = 'certtypeerr';
			$error['status'] = 0;
			return $error;
		}
		$errorflag = $this->chkIAGroupInfo($params);
		if(!$errorflag['status'])
		{
			$error['info']=$errorflag['info'];
			$error['data']=$errorflag['data'];
			return $error;
		}
		$where['id']=$id;
		$accountInfo = $this->getAccountByWhere($where);
		if(empty($accountInfo))
		{
			$error['data'] = array('certtypemsg', '该用户不存在！');
			$error['info'] = 'certtypeerr';
			$error['status'] = 0;
			return $error;
		}
		$updata['certtype'] = $params['certtype'];
		$updata['realname'] = $params['realname'];
		$updata['idnumber'] = $params['idnumber'];
		$updata['uploadify100'] = $params['uploadify100'];
		$updata['uploadify200'] = $params['uploadify200'];
		$updata['status'] = 1;
		$updata['auditstatus'] = 0;
		$updata['audittime'] = time();
		$updata['reason'] = '';
		$this->db->where($where);
		$updateflag=$this->db->update('account_info', $updata);
		if($updateflag > 0)
		{
			$error['status'] = 1;
			return $error;
		}
		else
		{
			$error['status'] = 0;
			$error['data'] = array('certtypemsg', '未知错误！');
			$error['info'] = 'certtypeerr';
			return $error;
		}
	}

	/**
	 * wangfei
	 * identity authentication:IA
	 * 检测用户身份认证信息
	 */
	public function chkIAGroupInfo($params){
		$error = array('status' => false);
		if(empty($params['certtype'])){
			$error['data'] = array('certtypemsg', '请选择证件类型！');
			$error['info'] = 'certtypeerr';
			return $error;
		}
		if(empty($params['realname']))
		{
			$error['data'] = array('realnamemsg', '请填写证件姓名。');
			$error['info'] = 'realnameerr';
			return $error;
		}
		if(empty($params['idnumber']))
		{
			$error['data'] = array('idnumbermsg', '请填写证件号码。');
			$error['info'] = 'idnumbererr';
			return $error;
		}
		if(empty($params['uploadify100']))
		{
			$error['data'] = array('uploadify100msg', '请上传证件正面图片。');
			$error['info'] = 'uploadify100err';
			return $error;
		}
		if(empty($params['uploadify200']))
		{
			$error['data'] = array('uploadify200msg', '请上传证件背面图片。');
			$error['info'] = 'uploadify200err';
			return $error;
		}
		$error['status'] = true;
		return $error;
	}
	
	
	
	/**
	 * 修改企业用户身份认证信息---
	 * 2015-04-17
	 * wangfei
	 * $error['status']    1:right,0:error
	 */
	public function  doEditCompany($params){
		$error = array('status' => 0);
		$id = $params['id'];
		if(empty($id))
		{
			$error['data'] = array('companynamemsg', '未知错误！');
			$error['info'] = 'companynameerr';
			$error['status'] = 0;
			return $error;
		}
		$errorflag = $this->chkIACompanyInfo($params);
		if(!$errorflag['status'])
		{
			$error['info']=$errorflag['info'];
			$error['data']=$errorflag['data'];
			return $error;
		}
		$where['id']=$id;
		$accountInfo = $this->getAccountByWhere($where);
		if(empty($accountInfo))
		{
			$error['data'] = array('companynamemsg', '该公司不存在！');
			$error['info'] = 'companynameerr';
			$error['status'] = 0;
			return $error;
		}
		$updata['companyname'] = $params['companyname'];
		$updata['companylicense'] = $params['companylicense'];
		$updata['companyid'] = $params['companyid'];
		$updata['uploadify300'] = $params['uploadify300'];
		$updata['status'] = 1;
		$updata['auditstatus'] = 0;
		$updata['audittime'] = time();
		$updata['reason'] = '';
		$this->db->where($where);
		$updateflag=$this->db->update('account_info', $updata);
		if($updateflag > 0)
		{
			$error['status'] = 1;
			return $error;
		}
		else
		{
			$error['status'] = 0;
			$error['data'] = array('companynamemsg', '未知错误！');
			$error['info'] = 'companynameerr';
			return $error;
		}
	}
	
	/**
	 * wangfei
	 * identity authentication:IA
	 * 检测企业用户身份认证信息
	 */
	public function chkIACompanyInfo($params){
		$error = array('status' => false);
		if(empty($params['companyname'])){
			$error['data'] = array('companynamemsg', '请填写公司名称！');
			$error['info'] = 'companynameerr';
			return $error;
		}
		if(empty($params['companylicense']))
		{
			$error['data'] = array('companylicensemsg', '请填写营业执照号');
			$error['info'] = 'companylicenseerr';
			return $error;
		}
		if(empty($params['companyid']))
		{
			$error['data'] = array('companyidmsg', '请填写组织机构代码');
			$error['info'] = 'companyiderr';
			return $error;
		}
		if(empty($params['uploadify300']))
		{
			$error['data'] = array('uploadify300msg', '请上传营业执照图片。');
			$error['info'] = 'uploadify300err';
			return $error;
		}
		
		$error['status'] = true;
		return $error;
	}
	
	
/**
	 * 修改个体工商身份认证信息---
	 * 2015-04-22
	 * wangfei
	 * $error['status']    1:right,0:error
	 */
	public function  doEditOwner($params){
		$error = array('status' => 0);
		$id = $params['id'];
		if(empty($id))
		{
			$error['data'] = array('individualnamemsg', '未知错误！');
			$error['info'] = 'individualnameerr';
			$error['status'] = 0;
			return $error;
		}
		$errorflag = $this->chkIAOwnerInfo($params);
		if(!$errorflag['status'])
		{
			$error['info']=$errorflag['info'];
			$error['data']=$errorflag['data'];
			return $error;
		}
		$where['id']=$id;
		$accountInfo = $this->getAccountByWhere($where);
		if(empty($accountInfo))
		{
			$error['data'] = array('individualnamemsg', '该个体工商不存在！');
			$error['info'] = 'individualnameerrl';
			$error['status'] = 0;
			return $error;
		}
		$updata['individualname'] = $params['individualname'];
		$updata['individualid'] = $params['individualid'];
		$updata['uploadify400'] = $params['uploadify400'];
		$updata['ownername'] = $params['ownername'];
		$updata['ownercerttype'] = $params['ownercerttype'];
		$updata['ownerid'] = $params['ownerid'];
		$updata['uploadify500'] = $params['uploadify500'];
		$updata['uploadify600'] = $params['uploadify600'];
		$updata['status'] = 1;
		$updata['auditstatus'] = 0;
		$updata['audittime'] = time();
		$updata['reason'] = '';
		$this->db->where($where);
		$updateflag=$this->db->update('account_info', $updata);
		if($updateflag > 0)
		{
			$error['status'] = 1;
			return $error;
		}
		else
		{
			$error['status'] = 0;
			$error['data'] = array('individualnamemsg', '未知错误！');
			$error['info'] = 'individualnameerr';
			return $error;
		}
	}
	
	/**
	 * wangfei
	 * identity authentication:IA
	 * 检测个体工商身份认证信息
	 */
	public function chkIAOwnerInfo($params){
		$error = array('status' => false);
		if(empty($params['individualname']))
		{
			$error['data'] = array('individualnamemsg', '请填写个体工商名称。');
			$error['info'] = 'individualnameerr';
			return $error;
		}
		if(empty($params['individualid']))
		{
			$error['data'] = array('individualidmsg', '请填写营业执照号。');
			$error['info'] = 'individualiderr';
			return $error;
		}
		
		if(empty($params['uploadify400']))
		{
			$error['data'] = array('uploadify400msg', '请上传营业执照图片。');
			$error['info'] = 'uploadify400err';
			return $error;
		}
		if(empty($params['ownername']))
		{
			$error['data'] = array('ownernamemsg', '请填写业主姓名。');
			$error['info'] = 'ownernameerr';
			return $error;
		}
		if(empty($params['ownercerttype']))
		{
			$error['data'] = array('ownercerttypemsg', '请选择证件类型。');
			$error['info'] = 'ownercerttypeerr';
			return $error;
		}
		if(empty($params['ownerid']))
		{
			$error['data'] = array('owneridmsg', '请填写证件号码。');
			$error['info'] = 'owneriderr';
			return $error;
		}
		if(empty($params['uploadify500']))
		{
			$error['data'] = array('uploadify500msg', '请上传证件正面图片。');
			$error['info'] = 'uploadify500err';
			return $error;
		}
		if(empty($params['uploadify600']))
		{
			$error['data'] = array('uploadify600msg', '请上传证件背面图片。');
			$error['info'] = 'uploadify600err';
			return $error;
		}
		
		$error['status'] = true;
		return $error;
	}
	
	
	/**
	 * 用户身份降级接口
	 *
	 * @author 王飞
	 * @date 2015.09.15
	 */
	public function doDownGradeAccount($params, $userInfo)
	{
		$error = array('status' => false);
		$error = $this->chkAddInfo($params, true);
		if($error['status'] == false)
		{
			return $error;
		}
		$data['accounttype'] = $params['accounttype'];
		$data['certtype'] = isset($params['certtype'])?$params['certtype']:null;
		$data['realname'] = trim($params['realname']);
		$data['idnumber'] = trim($params['idnumber']);
		$data['companyname'] = trim($params['companyname']);
		$data['companylicense'] = trim($params['companylicense']);
		$data['companyid'] = trim($params['companyid']);
		$data['individualname'] = trim($params['individualname']);
		$data['individualid'] = trim($params['individualid']);
		$data['ownername'] = trim($params['ownername']);
		$data['ownercerttype'] = isset($params['ownercerttype'])?$params['ownercerttype']:null;
		$data['ownerid'] = trim($params['ownerid']);
		$data['bankusername'] = trim($params['bankusername']);
		$data['province'] = $params['province'];
		$data['city'] = $params['city'];
		$data['bank'] = $params['bank'];
		$data['bankaddress'] = trim($params['bankaddress']);
		$data['bankaccount'] = trim($params['bankaccount']);
		$data['contact'] = $params['contact'];
		$data['mobile'] = trim($params['mobile']);
		$data['address'] = trim($params['address']);
		$data['telephone'] = trim($params['telephone']);
		$data['qqnumber'] = trim($params['qqnumber']);
		$data['uploadify100'] = isset($params['uploadify100'])?$params['uploadify100']:'';
		$data['uploadify200'] = isset($params['uploadify200'])?$params['uploadify200']:'';
		$data['uploadify300'] = isset($params['uploadify300'])?$params['uploadify300']:'';
		$data['uploadify400'] = isset($params['uploadify400'])?$params['uploadify400']:'';
		$data['uploadify500'] = isset($params['uploadify500'])?$params['uploadify500']:'';
		$data['uploadify600'] = isset($params['uploadify600'])?$params['uploadify600']:'';
		$data['userid'] = $userInfo['userid'];
		$data['auditstatus']=0;
		$data['status'] = 1;
		$data['tag'] = 4;
		$data['currency'] = $params['currency'];
		$data['createtime'] = time();
		$id = $this->db->insert('account_info', $data);
		if($id > 0)
		{
			$where['userid'] = $userInfo['userid'];
			$upData['qq'] = $data['qqnumber'];
			$upData['telephone'] = $data['mobile'];
			$this->db->where($where);
			$this->db->update('user_member', $upData);
			$error['status'] == 1;
			return $error;
		}
		else
		{
			$error['status'] = 0;
			$error['data'] = array('accounttypemsg', '未知错误！');
			$error['info'] = 'accounttypeerr';
			return $error;
		}
	}
	
	/*
	 * 获取账户历史信息
	 */
 	public function getAccountHistory($userid)
    {
        $where['userid'] = $userid;
        $this->db->order_by("createtime","desc");
   		 $query = $this->db->get_where('account_info', $where);
        if ($query->num_rows() > 0)
        {
            $data = $query->result_array();
        }
        
        if(empty($data))
        {
            return false;
        }
        return $data;
    }
	
}
<?php
include dirname(__FILE__).'/reportclass.php';
class AppReportClass extends ReportBase{
    private $sumImp = 0;
    private $sumAsk = 0;
    private $sumClick = 0;
    private $sumCost = 0;
    private $sumClickRate = 0;
    private $sumImpRate = 0;
    private $sumCpm = 0;
    private $sumCpc = 0;
    private $adforms = array(1=>'Banner',2=>'插屏',3=>'推荐墙',4=>'积分墙',6=>'积分墙',8=>'推荐墙',20=>'信息流',30=>'RTB广告',40=>'视频广告');

    //广告位类型与数据类型对应关系转换
    private function adFormRelation($adform)
    {
        if(empty($adform))
        {
            return 1;
        }
        elseif($adform == 4)
        {
            return 6;
        }
        elseif($adform == 3)
        {
            //精品推荐为3，原生精品推荐为8.
            return '3,8';
        }
        return $adform;
    }
    
    function postAppReport($appIDList, $sdate, $edate, $applist){
    	$condition = array();
        $condition['server']       = "ads_union";
        $condition['t']            = "d_all_cost";
        $condition['pkCanalId'] = 'in|'.$appIDList;
        $condition['sd'] = 'ge|'.strtotime($sdate);
        $condition['ed'] = 'le|'.strtotime($edate);
        $condition['s']  = "pkCanalId,pkAdType,pkDay,sum(deductImp) imp,sum(deductClick) click,sum(deductIncome) income,sum(coins) as coins,sum(complate) complate";
        $condition['g']  = "pkCanalId,pkAdType,pkDay";
        $condition['o']  = "pkCanalId,pkAdType,pkDay";
        $config = &get_config();
    	$url = $config['data_api'];
    	$create_ret =  curlPost($url,  $condition);
    	$create_ret = json_decode($create_ret,true);
    	$appdata = isset($create_ret['data'])?$create_ret['data']:array();
    	$asklist = $this->postAppAskReport($appIDList, $sdate, $edate);
    	$listData = array();
    	if($appdata){
	    	foreach($appdata as $v){
	    		$listData[$v['pkCanalId']][$v['pkAdType']]['appname'] = $applist[$v['pkCanalId']]['appname'];
		        $listData[$v['pkCanalId']][$v['pkAdType']]['adform'] = isset($this->adforms[$v['pkAdType']])?$this->adforms[$v['pkAdType']]:$v['pkAdType'];
	    		$data = array('ask'=>0, 'imp'=>0, 'click'=>0, 'income'=>0, 'imprate'=>'0', 'clickrate'=>'0', 'coins'=>0, 'complate'=>0);
	    		if(isset($asklist[$v['pkCanalId']][$v['pkAdType']][$v['pkDay']]))
	            {
	            	$data['ask'] = $asklist[$v['pkCanalId']][$v['pkAdType']][$v['pkDay']];
	            }
	            else
	            {
	                $data['ask'] = 0;
	            }
	            $data['imp'] = $v['imp'];
	            $data['click'] = $v['click'];
	            $data['coins'] = $v['coins'];
	            $data['complate'] = $v['complate'];
	            $data['income'] = formatmoney($v['income'],'get',2,'.');
	    		if($data['ask']>0)
	            {
	            	$data['imprate'] = number_format($v['imp']*100/$data['ask'],2);
	            }
	            if($data['imp']>0)
	            {
	                $data['clickrate'] = number_format($v['click']*100/$v['imp'],2);
	            }
	            $listData[$v['pkCanalId']][$v['pkAdType']]['list'][$v['pkDay']] = $data;
	    	}
    	}
        return $listData;
    }
    
    /**
     * 获取应用数据
     * Enter description here ...
     * @param $ids
     * @param $sDate
     * @param $eDate
     * @param $adform
     * @param $pkAdId
     * @param $gpkAdId
     */
    function getAppGraphReport($ids, $sDate, $eDate, $pkAdId = 'pkCanalId', $adform, $gpkAdId = 'pkCanalId', $appid = '')
    {
    	//hear
        if($pkAdId == 'pkPlaceId')
        {
            $t = 'd_app_placeid';
        }
        else
        {
            $t='d_all_cost';
        }
        $condition = array();
        $condition['server']       = "server=ads_union";
        $condition['t']            = "t={$t}";
        if(!empty($appid))
        {
            $condition['pkCanalId'] = "pkCanalId='".$appid."'";
        }
        $condition[$pkAdId] = "{$pkAdId}='".$ids."'";
        $condition['pkAdType'] = "pkAdType=in|".$this->adFormRelation($adform);
        $condition['spkDay'] = 'sd=ge|'.strtotime($sDate);
        $condition['epkDay'] = 'ed=le|'.strtotime($eDate);
        $condition['s']  = "s=".urlencode("{$gpkAdId},sum(deductImp) imp,sum(deductClick) click,sum(deductIncome) income,sum(coins) as coins,sum(complate) complate");
        $condition['g']  = "g={$gpkAdId}";
        $list = $this->getDataSourceOnce($condition);
        //获取请求数据
        $askList = $this->getAppAskReport($ids, $sDate, $eDate, $pkAdId, $adform, $gpkAdId, $appid);
        $tmp = array();
        foreach($list as $val){
            $tmp[$val[$gpkAdId]] = $val;
        }
        
        $listData = array();
        $askSum = 0;
        $impSum = 0;
        $clickSmp = 0;
        $incomeSmp = 0;
        $coinsSmp = 0;
        $complateSmp = 0;
        //获取分列表和总计
        $formatDate = mktimes($sDate,$eDate,true,true);
        foreach ($formatDate as $v)
        {
            $data = array('ask'=>0, 'imp'=>0, 'click'=>0, 'income'=>0, 'imprate'=>'0', 'clickrate'=>'0', 'coins'=>0, 'complate'=>0);
            if(isset($tmp[$v]))
            {
                if(isset($askList[$v]))
                {
                    $data['ask'] = $askList[$v]['ask'];
                }
                else
                {
                    $data['ask'] = 0;
                }
                $data['imp'] = $tmp[$v]['imp'];
                $data['click'] = $tmp[$v]['click'];
                $data['coins'] = $tmp[$v]['coins'];
                $data['complate'] = $tmp[$v]['complate'];
                $data['income'] = formatmoney($tmp[$v]['income'],'get',2,'.');
                if($data['ask']>0)
                {
                    $data['imprate'] = number_format($tmp[$v]['imp']*100/$data['ask'],2);
                }
                if($data['imp']>0)
                {
                    $data['clickrate'] = number_format($tmp[$v]['click']*100/$tmp[$v]['imp'],2);
                }
                $listData[$v] = $data;
                $askSum += $data['ask'];
                $impSum += $tmp[$v]['imp'];
                $clickSmp += $tmp[$v]['click'];
                $incomeSmp += $tmp[$v]['income'];
                $coinsSmp += $tmp[$v]['coins'];
                $complateSmp += $tmp[$v]['complate'];
            }
            else
            {
                $listData[$v] = $data;
            }
        }
        $reData['list'] = $listData;
        //汇总
        $data = array('ask'=>0, 'imp'=>0, 'click'=>0, 'income'=>0, 'imprate'=>0, 'clickrate'=>0, 'coins'=>0, 'complate'=>0);
        $data['ask'] = $askSum;
        $data['imp'] = $impSum;
        $data['click'] = $clickSmp;
        $data['coins'] = $coinsSmp;
        $data['complate'] = $complateSmp;
        $data['income'] = formatmoney($incomeSmp);
        if($data['ask']>0)
        {
            $data['imprate'] = number_format($data['imp']*100/$data['ask'],2);
        }
        if($data['imp']>0)
        {
            $data['clickrate'] = number_format($data['click']*100/$data['imp'],2);
        }
        $reData['sum'] = $data;
        return $reData;
    }

    function getAppGraphReport2($ids, $sDate, $eDate, $pkAdId = 'pkCanalId', $adform, $gpkAdId = 'pkCanalId', $appid = '')
    {
        $sql = "select a.*
                from app_gatherlog as a left join app_info as b on a.appid=b.chance_appid
                where b.appid=$ids and adform=$adform and logdate between '$sDate' and '$eDate'";

        $data = $this->db->query($sql)->result_array();
        if(empty($data))
        {
            return false;
        }

        $listData = array();
        $sumData = array('ask'=>0, 'imp'=>0, 'click'=>0, 'income'=>'0.00', 'imprate'=>0, 'clickrate'=>0, 'coins'=>0, 'complate'=>0);

        $formatDate = mktimes($sDate,$eDate,true,true);
        foreach ($formatDate as $v) {
            $listData[$v] = array('ask' => 0, 'imp' => 0, 'click' => 0, 'income' => '0.00', 'imprate' => 0, 'clickrate' => 0, 'coins' => 0, 'complate' => 0);
        }

        foreach($data as $row){
            $listData[$row['logdate']]['ask'] = $row['ask'];
            $listData[$row['logdate']]['imp'] = $row['imp'];
            $listData[$row['logdate']]['click'] = $row['click'];
            $listData[$row['logdate']]['income'] = formatmoney($row['income']*1000000, 'get', 2, '.');
            $listData[$row['logdate']]['imprate'] = $row['imprate'];
            $listData[$row['logdate']]['clickrate'] = $row['clickrate'];
            $listData[$row['logdate']]['coins'] = $row['coins'];
            $listData[$row['logdate']]['complate'] = $row['complate'];

            $sumData['ask'] += $row['ask'];
            $sumData['imp'] += $row['imp'];
            $sumData['click'] += $row['click'];
            $sumData['income'] += $row['income'];
            $sumData['coins'] += $row['coins'];
            $sumData['complate'] += $row['complate'];
        }

        $reData = array('list'=>$listData, 'sum'=>$sumData);
        return $reData;
    }

 	function postAppAskReport($appIDs, $sDate, $eDate)
    {
        $asklist = array();
    	$tables = array("d_app_banner", "d_app_pop", "d_app_moregame", "d_app_wall", "d_app_feeds");
    	$condition = array();
        $condition['server']       = "ads_union";
        foreach($tables as $t){
	        $condition['t'] = $t;
	        $condition['pkCanalId'] = 'in|'.$appIDs;
	        $condition['sd'] = 'ge|'.strtotime($sDate);
	        $condition['ed'] = 'le|'.strtotime($eDate);
	        if($t == "d_app_pop")
	        {
	            $condition['s']  = "pkCanalId,pkAdType,pkDay,sum(impFail+impSucc) ask";
	        }
	        else
	        {
	            $condition['s']  = "pkCanalId,pkAdType,pkDay,sum(askSucc) ask";
	        }
	        $condition['g']  = "pkCanalId,pkAdType,pkDay";
	        $config = &get_config();
    		$url = $config['data_api'];
	    	$postresult =  curlPost($url, $condition);
	    	$result = json_decode($postresult,true);
	    	$retdata = isset($result['data'])?$result['data']:array();
	    	if($retdata){
		    	foreach($retdata as $v){
		    		$asklist[$v['pkCanalId']][$v['pkAdType']][$v['pkDay']] = $v['ask'];
		    	}
	    	}
        }
        return $asklist;
    }
    
    
    /**
     * 获取请求数据
     * Enter description here ...
     * @param $ids
     * @param $sDate
     * @param $eDate
     * @param $adform
     * @param $pkAdId
     * @param $gpkAdId
     */
    function getAppAskReport($ids, $sDate, $eDate, $pkAdId = 'pkCanalId', $adform, $gpkAdId = 'pkCanalId', $appid='')
    {
        switch ($adform)
        {
            case 1:
                $t = "d_app_banner";
                break;
            case 2:
                $t = "d_app_pop";
                break;
            case 3:
                $t = "d_app_moregame";
                break;
            case 4:
                $t = "d_app_wall";
                break;
            case 5:
                $t = "d_app_banner";
                break;
            case 20:
            	$t= "d_app_feeds";
            	break;
            default:
                return false;
                break;
        }
        $condition = array();
        $condition['server']       = "server=ads_union";
        $condition['t']            = "t={$t}";
        if(!empty($appid))
        {
            $condition['pkCanalId'] = "pkCanalId='".$appid."'";
        }
        $condition[$pkAdId] = "{$pkAdId}='".$ids."'";
        $condition['spkDay'] = 'sd=ge|'.strtotime($sDate);
        $condition['epkDay'] = 'ed=le|'.strtotime($eDate);
        if($adform == 2)
        {
            $condition['s']  = "s=".urlencode("{$gpkAdId},sum(impFail+impSucc) ask");
        }
        else
        {
            $condition['s']  = "s=".urlencode("{$gpkAdId},sum(askSucc) ask");
        }
        $condition['g']  = "g={$gpkAdId}";
        $list = $this->getDataSourceOnce($condition);
        $tmp = array();
        foreach($list as $val){
            $tmp[$val[$gpkAdId]] = $val;
        }
        return $tmp;
    }

    function getDataSourceOnce($condition){
        $data = getDataApi($condition,true);
        //$data = getDataApi($condition,true,true);print_r($data);
        if(!empty($data['data']))
        return $data['data'];
        else
        return array();
    }
    

    /**
     * 
     * 获取视频积分墙非激励型 应用数据
     * @param $ids		查询的所有id序列
     * @param $sDate	查询的开始日期
     * @param $eDate	查询的结束日期
     * @param $pkAdId	主键
     * @param $adform	广告类型
     * @param $gpkAdId	groupby 应用的ID 
     * @param $appid
     */
    function getAppGraphReportVideo($ids, $sDate, $eDate, $pkAdId = 'pkCanalId', $adform, $gpkAdId = 'pkCanalId', $appid = '')
    {
        $t='t_day_video_uniq_all';
        $condition = array();
        $condition['server']       = "server=ads_union";
        $condition['t']            = "t={$t}";
        if(!empty($appid))
        {
            $condition['pkCanalId'] = "pkCanalId='".$appid."'";
        }
        $condition[$pkAdId] = "{$pkAdId}='".$ids."'";
        $condition['pkAdType'] = "pkAdType=in|".$this->adFormRelation($adform);
        $condition['spkDay'] = 'sd=ge|'.strtotime($sDate);
        $condition['epkDay'] = 'ed=le|'.strtotime($eDate);
        $condition['s']  = "s=".urlencode("{$pkAdId},{$gpkAdId},sum(uidClick) uidclick,sum(startDownload) startdownload,sum(downloadFailed) downloadfailed,sum(downloadSuccess) downloadsuccess,sum(startPlay) startplay,sum(playFailed) playfailed,sum(playSuccess) playsuccess, sum(clickVideoPic) clickvideopic,sum(income) income");
        $condition['g']  = "g={$gpkAdId}";
        $list = $this->getDataSourceOnce($condition);
        //获取请求数据
        $tmp = array();
        foreach($list as $val){
            $tmp[$val[$gpkAdId]] = $val;
        }
        $listData = array();
        $uidclickSum = 0;
        $startdownloadSum = 0;
        $downloadfailedSum = 0;
        $downloadsuccessSum = 0;
        $startplaySum = 0;
        $playfailedSum = 0;
        $playsuccessSum = 0;
        $clickvideopicSum = 0;
        $incomeSum = 0;
        //获取分列表和总计
        $formatDate = mktimes($sDate,$eDate,true,true);
        foreach ($formatDate as $v)
        {
            $data = array('uidclick'=>0,
             'startdownload'=>0, 
             'downloadfailed'=>0, 
             'downloadsuccess'=>0,
             'startplay'=>'0', 
             'playfailed'=>'0', 
             'playsuccess'=>0, 
             'clickvideopic'=>0,
             'income'=>0);
            if(isset($tmp[$v]))
            {
                $data['uidclick'] = $tmp[$v]['uidclick'];
                $data['startdownload'] = $tmp[$v]['startdownload'];
                $data['downloadfailed'] = $tmp[$v]['downloadfailed'];
                $data['downloadsuccess'] = $tmp[$v]['downloadsuccess'];
                $data['startplay'] = $tmp[$v]['startplay'];
                $data['playfailed'] = $tmp[$v]['playfailed'];
                $data['playsuccess'] = $tmp[$v]['playsuccess'];
                $data['clickvideopic'] = $tmp[$v]['clickvideopic'];
                $data['income'] = formatmoney($tmp[$v]['income'],'get',2,'.');
                $listData[$v] = $data;
                $uidclickSum += $data['uidclick'];
                $startdownloadSum += $tmp[$v]['startdownload'];
                $downloadfailedSum += $tmp[$v]['downloadfailed'];
                $downloadsuccessSum += $tmp[$v]['downloadsuccess'];
                $startplaySum += $tmp[$v]['startplay'];
                $playfailedSum += $tmp[$v]['playfailed'];
                $playsuccessSum += $tmp[$v]['playsuccess'];
                $clickvideopicSum += $tmp[$v]['clickvideopic'];
                $incomeSum += $tmp[$v]['income'];
            }
            else
            {
                $listData[$v] = $data;
            }
        }
        $reData['list'] = $listData;
        //汇总
        $data = array('uidclick'=>0, 'startdownload'=>0, 'downloadfailed'=>0, 'downloadsuccess'=>0, 'startplay'=>0, 'playfailed'=>0, 'playsuccess'=>0, 'clickvideopic'=>0,'income'=>0);
        $data['uidclick'] = $uidclickSum;
        $data['startdownload'] = $startdownloadSum;
        $data['downloadfailed'] = $downloadfailedSum;
        $data['downloadsuccess'] = $downloadsuccessSum;
        $data['startplay'] = $startplaySum;
        $data['playfailed'] = $playfailedSum;
        $data['playsuccess'] = $playsuccessSum;
        $data['clickvideopic'] = $clickvideopicSum;
        $data['income'] = formatmoney($incomeSum);
        $reData['sum'] = $data;
        return $reData;
      
    }
}

<?php
abstract class ReportBase extends MY_Model{
	/**
	 * 时间参数处理。
	 * @return type
	 */
	final function getStarttimeAndEndtime(){
		if(!empty($this->date)){
			return $this->date;
		}
		$sdate = $this->input->post('date');
		$endtime = date("Y-m-d",mktime(0,0,0,date("m"),date("d")-1,date("Y")));
		$stime   = date("Y-m-d",mktime(0,0,0,date("m"),date("d")-7,date("Y")));
		if(!empty($sdate))
		{
			$tmp     = explode("~", $sdate);
			$stime   = $tmp[0];
			$endtime = $tmp[1];
		}
		$this->date['sdate'] = $stime."~".$endtime;
		$this->date['starttime'] = $stime;
		$this->date['endtime'] = $endtime;
		return $this->date;
	}

	final function getDataSource($condition){
		$data = getDataApi($condition,true);
		if(!empty($data['data']))
			return $data['data'];
		else
			return array();
	}
}
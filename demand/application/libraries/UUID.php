<?php
class UUIDSystem
{
	static function currentTimeMillis()
	{
		list($usec, $sec) = explode(" ",microtime());
		return $sec.substr($usec, 2, 3);
	}

}


class UUIDNetAddress
{
	var $Name = 'localhost';
	var $IP = '127.0.0.1';

    function getLocalHost() // static
	{
		$address = new UUIDNetAddress();
		if( isset( $_SERVER["SERVER_NAME"] ) ){
			$address->Name = $_SERVER["SERVER_NAME"] . rand(0,65535);
		}

		if( isset( $_SERVER['SERVER_ADDR'] ) )
			$address->IP = $_SERVER["SERVER_ADDR"];
		if( null == $address->IP || "" == $address->IP)
			$address->IP = "127.0.0.1";
		return $address;
	}

    function toString()
	{
		return strtolower($this->Name.'/'.$this->IP);
	}

}

class UUIDRandom
{
    static function nextLong()
	{
		$tmp = rand(0,1)?'-':'';
		return $tmp.rand(1000, 9999).rand(1000, 9999).rand(1000, 9999).rand(100, 999).rand(100, 999);
	}
}

class UUID
{

    static $valueBeforeMD5 = null;
    static $valueAfterMD5 = null;

    static public function getUUID()
	{
		$address = UUIDNetAddress::getLocalHost();
        self::$valueBeforeMD5 = $address->toString() . ':' . UUIDSystem::currentTimeMillis() . ':' . UUIDRandom::nextLong();
        self::$valueAfterMD5 = md5(self::$valueBeforeMD5);
		$raw = strtoupper(self::$valueAfterMD5);
		return strtoupper( substr($raw,6,6).'-'.substr($raw,12,4).'-'.substr($raw,16,4).'-'.substr($raw,23) );
		//return $this->toString();
	}

    static public function toString()
	{
		$raw = strtoupper(self::$valueAfterMD5);
		return strtoupper( substr($raw,6,6).'-'.substr($raw,12,4).'-'.substr($raw,16,4).'-'.substr($raw,23) );
	}
}

//if( 1 == count($argv) ){
//	$obj = new UUID();
//	echo $obj->getUUID() . "\n";
//}
?>

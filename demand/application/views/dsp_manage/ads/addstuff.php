<!--  主体部分 [ -->
<div class="inner_container" id="innerContainer">
	<?php $this->load->view('manage/inc/left')?>
	<section>
		<div id="content" class="content">
			<div class="right_content" id="rightContent">
				<div class="jumbotron jumbotron_no_shadow add_app_box">
					<header class="title">
						<h3><strong  class="square_box"></strong><span>添加广告创意</span></h3>
					</header>
					<form id="frm" name="frm" method="post" action="/dsp_manage/ads/doaddstuff/">
						<div class="wrap">
							<div class="form-group">
								<span class="fl form-label"><i class="require_item">*</i>创意名称：</span>
								<span class="fl"><input class="form-control form-control-small" type="text" name="adstuffname" id="adstuffname" maxlength="50"></span>
								<span class="fl alert alert-small alert-warning" id="adstuffnameerr" style="display: none"><i class="icon error_icon"></i><span id="adstuffnamemsg"></span></span>
							</div>
							<div class="form-group">
								<span class="fl form-label"><i class="require_item">*</i>目标地址：</span>
								<span class="fl"><input class="form-control form-control-small" type="text" name="target" id="target" maxlength="50"></span>
								<span class="fl alert alert-small alert-warning" id="targeterr" style="display: none"><i class="icon error_icon"></i><span id="targetmsg"></span></span>
							</div>
							<div class="form-group">
								<span class="fl form-label"><i class="require_item">*</i>广告主监控回调地址：</span>
								<span class="fl"><input class="form-control form-control-small" type="text" name="callbackurl" id="callbackurl" maxlength="50"></span>
								<span class="fl alert alert-small alert-warning" id="callbackurlerr" style="display: none"><i class="icon error_icon"></i><span id="callbackurlmsg"></span></span>
							</div>
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>创意类型：</span>
								<div class="platform fl">
									<select name="stufftype" class="form-control">
										<option value="">请选择</option>
										<?php foreach (get_stufftype_array() as $key => $item) :?>
											<option value="<?php echo $key;?>"><?php echo $item;?></option>
										<?php endforeach;?>
									</select>
								</div>
								<span class="fl alert alert-small alert-warning" id="adformerr" style="display: none;"><i class="icon error_icon"></i><span id="adformmsg">请选择广告类型！</span></span>
							</div>
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>预下载广告：</span>
								<div class="platform fl">
									<label class="radio_style"><input type="radio" name="ispredown" value="1" checked>是</label>
									<label class="radio_style"><input type="radio" name="ispredown" value="0">否</label>
								</div>
								<span class="fl alert alert-small alert-warning" id="ispredownerr" style="display: none;"><i class="icon error_icon"></i><span id="ispredownmsg">请选择系统类型！</span></span>
							</div>
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>追踪idfa：</span>
								<div class="platform fl">
									<label class="radio_style"><input type="radio" name="istrackidfa" value="1" checked>是</label>
									<label class="radio_style"><input type="radio" name="istrackidfa" value="0">否</label>
								</div>
								<span class="fl alert alert-small alert-warning" id="istrackidfaerr" style="display: none;"><i class="icon error_icon"></i><span id="istrackidfamsg">请选择系统类型！</span></span>
							</div>
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>在api中投放：</span>
								<div class="platform fl">
									<label class="radio_style"><input type="radio" name="isapi" value="1" checked>是</label>
									<label class="radio_style"><input type="radio" name="isapi" value="0">否</label>
								</div>
								<span class="fl alert alert-small alert-warning" id="isapierr" style="display: none;"><i class="icon error_icon"></i><span id="isapimsg">请选择系统类型！</span></span>
							</div>
							<div class="form-group">
								<span class="fl form-label"><i class="require_item">*</i>dsp竞价比例区间：</span>
								<span class="fl">
									<input class="form-control form-control-small" type="text" name="lowerlimit" id="lowerlimit" maxlength="50">~
									<input class="form-control form-control-small" type="text" name="upperlimit" id="upperlimit" maxlength="50">
								</span>
								<span class="fl alert alert-small alert-warning" id="targeterr" style="display: none"><i class="icon error_icon"></i><span id="targetmsg"></span></span>
							</div>
							<div class="form-group">
								<span class="fl form-label"><i class="require_item">*</i>包名称：</span>
								<span class="fl"><input class="form-control form-control-small" type="text" name="packagename" id="packagename" maxlength="50"></span>
								<span class="fl alert alert-small alert-warning" id="packagenameerr" style="display: none"><i class="icon error_icon"></i><span id="packagenamemsg"></span></span>
							</div>
							<div class="form-group">
								<span class="fl form-label"><i class="require_item">*</i>versioncode应用版本编号：</span>
								<span class="fl"><input class="form-control form-control-small" type="text" name="appversioncode" id="appversioncode" maxlength="50"></span>
								<span class="fl alert alert-small alert-warning" id="appversioncodeerr" style="display: none"><i class="icon error_icon"></i><span id="appversioncodemsg"></span></span>
							</div>
							<div class="form-group">
								<span class="fl form-label"><i class="require_item">*</i>包大小，以K为单位：</span>
								<span class="fl"><input class="form-control form-control-small" type="text" name="appsize" id="appsize" maxlength="50"></span>
								<span class="fl alert alert-small alert-warning" id="appsizeerr" style="display: none"><i class="icon error_icon"></i><span id="appsizemsg"></span></span>
							</div>
							<div class="form-group">
								<span class="fl form-label"><i class="require_item">*</i>vsersionname应用版本名称：</span>
								<span class="fl"><input class="form-control form-control-small" type="text" name="appversionname" id="appversionname" maxlength="50"></span>
								<span class="fl alert alert-small alert-warning" id="appversionnameerr" style="display: none"><i class="icon error_icon"></i><span id="appversionnamemsg"></span></span>
							</div>
							<div class="form-group">
								<span class="fl form-label"><i class="require_item">*</i>图片尺寸640*100：</span>
								<span class="fl">
									<input type="file" class="form-control">
									<input type="hidden" name="img[]" value="">
									<input type="hidden" name="width[]" value="640">
									<input type="hidden" name="height[]" value="100">
								</span>
							</div>
							<div class="form-group">
								<span class="fl form-label"><i class="require_item">*</i>图片尺寸320*50：</span>
								<span class="fl">
									<input type="file" class="form-control">
									<input type="hidden" name="img[]" value="">
									<input type="hidden" name="width[]" value="320">
									<input type="hidden" name="height[]" value="50">
								</span>
							</div>
							<div class="form-group">
								<span class="fl form-label"><i class="require_item">*</i>图片尺寸1280*200：</span>
								<span class="fl">
									<input type="file" class="form-control">
									<input type="hidden" name="img[]" value="">
									<input type="hidden" name="width[]" value="1280">
									<input type="hidden" name="height[]" value="200">
								</span>
							</div>
							<div class="form-group">
								<span class="fl form-label"><i class="require_item">*</i>图片尺寸600*500：</span>
								<span class="fl">
									<input type="file" class="form-control">
									<input type="hidden" name="img[]" value="">
									<input type="hidden" name="width[]" value="600">
									<input type="hidden" name="height[]" value="500">
								</span>
							</div>
							<div class="form-group">
								<span class="fl form-label"><i class="require_item">*</i>图片尺寸500*600：</span>
								<span class="fl">
									<input type="file" class="form-control">
									<input type="hidden" name="img[]" value="">
									<input type="hidden" name="width[]" value="500">
									<input type="hidden" name="height[]" value="600">
								</span>
							</div>
							<div class="form-group">
								<span class="fl form-label"><i class="require_item">*</i>图片尺寸900*600：</span>
								<span class="fl">
									<input type="file" class="form-control">
									<input type="hidden" name="img[]" value="">
									<input type="hidden" name="width[]" value="900">
									<input type="hidden" name="height[]" value="600">
								</span>
							</div>
							<div class="form-group">
								<span class="fl form-label"><i class="require_item">*</i>图片尺寸600*900：</span>
								<span class="fl">
									<input type="file" class="form-control">
									<input type="hidden" name="img[]" value="">
									<input type="hidden" name="width[]" value="600">
									<input type="hidden" name="height[]" value="900">
								</span>
							</div>
							<div class="form-group">
								<span class="fl form-label"><i class="require_item">*</i>图片尺寸1280*720：</span>
								<span class="fl">
									<input type="file" class="form-control">
									<input type="hidden" name="img[]" value="">
									<input type="hidden" name="width[]" value="1280">
									<input type="hidden" name="height[]" value="720">
								</span>
							</div>
							<div class="form-group">
								<span class="fl form-label"><i class="require_item">*</i>图片尺寸720*1280：</span>
								<span class="fl">
									<input type="file" class="form-control">
									<input type="hidden" name="img[]" value="">
									<input type="hidden" name="width[]" value="720">
									<input type="hidden" name="height[]" value="1280">
								</span>
							</div>
						</div>
						<div class="function_btn">
							<input type="hidden" name="adgroupid" value="<?php echo $adgroupid;?>">
							<button type="submit" id="njh_add_app" class="btn btn-primary btn-primary-noboder">提交</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
<?php $this->load->view("manage/inc/footer");?>
<script>
	$('input[type=file]').change(function(){
		var $this = $(this);
		var files = $this.prop('files');

		var data = new FormData();
		data.append('img', files[0]);
		data.append('width', $this.attr('data-width'));
		data.append('height',  $this.attr('data-height'));
		data.append('img_type', 1);

		$.ajax({
			url: '/api/img/upload',
			type: 'POST',
			data: data,
			cache: false,
			processData: false,
			contentType: false,
			dataType: "json",
			success: function(data){

				if (data.status == 0) {
					var html = '<a href="'+data.data.img_url+'" target="_blank"><img width="200px" src="'+data.data.img_url+'"></a>';
					console.log(html,$(this));
					$this.next().val(data.data.img_url);
					$this.after(html);
				} else {
					alert(data.error);
				}

			}
		});
	});
	$("#frm").submit(function(e){
		e.preventDefault();
		$("#njh_add_app").attr("disabled", 'disabled');
		$.post("/dsp_manage/ads/doaddstuff/", $('#frm').serialize(), function(data){
			if(data && data['status'] == 0){
				if(data['data'] != '')
				{
					$("#"+data['data'][0]).html(data['data'][1]);
				}
				$("#"+data['info']).show();
				$("#njh_add_app").removeAttr("disabled");
				return false;
			}else{
				alert('保存成功！');
				location.href="/dsp_manage/ads/stufflist?adgroupid=<?php echo $adgroupid;?>";
			}
		}, 'json');
	});
</script>
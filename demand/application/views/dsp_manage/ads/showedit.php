<!--  主体部分 [ -->
<div class="inner_container" id="innerContainer">
	<?php $this->load->view('manage/inc/left')?>
	<section>
		<div id="content" class="content">
			<div class="right_content" id="rightContent">
				<div class="jumbotron jumbotron_no_shadow add_app_box">
					<header class="title">
						<h3><strong  class="square_box"></strong><span>修改广告</span></h3>
					</header>
					<form id="frm" name="frm" method="post">
						<div class="wrap">
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>推广计划：</span>
								<div class="platform fl">
									<select name="campaignid" class="form-control">
										<option value="">请选择</option>
										<?php foreach ($campaign as $item) :?>
										<option value="<?php echo $item['campaignid'];?>" <?php echo ($ads['campaignid']==$item['campaignid'])?' selected':'';?>><?php echo $item['campaignname']."(ID:".$item['campaignid'].")";?></option>
										<?php endforeach;?>
									</select>
								</div>
								<span class="fl alert alert-small alert-warning" id="campaigniderr" style="display: none;"><i class="icon error_icon"></i><span id="campaignidmsg">请选择推广计划！</span></span>
							</div>
							<div class="form-group">
								<span class="fl form-label"><i class="require_item">*</i>广告名称：</span>
								<span class="fl"><input class="form-control form-control-small" type="text" name="adgroupname" id="adgroupname" maxlength="50" value="<?php echo $ads['adgroupname'];?>"></span>
								<span class="fl alert alert-small alert-warning" id="adgroupnameerr" style="display: none"><i class="icon error_icon"></i><span id="adgroupnamemsg"></span></span>
							</div>
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>广告类型：</span>
								<div class="platform fl">
									<select name="adform" class="form-control">
										<option value="">请选择</option>
										<?php foreach (get_ads_adform_array() as $key => $item) :?>
											<option value="<?php echo $key;?>"<?php echo ($ads['adform']==$key)?' selected':'';?>><?php echo $item;?></option>
										<?php endforeach;?>
									</select>
								</div>
								<span class="fl alert alert-small alert-warning" id="adformerr" style="display: none;"><i class="icon error_icon"></i><span id="adformmsg">请选择广告类型！</span></span>
							</div>
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>点击后效果：</span>
								<div class="platform fl">
									<select name="effecttype" class="form-control">
										<option value="">请选择</option>
										<?php foreach (get_ads_effecttype_array() as $key => $item) :?>
											<option value="<?php echo $key;?>"<?php echo ($ads['effecttype']==$key)?' selected':'';?>><?php echo $item;?></option>
										<?php endforeach;?>
									</select>
								</div>
								<span class="fl alert alert-small alert-warning" id="adformerr" style="display: none;"><i class="icon error_icon"></i><span id="adformmsg">请选择广告类型！</span></span>
							</div>
							<div class="form-group">
								<span class="fl form-label"><i class="require_item">*</i>单价：</span>
								<span class="fl"><input class="form-control form-control-small" type="text" name="price" id="price" maxlength="50" value="<?php echo formatmoney($ads['price'],'get',2,'.');?>">元。</span>
								<span class="fl alert alert-small alert-warning" id="priceerr" style="display: none"><i class="icon error_icon"></i><span id="pricemsg"></span></span>
							</div>
							<div class="form-group">
								<span class="fl form-label"><i class="require_item">*</i>组日预算：</span>
								<span class="fl"><input class="form-control form-control-small" type="text" name="daybudget" id="daybudget" maxlength="50" value="<?php echo formatmoney($ads['daybudget'],'get',2,'.');?>">元/天,请输入大于等于100的整数。</span>
								<span class="fl alert alert-small alert-warning" id="daybudgeterr" style="display: none"><i class="icon error_icon"></i><span id="daybudgetmsg"></span></span>
							</div>
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>频次：</span>
								<div class="platform fl">
									<label class="radio_style"><input type="radio" name="frequencyset" value="0"<?php echo ($ads['frequencyset']==='0')?' checked':'';?>>不限</label>
									<label class="radio_style"><input type="radio" name="frequencyset" value="1"<?php echo ($ads['frequencyset']==1)?' checked':'';?>>展现</label>
									<label class="radio_style"><input type="radio" name="frequencyset" value="2"<?php echo ($ads['frequencyset']==2)?' checked':'';?>>点击</label>
								</div>
								<span class="fl alert alert-small alert-warning" id="frequencyseterr" style="display: none;"><i class="icon error_icon"></i><span id="frequencysetmsg">请选择系统类型！</span></span>
							</div>
							<div class="form-group">
								<span class="fl form-label"><i class="require_item">*</i>累计频次：</span>
								<span class="fl"><input class="form-control form-control-small" type="text" name="totalfrequency" id="totalfrequency" maxlength="50" value="<?php echo $ads['totalfrequency'];?>"></span>
								<span class="fl alert alert-small alert-warning" id="totalfrequencyerr" style="display: none"><i class="icon error_icon"></i><span id="totalfrequencymsg"></span></span>
							</div>
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>频次控制类型：</span>
								<div class="platform fl">
									<label class="radio_style"><input type="radio" name="frequencytype" value="1"<?php echo ($ads['frequencytype']==1)?' checked':'';?>>广告组</label>
									<label class="radio_style"><input type="radio" name="frequencytype" value="2"<?php echo ($ads['frequencytype']==2)?' checked':'';?>>广告创意</label>
								</div>
								<span class="fl alert alert-small alert-warning" id="frequencyseterr" style="display: none;"><i class="icon error_icon"></i><span id="frequencysetmsg">请选择系统类型！</span></span>
							</div>
							<div class="form-group">
								<span class="fl form-label"><i class="require_item">*</i>点击频次：</span>
								<span class="fl"><input class="form-control form-control-small" type="text" name="clickfrequency" id="clickfrequency" maxlength="50" value="<?php echo $ads['clickfrequency'];?>"></span>
								<span class="fl alert alert-small alert-warning" id="clickfrequencyerr" style="display: none"><i class="icon error_icon"></i><span id="clickfrequencymsg"></span></span>
							</div>
							<div class="form-group">
								<span class="fl form-label"><i class="require_item">*</i>展现上限：</span>
								<span class="fl"><input class="form-control form-control-small" type="text" name="showfrequency" id="showfrequency" maxlength="50" value="<?php echo $ads['showfrequency'];?>"></span>
								<span class="fl alert alert-small alert-warning" id="showfrequencyerr" style="display: none"><i class="icon error_icon"></i><span id="showfrequencymsg"></span></span>
							</div>
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>展现次数周期：</span>
								<div class="platform fl">
									<label class="radio_style"><input type="radio" name="frequencyperiod" value="1"<?php echo ($ads['frequencyperiod']==1)?' checked':'';?>>日</label>
									<label class="radio_style"><input type="radio" name="frequencyperiod" value="2"<?php echo ($ads['frequencyperiod']==2)?' checked':'';?>>周</label>
								</div>
								<span class="fl alert alert-small alert-warning" id="frequencyperioderr" style="display: none;"><i class="icon error_icon"></i><span id="frequencyperiodmsg">请选择系统类型！</span></span>
							</div>
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>投放方式：</span>
								<div class="platform fl">
									<label class="radio_style"><input type="radio" name="pattern" value="1"<?php echo ($ads['pattern']==1)?' checked':'';?>>匀速投放</label>
									<label class="radio_style"><input type="radio" name="pattern" value="2"<?php echo ($ads['pattern']==2)?' checked':'';?>>加速投放</label>
								</div>
								<span class="fl alert alert-small alert-warning" id="patternerr" style="display: none;"><i class="icon error_icon"></i><span id="patternmsg">请选择系统类型！</span></span>
							</div>
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>创意优化方式：</span>
								<div class="platform fl">
									<label class="radio_style"><input type="radio" name="optimizetype" value="1"<?php echo ($ads['optimizetype']==1)?' checked':'';?>>轮播展示</label>
									<label class="radio_style"><input type="radio" name="optimizetype" value="2"<?php echo ($ads['optimizetype']==2)?' checked':'';?>>点击优化</label>
								</div>
								<span class="fl alert alert-small alert-warning" id="optimizetypeerr" style="display: none;"><i class="icon error_icon"></i><span id="optimizetypemsg">请选择系统类型！</span></span>
							</div>
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>计费方式：</span>
								<div class="platform fl">
									<label class="radio_style"><input type="radio" name="chargemode" value="2" checked>CPM</label>
								</div>
								<span class="fl alert alert-small alert-warning" id="chargemodeerr" style="display: none;"><i class="icon error_icon"></i><span id="chargemodemsg">请选择系统类型！</span></span>
							</div>
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>投放时段：</span>
								<div class="platform fl">
									<label class="radio_style"><input type="radio" name="scheduletype" value="0"<?php echo ($ads['scheduletype']==='0')?' checked':'';?>>全时段投放</label>
									<label class="radio_style"><input type="radio" name="scheduletype" value="1"<?php echo ($ads['scheduletype']==1)?' checked':'';?>>分时段投放</label>
								</div>
								<span class="fl alert alert-small alert-warning" id="scheduletypeerr" style="display: none;"><i class="icon error_icon"></i><span id="scheduletypemsg">请选择系统类型！</span></span>
							</div>
							<div class="form-group">
								<span class="fl form-label"><i class="require_item">*</i>投放周期（只针对广告墙有效）：</span>
								<span class="fl">
									<input class="form-control form-control-small" type="text" name="starttime" id="starttime" maxlength="20" value="<?php echo date("Y-m-d",$ads['starttime']);?>">至
									<input class="form-control form-control-small" type="text" name="endtime" id="endtime" maxlength="20" value="<?php echo date("Y-m-d",$ads['endtime']);?>">
								</span>
							</div>
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>展示频次控制开关：</span>
								<div class="platform fl">
									<label class="radio_style"><input type="radio" name="showswitch" value="0"<?php echo ($ads['showswitch']==='0')?' checked':'';?>>关</label>
									<label class="radio_style"><input type="radio" name="showswitch" value="1"<?php echo ($ads['showswitch']==1)?' checked':'';?>>开</label>
								</div>
								<span class="fl alert alert-small alert-warning" id="showswitcherr" style="display: none;"><i class="icon error_icon"></i><span id="showswitchmsg">请选择系统类型！</span></span>
							</div>
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>点击频次控制开关：</span>
								<div class="platform fl">
									<label class="radio_style"><input type="radio" name="clickswitch" value="0"<?php echo ($ads['clickswitch']==='0')?' checked':'';?>>关</label>
									<label class="radio_style"><input type="radio" name="clickswitch" value="1"<?php echo ($ads['clickswitch']==1)?' checked':'';?>>开</label>
								</div>
								<span class="fl alert alert-small alert-warning" id="clickswitcherr" style="display: none;"><i class="icon error_icon"></i><span id="clickswitchmsg">请选择系统类型！</span></span>
							</div>
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>播放频次控制开关：</span>
								<div class="platform fl">
									<label class="radio_style"><input type="radio" name="playswitch" value="0"<?php echo ($ads['playswitch']==='0')?' checked':'';?>>关</label>
									<label class="radio_style"><input type="radio" name="playswitch" value="1"<?php echo ($ads['playswitch']==1)?' checked':'';?>>开</label>
								</div>
								<span class="fl alert alert-small alert-warning" id="playswitcherr" style="display: none;"><i class="icon error_icon"></i><span id="playswitchmsg">请选择系统类型！</span></span>
							</div>
							<div class="form-group">
								<span class="fl form-label"><i class="require_item">*</i>播放上限：</span>
								<span class="fl"><input class="form-control form-control-small" type="text" name="playfrequency" id="playfrequency" maxlength="50" value="<?php echo $ads['playfrequency'];?>"></span>
								<span class="fl alert alert-small alert-warning" id="playfrequencyerr" style="display: none"><i class="icon error_icon"></i><span id="playfrequencymsg"></span></span>
							</div>
						</div>
						<div class="function_btn">
							<input type="hidden" name="adgroupid" value="<?php echo $ads['adgroupid'];?>">
							<button type="submit" id="njh_add_app" class="btn btn-primary btn-primary-noboder">提交</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
<?php $this->load->view("manage/inc/footer");?>
<script>
	$("#frm").submit(function(e){
		e.preventDefault();
		$("#njh_add_app").attr("disabled", 'disabled');
		$.post("/dsp_manage/ads/doedit/", $('#frm').serialize(), function(data){
			if(data && data['status'] == 0){
				if(data['data'] != '')
				{
					$("#"+data['data'][0]).html(data['data'][1]);
				}
				$("#"+data['info']).show();
				$("#njh_add_app").removeAttr("disabled");
				return false;
			}else{
				alert('保存成功！');
				location.href="/dsp_manage/ads/showedit?adgroupid=<?php echo $ads['adgroupid'];?>";
			}
		}, 'json');
	});
</script>
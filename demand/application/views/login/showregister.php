<?php 
require_once "header.php";
?>
<div class="user_relative">
    <h2>新用户注册</h2>
    <div class="box">
        <div class="fl img_box img_register"></div>
        <div class="fr form_box jumbotron">
            <form method="post" action="/webindex/doregister">
                <div class="form-group">
                    <h3>注册成为CocosAds用户</h3>
                </div>
                <div class="form-group">
                    <input class="form-control"  size="50" type="text" name="username" placeholder="邮箱">
                    <span class="red"></span>
                </div>
                <div class="form-group">
                    <input id="pwd" class="form-control" type="password" size="50" name="password" placeholder="密码">
                    <span class="red"></span>
                </div>
                <div class="form-group">
                    <input id="pwd" class="form-control" size="50" name="passconf" type="password" placeholder="确认密码">
                    <span class="red"></span>
                </div>
                <div class="form-group">
                    <input class="form-control verify_code" size="10" name='captcha' type="text" placeholder="验证码">
                    <div class="verify_code_fix">
                        <img src="/captcha/index/70/30/4/<?php echo rand();?>" width="70" height="30" id='captchaimage'>
                        <label><a  href="javascript:document.getElementById('captchaimage').src='/captcha/index/70/30/4/'+Math.random();document.getElementById('captcha').focus();">看不清<br>换一张</a></label>
                        <span class="red"></span>
                    </div>
                </div>
                <div  class="form-group protocol">
                    <label><input type="checkbox"  checked='checked' onclick='dis(this.checked)'> 我同意<a href="javascript:;" id="protocol" title="请阅读">《相关服务和协议条款》</a></label>
                </div>
                <div class="form-group">
                    <input type="submit" value="创建账号" class="long-btn-nb long-btn-nb-primary">
                </div>
                <div class="form-group">
                    <span>已有账号，<a href="/" class="btn-link">登录</a></span>
                </div>
            </form>
        </div>
    </div>
</div>





<div class="light_box" id="protocol_modal" style="display: none;">
    <div id="tilScrollBar" style="height:450px; overflow-y: scroll;color:#666; padding:0 15px;">
        <h4>一、服务条款的确认和接纳</h4>
        <p>CocosAds涉及的产品、相关软件的所有权和运作权归上海拓畅信息技术有限公司所有，广告发布者和应用程序开发者，以下统称为用户，通过本网站注册程序，勾选"同意相关服务条款和协议“，点击”创建帐号“按钮，即表示用户与上海拓畅信息技术有限公司已达成协议，自愿接受本服务条款的所有内容和相关使用流程。</p>
        <h4>二、用户账户的安全性</h4>
        <p>1、用户一旦注册成功，成为CocosAds的用户，将获得一个用户名和密码，并有权利使用自己的用户名及密码随时登录CocosAds。<br />
            2、用户对用户名和密码的安全负全部责任，同时对以其用户名进行的所有活动和事件负全责。<br />
            3、用户不得以任何形式擅自转让或授权他人使用自己的CocosAds网站用户名。<br />
            4、如果用户泄漏了密码，应于48小时内与CocosAds客服联系进行挂失。超过该期限挂失，用户对密码泄露及造成的损失承担负全部责任。CocosAds确认挂失后会冻结该用户帐号直至帐号恢复。挂失前对密码泄露及造成的损失由用户承担负全部责任。</p>
        <br /><br />
        <h4>三、用户声明与保证</h4>
        <p>1、用户承诺其为完全民事行为能力的民事主体，具有达成交易履行其义务的能力。<br />
            2、用户有义务在注册时提供自己的真实资料，并保证诸如电子邮件地址、联系电话、联系地址、邮政编码等内容的有效性及安全性，保证CocosAds可以通过上述联系方式与自己进行联系。同时，用户也有义务在相关资料实际变更时及时更新有关注册资料。<br />
            3、用户通过CocosAds发布的移动应用程序和广告不得违反国家相关法律制度，包括但不限于如下信息：<br />
            (a) 反对宪法所确定的基本原则的；<br />
            (b) 危害国家安全，泄露国家秘密，颠覆国家政权，破坏国家统一的；<br />
            (c) 损害国家荣誉和利益的；<br />
            (d) 煽动民族仇恨、民族歧视，破坏民族团结的；<br />
            (e) 破坏国家宗教政策，宣扬邪教和封建迷信的；<br />
            (f) 散布谣言，扰乱社会秩序，破坏社会稳定的；<br />
            (g) 散布淫秽、色情、赌博、暴力、凶杀、恐怖或者教唆犯罪的；<br />
            (h) 侮辱或者诽谤他人，侵害他人合法权益的；<br />
            (i) 含有法律、行政法规禁止的其他内容的。<br />
            (j) 含有违反公序良俗的及言辞激烈、攻击他人、伤害他人感情的信息，夸大宣传、虚假信息等。<br />
            (j) 如有收费内容，未能明确告知手机用户。</p>
        <br /><br />
        <h4>四、 CocosAds提供的服务内容</h4>
        <p>1、CocosAds为用户间的移动应用程序广告交易提供广告自助交易管理系统和中介服务。<br />
            CocosAds务包括以下功能：<br />
            1）移动应用程序管理，应用程序开发者用户嵌入SDK时CocosAds提供技术支持；<br />
            2）广告活动的自助管理；<br />
            3）广告投放定向服务；<br />
            4）提供广告活动和应用程序的数据报表；<br />
            5）广告交易款项的支付中介；<br />
            6）广告发布者如需发布广告则需要先在CocosAds进行充值，每次充值的最低金额不能低于100元人民币。当广告开始投放后立即开始消耗用户在CocosAds的账户金额，如果用户设置的日预算金额大于账户余额时，当余额被消耗至零时CocosAds将会停止当前广告的投放。<br />
            7）广告发布者的广告费用结算，结算金额以CocosAds产生的有效统计数据为准。存在广告投放结束后用户仍有点击的情况，这种情况下的数据属于有效数据，当用户选择以广告点击方式结算时即会产生广告费用。<br />
            8）移动应用程序开发者（个人或机构）的广告收入结算。结算金额以CocosAds产生的有效统计数据为准，以CocosAds网站公布的开发者对广告市场营收的分成比例或者固定采购价格进行结算。双方的结算以人民币计算。支付时需要事先扣除相关手续费及税金。<br />
            9）关于提现流程及税收政策：<br />
            ●	每个月1日至10日为财务核算日，期间不能付款。<br />
            ●	11日～25日可提交提现申请，期间公司及个体工商户开发者需要将发票邮寄到上海拓畅信息技术有限公司，申请经审核通过后会完成付款。<br />
            ●	26日～30日（31日）上海拓畅信息技术有限公司审核通过后会完成付款。<br />
            ●  个人开发者无需主动提现，上海拓畅信息技术有限公司于每月26日～30日（31日）将上月收入自动打款到账户。<br />

            对于应用开发者来说，区分个人开发者账户、公司账户和个体工商户账户，对于公司账户和个体工商户账户<br />
            1. 需要提供贵公司的账户名称、开户行名称和银行账号；<br />
            2. 提现时需提供加盖贵公司发票专用章的地税服务业发票或地税局代开服务业发票，待CocosAds收到且审核无误后方可完成付款；<br />
            3. 发票抬头：上海拓畅信息技术有限公司； 发票科目：信息服务费；<br />
            4、CocosAds有权随时审核或者删除用户发布的涉嫌违法，侵害他人合法权益，或违反社会主义精神文明，或者CocosAds认为不妥当的广告或者应用程序信息</p><br /><br />
        <h4>五、服务的终止</h4>
        <p>在下列情况下，CocosAds有权终止向用户提供服务：<br />
            1、	在用户违反本服务协议相关规定时，CocosAds会通过注册邮箱及时告知用户在一定期限内更正其行为，若用户在该期限内不进行更正，CocosAds有权终止向该用户提供服务。如该用户再一次直接或间接或以他人名义注册为用户的，一经发现，CocosAds有权直接单方面终止向该用户提供服务；<br />
            2、 如CocosAds通过用户提供的信息与用户联系时，发现用户在注册时填写的电子邮箱已不存在或无法接收电子邮件的，经CocosAds以其它联系方式通知用户更改，而用户在七个工作日内仍未能提供新的电子邮箱地址的，CocosAds有权终止向该用户提供服务；<br />
            3、用户不得通过程序或人工方式进行刷量或作弊，若发现用户程序有作弊行为的， CocosAds将立即终止服务，并不再支付尚未支付的广告分成。<br />
            4、一旦CocosAds发现用户提供的数据或信息中含有虚假内容的，CocosAds有权随时终止向该用户提供服务；<br />
            5、本服务条款终止或更新时，用户明示不愿接受新的服务条款的；<br />
            6、其它CocosAds认为需终止服务的情况。<br />
            服务终止后，CocosAds没有义务为用户保留原账号中或与之相关的任何信息，或转发任何未曾阅读或发送的信息给用户或第三方。用户应负责自行储存相关信息。</p>
        <h4>六、 服务的变更、中断</h4>
        <p>1、用户理解，鉴于网络服务的特殊性，用户同意CocosAds会变更、中断部分或全部的网络服务，并删除（不再保存）用户在使用"服务"中提交的任何资料，而无需通知用户，也无需对任何用户或任何第三方承担任何责任。<br />
            2、用户理解，CocosAds需要定期或不定期地对提供网络服务的平台或相关的设备进行检修或者维护，如因此类情况或其他客观情况及第三方的行为而造成网络服务在合理时间内的中断，CocosAds无需为此承担任何责任。</p>
        <h4>七、 服务条款修改</h4>
        <p>1、CocosAds有权随时修改本服务条款的任何内容，一旦本服务条款的任何内容发生变动，CocosAds将会通过适当方式向用户提示修改内容。<br />
            2、如果不同意CocosAds对本服务条款所做的修改，用户有权停止使用网络服务。<br />
            3、如果用户继续使用网络服务，则视为用户接受CocosAds对本服务条款所做的修改。</p>
        <h4>八、 通告</h4>
        <p>所有发给用户的通告都可通过电子邮件或常规的信件传送。CocosAds会通过邮件服务发送消息给用户，告诉他们服务条款的修改、服务变更、或其它重要事项。</p>
        <h4>九、 免责与赔偿声明</h4>
        <p>1、若CocosAds已经明示其服务提供方式发生变更并提醒用户应当注意事项，用户未按要求操作所产生的一切后果由用户自行承担。<br />
            2、用户明确同意其使用CocosAds服务所存在的风险将完全由其自己承担；因其使用CocosAds服务而产生的一切后果也由其自己承担。<br />
            3、用户同意保障和维护CocosAds及其他用户的利益，由于用户登录网站内容违法、不真实、不正当、侵犯第三方合法权益，或用户违反本协议项下的任何条款而给CocosAds或任何其他第三人造成损失，用户同意承担由此造成的损害赔偿责任。</p>
        <h4>十、 隐私声明</h4>
        <p>1、适用范围：<br />
            (1)在用户注册CocosAds账户时，根据要求提供的个人注册信息；<br />
            (2)在用户使用CocosAds所提供的服务，或访问CocosAds网页时，CocosAds自动接收并记录的用户浏览器上的服务器数值，包括但不限于IP地址等数据及用户要求取用的网页记录；<br />
            2、信息使用：<br />
            (1) CocosAds不会向任何人出售或出借用户的个人信息，除非事先得到用户的许可。<br />
            (2) CocosAds亦不允许任何第三方以任何手段收集、编辑、出售或者无偿传播用户的个人信息。任何用户如从事上述活动，一经发现，CocosAds有权立即终止与该用户的服务协议，查封其账号。<br />
            (3)以向用户提供服务为目的，CocosAds可能通过使用用户的个人信息，向用户提供服务，包括但不限于向用户发出产品和服务信息，或者与CocosAds合作伙伴共享信息以便他们向用户发送有关其产品和服务的信息。<br />
            3、信息披露--用户的个人信息将在下述情况下部分或全部被披露：<br />
            (1)经用户同意，向第三方披露；<br />
            (2)根据法律的有关规定，或者行政或司法机构的要求，向第三方或者行政、司法机构披露；<br />
            (3)如果用户出现违反中国有关法律或者网站政策的情况，需要向第三方披露；<br />
            (4)为提供用户所要求的产品和服务，而必须和第三方分享用户的个人信息；<br />
            (5)其它CocosAds根据法律或者网站政策认为合适的披露；<br />
            (6)用户使用CocosAds服务时提供的银行账户信息，CocosAds将严格履行相关保密约定。<br />

            以上条款的解释权归所CocosAds最终所有。<br />
            在畅思的提示下，用户已认真阅读上述免责条款，并已充分了解并接受其内容。</p>
    </div>
</div>


<?php 
require_once "footer.html";
?>
<script type="text/javascript">
    function dis(ischecked)
    {
        if(ischecked==false) {
            $('input[type=submit]').attr('disabled',true);
        }else {
            $('input[type=submit]').attr('disabled',false);
        }
    }

    $(function(){
        $(document).on('click','#protocol',function(e){
            art.dialog({
                title:'服务条款',
                lock: true,
                fixed:true,
                width:500,
                height:500,
                background: '#fff', // 背景色
                opacity: 0.6,	// 透明度
                content: $('#protocol_modal').html(),
                id: 'protocol_modal'
            });
            $('#tilScrollBar').niceScroll({
                cursorcolor:"#d8d9dc",
                cursoropacitymax:1,
                touchbehavior:false,
                horizrailenabled:false,
                cursorwidth:"5px",
                cursorborder:"0",
                cursorborderradius:"5px"
            });

            e.preventDefault();
        });

    })
</script>
<?php 
require_once "header.php";
?>

<?php if ($type == 0){?>

    <div class="user_relative">
        <div  class="send_email_success">
            <h3><i class="icon send_email_success_icon"></i>还差一步即可完成注册！</h3>
            <div class="details">
                我们已经向您的邮箱<a href="http://<?php echo 'mail.'.substr($email, stripos($email, '@')+1, strlen($email)-stripos($email, '@'));?>" target="_blank" class="btn-link"><?php echo $email;?></a>发送了一封激活邮件，请点击邮件中的链接完成注册！
            </div>
            <div>
                <a href="http://<?php echo 'mail.'.substr($email, stripos($email, '@')+1, strlen($email)-stripos($email, '@'));?>" target="_blank" class="long-btn-nb long-btn-nb-primary">立即进入邮箱</a>
            </div>
            <div class="help">
                <label>没有收到确认邮件，怎么办？</label>
                <p>看看是否在邮箱的回收站中、垃圾邮件中</p>
            </div>
        </div>
    </div>
<?php 
}
elseif ($type == 5)
{
?>

    <div class="user_relative">
        <div  class="send_email_success">
            <h3><i class="icon send_email_success_icon"></i>激活成功！</h3>
            <div class="details">
                您的账户已成功激活,感谢您使用CocosAds
            </div>
            <div>
                <a href="/" class="long-btn-nb long-btn-nb-primary">登录</a>
            </div>
        </div>
    </div>
<?php 
}
else 
{
?>
    <div class="user_relative">
        <div  class="send_email_success">
            <h1 class="title"><b class="icon_yesb"></b><?php echo $title?></h1>
            <h3><i class="icon send_email_success_icon"></i>感谢您使用CocosAds！</h3>
        </div>
    </div>
<?php }?>

<?php 
require_once "footer.html";
?>
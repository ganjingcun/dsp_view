<?php 
require_once "header.php";
?>
    <!--  主体部分 [ -->
    <div class="container">
        <div class="user_relative">
            <h2>找回密码</h2>
            <div class="box">
                <div class="fl img_box img_find"></div>
                <div class="fr form_box jumbotron">
                    <form action="" method="post">
                        <br>
                        <br>
                        <div class="form-group">
                            <input class="form-control" type="text" id="email" name="email" placeholder="请输入邮箱"><br>
                            <span class="alert alert-small alert-warning" style="display: none;" id="emailerr"><i class="icon error_icon"></i><span id="emailmsg">请输入邮箱。</span></span>
                        </div>
                        <div class="form-group">
                            <input class="form-control verify_code" type="text" name="captcha" id="captcha" placeholder="验证码">
                            <div class="verify_code_fix">
                                <img src="/captcha/index/70/30/4/<?php echo rand();?>" width="70" height="30" id='captchaimage'>
                                <label><a  href="javascript:document.getElementById('captchaimage').src='/captcha/index/70/30/4/'+Math.random();document.getElementById('captcha').focus();">看不清<br>换一张</a></label>
                                <span class="red"></span>
                            </div>
                            <br>
                            <br>
                            <span class="alert alert-small alert-warning" style="display: none;" id="captchaerr"><i class="icon error_icon"></i><span id="captchamsg">验证码错误。</span></span>
                        </div>
                        <br>
                        <div id="sendLoading" style="line-height: 36px; display: none; font-size: 14px; text-align: center;">正在发送邮件...</div>
                        <br>
                        <br>
                        <div class="form-group">
                            <input type="button" value="发送验证信息至邮箱" id="sendemail" class="long-btn-nb long-btn-nb-primary">
                        </div>
                        <br>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- ] 主体部分 -->
<script type="text/javascript">
$(function(){
	$('#sendemail').click(function(e){
		var email = $('#email').val();
		var captcha = $("#captcha").val();
	    $("#captchaerr").hide();
        $("#emailerr").hide();
		$.post('/webindex/ajaxforgotpwd', 
			{ 
    			email:email,
    			captcha:captcha,
    			r:Math.random()
			}, 
			function(data)
			{
    			if(data && data['status'] == 0){
        			if(data['data'] != '')
        			{
        				$("#"+data['data'][0]).html(data['data'][1]);
            		}
    				$("#"+data['info']).show();
                    $('#sendLoading').hide();
                    $('#sendemail').attr('disabled',false);
    				return false;
    			}else{
    				location.href = '/webindex/sendaccess?email='+email;
    			}
			},
			'json'
        );
        $('#sendLoading').show();
        $('#sendemail').attr('disabled',true);
	});
})

</script>
<?php 
require_once "footer.html";
?>

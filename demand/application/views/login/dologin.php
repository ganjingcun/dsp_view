<?php 
require_once "header.php";
?>
	<div class="conter">
		<div class="login_box">
			<h1 class="hd">用户登录</h1>
			<form method="post" action="/webindex/dologin">
			<span class="red"></span>
				<div class="item"><strong>邮箱：</strong>
					<input type="text" size="50" class="input_text email" value="<?php echo $username ?>" name="username" onfocus="inputCueInfo(this,'邮箱');" />&nbsp;&nbsp;
					<span class="red"><?php echo !empty($usernamemsg) ? $usernamemsg : '' ?></span>
				</div>
				<div class="item"><strong>密码：</strong>  
					<input id="pwd" class="input_text pw" type="password" size="50" name="password"/>&nbsp;&nbsp; <!-- <a href="/index/forgotPassword" target="_blank">找回密码</a>&nbsp; -->
					<span class="red"><?php echo !empty($passwordmsg) ? $passwordmsg : '' ?></span>
				</div>
				<div class="item"><strong>验证码：</strong>
					<input type="text" size="10" class="input_text vali" name='captcha' value="验证码" onfocus="inputCueInfo(this,'验证码');"/>
					<img src="/captcha/index/70/30/4/<?php echo rand();?>" width="70" height="30" id='captchaimage' />&nbsp;
					<a class="inste" href="javascript:document.getElementById('captchaimage').src='/captcha/index/70/30/4/'+Math.random();document.getElementById('captcha').focus();">看不清换一张</a>
					<span class="red"><?php echo !empty($captchamsg) ? $captchamsg : '' ?></span>
				</div>
				<div class="item"><strong> </strong>
					<span><a href="<?php echo $this->config->item('openurl')?>/login_oauth/login_action?client_id=<?php echo $client_id;?>&redirect_url=<?php echo $redirect_url;?>&sign=<?php echo $sign;?>">CocoaChina账号登录</a></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<span><a href="/index.php/webindex/showregister">注册新用户</a></span>
				</div>
				<div class="item" style="padding-left:150px;">
					<input type="submit" class="button_ll" value="登 录" />
				</div>
			</form>
		</div>
	</div>
<?php 
require_once "footer.html";
?>

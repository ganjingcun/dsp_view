<!--  主体部分 [ -->
<div class="inner_container" id="innerContainer">
    <?php $this->load->view('manage/user/left')?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <div class="jumbotron jumbotron_no_shadow edit_info">
                    <header class="title">
                        <h3><strong  class="square_box"></strong><span>基本信息管理</span></h3>
                    </header>
                    <br>
                    <br>
                    <div class="form-group">
                        <div class="fl form-label"><i class="require_item">*</i>联系人姓名:</div>
                        <div class="fl">
                            <input type="text" size="40" id="contact" class="form-control form-control-small" name="contact" value="<?php echo $contact;?>" placeholder="平台联系开发者，签订协议等，请填写负责人姓名">
                        </div>
                        <span id="contacterr" class="fl alert alert-small alert-warning" style="display: none;"><i class="icon error_icon"></i><span id="contactmsg">请输入手机号码！</span></span>
                    </div>
                    <!--  -->
                    <div class="form-group">
                        <div class="fl form-label">注册邮箱:</div>
                        <div class="fl font"><?php echo $userinfo['username']?></div>
                    </div>
                    <!--  -->
                    <div class="form-group">
                        <div class="fl form-label"><i class="require_item">*</i>手机号码：</div>
                        <div class="fl">
                            <input type="text" size="40" id="mobile" class="form-control form-control-small" value="<?php echo $mobile;?>" name="mobile" placeholder="将用于提现手机验证">
                            <span id="mobileerr" class="fl alert alert-small alert-warning" style="display: none;"><i class="icon error_icon"></i><span id="mobilemsg">请输入手机号码！</span></span>
                        </div>
                    </div>
                    <!--  -->
                    <div class="form-group">
                        <div class="fl form-label"><i class="require_item">*</i>联系地址：</div>
                        <div class="fl">
                            <input type="text" size="40" id="address" name="address" class="form-control form-control-small" value="<?php echo $address;?>" placeholder="将用于邮寄合同或发票" >
                            <span id="addresserr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="addressmsg"></span></span>
                        </div>
                    </div>
                    <!--  -->
                    <div class="form-group">
                        <div class="fl form-label">固定电话：</div>
                        <div class="fl">
                            <input type="text" id="telephone" class="form-control form-control-small" name="telephone" value="<?php echo $telephone;?>" size="40" >
                            <span id="telephoneerr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="telephonemsg"></span></span>
                        </div>
                    </div>
                    <!--  -->
                    <div class="form-group">
                        <div class="fl form-label"><i class="require_item">*</i>QQ:</div>
                        <div class="fl">
                            <input type="text" id="qqnumber" class="form-control form-control-small" name="qqnumber" value="<?php echo $qqnumber;?>" size="40" >
                            <span id="qqnumbererr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="qqnumbermsg"></span></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="fl form-label">密钥:</div>
                        <div class="fl font"><?php echo $secretKey;?></div>
                    </div>
                    <!--  -->
                    <!-- 提交按钮 -->
                    <div class="form-group">
                        <div class="fl form-label" style="height:40px;"></div>
                        <div class="fl">
                            <button class="btn btn-primary btn-primary-noboder" id="formsubmit" name="formsubmit" type="button">&nbsp;&nbsp;保&nbsp;&nbsp;存&nbsp;&nbsp;</button>
                            <button class="btn btn-default btn-default-noboder" onclick="history.back();" id="cancelsubmit" name="cancelsubmit" type="button">&nbsp;&nbsp;取&nbsp;&nbsp;消&nbsp;&nbsp;</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php $this->load->view("manage/inc/footer");?>

<script>

$(document).ready(function() {
	$("#formsubmit").click(function (){
		var contact = $("#contact").val()
		var mobile = $("#mobile").val()
		var address = $("#address").val()
		var telephone = $("#telephone").val()
		var qqnumber = $("#qqnumber").val()
		msgInit();
		$("#formsubmit").attr("disabled", "disabled");
		$.post("/manage/userinfo/doeditcontact", 
				{ 
    			contact : contact,
    			mobile : mobile,
    			address : address,
    			telephone : telephone,
    			qqnumber : qqnumber,
	   			r:Math.random()
				}, 
				function(data)
				{
	    			if(data && data['status'] == 0){
	        			if(data['data'] != '')
	        			{
	        				$("#"+data['data'][0]).html(data['data'][1]);
	            		}
	    				$("#"+data['info']).show();
	    		        $("#formsubmit").removeAttr("disabled");
	    				return false;
	    			}else{
		    			alert('修改成功！');
	    				location.href="/manage/userinfo/index";
	    			}
				},
				'json');
			return false;
			return;		
		})
});

function msgInit()
{
	$("#contacterr").hide()
	$("#mobileerr").hide()
	$("#addresserr").hide()
	$("#telephoneerr").hide()
	$("#qqnumbererr").hide()
}
</script>

<!--  主体部分 [ -->
<div class="inner_container" id="innerContainer">
    <?php $this->load->view('manage/user/left')?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <!-- 内容部分 -->
                <div class="jumbotron jumbotron_no_shadow">
                    <header class="title">
                        <h3><strong class="square_box"></strong><span>身份验证</span></h3>
                    </header>
                    
                     <div class="common_font" style="padding-left: 100px;">
                        <div class="form-group">
                            <div class="fl form-label">公司全称:</div>
                            <div class="fl font-con" style="margin-bottom: 10px;">
	                           	 <input type="hidden" value="<?php echo $accountinfo['id'];?>" name="id" id="id">
	                             <input type="text" size="50" id="companyname" name="companyname" class="form-control form-control-small" value="<?php echo $accountinfo['companyname']?>" placeholder="请填写与营业执照一致的合法公司全称" />
	                       	</div>
	                       	<span id="companynameerr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="companynamemsg"></span></span>
                        </div>
                        <!--  -->
                        <div class="form-group">
                            <div class="fl form-label">营业执照号:</div>
                            <div class="fl font-con" style="margin-bottom: 10px;">
                            	<input type="text" size="50" id="companylicense" name="companylicense" class="form-control form-control-small" value="<?php echo $accountinfo['companylicense']?>"  placeholder="请填写营业执照号"/>
	                        </div>
	                        <span id="companylicenseerr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="companylicensemsg"></span></span>
                        </div>
                        <!--  -->
                        <div class="form-group">
                            <div class="fl form-label">组织机构代码:</div>
                            <div class="fl font-con" style="margin-bottom: 10px;">
                            	<input type="text" size="50" id="companyid" name="companyid" class="form-control form-control-small"  value="<?php echo $accountinfo['companyid']?>"  placeholder="请填写组织机构代码"/>
	                       	</div>
	                       	<a href="http://news.jwb.com.cn/art/2015/10/2/art_19907_5901212.html" class="fl alert-small" target="_blank">《组织机构代码三码合一》</a>
	                       	<span id="companyiderr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="companyidmsg"></span></span>
                        </div>
                       <div class="form-group">
                            <div class="fl form-label"><i class="require_item">*</i>营业执照:</div>
                            <div class="fl file_box" style="margin-bottom: 10px;margin-top:10px;">
                            	<input type="file" name='Filedata'  id="uploadify30" class='uploadify fl'  onchange='ajaxUploder(this,300)'/>
                                <div class="fl"><label class="cueinfo">图片格式必须为png或jpg，文件大小不超过2M。</label></div>
                                <br style="clear:left">
                                <div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
                                <div class="300image_show_box">
                                     <img style="width:300px;height:200px; margin-top: 15px;" src="/public/upload/<?php echo $accountinfo['uploadify300']?>">
                                     <input type="hidden" value="<?php echo $accountinfo['uploadify300'];?>" name="uploadify300" id="uploadify300"/>
                                     <a target="_blank" href="/public/upload<?php echo $accountinfo['uploadify300'];?>">查看实际效果</a>
                                </div>
                             </div>
                             <span id="uploadify300err" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="uploadify300msg"></span></span>
                        </div>
                          <!-- 提交按钮 -->
                            <div class="form-group">
                                <div class="fl form-label" style="height: 40px;"></div>
                                <div class="fl">
                                    <button class="btn btn-primary btn-primary-noboder" id="formsubmit" name="formsubmit" type="button">提交审核</button>
                                    <button type="button" class="btn btn-default btn-default-noboder" style="margin-left:50px"  onclick="history.back();">取&nbsp;&nbsp;&nbsp;&nbsp;消</button>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php $this->load->view("manage/inc/footer");?>
<script>
$(document).ready(function() {
	
	//初始化区域信息
	$("#formsubmit").click(function (){
		
		var id = $("#id").val();
		var companyname = $("#companyname").val();
		var companylicense=$("#companylicense").val();
		var companyid=$("#companyid").val();
		var uploadify300 = $("#uploadify300").val();
		msgInit();
		$("#formsubmit").attr("disabled", "disabled");
		$.ajax({url:"/manage/userinfo/doeidtcompany",
			type:"POST",
			data:{
				id : id,
				companyname : companyname,
				companylicense : companylicense,
				companyid : companyid,
		    	uploadify300 : uploadify300,
		   		r:Math.random(),
			},
			success:function(data){
				
				var dataObj = JSON.parse(data);
				if(dataObj && dataObj.status == 0){
	        		if(dataObj.data != '')
	        		{
	        			$("#"+dataObj.data[0]).html(dataObj.data[1]);
	            	}
	        		if(dataObj.info != '')
	        		{
	        			$("#"+dataObj.info).show();
	        		}
	    	        $("#formsubmit").removeAttr("disabled");
	    			return false;
	    		}else if(dataObj && dataObj.status == 1){
	    			alert('提交成功，请等待审核！');
	    			location.href = '/manage/userinfo/showaccountinfo';
	    		}
	    		else
	    		{
					alert('未知错误！');
	    		}
			},
		});	
	});	
});

function msgInit()
{
	$("#companynameerr").hide();
	$("#companylicenseerr").hide();
	$("#companyiderr").hide();
	$("#uploadify300err").hide();
}
function ajaxUploder(fileObj, tpl) {
	var obj 			= $(fileObj);
	var fileId 			= obj.attr('id');
	var image_show_box 	= obj.parents('div:eq(0)').find('.image_show_box');
	var maindiv 		= obj.parents('div:eq(0)');
	var loading = obj.siblings('.onload').show();
	$.ajaxFileUpload( {
				url : '/manage/indexreg/ajaxDoUpload/',
				secureuri : true,
				fileElementId : fileId,
				dataType : 'json',// 服务器返回的格式，可以是json
				success : function(data, status) {
        			$("#iconimgerr").hide();
					if(data['status'])
					{
						var str = '<div class="'+tpl+'image_show_box">';
						str+='<img src="/public/upload'+data['data']+'" style="width:300px;height:200px;" /><a href="/public/upload'+data['data']+'" target="_blank">查看实际效果</a>';
						str+='<input type="hidden" id="uploadify'+tpl+'" name="uploadify'+tpl+'" value="'+data['data']+'"/>';
						str += '&nbsp;&nbsp;</div>';
						maindiv.find('.'+tpl+'image_show_box').remove();// 将刚才上传的干掉
						maindiv.append(str);
						maindiv.find('.'+tpl+'_image_show_box').fadeIn("slow");
						}
					else
					{
						alert(data['data']);
						}
					loading.hide();
					return true;
				},
				error : function(data, status, e) {
					loading.hide();
				}
			})
}

</script>

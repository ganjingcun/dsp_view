<!--  主体部分 [ -->
<div class="inner_container" id="innerContainer">
    <?php $this->load->view('manage/user/left')?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <div class="jumbotron jumbotron_no_shadow edit_info">
                    <header class="title">
                        <h3><strong  class="square_box"></strong><span>收款信息管理</span></h3>
                    </header>
                    <br>
                    <br>
                    <form action="" id="creator" method="post" name="creator" >
                        <div class="form-group">
                            <div class="fl form-label"><i class="require_item">*</i>收款方名：</div>
                            <div class="fl">
                                <input type="text" id="bankusername" class="form-control form-control-small" name="bankusername" value="<?php echo $bankusername;?>"  size="40" placeholder="请填写收款方名" >
                            </div>
                            <span id="bankusernameerr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="bankusernamemsg"></span></span>
                        </div>
                        <div class="form-group">
                            <div class="fl form-label"><i class="require_item">*</i>收款币种：</div>
                            <div class="fl font"><?php echo $currency?></div>
                        </div>
                        <div class="form-group">
                            <div class="fl form-label"><i class="require_item">*</i>所在地区：</div>
                            <div class="fl">
                                <select name="province" id="province" onChange = "select()"></select>
                                &nbsp;
                                <select name="city" id="city" onChange = "select()"></select>
                            </div>
                            <span id="provinceerr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="provincemsg"></span></span>
                        </div>
                        <div class="form-group">
                            <div class="fl form-label"><i class="require_item">*</i>开户银行：</div>
                            <div class="fl">
                                <select id="bank" name="bank">
                                    <option value='中国银行'  <?php if($bank=='中国银行'){echo 'selected';}?> >中国银行</option>
                                    <option value='中国工商银行' <?php if($bank=='中国工商银行'){echo 'selected';}?> >中国工商银行</option>
                                    <option value='中国建设银行' <?php if($bank=='中国建设银行'){echo 'selected';}?> >中国建设银行</option>
                                    <option value='中国农业银行' <?php if($bank=='中国农业银行'){echo 'selected';}?> >中国农业银行</option>
                                    <option value='交通银行' <?php if($bank=='交通银行'){echo 'selected';}?> >交通银行</option>
                                    <option value='中国邮政储蓄银行' <?php if($bank=='中国邮政储蓄银行'){echo 'selected';}?> >中国邮政储蓄银行</option>
                                    <option value='中国民生银行' <?php if($bank=='中国民生银行'){echo 'selected';}?> >中国民生银行</option>
                                    <option value='中国光大银行' <?php if($bank=='中国光大银行'){echo 'selected';}?> >中国光大银行</option>
                                    <option value='招商银行' <?php if($bank=='招商银行'){echo 'selected';}?> >招商银行</option>
                                    <option value='中信银行' <?php if($bank=='中信银行'){echo 'selected';}?> >中信银行</option>
                                    <option value='华夏银行' <?php if($bank=='华夏银行'){echo 'selected';}?> >华夏银行</option>
                                    <option value='恒丰银行' <?php if($bank=='恒丰银行'){echo 'selected';}?> >恒丰银行</option>
                                    <option value='渤海银行' <?php if($bank=='渤海银行'){echo 'selected';}?> >渤海银行</option>
                                    <option value='浙商银行' <?php if($bank=='浙商银行'){echo 'selected';}?> >浙商银行</option>
                                    <option value='平安银行' <?php if($bank=='平安银行'){echo 'selected';}?> >平安银行</option>
                                    <option value='兴业银行' <?php if($bank=='兴业银行'){echo 'selected';}?> >兴业银行</option>
                                    <option value='上海浦东发展银行' <?php if($bank=='上海浦东发展银行'){echo 'selected';}?> >上海浦东发展银行</option>
                                    <option value='广东发展银行' <?php if($bank=='广东发展银行'){echo 'selected';}?> >广东发展银行</option>
                                    <option value='深圳发展银行' <?php if($bank=='深圳发展银行'){echo 'selected';}?> >深圳发展银行</option>
                                    <option value='农村信用合作社' <?php if($bank=='农村信用合作社'){echo 'selected';}?> >农村信用合作社</option>
                                    <option value='中国邮政储蓄银行' <?php if($bank=='中国邮政储蓄银行'){echo 'selected';}?> >中国邮政储蓄银行</option>
                                    <option value='上海银行' <?php if($bank=='上海银行'){echo 'selected';}?> >上海银行</option>
                                    <option value='北京银行' <?php if($bank=='北京银行'){echo 'selected';}?> >北京银行</option>
                                    <option value='北京农村商业银行' <?php if($bank=='北京农村商业银行'){echo 'selected';}?> >北京农村商业银行</option>
                                    <option value='城市信用合作社' <?php if($bank=='城市信用合作社'){echo 'selected';}?> >城市信用合作社</option>
                                    <option value='福建兴业银行' <?php if($bank=='福建兴业银行'){echo 'selected';}?> >福建兴业银行</option>
                                    <option value='广东南粤银行' <?php if($bank=='广东南粤银行'){echo 'selected';}?> >广东南粤银行</option>
                                    <option value='广州农村商业银行' <?php if($bank=='广州农村商业银行'){echo 'selected';}?> >广州农村商业银行</option>
                                    <option value='广州银行' <?php if($bank=='广州银行'){echo 'selected';}?> >广州银行</option>
                                    <option value='桂林银行' <?php if($bank=='桂林银行'){echo 'selected';}?> >桂林银行</option>
                                    <option value='杭州联合银行' <?php if($bank=='杭州联合银行'){echo 'selected';}?> >杭州联合银行</option>
                                    <option value='杭州银行' <?php if($bank=='杭州银行'){echo 'selected';}?> >杭州银行</option>
                                    <option value='江苏银行' <?php if($bank=='江苏银行'){echo 'selected';}?> >江苏银行</option>
                                    <option value='汉口银行' <?php if($bank=='汉口银行'){echo 'selected';}?> >汉口银行</option>
                                    <option value='徽商银行' <?php if($bank=='徽商银行'){echo 'selected';}?> >徽商银行</option>
                                    <option value='宁波银行' <?php if($bank=='宁波银行'){echo 'selected';}?> >宁波银行</option>
                                    <option value='上海农村商业银行' <?php if($bank=='上海农村商业银行'){echo 'selected';}?> >上海农村商业银行</option>
                                    <option value='中国进出口银行' <?php if($bank=='中国进出口银行'){echo 'selected';}?> >中国进出口银行</option>
                                    <option value='国家开发银行' <?php if($bank=='国家开发银行'){echo 'selected';}?> >国家开发银行</option>
                                    <option value='中国农业发展银行' <?php if($bank=='中国农业发展银行'){echo 'selected';}?> >中国农业发展银行</option>
                                    <option value='重庆三峡银行' <?php if($bank=='重庆三峡银行'){echo 'selected';}?> >重庆三峡银行</option>
                                    <option value='城市商业银行' <?php if($bank=='城市商业银行'){echo 'selected';}?> >城市商业银行</option>
                                    <option value='富滇银行' <?php if($bank=='富滇银行'){echo 'selected';}?> >富滇银行</option>
                                    <option value='新韩银行' <?php if($bank=='新韩银行'){echo 'selected';}?> >新韩银行</option>
                                    <option value='渣打银行' <?php if($bank=='渣打银行'){echo 'selected';}?> >渣打银行</option>
                                    <option value='花旗银行' <?php if($bank=='花旗银行'){echo 'selected';}?> >花旗银行</option>
                                    <option value='汇丰银行' <?php if($bank=='汇丰银行'){echo 'selected';}?> >汇丰银行</option>
                                    <option value='东亚银行' <?php if($bank=='东亚银行'){echo 'selected';}?> >东亚银行</option>
                                    <option value='三井住友银行' <?php if($bank=='三井住友银行'){echo 'selected';}?> >三井住友银行</option>
                                    <option value='其他' <?php if($bank=='其他'){echo 'selected';}?> >其他</option>
                                </select>&nbsp;<br><br>
                                <input type="text" size="40" id="bankaddress" class="form-control form-control-small" value="<?php echo $bankaddress;?>"  name="bankaddress" placeholder="请填写具体分行/支行" >
                            </div>
                            <span id="bankaddresserr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="bankaddressmsg"></span></span>
                        </div>
                        <div class="form-group">
                            <div class="fl form-label"><i class="require_item">*</i>银行账号：</div>
                            <div class="fl">
                                <input type="text" id="bankaccount" class="form-control form-control-small" name="bankaccount" value="<?php echo $bankaccount;?>"  size="40" placeholder="请填写收款银行卡号" >
                            </div>
                            <span id="bankaccounterr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="bankaccountmsg"></span></span>
                        </div>
                        <div class="form-group">
                            <div class="fl form-label"><i class="require_item">*</i>确认账号：</div>
                            <div class="fl">
                                <input type="text" id="rebankaccount" name="rebankaccount" class="form-control form-control-small" size="40" placeholder="请再次填写收款银行卡号" >
                            </div>
                            <span id="rebankaccounterr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="rebankaccountmsg"></span></span>
                        </div>
                        <!--  -->
                        <!-- 提交按钮 -->
                        <div class="form-group">
                            <div class="fl form-label" style="height:40px;"></div>
                            <div class="fl">
                                <button class="btn btn-primary btn-primary-noboder" id="formsubmit" name="formsubmit" type="button">&nbsp;&nbsp;保&nbsp;&nbsp;存&nbsp;&nbsp;</button>
                                <button class="btn btn-default btn-default-noboder" onclick="history.go(-1);" id="cancelsubmit" name="cancelsubmit" type="button">&nbsp;&nbsp;取&nbsp;&nbsp;消&nbsp;&nbsp;</button>
                            </div>
                        </div>
                        <!--  -->
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
<?php $this->load->view("manage/inc/footer");?>
<script>

$(document).ready(function() {
	//初始化区域信息
	init();
	$("#formsubmit").click(function (){
		var province = $("#province").val()
		var city = $("#city").val()
		var bank = $("#bank").val()
		var bankusername = $("#bankusername").val()
		var bankaddress = $("#bankaddress").val()
		var bankaccount = $("#bankaccount").val()
		var rebankaccount = $("#rebankaccount").val()
		msgInit();
		$("#formsubmit").attr("disabled", "disabled");
		$.post("/manage/userinfo/doeditfinance", 
				{ 
    			province : province,
    			city : city,
    			bank : bank,
    			bankusername : bankusername,
    			bankaddress : bankaddress,
    			bankaccount : bankaccount,
    			rebankaccount : rebankaccount,
	   			r:Math.random()
				}, 
				function(data)
				{
	    			if(data && data['status'] == 0){
	        			if(data['data'] != '')
	        			{
	        				$("#"+data['data'][0]).html(data['data'][1]);
	            		}
	    				$("#"+data['info']).show();
	    		        $("#formsubmit").removeAttr("disabled");
	    				return false;
	    			}else{
		    			alert('修改成功！');
	    				location.href="/manage/userinfo/showfinanceinfo";
	    			}
				},
				'json');
			return false;
			return;		
		});
});

function msgInit()
{
	$("#provinceerr").hide()
	$("#bankaddresserr").hide()
	$("#bankaccounterr").hide()
	$("#rebankaccounterr").hide()
	$("#bankusernameerr").hide()
}
</script>


<script language="javascript">
<!--
var where = new Array(34);
function comefrom(loca,locacity) { this.loca = loca; this.locacity = locacity; }
where[0]= new comefrom("请选择省份名","请选择城市名");
where[1] = new comefrom("北京","请选择|东城|西城|崇文|宣武|朝阳|丰台|石景山|海淀|门头沟|房山|通州|顺义|昌平|大兴|平谷|怀柔|密云|延庆");  //欢迎来到站长特效网，我们的网址是www.zzjs.net，很好记，zz站长，js就是js特效，本站收集大量高质量js代码，还有许多广告代码下载。
where[2] = new comefrom("上海","请选择|黄浦|卢湾|徐汇|长宁|静安|普陀|闸北|虹口|杨浦|闵行|宝山|嘉定|浦东|金山|松江|青浦|南汇|奉贤|崇明");//欢迎来到站长特效网，我们的网址是www.zzjs.net，很好记，zz站长，js就是js特效，本站收集大量高质量js代码，还有许多广告代码下载。
where[3] = new comefrom("天津","请选择|和平|东丽|河东|西青|河西|津南|南开|北辰|河北|武清|红挢|塘沽|汉沽|大港|宁河|静海|宝坻|蓟县");
where[4] = new comefrom("重庆","请选择|万州|涪陵|渝中|大渡口|江北|沙坪坝|九龙坡|南岸|北碚|万盛|双挢|渝北|巴南|黔江|长寿|綦江|潼南|铜梁|大足|荣昌|壁山|梁平|城口|丰都|垫江|武隆|忠县|开县|云阳|奉节|巫山|巫溪|石柱|秀山|酉阳|彭水|江津|合川|永川|南川");
where[5] = new comefrom("河北","请选择|石家庄|邯郸|邢台|保定|张家口|承德|廊坊|唐山|秦皇岛|沧州|衡水");
where[6] = new comefrom("山西","请选择|太原|大同|阳泉|长治|晋城|朔州|吕梁|忻州|晋中|临汾|运城");
where[7] = new comefrom("内蒙古","请选择|呼和浩特|包头|乌海|赤峰|呼伦贝尔盟|阿拉善盟|哲里木盟|兴安盟|乌兰察布盟|锡林郭勒盟|巴彦淖尔盟|伊克昭盟");
where[8] = new comefrom("辽宁","请选择|沈阳|大连|鞍山|抚顺|本溪|丹东|锦州|营口|阜新|辽阳|盘锦|铁岭|朝阳|葫芦岛");
where[9] = new comefrom("吉林","请选择|长春|吉林|四平|辽源|通化|白山|松原|白城|延边");
where[10] = new comefrom("黑龙江","请选择|哈尔滨|齐齐哈尔|牡丹江|佳木斯|大庆|绥化|鹤岗|鸡西|黑河|双鸭山|伊春|七台河|大兴安岭");
where[11] = new comefrom("江苏","请选择|南京|镇江|苏州|南通|扬州|盐城|徐州|连云港|常州|无锡|宿迁|泰州|淮安");
where[12] = new comefrom("浙江","请选择|杭州|宁波|温州|嘉兴|湖州|绍兴|金华|衢州|舟山|台州|丽水");
where[13] = new comefrom("安徽","请选择|合肥|芜湖|蚌埠|马鞍山|淮北|铜陵|安庆|黄山|滁州|宿州|池州|淮南|巢湖|阜阳|六安|宣城|亳州");
where[14] = new comefrom("福建","请选择|福州|厦门|莆田|三明|泉州|漳州|南平|龙岩|宁德");
where[15] = new comefrom("江西","请选择|南昌市|景德镇|九江|鹰潭|萍乡|新馀|赣州|吉安|宜春|抚州|上饶");
where[16] = new comefrom("山东","请选择|济南|青岛|淄博|枣庄|东营|烟台|潍坊|济宁|泰安|威海|日照|莱芜|临沂|德州|聊城|滨州|菏泽");
where[17] = new comefrom("河南","请选择|郑州|开封|洛阳|平顶山|安阳|鹤壁|新乡|焦作|濮阳|许昌|漯河|三门峡|南阳|商丘|信阳|周口|驻马店|济源");
where[18] = new comefrom("湖北","请选择|武汉|宜昌|荆州|襄樊|黄石|荆门|黄冈|十堰|恩施|潜江|天门|仙桃|随州|咸宁|孝感|鄂州");
where[19] = new comefrom("湖南","请选择|长沙|常德|株洲|湘潭|衡阳|岳阳|邵阳|益阳|娄底|怀化|郴州|永州|湘西|张家界");
where[20] = new comefrom("广东","请选择|广州|深圳|珠海|汕头|东莞|中山|佛山|韶关|江门|湛江|茂名|肇庆|惠州|梅州|汕尾|河源|阳江|清远|潮州|揭阳|云浮");
where[21] = new comefrom("广西","请选择|南宁|柳州|桂林|梧州|北海|防城港|钦州|贵港|玉林|南宁地区|柳州地区|贺州|百色|河池");
where[22] = new comefrom("海南","请选择|海口|三亚");
where[23] = new comefrom("四川","请选择|成都|绵阳|德阳|自贡|攀枝花|广元|内江|乐山|南充|宜宾|广安|达川|雅安|眉山|甘孜|凉山|泸州");
where[24] = new comefrom("贵州","请选择|贵阳|六盘水|遵义|安顺|铜仁|黔西南|毕节|黔东南|黔南");
where[25] = new comefrom("云南","请选择|昆明|大理|曲靖|玉溪|昭通|楚雄|红河|文山|思茅|西双版纳|保山|德宏|丽江|怒江|迪庆|临沧");
where[26] = new comefrom("西藏","请选择|拉萨|日喀则|山南|林芝|昌都|阿里|那曲");
where[27] = new comefrom("陕西","请选择|西安|宝鸡|咸阳|铜川|渭南|延安|榆林|汉中|安康|商洛");
where[28] = new comefrom("甘肃","请选择|兰州|嘉峪关|金昌|白银|天水|酒泉|张掖|武威|定西|陇南|平凉|庆阳|临夏|甘南");
where[29] = new comefrom("宁夏","请选择|银川|石嘴山|吴忠|固原");
where[30] = new comefrom("青海","请选择|西宁|海东|海南|海北|黄南|玉树|果洛|海西");
where[31] = new comefrom("新疆","请选择|乌鲁木齐|石河子|克拉玛依|伊犁|巴音郭勒|昌吉|克孜勒苏柯尔克孜|博尔塔拉|吐鲁番|哈密|喀什|和田|阿克苏");
where[32] = new comefrom("香港","香港");
where[33] = new comefrom("澳门","澳门");
where[34] = new comefrom("台湾","请选择|台北|高雄|台中|台南|屏东|南投|云林|新竹|彰化|苗栗|嘉义|花莲|桃园|宜兰|基隆|台东|金门|马祖|澎湖");

function select() {
    with(document.creator.province) { var loca2 = options[selectedIndex].value; }
    for(i = 0;i < where.length;i ++) {
        if (where[i].loca == loca2) {
            loca3 = (where[i].locacity).split("|");
            for(j = 0;j < loca3.length;j++) { with(document.creator.city) { length = loca3.length; options[j].text = loca3[j]; options[j].value = loca3[j]; var loca4=options[selectedIndex].value;}}
            break;
        }}
}

function init() {
    with(document.creator.province) {
        length = where.length;
        for(k=0;k<where.length;k++) { options[k].text = where[k].loca; options[k].value = where[k].loca; }
        options[selectedIndex].text = where[0].loca; options[selectedIndex].value = where[0].loca;
    }
    with(document.creator.city) {
        loca3 = (where[0].locacity).split("|");
        length = loca3.length;
        for(l=0;l<length;l++) { options[l].text = loca3[l]; options[l].value = loca3[l]; }
        options[selectedIndex].text = loca3[0]; options[selectedIndex].value = loca3[0];
    }

    var province = "<?php echo $province;?>",city = "<?php echo $city;?>";
    if(province != "" && city != ""){
        $("#province").val(province);
        select()
        $("#city").val(city);
    };
}

-->
</script>

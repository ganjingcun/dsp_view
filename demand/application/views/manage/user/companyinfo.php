<!--  主体部分 [ -->
<div class="inner_container" id="innerContainer">
    <?php $this->load->view('manage/user/left')?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <!-- 内容部分 -->
                <div class="jumbotron jumbotron_no_shadow">
                    <header class="title">
                        <h3><strong class="square_box"></strong><span>身份验证</span></h3>
                    </header>
					<div>
						<?php
						    if((!isset($updatauserinfo) || empty($updatauserinfo)) && $userinfo['auditstatus'] == 1)
						    {
						        ?>
						        <button class="btn btn-primary btn-primary-noboder fr" onclick="location.href='/manage/userinfo/showupdateaccount';" type="button">账户身份变更</button>
						    <?php
						    }
						    ?>
					</div>
					<br/>
                    <div class="common_font" style="padding-left: 100px;">
                        <div class="form-group">
                            <div class="fl form-label">身份状态:</div>
                            <div class="fl font-con">
                                <?php echo getAccountStatus($userinfo['auditstatus']);?>
                            </div>
                        </div>
                        <!--  -->
                        <div class="form-group">
                            <div class="fl form-label">身份类型:</div>
                            <div class="fl font-con"><?php echo getAccountType($userinfo['accounttype']);?></div>
                        </div>
                        <!--  -->
                        <div class="form-group">
                            <div class="fl form-label">公司全称:</div>
                            <div class="fl font-con"><?php echo $userinfo['companyname']?></div>
                        </div>
                        <!--  -->
                        <div class="form-group">
                            <div class="fl form-label">营业执照号:</div>
                            <div class="fl font-con"><?php echo $userinfo['companylicense']?></div>
                        </div>
                        <!--  -->
                        <div class="form-group">
                            <div class="fl form-label">组织机构代码:</div>
                            <div class="fl font-con"><?php echo $userinfo['companyid']?></div>
                        </div>
                        <!--  -->
                        <div class="form-group">
                            <div class="fl form-label">营业执照:</div>
                            <div class="fl font-con">
                                <img style="width:300px;height:200px; margin-top: 15px;" src="/public/upload/<?php echo $userinfo['uploadify300']?>">
                            </div>
                        </div>
                        
                         <div class="form-group" style="margin:30px;">
                            <span class="form-label fl" style="height: 40px;"></span>
                            <span class="fl font-con">
                            <?php
				        		if((!isset($updatauserinfo) || empty($updatauserinfo)) && ($userinfo['auditstatus'] == 2||$userinfo['auditstatus']==1))
				        		{ ?>
	                            <a href="/manage/userinfo/showeditgroup?id=<?php echo $userinfo['id']?>" class="btn btn-primary btn-primary-noboder">
	                            <i class="icon icon_edit_white_con"></i>修 改
	                            </a>
	                            	<?php
						        		}
						        		else{
						        		?>
						        		<a class="btn btn-primary btn-primary-noboder" disabled="disabled">
			                            	<i class="icon icon_edit_white_con"></i>修 改
			                            </a>
						        	<?php }?>
                            </span>
                        </div>
                    </div>
                
        
        
        
        
        <?php
if(isset($updatauserinfo) && !empty($updatauserinfo))
{
    ?>
    <br><br>
    <header class="title">
        <h3><strong  class="square_box"></strong><span>账户身份变更信息</span></h3>
    </header>
    <?php
    if($updatauserinfo['auditstatus'] == 0)
    {
        if($updatauserinfo['accounttype'] == 3)
        {
            ?>
            <div class="common_font" style="padding-left: 100px;">
                <div class="form-group">
                    <div class="fl form-label">审核状态:</div>
                    <div class="fl font-con"><?php echo getAccountStatus($updatauserinfo['auditstatus']);?></div>
                </div>
                <!--  -->
                <div class="form-group">
                    <div class="fl form-label">身份类型:</div>
                    <div class="fl font-con"><?php echo getAccountType($updatauserinfo['accounttype']);?></div>
                </div>
                <!--  -->
                <div class="form-group">
                    <div class="fl form-label">个体工商名称:</div>
                    <div class="fl font-con"><?php echo $updatauserinfo['individualname']?></div>
                </div>
                <!--  -->
                <div class="form-group">
                    <div class="fl form-label">营业执照号:</div>
                    <div class="fl font-con"><?php echo $updatauserinfo['individualid']?></div>
                </div>
                <!--  -->
                <div class="form-group">
                    <div class="fl form-label">营业执照:</div>
                    <div class="fl"><img style="width: 300px; height: 200px; margin-top: 15px;" src="/public/upload/<?php echo $updatauserinfo['uploadify400']?>"></div>
                </div>
                <!--  -->
                <div class="form-group">
                    <div class="fl form-label">业主姓名:</div>
                    <div class="fl font-con"><?php echo $updatauserinfo['ownername']?></div>
                </div>
                <!--  -->
                <div class="form-group">
                    <div class="fl form-label">证件类型:</div>
                    <div class="fl font-con"><?php echo getCertType($updatauserinfo['ownercerttype'])?></div>
                </div>
                <!--  -->
                <div class="form-group">
                    <div class="fl form-label">证件号码:</div>
                    <div class="fl font-con"><?php echo $updatauserinfo['ownerid']?></div>
                </div>
                <!--  -->
                <div class="form-group">
                    <div class="fl form-label">证件正面:</div>
                    <div class="fl"><img style="width: 300px; height: 200px; margin-top: 15px;" src="/public/upload/<?php echo $updatauserinfo['uploadify500']?>"></div>
                </div>
                <!--  -->
                <div class="form-group">
                    <div class="fl form-label">证件背面:</div>
                    <div class="fl font-con"><img style="width: 300px; height: 200px; margin-top: 15px;" src="/public/upload/<?php echo $updatauserinfo['uploadify600']?>"></div>
                </div>
                <br>
                <div class="form-group">
                    <div class="fl form-label" style="height: 40px;"></div>
                    <div class="fl ">
                        <button name="submitbtn" id="submitbtn" type="button" class="btn btn-primary btn-primary-noboder">撤销变更</button> 撤销变更后，将还原原有身份继续使用。
                    </div>
                </div>
            </div>
        <?php
        }
        elseif($updatauserinfo['accounttype'] == 2)
        {
            ?>
            <div class="common_font" style="padding-left: 100px;">
                <div class="form-group">
                    <div class="form-label fl">审核状态:</div>
                    <div class="fl font-con"><?php echo getAccountStatus($updatauserinfo['auditstatus']);?>
                    </div>
                </div>
                <!--  -->
                <div class="form-group">
                    <div class="fl form-label">身份类型:</div>
                    <div class="fl font-con"><?php echo getAccountType($updatauserinfo['accounttype']);?></div>
                </div>
                <!--  -->
                <div class="form-group">
                    <div class="fl form-label">公司全称:</div>
                    <div class="fl font-con"><?php echo $updatauserinfo['companyname']?></div>
                </div>
                <!--  -->
                <div class="form-group">
                    <div class="fl form-label">营业执照号:</div>
                    <div class="fl font-con"><?php echo $updatauserinfo['companylicense']?></div>
                </div>
                <!--  -->
                <div class="form-group">
                    <div class="fl form-label">组织机构代码:</div>
                    <div class="fl font-con"><?php echo $updatauserinfo['companyid']?></div>
                </div>
                <div class="form-group">
                    <div class="fl form-label">营业执照:</div>
                    <div class="fl font-con"><img style="width: 300px; height: 200px; margin-top: 15px;" src="/public/upload/<?php echo $updatauserinfo['uploadify300']?>"></div>
                </div>
                <br>
                <div class="form-group">
                    <div class="fl form-label" style="height:40px;"></div>
                    <div class="fl">
                        <button name="submitbtn" id="submitbtn" type="button" class="btn btn-primary btn-primary-noboder">撤销变更</button> 撤销变更后，将还原原有身份继续使用。
                    </div>
                </div>
            </div>
        <?php
        }
    	elseif($updatauserinfo['accounttype'] == 1)
        {
            ?>
            <div class="common_font" style="padding-left: 100px;">
                <div class="form-group">
                    <div class="form-label fl">审核状态:</div>
                    <div class="fl font-con"><?php echo getAccountStatus($updatauserinfo['auditstatus']);?>
                    </div>
                </div>
                <!--  -->
                <div class="form-group">
                    <div class="fl form-label">身份类型:</div>
                    <div class="fl font-con"><?php echo getAccountType($updatauserinfo['accounttype']);?></div>
                </div>
                <div class="form-group">
                    <div class="fl form-label">证件类型:</div>
                    <div class="fl font-con"><?php echo getCertType($updatauserinfo['certtype']);?></div>
                </div>
                <div class="form-group">
                    <div class="fl form-label">证件姓名:</div>
                    <div class="fl font-con"><?php echo $updatauserinfo['realname']?></div>
                </div>
                <!--  -->
                <div class="form-group">
                    <div class="fl form-label">证件号码:</div>
                    <div class="fl font-con"><?php echo $updatauserinfo['idnumber']?></div>
                </div>
                <br>
                <div class="form-group">
                    <div class="fl form-label" style="height:40px;"></div>
                    <div class="fl">
                        <button name="submitbtn" id="submitbtn" type="button" class="btn btn-primary btn-primary-noboder">撤销变更</button> 撤销变更后，将还原原有身份继续使用。
                    </div>
                </div>
            </div>
        <?php
        }
    }elseif($updatauserinfo['auditstatus'] == 2)
    {
        ?>
        <form action="" id="creator" method="post" name="creator" >
        <div class="edit_info upgrade_user">
        <br>
        <div class="active_success_info" id="newAppCue" style="width:100%;">
            <i class="icon icon_active_success_loger"></i>
            您提交的信息因以下原因被驳回：<?php echo $updatauserinfo['reason'];?>
            <a href="javascript:void(0);" class="close">×</a>
        </div>
        <br>
        <header class="title">
            <h3><strong  class="square_box"></strong><span>身份详细信息</span></h3>
        </header>
        <br>
        <div class="form-group accounttype1">
            <div class="info">
                *账户身份变更完成后，无法撤消，如需恢复，需变更身份再次提交审核，请谨慎操作！
            </div>
        </div>
        <div class="form-group accounttype2">
            <div class="info">
              *账户身份变更为公司，提现时须向CocosAds开具发票，如无法开发票，将无法提取收入！<br>
              *账户身份变更完成后，无法撤消，如需恢复，需变更身份再次提交审核，请谨慎操作！
            </div>
        </div>
        <div class="form-group accounttype3">
            <div class="info">
              *账户身份变更为个体工商户，提现时须向CocosAds开具发票，如无法开发票，将无法提取收入！<br>
              *账户身份变更完成后，无法撤消，如需恢复，需变更身份再次提交审核，请谨慎操作！
            </div>
        </div>
        <div class="form-group">
            <div class="fl form-label"><i class="require_item">*</i>身份类型：</div>
            <div class="fl radio-group" id="selectaccounttype">
                <label class="radio_style"><i class="icon radio_icon <?php if($updatauserinfo['accounttype'] == 3){echo 'active';}?>"></i><input style="display:none;" type="radio" <?php if($updatauserinfo['accounttype'] == 3){echo 'checked="checked"';}?>  name="accounttype" id="accounttype" value="3"> 个体工商户</label>
                <label class="radio_style"><i class="icon radio_icon <?php if($updatauserinfo['accounttype'] == 1){echo 'active';}?>"></i><input style="display:none;" type="radio" <?php if($updatauserinfo['accounttype'] == 1){echo 'checked="checked"';}?>  name="accounttype" id="accounttype" value="1"> 个人/团体</label>
            </div>
            <span id="accounttypeerr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="accounttypemsg"></span></span>
        </div>
        <!-- 个人团体 -->
        <div class="accounttype1" >
            <div class="form-group">
                <div class="fl form-label"><i class="require_item">*</i>证件类型：</div>
                <div class="fl radio-group" id="certtype_wrap">
                    <label class="radio_style"><i class="icon radio_icon <?php if($updatauserinfo['certtype'] == 1){echo 'active';}?>"></i><input style="display:none;" type="radio" name="certtype" id="certtype" value="1" <?php if($updatauserinfo['certtype'] == 1){echo 'checked="checked"';}?>>中国大陆身份证</label>
                    <label class="radio_style"><i class="icon radio_icon <?php if($updatauserinfo['certtype'] == 2){echo 'active';}?>"></i><input style="display:none;" type="radio" name="certtype" id="certtype" value="2" <?php if($updatauserinfo['certtype'] == 2){echo 'checked="checked"';}?>>中国港澳台身份证</label>
                    <label class="radio_style"><i class="icon radio_icon <?php if($updatauserinfo['certtype'] == 3){echo 'active';}?>"></i><input style="display:none;" type="radio" name="certtype" id="certtype" value="3" <?php if($updatauserinfo['certtype'] == 3){echo 'checked="checked"';}?>>海外证件</label>
                </div>
                <span id="certtypeerr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><em id="certtypemsg"></em></span>
            </div>
            <div class="form-group">
                <div class="fl form-label"><i class="require_item">*</i>证件姓名：</div>
                <div class="fl">
                    <input type="text" size="50" id="realname" name="realname" value="<?php echo $updatauserinfo['realname'];?>" placeholder="请如实填写，同时也是您银行账户的开户人" class="form-control form-control-small">
                    <span id="realnameerr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><em id="realnamemsg"></em></span>
                </div>
            </div>
            <div class="form-group">
                <div class="fl form-label"><i class="require_item">*</i>证件号码：</div>
                <div class="fl">
                    <input type="text" size="50" id="idnumber" name="idnumber" value="<?php echo $updatauserinfo['idnumber'];?>" placeholder="保存后将不可修改" class="form-control form-control-small">
                    <span id="idnumbererr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><em id="idnumbermsg"></em></span>
                </div>
            </div>
            <div class="form-group">
                <div class="fl form-label"><i class="require_item">*</i>证件正面：</div>
                <div class="fl">
                    <input type="file" name='Filedata'  id="uploadify10" class='uploadify'  onchange='ajaxUploder(this,100)'/>&nbsp;&nbsp; <br/>
                    <div class="fl"><label class="cueinfo">图片格式必须为png或jpg，文件大小不超过2M。</label></div>
                    <div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
                    <span id="uploadify100err" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><em id="uploadify100msg"></em></span>
                     <?php if($updatauserinfo['accounttype'] == 1){?>
                        <div class="100image_show_box">
                            <img style="width:300px;height:200px;" src="/public/upload<?php echo $updatauserinfo['uploadify100'];?>">
                            <input type="hidden" value="<?php echo $updatauserinfo['uploadify100'];?>" name="uploadify100" id="uploadify100">
                            <a target="_blank" href="/public/upload<?php echo $updatauserinfo['uploadify100'];?>">查看实际效果</a>
                        </div>
                    <?php }?>
                </div>
            </div>
            
            <div class="form-group">
                <div class="fl form-label"><i class="require_item">*</i>证件背面：</div>
                <div class="fl">
                    <input type="file" name='Filedata'  id="uploadify20" class='uploadify'  onchange='ajaxUploder(this,200)'/>&nbsp;&nbsp; <br/>
                    <div class="fl"><label class="cueinfo">图片格式必须为png或jpg，文件大小不超过2M。</label></div>
                    <div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
                    <span id="uploadify200err" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><em id="uploadify200msg"></em></span>
                     <?php if($updatauserinfo['accounttype'] == 1){?>
                        <div class="200image_show_box">
                            <img style="width:300px;height:200px;" src="/public/upload<?php echo $updatauserinfo['uploadify200'];?>">
                            <input type="hidden" value="<?php echo $updatauserinfo['uploadify200'];?>" name="uploadify200" id="uploadify200">
                            <a target="_blank" href="/public/upload<?php echo $updatauserinfo['uploadify200'];?>">查看实际效果</a>
                        </div>
                    <?php }?>
                </div>
            </div>
        </div>
        <!-- 公司 -->
        <div class="accounttype2" style="display: none;">
            <div class="form-group">
                <div class="fl form-label"><i class="require_item">*</i>公司全称：</div>
                <div class="fl">
                    <input type="text" size="50" id="companyname" name="companyname" value="<?php echo $updatauserinfo['companyname'];?>" placeholder="请填写与营业执照一致的合法公司全称" class="form-control form-control-small">
                </div>
                <span id="companynameerr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="companynamemsg"></span></span>
            </div>
            <div class="form-group">
                <div class="fl form-label"><i class="require_item">*</i>营业执照号：</div>
                <div class="fl">
                    <input type="text" size="50" id="companylicense" name="companylicense" value="<?php echo $updatauserinfo['companylicense'];?>"  placeholder="请填写营业执照号" class="form-control form-control-small">
                </div>
                <span id="companylicenseerr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="companylicensemsg"></span></span>
            </div>
            <div class="form-group">
                <div class="fl form-label"><i class="require_item">*</i>组织机构代码：</div>
                <div class="fl">
                    <input type="text" size="50" id="companyid" name="companyid"  value="<?php echo $updatauserinfo['companyid'];?>"  placeholder="请填写组织机构代码" class="form-control form-control-small">
                </div>
                <span id="companyiderr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="companyidmsg"></span></span>
            </div>
            <div class="form-group">
                <div class="fl form-label"><i class="require_item">*</i>营业执照：</div>
                <div class="fl file_box">
                    <input type="file" name='Filedata'  id="uploadify30" class='uploadify fl'  onchange='ajaxUploder(this,300)'/>
                    <div class="fl"><label class="cueinfo">图片格式必须为png或jpg，文件大小不超过2M。</label></div>
                    <br style="clear:left">
                    <div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
                    <?php if($updatauserinfo['accounttype'] == 2){?>
                        <div class="300image_show_box">
                            <img style="width:300px;height:200px;" src="/public/upload<?php echo $updatauserinfo['uploadify300'];?>">
                            <input type="hidden" value="<?php echo $updatauserinfo['uploadify300'];?>" name="uploadify300" id="uploadify300">
                            <a target="_blank" href="/public/upload<?php echo $updatauserinfo['uploadify300'];?>">查看实际效果</a>
                        </div>
                    <?php }?>
                </div>
                <span id="uploadify300err" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="uploadify300msg"></span></span>
            </div>
        </div>
        <!-- 个体工商户 -->
        <div class="accounttype3" style="display: none;">
            <div class="form-group">
                <div class="fl form-label">个体工商户名称：</div>
                <div class="fl">
                    <input type="text" size="50" id="individualname" name="individualname" value="<?php echo $updatauserinfo['individualname'];?>"  placeholder="请如实填写" class="form-control form-control-small">
                </div>
                <span id="individualnameerr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="individualnamemsg"></span></span>
            </div>
            <div class="form-group">
                <div class="fl form-label"><i class="require_item">*</i> 营业执照号：</div>
                <div class="fl">
                    <input type="text" size="50" id="individualid" name="individualid" value="<?php echo $updatauserinfo['individualid'];?>"  placeholder="请填写营业执照号" class="form-control form-control-small">
                </div>
                <span id="individualiderr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="individualidmsg"></span></span>
            </div>
            <div class="form-group">
                <div class="fl form-label"><i class="require_item">*</i>营业执照：</div>
                <div class="fl file_box">
                    <input type="file" name='Filedata'  id="uploadify40" class='uploadify fl'  onchange='ajaxUploder(this,400)'/>
                    <div class="fl"><label class="cueinfo">图片格式必须为png或jpg，文件大小不超过2M。</label></div>
                    <br style="clear:left;">
                    <div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
                    <span id="uploadify400err" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="uploadify400msg"></span></span>
                    <?php if($updatauserinfo['accounttype'] == 3){?>
                        <div class="400image_show_box">
                            <img style="width:300px;height:200px;" src="/public/upload<?php echo $updatauserinfo['uploadify400'];?>">
                            <input type="hidden" value="<?php echo $updatauserinfo['uploadify400'];?>" name="uploadify400" id="uploadify400">
                            <a target="_blank" href="/public/upload<?php echo $updatauserinfo['uploadify400'];?>">查看实际效果</a>
                        </div>
                    <?php }?>
                </div>
            </div>
            <div class="form-group">
                <div class="fl form-label"><i class="require_item">*</i>业主姓名：</div>
                <div class="fl">
                    <input type="text" size="50" id="ownername" name="ownername" value="<?php echo $updatauserinfo['ownername'];?>"  placeholder="请填写业主证件姓名" class="form-control form-control-small">

                </div>
                <span id="ownernameerr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="ownernamemsg"></span></span>
            </div>
            <div class="form-group">
                <div class="fl form-label"><i class="require_item">*</i>证件类型：</div>
                <div class="fl radio-group" id="ownercerttype_wrap">
                    <label class="radio_style"><i class="icon radio_icon <?php if($updatauserinfo['ownercerttype']==1){echo 'active';}?>"></i><input style="display: none;" type="radio" <?php if($updatauserinfo['ownercerttype']==1){echo 'checked="checked"';}?> value="1" name="ownercerttype" id="ownercerttype">中国大陆身份证</label>
                    <label class="radio_style"><i class="icon radio_icon <?php if($updatauserinfo['ownercerttype']==2){echo 'active';}?>"></i><input style="display: none;" type="radio" <?php if($updatauserinfo['ownercerttype']==2){echo 'checked="checked"';}?> value="2" name="ownercerttype" id="ownercerttype">中国港澳台身份证</label>
                    <label class="radio_style"><i class="icon radio_icon <?php if($updatauserinfo['ownercerttype']==3){echo 'active';}?>"></i><input style="display: none;" type="radio" <?php if($updatauserinfo['ownercerttype']==3){echo 'checked="checked"';}?> value="3" name="ownercerttype" id="ownercerttype">海外证件</label>
                </div>
                <span id="ownercerttypeerr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="ownercerttypemsg"></span></span>
            </div>
            <div class="form-group">
                <div class="fl form-label"><i class="require_item">*</i>证件号码：</div>
                <div class="fl">
                    <input type="text" size="50" id="ownerid" name="ownerid" value="<?php echo $updatauserinfo['ownerid'];?>"  placeholder="保存后将不可修改" class="form-control form-control-small">

                </div>
                <span id="owneriderr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="owneridmsg"></span></span>
            </div>
            <div class="form-group">
                <div class="fl form-label"><i class="require_item">*</i>证件正面：</div>
                <div class="fl file_box">
                    <input type="file" name='Filedata'  id="uploadify50" class='uploadify fl'  onchange='ajaxUploder(this,500)'/>
                    <div class="fl"><label class="cueinfo">图片格式必须为png或jpg，文件大小不超过2M。</label></div>
                    <br style="clear: left;">
                    <div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
                    <span id="uploadify500err" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="uploadify500msg"></span></span>
                    <?php if($updatauserinfo['accounttype'] == 3){?>
                        <div class="500image_show_box">
                            <img style="width:300px;height:200px;" src="/public/upload<?php echo $updatauserinfo['uploadify500'];?>">
                            <input type="hidden" value="<?php echo $updatauserinfo['uploadify500'];?>" name="uploadify500" id="uploadify500">
                            <a target="_blank" href="/public/upload<?php echo $updatauserinfo['uploadify500'];?>">查看实际效果</a>
                        </div>
                    <?php }?>
                </div>
            </div>
            <div class="form-group">
                <div class="fl form-label"><i class="require_item">*</i>证件背面：</div>
                <div class="fl file_box">
                    <input type="file" name='Filedata'  id="uploadify60" class='uploadify fl'  onchange='ajaxUploder(this,600)'/>
                    <div class="fl"><label class="cueinfo">图片格式必须为png或jpg，文件大小不超过2M。</label></div>
                    <br style="clear:left;">
                    <div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
                    <span id="uploadify600err" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="uploadify600msg"></span></span>
                    <?php if($updatauserinfo['accounttype'] == 3){?>
                        <div class="600image_show_box">
                            <img style="width:300px;height:200px;" src="/public/upload<?php echo $updatauserinfo['uploadify600'];?>">
                            <input type="hidden" value="<?php echo $updatauserinfo['uploadify600'];?>" name="uploadify600" id="uploadify600">
                            <a target="_blank" href="/public/upload<?php echo $updatauserinfo['uploadify600'];?>">查看实际效果</a>
                        </div>
                    <?php }?>
                </div>
            </div>
        </div>

        <!-- 银行账户信息 -->

        <header class="title">
            <h3><strong  class="square_box"></strong><span>银行账户信息</span></h3>
        </header>

        <div class="form-group">
            <div class="fl form-label"><i class="require_item">*</i>开户人姓名：</div>
            <div class="fl">
                <input type="text" size="20" id="bankusername" name="bankusername" value="<?php echo $updatauserinfo['bankusername'];?>" class="form-control form-control-small">
            </div>
            <span id="bankusernameerr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="bankusernamemsg"></span></span>
        </div>
        <div class="form-group">
            <div class="fl form-label"><i class="require_item">*</i>所在地区：</div>
            <div class="fl">
                <select name="province" id="province" onChange = "select()"></select>
                &nbsp;
                <select name="city" id="city" onChange = "select()"></select>
            </div>
            <span id="provinceerr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="provincemsg"></span></span>
        </div>
        <div class="form-group">
            <div class="fl form-label"><i class="require_item">*</i>开户银行：</div>
            <div class="fl">
                <select id="bank" name="bank">
                    <option value='中国银行'  <?php if($updatauserinfo['bank']=='中国银行'){echo 'selected';}?> >中国银行</option>
                    <option value='中国工商银行' <?php if($updatauserinfo['bank']=='中国工商银行'){echo 'selected';}?> >中国工商银行</option>
                    <option value='中国建设银行' <?php if($updatauserinfo['bank']=='中国建设银行'){echo 'selected';}?> >中国建设银行</option>
                    <option value='中国农业银行' <?php if($updatauserinfo['bank']=='中国农业银行'){echo 'selected';}?> >中国农业银行</option>
                    <option value='交通银行' <?php if($updatauserinfo['bank']=='交通银行'){echo 'selected';}?> >交通银行</option>
                    <option value='中国邮政储蓄银行' <?php if($updatauserinfo['bank']=='中国邮政储蓄银行'){echo 'selected';}?> >中国邮政储蓄银行</option>
                    <option value='中国民生银行' <?php if($updatauserinfo['bank']=='中国民生银行'){echo 'selected';}?> >中国民生银行</option>
                    <option value='中国光大银行' <?php if($updatauserinfo['bank']=='中国光大银行'){echo 'selected';}?> >中国光大银行</option>
                    <option value='招商银行' <?php if($updatauserinfo['bank']=='招商银行'){echo 'selected';}?> >招商银行</option>
                    <option value='中信银行' <?php if($updatauserinfo['bank']=='中信银行'){echo 'selected';}?> >中信银行</option>
                    <option value='华夏银行' <?php if($updatauserinfo['bank']=='华夏银行'){echo 'selected';}?> >华夏银行</option>
                    <option value='恒丰银行' <?php if($updatauserinfo['bank']=='恒丰银行'){echo 'selected';}?> >恒丰银行</option>
                    <option value='渤海银行' <?php if($updatauserinfo['bank']=='渤海银行'){echo 'selected';}?> >渤海银行</option>
                    <option value='浙商银行' <?php if($updatauserinfo['bank']=='浙商银行'){echo 'selected';}?> >浙商银行</option>
                    <option value='平安银行' <?php if($updatauserinfo['bank']=='平安银行'){echo 'selected';}?> >平安银行</option>
                    <option value='兴业银行' <?php if($updatauserinfo['bank']=='兴业银行'){echo 'selected';}?> >兴业银行</option>
                    <option value='上海浦东发展银行' <?php if($updatauserinfo['bank']=='上海浦东发展银行'){echo 'selected';}?> >上海浦东发展银行</option>
                    <option value='广东发展银行' <?php if($updatauserinfo['bank']=='广东发展银行'){echo 'selected';}?> >广东发展银行</option>
                    <option value='深圳发展银行' <?php if($updatauserinfo['bank']=='深圳发展银行'){echo 'selected';}?> >深圳发展银行</option>
                    <option value='农村信用合作社' <?php if($updatauserinfo['bank']=='农村信用合作社'){echo 'selected';}?> >农村信用合作社</option>
                    <option value='中国邮政储蓄银行' <?php if($updatauserinfo['bank']=='中国邮政储蓄银行'){echo 'selected';}?> >中国邮政储蓄银行</option>
                    <option value='上海银行' <?php if($updatauserinfo['bank']=='上海银行'){echo 'selected';}?> >上海银行</option>
                    <option value='北京银行' <?php if($updatauserinfo['bank']=='北京银行'){echo 'selected';}?> >北京银行</option>
                    <option value='北京农村商业银行' <?php if($updatauserinfo['bank']=='北京农村商业银行'){echo 'selected';}?> >北京农村商业银行</option>
                    <option value='城市信用合作社' <?php if($updatauserinfo['bank']=='城市信用合作社'){echo 'selected';}?> >城市信用合作社</option>
                    <option value='福建兴业银行' <?php if($updatauserinfo['bank']=='福建兴业银行'){echo 'selected';}?> >福建兴业银行</option>
                    <option value='广东南粤银行' <?php if($updatauserinfo['bank']=='广东南粤银行'){echo 'selected';}?> >广东南粤银行</option>
                    <option value='广州农村商业银行' <?php if($updatauserinfo['bank']=='广州农村商业银行'){echo 'selected';}?> >广州农村商业银行</option>
                    <option value='广州银行' <?php if($updatauserinfo['bank']=='广州银行'){echo 'selected';}?> >广州银行</option>
                    <option value='桂林银行' <?php if($updatauserinfo['bank']=='桂林银行'){echo 'selected';}?> >桂林银行</option>
                    <option value='杭州联合银行' <?php if($updatauserinfo['bank']=='杭州联合银行'){echo 'selected';}?> >杭州联合银行</option>
                    <option value='杭州银行' <?php if($updatauserinfo['bank']=='杭州银行'){echo 'selected';}?> >杭州银行</option>
                    <option value='江苏银行' <?php if($updatauserinfo['bank']=='江苏银行'){echo 'selected';}?> >江苏银行</option>
                    <option value='汉口银行' <?php if($updatauserinfo['bank']=='汉口银行'){echo 'selected';}?> >汉口银行</option>
                    <option value='徽商银行' <?php if($updatauserinfo['bank']=='徽商银行'){echo 'selected';}?> >徽商银行</option>
                    <option value='宁波银行' <?php if($updatauserinfo['bank']=='宁波银行'){echo 'selected';}?> >宁波银行</option>
                    <option value='上海农村商业银行' <?php if($updatauserinfo['bank']=='上海农村商业银行'){echo 'selected';}?> >上海农村商业银行</option>
                    <option value='中国进出口银行' <?php if($updatauserinfo['bank']=='中国进出口银行'){echo 'selected';}?> >中国进出口银行</option>
                    <option value='国家开发银行' <?php if($updatauserinfo['bank']=='国家开发银行'){echo 'selected';}?> >国家开发银行</option>
                    <option value='中国农业发展银行' <?php if($updatauserinfo['bank']=='中国农业发展银行'){echo 'selected';}?> >中国农业发展银行</option>
                    <option value='重庆三峡银行' <?php if($updatauserinfo['bank']=='重庆三峡银行'){echo 'selected';}?> >重庆三峡银行</option>
                    <option value='城市商业银行' <?php if($updatauserinfo['bank']=='城市商业银行'){echo 'selected';}?> >城市商业银行</option>
                    <option value='富滇银行' <?php if($updatauserinfo['bank']=='富滇银行'){echo 'selected';}?> >富滇银行</option>
                    <option value='新韩银行' <?php if($updatauserinfo['bank']=='新韩银行'){echo 'selected';}?> >新韩银行</option>
                    <option value='渣打银行' <?php if($updatauserinfo['bank']=='渣打银行'){echo 'selected';}?> >渣打银行</option>
                    <option value='花旗银行' <?php if($updatauserinfo['bank']=='花旗银行'){echo 'selected';}?> >花旗银行</option>
                    <option value='汇丰银行' <?php if($updatauserinfo['bank']=='汇丰银行'){echo 'selected';}?> >汇丰银行</option>
                    <option value='东亚银行' <?php if($updatauserinfo['bank']=='东亚银行'){echo 'selected';}?> >东亚银行</option>
                    <option value='三井住友银行' <?php if($updatauserinfo['bank']=='三井住友银行'){echo 'selected';}?> >三井住友银行</option>
                    <option value='其他' <?php if($updatauserinfo['bank']=='其他'){echo 'selected';}?> >其他</option>
                </select>
                <div class="alone">
                    <input type="text" size="40" id="bankaddress" value="<?php echo $updatauserinfo['bankaddress'];?>"  name="bankaddress" placeholder="请填写具体分行/支行" class="form-control form-control-small">
                </div>
            </div>
            <span id="bankaddresserr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="bankaddressmsg"></span></span>
        </div>
        <div class="form-group">
            <div class="fl form-label"><i class="require_item">*</i>银行账号：</div>
            <div class="fl">
                <input type="text" id="bankaccount" name="bankaccount" value="<?php echo $updatauserinfo['bankaccount'];?>"  size="40" placeholder="请填写收款银行卡号" class="form-control form-control-small">
            </div>
            <span id="bankaccounterr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="bankaccountmsg"></span></span>
        </div>
        <div class="form-group">
            <div class="fl form-label"><i class="require_item">*</i>确认账号：</div>
            <div class="fl">
                <input type="text" id="rebankaccount" name="rebankaccount" size="40" placeholder="请再次填写收款银行卡号" class="form-control form-control-small">
            </div>
            <span id="rebankaccounterr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="rebankaccountmsg"></span></span>
        </div>


        <!-- 联系方式 -->
        <header class="title">
            <h3><strong  class="square_box"></strong><span>联系方式</span></h3>
        </header>
        <div class="form-group">
            <div class="fl form-label"><i class="require_item">*</i>联系人：</div>
            <div class="fl">
                <input type="text" size="40" id="contact" name="contact" value="<?php echo $updatauserinfo['contact'];?>"  placeholder="平台联系开发者，签订协议等，请填写负责人姓名" class="form-control form-control-small">
            </div>
            <span id="contacterr" class="fl alert alert-small alert-warning hidden" style="display: none;"><i class="icon error_icon"></i><span id="contactmsg">请输入手机号码！</span></span>
        </div>
        <div class="form-group">
            <div class="fl form-label"><i class="require_item">*</i>手机号码：</div>
            <div class="fl">
                <input type="text" size="40" id="mobile" name="mobile" value="<?php echo $updatauserinfo['mobile'];?>"  placeholder="将用于提现手机验证" class="form-control form-control-small">
            </div>
            <span id="mobileerr" class="fl alert alert-small alert-warning hidden" style="display: none;"><i class="icon error_icon"></i><span id="mobilemsg">请输入手机号码！</span></span>
        </div>
        <div class="form-group">
            <div class="fl form-label"><i class="require_item">*</i>联系地址：</div>
            <div class="fl">
                <input type="text" size="40" id="address" name="address" value="<?php echo $updatauserinfo['address'];?>" placeholder="将用于邮寄合同或发票" class="form-control form-control-small">
            </div>
            <span id="addresserr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="addressmsg"></span></span>
        </div>
        <div class="form-group">
            <div class="fl form-label">固定电话：</div>
            <div class="fl">
                <input type="text" id="telephone" name="telephone" value="<?php echo $updatauserinfo['telephone'];?>" size="40" class="form-control form-control-small">
            </div>
            <span id="telephoneerr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="telephonemsg"></span></span>
        </div>
        <div class="form-group">
            <div class="fl form-label"><i class="require_item">*</i>QQ：</div>
            <div class="fl">
                <input type="text" id="qqnumber" name="qqnumber" size="40" value="<?php echo $updatauserinfo['qqnumber'];?>" class="form-control form-control-small">
            </div>
            <span id="qqnumbererr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="qqnumbermsg"></span></span>
        </div>

        <!-- 提交按钮 -->
        <div class="form-group">
            <div class="fl form-label" style="height:40px;"></div>
            <div class="fl">
                <button class="btn btn-primary btn-primary-noboder" id="accountsubmit" name="accountsubmit" type="button">提交审核</button>
                <button class="btn btn-default btn-default-noboder" name="submitbtn" id="submitbtn" type="button">撤销变更</button> 撤销变更后，将还原原有身份继续使用。
            </div>
        </div>
        </div>
        </form>
    <?php
    }
}
?>
        
 				</div>
            </div>
        </div>      
        
        
    </section>
</div>
<?php $this->load->view("manage/inc/footer");?>
<script language="javascript">
$(document).ready(function() {
	$("#formsubmit").click(function (){
		var contact = $("#contact").val()
		var mobile = $("#mobile").val()
		var address = $("#address").val()
		var telephone = $("#telephone").val()
		var qqnumber = $("#qqnumber").val()
		msgInit();
		$("#formsubmit").attr("disabled", "disabled");
		$.post("/manage/userinfo/doeditcontact", 
				{ 
    			contact : contact,
    			mobile : mobile,
    			address : address,
    			telephone : telephone,
    			qqnumber : qqnumber,
	   			r:Math.random()
				}, 
				function(data)
				{
	    			if(data && data['status'] == 0){
	        			if(data['data'] != '')
	        			{
	        				$("#"+data['data'][0]).html(data['data'][1]);
	            		}
	    				$("#"+data['info']).show();
	    		        $("#formsubmit").removeAttr("disabled");
	    				return false;
	    			}else{
		    			alert('修改成功！');
	    				location.reload();
	    			}
				},
				'json');
			return false;
			return;		
		});
});

function msgInit()
{
	$("#contacterr").hide()
	$("#mobileerr").hide()
	$("#addresserr").hide()
	$("#telephoneerr").hide()
	$("#qqnumbererr").hide()
}

$(document).ready(function() {
    //单选按钮
    $('#ownercerttype_wrap .radio_style').bind('click',function(e){
        $('#ownercerttype_wrap .radio_style').find('.icon').removeClass('active');
        $(this).find('.icon').addClass('active');
        $(this).find('input').attr('checked',true);
    });
    $('#certtype_wrap .radio_style').bind('click',function(e){
        $('#certtype_wrap .radio_style').find('.icon').removeClass('active');
        $(this).find('.icon').addClass('active');
        $(this).find('input').attr('checked',true);
    });
    $("#submitbtn").click(function (){
        $.post("/manage/userinfo/doupdatecancel",
            {
                r:Math.random()
            },
            function(data)
            {
                if(data && data['status'] == 0){
                    if(data['data'] != '')
                    {
                        alert(data['data']);
                    }
                }else{
                    alert('成功撤销升级！');
                }
                location.reload();
            },
            'json');
        return false;
    });
    
//初始化区域信息
    init();
    $("#mobile").blur(function (){
        mobile = $(this).val();
        $("#mobileerr").hide();
        if(mobile.length==0)
        {
            $("#mobileerr").show();
            $("#mobilemsg").html('请输入手机号码！');
            return ;
        }
        if(mobile.length!=11)
        {
            $("#mobileerr").show();
            $("#mobilemsg").html('请输入有效的手机号码！');
            return ;
        }

        var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
        if(!myreg.test(mobile))
        {
            $("#mobileerr").show();
            $("#mobilemsg").html('请输入有效的手机号码！');
            return ;
        }
    });

    $("#accountsubmit").click(function (){
        var accounttype = $('input:radio[name="accounttype"]:checked').val();
        var certtype = $('input:radio[name="certtype"]:checked').val();
        var realname = $("#realname").val()
        var id = $("#id").val()
        var idnumber = $("#idnumber").val()
        var companyname = $("#companyname").val()
        var companylicense = $("#companylicense").val()
        var companyid = $("#companyid").val()
        var individualname = $("#individualname").val()
        var individualid = $("#individualid").val()
        var ownername = $("#ownername").val()
        var ownercerttype = $('input:radio[name="ownercerttype"]:checked').val();
        var ownerid = $("#ownerid").val()
        var bankusername = $("#bankusername").val()
        var province = $("#province").val()
        var city = $("#city").val()
        var bank = $("#bank").val()
        var bankaddress = $("#bankaddress").val()
        var bankaccount = $("#bankaccount").val()
        var rebankaccount = $("#rebankaccount").val()
        var contact = $("#contact").val()
        var mobile = $("#mobile").val()
        var address = $("#address").val()
        var telephone = $("#telephone").val()
        var qqnumber = $("#qqnumber").val()
        var uploadify100 = $("#uploadify100").val()
        var uploadify200 = $("#uploadify200").val()
        var uploadify300 = $("#uploadify300").val()
        var uploadify400 = $("#uploadify400").val()
        var uploadify500 = $("#uploadify500").val()
        var uploadify600 = $("#uploadify600").val()
        msgInit();
        $("#accountsubmit").attr("disabled", "disabled");
        $.post("/manage/userinfo/doEditUpAccount",
            {
                accounttype : accounttype,
                certtype : certtype,
                id : id,
                realname : realname,
                idnumber : idnumber,
                companyname : companyname,
                companylicense : companylicense,
                companyid : companyid,
                individualname : individualname,
                individualid : individualid,
                ownername : ownername,
                ownercerttype : ownercerttype,
                ownerid : ownerid,
                bankusername : bankusername,
                province : province,
                city : city,
                bank : bank,
                bankaddress : bankaddress,
                bankaccount : bankaccount,
                rebankaccount : rebankaccount,
                contact : contact,
                mobile : mobile,
                address : address,
                telephone : telephone,
                qqnumber : qqnumber,
                uploadify100 : uploadify100,
                uploadify200 : uploadify200,
                uploadify300 : uploadify300,
                uploadify400 : uploadify400,
                uploadify500 : uploadify500,
                uploadify600 : uploadify600,
                r:Math.random()
            },
            function(data)
            {
                if(data && data['status'] == 0){
                    if(data['data'] != '')
                    {
                        $("#"+data['data'][0]).html(data['data'][1]);
                    }
                    $("#"+data['info']).show();
                    $("#accountsubmit").removeAttr("disabled");
                    return false;
                }else if(data && data['status'] == 1){
                    alert('提交成功，请等待审核！');
                    location.href = '/manage/userinfo/showaccountinfo';
                }
                else
                {
                    alert('未知错误，请联系管理员！');
                }
            },
            'json');
        return false;
        return;
    });


    $("#selectaccounttype .radio_style").click(function(event) {
        $('#selectaccounttype .radio_style').find('.icon').removeClass('active');
        $(this).find('.icon').addClass('active');
        $(this).find('input').attr('checked',true);
        var accounttype = $('input:radio[name="accounttype"]:checked').val();
        accounttypeTab(accounttype);
    });

    $("#realname").blur(function(){
        $("#bankusername").val($("#realname").val());
    })
    $("#companyname").blur(function(){
        $("#bankusername").val($("#companyname").val());
    })
    $("#ownername").blur(function(){
        $("#bankusername").val($("#ownername").val());
    })
        
});
	
	function msgInit()
	{
	    $("#accounttypeerr").hide()
	    $("#certtypeerr").hide()
	    $("#realnameerr").hide()
	    $("#idnumbererr").hide()
	    $("#companynameerr").hide()
	    $("#companylicenseerr").hide()
	    $("#companyiderr").hide()
	    $("#individualnameerr").hide()
	    $("#individualiderr").hide()
	    $("#ownernameerr").hide()
	    $("#ownercerttypeerr").hide()
	    $("#owneriderr").hide()
	    $("#bankusernameerr").hide()
	    $("#provinceerr").hide()
	    $("#bankerr").hide()
	    $("#bankaddresserr").hide()
	    $("#bankaccounterr").hide()
	    $("#rebankaccounterr").hide()
	    $("#contacterr").hide()
	    $("#mobileerr").hide()
	    $("#addresserr").hide()
	    $("#telephoneerr").hide()
	    $("#qqnumbererr").hide()
	    $("#uploadify100err").hide()
	    $("#uploadify200err").hide()
	    $("#uploadify300err").hide()
	    $("#uploadify400err").hide()
	    $("#uploadify500err").hide()
	    $("#uploadify600err").hide()
	}
	
	function ajaxUploder(fileObj, tpl) {
	    var obj 			= $(fileObj);
	    var fileId 			= obj.attr('id');
	    var image_show_box 	= obj.parents('div:eq(0)').find('.image_show_box');
	    var maindiv 		= obj.parents('div:eq(0)');
	    var loading = obj.siblings('.onload').show();
	    $.ajaxFileUpload( {
	        url : '/manage/indexreg/ajaxDoUpload/',
	        secureuri : true,
	        fileElementId : fileId,
	        dataType : 'json',// 服务器返回的格式，可以是json
	        success : function(data, status) {
	            $("#iconimgerr").hide();
	            if(data['status'])
	            {
	                var str = '<div class="'+tpl+'image_show_box">';
	                str+='<img src="/public/upload'+data['data']+'" style="width:300px;height:200px;" /><a href="/public/upload'+data['data']+'" target="_blank">查看实际效果</a>';
	                str+='<input type="hidden" id="uploadify'+tpl+'" name="uploadify'+tpl+'" value="'+data['data']+'"/>';
	                str += '&nbsp;&nbsp;</div>';
	                maindiv.find('.'+tpl+'image_show_box').remove();// 将刚才上传的干掉
	                maindiv.append(str);
	                maindiv.find('.'+tpl+'_image_show_box').fadeIn("slow");
	            }
	            else
	            {
	                alert(data['data']);
	            }
	            loading.hide();
	            return true;
	        },
	        error : function(data, status, e) {
	            loading.hide();
	        }
	    })
	}
	
	function accounttypeTab(accounttype){
	    if(accounttype == 1)
	    {
	        $(".accounttype1").show();
	        $(".accounttype2").hide();
	        $(".accounttype3").hide();
	    }
	    else if(accounttype == 2)
	    {
	        $(".accounttype2").show();
	        $(".accounttype1").hide();
	        $(".accounttype3").hide();
	    }
	    else if(accounttype == 3)
	    {
	        $(".accounttype3").show();
	        $(".accounttype1").hide();
	        $(".accounttype2").hide();
	    }
	}

    
    var where = new Array(34);
    function comefrom(loca,locacity) { this.loca = loca; this.locacity = locacity; }
    where[0]= new comefrom("请选择省份名","请选择城市名");
    where[1] = new comefrom("北京","请选择|东城|西城|崇文|宣武|朝阳|丰台|石景山|海淀|门头沟|房山|通州|顺义|昌平|大兴|平谷|怀柔|密云|延庆");  //欢迎来到站长特效网，我们的网址是www.zzjs.net，很好记，zz站长，js就是js特效，本站收集大量高质量js代码，还有许多广告代码下载。
    where[2] = new comefrom("上海","请选择|黄浦|卢湾|徐汇|长宁|静安|普陀|闸北|虹口|杨浦|闵行|宝山|嘉定|浦东|金山|松江|青浦|南汇|奉贤|崇明");//欢迎来到站长特效网，我们的网址是www.zzjs.net，很好记，zz站长，js就是js特效，本站收集大量高质量js代码，还有许多广告代码下载。
    where[3] = new comefrom("天津","请选择|和平|东丽|河东|西青|河西|津南|南开|北辰|河北|武清|红挢|塘沽|汉沽|大港|宁河|静海|宝坻|蓟县");
    where[4] = new comefrom("重庆","请选择|万州|涪陵|渝中|大渡口|江北|沙坪坝|九龙坡|南岸|北碚|万盛|双挢|渝北|巴南|黔江|长寿|綦江|潼南|铜梁|大足|荣昌|壁山|梁平|城口|丰都|垫江|武隆|忠县|开县|云阳|奉节|巫山|巫溪|石柱|秀山|酉阳|彭水|江津|合川|永川|南川");
    where[5] = new comefrom("河北","请选择|石家庄|邯郸|邢台|保定|张家口|承德|廊坊|唐山|秦皇岛|沧州|衡水");
    where[6] = new comefrom("山西","请选择|太原|大同|阳泉|长治|晋城|朔州|吕梁|忻州|晋中|临汾|运城");
    where[7] = new comefrom("内蒙古","请选择|呼和浩特|包头|乌海|赤峰|呼伦贝尔盟|阿拉善盟|哲里木盟|兴安盟|乌兰察布盟|锡林郭勒盟|巴彦淖尔盟|伊克昭盟");
    where[8] = new comefrom("辽宁","请选择|沈阳|大连|鞍山|抚顺|本溪|丹东|锦州|营口|阜新|辽阳|盘锦|铁岭|朝阳|葫芦岛");
    where[9] = new comefrom("吉林","请选择|长春|吉林|四平|辽源|通化|白山|松原|白城|延边");
    where[10] = new comefrom("黑龙江","请选择|哈尔滨|齐齐哈尔|牡丹江|佳木斯|大庆|绥化|鹤岗|鸡西|黑河|双鸭山|伊春|七台河|大兴安岭");
    where[11] = new comefrom("江苏","请选择|南京|镇江|苏州|南通|扬州|盐城|徐州|连云港|常州|无锡|宿迁|泰州|淮安");
    where[12] = new comefrom("浙江","请选择|杭州|宁波|温州|嘉兴|湖州|绍兴|金华|衢州|舟山|台州|丽水");
    where[13] = new comefrom("安徽","请选择|合肥|芜湖|蚌埠|马鞍山|淮北|铜陵|安庆|黄山|滁州|宿州|池州|淮南|巢湖|阜阳|六安|宣城|亳州");
    where[14] = new comefrom("福建","请选择|福州|厦门|莆田|三明|泉州|漳州|南平|龙岩|宁德");
    where[15] = new comefrom("江西","请选择|南昌市|景德镇|九江|鹰潭|萍乡|新馀|赣州|吉安|宜春|抚州|上饶");
    where[16] = new comefrom("山东","请选择|济南|青岛|淄博|枣庄|东营|烟台|潍坊|济宁|泰安|威海|日照|莱芜|临沂|德州|聊城|滨州|菏泽");
    where[17] = new comefrom("河南","请选择|郑州|开封|洛阳|平顶山|安阳|鹤壁|新乡|焦作|濮阳|许昌|漯河|三门峡|南阳|商丘|信阳|周口|驻马店|济源");
    where[18] = new comefrom("湖北","请选择|武汉|宜昌|荆州|襄樊|黄石|荆门|黄冈|十堰|恩施|潜江|天门|仙桃|随州|咸宁|孝感|鄂州");
    where[19] = new comefrom("湖南","请选择|长沙|常德|株洲|湘潭|衡阳|岳阳|邵阳|益阳|娄底|怀化|郴州|永州|湘西|张家界");
    where[20] = new comefrom("广东","请选择|广州|深圳|珠海|汕头|东莞|中山|佛山|韶关|江门|湛江|茂名|肇庆|惠州|梅州|汕尾|河源|阳江|清远|潮州|揭阳|云浮");
    where[21] = new comefrom("广西","请选择|南宁|柳州|桂林|梧州|北海|防城港|钦州|贵港|玉林|南宁地区|柳州地区|贺州|百色|河池");
    where[22] = new comefrom("海南","请选择|海口|三亚");
    where[23] = new comefrom("四川","请选择|成都|绵阳|德阳|自贡|攀枝花|广元|内江|乐山|南充|宜宾|广安|达川|雅安|眉山|甘孜|凉山|泸州");
    where[24] = new comefrom("贵州","请选择|贵阳|六盘水|遵义|安顺|铜仁|黔西南|毕节|黔东南|黔南");
    where[25] = new comefrom("云南","请选择|昆明|大理|曲靖|玉溪|昭通|楚雄|红河|文山|思茅|西双版纳|保山|德宏|丽江|怒江|迪庆|临沧");
    where[26] = new comefrom("西藏","请选择|拉萨|日喀则|山南|林芝|昌都|阿里|那曲");
    where[27] = new comefrom("陕西","请选择|西安|宝鸡|咸阳|铜川|渭南|延安|榆林|汉中|安康|商洛");
    where[28] = new comefrom("甘肃","请选择|兰州|嘉峪关|金昌|白银|天水|酒泉|张掖|武威|定西|陇南|平凉|庆阳|临夏|甘南");
    where[29] = new comefrom("宁夏","请选择|银川|石嘴山|吴忠|固原");
    where[30] = new comefrom("青海","请选择|西宁|海东|海南|海北|黄南|玉树|果洛|海西");
    where[31] = new comefrom("新疆","请选择|乌鲁木齐|石河子|克拉玛依|伊犁|巴音郭勒|昌吉|克孜勒苏柯尔克孜|博尔塔拉|吐鲁番|哈密|喀什|和田|阿克苏");
    where[32] = new comefrom("香港","香港");
    where[33] = new comefrom("澳门","澳门");
    where[34] = new comefrom("台湾","请选择|台北|高雄|台中|台南|屏东|南投|云林|新竹|彰化|苗栗|嘉义|花莲|桃园|宜兰|基隆|台东|金门|马祖|澎湖");

    function select() {
        with(document.creator.province) { var loca2 = options[selectedIndex].value; }
        for(i = 0;i < where.length;i ++) {
            if (where[i].loca == loca2) {
                loca3 = (where[i].locacity).split("|");
                for(j = 0;j < loca3.length;j++) {with(document.creator.city) { length = loca3.length; options[j].text = loca3[j]; options[j].value = loca3[j]; var loca4=options[selectedIndex].value;}}
                break;
            }}
    }


    function init() {
        with(document.creator.province) {
            length = where.length;
            for(k=0;k<where.length;k++) { options[k].text = where[k].loca; options[k].value = where[k].loca; }
            options[selectedIndex].text = where[0].loca; options[selectedIndex].value = where[0].loca;
        }
        with(document.creator.city) {
            loca3 = (where[0].locacity).split("|");
            length = loca3.length;
            for(l=0;l<length;l++) { options[l].text = loca3[l]; options[l].value = loca3[l]; }
            options[selectedIndex].text = loca3[0]; options[selectedIndex].value = loca3[0];
        }
        <?php
                if(isset($updatauserinfo) && $updatauserinfo['auditstatus'] == 2){
            ?>
        var province = "<?php echo $updatauserinfo['province'];?>",
            city = "<?php echo $updatauserinfo['city'];?>";
        if(province != "" && city != ""){
            $("#province").val(province);
            select()
            $("#city").val(city);
        };
        <?php }?>
    }



    accounttypeTab($('input:radio[name="accounttype"]:checked').val());


    $('#newAppCue .close').click(function(){
        $("#newAppCue").fadeOut();
    });

</script>

<!--  主体部分 [ -->
<div class="inner_container" id="innerContainer">
    <?php $this->load->view('manage/user/left')?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <div class="jumbotron jumbotron_no_shadow edit_info">
                    <header class="title">
                        <h3><strong  class="square_box"></strong><span>修改密码</span></h3>
                    </header>
                    <br>
                    <br>
                    <div class="form-group">
                        <div class="fl form-label">注册邮箱:</div>
                        <div class="fl font"><?php echo $userinfo['username']?></div>
                    </div>
                    <!--  -->
                    <div class="form-group">
                        <div class="fl form-label"><i class="require_item">*</i>当前密码:</div>
                        <div class="fl">
                            <input type="password" size="40" id="oldpwd" class="form-control form-control-small" name="oldpwd" placeholder="请填写当前密码" >
                        </div>
                        <span id="oldpwderr" class="fl alert alert-small alert-warning" style="display: none;"><i class="icon error_icon"></i><span id="oldpwdmsg"></span></span>
                    </div>
                    <!--  -->
                    <div class="form-group">
                        <div class="fl form-label"><i class="require_item">*</i>新密码：</div>
                        <div class="fl">
                            <input type="password" size="40" id="newpwd" class="form-control form-control-small" name="newpwd" placeholder="密码不能少于6位或大于16位！" >
                        </div>
                        <span id="newpwderr" class="fl alert alert-small alert-warning" style="display: none;"><i class="icon error_icon"></i><span id="newpwdmsg"></span></span>
                    </div>
                    <!--  -->
                    <div class="form-group">
                        <div class="fl form-label"><i class="require_item">*</i>确认密码：</div>
                        <div class="fl">
                            <input type="password" size="40" id="renewpwd" class="form-control form-control-small" name="renewpwd" placeholder="密码不能少于6位或大于16位！" >
                        </div>
                        <span id="renewpwderr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="renewpwdmsg"></span></span>
                    </div>
                    <!-- 提交按钮 -->
                    <div class="form-group">
                        <div class="fl form-label" style="height: 40px;"></div>
                        <div class="fl"><button class="btn btn-primary btn-primary-noboder" type="button" id="formsubmit" name="formsubmit">&nbsp;&nbsp;保&nbsp;&nbsp;存&nbsp;&nbsp;</button></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php $this->load->view("manage/inc/footer");?>
<script>
$("#formsubmit").click(function(e){
	var oldpwd = $("#oldpwd").val();
	var newpwd = $("#newpwd").val();
	var renewpwd = $("#renewpwd").val();
	$("#oldpwderr").hide();
	$("#newpwderr").hide();
	$("#renewpwderr").hide();
	$("#formsubmit").attr("disabled", 'disabled');
	$.post("/manage/userinfo/doeditpwd/", {oldpwd:oldpwd, newpwd:newpwd, renewpwd:renewpwd}, function(data){
		if(data && data['status'] == 0){
			if(data['data'] != '')
			{
				$("#"+data['data'][0]).html(data['data'][1]);
    		}
			$("#"+data['info']).show();
	        $("#formsubmit").removeAttr("disabled");
			return false;
		}else{
			alert('修改成功！');
			location.href="/manage/userinfo/index";
		}
		}, 'json');
});
</script>

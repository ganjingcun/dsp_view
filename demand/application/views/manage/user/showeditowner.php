<!--  主体部分 [ -->
<div class="inner_container" id="innerContainer">
    <?php $this->load->view('manage/user/left')?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <!-- 内容部分 -->
                <div class="jumbotron jumbotron_no_shadow" >
                    <header class="title">
                        <h3><strong class="square_box"></strong><span>身份验证</span></h3>
                    </header>

                    <div class="common_font" style="padding-left: 100px;">
                        
                        <div class="form-group">
                            <div class="fl form-label">个体工商名称:</div>
                            <div class="fl font-con" style="margin-bottom: 10px;">
    							<input type="hidden" value="<?php echo $accountinfo['id'];?>" name="id" id="id">
	                            <input type="text" size="50" id="individualname" name="individualname" class="form-control form-control-small" value="<?php echo $accountinfo['individualname']?>"  placeholder="请填写个体工商名称。" />                        
							</div>
							 <span id="individualnameerr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="individualnamemsg"></span></span>
                        </div>
                        <!--  -->
                        <div class="form-group">
                            <div class="fl form-label">营业执照号:</div>
                            <div class="fl font-con" style="margin-bottom: 10px;">
                            	<input type="text" size="50" id="individualid" name="individualid" class="form-control form-control-small" value="<?php echo $accountinfo['individualid']?>" placeholder="请填写营业执照号。" />                        	
                            </div>
                            <span id="individualiderr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="individualidmsg"></span></span>
                        </div>
                        
                        <div class="form-group">
                            <div class="fl form-label"><i class="require_item">*</i>营业执照:</div>
                            <div class="fl file_box" style="margin-bottom: 10px;margin-top:10px;">
                            	<input type="file" name='Filedata'  id="uploadify40" class='uploadify fl'  onchange='ajaxUploder(this,400)'/>
                                <div class="fl"><label class="cueinfo">图片格式必须为png或jpg，文件大小不超过2M。</label></div>
                                <br style="clear:left">
                                <div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
                                <div class="400image_show_box">
                                     <img style="width:300px;height:200px; margin-top: 15px;" src="/public/upload/<?php echo $accountinfo['uploadify400']?>">
                                     <input type="hidden" value="<?php echo $accountinfo['uploadify400'];?>" name="uploadify400" id="uploadify400"/>
                                     <a target="_blank" href="/public/upload<?php echo $accountinfo['uploadify400'];?>">查看实际效果</a>
                                </div>
                             </div>
                             <span id="uploadify400err" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="uploadify400msg"></span></span>
                        </div>
                        <!--  -->
                        <div class="form-group">
                            <div class="fl form-label">业主姓名:</div>
                            <div class="fl font-con" style="margin-bottom: 10px;margin-top:10px;">
                           		<input type="text" size="50" id="ownername" name="ownername" class="form-control form-control-small" value="<?php echo $accountinfo['ownername']?>" placeholder="请填写业主姓名。" />
                            </div>
                            <span id="ownernameerr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="ownernamemsg"></span></span>
                        </div>
                        
                        <div class="form-group">
                                    <div class="fl form-label"><i class="require_item">*</i>证件类型：</div>
                                    <div class="fl radio-group" id="ownercerttype_wrap" style="margin-bottom: 10px;margin-top:10px;">
                                        <label class="radio_style"><i class="icon radio_icon <?php if($accountinfo['ownercerttype']==1){echo 'active';}?>"></i><input style="display: none;" type="radio" name="ownercerttype" id="ownercerttype" <?php if($accountinfo['ownercerttype']==1){echo 'checked';}?> value="1" >中国大陆身份证</label>
                                        <label class="radio_style"><i class="icon radio_icon <?php if($accountinfo['ownercerttype']==2){echo 'active';}?>"></i><input style="display: none;" type="radio" name="ownercerttype" id="ownercerttype" <?php if($accountinfo['ownercerttype']==2){echo 'checked';}?> value="2" >中国港澳台身份证</label>
                                        <label class="radio_style"><i class="icon radio_icon <?php if($accountinfo['ownercerttype']==3){echo 'active';}?>"></i><input style="display: none;" type="radio" name="ownercerttype" id="ownercerttype" <?php if($accountinfo['ownercerttype']==3){echo 'checked';}?>  value="3" >海外证件</label>
                                    </div>
                                    <span id="ownercerttypeerr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="ownercerttypemsg"></span></span>
                                </div>
                        <!--  -->
                        <div class="form-group">
                            <div class="fl form-label">证件号码:</div>
                            <div class="fl font-con" style="margin-bottom: 10px;">
                            	<input type="text" size="50" id="ownerid" name="ownerid" class="form-control form-control-small" value="<?php echo $accountinfo['ownerid']?>" placeholder="请填写证件号码。" />
                            </div>
                             <span id="owneriderr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="owneridmsg"></span></span>
                        </div>
                        <div class="form-group">
                            <div class="fl form-label"><i class="require_item">*</i>证件正面:</div>
                            <div class="fl file_box" style="margin-bottom: 10px;margin-top:10px;">
                            	<input type="file" name='Filedata'  id="uploadify50" class='uploadify fl'  onchange='ajaxUploder(this,500)'/>
                                <div class="fl"><label class="cueinfo">图片格式必须为png或jpg，文件大小不超过2M。</label></div>
                                <br style="clear:left">
                                <div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
                                <div class="500image_show_box">
                                     <img style="width:300px;height:200px; margin-top: 15px;" src="/public/upload/<?php echo $accountinfo['uploadify500']?>">
                                     <input type="hidden" value="<?php echo $accountinfo['uploadify500'];?>" name="uploadify500" id="uploadify500"/>
                                     <a target="_blank" href="/public/upload<?php echo $accountinfo['uploadify500'];?>">查看实际效果</a>
                                </div>
                             </div>
                             <span id="uploadify500err" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="uploadify500msg"></span></span>
                        </div>
                        
                        <div class="form-group">
                            <div class="fl form-label"><i class="require_item">*</i>证件背面:</div>
                            <div class="fl file_box" style="margin-bottom: 10px;margin-top:10px;">
                            	<input type="file" name='Filedata'  id="uploadify60" class='uploadify fl'  onchange='ajaxUploder(this,600)'/>
                                <div class="fl"><label class="cueinfo">图片格式必须为png或jpg，文件大小不超过2M。</label></div>
                                <br style="clear:left">
                                <div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
                                <div class="600image_show_box">
                                     <img style="width:300px;height:200px; margin-top: 15px;" src="/public/upload/<?php echo $accountinfo['uploadify600']?>">
                                     <input type="hidden" value="<?php echo $accountinfo['uploadify600'];?>" name="uploadify600" id="uploadify600"/>
                                     <a target="_blank" href="/public/upload<?php echo $accountinfo['uploadify600'];?>">查看实际效果</a>
                                </div>
                             </div>
                             <span id="uploadify600err" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="uploadify600msg"></span></span>
                        </div>
                         <!-- 提交按钮 -->
                            <div class="form-group">
                                <div class="fl form-label" style="height: 40px;"></div>
                                <div class="fl" style="margin-bottom: 10px;margin-top:10px;">
                                    <button class="btn btn-primary btn-primary-noboder" id="formsubmit" name="formsubmit" type="button">提交审核</button>
                                    <button type="button" class="btn btn-default btn-default-noboder" style="margin-left:50px"  onclick="history.back();">取&nbsp;&nbsp;&nbsp;&nbsp;消</button>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php $this->load->view("manage/inc/footer");?>
<script>

//单选按钮
$('#ownercerttype_wrap .radio_style').bind('click',function(e){
    $('#ownercerttype_wrap .radio_style').find('.icon').removeClass('active');
    $(this).find('.icon').addClass('active');
    $(this).find('input').attr('checked',true);
});
$(document).ready(function() {
	$("#formsubmit").click(function (){
		var id = $("#id").val();
		var individualname = $("#individualname").val();
		var individualid=$("#individualid").val();
		var ownername=$("#ownername").val();
		var ownerid=$("#ownerid").val();
		var ownercerttype = $('input:radio[name="ownercerttype"]:checked').val();
		var uploadify400 = $("#uploadify400").val();
		var uploadify500 = $("#uploadify500").val();
		var uploadify600 = $("#uploadify600").val();
		msgInit();
		$("#formsubmit").attr("disabled", "disabled");
		$.ajax({url:"/manage/userinfo/doeidtowner",
			type:"POST",
			data:{
				id : id,
				individualname : individualname,
				individualid : individualid,
				ownername : ownername,
				ownerid : ownerid,
				ownercerttype : ownercerttype,
				uploadify400 : uploadify400,
				uploadify500 : uploadify500,
				uploadify600 : uploadify600,
		   		r:Math.random(),
			},
			success:function(data){
				var dataObj = JSON.parse(data);
				if(dataObj && dataObj.status == 0){
	        		if(dataObj.data != '')
	        		{
	        			$("#"+dataObj.data[0]).html(dataObj.data[1]);
	            	}
	        		if(dataObj.info != '')
	        		{
	        			$("#"+dataObj.info).show();
	        		}
	    	        $("#formsubmit").removeAttr("disabled");
	    			return false;
	    		}else if(dataObj && dataObj.status == 1){
	    			alert('提交成功，请等待审核！');
	    			location.href = '/manage/userinfo/showaccountinfo';
	    		}
	    		else
	    		{
					alert('未知错误！');
	    		}
			},
		});	
	});	
});

function msgInit()
{
	$("#individualnameerr").hide();
	$("#individualiderr").hide();
	$("#ownernameerr").hide();
	$("#owneriderr").hide();
	$("#ownercerttypeerr").hide();
	$("#uploadify400err").hide();
	$("#uploadify500err").hide();
	$("#uploadify600err").hide();
}
function ajaxUploder(fileObj, tpl) {
	var obj 			= $(fileObj);
	var fileId 			= obj.attr('id');
	var image_show_box 	= obj.parents('div:eq(0)').find('.image_show_box');
	var maindiv 		= obj.parents('div:eq(0)');
	var loading = obj.siblings('.onload').show();
	$.ajaxFileUpload( {
				url : '/manage/indexreg/ajaxDoUpload/',
				secureuri : true,
				fileElementId : fileId,
				dataType : 'json',// 服务器返回的格式，可以是json
				success : function(data, status) {
        			$("#iconimgerr").hide();
					if(data['status'])
					{
						var str = '<div class="'+tpl+'image_show_box">';
						str+='<img src="/public/upload'+data['data']+'" style="width:300px;height:200px;" /><a href="/public/upload'+data['data']+'" target="_blank">查看实际效果</a>';
						str+='<input type="hidden" id="uploadify'+tpl+'" name="uploadify'+tpl+'" value="'+data['data']+'"/>';
						str += '&nbsp;&nbsp;</div>';
						maindiv.find('.'+tpl+'image_show_box').remove();// 将刚才上传的干掉
						maindiv.append(str);
						maindiv.find('.'+tpl+'_image_show_box').fadeIn("slow");
						}
					else
					{
						alert(data['data']);
						}
					loading.hide();
					return true;
				},
				error : function(data, status, e) {
					loading.hide();
				}
			})
}
</script>

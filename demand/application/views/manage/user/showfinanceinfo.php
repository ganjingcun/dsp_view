<!--  主体部分 [ -->
<div class="inner_container" id="innerContainer">
    <?php $this->load->view('manage/user/left')?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <div class="jumbotron jumbotron_no_shadow">
                    <header class="title">
                        <h3><strong  class="square_box"></strong><span>收款信息管理</span></h3>
                    </header>
                    <div style="padding-left: 100px;" class="common_font">
                        <div class="form-group">
                            <span class="form-label fl">收款方名:</span>
                            <span class="fl font-con"><?php echo $bankusername;?></span>
                        </div>
                        <div class="form-group">
                            <span class="form-label fl">收款币种:</span>
                            <span class="fl font-con"><?php echo $currency;?></span>
                        </div>
                        <div class="form-group">
                            <span class="form-label fl">所在地区:</span>
                            <span class="fl font-con"><?php echo $province;?> &nbsp;<?php echo $city;?></span>
                        </div>
                        <div class="form-group">
                            <span class="form-label fl">开户银行:</span>
                            <span class="fl font-con"><?php echo $bank;?></span>
                        </div>
                        <div class="form-group">
                            <span class="form-label fl">开户银行地址:</span>
                            <span class="fl font-con"><?php echo $bankaddress;?></span>
                        </div>
                        <div class="form-group">
                            <span class="form-label fl">银行账号:</span>
                            <span class="fl font-con"><?php echo $bankaccount;?></span>
                        </div>
                        <div class="form-group">
                            <span class="form-label fl" style="height: 40px;"></span>
                            <span class="fl font-con">
<!--                            		<a href="/manage/userinfo/showfinancedetail" class="btn btn-primary btn-primary-noboder"><i class="icon icon_edit_white_con"></i>编 辑</a>-->
                            	<?php
				        		if($auditstatus == 1)
				        		{ ?>
                            		<a href="/manage/userinfo/showfinancedetail" class="btn btn-primary btn-primary-noboder"><i class="icon icon_edit_white_con"></i>编 辑</a>
                            	<?php
				        		}
				        		else{
				        		?>
				        			<a href="/manage/userinfo/showfinancedetail" class="btn btn-primary btn-primary-noboder" disabled="disabled"><i class="icon icon_edit_white_con"></i>编 辑</a>
				        		<?php }?>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php $this->load->view("manage/inc/footer");?>
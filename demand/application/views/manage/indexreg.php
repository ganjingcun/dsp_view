<!--  主体部分 [ -->
<div class="indexreg_container">
    <div class="padding_box"></div>
<!--     <div class="register_instruction jumbotron jumbotron_no_shadow">
        <header class="title">
            <h3 class="title_normal">
                <strong class="square_box"></strong>
                <span>注册说明</span>
            </h3>
        </header>
        <br>
        <ol>
            <li>1、为了给您提供更好的服务，请填写真实资料。我们将会对您的资料进行审核，通过审核后方可使用CocosAds。</li>
            <li>2、上海拓畅信息技术有限公司对个人开发者每月收入进行结算审核，核对通过后，结算收入将于结算月的下一个自然月自动定期支付给个人开发者。</li>
            <li>3、上海拓畅信息技术有限公司会按照中国个人所得税关于劳务报酬的相关法案，根据收入对个人开发者代扣代缴个税，具体规则请查看<a href="http://www.chinatax.gov.cn/n8136506/n8136593/n8137681/n8225749/8450257.html" target="_blank">《国税总局关于劳务报酬个税的说明》</a></li>
            <li>4、为了保障您的权益，建议您预估广告收入；若您的产品可能产生较高额度收入，建议您以企业或个体工商户身份完成注册。</li>
            <li>5、我们会保护您的隐私资料，绝不会向第三方透露您的个人资料。</li>
        </ol>
    </div> -->
    <form action="" id="creator" method="post" name="creator" >
        <?php if($isempty){  ?>
            <div class="active_success_info" id="newAppCue" style="width:100%;">
                <i class="icon icon_active_success_loger"></i>
                您提交的信息因以下原因被驳回：<?php echo $reason?>
                <a href="javascript:void(0);" class="close">×</a>
            </div>
            <input type="hidden" value="<?php echo $id;?>" id="id" name="id">
        <?php }?>
        <div class="active_success_info">
            <span class="content">激活成功，为了您的资产安全，请您完善个人信息！</span>
            <i class="icon icon_active_success_loger"></i>
        </div>
        <br>
        <br>

        <section class="person_detail indexreg_form_box">
            <header class="title">
                <h3 class="title_normal">
                    <strong class="square_box"></strong>
                    <span>身份详细信息</span>
                </h3>
            </header>
            <div class="box">
                <div class="form-group">
                    <span class="fl form-label"><i class="require_item">*</i>注册邮箱：</span>
                    <span class="fl form-control-content"><?php echo $userinfo['username'];?></span>
                </div>
                <div class="form-group">
                    <span class="fl form-label"><i class="require_item">*</i>身份类型：</span>
                    <span class="fl radio-group" id="selectaccounttype">
                        <label class="radio_style"><i class="icon radio_icon <?php if($isempty == false || ($isempty && $accounttype==1)){echo 'active'; }?>"></i><input style="display: none;" name="accounttype" type="radio"  value="1" <?php if($isempty && $accounttype==1){echo 'checked="checked"';} if($isempty == false){echo 'checked="checked"';}?> >个人/团体</label>
                        <label class="radio_style"><i class="icon radio_icon <?php if($isempty && $accounttype==2){echo 'active'; }?>"></i><input style="display: none;" name="accounttype" type="radio"  value="2" <?php if($isempty && $accounttype==2){echo 'checked="checked"';}?>>公司</label>
                        <label class="radio_style"><i class="icon radio_icon <?php if($isempty && $accounttype==3){echo 'active'; }?>"></i><input style="display: none;" name="accounttype" type="radio"  value="3" <?php if($isempty && $accounttype==3){echo 'checked="checked"';}?>>个体工商户</label>
                    </span>
                    <span id="accounttypeerr" class="fl alert alert-small alert-warning" style="display:none; "><i class="icon error_icon"></i><span id="accounttypemsg"></span></span>
                </div>

                <div class="form-group accounttype2">
                    <div class="info">
                        *注册为公司，提现时必须向CocosAds开具发票，如无法开发票，请注册为个人/团体
                    </div>
                </div>
                <div class="form-group accounttype3">
                    <div class="info">
                        *注册为个体工商户，提现时必须向CocosAds开具发票，如无法开发票，请注册为个人/团体
                    </div>
                </div>

                <!-- 个人团体 -->
                <div class="accounttype1 accounttype_wrap" >
                    <div class="form-group">
                        <span class="fl form-label"><i class="require_item">*</i>证件类型：</span>
                        <span class="fl radio-group" id="certtype_wrap">
                            <label class="radio_style"><i class="icon radio_icon <?php if($isempty && $certtype==1){echo 'active'; }?> "></i><input style="display: none;" name="certtype" type="radio" value="1" <?php if($isempty && $certtype==1){echo 'checked="checked"';}?>>中国大陆身份证</label>
                            <label class="radio_style"><i class="icon radio_icon <?php if($isempty && $certtype==2){echo 'active'; }?> "></i><input style="display: none;" name="certtype" type="radio" value="2" <?php if($isempty && $certtype==2){echo 'checked="checked"';}?>>中国港澳台身份证</label>
                            <label class="radio_style"><i class="icon radio_icon <?php if($isempty && $certtype==3){echo 'active'; }?> "></i><input style="display: none;" name="certtype" type="radio" value="3" <?php if($isempty && $certtype==3){echo 'checked="checked"';}?>>海外证件</label>
                        </span>
                        <span id="certtypeerr"  class="fl alert alert-small alert-warning" style="display:none;"><i class="icon error_icon"></i><span id="certtypemsg"></span></span>
                    </div>

                    <div class="form-group">
                        <span class="fl form-label"><i class="require_item">*</i>证件姓名：</span>
                        <span class="fl">
                            <input type="text" size="50"  id="realname" name="realname" class="form-control form-control-small" value="<?php if($isempty){echo $realname;}?>" placeholder="请如实填写，同时也是您支付账号的开户人">
                        </span>
                        <span id="realnameerr" class="fl alert alert-small alert-warning" style="display:none;"><i class="icon error_icon"></i><span id="realnamemsg"></span></span>
                    </div>

                    <div class="form-group">
                        <span class="fl form-label"><i class="require_item">*</i>证件号码：</span>
                        <span class="fl">
                            <input type="text" size="50" id="idnumber" name="idnumber" class="form-control form-control-small" value="<?php if($isempty){echo $idnumber;}?>" placeholder="请填写证件号码">
                        </span>
                        <span id="idnumbererr" class="fl alert alert-small alert-warning" style="display:none;"><i class="icon error_icon"></i><span id="idnumbermsg"></span></span>
                    </div>

                    <div class="form-group">
                        <span class="fl form-label"><i class="require_item">*</i>证件正面：</span>
                        <span class="fl form-file">
                            <input type="file"  name='Filedata'  id="uploadify10" class='uploadify' onchange='ajaxUploder(this,100)'>
                        </span>
                        <span class="form-file-label fl">图片格式必须为png或jpg，文件大小不超过2M。</span>
                        <span id="uploadify100err" class="fl alert alert-small alert-warning" style="display:none;"><i class="icon error_icon"></i><span id="uploadify100msg"></span></span>
                        <br style="clear:left;">
                        <div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
                        <?php if($isempty && !empty($uploadify100))
                        {
                            ?>
                            <div class="100image_show_box">
                                <img style="width:300px;height:200px;" src="/public/upload<?php if($isempty && !empty($uploadify100)){echo $uploadify100;}?>">
                                <input type="hidden" value="<?php if($isempty && !empty($uploadify100)){echo $uploadify100;}?>" name="uploadify100" id="uploadify100">
                                <a target="_blank" href="/public/upload<?php if($isempty && !empty($uploadify100)){echo $uploadify100;}?>">查看实际效果</a>
                            </div>
                        <?php }?>
                    </div>

                    <div class="form-group">
                        <span class="fl form-label"><i class="require_item">*</i>证件反面：</span>
                        <span class="fl form-file">
                            <input type="file"  name='Filedata'  id="uploadify20" class='uploadify'  onchange='ajaxUploder(this,200)'>
                        </span>
                        <span class="form-file-label fl">图片格式必须为png或jpg，文件大小不超过2M。</span>
                        <span id="uploadify200err" class="fl alert alert-small alert-warning" style="display:none;"><i class="icon error_icon"></i><span id="uploadify200msg"></span></span>
                        <br style="clear:left;">
                        <div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
                        <?php if($isempty && !empty($uploadify200))
                        {
                            ?>
                            <div class="200image_show_box">
                                <img style="width:300px;height:200px;" src="/public/upload<?php if($isempty && !empty($uploadify200)){echo $uploadify200;}?>">
                                <input type="hidden" value="<?php if($isempty && !empty($uploadify200)){echo $uploadify200;}?>" name="uploadify200" id="uploadify200">
                                <a target="_blank" href="/public/upload<?php if($isempty && !empty($uploadify100)){echo $uploadify200;}?>">查看实际效果</a>
                            </div>
                        <?php }?>
                    </div>
                </div>
                <!-- 公司 -->
                <div class="accounttype2 accounttype_wrap" style="display: none;">
                    <div class="form-group">
                        <span class="form-label fl"><span class="require_item">*</span>公司全称：</span>
                        <span class="fl">
                            <input type="text" size="50" id="companyname" name="companyname" class="form-control form-control-small" value="<?php if($isempty){echo $companyname;}?>" placeholder="请填写与营业执照一致的合法公司全称" >
                        </span>
                        <span id="companynameerr" class="fl alert alert-small alert-warning" style="display:none;"><i class="icon error_icon"></i><span id="companynamemsg"></span></span>
                    </div>
                    <div class="form-group">
                        <span class="form-label fl"><span class="require_item">*</span>营业执照号：</span>
                        <span class="fl">
                            <input type="text" size="50" id="companylicense" name="companylicense" class="form-control form-control-small" value="<?php if($isempty){echo $companylicense;}?>"  placeholder="请填写营业执照号">
                        </span>
                        <span id="companylicenseerr" class="fl alert alert-small alert-warning" style="display:none;"><i class="icon error_icon"></i><span id="companylicensemsg"></span></span>
                    </div>
                    <div class="form-group">
                        <span class="form-label fl"><span class="require_item">*</span>组织机构代码：</span>
                        <span class="fl">
                            <input type="text" size="50" id="companyid" name="companyid" class="form-control form-control-small"  value="<?php if($isempty){echo $companyid;}?>"  placeholder="请填写组织机构代码">
                        </span>
                         <a href="http://news.jwb.com.cn/art/2015/10/2/art_19907_5901212.html" class="fl alert-small" target="_blank">《组织机构代码三码合一》</a>
                        <span id="companyiderr" class="fl alert alert-small alert-warning" style="display:none;"><i class="icon error_icon"></i><span id="companyidmsg"></span></span>
                    </div>
                    <div class="form-group">
                        <span class="form-label fl"><span class="require_item">*</span>营业执照：</span>
                        <span class="fl form-file">
                            <input type="file" name='Filedata'  id="uploadify30" class='uploadify'  onchange='ajaxUploder(this,300)'/>&nbsp;&nbsp; <br/>
                        </span>
                        <span class="form-file-label fl">图片格式必须为png或jpg，文件大小不超过2M。</span>
                        <span id="uploadify300err" class="fl alert alert-small alert-warning" style="display:none;"><i class="icon error_icon"></i><span id="uploadify300msg"></span></span>
                        <br style="clear:left;">
                        <div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
                        <?php if($isempty && !empty($uploadify300))
                        {
                            ?>
                            <div class="300image_show_box">
                                <img style="width:300px;height:200px;" src="/public/upload<?php if($isempty && !empty($uploadify300)){echo $uploadify300;}?>">
                                <input type="hidden" value="<?php if($isempty && !empty($uploadify300)){echo $uploadify300;}?>" name="uploadify300" id="uploadify300">
                                <a target="_blank" href="/public/upload<?php if($isempty && !empty($uploadify300)){echo $uploadify300;}?>">查看实际效果</a>
                            </div>
                        <?php }?>
                    </div>
                </div>
                <!-- 个体工商户 -->
                <div class="accounttype3 accounttype_wrap" style="display: none;">
                    <div class="form-group">
                        <span class="form-label fl">个体工商户名称：</span>
                        <span class="fl">
                            <input type="text" size="50" id="individualname" name="individualname" class="form-control form-control-small" value="<?php if($isempty){echo $individualname;}?>"  placeholder="请如实填写" >
                        </span>
                        <span id="individualnameerr" class="fl alert alert-small alert-warning" style="display:none;"><i class="icon error_icon"></i><span id="individualnamemsg"></span></span>
                    </div>
                    <div class="form-group">
                        <span class="form-label fl"><span class="require_item">*</span> 营业执照号：</span>
                        <span class="fl">
                            <input type="text" size="50" id="individualid" name="individualid" class="form-control form-control-small" value="<?php if($isempty){echo $individualid;}?>"  placeholder="请填写营业执照号" >
                        </span>
                        <span id="individualiderr" class="fl alert alert-small alert-warning" style="display:none;"><i class="icon error_icon"></i><span id="individualidmsg"></span></span>
                    </div>
                    <div class="form-group">
                        <span class="form-label fl"><span class="require_item">*</span>营业执照：</span>
                        <span class="fl form-file">
                            <input type="file" name='Filedata'  id="uploadify40" class='uploadify'  onchange='ajaxUploder(this,400)'/>
                        </span>
                        <span class="form-file-label fl">图片格式必须为png或jpg，文件大小不超过2M。</span>
                        <span id="uploadify400err" class="fl alert alert-small alert-warning" style="display:none;"><i class="icon error_icon"></i><span id="uploadify400msg"></span></span>
                        <br style="clear:left;">
                        <div class="onload" style='text-align:left;display:none; margin-left: 220px;'><b class="icon"></b>正在上传，请稍侯…</div>
                        <?php if($isempty && !empty($uploadify400))
                        {
                            ?>
                            <div class="400image_show_box">
                                <img style="width:300px;height:200px;" src="/public/upload<?php if($isempty && !empty($uploadify400)){echo $uploadify400;}?>">
                                <input type="hidden" value="<?php if($isempty && !empty($uploadify400)){echo $uploadify400;}?>" name="uploadify400" id="uploadify400">
                                <a target="_blank" href="/public/upload<?php if($isempty && !empty($uploadify400)){echo $uploadify400;}?>">查看实际效果</a>
                            </div>
                        <?php }?>
                    </div>
                    <div class="form-group">
                        <span class="form-label fl"><span class="require_item">*</span>业主姓名：</span>
                        <span class="fl">
                            <input type="text" size="50" id="ownername" name="ownername" class="form-control form-control-small" value="<?php if($isempty){echo $ownername;}?>"  placeholder="请填写业主证件姓名">
                        </span>
                        <span id="ownernameerr" class="fl alert alert-small alert-warning" style="display:none;"><i class="icon error_icon"></i><span id="ownernamemsg"></span></span>
                    </div>
                    <div class="form-group">
                        <span class="form-label fl"><span class="require_item">*</span>证件类型：</span>
                        <span class="fl radio-group" id="ownercert">
                            <label class="radio_style"><i class="icon radio_icon "></i><input style="display: none;"  type="radio" <?php if($isempty && $ownercerttype==1){echo 'checked="checked"';}?>  value="1" name="ownercerttype">中国大陆身份证</label>
                            <label class="radio_style"><i class="icon radio_icon "></i><input style="display: none;" type="radio" <?php if($isempty && $ownercerttype==2){echo 'checked="checked"';}?>  value="2" name="ownercerttype">中国港澳台身份证</label>
                            <label class="radio_style"><i class="icon radio_icon "></i><input style="display: none;" type="radio" <?php if($isempty && $ownercerttype==3){echo 'checked="checked"';}?>  value="3" name="ownercerttype">海外证件</label>
                        </span>
                        <span id="ownercerttypeerr" class="fl alert alert-small alert-warning" style="display:none;"><i class="icon error_icon"></i><span id="ownercerttypemsg"></span></span>
                    </div>
                    <div class="form-group">
                        <span class="form-label fl"><span class="require_item">*</span>证件号码：</span>
                        <span class="fl">
                            <input type="text" size="50" id="ownerid" name="ownerid" class="form-control form-control-small" value="<?php if($isempty){echo $ownerid;}?>"  placeholder="保存后将不可修改" class="input_text">
                        </span>
                        <span id="owneriderr" class="fl alert alert-small alert-warning" style="display:none;"><i class="icon error_icon"></i><span id="owneridmsg"></span></span>
                    </div>
                    <div class="form-group">
                        <span class="form-label fl"><span class="require_item">*</span>证件正面：</span>
                        <span class="fl form-file">
                            <input type="file" name='Filedata'  id="uploadify50" class='uploadify'  onchange='ajaxUploder(this,500)'/>&nbsp;&nbsp; <br/>
                        </span>
                        <span class="form-file-label fl">图片格式必须为png或jpg，文件大小不超过2M。</span>
                        <span id="uploadify500err" class="fl alert alert-small alert-warning" style="display:none;"><i class="icon error_icon"></i><span id="uploadify500msg"></span></span>
                        <br style="clear:left;">
                        <div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
                        <?php if($isempty && !empty($uploadify500))
                        {
                            ?>
                            <div class="500image_show_box">
                                <img style="width:300px;height:200px;" src="/public/upload<?php if($isempty && !empty($uploadify500)){echo $uploadify500;}?>">
                                <input type="hidden" value="<?php if($isempty && !empty($uploadify500)){echo $uploadify500;}?>" name="uploadify500" id="uploadify500">
                                <a target="_blank" href="/public/upload<?php if($isempty && !empty($uploadify500)){echo $uploadify500;}?>">查看实际效果</a>
                            </div>
                        <?php }?>
                    </div>
                    <div class="form-group">
                        <span class="form-label fl"><span class="require_item">*</span>证件背面：</span>
                        <span class="fl form-file">
                            <input type="file" name='Filedata'  id="uploadify60" class='uploadify'  onchange='ajaxUploder(this,600)'/>&nbsp;&nbsp; <br/>
                        </span>
                        <span class="form-file-label fl">图片格式必须为png或jpg，文件大小不超过2M。</span>
                        <span id="uploadify600err" class="fl alert alert-small alert-warning" style="display:none;"><i class="icon error_icon"></i><span id="uploadify600msg"></span></span>
                        <br style="clear:left;">
                        <div class="onload" style='text-align:left;display:none; margin-left:-220px;'><b class="icon"></b>正在上传，请稍侯…</div>
                        <?php if($isempty && !empty($uploadify600))
                        {
                            ?>
                            <div class="600image_show_box">
                                <img style="width:300px;height:200px;" src="/public/upload<?php if($isempty && !empty($uploadify600)){echo $uploadify600;}?>">
                                <input type="hidden" value="<?php if($isempty && !empty($uploadify600)){echo $uploadify600;}?>" name="uploadify600" id="uploadify600">
                                <a target="_blank" href="/public/upload<?php if($isempty && !empty($uploadify600)){echo $uploadify600;}?>">查看实际效果</a>
                            </div>
                        <?php }?>
                    </div>
                </div>
            </div>
        </section>

        <section class="bank_info indexreg_form_box">
            <header class="title">
                <h3 class="title_normal">
                    <strong class="square_box"></strong>
                    <span>身份详细信息</span>
                </h3>
            </header>
            <div class="box">
            	<div class="form-group">
                    <span class="fl form-label"><i class="require_item">*</i>货币类型：</span>
                    <span class="fl radio-group" id="selectcurrency">
                        <label class="radio_style"><i class="icon radio_icon <?php if($isempty && $currency == 1) echo "active";?>"></i><input style="display:none;" name="currencytype" type="radio"  value="1" <?php if($isempty && $currency == 1) echo "checked='checked'";?>>人民币</label>
                        <label class="radio_style"><i class="icon radio_icon <?php if($isempty && $currency == 2) echo "active";?>"></i><input style="display:none;" name="currencytype" type="radio"  value="2" <?php if($isempty && $currency == 2) echo "checked='checked'";?>>美元</label>
                    </span>
                    <span id="currencytypeerr" class="fl alert alert-small alert-warning" style="display:none; "><i class="icon error_icon"></i><span id="currencytypemsg"></span></span>
                </div>
                
                <div class="form-group">
                    <span class="fl form-label"><i class="require_item">*</i>开户人名：</span>
                    <span class="fl">
                        <input type="text" size="20" id="bankusername" name="bankusername" class="form-control form-control-small"  value="<?php if($isempty){echo $bankusername;}?>" placeholder="请填写收款银行卡开户人姓名">
                    </span>
                    <span id="bankusernameerr" class="fl alert alert-small alert-warning" style="display:none;"><i class="icon error_icon"></i><span id="bankusernamemsg"></span></span>
                </div>

                <div class="form-group">
                    <span class="fl form-label"><i class="require_item">*</i>所在地区：</span>
                    <span class="fl">
                        <select class="form-select" name="province" id="province" onChange = "select()">
                            <option>省份/直辖市</option>
                        </select>
                        <select class="form-select" name="city" id="city" onChange = "select()">
                            <option>市/区</option>
                        </select>
                    </span>
                    <span id="provinceerr" class="fl alert alert-small alert-warning" style="display:none;"><i class="icon error_icon"></i><span id="provincemsg"></span></span>
                </div>

                <div class="form-group">
                    <span class="fl form-label"><i class="require_item">*</i>开户银行：</span>
                    <span class="fl">
                        <select id="bank" name="bank">
                            <option value='中国银行'  <?php if($isempty && $bank=='中国银行'){echo 'selected';}?> >中国银行</option>
                            <option value='中国工商银行' <?php if($isempty && $bank=='中国工商银行'){echo 'selected';}?> >中国工商银行</option>
                            <option value='中国建设银行' <?php if($isempty && $bank=='中国建设银行'){echo 'selected';}?> >中国建设银行</option>
                            <option value='中国农业银行' <?php if($isempty && $bank=='中国农业银行'){echo 'selected';}?> >中国农业银行</option>
                            <option value='交通银行' <?php if($isempty && $bank=='交通银行'){echo 'selected';}?> >交通银行</option>
                            <option value='中国邮政储蓄银行' <?php if($isempty && $bank=='中国邮政储蓄银行'){echo 'selected';}?> >中国邮政储蓄银行</option>
                            <option value='中国民生银行' <?php if($isempty && $bank=='中国民生银行'){echo 'selected';}?> >中国民生银行</option>
                            <option value='中国光大银行' <?php if($isempty && $bank=='中国光大银行'){echo 'selected';}?> >中国光大银行</option>
                            <option value='招商银行' <?php if($isempty && $bank=='招商银行'){echo 'selected';}?> >招商银行</option>
                            <option value='中信银行' <?php if($isempty && $bank=='中信银行'){echo 'selected';}?> >中信银行</option>
                            <option value='华夏银行' <?php if($isempty && $bank=='华夏银行'){echo 'selected';}?> >华夏银行</option>
                            <option value='恒丰银行' <?php if($isempty && $bank=='恒丰银行'){echo 'selected';}?> >恒丰银行</option>
                            <option value='渤海银行' <?php if($isempty && $bank=='渤海银行'){echo 'selected';}?> >渤海银行</option>
                            <option value='浙商银行' <?php if($isempty && $bank=='浙商银行'){echo 'selected';}?> >浙商银行</option>
                            <option value='平安银行' <?php if($isempty && $bank=='平安银行'){echo 'selected';}?> >平安银行</option>
                            <option value='兴业银行' <?php if($isempty && $bank=='兴业银行'){echo 'selected';}?> >兴业银行</option>
                            <option value='上海浦东发展银行' <?php if($isempty && $bank=='上海浦东发展银行'){echo 'selected';}?> >上海浦东发展银行</option>
                            <option value='广东发展银行' <?php if($isempty && $bank=='广东发展银行'){echo 'selected';}?> >广东发展银行</option>
                            <option value='深圳发展银行' <?php if($isempty && $bank=='深圳发展银行'){echo 'selected';}?> >深圳发展银行</option>
                            <option value='农村信用合作社' <?php if($isempty && $bank=='农村信用合作社'){echo 'selected';}?> >农村信用合作社</option>
                            <option value='中国邮政储蓄银行' <?php if($isempty && $bank=='中国邮政储蓄银行'){echo 'selected';}?> >中国邮政储蓄银行</option>
                            <option value='上海银行' <?php if($isempty && $bank=='上海银行'){echo 'selected';}?> >上海银行</option>
                            <option value='北京银行' <?php if($isempty && $bank=='北京银行'){echo 'selected';}?> >北京银行</option>
                            <option value='北京农村商业银行' <?php if($isempty && $bank=='北京农村商业银行'){echo 'selected';}?> >北京农村商业银行</option>
                            <option value='城市信用合作社' <?php if($isempty && $bank=='城市信用合作社'){echo 'selected';}?> >城市信用合作社</option>
                            <option value='福建兴业银行' <?php if($isempty && $bank=='福建兴业银行'){echo 'selected';}?> >福建兴业银行</option>
                            <option value='广东南粤银行' <?php if($isempty && $bank=='广东南粤银行'){echo 'selected';}?> >广东南粤银行</option>
                            <option value='广州农村商业银行' <?php if($isempty && $bank=='广州农村商业银行'){echo 'selected';}?> >广州农村商业银行</option>
                            <option value='广州银行' <?php if($isempty && $bank=='广州银行'){echo 'selected';}?> >广州银行</option>
                            <option value='桂林银行' <?php if($isempty && $bank=='桂林银行'){echo 'selected';}?> >桂林银行</option>
                            <option value='杭州联合银行' <?php if($isempty && $bank=='杭州联合银行'){echo 'selected';}?> >杭州联合银行</option>
                            <option value='杭州银行' <?php if($isempty && $bank=='杭州银行'){echo 'selected';}?> >杭州银行</option>
                            <option value='江苏银行' <?php if($isempty && $bank=='江苏银行'){echo 'selected';}?> >江苏银行</option>
                            <option value='汉口银行' <?php if($isempty && $bank=='汉口银行'){echo 'selected';}?> >汉口银行</option>
                            <option value='徽商银行' <?php if($isempty && $bank=='徽商银行'){echo 'selected';}?> >徽商银行</option>
                            <option value='宁波银行' <?php if($isempty && $bank=='宁波银行'){echo 'selected';}?> >宁波银行</option>
                            <option value='上海农村商业银行' <?php if($isempty && $bank=='上海农村商业银行'){echo 'selected';}?> >上海农村商业银行</option>
                            <option value='中国进出口银行' <?php if($isempty && $bank=='中国进出口银行'){echo 'selected';}?> >中国进出口银行</option>
                            <option value='国家开发银行' <?php if($isempty && $bank=='国家开发银行'){echo 'selected';}?> >国家开发银行</option>
                            <option value='中国农业发展银行' <?php if($isempty && $bank=='中国农业发展银行'){echo 'selected';}?> >中国农业发展银行</option>
                            <option value='重庆三峡银行' <?php if($isempty && $bank=='重庆三峡银行'){echo 'selected';}?> >重庆三峡银行</option>
                            <option value='城市商业银行' <?php if($isempty && $bank=='城市商业银行'){echo 'selected';}?> >城市商业银行</option>
                            <option value='富滇银行' <?php if($isempty && $bank=='富滇银行'){echo 'selected';}?> >富滇银行</option>
                            <option value='新韩银行' <?php if($isempty && $bank=='新韩银行'){echo 'selected';}?> >新韩银行</option>
                            <option value='渣打银行' <?php if($isempty && $bank=='渣打银行'){echo 'selected';}?> >渣打银行</option>
                            <option value='花旗银行' <?php if($isempty && $bank=='花旗银行'){echo 'selected';}?> >花旗银行</option>
                            <option value='汇丰银行' <?php if($isempty && $bank=='汇丰银行'){echo 'selected';}?> >汇丰银行</option>
                            <option value='东亚银行' <?php if($isempty && $bank=='东亚银行'){echo 'selected';}?> >东亚银行</option>
                            <option value='三井住友银行' <?php if($isempty && $bank=='三井住友银行'){echo 'selected';}?> >三井住友银行</option>
                            <option value='其他' <?php if($isempty && $bank=='其他'){echo 'selected';}?> >其他</option>
                        </select>
                    </span>
                    <div class="alone">
                        <input type="text" size="40" name="bankaddress" id="bankaddress" class="form-control form-control-small" value="<?php if($isempty){echo $bankaddress;}?>"  placeholder="请填写具体分行/支行">
                    </div>
                    <span id="bankaddresserr" class="fl alert alert-small alert-warning" style="display:none; margin-left:220px; margin-top:15px;"><i class="icon error_icon"></i><span id="bankaddressmsg"></span></span>
                </div>

                <div class="form-group">
                    <span class="fl form-label"><i class="require_item">*</i>银行账号：</span>
                    <span class="fl">
                        <input type="text" size="40" id="bankaccount" name="bankaccount" class="form-control form-control-small" value="<?php if($isempty){echo $bankaccount;}?>" placeholder="请填写收款银行卡号">
                    </span>
                    <span id="bankaccounterr" class="fl alert alert-small alert-warning" style="display:none;"><i class="icon error_icon"></i><span id="bankaccountmsg"></span></span>
                </div>

                <div class="form-group">
                    <span class="fl form-label"><i class="require_item">*</i>确认账号：</span>
                    <span class="fl">
                        <input type="text" id="rebankaccount" name="rebankaccount" size="40" class="form-control form-control-small" placeholder="请填写收款银行卡号">
                    </span>
                    <span id="rebankaccounterr" class="fl alert alert-small alert-warning" style="display:none;"><i class="icon error_icon"></i><span id="rebankaccountmsg"></span></span>
                </div>
            </div>
        </section>

        <section class="contact_info indexreg_form_box">
            <header class="title">
                <h3 class="title_normal">
                    <strong class="square_box"></strong>
                    <span>联系方式</span>
                </h3>
            </header>
            <div class="box">
                <div class="form-group">
                    <span class="fl form-label"><i class="require_item">*</i>联系人：</span>
                    <span class="fl">
                        <input type="text" size="40" id="contact" name="contact" value="<?php if($isempty){echo $contact;}?>" class="form-control form-control-small" placeholder="平台联系开发者，签订协议等，请填写负责人姓名">
                    </span>
                    <span id="contacterr" class="fl alert alert-small alert-warning" style="display:none;"><i class="icon error_icon"></i><span id="contactmsg"></span></span>
                </div>

                <div class="form-group">
                    <span class="fl form-label"><i class="require_item">*</i>手机号码：</span>
                    <span class="fl">
                        <input type="text" size="40" id="mobile" name="mobile"  value="<?php if($isempty){echo $mobile;}?>" class="form-control form-control-small" placeholder="将用于提现手机验证">
                    </span>
                    <span id="mobileerr" class="fl alert alert-small alert-warning" style="display:none;"><i class="icon error_icon"></i><span id="mobilemsg"></span></span>
                </div>

                <div class="form-group">
                    <span class="fl form-label"><i class="require_item">*</i>联系地址：</span>
                    <span class="fl">
                        <input type="text" size="40" id="address" name="address" value="<?php if($isempty){echo $address;}?>" class="form-control form-control-small" placeholder="将用于邮寄合同或发票">
                    </span>
                    <span id="addresserr" class="fl alert alert-small alert-warning" style="display:none;"><i class="icon error_icon"></i><span id="addressmsg"></span></span>
                </div>

                <div class="form-group">
                    <span class="fl form-label">固定电话：</span>
                    <span class="fl">
                        <input type="text" id="telephone" name="telephone" value="<?php if($isempty){echo $telephone;}?>" size="40" class="form-control form-control-small" placeholder="">
                    </span>
                    <span id="telephoneerr" class="fl alert alert-small alert-warning" style="display:none;"><i class="icon error_icon"></i><span id="telephonemsg"></span></span>
                </div>

                <div class="form-group">
                    <span class="fl form-label"><i class="require_item">*</i>QQ：</span>
                    <span class="fl">
                        <input type="text" id="qqnumber" name="qqnumber" size="40" value="<?php if($isempty){echo $qqnumber;}?>" class="form-control form-control-small" placeholder="">
                    </span>
                    <span id="qqnumbererr" class="fl alert alert-small alert-warning" style="display:none;"><i class="icon error_icon"></i><span id="qqnumbermsg"></span></span>
                </div>

            </div>
        </section>
        
        <section class="contact_info indexreg_form_box">
            <header class="title">
                <h3 class="title_normal">
                    <strong class="square_box"></strong>
                    <span>其他</span>
                </h3>
            </header>
            <div class="box">
            	<div class="form-group" style="margin-left:8px;">
            		<span class="fl form-label" style="width:350px;"><i class="require_item">*</i>您通过何种渠道了解到CocosAds：</span><br/><br/><br/>
                    <div class="radio-group" id="infosourcediv" style="padding-left: 125px;">
                    	<label class="radio_style"><i class="icon radio_icon <?php if($isempty && $infosource==1){echo 'active'; }?> "></i><input style="display: none;" name="infosource" value="1" type="radio" <?php if($isempty && $accounttype==1){echo 'checked="checked"';}?> /> 新闻</label>&nbsp;&nbsp;
    					<label class="radio_style"><i class="icon radio_icon <?php if($isempty && $infosource==2){echo 'active'; }?> "></i><input style="display: none;" name="infosource" value="2" type="radio" <?php if($isempty && $accounttype==2){echo 'checked="checked"';}?> /> 沙龙</label>&nbsp;&nbsp;
        				<label class="radio_style"><i class="icon radio_icon <?php if($isempty && $infosource==3){echo 'active'; }?> "></i><input style="display: none;" name="infosource" value="3" type="radio" <?php if($isempty && $accounttype==3){echo 'checked="checked"';}?> /> 行业大会</label>&nbsp;&nbsp;
        				<label class="radio_style"><i class="icon radio_icon <?php if($isempty && $infosource==4){echo 'active'; }?> "></i><input style="display: none;" name="infosource" value="4" type="radio" <?php if($isempty && $accounttype==4){echo 'checked="checked"';}?> /> 朋友介绍</label>&nbsp;&nbsp;
        				<label class="radio_style"><i class="icon radio_icon <?php if($isempty == false || ($isempty && $infosource==5)){echo 'active'; }?>"></i><input style="display: none;" name="infosource" value="5" type="radio" <?php if($isempty == false || ($isempty && $accounttype==5)){echo 'checked="checked"';}?>/> 其他</label>&nbsp;&nbsp;
						<input name="sourcename" id="sourcename" placeholder="请输入2-20字符" value="<?php if($isempty && $infosource==5){echo $sourcename; }?>"/><br/>
						<span  id="infosourceerr" class="fl alert alert-small alert-warning" style="display:none;"><i class="icon error_icon"></i><span id="infosourcemsg"></span></span>
					</div>
            	</div>
            </div>
       </section>
        <hr>
        <div class="btn-group">
            <button class="btn btn-primary btn-primary-noboder" id="formsubmit" name="formsubmit" type="button">提交审核</button>
        </div>

    </form>
</div>

<!-- ] 主体部分 -->
<?php $this->load->view("manage/inc/footer");?>
<script type="text/javascript">

$(document).ready(function() {
    $('body').removeClass('bluegray');


    //单选按钮
    $('#certtype_wrap .radio_style').bind('click',function(e){
        $('#certtype_wrap .radio_style').find('.icon').removeClass('active');
        $(this).find('.icon').addClass('active');
        $(this).find('input').attr('checked',true);
    });

    $('#ownercert .radio_style').bind('click',function(e){
        $('#ownercert .radio_style').find('.icon').removeClass('active');
        $(this).find('.icon').addClass('active');
        $(this).find('input').attr('checked',true);
    });

    $('#selectcurrency .radio_style').bind('click',function(e){
        $('#selectcurrency .radio_style').find('.icon').removeClass('active');
        $(this).find('.icon').addClass('active');
        $(this).find('input').attr('checked',true);
    });

	//初始化区域信息
	init();
	$("#mobile").blur(function (){
		mobile = $(this).val();
		$("#mobileerr").hide();
	    if(mobile.length==0)
	    {
	        $("#mobileerr").show();
	        $("#mobilemsg").html('请输入手机号码！');
	        return ;
	    }    
	    if(mobile.length!=11)
	    {
		    $("#mobileerr").show();
		    $("#mobilemsg").html('请输入有效的手机号码！');
	        return ;
	    }
	    
	    var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
	    if(!myreg.test(mobile))
	    {
		    $("#mobileerr").show();
		    $("#mobilemsg").html('请输入有效的手机号码！');
	        return ;
	    }
	});

	$("#formsubmit").click(function (){
		var accounttype = $('input:radio[name="accounttype"]:checked').val();
		var certtype = $('input:radio[name="certtype"]:checked').val();
		var realname = $("#realname").val()
		var id = $("#id").val()
		var idnumber = $("#idnumber").val()
		var companyname = $("#companyname").val()
		var companylicense = $("#companylicense").val()
		var companyid = $("#companyid").val()
		var individualname = $("#individualname").val()
		var individualid = $("#individualid").val()
		var ownername = $("#ownername").val()
		var ownercerttype = $('input:radio[name="ownercerttype"]:checked').val();
		var ownerid = $("#ownerid").val()
		var bankusername = $("#bankusername").val()
		var province = $("#province").val()
		var city = $("#city").val()
		var currency = $('input:radio[name="currencytype"]:checked').val();
		var bank = $("#bank").val()
		var bankaddress = $("#bankaddress").val()
		var bankaccount = $("#bankaccount").val()
		var rebankaccount = $("#rebankaccount").val()
		var contact = $("#contact").val()
		var mobile = $("#mobile").val()
		var address = $("#address").val()
		var telephone = $("#telephone").val()
		var qqnumber = $("#qqnumber").val()
		var uploadify100 = $("#uploadify100").val()
		var uploadify200 = $("#uploadify200").val()
		var uploadify300 = $("#uploadify300").val()
		var uploadify400 = $("#uploadify400").val()
		var uploadify500 = $("#uploadify500").val()
		var uploadify600 = $("#uploadify600").val()
		var sourcename = $("#sourcename").val();
		var infosource = $('input:radio[name="infosource"]:checked').val()
		msgInit();
		$("#formsubmit").attr("disabled", "disabled");
		$.post("/manage/indexreg/dosave", 
				{ 
    			accounttype : accounttype,
    			certtype : certtype,
    			id : id,
    			realname : realname,
    			idnumber : idnumber,
    			companyname : companyname,
    			companylicense : companylicense,
    			companyid : companyid,
    			individualname : individualname,
    			individualid : individualid,
    			ownername : ownername,
    			ownercerttype : ownercerttype,
    			ownerid : ownerid,
    			bankusername : bankusername,
    			province : province,
    			city : city,
    			currency : currency,
    			bank : bank,
    			bankaddress : bankaddress,
    			bankaccount : bankaccount,
    			rebankaccount : rebankaccount,
    			contact : contact,
    			mobile : mobile,
    			address : address,
    			telephone : telephone,
    			qqnumber : qqnumber,
    			uploadify100 : uploadify100,
    			uploadify200 : uploadify200,
    			uploadify300 : uploadify300,
    			uploadify400 : uploadify400,
    			uploadify500 : uploadify500,
    			uploadify600 : uploadify600,
    			sourcename : sourcename,
    			infosource : infosource,
	   			r:Math.random()
				}, 
				function(data)
				{
	    			if(data && data['status'] == 0){
	        			if(data['data'] != '')
	        			{
	        				$("#"+data['data'][0]).html(data['data'][1]);
	            		}
	    				$("#"+data['info']).show();
                        $("html,body").animate({scrollTop:$('#'+data['info']).offset().top-87},500);
	    		        $("#formsubmit").removeAttr("disabled");
	    				return false;
	    			}else if(data && data['status'] == 1){
		    			alert('提交成功，请等待审核！');
	    				location.href = '/manage/appinfo/index';
	    			}
	    			else
	    			{
		    			alert('未知错误，请联系管理员！');
		    			}
				},
				'json');
			return false;
			return;		
		})
		
});

function msgInit()
{
	$("#accounttypeerr").hide()
	$("#certtypeerr").hide()
	$("#realnameerr").hide()
	$("#idnumbererr").hide()
	$("#companynameerr").hide()
	$("#companylicenseerr").hide()
	$("#companyiderr").hide()
	$("#individualnameerr").hide()
	$("#individualiderr").hide()
	$("#ownernameerr").hide()
	$("#ownercerttypeerr").hide()
	$("#owneriderr").hide()
	$("#bankusernameerr").hide()
	$("#provinceerr").hide()
	$("#bankerr").hide()
	$("#bankaddresserr").hide()
	$("#bankaccounterr").hide()
	$("#rebankaccounterr").hide()
	$("#contacterr").hide()
	$("#mobileerr").hide()
	$("#addresserr").hide()
	$("#telephoneerr").hide()
	$("#qqnumbererr").hide()
	$("#uploadify100err").hide()
	$("#uploadify200err").hide()
	$("#uploadify300err").hide()
	$("#uploadify400err").hide()
	$("#uploadify500err").hide()
	$("#uploadify600err").hide()
	$("#infosourceerr").hide()
	$("#currencytypeerr").hide()
}

function ajaxUploder(fileObj, tpl) {
	var obj 			= $(fileObj);
	var fileId 			= obj.attr('id');
	var image_show_box 	= obj.parents('.form-group').find('.image_show_box');
	var maindiv 		= obj.parents('.form-group');
	var loading = obj.parent().siblings('.onload').show();
	$.ajaxFileUpload( {
				url : '/manage/indexreg/ajaxDoUpload/',
				secureuri : true,
				fileElementId : fileId,
				dataType : 'json',// 服务器返回的格式，可以是json
				success : function(data, status) {
        			$("#iconimgerr").hide();
					if(data['status'])
					{
						var str = '<div class="'+tpl+'image_show_box" style="padding-left:220px;">';
						str+='<img src="/public/upload'+data['data']+'" style="width:300px;height:200px; margin-right:10px; vertical-align:bottom;" /><a href="/public/upload'+data['data']+'" target="_blank">查看实际效果</a>';
						str+='<input type="hidden" id="uploadify'+tpl+'" name="uploadify'+tpl+'" value="'+data['data']+'"/>';
						str += '&nbsp;&nbsp;</div>';
						maindiv.find('.'+tpl+'image_show_box').remove();// 将刚才上传的干掉
						maindiv.append(str);
						maindiv.find('.'+tpl+'_image_show_box').fadeIn("slow");
						}
					else
					{
						alert(data['data']);
						}
					loading.hide();
					return true;
				},
				error : function(data, status, e) {
					loading.hide();
				}
			})
}

function accounttyepTab(accounttype){
	if(accounttype == 1)
	{
		$(".accounttype1").show();
		$(".accounttype2").hide();
		$(".accounttype3").hide();
		}
	else if(accounttype == 2)
	{
		$(".accounttype2").show();
		$(".accounttype1").hide();
		$(".accounttype3").hide();
		}
	else if(accounttype == 3)
	{
		$(".accounttype3").show();
		$(".accounttype1").hide();
		$(".accounttype2").hide();
    }
}


$('#selectaccounttype .radio_style').bind('click',function(e){
    $('#selectaccounttype .radio_style').find('.icon').removeClass('active');
    $(this).find('.icon').addClass('active');
    $(this).find('input').attr('checked',true);
    var accounttype = $('input:radio[name="accounttype"]:checked').val();
    accounttyepTab(accounttype);
});

$('#infosourcediv .radio_style').bind('click',function(e){
    $('#infosourcediv .radio_style').find('.icon').removeClass('active');
    $(this).find('.icon').addClass('active');
    $(this).find('input').attr('checked',true);
    var infosource = $('input:radio[name="infosource"]:checked').val();
    $("#sourcename").hide();
    $("#sourcename").val("");
	if(infosource == 5)
	{
		$("#sourcename").show();
	}
});

$("#realname").blur(function(){
	$("#bankusername").val($("#realname").val());
})
$("#companyname").blur(function(){
	$("#bankusername").val($("#companyname").val());
})
$("#ownername").blur(function(){
	$("#bankusername").val($("#ownername").val());
})
</script>

<script language="javascript">
<!--
var where = new Array(34);
function comefrom(loca,locacity) { this.loca = loca; this.locacity = locacity; }
where[0]= new comefrom("请选择省份名","请选择城市名");
where[1] = new comefrom("北京","请选择|东城|西城|崇文|宣武|朝阳|丰台|石景山|海淀|门头沟|房山|通州|顺义|昌平|大兴|平谷|怀柔|密云|延庆");  //欢迎来到站长特效网，我们的网址是www.zzjs.net，很好记，zz站长，js就是js特效，本站收集大量高质量js代码，还有许多广告代码下载。
where[2] = new comefrom("上海","请选择|黄浦|卢湾|徐汇|长宁|静安|普陀|闸北|虹口|杨浦|闵行|宝山|嘉定|浦东|金山|松江|青浦|南汇|奉贤|崇明");//欢迎来到站长特效网，我们的网址是www.zzjs.net，很好记，zz站长，js就是js特效，本站收集大量高质量js代码，还有许多广告代码下载。
where[3] = new comefrom("天津","请选择|和平|东丽|河东|西青|河西|津南|南开|北辰|河北|武清|红挢|塘沽|汉沽|大港|宁河|静海|宝坻|蓟县");
where[4] = new comefrom("重庆","请选择|万州|涪陵|渝中|大渡口|江北|沙坪坝|九龙坡|南岸|北碚|万盛|双挢|渝北|巴南|黔江|长寿|綦江|潼南|铜梁|大足|荣昌|壁山|梁平|城口|丰都|垫江|武隆|忠县|开县|云阳|奉节|巫山|巫溪|石柱|秀山|酉阳|彭水|江津|合川|永川|南川");
where[5] = new comefrom("河北","请选择|石家庄|邯郸|邢台|保定|张家口|承德|廊坊|唐山|秦皇岛|沧州|衡水");
where[6] = new comefrom("山西","请选择|太原|大同|阳泉|长治|晋城|朔州|吕梁|忻州|晋中|临汾|运城");
where[7] = new comefrom("内蒙古","请选择|呼和浩特|包头|乌海|赤峰|呼伦贝尔盟|阿拉善盟|哲里木盟|兴安盟|乌兰察布盟|锡林郭勒盟|巴彦淖尔盟|伊克昭盟");
where[8] = new comefrom("辽宁","请选择|沈阳|大连|鞍山|抚顺|本溪|丹东|锦州|营口|阜新|辽阳|盘锦|铁岭|朝阳|葫芦岛");
where[9] = new comefrom("吉林","请选择|长春|吉林|四平|辽源|通化|白山|松原|白城|延边");
where[10] = new comefrom("黑龙江","请选择|哈尔滨|齐齐哈尔|牡丹江|佳木斯|大庆|绥化|鹤岗|鸡西|黑河|双鸭山|伊春|七台河|大兴安岭");
where[11] = new comefrom("江苏","请选择|南京|镇江|苏州|南通|扬州|盐城|徐州|连云港|常州|无锡|宿迁|泰州|淮安");
where[12] = new comefrom("浙江","请选择|杭州|宁波|温州|嘉兴|湖州|绍兴|金华|衢州|舟山|台州|丽水");
where[13] = new comefrom("安徽","请选择|合肥|芜湖|蚌埠|马鞍山|淮北|铜陵|安庆|黄山|滁州|宿州|池州|淮南|巢湖|阜阳|六安|宣城|亳州");
where[14] = new comefrom("福建","请选择|福州|厦门|莆田|三明|泉州|漳州|南平|龙岩|宁德");
where[15] = new comefrom("江西","请选择|南昌市|景德镇|九江|鹰潭|萍乡|新馀|赣州|吉安|宜春|抚州|上饶");
where[16] = new comefrom("山东","请选择|济南|青岛|淄博|枣庄|东营|烟台|潍坊|济宁|泰安|威海|日照|莱芜|临沂|德州|聊城|滨州|菏泽");
where[17] = new comefrom("河南","请选择|郑州|开封|洛阳|平顶山|安阳|鹤壁|新乡|焦作|濮阳|许昌|漯河|三门峡|南阳|商丘|信阳|周口|驻马店|济源");
where[18] = new comefrom("湖北","请选择|武汉|宜昌|荆州|襄樊|黄石|荆门|黄冈|十堰|恩施|潜江|天门|仙桃|随州|咸宁|孝感|鄂州");
where[19] = new comefrom("湖南","请选择|长沙|常德|株洲|湘潭|衡阳|岳阳|邵阳|益阳|娄底|怀化|郴州|永州|湘西|张家界");
where[20] = new comefrom("广东","请选择|广州|深圳|珠海|汕头|东莞|中山|佛山|韶关|江门|湛江|茂名|肇庆|惠州|梅州|汕尾|河源|阳江|清远|潮州|揭阳|云浮");
where[21] = new comefrom("广西","请选择|南宁|柳州|桂林|梧州|北海|防城港|钦州|贵港|玉林|南宁地区|柳州地区|贺州|百色|河池");
where[22] = new comefrom("海南","请选择|海口|三亚");
where[23] = new comefrom("四川","请选择|成都|绵阳|德阳|自贡|攀枝花|广元|内江|乐山|南充|宜宾|广安|达川|雅安|眉山|甘孜|凉山|泸州");
where[24] = new comefrom("贵州","请选择|贵阳|六盘水|遵义|安顺|铜仁|黔西南|毕节|黔东南|黔南");
where[25] = new comefrom("云南","请选择|昆明|大理|曲靖|玉溪|昭通|楚雄|红河|文山|思茅|西双版纳|保山|德宏|丽江|怒江|迪庆|临沧");
where[26] = new comefrom("西藏","请选择|拉萨|日喀则|山南|林芝|昌都|阿里|那曲");
where[27] = new comefrom("陕西","请选择|西安|宝鸡|咸阳|铜川|渭南|延安|榆林|汉中|安康|商洛");
where[28] = new comefrom("甘肃","请选择|兰州|嘉峪关|金昌|白银|天水|酒泉|张掖|武威|定西|陇南|平凉|庆阳|临夏|甘南");
where[29] = new comefrom("宁夏","请选择|银川|石嘴山|吴忠|固原");
where[30] = new comefrom("青海","请选择|西宁|海东|海南|海北|黄南|玉树|果洛|海西");
where[31] = new comefrom("新疆","请选择|乌鲁木齐|石河子|克拉玛依|伊犁|巴音郭勒|昌吉|克孜勒苏柯尔克孜|博尔塔拉|吐鲁番|哈密|喀什|和田|阿克苏");
where[32] = new comefrom("香港","香港");
where[33] = new comefrom("澳门","澳门");
where[34] = new comefrom("台湾","请选择|台北|高雄|台中|台南|屏东|南投|云林|新竹|彰化|苗栗|嘉义|花莲|桃园|宜兰|基隆|台东|金门|马祖|澎湖");

function select() {
    with(document.creator.province) { var loca2 = options[selectedIndex].value; }
    for(i = 0;i < where.length;i ++) {
        if (where[i].loca == loca2) {
            loca3 = (where[i].locacity).split("|");
            for(j = 0;j < loca3.length;j++) { with(document.creator.city) { length = loca3.length; options[j].text = loca3[j]; options[j].value = loca3[j]; var loca4=options[selectedIndex].value;}}
            break;
        }
    }
}


function init() {
    with(document.creator.province) {
        length = where.length;
        for(k=0;k<where.length;k++) { options[k].text = where[k].loca; options[k].value = where[k].loca; }
        options[selectedIndex].text = where[0].loca; options[selectedIndex].value = where[0].loca;
    }
    with(document.creator.city) {
        loca3 = (where[0].locacity).split("|");
        length = loca3.length;
        for(l=0;l<length;l++) { options[l].text = loca3[l]; options[l].value = loca3[l]; }
        options[selectedIndex].text = loca3[0]; options[selectedIndex].value = loca3[0];
    }
    <?php
            if($isempty){
        ?>
    var province = "<?php echo $province;?>",
        city = "<?php echo $city;?>";
    if(province != "" && city != ""){
        $("#province").val(province);
        select()
        $("#city").val(city);
    };
    <?php }?>
}



accounttyepTab($('input:radio[name="accounttype"]:checked').val());

$('#newAppCue .close').click(function(){
	$("#newAppCue").fadeOut();
});
-->
</script>
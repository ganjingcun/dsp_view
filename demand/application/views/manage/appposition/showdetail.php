<style>

.form_input label {display: inline-block;}

</style>
        <div class="bread_crumbs">
        	<span>您目前所在位置：</span>&nbsp;<a href="/"><b class="icon_home"></b></a> > 应用管理 > 添加应用
        </div>
</div>
	<!-- 主体内容 -->
	<div class="warp">
		<div class="main_content main_content_leftside"><!-- 左侧内容 -->	
			
			<div class="inner_con">
			<div class="content content_leftside">		
			<?php $this->load->view('manage/inc/left')?>
					<div>
						<h1 class="main_title">添加应用</h1>
						<hr class="dline" />
							<div class="form_item">
								<div class="form_label"><b style="font-size:18px;">Publisher ID : </b></div>
								<div class="form_input" id="sys_tab">
									<span id="appid"><?php echo $publisherID?></span>
								</div>
								<div class="form_label"></div>
								<div class="form_input">
									<a class="button_m" href="/manage/appinfo/showsdkinfo">下载SDK</a>
								</div>
								<div class="db10"></div>
							</div>
							
							<div class="form_item" id="add_ios">
								<div class="form_label"><span class="red"></span>应用类型：</div>
								<div class="form_input">
									<?php
									if($ostypeid == 1)
									{
										echo 'Android';
									}
									elseif($ostypeid == 2)
									{
										echo 'iOS';
									}?>	
								</div>
							</div>
							<div class="form_item <?php if($ostypeid == 1) echo 'hidden'?>" id="add_ios">
								<div class="form_label">平台类型：</div>
								<div class="form_input">
								<?php
								if($ostypeid == 2)
								{
									if($devicetypeid == 2) echo 'iPhone';
									if($devicetypeid == 3) echo 'iPad';
									if($devicetypeid == 4) echo 'Universal';
								}
								?>
								</div>
							</div>
							<div class="form_item">								
								<div class="form_label"><span class="red"></span>应用名称：</div>
								<div class="form_input"><?php echo $appname?></div>
							</div>
							<div class="form_item">
								<div class="form_label">下载地址：</div>
								<div class="form_input"><?php echo $appurl?></div>
							</div>
							<div class="form_item">
								<div class="form_label">应用分类：</div>
								<div class="form_input">
								<?php echo $typename?>
								</div>
								<?php 
								if(isset($childtypename) && !empty($childtypename))
								{?><br>
								<div style="padding:2px;" class="form_input subform ">
								<?php
								foreach ($childtypename as $value) {
									?>
									<label data-v="" class="selected"><?php echo $value?></label>
									<?php 
								}
								?>
								</div>
								<?php }?>
							</div>
							<div class="form_item">
								<div class="form_label">应用介绍：</div>
								<div class="form_input"><?php echo $appdescription?></div>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php $this->load->view("manage/inc/footer");?>

<!--  主体部分 [ -->
<div class="inner_container" id="innerContainer">
    <?php $this->load->view('manage/inc/left')?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <div class="jumbotron jumbotron_no_shadow add_app_box">
                    <header class="title">
                        <h3><strong  class="square_box"></strong><span>添加应用</span></h3>
                    </header>
                    <div class="add_app_step add_app_step_02"></div>
                    <div class="wrap">
                        <div class="ad_manager">
                            <div class="note">
                                <p>* 为方便您的产品接入SDK，允许广告位ID为空，并定义默认广告位</p>
                                <p>* 建议您创建多个广告位，分别管理不同的广告触发时机</p>
                            </div>
                            <table class="table table-bordered no-margin-bottom">
                                <thead>
                                    <tr>
                                        <th>广告位ID</th>
                                        <th>广告位名称</th>
                                        <th>
                                            <select>
                                                <option>全部广告形式</option>
                                                <option>Banner广告</option>
                                                <option>插屏广告</option>
                                                <option>推荐墙广告</option>
                                                <option>积分墙广告</option>
                                            </select>
                                        </th>
                                        <th>广告位描述</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                if(!empty($positionList))
                                {
                                    $i = 0;
                                    foreach ($positionList as $v)
                                    {
                                        if(empty($v['positionid']))
                                        {
                                            if($i == 0)
                                            {
                                                ?>
                                                <tr id="emptyPosition" class="empty_container"><td colspan="5">默认广告位不展示在广告位栏，如需灵活控制广告触发时机，建议您创建新广告位!</td></tr>
                                            <?php
                                            }
                                            $i++;
                                            continue;
                                        }
                                        ?>
                                        <tr class="ar" style="word-wrap: break-word;word-break: normal;">
                                            <td><?php echo $v['positionid']?></td>
                                            <td width="20%"><?php echo $v['positionname']?></td>
                                            <td><?php
                                                $str = 'Banner广告';
                                                switch ($v['adform'])
                                                {
                                                    case 1:
                                                        $str = 'Banner广告';
                                                        break;
                                                    case 2:
                                                        $str = '插屏广告';
                                                        break;
                                                    case 3:
                                                        $str = '推荐墙广告';
                                                        break;
                                                    case 4:
                                                        $str = '积分墙广告';
                                                        break;
                                                }
                                                echo $str;
                                                ?></td>
                                            <td width="30%"><?php echo $v['positiondesc']?></td>
                                        </tr>
                                    <?php
                                    }
                                }
                                ?>
                                </tbody>
                            </table>

                            <?php if (empty($positionInfo)){?>
                                <div class="function_btn">
                                    <a id="newadd" class="btn btn-primary btn-primary-noboder" onclick="return false;"><i class="icon add_icon"></i>添加广告位</a>
                                </div>
                            <?php }?>

                            <br>
                            <div id="newPosition" style="display: none;">
                                <form id="frm" name="frm" action="" method="post">
                                    <div class="new_position jumbotron_no_shadow">
                                        <h3 class="fixed_title">新增广告位</h3>
                                        <div style="padding-left: 100px;">
                                            <div class="form-group">
                                                <span class="fl form-label">广告位ID</span>
                                                <span class="fl values"><?php if(!empty($positionInfo["positionid"])) {echo $positionInfo["positionid"];} else {echo $positionid;}?></span>
                                                <input type="hidden" value="<?php if(!empty($positionInfo["positionid"])) {echo $positionInfo["positionid"];} else {echo $positionid;}?>" id="positionid" name="positionid">
                                                <input type="hidden" value="<?PHP echo $appid?>" id="appid" name="appid">
                                                <input type="hidden" value="<?php if(!empty($positionInfo["positionid"])) {echo 'edit';} else {echo 'add';}?>" id="subtype" name="subtype">
                                                <span class="fl alert alert-small alert-warning" id="positioniderr" style="display: none;"><i class="icon error_icon"></i><span id="positionidmsg"></span></span>
                                            </div>

                                            <div class="form-group">
                                                <span class="fl form-label"><i class="require_item">*</i>广告位名称</span>
                                                <span class="fl">
                                                    <input type="text" name="positionname" id="positionname" class="fl form-control form-control-small" placeholder="广告位名称" value="<?php if(!empty($positionInfo["positionid"])) {echo $positionInfo["positionname"];}?>">
                                                </span>
                                                <span class="fl alert alert-small alert-warning" id="positionnameerr" style="display: none; margin-left:10px;"><i class="icon error_icon"></i><span id="positionnamemsg"></span></span>
                                            </div>

                                            <div class="form-group">
                                                <span class="fl form-label">广告位描述</span>
                                            <span class="fl">
                                                <textarea name="positiondesc" id="positiondesc" maxlength="500" class="fl form-control form-control-small" placeholder="广告位描述"><?php if(!empty($positionInfo["positionid"])) {echo $positionInfo["positiondesc"];}?></textarea>
                                            </span>
                                            </div>
                                            <div class="form-group">
                                                <span class="fl form-label"><i class="require_item">*</i>广告形式</span>
                                                <span id="adformdiv" class="fl radio_label">
                                                    <label><input name="adform" id="adform" value="1" type="radio" <?PHP if(!empty($positionInfo["positionid"]) && $positionInfo['adform'] == 1) echo "checked"?>>Banner广告</label>
                                                    <label><input name="adform" id="adform" value="2" type="radio" <?PHP if(!empty($positionInfo["positionid"]) && $positionInfo['adform'] == 2) echo "checked"?>>插屏广告</label>
                                                    <label><input name="adform" id="adform" value="3" type="radio" <?PHP if(!empty($positionInfo["positionid"]) && $positionInfo['adform'] == 3) echo "checked"?>>推荐墙广告</label>

                                                    <?php
                                                    if($isintegral == 1)
                                                    {
                                                        ?>
                                                        <label><input name="adform" id="adform"  value="4" type="radio" <?PHP if(!empty($positionInfo["positionid"]) && $positionInfo['adform'] == 4) echo "checked"?>/>积分墙广告</label>
                                                    <?php
                                                    }
                                                    ?>
                                                    <div class="alert alert-small alert-warning" style="margin-top:20px; display: none;" id="adformerr"><i class="icon error_icon"></i><span id="adformmsg">请选择广告形式。</span></div>
                                            	</span>
                                            </div>

                                            <div id="integrationdiv" class="integral" style="display: <?php if($isintegral != 1 || !isset($positionInfo["positionid"])){echo "none";}?>;">
                                                <i class="icon icon_jiao"></i>
                                                <div class="box">
                                                    <label class="fl">兑换比例：</label>
                                                    <div class="fl">
                                                        1人民币 = <?php echo $integration;?> <?php echo $unit;?>
                                                    </div>
                                                </div>
                                                <div class="box">
                                                    <label class="fl">选择托管方式：</label>
                                                    <div class="fl entrust_form">
                                                        <label><input  name="trusteeshiptype" class="osclass" value="2" type="radio" checked>自定义积分账户</label>
                                                        <span style="display: none;" class="fl alert alert-small alert-warning" id="trusteeshiptypeerr"><b class="icon_errer"></b><em id="trusteeshiptypemsg">请选择托管方式。</em></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" style="display: none">
                                                <span class="fl form-label">奖励返还提示语:</span>
                                            <span class="fl">
                                                <textarea name="rewardesc" id="rewardesc" class="fl form-control form-control-small" maxlength="500" placeholder="奖励返还提示语"><?PHP if(!empty($positionInfo["positionid"])) {echo $positionInfo['rewardesc'];}?></textarea>
                                            </span>
                                            </div>

                                            <div id="popupdiv" class="popupdiv" style="display: <?php if(!isset($positionInfo["positionid"]) || $positionInfo['adform'] != 2 ){echo "none";}?>;">
                                                <div class="form-group">
                                                    <span class="fl form-label"><i class="require_item">*</i>广告形态</span>
                                                    <span id="adstylech" class="fl radio_label">
                                                    	<label><input name="adstyle" id="adstyle" value="0" type="radio" <?PHP if(!isset($positionInfo['adstyle']) || $positionInfo['adstyle'] == 0) echo "checked"?>>全屏</label>
                                                    	<label><input name="adstyle" id="adstyle" value="1" type="radio" <?PHP if(isset($positionInfo['adstyle']) && $positionInfo['adstyle'] == 1) echo "checked"?>>自定义</label>
                                                    </span>
                                                </div>
                                                <div class="form-group" id="adstylediv" style="display: <?PHP if(!isset($positionInfo['adstyle']) || $positionInfo['adstyle'] == 0) echo "none"?>;">
                                                    <div class="form-group">
                                                        <span class="fl form-label"><i class="require_item">*</i>图片尺寸</span>
                                                        <span class="fl radio_label">
                                                        	<label><input name="adstyleimgsize" id="adstyleimgsize" value="1" type="radio" <?PHP if(isset($positionInfo['adstyleimgsize']) && $positionInfo['adstyleimgsize'] == 1 || empty($positionInfo['adstyleimgsize'])) echo "checked"?>> 3:2</label>
                                                        	<label><input name="adstyleimgsize" id="adstyleimgsize" value="2" type="radio" <?PHP if(isset($positionInfo['adstyleimgsize']) && $positionInfo['adstyleimgsize'] == 2) echo "checked"?>> 6:5</label>
                                                        </span>
                                                    </div>
                                                    <div class="form-group" >
                                                        <span class="fl form-label"><i class="require_item">*</i>插屏尺寸</span>
                                                        <span class="fl radio_label">
                                                        	<select name="adstylesize" id="adstylesize">
                                                            	<option value="90" <?PHP if(isset($positionInfo['adstylesize']) && $positionInfo['adstylesize'] == 90 ) echo "selected"?>>90%</option>
                                                            	<option value="75" <?PHP if(isset($positionInfo['adstylesize']) && $positionInfo['adstylesize'] == 75 ) echo "selected"?>>75%</option>
                                                            	<option value="60" <?PHP if(isset($positionInfo['adstylesize']) && $positionInfo['adstylesize'] == 60 ) echo "selected"?>>60%</option>
                                                            	<option value="50" <?PHP if(isset($positionInfo['adstylesize']) && $positionInfo['adstylesize'] == 50 ) echo "selected"?>>50%</option>
                                                        	</select>
                                                        </span>
                                                    </div>
                                                    <div class="form-group" style="display:none">
                                                        <span class="fl form-label"><i class="require_item">*</i>边框颜色</span>
                                                        <span id="bgcolordiv" class="fl radio_label">
                                                        	<select id="adstylecolor" name="adstylecolor" style="width: 100px;background-color: #<?php echo $positionInfo['adstylecolor'];?>">
                                                            	<option <?PHP if(isset($positionInfo['adstylecolor']) && $positionInfo['adstylecolor'] == "") echo "selected"?> style="background-color:#FFFFFF" val="#FFFFFF" value="">无</option>
                                                            	<option <?PHP if(isset($positionInfo['adstylecolor']) && $positionInfo['adstylecolor'] == "FFF68F") echo "selected"?> style="background-color:#FFF68F" val="#FFF68F" value="FFF68F"></option>
                                                            	<option <?PHP if(isset($positionInfo['adstylecolor']) && $positionInfo['adstylecolor'] == "FF8C00") echo "selected"?> style="background-color:#FF8C00" val="#FF8C00" value="FF8C00"></option>
                                                            	<option <?PHP if(isset($positionInfo['adstylecolor']) && $positionInfo['adstylecolor'] == "FF82AB") echo "selected"?> style="background-color:#FF82AB" val="#FF82AB" value="FF82AB"></option>
                                                            	<option <?PHP if(isset($positionInfo['adstylecolor']) && $positionInfo['adstylecolor'] == "87CEFA") echo "selected"?> style="background-color:#87CEFA" val="#87CEFA" value="87CEFA"></option>
                                                            	<option <?PHP if(isset($positionInfo['adstylecolor']) && $positionInfo['adstylecolor'] == "5CACEE") echo "selected"?> style="background-color:#5CACEE" val="#5CACEE" value="5CACEE"></option>
                                                            	<option <?PHP if(isset($positionInfo['adstylecolor']) && $positionInfo['adstylecolor'] == "7FFFD4") echo "selected"?> style="background-color:#7FFFD4" val="#7FFFD4" value="7FFFD4"></option>
                                                            	<option <?PHP if(isset($positionInfo['adstylecolor']) && $positionInfo['adstylecolor'] == "66CDAA") echo "selected"?> style="background-color:#66CDAA" val="#66CDAA" value="66CDAA"></option>
                                                            	<option <?PHP if(isset($positionInfo['adstylecolor']) && $positionInfo['adstylecolor'] == "458B74") echo "selected"?> style="background-color:#458B74" val="#458B74" value="458B74"></option>
                                                            	<option <?PHP if(isset($positionInfo['adstylecolor']) && $positionInfo['adstylecolor'] == "EEEEEE") echo "selected"?> style="background-color:#EEEEEE" val="#EEEEEE" value="EEEEEE"></option>
                                                            	<option <?PHP if(isset($positionInfo['adstylecolor']) && $positionInfo['adstylecolor'] == "AEAEAE") echo "selected"?> style="background-color:#AEAEAE" val="#AEAEAE" value="AEAEAE"></option>
    	                                                       	<option <?PHP if(isset($positionInfo['adstylecolor']) && $positionInfo['adstylecolor'] == "767676") echo "selected"?> style="background-color:#767676" val="#767676" value="767676"></option>
                                                            	<option <?PHP if(isset($positionInfo['adstylecolor']) && $positionInfo['adstylecolor'] == "454545") echo "selected"?> style="background-color:#454545" val="#454545" value="454545"></option>
                                                        	</select>
                                                        </span>
                                                    </div>
                                                    
                                                    <div id="borderdiv" class="form-group">
                                                        <span class="fl form-label"><i class="require_item">*</i>边框设置</span>
                                                        <span class="fl radio_label">
                                                        	<label><input name="bordertype" id="bordertype" value="0" type="radio">无边框</label>
                                                        	<label><input name="bordertype" id="bordertype" value="1" type="radio" checked>系统边框</label>
                                                        	<label><input name="bordertype" id="bordertype" value="2" type="radio">自定义边框</label>
                                                        </span>
                                                    </div>
                                                    <div id="sysBorder" class="form-group">
                                                   	 	<span class="fl form-label"><i class="require_item">*</i>系统边框</span>
						                                <div class="subform">
						                                <span class="fl radio_label check_labels">
							                                <label><input name="borderpolicy" id="borderpolicy" value="1" type="radio" checked>使用单个边框</label>
							                                <?php $sysBorderGroupId=0;
							                                	if(!$sysBorderList){?>
							                                	<span class="fl alert alert-small alert-warning"><em>暂无系统边框</em></span><br/><br/>
							                                <?php }else foreach($sysBorderList as $v){
							                                	if($v['defaultborder']==1) $sysBorderGroupId=$v['bordergroupid'];
							                                ?>
							                                	<img class="border <?php if($v['defaultborder']==1) echo 'checkedborder';?>" id="<?php echo $v['bordergroupid']?>" src="http://res.cocounion.com<?php echo $v['path']?>" alt="cannot shown"/>
							                                <?php }?>
							                                <input type="hidden" id="sysBorderGroupId" value="<?php echo $sysBorderGroupId?>">
	                                                        <label><input name="borderpolicy" id="borderpolicy" value="2" type="radio">所有边框随机</label>
	                                                        <span style="display: none;" class="fl alert alert-small alert-warning" id="sysBordererr"><i class="icon error_icon"></i><b id="sysBordermsg"></b></span>
                                                        </span>
                                                        </div>
						                            </div>
						                            
						                            <div id="cusBorder" class="form-group" style="display:none;">
						                            
						                            	<span class="fl form-label"><i class="require_item">*</i>上传边框</span>
						                            	<div class="form_input"><label class="cueinfo">图片格式支持JPG、PNG；文件大小不超过100KB。</label></div>
														<span class="form_info_cue_errer hidden" id="imgerr"><b class="icon_errer"></b><em></em></span>
							
						                            <div class="uploadBorder">
						                            	<div class="form_label"></div>
				    									<div class="borderImg">
				    										<input type="file" name='Filedata'  id="border1" class='uploadify' onchange='ajaxUploder(this,"600*500",12)'/>&nbsp;&nbsp; <br/>
				    										<div class="form_input"><label class="cueinfo">图片尺寸：600×500 图片要求：外部窗口600*500，内部窗口566*468</label>
				    										<span id="bordererr1" style="display: none; margin-left:10px;"><i class="icon error_icon"></i><span id="bordermsg1"></span></span>
				    										</div>
				    										<div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
				    									</div><br>
				    									
				    									
				    									<div class="form_label"></div>
				    									<div class="borderImg">
				    										<input type="file" name='Filedata'  id="border2" class='uploadify'  onchange='ajaxUploder(this,"500*600",13)'/>&nbsp;&nbsp; <br/>
				    										<div class="form_input"><label class="cueinfo">图片尺寸：500*600 图片要求：外部窗口500*600，内部窗口466*568</label>
				    										<span id="bordererr2" style="display: none; margin-left:10px;"><i class="icon error_icon"></i><span id="bordermsg2"></span></span>
				    										</div>
				    										<div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
				    									</div><br>
				    									
				    									
				    									<div class="form_label"></div>
				    									<div class="form_input">
				    										<input type="file" name='Filedata'  id="border3" class='uploadify'  onchange='ajaxUploder(this,"900*600",8)'/>&nbsp;&nbsp; <br/>
				    										<div class="form_input"><label class="cueinfo">图片尺寸：900*600 图片要求：外部窗口900*600，内部窗口866*568</label>
				    										<span id="bordererr3" style="display: none; margin-left:10px;"><i class="icon error_icon"></i><span id="bordermsg3"></span></span>
				    										</div>
				    										<div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
				    									</div><br>
				    									<em class="red" ><b class="icon_error" id='1692_default_imgs_error'></b></em>
				    									
				    									<div class="form_label"></div>
				    									<div class="form_input">
				    										<input type="file" name='Filedata'  id="border4" class='uploadify'  onchange='ajaxUploder(this,"600*900",9)'/>&nbsp;&nbsp; <br/>
				    										<div class="form_input"><label class="cueinfo">图片尺寸：600*900 图片要求：外部窗口600*900，内部窗口566*868</label>
				    										<span id="bordererr4" style="display: none; margin-left:10px;"><i class="icon error_icon"></i><span id="bordermsg4"></span></span>
				    										</div>
				    										<div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
				    									</div><br>
				    									<em class="red" ><b class="icon_error" id='1692_default_imgs_error'></b></em>
				    									
				    									<div class="form_label"></div>
				    									<div class="form_input">
				    										<input type="file" name='Filedata'  id="border5" class='uploadify'  onchange='ajaxUploder(this,"1280*720",10)'/>&nbsp;&nbsp; <br/>
				    										<div class="form_input"><label class="cueinfo">图片尺寸：1280*720 图片要求：外部窗口1280*720，内部窗口1246*688</label>
				    										<span id="bordererr5" style="display: none; margin-left:10px;"><i class="icon error_icon"></i><span id="bordermsg5"></span></span>
				    										</div>
				    										<div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
				    									</div><br>
				    									<em class="red" ><b class="icon_error" id='1692_default_imgs_error'></b></em>
				    									
				    									<div class="form_label"></div>
				    									<div class="form_input">
				    										<input type="file" name='Filedata'  id="border6" class='uploadify'  onchange='ajaxUploder(this,"720*1280",11)'/>&nbsp;&nbsp; <br/>
				    										<div class="form_input"><label class="cueinfo">图片尺寸：720*1280 图片要求：外部窗口720*1280，内部窗口686*1248</label>
				    										<span id="bordererr6" style="display: none; margin-left:10px;"><i class="icon error_icon"></i><span id="bordermsg6"></span></span>
				    										</div>
				    										<div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
				    									</div><br>
				    									<em class="red" ><b class="icon_error" id='1692_default_imgs_error'></b></em>
				    									</div>
						                            </div>
						                            
						                            
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="new_position_btn_group">
                                        <a href="javascript:;" id="addBtnOk" class="btn btn-success btn-success-noboder"  onclick="return false;">确&nbsp;&nbsp;&nbsp;&nbsp;认</a>
                                        <a href="javascript:;" id="addBtnCancel" class="close btn btn-default btn-default-noboder">取&nbsp;&nbsp;&nbsp;&nbsp;消</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="function_btn">
                        <button type="submit" id="next_app" class="btn btn-primary btn-primary-noboder" onclick="location.href='/manage/appinfo/showdetail?appid=<?php echo $appid?>';">下一步</button>
                        <button type="reset" onclick="confirmCancel();" class="btn btn-default btn-default-noboder">取消</button>
                    </div>
                    <div style="height : 50px;"></div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php $this->load->view("manage/inc/footer");?>

<script>
var r = /^\+?[0-9][0-9]*$/;
$(document).ready(function(){

	$('.check_labels').on('click','.border',function(){
		$('.check_labels').find('.border').removeClass('checkedborder');
        $(this).toggleClass('checkedborder');
        $('#sysBorderGroupId').val($(this).attr('id'));
		if($('input:radio[name="borderpolicy"]:checked').val() == 2)
		{
			$('.check_labels').find('.border').removeClass('checkedborder');
		}
    })
    $("#sysBorder :radio").click(function(){
		if($('input:radio[name="borderpolicy"]:checked').val() == 2){
			$('.check_labels .border').removeClass('checkedborder');
			$('#sysBorderGroupId').val('0');
		}
    });
	$("#borderdiv :radio").click(function(){
		if($('input:radio[name="bordertype"]:checked').val() == 1){
			$('#sysBorder').show();
		}else{
			$('#sysBorder').hide();
		}

		if($('input:radio[name="bordertype"]:checked').val() == 2){
			$('#cusBorder').show();
		}else{
			$('#cusBorder').hide();
		}
    });
    
	$("#addBtnOk").click(function(){
		var positionid = $("#positionid").val();
		var positionname = $("#positionname").val();
		var positiondesc = $("#positiondesc").val();
		var adform = $('input:radio[name="adform"]:checked').val();
		var appid = $("#appid").val();
		var trusteeshiptype = $('input:radio[name="trusteeshiptype"]:checked').val();
		var rewardesc = $("#rewardesc").val();
		var subtype = $("#subtype").val();
        var adstyle = $('input:radio[name="adstyle"]:checked').val();
        var adstyleimgsize = $('input:radio[name="adstyleimgsize"]:checked').val();
        var adstylesize = $("#adstylesize").val();
        var adstylecolor = $("#adstylecolor").val();
        var bordertype = $('input:radio[name="bordertype"]:checked').val();
        var borderpolicy = $('input:radio[name="borderpolicy"]:checked').val();
        var img_border1 = $("#img_border1").val()==null?"":$("#img_border1").val();
        var img_border2 = $("#img_border2").val()==null?"":$("#img_border2").val();
        var img_border3 = $("#img_border3").val()==null?"":$("#img_border3").val();
        var img_border4 = $("#img_border4").val()==null?"":$("#img_border4").val();
        var img_border5 = $("#img_border5").val()==null?"":$("#img_border5").val();
        var img_border6 = $("#img_border6").val()==null?"":$("#img_border6").val();
        var sysBorderGroupId = $('#sysBorderGroupId').val();

	    $("#positionnameerr").hide();
        $("#adformerr").hide();
        $("#positioniderr").hide();
        $("#trusteeshiptypeerr").hide();
        $("#sysBordererr").hide();
        for(var i=1; i<=6; i++)
        {
        	$("#bordererr"+i).hide();
        }
        if(subtype == 'edit')
        {
	    	$url = '/manage/appposition/doedit';
        }
        else
        {
        	$url = '/manage/appposition/doadd';
        }
		$.post($url, 
			{ 
				positionid:positionid,
				positionname:positionname,
				positiondesc:positiondesc,
				adform:adform,
				appid:appid,
				trusteeshiptype:trusteeshiptype,
				rewardesc:rewardesc,
                adstyle:adstyle,
                adstyleimgsize:adstyleimgsize,
                adstylesize:adstylesize,
                adstylecolor:adstylecolor,
                bordertype:bordertype,
                borderpolicy:borderpolicy,
                img_border1:img_border1,
                img_border2:img_border2,
                img_border3:img_border3,
                img_border4:img_border4,
                img_border5:img_border5,
                img_border6:img_border6,
                sysBorderGroupId:sysBorderGroupId,
    			r:Math.random()
			}, 
			function(data)
			{
    			if(data && data['status'] == 0){
        			if(data['data'] != '')
        			{
        				$("#"+data['data'][0]).html(data['data'][1]);
            		}
    				$("#"+data['info']).show();
    				return false;
    			}else{
    				location.href = '/manage/appposition/showadd?appid='+appid;
    			}
			},
			'json');
		return false;
	});
	$("#bgcolordiv select").change(function(){
		var index = $(this).val();
		var bgcolor = $(this).find("option[value="+index+"]").attr("val");
		$(this).css("background-color",bgcolor);
	})
});

function ajaxUploder(fileObj, imgtype, sizeid) {
	var obj 			= $(fileObj);
	var fileId 			= obj.attr('id');
	var maindiv 		= obj.parents('div:eq(0)');


	var loading = obj.siblings('.onload').show();
	var err_msg = obj.parents('div:eq(0)').siblings('span:eq(0)');

	$.ajaxFileUpload( {
				url : '/manage/appposition/ajaxDoUploadStuff/?type=' + imgtype,
				secureuri : true,
				fileElementId : fileId,
				dataType : 'json',// 服务器返回的格式，可以是json
				success : function(data, status) {
					err_msg.hide();
					var msgObj = data[0]
					if (msgObj.isReceived == true) {
						var str = '<div class="'+fileId+'_image_show_box">';
						
						str+='<img src="/public/upload'+msgObj.imgInfo['newImg']+'" class="border" /><a href="/public/upload'+msgObj.imgInfo['newImg']+'" target="_blank">查看实际效果</a>';	
						str+='<input type="hidden" id="img_'+fileId+'" value="'+msgObj.imgInfo['newImg']+'"/>';
		
						str += '&nbsp;&nbsp;</div>';
						maindiv.find('.'+fileId+'_image_show_box').remove();
						maindiv.append(str);
						maindiv.find('.'+fileId+'_image_show_box').fadeIn("slow");

					} else {
						var msg = msgObj.errorMessage;
						alert('    ' + msg);
					}
					loading.hide();
					return true;
				},
				error : function(data, status, e) {
					loading.hide();
				}
			})
}

function ConfirmDel(appid, positionid) {
	if (confirm("确定要删除此条广告位信息吗？")){
			$.get("/manage/appposition/ajaxDoDel", 
			{ 
				appid : appid,
				positionid : positionid,
    			r:Math.random()
			}, 
			function(data)
			{

    				location.href = '/manage/appposition/showadd?appid='+appid;
			},
			'json');
	}else{
		return false;
	}
}

$("#adformdiv :radio").click(function(){
	var adform = $('input:radio[name="adform"]:checked').val();
	$("#integrationdiv").hide();
	$("#popupdiv").hide();
	if(adform == 4)
	{
		$("#integrationdiv").show();
		}
	else if(adform == 2)
	{
		$("#popupdiv").show();
		}
	})

$("#adstylech :radio").click(function(){
	var adstyle = $('input:radio[name="adstyle"]:checked').val();
	$("#adstylediv").hide();
	if(adstyle == 1)
	{
		$("#adstylediv").show();
		}
	})

$("#adstylech :radio").click(function(){
	var adstyle = $('input:radio[name="adstyle"]:checked').val();
	$("#adstylediv").hide();
	if(adstyle == 1)
	{
		$("#adstylediv").show();
		}
	})
	
	$('#addBtnCancel').click(function(){
		$('#newPosition').hide();
		$('#newadd').show();
		});

	$('#newadd').click(function(){
		$('#newPosition').show();
		$('#newadd').hide();
		});

function confirmCancel(){
    art.dialog({
        title:'<i class="icon icon_node"></i>提示',
        lock: true,
        fixed:true,
        ok:function(){
            location.href='/manage/appinfo/showlist';
            return true;
        },
        cancel:true,
        background: '#fff', // 背景色
        opacity: 0.6,	// 透明度
        content: '<div style="text-align: center; font-size:14px;">您确定要取消吗？</div>',
        id: 'deleteLightBox',
        okVal:'确认',
        cancelVal:'取消'
    });
}
</script>
<!--  主体部分 [ -->
<div class="inner_container" id="innerContainer">
    <?php $this->load->view('manage/inc/left')?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <?php $this->load->view('manage/inc/righttop')?>
                <div class="inner_tab">
                    <ul class="tab_item">
                        <li><a href="/manage/appchannel/showinfo/?appid=<?php echo $appid?>">渠道管理</a></li>
                        <li><a href="/manage/appposition/showinfo/?appid=<?php echo $appid?>">广告位管理</a></li>
                        <li><a href="/manage/apptag/showinfo/?appid=<?php echo $appid?>">标签管理</a></li>
                        <li class="active"><a href="javascript:;">基本信息</a></li>
                        <?php if($isintegral == 1){  ?><li ><a href="/manage/appinfo/backconfig/?appid=<?php echo $appid?>">积分回调方式</a></li><?php } ?>
                    </ul>
                    <div class="jumbotron jumbotron_no_shadow tab_box base_info" >
                        <div id="editDone" class="edit_done">
                            <table class="table table-bordered table-bordered-rlborder">
                                <tr>
                                    <td><label>系统类型：</label>
                                        <?php
                                        if($ostypeid == 1)
                                        {
                                            echo 'Android';
                                        }
                                        elseif($ostypeid == 2)
                                        {
                                            echo 'iOS';
                                        }?>
                                    </td>
                                    <td><label>平台类型：</label>
                                        <?php
                                        if($ostypeid == 2)
                                        {
                                            if($devicetypeid == 2) echo 'iPhone';
                                            if($devicetypeid == 3) echo 'iPad';
                                            if($devicetypeid == 4) echo 'Universal';
                                        }else{
                                            echo '无';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label>应用名称：</label><?php echo $appname?></td>
                                    <td><label>应用ID：&nbsp;&nbsp;&nbsp;</label><?php echo $appid?></td>
                                </tr>
                                <tr>
                                    <td><label>应用包名称：</label><?php echo $packagename?></td>
                                    <td><label>下载地址：</label><?php echo $appurl?></td>
                                </tr>
                                <tr>
                                    <td colspan="2"><label>对接密钥：</label><?php echo $secretkey?></td>
                                </tr>
                                <tr>
                                    <td colspan="2"><label>积分墙：</label>
                                        <?php if($isintegral == 1){echo '开启';}else{echo '未开启';}?>
                                    </td>
                                </tr>
                                <tr style="display:<?php if($isintegral == 0){echo 'none';}?>" id="isintegraldiv">
                                    <td colspan="2">
                                        <label>兑换比例：</label>
                                        <span style="font-family: 'arial'">&yen;</span> 1  = <?php echo $integration?> <?php echo $unit?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="app_classify">
                                        <label>应用分类：</label>
                                        <div class="list">
                                            <strong><?php echo $typename?></strong>
                                            <?php
                                            if(isset($childtypename) && !empty($childtypename))
                                            {?>
                                                <div style="padding:2px;" class="form_input subform">
                                                    <?php
                                                    foreach ($childtypename as $value) {
                                                        ?>
                                                        <span data-v=""><?php echo $value?></span>
                                                    <?php
                                                    }
                                                    ?>
                                                </div>
                                            <?php }?>
                                        </div>
                                    </td>
                                </tr>
                            <tr>
                                <td colspan="2"  class="detail">
                                    <label>应用介绍：</label>
                                    <div class="detail_content">
                                        <?php echo $appdescription?>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <div class="btn-group function_btn base_info_btn_group">
                            <a href="/manage/appinfo/showedit/?appid=<?php echo $appid?>" id="baseInfoEdit" class="btn btn-primary btn-primary-noboder"><i class="icon icon_edit_white_con"></i>编&nbsp;&nbsp;&nbsp;&nbsp;辑</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- ] 主体部分 -->
<?php $this->load->view("manage/inc/footer");?>
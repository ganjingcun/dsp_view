<!--  主体部分 [ -->
<link href="/public/version1.0/css/bootstrap.min.css" rel="stylesheet">
<script src="/public/version1.0/js/bootstrap.min.js"></script>
<div class="inner_container" id="innerContainer">
    <?php $this->load->view('manage/appinfo/left')?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <header class="title">
                    <strong class="title_strong"><i class="icon top_title_icon"></i>SDK下载</strong>
                </header>
                <div id="sdkSlides" class="sdk_slides">
                    <div class="slides_container">

                        <div class="inner">
                            <div class="box">
                                <div class="fl sdk_theme sdk_img_ios"></div>
                                <div class="fl down_content">
                                    <h3>CocosAds 广告SDK</h3>
                                    <div class="sdk_theme skd_ios_font"></div>
                         			
                         			<!-- <ul class="nav nav-tabs">
									   <li class="active"><a href="#common" data-toggle="tab">普通SDK</a></li>
									   <li><a href="#video" data-toggle="tab">视频SDK</a></li>
									</ul> -->
									<div id="myTabContent" class="tab-content" style="margin-top: 20px; ">
									   <div class="tab-pane fade in active" id="common">
									   <?php if($iossdkinfo1['id']!="" || $iossdkinfo1['id']!=null){?>
									     	<a href="javascript:void(0);" id="iosdown" class="small-btn small-btn-primary"><i class="icon download_icon"></i><!-- 普通 -->SDK下载</a>
		                                    <div class="detail">
		                                        <p><label>版本：</label><?php echo $iossdkinfo1['version']?></p>
		                                        <p><label>大小：</label><?php echo $iossdkinfo1['size']?> (核心库)</p>
		                                        <p><label>更新时间：</label><?php echo date("Y-m-d",$iossdkinfo1['updatetime'])?></p>
		                                        <p><label>md5sum：</label><?php echo $iossdkinfo1['md5sum']?></p>
		                                    </div>
		                                    <div class="news">
		                                        <h4>更新动态</h4>
		                                        <p><?php echo $iossdkinfo1['content']?></p>
		                                    </div>
		                                    <div><a href="/manage/appinfo/showsdkinfolist/?ostype=1&sdktype=1">历史版本</a></div>
		                                    <?php }
		                                    else{?>
		                                   <p><label style="font-size: 18px;">暂无版本信息</label></p>
		                                    <?php }?>
									   </div>
									   <div class="tab-pane fade" id="video">
									   <?php if($iossdkinfo2['id']!="" || $iossdkinfo2['id']!=null){?>
									     	<a href="javascript:void(0);" id="videodown" class="small-btn small-btn-primary"><i class="icon download_icon"></i>视频SDK下载</a>
		                                    <div class="detail">
		                                        <p><label>版本：</label><?php echo $iossdkinfo2['version']?></p>
		                                        <p><label>大小：</label><?php echo $iossdkinfo2['size']?> (核心库)</p>
		                                        <p><label>更新时间：</label><?php echo date("Y-m-d",$iossdkinfo2['updatetime'])?></p>
		                                        <p><label>md5sum：</label><?php echo $iossdkinfo2['md5sum']?></p>
		                                    </div>
		                                    <div class="news">
		                                        <h4>更新动态</h4>
		                                        <p><?php echo $iossdkinfo2['content']?></p>
		                                    </div>
		                                    <div><a href="/manage/appinfo/showsdkinfolist/?ostype=1&sdktype=2">历史版本</a></div>
		                                <?php }
		                                    else{?>
		                                    <p><label style="font-size: 18px;">暂无版本信息</label></p>
		                                    <?php }?>
									   </div>
									</div>
                         			
                                </div>
                            </div>
                        </div>

                        <div class="inner">
                            <div class="box">
                                <div class="fl sdk_theme sdk_img_android"></div>
                                <div class="fl down_content">
                                    <h3>CocosAds 广告SDK</h3>
                                    <div class="sdk_theme skd_android_font"></div>
                                    <a href="javascript:;" id="androiddown" class="small-btn small-btn-primary"><i class="icon download_icon"></i>SDK下载</a>
                                    <div class="detail">
                                        <p><label>版本：</label><?php echo $androidsdkinfo['version']?></p>
                                        <p><label>大小：</label><?php echo $androidsdkinfo['size']?>(核心库)</p>
                                        <p><label>更新时间：</label><?php echo date("Y-m-d",$androidsdkinfo['updatetime'])?></p>
                                        <p><label>md5sum：</label><?php echo $androidsdkinfo['md5sum']?></p>
                                    </div>
                                    <div class="news">
                                        <h4>更新动态</h4>
                                        <p><?php echo $androidsdkinfo['content']?></p>
                                    </div>
                                    <div><a href="/manage/appinfo/showsdkinfolist/?ostype=2&sdktype=1">历史版本</a></div>
                                </div>
                            </div>
                        </div>

                        <div class="inner">
                            <div class="box">
                                <div class="fl sdk_theme sdk_img_cocos"></div>
                                <div class="fl down_content">
                                    <h3>CocosAds 广告SDK</h3>
                                    <div class="sdk_theme skd_cocos_font"></div>
                                    <a href="javascript:;" id="cocosdown" class="small-btn small-btn-primary"><i class="icon download_icon"></i>SDK下载</a>
                                    <div class="detail">
                                        <p><label>版本：</label><?php echo $cocossdkinfo['version']?></p>
                                        <p><label>大小：</label><?php echo $cocossdkinfo['size']?>(核心库)</p>
                                        <p><label>更新时间：</label><?php echo date("Y-m-d",$cocossdkinfo['updatetime'])?></p>
                                        <p><label>md5sum：</label><?php echo $cocossdkinfo['md5sum']?></p>
                                    </div>
                                    <div class="news">
                                        <h4>更新动态</h4>
                                        <p><?php echo $cocossdkinfo['content']?></p>
                                    </div>
                                    <div><a href="/manage/appinfo/showsdkinfolist/?ostype=3&sdktype=1">历史版本</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a href="javascript:;" class="prev">iOS</a>
                    <a href="javascript:;" class="next">Android</a>
                </div>
            </div>
    </section>
</div>
<?php $this->load->view("manage/inc/footer");?>
<script>
$(function(){
	$(".pagination").css("padding-left","40px");
	$(".sdk_slides .pagination li:first-child").css('width','65px');
	$(".pagination a").css("border","none");
	$(".sdk_slides").css('height','700px');
	$(".sdk_slides .slides_container").css('height','698px');
	$(".sdk_slides .slides_container .inner").css('height','696px');
	$(".sdk_slides .slides_container .inner .box").css('height','694px');
	$(".sdk_slides .next").css('width','66px');
	$(".sdk_slides .prev").css('width','66px');
	$(".sdk_slides .pagination").css('top','-790px');
	$(".nav-tabs a").css('font-family','微软雅黑');
	$(".nav-tabs a").css('color',' #303030');
	$("#common").hide();
	$("#video").hide();
	 $('.nav-tabs a').click(function(e){
		 //$("#myTabContent .tab-pane").hide();
		 $("#myTabContent .active").hide();
		  $($(this).attr("href")).show();
		});
		
	$(".pagination").css("display","block");
	
	$("#iosdown").click(function(){
		window.open("<?php echo base_url(strstr($iossdkinfo1['packpath'], 'public/upload/'))?>", 'down');
	});
	$("#videodown").click(function(){
		window.open("<?php echo base_url(strstr($iossdkinfo2['packpath'], 'public/upload/'))?>", 'down');
	});
	$("#androiddown").click(function(){
 		window.open("<?php echo base_url(strstr($androidsdkinfo['packpath'], 'public/upload/'))?>", 'down');
	});
	$("#cocosdown").click(function(){
 		window.open("<?php echo base_url(strstr($cocossdkinfo['packpath'], 'public/upload/'))?>", 'down');
	});
	$("#common").show();
});
</script>
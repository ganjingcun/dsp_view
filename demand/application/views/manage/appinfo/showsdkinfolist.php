<!--  主体部分 [ -->
<link href="/public/version1.0/css/bootstrap.min.css" rel="stylesheet">
<script src="/public/version1.0/js/bootstrap.min.js"></script>
<div class="inner_container" id="innerContainer">
    <?php $this->load->view('manage/appinfo/left')?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <header class="title">
                    <strong class="title_strong"><i class="icon top_title_icon"></i>历史版本</strong>
                </header>
                <div class="history_version">
                	<!-- <ul class="nav nav-tabs" style="padding-left: 0px;width: 700px; margin-left: 520px;<?php if($ostype==1){?>visibility:visible;<?php }else {?>visibility:hidden;<?php }?>">
					   <li class="active" id="ioscommontab"><a href="#ioscommon" data-toggle="tab">普通SDK</a></li>
					   <li id="iosvideotab"> <a href="#iosvideo" data-toggle="tab">视频SDK</a></li>
					</ul> -->
					<div class="box">
                        <div class="top"><i class="icon top_line"></i></div>
                        <div class="middle">             	
                            <div class="line"></div>                        
                            <ul id="sdkDetailsList">
                                <li class="empty"></li>
                                <?php if($ostype==1){?>	
			                								
								<div id="myTabContent" class="tab-content">
									<div class="tab-pane fade in active" id="ioscommon">
									<?php foreach ($ioscommon as $ios){?>
										<li>
											<label class="fl">版本：<?php echo $ios['version'];?><i class="icon icon_dot"></i></label>
		                                    <div class="detail fl">
		                                        <p><span>大小：</span><?php echo $ios['size'];?>(核心库)</p>
		                                        <p><span>更新时间：</span><?php  echo date("Y-m-d",$ios['updatetime']);?></p>
		                                        <p><span>md5sum：</span><?php echo $ios['md5sum'];?></p>
		                                    </div>
		                                    <div class="news fl">
		                                        <label class="fl">更新动态</label>
		                                        <div class="newsinner fl">
		                                            <p><?php echo $ios['content'];?></p>
		                                        </div>
		                                    </div>
										</li>
										<?php }?>
									</div>
									<div class="tab-pane fade in active" id="iosvideo">
									<?php if($iosvideo!="" || $iosvideo !=null || $iosvideo!=false){
										foreach ($iosvideo as $ios){?>
										<li>
											<label class="fl">版本：<?php echo $ios['version'];?><i class="icon icon_dot"></i></label>
		                                    <div class="detail fl">
		                                        <p><span>大小：</span><?php echo $ios['size'];?>(核心库)</p>
		                                        <p><span>更新时间：</span><?php echo date("Y-m-d",$ios['updatetime']);?></p>
		                                        <p><span>md5sum：</span><?php echo $ios['md5sum'];?></p>
		                                    </div>
		                                    <div class="news fl">
		                                        <label class="fl">更新动态</label>
		                                        <div class="newsinner fl">
		                                            <p><?php echo $ios['content'];?></p>
		                                        </div>
		                                    </div>
										</li>
										<?php }
										}
										else{?>
										<li>
											<label class="fl">  <i class="icon icon_dot"></i></label>
			                                    <div class="detail fl">
			                                        <p><span style="font-size: 18px;">暂无版本信息</span>
			                                    </div>
										</li>
										<?php }?>
									</div>
								</div>
								<?php }?>
								
								<?php if($ostype==2){?>	
                                <div id="androidcommon">
	                                <?php foreach ($androidcommon as $android){?>
	                                <li >
	                                    <label class="fl">版本：<?php echo $android['version'];?><i class="icon icon_dot"></i></label>
	                                    <div class="detail fl">
	                                        <p><span>大小：</span><?php echo $android['size'];?>(核心库)</p>
	                                        <p><span>更新时间：</span><?php echo date("Y-m-d",$android['updatetime']);?></p>
	                                        <p><span>md5sum：</span><?php echo $android['md5sum'];?></p>
	                                    </div>
	                                    <div class="news fl">
	                                        <label class="fl">更新动态</label>
	                                        <div class="newsinner fl">
	                                            <p><?php echo $android['content'];?></p>
	                                        </div>
	                                    </div>
	                                </li>
	                                <?php }?>
                                </div>
                                <?php }?>

                                <?php if($ostype==3){?>	
                                <div id="cocoscommon">
	                                <?php foreach ($cocoscommon as $android){?>
	                                <li >
	                                    <label class="fl">版本：<?php echo $android['version'];?><i class="icon icon_dot"></i></label>
	                                    <div class="detail fl">
	                                        <p><span>大小：</span><?php echo $android['size'];?>(核心库)</p>
	                                        <p><span>更新时间：</span><?php echo date("Y-m-d",$android['updatetime']);?></p>
	                                        <p><span>md5sum：</span><?php echo $android['md5sum'];?></p>
	                                    </div>
	                                    <div class="news fl">
	                                        <label class="fl">更新动态</label>
	                                        <div class="newsinner fl">
	                                            <p><?php echo $android['content'];?></p>
	                                        </div>
	                                    </div>
	                                </li>
	                                <?php }?>
                                </div>
                                <?php }?>
                                <li class="empty"></li>
                            </ul>
                        </div>
                        <div class="bottom"><i class="icon bottom_line"></i></div>
                    </div>
                    <div class="btn_sdk">
                        <a href="/manage/appinfo/showsdkinfolist/?ostype=1&sdktype=1" class="<?php echo $_GET['ostype']==1 ? 'active':''; ?> btn_sdk_ios"></a>
                        <a href="/manage/appinfo/showsdkinfolist/?ostype=2&sdktype=1" class="<?php echo $_GET['ostype']==2 ? 'active':''; ?> btn_sdk_android"></a>
                        <a href="/manage/appinfo/showsdkinfolist/?ostype=3&sdktype=1" class="<?php echo $_GET['ostype']==3 ? 'active':''; ?> btn_sdk_cocos"></a>
                    </div>
                </div>
            </div>
    </section>
</div>

<?php $this->load->view("manage/inc/footer");?>
<script>
    $(function(){
        $('#sdkDetailsList').niceScroll({
            preservenativescrolling:false,
            cursorcolor:"#ccc",
            cursoropacitymax:1,
            touchbehavior:false,
            cursorwidth:"5px",
            cursorborder:"0",
            cursorborderradius:"5px"
        });

        $('.history_version .btn_sdk').css('top','-565px');
        $(".nav-tabs a").css('font-family',"Helvetica Neue");
        $(".nav-tabs a").css('color',' #303030');
    	$("#ioscommon").hide();
    	$("#iosvideo").hide();
    	$("#androidcommon").hide();
    	var ostype=<?php echo $ostype?>;
    	var sdktype=<?php echo $sdktype?>;
    	 $('.nav-tabs a').click(function(e){
    		  $("#myTabContent .active").hide();
    		  $($(this).attr("href")).show();
    		  $("#androidcommon").hide();
    		  $("#cocoscommon").hide();
    		});
    	if(ostype==1){
        	$('.nav-tabs .active').removeClass('active');
        	if(sdktype==1){
    			$("#ioscommon").show();
    			$('.nav-tabs').find("li[id='ioscommontab']").addClass('active');
        	}
        	else if(sdktype==2){
            	$('#iosvideo').show();
            	$('.nav-tabs').find("li[id='iosvideotab']").addClass('active');
        	}
    	}
    	else if (ostype==2){
    		$("#androidcommon").show();
    	}
    	else if (ostype==3){
    		$("#cocoscommon").show();
    	}
    	
    })
</script>

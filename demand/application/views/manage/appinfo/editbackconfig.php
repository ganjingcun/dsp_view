<!--  主体部分 [ -->
<div class="inner_container" id="innerContainer">
    <?php $this->load->view('manage/inc/left')?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <?php $this->load->view('manage/inc/righttop')?>
                <div class="inner_tab">
                    <ul class="tab_item">
                        <li><a href="/manage/appposition/showinfo/?appid=<?php echo $appid?>">广告位管理</a></li>
                        <li><a href="/manage/appchannel/showinfo/?appid=<?php echo $appid?>">渠道管理</a></li>
                        <li><a href="/manage/apptag/showinfo/?appid=<?php echo $appid?>">标签管理</a></li>
                        <li ><a href="/manage/appinfo/showinfo/?appid=<?php echo $appid; ?>">基本信息</a></li>
                        <?php if($isintegral == 1){  ?><li class="active"><a href="javascript:;">积分回调方式</a></li><?php } ?>
                    </ul>
                    <form action="/manage/appinfo/saveconfig" name="frm" method="post">
                        <input type="hidden" name="old_secretKey" id="old_secretKey" value="<?php echo $appcampaignid ?>">
                    <div class="jumbotron jumbotron_no_shadow tab_box base_info" >
                        <div id="editInfo" class="edit_info" style="padding-left: 10px;">
                             
                            <div class="form-group">
                                <span class="fl form-label" style="width: 280px;"><i class="require_item">*</i>积分墙回调方式：</span>
                                <span class="fl">
                                  <label  style="font-weight: lighter;"><input type="radio"  name="wallrechargetype" class="wallrechargetype" value="0"<?php if($wallrechargetype == 0){ ?> checked <?php } ?> value="0"  >客户端回调
                                   </label><label  style="font-weight: lighter;"><input type="radio"  name="wallrechargetype" class="wallrechargetype" value="1"  <?php if($wallrechargetype == 1){ ?>checked <?php } ?> value="1"  >服务器端回调
                                 </label></span>
                            </div>
                            <div class="control-group " name="appcampaigniddiv" id="appcampaigniddiv"  <?php if($wallrechargetype == 0){ ?>style="display: none;"<?php } ?>>
                            <div class="form-group">
                                <span class="fl form-label" style="width: 280px;"><i class="require_item">*</i>服务器端地址：</span>
                                <span class="fl values">
                                    <input type="text" class="fl form-control form-control-small" name="back_url" id="back_url" value="<?php if(!empty($coco_url)) echo $coco_url;?>"  placeholder="服务器端地址">
                                    <span class="fl alert alert-small alert-warning" style="margin-left: 10px;line-height:18px;display: none" id="back_urlerr"><i class="icon error_icon"></i><span id="back_urlmsg"></span></span>
                                </span>
                            </div>
                             <div class="form-group">
                                <span class="fl form-label" style="width: 280px;">服务器密钥(可选)：</span>
                                <span class="fl">
                                    <input type="text" name="secretKey" id="secretKey"  class="fl form-control form-control-small"  value="<?php if(!empty($secretKey)) echo $secretKey;?>" placeholder="请填写服务器密钥">
                                    <span class="fl alert alert-small alert-warning" style="margin-left: 10px;line-height:18px;display: none" id="secretKeyerr"><i class="icon error_icon"></i><span id="secretKeymsg"></span></span>
                                </span>
                            </div>
                             </div>
                             <div class="btn-group base_info_btn_group" style="margin-left: 220px;">
                                <a href="javascript:;" id="njh_add_app" class="btn btn-success btn-success-noboder">保&nbsp;&nbsp;&nbsp;&nbsp;存</a>
                                <a href="/manage/appinfo/backconfig/?appid=<?php echo $appid; ?>" id="" class="btn btn-default btn-default-noboder">取&nbsp;&nbsp;&nbsp;&nbsp;消</a>
                            </div>
                        
                        </div>
                    </div>
                    </form>
                </div>
            </div>
    </section>
</div>
<!-- ] 主体部分 -->
<?php $this->load->view("manage/inc/footer");?>
<script>
function onClickSubmit(e){
    var _btn = $(this)
    var buttonCss = {
            "background-color":_btn.css("background-color"), 
            "color":_btn.css("color"),
            "cursor":_btn.css("cursor")
   }
//    console.log(buttonCss)
    var wallrechargetype = $('input:radio[name="wallrechargetype"]:checked').val()
    var url =  $('#back_url').val();
    var secretKey =  $('#secretKey').val(); 
    var old_secretKey = $('#old_secretKey').val();
    $("#back_urlerr").hide();
    $("#secretKeyerr").hide();
    _btn.html("保存中...").unbind("click").css({"background-color":"orange", "color":"#333","cursor":"auto"});
    $.post("/manage/appinfo/saveconfig?appid="+"<?php echo $appid; ?>", { 'url':url,'secretKey':secretKey,'old_secretKey':old_secretKey,'wallrechargetype':wallrechargetype},  function(data) {
          //console.log(data);
          _btn.css(buttonCss).html("保    存").click(onClickSubmit);
          if(data && data['status'] == 0){
                 if(data['data'] == 'error'){
                    alert('设置失败,请重试');
                }else{
                  if(data['data'] != '')
                  {
                      $("#"+data['data'][0]).html(data['data'][1]);
                  }
                  $("#"+data['info']).show();
                  
                  return false;
                }
          }else{
        	  _btn.html("保    存")
              alert('设置成功');
              location.href = '/manage/appinfo/backconfig/?appid='+<?php echo $appid; ?>;
          }
      },
      'json');
      return false;
}
$(document).ready(function(){
    $("#njh_add_app").click(onClickSubmit);
    $(".wallrechargetype:radio").click(function(){
        $(this).val()==0 ? $("#appcampaigniddiv").hide() : $("#appcampaigniddiv").show();
    });
})

</script>

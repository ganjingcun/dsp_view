<!--  主体部分 [ -->
<div class="inner_container" id="innerContainer">
    <?php $this->load->view('manage/appinfo/apileft')?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <header class="title">
                    <strong class="title_strong"><i class="icon top_title_icon"></i>历史版本</strong>
                </header>
                <div class="history_version">
                    <div class="box">
                        <div class="top"><i class="icon top_line"></i></div>
                        <div class="middle">
                            <?php
                                if($listinfo != false ){
                                ?>
                                <div class="line"></div>
                            	<ul id="apiDetailsList">                                
                               
	                                <li class="empty"></li>
                                <?php
	                                foreach ($listinfo as $value) {
	                                ?>
	                                <li>
	                                    <label class="fl">版本：<?php echo $value['version'];?><i class="icon icon_dot"></i></label>
	                                    <div class="detail fl">
	                                        <p><span>大小：</span><?php echo $value['size'];?>(核心库)</p>
	                                        <p><span>更新时间：</span><?php echo date("Y-m-d",$value['updatetime']);?></p>
	                                        <p><span>md5sum：</span><?php echo $value['md5sum'];?></p>
	                                    </div>
	                                    <div class="news fl">
	                                        <label class="fl">更新动态</label>
	                                        <div class="newsinner fl">
	                                            <p><?php echo $value['content'];?></p>
	                                        </div>
	                                    </div>
	                                </li>
	                                <?php
	                                }?>
	                                 <li class="empty"></li>
                            	</ul>
                             <?php
                                }
                                else{?>
                                	<div class="line"></div>
                                	<label style="margin-top: 100px;margin-left: 200px; margin-bottom: 100px; margin-right: 200px;">
                                		<h1>暂无版本信息</h1>
                                	</label>
                                <?php }?>
                        </div>
                        <div class="bottom"><i class="icon bottom_line"></i></div>
                    </div>
                </div>
            </div>
    </section>
</div>

<?php $this->load->view("manage/inc/footer");?>
<script>
    $(function(){
        $('#apiDetailsList').niceScroll({
            preservenativescrolling:false,
            cursorcolor:"#ccc",
            cursoropacitymax:1,
            touchbehavior:false,
            cursorwidth:"5px",
            cursorborder:"0",
            cursorborderradius:"5px"
        });
    })
</script>

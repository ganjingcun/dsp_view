<!--  主体部分 [ -->
<div class="inner_container" id="innerContainer">
    <?php $this->load->view('manage/inc/left')?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <div class="jumbotron jumbotron_no_shadow add_app_box">
                    <header class="title">
                        <h3><strong  class="square_box"></strong><span>添加应用</span></h3>
                    </header>
                    <div class="add_app_step add_app_step_01"></div>
                    <form id="frm" name="frm" action="/manage/appinfo/doadd" method="post">
                        <div class="wrap">
                            <div class="form-group ostype" id="sys_tab">
                                <input type="hidden" value="2" id="ostypeid" name="ostypeid">
                                <span class="fl form-label"><i class="require_item">*</i>系统类型：</span>
                                <ul class="tab_item fl">
                                    <li class="active" data-tab="add_ios"><a class="icon btn_ios" href="javascript:;void(0);"  data-value="2" title="iOS"><i class="icon"></i></a></li>
                                    <li data-tab="" ><a class="icon btn_android" href="javascript:;void(0);" data-value="1" title="Android"><i class="icon"></i></a></li>
                                </ul>
                                <div class="fl alert alert-small alert-warning" id="ostypeiderr" style="display: none"><i class="icon error_icon"></i><span id="ostypeidmsg">请选择系统类型！</span></div>
                            </div>


                            <div class="form-group tab_box add_ios">
                                <span class="fl form-label"><i class="require_item">*</i>平台类型：</span>
                                <div class="platform fl">
                                    <label class="radio_style"><i class="icon radio_icon"></i><input style="display: none;" type="radio" name="devicetypeid" value="2">iPhone</label>
                                    <label class="radio_style"><i class="icon radio_icon"></i><input style="display: none;" type="radio" name="devicetypeid" value="3">iPad</label>
                                    <label class="radio_style"><i class="icon radio_icon"></i><input style="display: none;" type="radio" name="devicetypeid" value="4">Universal</label>
                                </div>
                                <span class="fl alert alert-small alert-warning" id="devicetypeiderr" style="display: none;"><i class="icon error_icon"></i><span id="devicetypeidmsg">请选择平台类型！</span></span>
                            </div>


                            <div class="form-group">
                                <span class="fl form-label"><i class="require_item">*</i>应用名称：</span>
                                <span class="fl"><input class="form-control form-control-small" type="text" name="appname" id="appname" maxlength="50"></span>
                                <span class="fl alert alert-small alert-warning" id="appnameerr" style="display: none"><i class="icon error_icon"></i><span id="appnamemsg"></span></span>
                            </div>

                            <div class="form-group">
                                <span class="fl form-label"><i class="require_item">*</i>应用包名称：</span>
                                <span class="fl"><input name="packagename" id="packagename" class="form-control form-control-small"  maxlength="200"/></span>
                                <label class="cueinfo fl"> &nbsp;&nbsp;* iOS:填写Bundle identifier中的值  Android：填写AndroidManifest.xml中manifest package的值</label>
                                <span class="fl alert alert-small alert-warning" id="packagenameerr" style="display: none"><i class="icon error_icon"></i><span id="packagenamemsg"></span></span>
                            </div>


                            <div class="form-group">
                                <span class="fl form-label">下载地址：</span>
                                <span class="fl"><input class="form-control form-control-small" type="text" name="appurl" id="appurl"></span>
                                <span class="fl alert alert-small alert-warning" id="appurlerr" style="display: none"><i class="icon error_icon"></i><span id="appurlmsg">请填写正确的下载地址。</span></span>
                            </div>


                            <div class="form-group coinwall">
                                <span class="fl form-label">积分墙：</span>
                                <label class="fl checkbox_style"><i class="icon checkbox_icon"></i><input style="display: none;" name="isintegral" id="isintegral" class="input_text"  type="checkbox" value="1"> 开启积分墙</label>
                                <label class="cueinfo fl"> * 开启积分墙后，才能够给应用添加积分墙广告位。</label>
                            </div>

                            <div class="form-group " id="isintegraldiv" style="display: none;">
                                <span class="fl form-label"><i class="require_item">*</i>兑换比例：</span>
                                <div class="fl isintegral_box">
                                    <span><span style="font-family: 'arial'">&yen;</span> 1  =</span>
                                    <input name="integration" id="integration" type="input" class="form-control form-control-small" value="" size="6" onkeyup="if(!(r.test(this.value))){ alert('请输入正整数！');this.value='';this.focus();return false;}"/>&nbsp;&nbsp;
                                    <input name="unit" id="unit" value="金币" class="form-control form-control-small" type="input" size="6"/>
                                    <span class="alert alert-small alert-warning" id="integrationerr" style="display: none"><i class="icon error_icon"></i><span id="integrationmsg">请填写正确的兑换比例。</span></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <span class="fl form-label"><i class="require_item">*</i>应用分类：</span>
                                <select class="fl select_app" id="apptypeid" name="apptypeid">
                                    <option value="0">请选择</option>
                                    <?php
                                    foreach ($apptype as $k=>$v)
                                    {
                                        ?>
                                        <option value="<?php echo $k?>"><?php echo $v['typename']?></option>
                                    <?php
                                    }
                                    ?>
                                </select>

                                <span class="fl alert alert-small alert-warning" id="apptypeiderr" style="display: none;"><i class="icon error_icon"></i><span id="apptypeidmsg">请选择应用分类。</span></span>
                                <div id="form_way_0" class="check_labels checkboxlist"></div>
                                <?php
                                foreach ($apptype as $k=>$v)
                                {
                                    ?>
                                    <div  id="form_way_<?php echo $k?>" class="check_labels checkboxlist" style="display: none;">
                                        <?php
                                        if(isset($v['childtype']))
                                        {
                                            foreach ($v['childtype'] as $cv)
                                            {
                                                ?>
                                                <span class="track" data-tagid="<?php echo $cv['apptypeid']?>"><i class="icon icon_track"></i><?php echo $cv['typename']?></span>
                                            <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                <?php
                                }
                                ?>
                                <span class="alert alert-small alert-warning" id="appchildtypeiderr" style="display: none; margin-left: 120px;"><i class="icon error_icon"></i><span id="appchildtypeidmsg">请选择应用分类。</span></span>
                            </div>

                            <div class="form-group appdescription">
                                <span class="fl form-label"><i class="require_item">*</i>应用介绍：</span>
                                <span class="fl">
                                    <textarea name="appdescription" id="appdescription" maxlength="500" class="fl form-control form-control-small"></textarea>
                                </span>
                                <span class="fl alert alert-small alert-warning" id="appdescriptionerr" style="display:none;"><i class="icon error_icon"></i><span id="appdescriptionmsg">请选择应用分类。</span></span>
                            </div>
                        </div>

                        <div class="function_btn">
                            <button type="submit" id="njh_add_app" class="btn btn-primary btn-primary-noboder" onclick="return false;">保存并下一步</button>
                            <button type="reset" onclick="confirmCancel();" class="btn btn-default btn-default-noboder">取 消</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
<?php $this->load->view("manage/inc/footer");?>

<script>
var r = /^\+?[0-9][0-9]*$/;

$(document).ready(function(){
    $('.black_mask').show();
    //单选按钮
    $('.radio_style').bind('click',function(e){
        $('.radio_style').find('.icon').removeClass('active');
        $(this).find('.icon').addClass('active');
    });
    //多选按钮
    $('.checkbox_style').bind('click',function(event){
        if(event.target.nodeName=='INPUT'){
            if($('.checkbox_style').find('.icon').hasClass('active')){
                $('.checkbox_style').find('.icon').removeClass('active');
            }else{
                $('.checkbox_style').find('.icon').addClass('active');
            }
        }
    });

    $('#apptypeid').bind("change",function(e){
        $('.checkboxlist').hide();
        $('.checkboxlist .track').removeClass('checked');
        $("#form_way_"+$(this).val()).show();
    })

    $('.checkboxlist').on('click','.track',function(){
        $(this).toggleClass('checked');
    })

	$("#njh_add_app").click(function(){
		var appname = $("#appname").val();
		var packagename = $("#packagename").val();
		var appurl = $("#appurl").val();
		var devicetypeid = $('input:radio[name="devicetypeid"]:checked').val();
		var isintegral = $('input:checkbox[name="isintegral"]:checked').val();
		var ostypeid = $("#ostypeid").val();
		var apptypeid = $("#apptypeid option:selected").val();
		var appdescription = $("#appdescription").val();
		var appchildtypeid = '';
		var integration = $("#integration").val();
		var unit = $("#unit").val();

        $(".checkboxlist .track").each(function(){
            if($(this).hasClass("checked") == true)
            {
                appchildtypeid+=$(this).data('tagid')+",";
            }
        })

	    $("#ostypeiderr").hide();
        $("#devicetypeiderr").hide();
        $("#appnameerr").hide();
        $("#appurlerr").hide();
        $("#apptypeiderr").hide();
        $("#packagenameerr").hide();
        $("#integrationerr").hide();
        $("#uniterr").hide();
	    
		$.post("/manage/appinfo/doadd", 
			{ 
    			appname:appname,
    			packagename:packagename,
    			appurl:appurl,
    			devicetypeid:devicetypeid,
    			ostypeid:ostypeid,
    			apptypeid:apptypeid,
    			appdescription:appdescription,
    			appchildtypeid:appchildtypeid,
    			integration:integration,
    			unit:unit,
    			isintegral:isintegral,
    			r:Math.random()
			}, 
			function(data)
			{
    			if(data && data['status'] == 0){
        			if(data['data'] != '')
        			{
        				$("#"+data['data'][0]).html(data['data'][1]);
            		}
    				$("#"+data['info']).show();
    				return false;
    			}else{
    				location.href = '/manage/appposition/showadd?appid='+data['data'][0];
    			}
			},
			'json');
		return false;
	});
});

$("#sys_tab .tab_item li a").click(function(){
        $('.tab_box').hide();
		$("#ostypeid").val($(this).data("value"));
		$("#apptypeid option").eq(0).attr("selected" ,true);
		$(".checkboxlist .track").removeClass("checked");
		$(".checkboxlist").hide();
});
$("#isintegral").click(function(){
	if($(this).is(":checked"))
	{
        $("#isintegraldiv").show();
    }
	else
	{
		$("#isintegraldiv").hide();
    }
})

    function confirmCancel(){
        art.dialog({
            title:'<i class="icon icon_node"></i>提示',
            lock: true,
            fixed:true,
            ok:function(){
                location.href='/manage/appinfo/showlist';
                return true;
            },
            cancel:true,
            background: '#fff', // 背景色
            opacity: 0.6,	// 透明度
            content: '<div style="text-align: center; font-size: 14px;">您的操作将不会被保存，确定要继续吗？</div>',
            id: 'deleteLightBox',
            okVal:'确认',
            cancelVal:'取消'
        });
    }

</script>

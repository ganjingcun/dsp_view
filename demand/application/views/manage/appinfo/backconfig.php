<!--  主体部分 [ -->
<div class="inner_container" id="innerContainer">
    <?php $this->load->view('manage/inc/left')?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <?php $this->load->view('manage/inc/righttop')?>
                <div class="inner_tab">
                    <ul class="tab_item">
                        <li><a href="/manage/appposition/showinfo/?appid=<?php echo $appid?>">广告位管理</a></li>
                        <li><a href="/manage/appchannel/showinfo/?appid=<?php echo $appid?>">渠道管理</a></li>
                        <li><a href="/manage/apptag/showinfo/?appid=<?php echo $appid?>">标签管理</a></li>
                        <li ><a href="/manage/appinfo/showinfo/?appid=<?php echo $appid; ?>">基本信息</a></li>
                        <li class="active"><a href="javascript:;">积分回调方式</a></li>
                    </ul>
                    <form action="/manage/appinfo/saveconfig" name="frm" method="post">
                        <input type="hidden" name="old_secretKey" id="old_secretKey" value="<?php echo $appcampaignid ?>">
                    <div class="jumbotron jumbotron_no_shadow tab_box base_info" >
                        <div id="editInfo" class="edit_info" style="padding-left: 10px;">
                             
                            <div class="form-group">
                                <span class="fl form-label" style="width: 280px;"><i class="require_item">*</i>积分墙回调方式：</span>
                                <span class="fl">
                                  <label  style="font-weight: lighter;"><?php if($wallrechargetype == 0){ ?> 客户端回调 <?php } else if($wallrechargetype == 1){ ?>服务器端回调 <?php } ?>  
                                 </span>
                            </div>
                             <?php if($wallrechargetype == 1){ ?>
                             <div class="form-group">
                                <span class="fl form-label" style="width: 280px;"><i class="require_item">*</i>服务器端地址：</span>
                                <span class="fl values">
                                   <?php if(!empty($coco_url)) echo $coco_url;?> 
                                 </span>
                            </div>
                             <div class="form-group">
                                <span class="fl form-label" style="width: 280px;">服务器密钥：</span>
                                <span class="fl">
                                     <?php if(!empty($secretKey)) echo $secretKey;?> 
                                 </span>
                            </div>
                            <?php } ?>
                             <div class="btn-group base_info_btn_group" style="margin-left: 220px;">
                                 <a href="/manage/appinfo/editWallConfig/?appid=<?php echo $appid?>" id="baseInfoEdit" class="btn btn-primary btn-primary-noboder"><i class="icon icon_edit_white_con"></i>编&nbsp;&nbsp;&nbsp;&nbsp;辑</a>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
    </section>
</div>
 
<link rel="stylesheet" href="/public/css/index.css" media="all" />
<link rel="stylesheet" href="/public/css/datepicker.css" media="all" />
<link rel="stylesheet" href="/public/css/popup.css" media="all" />
<script type="text/javascript" src="/public/js/jquery.min.js"></script>
<script type="text/javascript" src="/public/js/highcharts.js"></script>
<script type="text/javascript" src="/public/js/chartscolor_2.js"></script>
<script type="text/javascript" src="/public/js/common.js"></script>
 	<div class="bread_crumbs">
       	<span>您目前所在位置：</span>&nbsp;<a href="/"><b class="icon_home"></b></a> > <span>首页</span>
    </div>
</div>
	<!-- 主体内容 -->
	<div class="warp">
		<div class="main_content">
			<!-- 主体内容 -->
			
			<!-- 账户开销信息 -->
			<!--div class="account_spending">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<col width="20%" /><col width="20%" /><col width="20%" /><col width="20%" /><col width="20%" />
					<tr>
						<th class="no_bg"><a href="javascript:;">消费</a></th>
						<th><a href="javascript:;">收入</a></th>
						<th><a href="javascript:;">充值</a></th>
						<th><a href="javascript:;">提现</a></th>
						<th><a href="javascript:;" title="点击查看账户记录">账户余额</a></th>
					</tr>
					<tr class="ar">
						<td class="no_bg">{number_format($usermoney['mgtotalcost'],2,'.',',')}元</td>
						<td>--元</td>
						<td>{number_format($usermoney['offlinemoney'],2,'.',',')}元</td>
						<td>--元</td>
						<td>{number_format($usermoney['mgtotalmoney'],2,'.',',')}元</td>
					</tr>
				</table>
			</div>
			<br />
<br />
<br /-->

			<!-- 消费报表 -->
			<div class="chart_list h" style="display:none">
				<div class="left">
					<h3 class="chart_title"><b class="icon_chartline"></b>最近一个月的<em class="red">消费</em>走势<a class="link" href="/manage/adactivity/datalist">查看广告列表</a></h3>
					<div class="chartbox" id="chart1"></div>
				</div>
				<div class="right">
					<h3 class="chart_title"><b class="icon_chartbar"></b><em class="red">消费</em>分布</h3>
					<div class="chartbox" id="chart2"></div>
				</div>
			</div>
			
			<!-- 蓝色分割线 -->
			<!--div class="dline_blu"></div-->
			
			<!-- 收入报表 -->
			<div class="chart_list">
				<div class="left">
					<h3 class="chart_title"><b class="icon_chartline"></b>最近一个月的<em class="gre">收入</em>走势<a href="/manage/appincomeoview/index" class="link" >查看详情</a></h3>
					<div class="chartbox" id="chart3"></div>
				</div>
				<div class="right">
					<h3 class="chart_title"><b class="icon_chartbar"></b><em class="gre">收入</em>分布</h3>
					<div class="chartbox" id="chart4"></div>
				</div>
			</div>
					
		</div>
	</div>
<?php $this->load->view("manage/inc/footer");?>
<script type="text/javascript">
		
var chart,chart2,chart3,chart4;
$(document).ready(function() {
	chart = new Highcharts.Chart({
		chart: {
			renderTo: 'chart1',//对应的模块ID
			type:'line',
			plotBackgroundColor: null,//图表背景颜色,默认透明
			plotBorderWidth: null,//图表背景边框
			plotShadow: false//阴影，默认2像素
		},
		//title: 标题
		title: {
			text: '消费走势'
		},
		plotOptions:{
			line:{
				marker:{
					enabled: true
				}
			}
		},	
		//tooltip: 浮层内容
		tooltip: {
			formatter: function() {
				return '<b>'+ this.series.name +'</b><br />'+this.x +': &yen;'+ this.y;
			}
		},
		legend: {
			enabled:false,
			layout: 'vertical',
			align: 'left',
			verticalAlign: 'top',
			x: 100,
			y: 50
		},
		series: [{ 	
			name:'消费走势', 
			data:[{foreach item=item from=$datadatecost}{number_format($item,2,'.','')},{/foreach}],
			//showInLegend:false,//显示/隐藏 线条开关
			marker: { symbol:'circle' } //节点样式 "circle"圆点, "square"方, "diamond"钻型, "triangle"三角 and "triangle-down"向下的三角
			}]
	});
	
chart2 = new Highcharts.Chart({
	chart: {
			renderTo: 'chart2',//对应的模块ID
			plotBackgroundColor: null,//图表背景颜色,默认透明
			plotBorderWidth: null,//图表背景边框
			plotShadow: false//阴影，默认2像素
		},
		//title: 标题
		title: {
			text: '消费总额{number_format(array_sum($datadevicetype),2,'.',',')}元'
		},
		//tooltip: 浮层内容
		tooltip: {
			formatter: function() {
				return '<b>'+ this.point.name +'</b>: '+ Math.round(this.percentage*100)/100 +' %';
			}
		},
		legend: {
			align:'center',
			x: 0,//坐标偏移量
			y: 14,
			floating: true
		},

		plotOptions: {
			pie: {
				dataLabels: {
					formatter: function() {
						return '<b>'+ this.point.name +'</b>: '+ Math.round(this.percentage*100)/100 +' %';
					}
				}
			}
		},
		series: [{
			type: 'pie',
			name: 'Browser share',
			data: [
			    {foreach from=$datadevicetype item=item key=k}
				['{$k}',{number_format($item,2,'.','')}],
			    {/foreach}
			]
		}]
	});



	chart3 = new Highcharts.Chart({
		chart: {
			renderTo: 'chart3',//对应的模块ID
			type:'line',
			plotBackgroundColor: null,//图表背景颜色,默认透明
			plotBorderWidth: null,//图表背景边框
			plotShadow: false//阴影，默认2像素
		},
		//title: 标题
		title: {
			text: '收入走势'
		},
		plotOptions:{
			line:{
				marker:{
					enabled: true
				}
			}
		},
		//tooltip: 浮层内容
		tooltip: {
			formatter: function() {
			return '<b>'+ this.series.name +'</b><br />'+this.x +':  &yen;'+ this.y;
			}
		},
		legend: {
			enabled:false,
			layout: 'vertical',
			align: 'left',
			verticalAlign: 'top',
			x: 100,
			y: 50
		},
		series: [{$data1}],
		xAxis:{
			categories:{$arr}
		}
	});
	
chart4 = new Highcharts.Chart({
	chart: {
			renderTo: 'chart4',//对应的模块ID
			plotBackgroundColor: null,//图表背景颜色,默认透明
			plotBorderWidth: null,//图表背景边框
			plotShadow: false//阴影，默认2像素
		},
		//title: 标题
		title: {
			text: ''
		},
		//tooltip: 浮层内容
		tooltip: {
			formatter: function() {
				return '<b>'+ this.point.name +'</b>: '+ 	Math.round(this.percentage*100)/100 +' %';
			}
		},
		legend: {
			align:'center',
			x: 0,//坐标偏移量
			y: 14,
			floating: true
		},

		plotOptions: {
			pie: {
				dataLabels: {
					formatter: function() {
						return '<b>'+ this.point.name +'</b>: '+ 	Math.round(this.percentage*100)/100 +' %';
					}
				}
			}
		},
		series: [{
			type: 'pie',
			name: 'Browser share',
			data: {$adevi}
		}]
	});
});
Highcharts.theme = {
	xAxis: {
		categories: [{foreach from=$datadatecost item=item key=k}'{substr($k, -5)}',{/foreach}]
		},
	yAxis: {
		title: {
			text: '金额（元）'
		}
	}
};
	var highchartsOptions = Highcharts.setOptions(Highcharts.theme);
</script>



<!--  主体部分 [ -->
<div class="inner_container" id="innerContainer">
    <?php $this->load->view('manage/inc/left')?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <?php $this->load->view('manage/inc/righttop')?>
                <div class="inner_tab">
                    <ul class="tab_item">
                        <li class="active"><a href="javascript:;">渠道管理</a></li>
                        <li><a href="/manage/appposition/showinfo/?appid=<?php echo $appid?>">广告位管理</a></li>
                        <li><a href="/manage/apptag/showinfo/?appid=<?php echo $appid?>">标签管理</a></li>
                        <li><a href="/manage/appinfo/showinfo/?appid=<?php echo $appid?>">基本信息</a></li>
                         <?php if($isintegral == 1){  ?><li ><a href="/manage/appinfo/backconfig/?appid=<?php echo $appid?>">积分回调方式</a></li><?php } ?>
                    </ul>
                    <div class="jumbotron jumbotron_no_shadow tab_box track_manager">
                        <table class="table table-bordered no-margin-bottom">
                            <thead>
                            <tr>
                                <th>渠道</th>
                                <th>渠道ID(PublisherId)</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if(!empty($channellist))
                            {
                                foreach ($channellist as $v)
                                {
                                    ?>
                                    <tr>
                                        <td><?php echo $v['channelname']?></td>
                                        <td><?php echo $v['publisherID']?></td>
                                    </tr>
                                <?php
                                }
                            }else{
                                echo '<tr class="empty_container">
                                    <td colspan="2"><span class="non_empty"><i class="icon node_icon"></i>点击可创建新的渠道</span>您还没有添加渠道！</td>
                                </tr>';
                            }
                            ?>
                            </tbody>
                        </table>
                        <?php
                        if($usertype == 3)
                        {
                            ?>
                            <br>

                            <div class="ditch_selected jumbotron_no_shadow">
                                <div class="tabmenu">
                                    <?php
                                    $i = 0;
                                    foreach($ownchanneltype as $k=>$v)
                                    {
                                        ?>
                                        <span class="tab <?php if($i == 0){echo 'sel';}?>" ><a href="javascript:void(0);" target-id="#sel_<?php echo $v['channelid']?>"><?php echo $v['channelname']?></a></span>
                                        <?php
                                        $i++;
                                    }
                                    ?>
                                    <span class="tab" ><a href="javascript:void(0);" target-id="#sel_0"><b class="icon_zoom"></b>搜索</a></span>
                                </div>
                                <div>
                                    <?php
                                    $i = 0;
                                    foreach($ownchanneltype as $k=>$v)
                                    {
                                        ?>
                                        <div class="sel_list check_labels" id="sel_<?php echo $v['channelid']?>" style="display:<?php if($i != 0) {echo 'none';}?>">

                                            <?php
                                            if(isset($ownchanneltype[$k]['childlist']))
                                            {
                                                echo '<span class="track selected_all" data-channelid=""><i class="icon icon_track"></i>全部</span>';
                                                foreach($ownchanneltype[$k]['childlist'] as $vc)
                                                {
                                                    ?>
                                                    <span class="track" data-channelid="<?php echo $vc['channelid']?>"><i class="icon icon_track"></i><?php echo $vc['channelname']?></span>
                                                <?php
                                                }
                                            }
                                            else
                                            {
                                                echo "<div class='alert alert-warning'>暂无数据！</div>";
                                            }
                                            ?>
                                        </div>
                                        <?php
                                        $i++;
                                    }
                                    ?>
                                    <div class="sel_list" id="sel_0" style="display:none;" >
                                        <br>
                                        <div style="height: 50px;">
                                            <input type='text' class="fl form-control form-control-small" name="keys" id="keys" style="width:200px; margin-right: 10px;"/>
                                            <input type="button" class="fl btn btn-primary btn-primary-noboder" name="s" id="s" value="搜 索">
                                            <span id="keyserr" style="display:none; margin-left: 10px;" class="fl alert alert-small alert-warning"><i class="icon error_icon"></i><span id="keysmsg">请填写要搜索的渠道名称。</span></span>
                                            <span id="keyemptyerr" class="fl alert alert-small alert-warning" style="display:none; margin-left: 10px;"><i class="icon error_icon"></i><span id="channelidmsg">你搜索的数据不存在！</span></span>
                                        </div>
                                        <div id="keysdiv" class="sel_list check_labels"></div>
                                    </div>
                                </div>
                                <br>
                                <span id="channeliderr" class="alert alert-small alert-warning" style="display:none;"><i class="icon error_icon"></i><span id="channelidmsg">请选择渠道。</span></span>

                            </div>

                            <div>
                                <input type="hidden" id="appid" name="appid" value="<?php echo $appid?>">
                            </div>
                            <div>
                                <button type="button" class="btn btn-primary btn-primary-noboder" id="addPublisherID">生成渠道ID</button>
                            </div>
                        <?php
                        }
                        else
                        {
                            ?>
                            <div class="function_btn"><a href="#" id="addTrackBtn" class="btn btn-primary btn-primary-noboder"><i class="icon add_icon"></i>添加渠道</a></div>
                            <br>
                            <div id="newTracks" style="display: none;">
                                <div class="new_tracks jumbotron_no_shadow">
                                    <h3 class="fixed_title">新增渠道</h3>
                                    <div class="add_track">
                                        <input id="channelname" class="fl form-control form-control-small add_input" type="text" placeholder="请填写要添加的渠道名字">
                                    <?php if($ostypeid == 1){?>
                                        <select class="fl form-control form-control-small add_input" id="publisher_select">
                                        	<option value='1'>公共渠道</option>
                                        	<option value='2'>应用商店</option>
                                        </select>
                                        <select id="appstore_show" class="fl form-control form-control-small add_input" style="display:none;">
                                        <?php if(isset($marketlist) && !empty($marketlist)){
                                        		foreach ($marketlist as $market){?>
                                        			<option value="<?php echo $market['marketid']?>"><?php echo $market['marketname']?></option>
                                        	<?php }
                                        		}
                                        		else{?>
                                        		<option value="0">暂无应用商店</option>
                                        		<?php }?>
                                        </select>
                                    <?php }?>
                                       
                                        <input type="hidden" id="appid" name="appid" value="<?php echo $appid?>">
                                        <a href="javascript:;" id="addchannel" class="fl btn btn-success btn-success-noboder">添加</a>
                                        <span id="channelnameerr" class="fl alert alert-small alert-warning" style="display: none;"><i class="fl icon error_icon"></i><span id="channelnamemsg"></span></span>
                                    </div>

                                    <?php
                                    if(!empty($channeltype))
                                    {
                                        echo '<div id="addedTracks" class="added_tracks check_labels">';
                                        foreach ($channeltype as $k => $v)
                                        {?>
                                            <span class="track" data-channelid="<?php echo $k?>"><i class="icon icon_track"></i><?php echo $v['channelname']?></span>
                                        <?php
                                        }
                                    }
                                    else
                                    {
                                        echo '<div id="addedTracks" class="added_tracks check_labels" style="display: none">';
                                    }
                                    echo '</div>';
                                    ?>
                                    <div id="channeliderr" class="alert alert-small alert-warning in_block"  style="display: none;"><i class="icon error_icon"></i><span id="channelidmsg">请选择渠道。</span></div>
                                </div>
                                <div class="function_btn">
                                    <a href="#" id="addPublisherID" class="btn btn-primary btn-primary-noboder">生成Publisher ID</a>
                                    <a href="#" id="addPublisherIDCancel" class="btn btn-default btn-default-noboder">取消</a>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
    </section>
</div>

<!-- ] 主体部分 -->
<?php $this->load->view("manage/inc/footer");?>
<script>
var ditchSelected = (function(){
    var tabmenus = {};
    function getSelectedItems(){
        return $('.ditch_selected .sel_list input:checked');
    };

    function setSelected(data){
        $('.ditch_selected .sel_list').each(function(index, el) {
            for (var i in data[index]) {
                $(this).find('.item:eq('+data[index][i]+')').each(function(index, el) {
                    $(this).click();
                });
            };
        });
    };

    function selectAll(list){
        list.find('.track').addClass('checked');
    };

    function reselectArr(list){
        list.find('.track').removeClass('checked');
    }

    function itemAddClick(arrObj){
        arrObj.click(function(event) {
            itemOnClick($(this));
            return false
        });
    };

    function itemOnClick(_this){
        //var tid = _this.find("input");
        if(_this.hasClass('checked')){
            _this.removeClass('checked');
          //  tid.removeAttr('checked');
            if(_this.hasClass('selected_all')){ reselectArr(_this.parent()); };
        }else{
            _this.addClass('checked');
            //tid.attr('checked',true);
            if(_this.hasClass('selected_all')){ selectAll(_this.parent()); };
        };

    }

    function init(){
        tabmenus = $(".ditch_selected .tabmenu .tab");

        tabmenus.find("a").click(function(event) {
            var tid = $(this).attr('target-id');
            $(this).parent().addClass('sel').siblings().removeClass('sel')
            $(tid).show().siblings().each(function(index, el) {
                reselectArr($(this));
                $('#channeliderr').hide();
                $(this).hide();
            });
        });
        itemAddClick($('.sel_list .track'))
    };

    return {
        init:init,
        getSelects:getSelectedItems,
        setSelects:setSelected,
        itemAddClick:itemAddClick
    }

})()
$(function(){
    //添加渠道按钮
    $('#addTrackBtn').click(function(){
        $('#newTracks').show();
        $(this).hide();
    });
    $('#addedTracks').on('click','.track',function(){
        $(this).toggleClass('checked');

        if($('#addedTracks .track').hasClass('checked')){
            $("#channeliderr").hide();
        }
    });

    $(document).on('click','.sel_list .track',function(){
        $(this).toggleClass('checked');
        if($('.sel_list .track').hasClass('checked')){
            $("#channeliderr").hide();
        }
    });

    $('#addPublisherIDCancel').click(function(){
        $('#newTracks').hide();
        $('#addTrackBtn').show();
        $('#addedTracks .track').removeClass('checked');
    });
    $("#addPublisherID").click(function(){
//                $('#newTracks').hide();
//                $('#addTrackBtn').show();
        var appid = $("#appid").val();
        var channelid = '';
        $(".track").each(function(){
            if($(this).hasClass("checked") == true )
            {
                channelid+=$(this).data('channelid')+",";
            }
        })
        if(channelid==null || channelid == '')
        {
            $("#channeliderr").show();
            return false;
        }
        $("#channeliderr").hide();

        $.post("/manage/appchannel/doAddChannel",
            {
                appid:appid,
                channelid:channelid,
                r:Math.random()
            },
            function(data)
            {
                if(data && data['status'] == 0){
                    if(data['data'] != '')
                    {
                        $("#"+data['data'][0]).html(data['data'][1]);
                    }
                    $("#"+data['info']).show();
                    return false;
                }else{
                    location.href = '/manage/appchannel/showinfo/?appid='+appid;
                }
            },
            'json');
        return false;
    });

    $(document).on('click','#addchannel',function(){
        var channelName = $('#channelname').val();
        var appid = $("#appid").val();
        var marketid = null;
        if($("#publisher_select").val() == 2){
        	marketid = $("#appstore_show").val();
        }
        $('#channelnameerr').hide();
        $.ajax({
            url: "/manage/appchannel/doadd",
            type: 'POST',
            data:  {
                channelname:channelName,
                appid:appid,
                marketid:marketid,
                r:Math.random()
            },
            dataType: 'json',
            success: function(data)
            {
                if(data && data['status'] == 0){
                    if(data['data'] != '')
                    {
                        $("#"+data['data'][0]).html(data['data'][1]);
                    }
                    $("#"+data['info']).show();
                    return false;
                }else{
                    $('#addedTracks').show();
                    var addedTracksHtml = $('#addedTracks').html();
                    addedTracksHtml = '<span class="track checked" data-channelid="'+data.data+'"><i class="icon icon_track"></i>'+channelName+'</span>'+addedTracksHtml;
                    $('#addedTracks').html(addedTracksHtml)
                }
            }
        });
        return false;
    });

    //搜索渠道
    $("#s").click(function(){
        var keys = $("#keys").val();
        var appid = $("#appid").val();
        if(keys == null || keys == '')
        {
            $("#keyserr").show();
        }
        $("#keyserr").hide();
        $('#keyemptyerr').hide();
        $.post("/manage/appchannel/searchchannel",
            {
                keys:keys,
                appid:appid,
                r:Math.random()
            },
            function(data)
            {
                if(data && data['status'] == 0){
                    if(data['data'] != '')
                    {
                        $("#"+data['data'][0]).html(data['data'][1]);
                    }
                    $("#"+data['info']).show();
                    return false;
                }else if(data && data['status'] == 1){
                    var str = '';
                    var leng = data.data.length;
                    for(var i=0; i<leng;i++){
                        var el = data.data[i];
                        str += "<span class='track' data-channelid='"+el['channelid']+"'><i class='icon icon_track'></i>"+el['channelname']+"</span>";
                    };
                    $('#keysdiv').html(str);
                    ditchSelected.itemAddClick($('#keysdiv .item'));
                }else if(data && data['status'] == 2){
                    $('#keyemptyerr').show();
                }
            },
            'json');
        return false;
    });
    //初始化渠道选择
    ditchSelected.init();

    
    $("#publisher_select").bind('click',function(){
		var selectval = $(this).val();
		if(selectval == 2){
			$("#appstore_show").removeAttr("style");
		}
		else{
			$("#appstore_show").removeProp("style");
			$("#appstore_show").prop("style","display:none");
		}
    });
   
})


</script>
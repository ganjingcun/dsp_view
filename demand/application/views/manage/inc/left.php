<aside style="display:none">
    <div id="leftBar" class="left_bar">
        <div class="add_app">
            <a href="/manage/appinfo/showadd" class="add_app_btn"><i class="icon add_app_icon"></i>添加应用</a>
            <a href="/manage/appinfo/showbatchadd" class="add_app_btn" style="margin-top:20px;"><i class="icon add_app_icon"></i>批量添加应用</a>
        </div>
        <div class="app_title">
            <div class="black_mask"></div>
            <ul class="first">
                <li  class="open <?php if(strpos($_SERVER['REQUEST_URI'], 'showlist')){echo 'active';}?>"><a class="a_status" href="/manage/appinfo/showlist"><i class="icon app_all_icon"></i>所有应用概况</a></li>
                <li  class="open <?php if(!strpos($_SERVER['REQUEST_URI'], 'showlist')){echo 'active';}?>">
                    <a href="javascript:;" class="a_status"><i class="icon app_detail_icon"></i>应用详情<i class="icon icon_open"></i></a>
                    <ul class="second" style="<?php if ((isset($appid) && $appid > 1)){?>display: block;<?php }?> ">
                        <li>
                            <div class="title">
                                <span class="heng">+</span>
                                <span>iOS</span>
                            </div>
                            <ul id="scroll_ios" class="third">
                                <?php
                                if(!empty($leftlist))
                                    foreach ($leftlist as $v)
                                    {
                                        if($v['ostypeid'] == 2)
                                        {
                                            ?>
                                            <li ><a <?php if(isset($appid) && $v['appid'] == $appid){echo "class='on'";}?> href="/manage/appchannel/showinfo/?appid=<?php echo $v['appid']?>"><?php echo $v['appname'];?></a></li>
                                        <?php
                                        }
                                    }
                                ?>
                            </ul>
                        </li>
                        <li>
                            <div class="title">
                                <span class="heng">+</span>
                                <span>Android</span>
                            </div>
                            <ul id="scroll_android" class="third">
                                <?php
                                if(!empty($leftlist))
                                    foreach ($leftlist as $v)
                                    {
                                        if($v['ostypeid'] == 1)
                                        {
                                            ?>
                                            <li><a <?php if(isset($appid) && $v['appid'] == $appid){echo "class='on'";}?> href="/manage/appchannel/showinfo/?appid=<?php echo $v['appid']?>"><?php echo $v['appname'];?></a></li>
                                        <?php
                                        }
                                    }
                                ?>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</aside>
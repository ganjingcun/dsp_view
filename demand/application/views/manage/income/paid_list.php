<!--  主体部分 [ -->
<div class="inner_container" id="innerContainer">
    <?php $this->load->view('manage/income/left')?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <div class="jumbotron jumbotron_no_shadow">
                    <header class="title">
                        <h3><strong class="square_box"></strong><span>提现记录</span></h3>
                    </header>
                    <br>
                    <form name='form1' action="/manage/income/paid_list">
                        <div class="form-group">
                            <div class="fl form-label">按照年份筛选:</div>
                            <div class="fl">
                                <select  name="search_year" id="search_year" style="height:40px;">
                                    <?php foreach($years as $key=>$val){ ?>
                                        <option <?php if($search_year == $val){ ?> selected="selected" <?php } ?>><?php echo $val;?></option>
                                    <?php } ?>
                                </select>
                                <input class="btn btn-primary btn-primary-noboder" type="submit" name="sub" value="搜索">
                            </div>
                        </div>
                    </form>
                    <div class="set_budget">
                        <table class="table table-borderd table-bordered-rlborder">
                            <tbody>
                            <tr>
                                <th>单号</th>
                                <th>提款时间</th>
                                <th>提款金额(元)</th>
                                <th>查看提款单</th>
                                <th>打印提款单</th>
                                <th>状态</th>
                            </tr>
                            <?php if(is_array($income_list)){ foreach ($income_list as $key=>$val) { ?>
                                <tr class="ac">
                                    <td><?php echo $val['incomekey'];?></td>
                                    <td><?php echo date("Y-m-d H:i",$val['addtime']);?></td>
                                    <td><span style="font-family: 'arial'">&yen;</span> <?php echo formatmoney($val['aftertaxmoney']);?></td>
                                    <td><a href="/manage/income/paid_detail?id=<?php echo $val['id'];?>" target="_black">查看</a></td>
                                    <td><a href="/manage/paidprint/paid_detail?id=<?php echo $val['id'];?>" target="_black">打印</a></td>
                                    <td><?php if( $val['status'] == '1'){ ?><b class="gre">完成</b><?php }else{ echo '待付款';} ?></td>
                                </tr>
                            <?php } } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php $this->load->view("manage/inc/footer");?>
	
	<div id="popu_cuebox" class="popu_cuebox">
		<div class="con"></div>
	</div>
	
<script>
//问号 icon 弹出提示框
$(".icon_help").hover(
	function(e){
		var xy = $(this).offset();
		console.log(xy);
		$("#popu_cuebox .con").html($(this).attr("data-title"));
		$("#popu_cuebox").css({
			"top":xy.top,
			"left":xy.left
		});
		$("#popu_cuebox").show();
	},function(){
			$("#popu_cuebox").hide();
});

$("#popu_cuebox").hover(function(){
		$(this).show();
	},function(){
		$(this).hide();
})



$(function(){
	setTimeout(function(){
		$("#side_menu .menu_item .hd a").trigger('click');
	},1000);
})

</script>
 
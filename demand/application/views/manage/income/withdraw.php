<!--  主体部分 [ -->
<div class="inner_container" id="innerContainer">
    <?php $this->load->view('manage/income/left')?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <div class="jumbotron jumbotron_no_shadow">
                    <div>
                        请<b>发票用户</b>将<b>提款单</b>与<b>发票</b>一起邮寄至<b>上海拓畅信息技术有限公司</b><span class="ora">(邮寄地址: 北京市朝阳区望京广顺南大街 悠乐汇A座,507-2, 100102).</span><br/>
                        上海拓畅信息技术有限公司统一打款期为每月25日～31日。如果此期间上海拓畅信息技术有限公司未收到提款单, 打款程序将顺延至下个自然月。
                    </div>
                    <hr>
                    <!-- 上海拓畅信息技术有限公司提款单 -->
                    <header class="title">
                        <h3><strong class="square_box"></strong><span>上海拓畅信息技术有限公司提款单</span></h3>
                    </header>
                    <br>
                    <table class="table table-borderd table-bordered-rlborder">
                        <thead>
                        <tr>
                            <th>单号</th>
                            <th>项目</th>
                            <th>账号</th>
                            <th>提款时间</th>
                            <th>提款金额</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="ac">
                            <td><?php echo $incomekey;?></td>
                            <td>流量收入</td>
                            <td><?php echo $username;?></td>
                            <td><?php echo date("Y-m-d");?></td>
                            <td><span style="font-family: 'arial'">&yen;</span> <?php echo formatmoney($sum_income);?></td>
                        </tr>
                        </tbody>
                    </table><br>

                    <!-- 金额明细  -->
                    <header class="title">
                        <h3><strong class="square_box"></strong><span>金额明细</span></h3>
                    </header>
                    <br>
                    <table class="table table-borderd table-bordered-rlborder">
                        <thead>
                        <tr>
                            <th>月份</th>
                            <th>税前收入(元)</th>
                            <?php if ($accounttype == '1'){ ?>
                                <th> 税负</th>
                                <th>税后金额(元) </th>
                            <?php } ?>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($income_list as $key=>$val){ ?>
                            <tr class="ac">
                                <td><?php echo $val['month'];?></td>
                                <td><span style="font-family: 'arial'">&yen;</span> <?php echo formatmoney($val['cincome']);?></td>
                                <?php if ($accounttype == '1'){ ?>
                                    <td><span style="font-family: 'arial'">&yen;</span> <?php echo formatmoney($val['taxmoney']);?></td>
                                    <td><span style="font-family: 'arial'">&yen;</span> <?php echo formatmoney($val['aftertaxmoney']);?></td>
                                <?php } ?>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table><br>


                    <!-- 收款账户  -->
                    <header class="title">
                        <h3><strong class="square_box"></strong><span>收款账户</span></h3>
                    </header>
                    <br>
                    <div class="common_font" style="margin-left:100px;">
                        <div class="form-group">
                            <?php if($accounttype == '2') { ?>
                                <div class="fl form-label">公司名称:</div>
                                <div class="fl font-con"><?php echo $account_info['companyname'];?></div>
                            <?php }elseif($accounttype == '3') { ?>
                                <div class="fl form-label">个体工商户名称:</div>
                                <div class="fl font-con"><?php echo $account_info['individualname'];?></div>
                            <?php } ?>
                        </div>
                        <div class="form-group">
                            <div class="fl form-label">收款人名:</div>
                            <div class="fl font-con"><?php echo $account_info['bankusername'];?></div>
                        </div>
                        <div class="form-group">
                            <div class="fl form-label">开户银行:</div>
                            <div class="fl font-con"><?php echo $account_info['bank'];?></div>
                        </div>
                        <div class="form-group">
                            <div class="fl form-label">银行账号:</div>
                            <div class="fl font-con"><?php echo $account_info['bankaccount'];?></div>
                        </div>
                    </div><br>

                    <!-- 盖章 -->
                    <header class="title">
                        <h3><strong class="square_box"></strong><span>盖章</span></h3>
                    </header>
                    <br>
                    <div class="alert alert-warning">
                        <span style="color: #f00;"><span class="require_item">* </span>确定后将预扣可提现余额； 您可以在提款记录中查看记录或重新打印</span><br><br>
                        <a id="yesgo"  class="btn btn-primary btn-primary-noboder" href="/manage/income/create_paid" title="确认并打印" target="">确认并打印</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php $this->load->view("manage/inc/footer");?>
	
<script>
$(function(){
	setTimeout(function(){
		$("#side_menu .menu_item .hd a").trigger('click');
	},1000);
})
</script>

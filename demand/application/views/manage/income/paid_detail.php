<!--  主体部分 [ -->
<div class="inner_container" id="innerContainer">
    <?php $this->load->view('manage/income/left')?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <div class="jumbotron jumbotron_no_shadow">
                    <!-- 提款单 -->
                    <div class="tikuandan">
                        <header class="title">
                            <h3><strong class="square_box"></strong><span>上海拓畅信息技术有限公司提款单</span></h3>
                        </header>
                        <br>
                        <div class="set_budget">
                            <table class="table table-borderd table-bordered-rlborder">
                                <thead>
                                    <tr>
                                        <th>单号</th>
                                        <th>项目</th>
                                        <th>账号</th>
                                        <th>提款时间</th>
                                        <th>提款金额</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="ac">
                                        <td><?php echo $paid_info['incomekey'];?></td>
                                        <td>流量收入</td>
                                        <td><?php echo $paid_info['username'];?></td>
                                        <td><?php echo date("Y-m-d H:i",$paid_info['addtime']);?></td>
                                        <td><span style="font-family: 'arial'">&yen;</span> <?php echo formatmoney($paid_info['aftertaxmoney']);?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <br><br>
                        <!--  -->
                        <header class="title">
                            <h3><strong class="square_box"></strong><span>金额明细</span></h3>
                        </header>
                        <br>
                        <div class="set_budget">
                            <table class="table table-borderd table-bordered-rlborder">
                                <thead>
                                    <tr>
                                        <th>月份</th>
                                        <th>税前收入(元)</th>
                                        <?php if ($accounttype == '1'){ ?>
                                            <th> 税负</th>
                                            <th>税后金额(元) </th>
                                        <?php } ?>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach($income_info as $key=>$val){ ?>
                                    <tr class="ac">
                                        <td><?php echo $val['month'];?></td>
                                        <td><span style="font-family: 'arial'">&yen;</span> <?php echo formatmoney($val['cincome']);?></td>
                                        <?php if ($accounttype == '1'){ ?>
                                            <td><span style="font-family: 'arial'">&yen;</span> <?php echo formatmoney($val['taxmoney']);?></td>
                                            <td><span style="font-family: 'arial'">&yen;</span> <?php echo formatmoney($val['aftertaxmoney']);?></td>
                                        <?php } ?>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <br><br>

                        <header class="title">
                            <h3><strong class="square_box"></strong><span>收款账户</span></h3>
                        </header>
                        <div class="common_font">
                            <div class="form-group">
                                <?php if ($accounttype == '1'){ ?>
                                    <div class="fl form-label">真实姓名:</div>
                                    <div class="fl font-con"><?php echo $paid_info['realname'];?></div>
                                <?php }elseif($accounttype == '2') { ?>
                                    <div class="fl form-label">公司名称:</div>
                                    <div class="fl font-con"><?php echo $paid_info['companyname'];?></div>
                                <?php }elseif($accounttype == '3') { ?>
                                    <div class="fl form-label">个体工商户名称:</div>
                                    <div class="fl font-con"><?php echo $paid_info['individualname'];?></div>
                                <?php } ?>
                            </div>
                            <div class="form-group">
                                <div class="fl form-label">收款人名:</div>
                                <div class="fl font-con"><?php echo $paid_info['bankusername'];?></div>
                            </div>
                            <div class="form-group">
                                <div class="fl form-label">开户银行:</div>
                                <div class="fl font-con"><?php echo $paid_info['bank'];?></div>
                            </div>
                            <div class="form-group">
                                <div class="fl form-label">银行账号:</div>
                                <div class="fl font-con"><?php echo $paid_info['bankaccount'];?></div>
                            </div>
                        </div>
                        <br><br><br>


                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php $this->load->view("manage/inc/footer");?>

<script>
$(function(){
	setTimeout(function(){
		$("#side_menu .menu_item .hd a").trigger('click');
	},1000);
})

</script>
 
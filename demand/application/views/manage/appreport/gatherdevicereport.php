<form action="" method="post" name="frm">
    <div class="inner_container" id="innerContainer">
        <?php include "left.php"; ?>
        <section>
            <div id="content" class="content">
                <div class="right_content" id="rightContent">
                    <?php include "gathertop.php";?>
                        <div class="jumbotron jumbotron_no_shadow tab_box">
                            <div class="chart_datanull" style="display:<?php if(isset($tablereport) && !empty($tablereport)){?>none<?php }?>">
                                <p class="ac">没有查询到相关数据，请重新选择时间周期。</p>
                            </div>
                            <!-- 报表 饼图 -->
                            <div id="chart1" style="width: 100%; height: 400px; margin: 0 auto;<?php if(!isset($tablereport) || empty($tablereport)){?>display:none<?php }?>"></div>
                            <!-- 报表 饼图 -->
                            <br>
                            <br>
                            <div class="function_btn" style="float:right; margin-bottom:10px;"><a href="/manage/appreport/exportdevicereport?type=exportreport" id="exportbtn" class="btn btn-primary btn-primary-noboder">下载报表</a></div>
                            <table class="table table-bordered table-bottom30 applist">
                                <thead>
                                <tr class="first_th">
                                    <th></th>
                                    <th>eCPM收入</th>
                                    <th>eCPC收入</th>
                                    <th>eCPA收入</th>
                                    <th>总计</th>
                                    <th>占比</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if(isset($tablereport) && !empty($tablereport))
                                        foreach($tablereport as $k=>$v)
                                        {
                                    ?>
                                    <tr style="text-align: center">
                                        <td><?php echo $k;?></td>
                                        <td><span style="font-family: 'arial'">&yen;</span> <?php echo isset($v[2])?formatmoney($v[2]):'0.00'?></td>
                                        <td><span style="font-family: 'arial'">&yen;</span> <?php echo isset($v[1])?formatmoney($v[1]):'0.00'?></td>
                                        <td><span style="font-family: 'arial'">&yen;</span> <?php echo isset($v[3])?formatmoney($v[3]):'0.00'?></td>
                                        <td><span style="font-family: 'arial'">&yen;</span> <?php echo isset($v['sum'])?formatmoney($v['sum']):'0.00'?></td>
                                        <td><?php echo isset($v['ratio'])?number_format($v['ratio'],2):'0.00'?>%</td>
                                    </tr>
                                    <?php
                                        }
                                    ?>
                                    <tr>
                                        <td colspan="6">
                                            <div class="fr">总收入：<span style="font-family: 'arial'">&yen;</span> <?php echo formatmoney($sumincome);?></div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div><!--  闭合inner_tab -->
                </div>
            </div>
        </section>
    </div>
</form>
<?php $this->load->view("manage/inc/footer");?>
<script type="text/javascript">
<!--
var chart;
$(document).ready(function() {
	chart = new Highcharts.Chart({
        /**
         * chart:
         * @ renderTo 对应的模块ID
         * @ plotBackgroundColor 图表背景颜色,默认透明
         * @ plotBorderWidth 图表背景边框
         * @ plotShadow 阴影，默认2像素
         */
		chart: {
			renderTo: 'chart1',//对应的模块ID
			plotBackgroundColor: null,//图表背景颜色,默认透明
			plotBorderWidth: null,//图表背景边框
			plotShadow: false//阴影，默认2像素
		},
		title: {//title: 标题
			text: '平台类型分布'
		},
		tooltip: {//tooltip: 浮层内容
			formatter: function() {
				return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
			}
		},
		/**
		 * legend: 数据类别框
		 * @ layout 排列方式，默认horizontal横向 vertical竖向
		 * @ verticalAlign 起始位置,默认top 顶部，bottom底部
		 * @ x,y 坐标偏移量
		 * @ backgroundColor 背景颜色
		 * @ borderColor 边框颜色
		 * @ borderWidth 边框厚度
		 */
		legend: {
			align:'center',
			x: 0,//坐标偏移量
			y: 14,
			floating: true
		},
		plotOptions: {
			//plotOptions: 图表类型
			//pie饼图、area区域图、scatter散点图、line曲线图一、spline曲线图二、series横向圆柱图、column圆柱图
			pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,//图表色块描述 开关
					color: '#000000',//图表色块描述 文字颜色
					connectorColor: '#000000',//图表色块描述 线条颜色
					formatter: function() {
						return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
					}
				},
				showInLegend: true
			}
		},
		series: [{
			type: 'pie',
			name: 'Browser share',
			data: [
                <?php
                    $i = 0;
                    if(isset($graphreport))
                    foreach ($graphreport as $k=>$v)
                    {
                        if($k == '0' || empty($k))
                        {
                            continue;
                        }
                        if ($i==0)
                        {
                    ?>
                    {
                        name: '<?php echo $k;?>',
                        y:<?php  echo formatmoney($v,'get',2,'.');?>,
                        sliced: true,//默认选择区域，默认为 false
                        selected: true
                    },
                    <?php
                        }
                        else
                        {
                    ?>['<?php
                            echo $k;?>',<?php echo formatmoney($v,'get',2,'.');?>],
                        <?php
                        }
                    $i++;
                    }
                ?>
			]
		}]
	});

    //周期选择
    $('#inputDate ,.inset_icon').click(function(){
        $('#dateLightBox').show();
        $('#dateLightBox').css('visibility','visible');
    })
    $('#date3').DatePickerClear();
    $('#date3').DatePicker({
        flat: true,
        format:'Y-m-d',
        date: $('#inputDate').val(),
        current: $('#inputDate').val(),
        mode:'range',
        starts: 1,
        calendars: 2,
        onBeforeShow: function(){
            $(this).DatePickerSetDate($('#inputDate').val(), true);
        },
        onChange: function(formated,dates){
            $('#inputDate').val(formated[0]+"~"+formated[1]);
        }
    });

    $("#dateLightBox .right a.time_sel").click(function(e){
        var data = $(this).attr("data");
        if(data){
            var time = SetDate(data);
            $('#inputDate').val(time[0]+"~"+time[1]);
        }else{
            return
        }
    });

    $('#clear_button').click(function(){
        $('#dateLightBox').fadeOut();
    })

    $('#submission').click(function(){
        $('[name=frm]').submit();
    })

    $("table.applist tbody tr").hover(function(e){
        $(this).css("background-color","#F4F5F8");
    },function(e){
        $(this).css("background-color","");
    });
});
//-->
</script>


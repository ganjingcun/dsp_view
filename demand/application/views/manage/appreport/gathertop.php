<div class="income_box">
    <div class="fl jumbotron jumbotron_no_shadow">
        <header class="title">
            <h3><strong  class="square_box"></strong><span>今日收入</span></h3>
        </header>
        <div class="num">
            <span><?php echo empty($commonreport['day'])?0:$commonreport['day'];?></span><em>元</em>
        </div>
    </div>
    <div class="fl jumbotron jumbotron_no_shadow">
        <header class="title">
            <h3><strong  class="square_box"></strong><span>昨日收入</span></h3>
        </header>
        <div class="num">
            <span><?php echo empty($commonreport['yesterday'])?0:$commonreport['yesterday'];?></span><em>元</em>
        </div>
    </div>
    <div class="fl jumbotron jumbotron_no_shadow">
        <header class="title">
            <h3><strong  class="square_box"></strong><span>账户余额</span></h3>
        </header>
        <div class="num">
            <span><?php echo formatmoney($totalmoney); ?></span><em>元</em>
        </div>
    </div>
</div>

<header class="title" style="height: 50px;">
    <h3 class="title_normal">
        <strong  class="square_box"></strong>
        <span>应用数量：<a href="/manage/appinfo/showlist"><?php echo is_array($applist) ? count($applist) : 0;?>个</a></span>
        <!--        <div class="function_btn" style="margin-left:130px; margin-top:-30px; "><a href="/manage/detailreport/appDetailGather?type=exportreport&date=<?php echo $sdate.'~'.$edate; ?>" id="exportbtn" class="btn btn-primary btn-primary-noboder">下载详情报表</a></div>
        <a href="#" class="btn btn-success btn-success-noboder"><i class="icon icon_crash"></i>前往提现</a>-->
    </h3>
</header>

<div class="inner_tab">
    <ul class="tab_item">
        <?php
        /*
        <li class="<?php echo strpos($_SERVER['PHP_SELF'],'gatherreport') > 0 ? 'active' : '';?>" data-tab="general_situation"><a href="/manage/appreport/gatherreport">总体概况</a></li>
        <li class="<?php echo strpos($_SERVER['PHP_SELF'],'gatheradtypereport') > 0 ? 'active' : '';?>" data-tab="ad_form"><a href="/manage/appreport/gatheradtypereport">广告形式分布</a></li>
        <li class="<?php echo strpos($_SERVER['PHP_SELF'],'gatherdevicereport') > 0 ? 'active' : '';?>" data-tab="platform_form"><a href="/manage/appreport/gatherdevicereport">平台类型分布</a></li>
        <li class="<?php echo strpos($_SERVER['PHP_SELF'],'gatherappreport') > 0 ? 'active' : '';?>" data-tab="app_form"><a href="/manage/appreport/gatherappreport">应用分布</a></li>
        */
        ?>
        <div class="inputDate_box fr">
            <span class="inset_icon"><i class="icon icon_date"></i></span>
            <input type="text" class="fr form-control form-control-small" id="inputDate" size="30" name="inputDate" readonly="readonly" value="<?php echo $sdate.'~'.$edate; ?>" style="width:220px;">
        </div>
        <!-------------定义弹出时间层的位置----------------->
        <div id="dateLightBox" class="popup" style="display: none;">
            <i class="icon icon_white_jiao"></i>
            <div class="calendar_box" >
                <div class="fl left">
                    <div id="date3" class="calendar"></div>
                </div>
                <div class="fr right">
                    <h4>预设范围:</h4>
                    <a href="javascript:;" class="time_sel" data="0">今天</a>
                    <a href="javascript:;" class="time_sel" data="-1">昨天</a>
                    <a href="javascript:;" class="time_sel" data="-7">最近7天</a>
                    <a href="javascript:;" class="time_sel" data="-30">最近30天</a>
                    <a href="javascript:" class="time_sel" data="-90">最近90天</a>
                </div>
                <div class="popup_btn">
                    <a href="javascript:;" id="submission" class="btn btn-primary btn-primary-noboder">确定</a>
                    <a href="javascript:;" id="clear_button" class="btn btn-default btn-default-noboder">取消</a>
                </div>
            </div>
        </div>
    </ul>
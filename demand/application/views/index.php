<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>CocosAds</title>
    <link href="/public/version1.0/css/punch.css" rel="stylesheet" media="all">
    <link href="/public/version1.0/css/slideup.css" rel="stylesheet">
    <script type="text/javascript" src="/public/version1.0/js/html5.js"></script>
    <script type="text/javascript" src="/public/version1.0/js/jquery.js"></script>
    <script type="text/javascript" src="public/version1.0/js/slideup.js"></script>
</head>

<body>
    <!-- header [ -->
    <header id="top">
        <section>
            <div class="top_inner">
                <ul class="menu">
<!--                    <li><a href="/public/version1.0/" title="新闻中心">新闻中心</a></li>-->
                    <li><a href="/public/version1.0/customQuestion.html" title="帮助中心">帮助中心</a></li>
                    <li><a href="/public/version1.0/contact.html" title="联系我们">联系我们</a></li>
                    <li><a href="/public/version1.0/about.html" title="关于我们">关于我们</a></li>
                    <li><a href="http://old.punchbox.org" title="前往旧平台">前往旧平台</a></li>
                </ul>
                <div class="top_box clearfix">
                    <a href="/" class="fl logo"><img src="/public/version1.0/images/logo.png" width="129" height="85"></a>
                    <div class="fr fix_btn">
                        <a href="/abdev/index" class="btn btn-lg btn-success"><i class="icon dever_icon"></i>我是开发者</a>
                        <a href="/public/version1.0/ader.html" class="btn btn-lg btn-primary"><i class="icon ader_icon"></i>我是广告主</a>
                    </div>
                </div>
                <div class="login_box">
                    <div class="login_form login_form_suc" style="display: none">
                        <br>
                        <br>
                        <br>
                        <br>
                        <h3>感谢您对CocosAds的信任</h3>
                        <br>
                        <div class="form-group">
                            <span class="user_line">您正在使用的账号是：</span>
                            <br>
                            <span class="userAcc"></span>
                        </div>
                        <br>
                        <div class="form-group">
                            <a href="/manage/appinfo/showlist" class="long-btn long-btn-primary">开始使用</a>
                        </div>
                        <div class="form-group"><a href="/webindex/logout" class="quit">退出</a></div>
                    </div>
                    <?php
                        if($username){
                            ?>
                            <div class="login_form login_form_suc">
                                <br>
                                <br>
                                <br>
                                <br>
                                <h3>感谢您对CocosAds的信任</h3>
                                <br>
                                <div class="form-group">
                                    <span class="user_line">您正在使用的账号是：</span>
                                    <br>
                                    <span class="userAcc"><?php echo $username ?></span>
                                </div>
                                <br>
                                <div class="form-group">
                                    <a href="/manage/appinfo/showlist" class="long-btn long-btn-primary">开始使用</a>
                                </div>
                                <div class="form-group"><a href="/webindex/logout" class="quit">退出</a></div>
                            </div>
                    <?php
                        }else{
                    ?>
                            <div class="login_form login_form_none">
                                <h3>登录</h3>
                                <div class="coco_login">
                                    <a href="<?php echo $this->config->item('openurl')?>/login_oauth/login_action?client_id=<?php echo $client_id;?>&redirect_url=<?php echo $redirect_url;?>&sign=<?php echo $sign;?>">cocoachina账号登录</a>
                                </div>
                                <form method="post" action="/webindex/dologin" id="loginForm">
                                    <div class="form-group">
                                        <div class="user_name add_bg">
                                            <label class="fl"><i class="icon user_name_icon"></i></label>
                                            <input class="fl text_input" id="userEmail" size="50" name="username" type="text" value="<?php if($autologin){echo $autousername;}?>" placeholder="邮箱">
                                            <span class="red"><?php echo !empty($passwordmsg) ? $passwordmsg : '' ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="pass_word add_bg">
                                            <label class="fl"><i class="fl icon pass_word_icon"></i></label>
                                            <input class="fl text_input" id="pwd" type="password" value="<?php if($autologin){echo $autopwd;}?>" size="50" name="password" placeholder="密码">
                                            <span class="red"><?php echo !empty($passwordmsg) ? $passwordmsg : '' ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="ident_code fl"><input class="text_input" type="text" id="captcha" name='captcha' placeholder="验证码"></div>
                                        <div class="fl ident_code_fix">
                                            <img src="/captcha/index/80/40/4/<?php echo rand();?>" id='captchaimage' width="80" height="40" id='captchaimage' />
                                            <label><a href="javascript:document.getElementById('captchaimage').src='/captcha/index/80/40/4/'+Math.random();document.getElementById('captcha').focus();">看不清<br>换一张</a></label>
                                        </div>
                                        <span class="red"><?php echo !empty($captchamsg) ? $captchamsg : '' ?></span>
                                    </div>
                                    <div class="form-group">
                                        <!--[if !IE]> -->
<!--                                        <span class="remember_me fl"><input id="login_check" type="checkbox"><label class="login_check" for="login_check"><em>记住我</em></label></span>-->
                                        <!-- <![endif]-->
                                        <!--[if IE]>
                                        <!--<span class=""><input id="login_check" type="checkbox"><label class="login_check" for="login_check">记住我</label></span>-->
                                        <![endif]-->
                                        <div class="errorTitle" id="errorTitle" style="display: none;"></div>
                                    </div>
                                    <div class="form-group">
                                        <input type="button" id="submitLoginBtn" class="long-btn long-btn-warning" value="登&nbsp;&nbsp;&nbsp;&nbsp;录">
                                    </div>
                                </form>
                                <div class="form-group">
                                    <label class="fl" style="position: relative; font-weight:normal; font-size:14px; color:#fff;">
                                        <input type="checkbox" id="autologin" name="autologin" <?php if($autologin){echo 'checked';}?> style="position: relative; top:2px;" value="1" onclick="chkitem();" > 两周内免登录
                                    </label>
                                    <a class="miss_pwd fr"  href="webindex/forgotpassword">忘记密码？</a>
                                    <a class="register_link fr" style="margin-right: 10px;" href="webindex/showregister">注册</a>
                                </div>
                            </div>
                    <?php
                        }
                    ?>

                </div>
            </div>
        </section>
    </header>
    <!-- ] header -->

    <!-- banner  [-->
    <div id="banner">
        <div id="slides">
            <div class="slides_container">
<!--                <div class="inner"> 删除双蛋活动图片-->
<!--                    <img src="/public/version1.0/images/banner07.jpg" border="0" alt="#" >-->
<!--                    <div class="caption" style="display:none">-->
<!--                        <p><img src="/public/version1.0/images/relative_font7.png" border="0"></p><br>-->
<!--                        <p style="text-align: left;"><a href="http://lnk8.cn/ZV1IB5" target="_blank"><img src="/public/version1.0/images/relative_font6_btn.png" border="0"></a></p>-->
<!--                    </div>-->
<!--                </div>-->
<!--				<div class="inner">删除五一活动图片-->
<!--                    <img src="/public/version1.0/images/banner08.jpg" border="0" alt="#" width="1000" height="350">-->
<!--                    <div class="caption">-->
<!--                        <p><img src="/public/version1.0/images/relative_font8.png" border="0"></p>-->
<!--                        <p style="text-align: left;margin-top:10px;"><a href="/public/upload/stuff/external/html5/20150430/laborday/laborday.html"  target="_blank"><img src="/public/version1.0/images/relative_font6_btn.png" border="0"></a></p>-->
<!--                    </div>-->
<!--                </div>-->
                <div class="inner">
                    <img src="/public/version1.0/images/banner06.jpg" border="0" alt="#" width="1000" height="350">
                    <div class="caption">
                        <p><img src="/public/version1.0/images/relative_font6.png" border="0"></p>
                        <p style="text-align: left;"><a href="https://www.chartboost.com/en/signup?lead_source=chukong" target="_blank"><img src="/public/version1.0/images/relative_font6_btn.png" border="0"></a></p>
                    </div>
                </div>
                <div class="inner">
                    <img src="/public/version1.0/images/banner01.jpg" border="0" alt="#" width="1000" height="350">
                    <div class="caption" style="bottom:0">
                        <p><img src="/public/version1.0/images/relative_font1.png" border="0"></p>
                    </div>
                </div>
                <div class="inner">
                    <img src="/public/version1.0/images/banner02.jpg" border="0" alt="#" width="1000" height="350">
                    <div class="caption">
                        <p><img src="/public/version1.0/images/relative_font2.png" border="0"></p>
                    </div>
                </div>
                <div class="inner">
                    <img src="/public/version1.0/images/banner03.jpg" border="0" alt="#" width="1000" height="350">
                    <div class="caption">
                        <p><img src="/public/version1.0/images/relative_font3.png" border="0"></p>
                    </div>
                </div>

            </div>
            <a href="javascript:;" class="prev"></a>
            <a href="javascript:;" class="next"></a>
        </div>
    </div>
    <!-- ] banner -->

    <!--  主体部分 [ -->
    <div class="container_wrap">
        <div class="announce_classify_wrap">
            <div class="container">
                <!-- 公告 [ -->
                <div class="announce">
                    <div class="fl announce_icon_wrap"><i class="icon announce_icon"></i></div>
					<div style="margin-top:30px;">
                    	  <ul class="line">
                    	  <?php 
                    		if(!empty($noticeboard)&&$noticeboard!=false){ 
                    			foreach ($noticeboard as $item){
                    			?>
		                            	<li style="margin-top: 0px; ">
		                             		<i class="icon dot"></i>
		                             		<span style="color:#666;font-size:13px;" title="<?php echo $item['title']?>">
		                             			<?php echo $item['title']?>
		                             		</span>
										</li>
	                               <?php 
			                        }
	                    	}
                    		?>
						    
						  </ul>
                    	
                    </div>
                </div>
                <!--  ] 公告  -->

                <!-- 分类 [ -->
                <div class="classify">
                    <div class="fl white_box">
                        <div class="white_box_inner">
                            <div class="fl title">
                                <i class="icon classify_icon_dev"></i>
                                <h3>开发者</h3>
                            </div>
                            <div class="fl classify_content">
                                <div class="list_detail">
                                    <span>广告嵌入流程最简单</span>
                                    <span>结算单价高，付款速度快</span>
                                    <span>保证开发者稳定收入</span>
                                    <span>广告展现形式丰富</span>
                                    <span>99%超高填充率</span>
                                </div>
                                <div>
                                    <a href="/abdev/index" class="open_classify" title="进入">我要 '嵌' 广告</a>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="fr white_box">
                        <div class="white_box_inner">
                            <div class="fl title">
                                <i class="icon classify_icon_ad"></i>
                                <h3>广告主</h3>
                            </div>
                            <div class="fl classify_content">
                                <div class="list_detail">
                                    <span>优质流量，精准定向</span>
                                    <span>最佳的投放效果</span>
                                    <span>灵活丰富的广告展现形式</span>
                                    <span>创新的点击效果</span>
                                    <span>公开权威的实时监测数据</span>
                                </div>
                                <div>
                                    <a href="/public/version1.0/ader.html" class="open_classify" title="进入">我要 '投' 广告</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ] 分类 -->
            </div>
        </div>

        <div class="container">
            <!-- 成功案例 [ -->
            <div class="suc_examples">
                <div class="suc_examples_title"><div class="big_icon_title suc_examples_icon_title"></div></div>
                <div class="tab">
                    <ul class="tab_item">
                        <li class="active" data-tab="devloper">开发者</li>
                        <li data-tab="Ader">广告主</li>
                    </ul>
                </div>
                <div class="tab_box devloper">
                    <ul>
                        <li>
                            <a href="javascript:;" title="PPTV">
                                <img src="/public/version1.0/images/ader_pro1.jpg" height="180">
                                <div class="title_bg">
                                    <span class="title">PPTV</span>
                                    <div class="bg"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" title="捕鱼达人2">
                                <img src="/public/version1.0/images/ader_pro2.jpg" height="180">
                                <div class="title_bg">
                                    <span class="title">捕鱼达人2</span>
                                    <div class="bg"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" title="书旗小说">
                                <img src="/public/version1.0/images/ader_pro3.jpg" height="180">
                                <div class="title_bg">
                                    <span class="title">书旗小说</span>
                                    <div class="bg"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" title="鳄鱼小顽皮爱洗澡">
                                <img src="/public/version1.0/images/ader_pro4.jpg" height="180">
                                <div class="title_bg">
                                    <span class="title">鳄鱼小顽皮爱洗澡</span>
                                    <div class="bg"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" title="优酷">
                                <img src="/public/version1.0/images/ader_pro5.jpg" height="180">
                                <div class="title_bg">
                                    <span class="title">优酷</span>
                                    <div class="bg"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" title="找你妹">
                                <img src="/public/version1.0/images/ader_pro6.jpg" height="180">
                                <div class="title_bg">
                                    <span class="title">找你妹</span>
                                    <div class="bg"></div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="tab_box Ader" style="display: none;">
                    <ul>
                        <li>
                            <a href="javascript:;" title="时空猎人">
                                <img src="/public/version1.0/images/dev_pro1.jpg" height="180">
                                <div class="title_bg">
                                    <span class="title">时空猎人</span>
                                    <div class="bg"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" title="京东商城">
                                <img src="/public/version1.0/images/dev_pro2.jpg" height="180">
                                <div class="title_bg">
                                    <span class="title">京东商城</span>
                                    <div class="bg"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" title="我叫MT">
                                <img src="/public/version1.0/images/dev_pro3.jpg" height="180">
                                <div class="title_bg">
                                    <span class="title">我叫MT</span>
                                    <div class="bg"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" title="乐元素">
                                <img src="/public/version1.0/images/dev_pro4.jpg" height="180">
                                <div class="title_bg">
                                    <span class="title">乐元素</span>
                                    <div class="bg"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" title="别克">
                                <img src="/public/version1.0/images/dev_pro5.jpg" height="180">
                                <div class="title_bg">
                                    <span class="title">别克</span>
                                    <div class="bg"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" title="秦时明月">
                                <img src="/public/version1.0/images/dev_pro6.jpg" height="180">
                                <div class="title_bg">
                                    <span class="title">秦时明月</span>
                                    <div class="bg"></div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- ] 成功案例 -->
        </div>
    </div>
    <!-- ] 主体部分 -->


    <footer id="footer">
        <div class="bottom_menu">
            <div  class="inner">
                <ul class="fl">
                    <!--<li><a href="#">新闻中心</a> </li>-->
                    <li><a href="/public/version1.0/customQuestion.html">帮助中心</a> </li>
                    <li><a href="/public/version1.0/contact.html">联系我们</a> </li>
                    <li><a href="/public/version1.0/about.html">关于我们</a> </li>
                </ul>
                <a href="/"  class="fr bot_logo"><img src="/public/version1.0/images/logo.png" width="129" height="85"></a>
            </div>
        </div>

        <div class="bottom_other">
            <div class="inner">
                <dl class="fl">
					<dt>开发者合作</dt>
                    <dd>邮箱：chancebd@chance-ad.com</dd>
                    <dd>QQ：1927018418</dd>
                    <dd>电话：15811281483</dd>
                </dl>
                <dl class="fl">
                    <dt>广告投放合作</dt>
                    <dd>邮箱：chenjuan@chance-ad.com</dd>
                    <dd>QQ：39203740</dd>
                    <dd>电话：18601399799</dd>
                </dl>
                <dl class="fl">
                    <dt>SDK 接入技术支持</dt>
                    <dd>邮箱：mengxia@chance-ad.com</dd>
                    <dd>QQ：2241567135</dd>
                    <dd>电话：13552596924</dd>
                </dl>
            </div>
            <div class="inner links">
                <dl class="fl">
                    <dt>友情链接</dt>
                    <dd>
                        <a href="http://tools.appying.com/" target="_blank">iOS积分墙风向标</a>
                    </dd>
                </dl>
            </div>
        </div>

        <div class="copy">
            <div class="inner">
                &copy;2012-2013 cocounion.com 京ICP备 11006519 号
            </div>
        </div>
    </footer>


    <script type="text/javascript" src="/public/version1.0/js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="/public/version1.0/js/jquery.slides.js"></script>
    <script type="text/javascript" src="/public/version1.0/js/util.js"></script>
    <script type="text/javascript" src="/public/version1.0/js/jquery.lightbox.js"></script>
    <script type="text/javascript" src="/public/version1.0/js/jquery.lavalamp.js"></script>
    <script type="text/javascript" src="/public/version1.0/js/jquery.imgscale.js"></script>
    <script type="text/javascript" src="/public/version1.0/js/punch.js"></script>
    <script>
        function checkLogin(){
            //设置验证合作必填
            var userEmail = $('#userEmail').val();
            var pwd = $('#pwd').val();
            var captcha = $('#captcha').val();
            var autologin = $('input:checkbox[name="autologin"]:checked').val();
            var error_val = '';
            if(userEmail == ''){
                error_val = '用户邮箱不能为空';
                $('#errorTitle').html('<img src="/public/version1.0/images/error_title.png">'+error_val);
                $('#errorTitle').show();
                return false;
            }
            if(pwd == ''){
                error_val = '密码不能为空';
                $('#errorTitle').html('<img src="/public/version1.0/images/error_title.png">'+error_val);
                $('#errorTitle').show();
                return false;
            }
            if(captcha == ''){
                error_val = '验证码不能为空';
                $('#errorTitle').html('<img src="/public/version1.0/images/error_title.png">'+error_val);
                $('#errorTitle').show();
                return false;
            }
            $.post('/webindex/dologin',{'username':userEmail,'password':pwd,'captcha':captcha, 'autologin':autologin},function(data){
                var json_data = JSON.parse(data);

                if(json_data.status){
                    $('.login_form_none').hide();
                    $('.login_form_suc').show();
                    if(json_data.info){
                        $('.userAcc').html(json_data.info);
                    }
                }else{
                    error_val = json_data.data;
                    $('#errorTitle').html('<img src="/public/version1.0/images/error_title.png">'+error_val);
                    $('#errorTitle').show();
                    return;
                }
            });
            return false;
            $('#errorTitle').hide();
        }
        $(function(){
        	$(".line").slideUp();	
            $('#submitLoginBtn').bind('click',function(){
                checkLogin();
            })
            $('#loginForm input.text_input').bind('keyup',function(e){
                if(e.keyCode!=13){
                    return;
                }
                checkLogin();
            })
        })
        function chkitem()
        {
        	var autologin = $('input:checkbox[name="autologin"]:checked').val();
            	$.ajax({url: 'webindex/cancelautologin', type: 'GET', dataType: 'json',
                    data: {r:Math.random()},
                    success:function(data){
                        if(data.data == 1)
                        {
                            if(autologin != 1)
                            {
                        		$("#pwd").val('');
                            }
                        }
                    }
                });
            }
    </script>
</body>
</html>
<!-- 
<div id="img1" style="Z-INDEX: 9999; LEFT: 2px; WIDTH: 231px; POSITION: absolute; TOP: 43px; HEIGHT: 115px;
 visibility: visible;">
    <img src="/public/version1.0/images/pf.png" width="231" height="115" border="0" usemap="#Map">
    <map name="Map" id="Map">
        <area shape="rect" coords="206,0,231,25" href="javascript:void($('#img1').remove())">
    </map>
</div>
 -->

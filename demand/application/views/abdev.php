<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>CocosAds</title>
    <link href="/public/version1.0/css/punch.css" rel="stylesheet" media="all">
    <script type="text/javascript" src="/public/version1.0/js/html5.js"></script>
</head>
<body>
    <!-- header [ -->
    <header id="top">
        <section>
            <div class="top_inner">
                <ul class="menu">
                    <!--                    <li><a href="/public/version1.0/" title="新闻中心">新闻中心</a></li>-->
                    <li><a href="/public/version1.0/customQuestion.html" title="帮助中心">帮助中心</a></li>
                    <li><a href="/public/version1.0/contact.html" title="联系我们">联系我们</a></li>
                    <li><a href="/public/version1.0/about.html" title="关于我们">关于我们</a></li>
                    <li><a href="http://old.punchbox.org" title="前往旧平台">前往旧平台</a></li>
                </ul>
                <div class="top_box clearfix">
                    <a href="/" class="fl logo"><img src="/public/version1.0/images/logo.png" width="129" height="85"></a>
                    <div class="fr fix_btn">
                        <a href="" class="btn btn-lg btn-success"><i class="icon dever_icon"></i>我是开发者</a>
                        <a href="/public/version1.0/ader.html" class="btn btn-lg btn-primary"><i class="icon ader_icon"></i>我是广告主</a>
                    </div>
                </div>
            </div>
        </section>
    </header>
    <!-- ] header -->

    <!-- banner  [-->
    <div id="banner">
        <div id="slides">
            <div class="slides_container">
                <div class="inner">
                    <img src="/public/version1.0/images/banner05.jpg" border="0" alt="#" width="1000" height="350">
                    <div class="caption" style="bottom:0">
                        <p><img src="/public/version1.0/images/relative_font5.png" border="0"></p>
                        <br>
                        <a href="/manage/appinfo/showlist" class="long-btn long-btn-primary">开始使用</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ] banner -->

    <div class="container_wrap">
        <div class="container_wrap_inner">
            <div class="container">
                <div class="cross_center_wrap">
                    <!-- 公告 [ -->
                    <div class="one_col_novice_help novice_help">
                        <div class="novice_help_title"><div class="big_icon_title novice_help_icon_title"></div></div>
                        <div class="">
                            <ul class="novice_help_list">
                                <li><i class="icon dot"></i><a href="/public/version1.0/customQuestionDev.html#1" title="">CocosAds开发者如何盈利？</a></li>
                                <li><i class="icon dot"></i><a href="/public/version1.0/customQuestionDev.html#3" title="">开发者平台使用的流程有哪些？</a></li>
                                <li><i class="icon dot"></i><a href="/public/version1.0/customQuestionDev.html#9" title="">如何提高我的广告收益？</a></li>
                            </ul>
                        </div>
                    </div>
                    <!--  ] 公告  -->
                    <div class="sdk">
                        <div class="sdk_title"><div class="big_icon_title sdk_title_icon"></div></div>
                        <div class="sdk_wrap">
                            <div class=" sdk_box android_sdk_box">
                                <div class="front">
                                    <div class="pic_content">
                                        <i class="icon adroid_icon"></i>
                                    </div>
                                </div>
                                <div class="btn_list" style="margin-top: 70px;">
                                    <a href="<?php echo $androidsdkinfo['packpath']?>" target="_blank" class="small-btn small-btn-primary"><i class="icon download_icon"></i>下载</a>
<!--                                    <a href="" target="_blank" class="small-btn small-btn-white"><i class="icon doc_icon"></i>文档</a>-->
                                </div>
                                <div class="details">
                                    <span>SDK 版本：<?php echo $androidsdkinfo['version']?></span>
                                    <span>大小：<?php echo $androidsdkinfo['size']?></span>
                                    <span>更新日期：<?php echo date("Y-m-d",$androidsdkinfo['updatetime'])?></span>
                                </div>
                            </div>
                            <div class=" sdk_box ios_sdk_box">
                                <div class="front">
                                    <div class="pic_content">
                                        <i class="icon ios_icon"></i>
                                    </div>
                                </div>
                                <div class="btn_list" style="margin-top: 70px;">
                                    <a href="<?php echo $iossdkinfo['packpath']?>" target="_blank" class="small-btn small-btn-primary"><i class="icon download_icon"></i>下载</a>
<!--                                    <a href="#" class="small-btn small-btn-white"><i class="icon doc_icon"></i>文档</a>-->
                                </div>
                                <div class="details">
                                    <span>SDK 版本：<?php echo $iossdkinfo['version']?></span>
                                    <span>大小：<?php echo $iossdkinfo['size']?></span>
                                    <span>更新日期：<?php echo date("Y-m-d",$iossdkinfo['updatetime'])?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--  主体部分 [ -->
    <div class="container">

        <!-- 成功案例 [ -->
        <div class="suc_examples">
            <div class="suc_examples_title"><div class="big_icon_title suc_examples_icon_title"></div></div>
            <div class="tab">
                <ul class="tab_item">
                    <li class="active" data-tab="devloper"></li>
                </ul>
            </div>
            <div class="tab_box devloper">
                <ul>
                    <li>
                        <a href="javascript:;" title="PPTV">
                            <img src="/public/version1.0/images/ader_pro1.jpg" height="180">
                            <div class="title_bg">
                                <span class="title">PPTV</span>
                                <div class="bg"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" title="捕鱼达人2">
                            <img src="/public/version1.0/images/ader_pro2.jpg" height="180">
                            <div class="title_bg">
                                <span class="title">捕鱼达人2</span>
                                <div class="bg"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" title="书旗小说">
                            <img src="/public/version1.0/images/ader_pro3.jpg" height="180">
                            <div class="title_bg">
                                <span class="title">书旗小说</span>
                                <div class="bg"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" title="鳄鱼小顽皮爱洗澡">
                            <img src="/public/version1.0/images/ader_pro4.jpg" height="180">
                            <div class="title_bg">
                                <span class="title">鳄鱼小顽皮爱洗澡</span>
                                <div class="bg"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" title="优酷">
                            <img src="/public/version1.0/images/ader_pro5.jpg" height="180">
                            <div class="title_bg">
                                <span class="title">优酷</span>
                                <div class="bg"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" title="找你妹">
                            <img src="/public/version1.0/images/ader_pro6.jpg" height="180">
                            <div class="title_bg">
                                <span class="title">找你妹</span>
                                <div class="bg"></div>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- ] 成功案例 -->
    </div>
    <!-- ] 主体部分 -->

    <footer id="footer">
        <div class="bottom_menu">
            <div  class="inner">
                <ul class="fl">
                    <!--<li><a href="#">新闻中心</a> </li>-->
                    <li><a href="/public/version1.0/customQuestion.html">帮助中心</a> </li>
                    <li><a href="/public/version1.0/contact.html">联系我们</a> </li>
                    <li><a href="/public/version1.0/about.html">关于我们</a> </li>

                </ul>
                <a href="/"  class="fr bot_logo"><img src="/public/version1.0/images/logo.png" width="129" height="85"></a>
            </div>
        </div>

        <div class="bottom_other">
            <div class="inner">
                <dl class="fl">
					<dt>开发者合作</dt>
                    <dd>邮箱：chancebd@chance-ad.com</dd>
                    <dd>QQ：1927018418</dd>
                    <dd>电话：15811281483</dd>
                </dl>
                <dl class="fl">
                    <dt>广告投放合作</dt>
                    <dd>邮箱：chenjuan@chance-ad.com</dd>
                    <dd>QQ：39203740</dd>
                    <dd>电话：18601399799</dd>
                </dl>
                <dl class="fl">
                    <dt>SDK 接入技术支持</dt>
                    <dd>邮箱：mengxia@chance-ad.com</dd>
                    <dd>QQ：2241567135</dd>
                    <dd>电话：13552596924</dd>
                </dl>
            </div>
            <div class="inner links">
                <dl class="fl">
                    <dt>友情链接</dt>
                    <dd>
                        <a href="http://tools.appying.com/" target="_blank">iOS积分墙风向标</a>
                    </dd>
                </dl>
            </div>
        </div>

        <div class="copy">
            <div class="inner">
                &copy;2012-2013 cocounion.com 京ICP备 11006519 号
            </div>
        </div>
    </footer>

    <script type="text/javascript" src="/public/version1.0/js/jquery.js"></script>
    <script type="text/javascript" src="/public/version1.0/js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="/public/version1.0/js/jquery.slides.js"></script>
    <script type="text/javascript" src="/public/version1.0/js/util.js"></script>
    <script type="text/javascript" src="/public/version1.0/js/jquery.lightbox.js"></script>
    <script type="text/javascript" src="/public/version1.0/js/jquery.lavalamp.js"></script>
    <script type="text/javascript" src="/public/version1.0/js/jquery.imgscale.js"></script>
    <script type="text/javascript" src="/public/version1.0/js/punch.js"></script>
</body>
</html>

<div id="img1" style="Z-INDEX: 9999; LEFT: 2px; WIDTH: 231px; POSITION: absolute; TOP: 43px; HEIGHT: 115px;display:none;
 visibility: visible;"><img src="/public/version1.0/images/piao.png" width="217px" hight="237" border="0" usemap="#Map">
   <div id="img_del" style="cursor:pointer; position: absolute; width: 20px; height: 20px; right: 44px; top: 44px;" onclick="$('#img1').remove();"></div>
   <div id="img_detail" style="cursor:pointer; position: absolute; width: 150px; height: 40px; right: 45px; top: 200px;" onclick="window.open('/public/ad/');"></div>
</div>

<?php
//数据库数字字段转换成对应中文字符

/**
 * 广告状态转换
 * @param $status int
 * @return string $status_str
 */
function get_ads_status($status = 0)
{
	$str_array = array(
		0 => '未完成',
		1 => '有效',
		2 => '暂停',
		3 => '删除',
	);
	$status_str = $str_array[$status];
    return $status_str;
}

/**
 * 点击后效果数组
 * @return string $status_str
 */
function get_ads_effecttype_array()
{
	$str_array = array(
		1 => '直接下载',
		2 => '二次确认',
		3 => '应用内打开页面',
		4 => '打开浏览器',
		5 => '唯一按钮',
		6 => '一次半确认',
		7 => '播放视频',
		8 => '极速下载',
		20 => '游戏内模块',
		21 => 'HTML页面',
	);
	return $str_array;
}

/**
 * 广告类型数组
 * @return string $status_str
 */
function get_ads_adform_array()
{
	$str_array = array(
		1 => 'Banner广告',
		2 => '弹出广告',
		3 => '精品推荐',
		4 => '广告墙',
		9 => 'kt公告',
		10 => 'kt应用',
		11 => 'ccplay礼包',
		12 => '攻略',
		13 => 'ccplay客服号码',
		14 => 'ccplayFAQ',
		20 => '信息流广告',
		30 => 'DSP广告',
		40 => '视频广告',
	);
	return $str_array;
}

/**
 * 广告类型转换
 * @param $adform int
 * @return string $status_str
 */
function get_ads_adform($adform = 0)
{
	$str_array = get_ads_adform_array();
	$adform_str = $str_array[$adform];
	return $adform_str;
}

/**
 * 推广计划状态转换
 * @param $status int
 * @return string $status_str
 */
function get_campaign_status($status = 0)
{
	$str_array = array(
		1 => '待投放',
		2 => '投放中',
		3 => '已暂停',
		4 => '已中止',
		5 => '已结束',
		6 => '已删除',
	);
	$status_str = $str_array[$status];
	return $status_str;
}

/**
 * 推广目标类型转换
 * @param $targettype int
 * @return string $status_str
 */
function get_campaign_targettype($targettype = 0)
{
	$str_array = array(
		1 => 'App',
		2 => 'Mobile Website',
		3 => 'Cocoplay',
		4 => '视频广告',
		5 => '白鹭',
	);
	$targettype_str = $str_array[$targettype];
	return $targettype_str;
}

/**
 * 推广目标类型转换
 * @param $ostypeid int
 * @return string $status_str
 */
function get_campaign_ostypeid($ostypeid = 0)
{
	$str_array = array(
		0 => '不限',
		1 => 'Android',
		2 => 'iOS',
	);
	$ostypeid_str = $str_array[$ostypeid];
	return $ostypeid_str;
}

/**
 * 广告创意类型数组
 * @return string $status_str
 */
function get_stufftype_array()
{
	$str_array = array(
		1 => 'Banner图片',
		2 => 'Banner图文',
		3 => 'pop图片',
		4 => 'pop动效广告',
		5 => '精品推荐',
		6 => '积分墙',
		7 => 'Banner文字',
	);
	return $str_array;
}

/**
 * 广告创意类型转换
 * @param $stufftype int
 * @return string $status_str
 */
function get_stufftype($stufftype = 0)
{
	$str_array = get_stufftype_array();
	$stufftype_str = $str_array[$stufftype];
	return $stufftype_str;
}

/**
 * 广告创意状态转换
 * @param $status int
 * @return string $status_str
 */
function get_stuffstatus($status = 0)
{
	$str_array = array(
		0 => '审核中',
		1 => '审核通过',
		2 => '审核驳回',
		3 => '广告删除',
	);
	$stufftype_str = $str_array[$status];
	return $stufftype_str;
}
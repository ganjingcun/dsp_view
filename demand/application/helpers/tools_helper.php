<?php
/**
 * 去掉空格
 * @param $data 数组或字符串都可以，
 * @return $data 然会经过处理的数组
 */
function myTrim(&$data)
{
    if(!is_array($data))
    {
        return trim($data);
    }
    else
    {
        foreach($data as $k=>&$v)
        {
            $v = myTrim($v);
        }
    }
    return $data;
}
//CocoaChina登陆验签
function cocoSignData($arr, $secret = '046da2af2bb263b4c82b83b9fbb80533') {
    ksort($arr);
    $res = $secret;
    foreach ($arr as $key => $value) {
        $res.=$key . urlencode($value);
    }
    $res .= $secret;
    return strtoupper(MD5($res));
}
/**
 * 身份类型
 */
function getAccountType($type)
{
    $data = '';
    switch ($type)
    {
        case 1:
            $data = '个人/团体';
            break;
        case 2:
            $data = '公司';
            break;
        case 3:
            $data = '个体工商';
            break;
    }
    return $data;
}
/**
 * 状态
 */
function getAccountStatus($type)
{
    $data = '';
    switch ($type)
    {
        case 0:
            $data = '<span class="label label-warning">审核中</span>';
            break;
        case 1:
            $data = '<span class="label label-success">审核通过</span>';
            break;
        case 2:
            $data = '<span class="label label-danger">驳回</span>';
            break;
    }
    return $data;
}
/**
 * 证件类型
 */
function getCertType($type)
{
    $data = '';
    switch ($type)
    {
        case 1:
            $data = '中国大陆身份证';
            break;
        case 2:
            $data = '中国港澳台身份证';
            break;
        case 3:
            $data = '海外证件';
            break;
    }
    return $data;
}

//时间做key的正排序
function dateKeySort($a, $b)        //用户自定义回调函数
{
    if(strtotime($a)==strtotime($b))         //如果两个参数相等
    {
        return 0;         //返回0
    }
    return(strtotime($a)<strtotime($b))?-1:1;       //如果第1个参数大于第2个返回1，否则-1
}

/**
 * 广告类型
 */
function getAdType($type)
{
    $data = '';
    switch ($type)
    {
        case 1:
            $data = 'Banner';
            break;
        case 2:
            $data = '插屏广告';
            break;
        case 3:
            $data = '推荐墙广告';
            break;
        case 4:
            $data = '开屏广告';
            break;
        case 6:
            $data = '积分墙';
            break;
        case 8://精品推荐为3，原生精品推荐为8.
            $data = '推荐墙广告';
            break;
        case 20:
            $data = '信息流广告';
            break;
        default:
            $data = '';
            break;
    }
    return $data;
}

/**
 * 生成联系24小时时间
 * Enter description here ...
 */
function mkhours()
{
    $hour = array();
    for($ii=0;$ii<24;$ii++)
    {
        if($ii<10)
        $hour[] = "0$ii";
        else
        $hour[] = $ii;
    }
    return $hour;
}

/**
 *
 * Enter description here ...
 * @param $price
 * @param $type
 */
function formatmoney($money, $type='get', $num = 2, $replace = '')
{
    if(!is_numeric($money))
    {
        return $money;
    }
    if($type == 'get')
    {
        if($replace == '')
        {
            $data = number_format($money / 1000000, $num);
        }
        else
        {
            $data = number_format($money / 1000000, $num, $replace,'');
        }
    }
    elseif($type == 'set')
    {
        $data = $money * 1000000;
    }
    return $data;
}
/**
 * 时间生成函数
 * @param $start
 * @param $end
 * @param $needStart 返回的日期是否包括 $start
 * @param $needEnd 返回的日期是否包括 $end
 * @return array
 */
function mktimes($start,$end,$needStart = false,$needEnd = false)
{
    $day = (strtotime($end)-strtotime($start))/86400;
    $y = date('Y',strtotime($start));
    $m = date('m',strtotime($start));
    $d = date('d',strtotime($start));

    $startIndex = $needStart?0:1;
    $day = $needEnd?$day+1:$day;

    $t = array();
    for($i=$startIndex;$i<$day;$i++)
    {
        $t[] = date('Y-m-d', mktime(0,0,0,$m,$d+$i,$y));
    }
    return $t;
}
/**
 * 获取GET方式的数据并去除空格
 * Enter description here ...
 * @param $name
 */
function sget($name){
    if(isset($_GET[$name])){
        if(!is_array($_GET[$name])){
            return trim(strip_tags($_GET[$name]));
        }
        foreach($_GET[$name] as $key=>$val){
            $_GET[$key] = arrayTrim($val);
        }
        return $_GET[$name];
    }
    return FALSE;
}

/**
 * 获取POST方式的数据并去除空格
 * Enter description here ...
 * @param $name
 */
function spost($name){
    if(isset($_POST[$name])){
        if(!is_array($_POST[$name])){
            return trim(strip_tags($_POST[$name]));
        }
        foreach($_POST[$name] as $key=>$val){
            $_POST[$key] = arrayTrim($val);
        }
        return $_POST[$name];
    }
    return FALSE;
}

/**
 * 数组行去除参数两端空格
 * Enter description here ...
 * @param $val
 */
function arrayTrim($val){
    if(!is_array($val)){
        return trim(strip_tags($val));
    }
    foreach($val as $k=>$v){
        $val[$k] = arrayTrim($v);
    }
    return $val;
}

/**
 * 获取url返回值，get方法
 *
 * @param mixed $url 请求地址
 * @param int $timeout 超时时间
 * @param array $header HTTP头信息
 * @access public
 * @return void
 */
function curlGet($url, $timeout = 5, $header = array())
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    curl_setopt($ch, CURLOPT_ENCODING ,'gzip');
    curl_setopt($ch,CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
    $ret = curl_exec($ch);
    curl_close($ch);
    return $ret;
}

/**
 * 获取url返回值，post方法
 *
 * @param mixed $url 请求地址
 * @param int $timeout 超时时间
 * @param array $header HTTP头信息
 * @access public
 * @return void
 */
function curlPost($c_url, $c_url_data, $method='POST', $is_json=false) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $c_url);
    curl_setopt($ch, CURLOPT_USERAGENT,"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method); //设置请求方式
    if ($is_json)
    {
        $data_string = json_encode($c_url_data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    }
    else
    {
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($c_url_data));
    }
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($ch);
    
    $errno = curl_errno( $ch );
 	$info  = curl_getinfo( $ch );
 	$info['errno'] = $errno;
	error_log('info: '.json_encode($info));
	error_log('curl_multi_getcontent: '.curl_multi_getcontent($ch));
    curl_setopt($ch, CURLOPT_POST, 0); //当前面设置set ($ch, curlopt_post, 1)时，用curl_exec  post数据后，需要重新设置CURLOPT_POST为0，否则，会有500或者403错误。
    
    curl_close($ch);
    unset($ch);
    return $result;
}

/**
 * 数据源
 * @param $condition array
 */
function getDataApi($condition=array(),$is_array = true,$is_debug=false)
{
    $config = &get_config();
    $url = $config['data_api'].implode("&",$condition);header("URL:$url");
    if($is_debug)
    return $url;
    if($is_array){
        $tmp = json_decode(curlGet($url,1),true);
        if(!isset($tmp['data'])){
            return array('data'=>array());
        }else{
            return $tmp;
        }
    }else{
        return @curlGet($url,1);
    }
}

/**
 * 模板中数组数据判断
 * @param type $arr 数组
 * @param type $str 一维下吉林省
 * @param type $sub 二维下标
 * @param type $int 返回字符或数字标识
 * @return type
 */
function getReportData($arr, $str, $sub, $int=0){
    return isset($arr[$str][$sub]) ? $arr[$str][$sub] : ($int ? 0 : '');
}

/**
 * 报表数据中比例计算
 * @param type 分子
 * @param type 分母
 * @return string
 */
function getReportRate($var1, $var2){
    if($var2 <= 0){
        return '0%';
    }
    return number_format($var1 / $var2 * 100, 2).'%';
}
/**
 * 添加APP 生成的密钥
 */
function getAppSecret (){
        $secret = md5(time() . mt_rand(1,10000));
        return strtoupper($secret);
}
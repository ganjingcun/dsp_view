<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 验证登陆,后台继承类
 *
 * @brief 验证登陆
 * @author 王栋
 * @date 2012.03.09
 * @note 详细说明及修改日志
 */
class MY_Controller  extends CI_Controller
{
    protected $userInfo;
    var $per_page = 12;		//后台默认分页每页显示条数
    function __construct($isCheckLogin=true)
    {
        parent::__construct();
        $this->load->model('user/AuthClass');
        $this->load->helper('tools');
        $this->load->helper('url');
        $this->load->model("user/UserInfoClass");
        $this->load->library('session');

        if($isCheckLogin==true){
            $this->init();
            $this->top();
        }
        
        if(isset($_SERVER['HTTP_USER_AGENT']) and
        (strpos($_SERVER['HTTP_USER_AGENT'], "MSIE 6.0") or strpos($_SERVER['HTTP_USER_AGENT'], "MSIE 5.5") or strpos($_SERVER['HTTP_USER_AGENT'], "MSIE 4.01")) )
        {
            redirect('manage/error/showie6');
        }
    }


    /**
     * 初始化
     */
    private function init()
    {
        //判断是否登录
        $is_login = $this->check_login();
        if(! $is_login)
        {
            redirect('/webindex');
            return;
        }
        $this->userInfo = AuthClass::getCurrentUser();
        $chkUserInfo = $this->UserInfoClass->getUserInfo($this->userInfo['username']);
        if(empty($chkUserInfo))
        {
            redirect('/webindex');
            return;
        }
        $this->session->set_userdata(array('istype' => $chkUserInfo['accounttype']));
        $this->session->set_userdata(array('isauth' => $chkUserInfo['accountstatus']));
        //强制刷新session;
        $this->userInfo = $this->session->userdata;
        if($chkUserInfo['accountstatus'] >=2 )
        {
            redirect('/manage/indexreg');
            return;
        }
        if($this->session->userdata('isremind') == 0)
        {
            $this->session->set_userdata(array('isremind' => 1));
            redirect('/manage/indexreg/upremind');
        }
    }

    /**
     * 初始化头部
     */
    public function top()
    {
        //		if($this->session->userdata('realname')!='')
        //		{
        //			$this->smarty->assign('userName',$this->session->userdata('realname'));
        //		}
        //		else
        //		{
        //			$this->smarty->assign('userName',$this->session->userdata('username'));
        //		}
        //		$this->smarty->assign('isopen',$this->session->userdata('isopen'));
        //		$this->smarty->assign('userid',$this->session->userdata('id'));
        //		$this->smarty->assign('usertype',$this->session->userdata('usertype'));
    }

    /**
     * 判断是否登录
     */
    protected function check_login()
    {
        return $this->session->userdata('userid');
    }



    /**
     * 显示页面模板，跟$this->load->display（。。）的用法一样，不过这里集成了$this->smarty->assign,
     * 使页面传值更方便
     * @param $tpl
     * @param $param
     * @return unknown_type
     */
    protected function display($tpl,array $param=array() )
    {
        //		if( !empty($param) )
        //		foreach($param as $k=>$v)
        //		{
        //			$this->smarty->assign($k,$v);
        //		}
        //
        //		$this->load->display($tpl);
    }

    //通用头部
    protected function loadHeader($pageTile = '广告活动管理', $slab = 0, $smenu = 0)
    {
        $headinfo['pageTitle'] = $pageTile;
        $headinfo['smenu'] = $smenu;
        $headinfo['slab'] = $slab;
        $headinfo['userinfo'] = $this->userInfo;
        $this->load->view("manage/inc/header", $headinfo);
    }

    /*
     * 提示跳转
     * @param $msg 提示信息
     * @param $location 跳转链接
     */
    function pageMsgBack($msg,$location=0) {
        //		$this->smarty->assign('errormsg',$msg);
        //		$this->smarty->assign('location',$location);
        //		$this->load->display('manage/inc/error.html');
        return;
    }

	public function is_post() {
		return (strtoupper($_SERVER['REQUEST_METHOD']) == "POST") ? TRUE : FALSE;
	}
}
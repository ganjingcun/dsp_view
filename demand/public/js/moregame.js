/*function htmlEncode (str){
	var div = document.createElement("div");
	var text = document.createTextNode(str);
	div.appendChild(text);
	return div.innerHTML;
}
function htmlDecode (str){
	var div = document.createElement("div");
	div.innerHTML = str;
	return div.innerHTML;
}*/
function formatDate(date, format) {
	if (arguments.length < 2 && !date.getTime) {
		format = date;
		date = new Date();
	}else{
		date = new Date(date);
	}
	typeof format != 'string' && (format = 'YYYY年MM月DD日 hh时mm分ss秒');
	var week = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', '日', '一', '二', '三', '四', '五', '六'];
	return format.replace(/YYYY|YY|MM|DD|hh|mm|ss|星期|周|www|week/g, function(a) {
	    switch (a) {
	    case "YYYY":return date.getFullYear();
	    case "YY":return (date.getFullYear()+"").slice(2);
	    case "MM":return date.getMonth() + 1;
	    case "DD":return date.getDate();
	    case "hh":return date.getHours();
	    case "mm":return date.getMinutes();
	    case "ss":return date.getSeconds();
	    case "星期":return "星期" + week[date.getDay() + 7];
	    case "周":return "周" +  week[date.getDay() + 7];
	    case "week":return week[date.getDay()];
	    case "www":return week[date.getDay()].slice(0,3);
	    }
	});
}

/**
 * 获取COOKIE内容值
 */
function getcookies(){
	var strcookie = document.cookie;
	var arrcookie=strcookie.split("; ");
	var cookie = new Array();
	for(var i=0;i<arrcookie.length;i++){
		var arr=arrcookie[i].split("=");
		cookie[arr[0]] = unescape(arr[1]);
	}
	return cookie;
}
/**
 * 写cookie
 */
function setcookies(name, value){
	if(value == null){
		var expires = new Date();
		expires.setTime(expires.getTime() - 1000);
		return document.cookie = name+"="+escape(value)+";expires="+expires.toGMTString()+";path=/";
	}
	return document.cookie = name+"="+escape(value)+";path=/";
}
/*  moregame 精品推荐页设置 */
$(function(){
	var njh_appid = $("#canalid").val();
	var mgstatus = {1:'暂停',0:'开启'};
	var mgopen = {1:'<span class="gen">投放中</span>',0:'<span class="red">暂停</span>',2:'待投放'};
	//日期输入框
	function inputDateOnClick(obj){
		var obj = $(obj);
		obj.DatePicker({
			format:'Y-m-d',
			date: obj.val(),
			current: obj.val(),
			starts: 1,
			position: 'bottom',
      		enablePast:true,
			onBeforeShow: function(){
				obj.DatePickerSetDate(obj.val(), true);
			},
			onChange: function(formated,dates){
					obj.val(formated);
					obj.DatePickerHide();
			}
		});
	}

	$('.inputdate').each(function(){
		inputDateOnClick($(this));
	});
	$("#hs_list_con").toggle(function(e){
		this.innerHTML="收起明细&#9660;";
		$("#list_con").slideDown();
	},function(e){
		this.innerHTML="展开明细&#9660;";
		$("#list_con").slideUp();
	});
	$("#ad_on_off2").click(function(){
		var obj = this;
		var name=$(obj).attr("name");
		$("#ad_on_off_em").removeClass("gen");
		$("#ad_on_off_em").addClass("red");
		$("#ad_on_off_em").html("开关修改中，请等待……");
		if($(this).hasClass('off')){
			$.post("/manage/adcanal/ajaxWallSwitch", {switchs:1,canalid:njh_appid, name:name}, function(data){
				if(data && data['status'] == 1){
					$(obj).addClass("on");
					$(obj).removeClass("off");
					$("#eidt_con").hide();
					$("#ad_on_off_em").removeClass("red");
					$("#ad_on_off_em").addClass("gre");
					$("#ad_on_off_em").html('<b class="icon_right"></b>设置完成');
				}
			},'json');
		}else{
			$.post("/manage/adcanal/ajaxWallSwitch/", {switchs:0,canalid:njh_appid,name:name}, function(data){
				if(data && data['status'] == 1){
					$(obj).addClass("off");
					$(obj).removeClass("on");
					$("#eidt_con").hide();
					$("#ad_on_off_em").removeClass("red");
					$("#ad_on_off_em").addClass("gre");
					$("#ad_on_off_em").html('<b class="icon_right"></b>设置完成');
				}
			},'json');
		}
	});
	//样式选择
	$(".style_select .styleitem").click(function(){
		var name= $(this).attr("name");
		var canalid = njh_appid;
		var obj = this;
		$.post("/manage/adcanal/ajaxWallStyle", { name:name, canalid:canalid} , function(data){
			if(data && data['status'] == 1){
				$(obj).siblings().find("b").removeClass("curr");
				$(obj).find("b").addClass("curr");
			}else{
				alert(data['info']);
			}
		},'json');
	});
	$(".njh_add_stuff").click(function(){
		$("#appurl1").val('');
		$("#sortid1").val('');
		$("#material").val("");
		$("#target").val("");
		$("#popup_1").fadeIn();
	});
	$(".njh_edit").live('click',function(){
		var appname = $(this).parents("tr").find("div[class=title]").html();
		var descs = $(this).parents("tr").find("div[class=detail]").html();
		var star = $(this).parents("tr").find("td:eq(2)").attr("name");
		var appid = $(this).attr("id");
		var cid = $(this).attr("cid");
		var material = $(this).attr("title");
		var target = $(this).attr("key");
		var url = $(this).attr("url");
		var sortid = $(this).attr("name");
		var starttime = $(this).attr("starttime");
		var endtime = $(this).attr("endtime");
		$("#appname").val(appname);
		$("#descs").val(descs);
		$("#appid").val(appid);
		$("#cid").val(cid);
		$("#material2").val(material);
		$("#target2").val(target);
		$("#url2").val(url);
		$("#sortid2").val(sortid);
		$("select[name=star]").val(star);
		$("#edit_starttime").val(starttime);
		$("#edit_endtime").val(endtime);
		$("#popup_2").fadeIn();
	});

	$(".njh_save_stuff2").click(function(){
		var appname = $('#appname').val();
		var descs = $('#descs').val();
		var star = $("select[name=star] option:selected").val();
		var material = $("#material2").val();
		var target = $("#target2").val();
		var url = $("#url2").val();
		var sortid = $("#sortid2").val();
		var starttime = $("#edit_starttime").val();
		var endtime = $("#edit_endtime").val();
		if(star<0){
			return false;
		}
		if(material == ''){
			alert("素材名称不能为空！");
			return false;
		}
		if(appname == ''){
			alert("App名不能为空！");
			return false;
		}
		if(sortid < 1 || sortid >200){
			alert("排序的范围只能是1至200之间。");
			return false;
		}
			
		var appid = $("#appid").val();
		var cid = $('#cid').val();
		$.post("/manage/app/ajaxEditApp", {cid:cid, descs:descs, star:star, appname: appname, canalid:njh_appid, sortid:sortid, target:target, url:url,material:material, starttime:starttime, endtime:endtime}, function(data){
			if(data && data['status'] ==1){
				var thisoneid= data['data']['thisoneid'];
				$("#"+thisoneid).parents("tr").find("div[class=title]").html(data['data']['appname']);
				$("#"+thisoneid).parents("tr").find("div[class=detail]").html(data['data']['descs']);
				$("#"+thisoneid).parents("tr").find("td:eq(2)").html('<div class="star star'+(star*10)+'"></div>');
				$("#"+thisoneid).parents("tr").find("td:eq(2)").attr("name", star);
				$("#"+thisoneid).parents("tr").find("td:eq(4)").html(data['data']['starttime']+"~"+data['data']['endtime']);
				$("#"+thisoneid).parents("tr").find("td:eq(5)").html(sortid);
				$("#"+thisoneid).parents("tr").find("td:eq(6)").html(data['data']['uptime']);
				$("#"+thisoneid).parents("tr").find("td:eq(7)").html(data['data']['status']);
				if($("#"+thisoneid).find(".njh_stopandstart").length == 0){
					var html ='<a class="njh_stopandstart" href="javascript:void(0)" name="'+data['data']['nowstatus']+'" id="'+data['data']['id']+'">'+data['data']['showstatus']+'</a>';		
					$("#"+thisoneid).prepend(html);
				}
				$("#"+thisoneid).find("a:eq(1)").attr("title", data['data']['material']);
				$("#"+thisoneid).find("a:eq(1)").attr("key", data['data']['download']);
				$("#"+thisoneid).find("a:eq(1)").attr("name", data['data']['oid']);
				$("#"+thisoneid).find("a:eq(1)").attr("starttime", data['data']['starttime']);
				$("#"+thisoneid).find("a:eq(1)").attr("endtime", data['data']['endtime']);
				$("#popup_2").fadeOut();
			}else{
				if(data && data['info']){
					alert(data['info']);
				}
			}
		}, 'json');
	});

	$("input[name=adsize]").change(function(){
		var val = $(this).val();
		$("#adsize_em").removeClass("gen");
		$("#adsize_em").addClass("red");
		$("#adsize_em").html("屏幕尺寸修改中，请等待……");
		$.post("/manage/app/ajaxSize", {val:val, appid:njh_appid}, function(data){
			if(data && data['status'] == 1){
				$("#adsize_em").removeClass("red");
				$("#adsize_em").addClass("gre");
				$("#adsize_em").html('<b class="icon_right"></b>设置完成');
			}
		},'json');
	});
	$(".screen").change(function(){
		var val = $(this).val();
		var name = $(this).attr("name");
		$("#screen_em").removeClass("gen");
		$("#screen_em").addClass("red");
		$("#screen_em").html("模板修改中，请等待……");
		$.post("/manage/adcanal/ajaxWallScreen", {val:val, canalid:njh_appid,name:name}, function(data){
			if(data && data['status'] == 1){
				$("#screen_em").removeClass("red");
				$("#screen_em").addClass("gre");
				$("#screen_em").html('<b class="icon_right"></b>设置完成');
			}
		},'json');
	});
	
	$(".sizetpl").change(function(){
		var val = $(this).val();
		var name = $(this).attr("name");
		$("#sizetpl_em").removeClass("gen");
		$("#sizetpl_em").addClass("red");
		$("#sizetpl_em").html("尺寸修改中，请等待……");
		$.post("/manage/adcanal/ajaxWallSize", {val:val, canalid:njh_appid,name:name}, function(data){
			if(data && data['status'] == 1){
				$("#sizetpl_em").removeClass("red");
				$("#sizetpl_em").addClass("gre");
				$("#sizetpl_em").html('<b class="icon_right"></b>设置完成');
			}
		},'json');
	});

	$(".njh_save_stuff1").click(function(){
		var url = $("#appurl1").val();
		var sortid = $("#sortid1").val();
		var material = $("#material").val();
		var target = $("#target").val();
		var starttime = $("#start_starttime").val();
		var endtime = $("#add_endtime").val();
		if(material == ''){
			alert("素材名称不能为空！");
			return false;
		}
		if(url.length <20){
			alert("您输入的App URL有误，请检查后重新输入！");
			return false;
		}
		if(sortid < 1 || sortid >200){
			alert("排序的范围只能是1至200之间。");
			return false;
		}		
		$(".njh_save_stuff1").hide();
		$("#njh_hidden").show();
		$.post("/manage/app/ajaxAddApp",{url:url, sortid:sortid, canalid:njh_appid, material:material, target:target, starttime:starttime, endtime:endtime }, function(data){
			if(data && data['status'] == 1){
				$("#popup_1").fadeOut();
				var sortnum = data['data']['sortnum'];
				if(sortnum<0){
					sortnum = 0;
				}
				var text = '<tr id="1"><td><div>'+sortid+'</div></td><td><div class="licon fl"><img src="'+data['data']['icon']+'" width="48" height="48"></div><div class="mcon"><div class="title">'+data['data']['appname']+'</div><div class="detail">'+data['data']['descs']+'</div></div></td><td name="'+data['data']['star']+'"><div class="star star'+data['data']['stars']+'"></div></td><td>'+data['data']['price']+'</td><td>'+data['data']['starttime']+'~'+data['data']['endtime']+'</td><td>'+data['data']['oid']+'</td><td>'+data['data']['uptime']+'</td><td>'+data['data']['status_name']+'</td><td id="'+data['data']['appid']+njh_appid+'"><a id="'+data['data']['id']+'" name="3" href="javascript:void(0)" class="njh_stopandstart">暂停</a> <a href="javascript:void(0)" cid="'+data['data']['id']+'" url="'+data['data']['url']+'" id="'+njh_appid+'" name="'+data['data']['oid']+'" key="'+data['data']['download']+'" title="'+data['data']['material']+'" starttime="'+starttime+'" endtime="'+endtime+'" class="njh_edit">编辑</a></td></tr>';
				if(sortnum<1){
					$("table.applist tr:eq(1)").before(text);
				}else{
					$("table.applist tr:eq("+sortnum+")").after(text);
				}
				$("#njh_del_tr").remove();
				repairNum();
			}
			if(data && data['info']){
				alert(data['info']);
				$(".njh_save_stuff1").show();
				$("#njh_hidden").hide();
			}
		},'json');
	});
	$("#storeid").blur(function(){
		var storeid = $(this).val();
		var obj = this;
		if(storeid.length > 0){
			$("#storeid_em").removeClass("gen");
			$("#storeid_em").addClass("red");
			$("#storeid_em").html("StoreID修改中，请等待……");
			$.post("/manage/app/ajaxStoreid",{storeid:storeid, appid:njh_appid}, function(data){
				if(data && data['status'] == 1){
					$("#storeid_em").removeClass("red");
					$("#storeid_em").addClass("gre");
					$("#storeid_em").html('<b class="icon_right"></b>设置完成');
				}
			},'json');
		}
	});

	$(".njh_stopandstart").live('click',function(){
		var id = $(this).attr('id');
		var status = $(this).attr('name');

		var obj = this;
		var showstatus = {'-1':'待投放','1':'暂停','0':'开启','2':'投放结束'};
		$.post("/manage/adcanal/ajaxStatus", {id:id, status:status, canalid:njh_appid} , function(data){
			if(data && data['status'] == 1){
				$(obj).text(data['data']['linkname'] + " ");
				$(obj).attr('name', data['data']['status']);
				$(obj).parents("tr").find("td:eq(6)").html(data['data']['uptime']);
				$(obj).parents("tr").find("td:eq(7)").html(data['data']['status_name']);
				$(obj).parents("tr").attr("data_id",data['data']['status_id']);
			}else{
				if(data && data['info']){
					alert(data['info']);
				}
			}
		},'json');
	});

	$("#njh_adcou").click(function(){
		if($(this).attr("checked")){
			var accept = 1;
		}else{
			var accept = 0;
		}
		$("#njh_adcou_em").removeClass("gen");
		$("#njh_adcou_em").addClass("red");
		$("#njh_adcou_em").html("开关修改中，请等待……");
		$.post("/manage/app/ajaxAccept", {val:accept, canalid:njh_appid}, function(data){
			if(data && data['status'] == 1){
				$("#njh_adcou_em").removeClass("red");
				$("#njh_adcou_em").addClass("gre");
				$("#njh_adcou_em").html('<b class="icon_right"></b>设置完成');
			}
		},'json');
	});

	$("#njh_srch").click(function(){
		var sech = $("#njh_srch_val").val();
		if(sech =='请输入App名' || sech == ''){
			$("table.applist tr:gt(0)").css("display","");
			return false;
		}
		$("#clean2").show();
		var find = 0;
		$("table.applist tr:gt(0)").each(function(){
			sech = sech.toLowerCase();
			var text = $("td .title",this).eq(0).html();
			text = text.toLowerCase();
			if(text){
				if(text.indexOf(sech) != -1){
					find++;
					$(this).css("display","");
				}else{
					$(this).css("display","none");
				}
			}
		});
		if(find == 0){
			alert("抱歉，没有对应的App");
		}
	});
	$("#clean2").click(function(){
		searchClean();
		var find = 0;
		var sech = '';
		$("table.applist tr:gt(0)").css("display","");
		
	});
	$(".icon_clean").hide();
	$("#njh_search_app").click(function(){
		var search = $("#njh_appname").val();
		$(".icon_clean").show();
		if(search == '输入App名称'){
			search = '';
		}
		appnamesearch(search);
	});
	$(".njh_adtype").change(function(){
		if($(this).val()==2){
			location.href="/manage/app/openedit?appid="+njh_appid;
		}else{
			location.href="/manage/app/walledit?appid="+njh_appid;
		}
	});
	$(".njh_open_edit").live('click',function(){
		$("#edit_stuff").val($(this).parents("tr").find("td:eq(1)").html());
		$("#edit_target").html($(this).attr("value"));
		var starttime = $(this).parents('tr').find("td:eq(2)").html();
		var endtime = $(this).parents('tr').find("td:eq(3)").html();
		//starttime = starttime.split(' ');
		//endtime = endtime.split(' ');
		$("#edit_starttime").val(starttime);
		$("#edit_endtime").val(endtime);
		var a900 = $(this).attr("name");
		var a600 = $(this).attr("type");
		//$("#ae900img").html("<img src='http://pic.punchbox.org"+a900+"' width=300 height=200 />");
		//$("#ae600img").html("<img src='http://pic.punchbox.org"+a600+"' width=200 height=300 />");
		$("#ae900img").html("<img src='"+a900+"' width=300 height=200 />");
		$("#ae600img").html("<img src='"+a600+"' width=200 height=300 />");
		var id = $(this).attr("id");
		$(".confirm[name=edit]").attr("id",id);
		$("#popup_3").show();
	});
	$(".njh_open_add").click(function(){
		$("#add_stuff").val("");
		$("#add_target").val("");
		$("#uploadfile1").val("");
		$("#uploadfile2").val("");
		$(".inputdate").val($("#njh_thisdate").val());
		$("#ad900img img").attr("src","");
		$("#ad600img img").attr("src","");
		$("#popup_2").show();
	});

	$(".njh_del").click(function(){
		var id=$(this).attr("id");
		$("input[name=uploadata]").val("");
		$("#a"+id+"img").html("");
		$("#a"+id).val("");
	});

	$(".confirm").click(function(){
		var name=$(this).attr("name");
		var appid = $("#njh_appid").val();

		var stuff = $("#"+name+"_stuff").val();
		var starttime = $("#"+name+"_starttime").val();
		var endtime = $("#"+name+"_endtime").val();
		var id = 0;
		if(name== 'add'){
			var a900=$("#ad900img").find("img").attr("src");
			var a600=$("#ad600img").find("img").attr("src");
			var target = $("#add_target").val();
		}else{
			var a900=$("#ae900img").find("img").attr("src");
			var a600=$("#ae600img").find("img").attr("src");
			var id = $(this).attr("id");
			var target = '';
		}

		$.post("/manage/app/ajaxOpenSave",{name:name,stuff:stuff,starttime:starttime,endtime:endtime,a900:a900,a600:a600,appid:appid,target:target,id:id},function(data){
			if(data && data['status'] == 1){
				var rstatus = '';
				if(data['data']['status'] == '投放中' || data['data']['status'] == '暂停'){
					if(data['data']['status'] =='投放中'){
						data['data']['status'] = '<span class="gen">投放中</span>';
					}
					if(data['data']['status'] =='暂停'){
						data['data']['status'] = '<span class="red">暂停</span>';
					}
					rstatus = "<a id=\""+(data['data']['id'])+"\" name=\""+(data['data']['statusid'])+"\" key=\"open\" href=\"javascript:void(0)\" class=\"njh_stopandstart\">"+(data['data']['statusid'] ==1?'开启':'暂停')+"</a>";
				}
				var text = "<tr id='njh_"+(data['data']['id'])+"'><td><div>"+(data['data']['id'])+"</div></td><td>"+(data['data']['stuff'])+"</td><td>"+(data['data']['starttime'])+"</td><td>"+(data['data']['endtime'])+"</td><td>"+(data['data']['status'])+"</td><td>"+rstatus+" <a href=\"javascript:void(0)\"  id=\""+(data['data']['id'])+"\" name=\""+(data['data']['horizontal'])+"\" type=\""+(data['data']['vertical'])+"\" value=\""+(data['data']['target'])+"\" class=\"njh_open_edit\">编辑</a></td></tr>";
				if(name=='add'){
					$("#popup_2").hide();
					$("table.applist tr:eq(1)").before(text);
				}else{
					$("#njh_"+data['data']['id']).replaceWith(text);
					$("#popup_3").hide();
				}
				$("#njh_del_tr").remove();
				repairNum();
			}
			if(data && data['info']){
				alert(data['info']);
			}
		},'json');
	});

	
});
function ajaxFileUpload(fileObj,type){
	var obj       = $(fileObj);
	var fileId 	  = obj.attr('id');
	var str       = '';
	$.ajaxFileUpload({
		url:'/manage/app/ajaxUpload?type='+type,
		secureuri:false,
		fileElementId:fileId,
		dataType: 'json',
		success: function(data){
			if(data.status)
			{
				if(fileId=="uploadfile1"){
					$('#ad900img').html("<img src='"+data.data+"' width=300 height=200 />");
				}else if(fileId=="uploadfile2"){
					$('#ad600img').html("<img src='"+data.data+"' width=200 height=300 />");
				}else if(fileId=="uploadfile3"){
					$('#ae900img').html("<img src='"+data.data+"' width=300 height=200 />");
				}else if(fileId=="uploadfile4"){
					$('#ae600img').html("<img src='"+data.data+"' width=200 height=300 />");
				}
			}
			else{
				alert(data.info);
			}
		}
	});
}
function repairNum(){
	$("table.applist tr:gt(0)").each(function(index, element) {
		var n = index;
		if(index<9){n = (index+1);}
			$(this).find("td").eq(0).html(n);
	});
}

function appnamesearch(search){
	setcookies('njh_app_namelist', search);
	if(!search || search == '' || search =='undefined'){
		$("#njh_appname_list a").each(function(){$(this).show()});
		return false;
	}
	$("#njh_appname_list a").each(function(){
		var name = $(this).html();
		if(name.indexOf(search) != -1){
			$(this).show();
		}else{
			$(this).hide();
		}
	});
}
function statusselect(status){

	$("table.applist tr:gt(0)").each(function(){
		if(status == ''){
			$(this).css("display","");
		}else{
			if($(this).attr("data_id") == status){
				$(this).css("display","");
			}else{
				$(this).css("display","none");	
			}
		}		
	});
}
var cookie = getcookies();
if(cookie && cookie['njh_app_namelist'] != ''){
	appnamesearch(cookie['njh_app_namelist']);$("#njh_appname").val(cookie['njh_app_namelist']);
}
$("#directional").click(function() {
	var obj = this;
	var name = $(obj).attr("name");
	var njh_appid = $("#canalid").val();
	$.post("/manage/adcanal/ajaxDirectionalSwitch", {
		'name' : name,
		'canalid' : njh_appid,
		'areaid' : $("#input_hidden_area").val(),
		'verid' : $("#input_hidden_ver").val(),
		'channelid' : $("#input_hidden_way").val()
	}, function(data) {
		if (data && data['status'] == 1) {
			alert(data['info']);
			window.location = window.location;
		}
	}, 'json');
});

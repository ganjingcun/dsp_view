// JavaScript Document
var group = new Object();
group.rename = function(list,id){
		var n = $("#"+list+" li").index($("#"+id))+1;
		var name = $("#"+id).find("input").eq(1).attr("value")//获取输入框内容
		name = String(name).replace(/\s+/g,"")//去掉空格
		if(name != "输入组名" && name != "" ){
			$.ajax({
				type:"POST",
				data:"groupname="+name,
				//date:{groupname:name},
				url:"/manage/usercenter/ajaxgroupinfo",
				success:function(msg){
					if(msg>0)
					{
						$("#"+id).html('<input id="groupid[]" name="groupid[]" type="checkbox" checked="checked" value="'+msg+'" /> <label for="c'+n+'">'+name+'</label>');
					}
					else if(msg=='0')
					{
						alert('非法的权限!');
					}				
					else if(msg=='-1')
					{
						alert('组名称不能少于3个或大于30个字符!');
					}				
					else if(msg=='-2')
					{
						alert('组名称不能重复!');
					}
					else if(msg=='-3')
					{
						alert('创建组错误!');
					}
				}
			});
		}else{
			alert("请输入组名!")
			}
		}
group.add = function(list){	
		var s = $('#'+list+' li');
		var n =s.length;
		var name = "add_"+(n+1);
		var str = $("#add_"+n).find("input").eq(1).attr("value")//获取输入框内容
		str = String(str).replace(/\s+/g,"")//去掉空格
		if(str != "输入组名" && str!=""){
			$("#add_"+n).find("input").eq(1).attr("value",str)
			$('#'+list).append('<li id="'+ name +'" ><input id="'+(n+1)+'" type="checkbox" /> <input type="text" value="输入组名" onclick="inputCueInfo(this,\'输入组名\')" style="width:70px;" /><a class="add" href="javascript:group.rename(\''+list+'\', \''+name+'\');" ></a><a class="del" href="javascript:group.del(\''+name+'\');" ></a></li>');	
		}else{
			alert("请输入组名!")
			}
		}
group.del = function (id){
		$("#"+id).remove();
}
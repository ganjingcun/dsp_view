//元素名称：m_n; tab_m_n  Tab菜单切换
//count:交换的个数
var $get = function(){
	var elements = new Array();
	var i = arguments.length;
	for (var i = 0; i < arguments.length; i++) {
		var element = arguments[i];
		if (typeof element == 'string')
			element = document.getElementById(element);
		if (arguments.length == 1) {
			return element;
		}
		elements.push(element);
		
	}
	return elements;
}

function showtab(m,n,count){
	for(var i=1;i<=count;i++){
		$get("td_"+m+"_"+i).className = $get("td_"+m+"_"+i).className.replace("hover","");
		$get("tab_"+m+"_"+i).className = $get("tab_"+m+"_"+i).className.replace("show","hidden");
		
	}
	if(n>0){
		$get("td_"+m+"_"+n).className += " hover";
		$get("tab_"+m+"_"+n).className = $get("tab_"+m+"_"+n).className.replace("hidden","show");
	}
}// Tab菜单切换结束

//主菜单切换
function menuClassTab(n,long,classname){
	for(var i=1;i<=long;i++){
		n ==i ? document.getElementById("td_"+i).className = classname : document.getElementById("td_"+i).className = '';
	}
}


//表格相关： 表格TR隐藏和切换
function tabletab(listId,trId,e){		
		$("#"+listId+" .curr").removeClass("curr");
		$("#"+listId+" .show").hide();	
		$("#"+listId+" .show").removeClass("show");	
		$(e).addClass("curr");
		$("#"+trId).addClass("show");
		$("#"+trId).removeClass("hidden");	
		$("#"+trId).show();
	}

function table_in_tab(trId){
		$("#"+trId).addClass("show");
		$("#"+trId).removeClass("hidden");	
		$("#"+trId).show();
	}

//隐藏对象或显示对象
function togObj(id){
	var objStyle = document.getElementById(id).style.display;
	if(objStyle != 'none'){
		document.getElementById(id).style.display='none';
	}else{
		document.getElementById(id).style.display='block';
		}
}

//输入框相关，输入框提示信息	
function inputCueInfo(id,s){
	if(id.value == s){
		id.value = "";
		id.style.color = "#666";
	}		
	id.onblur = function(){
		if(id.value == "") {
			id.value = s;
			id.style.color = "#999";
		}
	}
}



function user_choice(obj){
	$(obj).unbind()
	$(obj).click(function(){
		if( this.className == "ch"){
			this.className = "";
		}else if(this.className != "null"){
			this.className = "ch";
		}
	})		
}
		
var member = new Object();
member.move = function(listA,listB,trait){
	listA = "#"+listA;
	listB = "#"+listB;
	trait = " "+trait;		
	userList = $(listA+trait);
	var obj = userList.clone();
	obj.appendTo($(listB));
	userList.remove();
	userList.addClass("null");
	user_choice(obj);
};
//member.add = function(){};	
//member.del = function(){};



//日期格式化
Date.prototype.format = function(format)
{
    var o =
    {
        "M+" : this.getMonth()+1, //month
        "d+" : this.getDate(),    //day
        "h+" : this.getHours(),   //hour
        "m+" : this.getMinutes(), //minute
        "s+" : this.getSeconds(), //second
        "q+" : Math.floor((this.getMonth()+3)/3),  //quarter
        "S" : this.getMilliseconds() //millisecond
    }
    if(/(y+)/.test(format))
    format=format.replace(RegExp.$1,(this.getFullYear()+"").substr(4 - RegExp.$1.length));
    for(var k in o)
    if(new RegExp("("+ k +")").test(format))
    format = format.replace(RegExp.$1,RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length));
    return format;
}


/* ==================== 日历排期 ===================== */


function SetDate(day){
	day = Number(day)
	var thisDate = new Date();
	var pastDate = new Date();
	if(day == -1){
		thisDate.setDate(thisDate.getDate()-1);
		}
	day < -1 ? pastDate.setDate(pastDate.getDate()+day+1) : pastDate.setDate(pastDate.getDate()+day);
	var dateEnd = String(thisDate.getFullYear()+"-"+(thisDate.getMonth()+1)+"-"+thisDate.getDate());
	var dataStart = String(pastDate.getFullYear()+"-"+(pastDate.getMonth()+1)+"-"+pastDate.getDate());
	day>0 ? $('#'+date_box).DatePickerSetDate([dateEnd,dataStart],false) : $('#'+date_box).DatePickerSetDate([dataStart,dateEnd],false)
}

function dateToStr(obj){
	var dateArray = new Array()
	for( var i=0;i<obj.length; i++){
		dateArray.push(String(obj[i].getFullYear()+"-"+(obj[i].getMonth()+1)+"-"+obj[i].getDate()))
		}
	return dateArray
}




//============数据列表 操作=============
var dataList = new Object();
dataList.id = "user_list";
//
dataList.allCheck = function(){
	$("#"+dataList.id+" tr input").attr("checked","checked")
}
//
dataList.removeCheck = function(){
	$("#"+dataList.id+" tr input").removeAttr("checked")
}


//========================= 鼠标经过切换图片 =============================

function onHoverImg(e,aimId){
	if($(e.target).attr("href")){
		$("#"+aimId+" img").attr("src",$(e.target).attr("href"))
		}else if($(e.target).attr("title")){
			$("#"+aimId+" img").attr("src",$(e.target).attr("title"))
		}
	var img = $("#"+aimId+" img").show()
	if(img){
		$("#"+aimId).css({
			'position':'absolute',
				'left':'0',//e.pageX-($("#"+aimId).width()/2)
				'top':$(e.target).position().top,
				//'margin-left':($("#"+aimId).width()/2)*-1,
				'display':'inline-block'
		})
	}
}


//===============全局下拉菜单 select_all============
window.onload = function(){
	$('.select_all').click(function(){
		var selectsd = $(this).find('.selected')
		$(this).find(".select_list").slideDown('fast', function(){
				$(this).find('a').click(function(){
						selectsd.html($(this).html()+"<s></s>")
					})
				})
		$(this).hover({}, function(){
			$(this).find('.select_list').slideUp('fast')
			})
		})
}



// JavaScript Document
//所有报表页 相关

$(function() {
	// event side collapsible tab
	$(".event-side ul li.side-ttl").click(function() {
		var $this = $(this);
		$this.toggleClass('side-cur');
		var icon = $this.children('b.icon_open').toggleClass('icon_closeup');
		if( icon.hasClass("icon_closeup") ) {
			$this.next().slideDown();
		}
		else {
			$this.next().slideUp();
		}
	})

	$('.side-g-data').each(function(){
		 $(this).find(".side-data-item").click(function(){
			 var eObj = $(this)
			 var icon = eObj.children('b.icon_gplus').toggleClass("icon_gr");
			 if( icon.hasClass("icon_gr")){
				//eObj.find('.side-data-in').slideUp();
				eObj.next('.side-data-in').slideDown();
			}
			else {
				eObj.next('.side-data-in').slideUp();
			}
		})
	})
	
	$('.side-data-in').each(function(){
		var list = $(this)
		$(this).find('a').click(function(){
			list.find('a').removeClass('curr');
			$(this).addClass('curr')
		})	
	})
	
	
	//顶部时间 切换菜单
	$('.event-con-top .tab').each(function(){
		var obj = $(this)
		obj.find('a').click(function(){
			obj.find('a').removeClass('event-nav-cur')
			$(this).addClass('event-nav-cur')	
			
		})
	})
	
	//图表 数据类型 切换菜单
	$('.tab_menu').each(function(){
		var obj = $(this)
		obj.find('a').click(function(){
			obj.find('a').removeClass("curr")
			$(this).addClass("curr")
			
		})	
	})
	
	//列表里的小箭头
	$('.icon_arrowt').click(function(){
		if($(this).hasClass('icon_arrowt_cur')) {
			$(this).removeClass('icon_arrowt_cur')
		}else{
			$(this).addClass('icon_arrowt_cur')	
		}
	})
	
})
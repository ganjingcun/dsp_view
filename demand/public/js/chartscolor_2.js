
Highcharts.theme = {
	colors: ['#00a2db', '#fcb943', '#9ac557', '#f44d3b', '#62676e', '#c557bc', '#37d3bb', '#f4e93b', '#ff5a5a', '#e6dfce', '#2ad0de'],
			/*'#514e95'*//*#01addf*/
	chart: {
		backgroundColor: ''
	},
	title: {
		style: {
			color: '#666666',
			font: 'bold 16px "微软雅黑", "Trebuchet MS", Verdana, sans-serif'
		}
	},
	subtitle: {
		style: {
			color: '#666666',
			font: 'bold 12px "微软雅黑","Trebuchet MS", Verdana, sans-serif'
		}
	},
	xAxis: {
		gridLineWidth:0,//X竖线
		lineColor:'#cccccc',
		tickColor:'#cccccc',
		gridLineColor: '#bbb',
		labels: {
			style: {
				color: '#666666',
				font: '11px "微软雅黑",Trebuchet MS, Verdana, sans-serif'
			}
		},
		title: {
			style: {
				color: '#666666',
				fontWeight: 'bold',
				fontSize: '12px',
				fontFamily: '"微软雅黑",Trebuchet MS, Verdana, sans-serif'
			}
		}
	},
	yAxis: {
		min:0,
		lineColor: '#cccccc',
		tickColor: '#cccccc',
		gridLineColor: '#ddd',
		labels: {
			style: {
				color: '#666666',
				font: '11px "微软雅黑",Trebuchet MS, Verdana, sans-serif'
			}
		},
		title: {
			style: {
				color: '#666666',
				fontWeight: 'bold',
				fontSize: '12px',
				fontFamily: '"微软雅黑",Trebuchet MS, Verdana, sans-serif'
			}
		}
	},
	plotOptions: {//线条样式
		 series: {//节点和数据样式
			lineWidth:2,//线条粗细
			animation:{//开始展现时效果
				duration:1000,//耗时(毫秒),展现所需时间
				easing:'linear'//展现时的动态(主要体现在柱状图) linear ,swing,transition ,easing
			},
			dataLabels: {//节点上文字
				//enabled:true,//是否显示节点上的文字
				align: 'left',//文字对齐
				x: 2,//坐标x
				y: -10
			}
		},
		line: {
			//step:false,//线条平滑过度
			//stickyTracking:true,//鼠标moveout事件
			//stacking:null,//是否共享Y轴, 'normal' ,'percent',null
			//showInLegend:true,//线条开关 是否显示
			//allowPointSelect:false,//是否允许鼠标点选节点后改变样式
			fillOpacity: 0.1,
			dashStyle:'Solid',//线条样式 Solid 实线,ShortDash,ShortDot,ShortDashDot,ShortDashDotDot,Dot,Dash,LongDash,DashDot,ongDashDot,LongDashDotDot
			marker: {
				enabled: false,//是否显示节点
				//symbol: 'circle',//所有节点形状 "circle"圆点, "square"方, "diamond"钻型, "triangle"三角 and "triangle-down"向下的三角
				//radius: 3,//节点大小
				lineWidth: 1,//节点边宽
				states:{
					//pointStart: Date.UTC(2010, 0, 1),//X轴开始的值,日期类型,需要配合xAxis设置 xAxis:{ type:'datetime' }
					//pointInterval: 24 * 3600 * 1000, //步长值
					connectNulls:false,//空数据时的处理方式,默认false,空开一个节点. true连接另外2个点.
					hover: {//鼠标进过节点修饰
						enabled:true,
						radius:5,//点大小
						lineWidth: 1//点边框
					}
				}
			}
		},
		pie: {
			allowPointSelect: true,
			cursor: 'pointer',
			dataLabels: {
				enabled: true,//图表色块描述 开关
				color: '#777',//图表色块描述 文字颜色
				connectorColor: '#888',//图表色块描述 线条颜色
				formatter: function() {
					return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
				}
			},
			showInLegend: true
		}
	},
	legend: {
		itemStyle: {
			font: '9pt Trebuchet MS, Verdana, sans-serif',
			color: '#666666'
		},
		layout: 'horizontal',// 排列方式，默认horizontal横向 vertical竖向
		align:'left',
		verticalAlign:'bottom',// 起始位置,默认top 顶部,bottom底部
		x: 0,//坐标偏移量
		y: 0,
		floating: true,
		backgroundColor:'#FFFFFF',//背景颜色
		borderColor:'#ccc',//边框颜色
		borderWidth:1,//边框厚度
		
		itemHoverStyle: {
			color: '#000000'
		},
		itemHiddenStyle: {
			color: 'gray'
		}
	},
	labels: {
		style: {
			color: '#99b'
		}
	}
};

// Apply the theme
var highchartsOptions = Highcharts.setOptions(Highcharts.theme);

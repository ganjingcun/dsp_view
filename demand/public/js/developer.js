$(document).ready(function(){
	var cookie = getcookies();
	if(cookie && cookie['selectname']){
		$("#navName").val(cookie['selectname']);
		appsearch(cookie['selectname']);
	}
	$("#njh_left_btn").click(function(){
		searchapp();
	});
});
function searchapp(){
	var selectname = $("#navName").val();
	if(selectname.length > 0){
		appsearch(selectname);
	}else{
		setcookies('selectname', null);
		$(".event-side li").css("display","");
		$(".event-side li").css("class","side-ttl");
		$(".event-side li.side-cur").each(function(index,item){
				$(item).next('li').css("display","list-item");
		});
	}
}

/**
 * 获取COOKIE内容值 
 */
function getcookies(){
	var strcookie = document.cookie;
	var arrcookie=strcookie.split("; ");
	var cookie = new Array();
	for(var i=0;i<arrcookie.length;i++){
		var arr=arrcookie[i].split("=");
		cookie[arr[0]] = unescape(arr[1]);
	}
	return cookie;
}

/**
 * 写cookie 
 */
function setcookies(name, value){
	if(value == null){
		var expires = new Date();
		expires.setTime(expires.getTime() - 1000);
		return document.cookie = name+"="+escape(value)+";expires="+expires.toGMTString();
	}
	return document.cookie = name+"="+escape(value);
}

/**
 * 查询操作过程。 
 * @param {Object} selectname
 */
function appsearch(selectname){
	$(".event-side strong").each(function(){
		var text = $(this).html();
		var id = $(this).parents("li").attr("name");
		$("li[name="+id+"]").hide();
		if(text.indexOf(selectname) != -1){
			setcookies("selectname", selectname);
			$("li[name="+id+"]").css("display","");
			$(".event-side li").css("class","side-ttl");

			if($("li[name="+id+"]").hasClass("side-cur")){
				$("li[name="+id+"]").next('li').css("display","list-item");
			}
			else
			{
				$("li[name="+id+"]").next('li').css("display","none");
			}
		}
	});
}

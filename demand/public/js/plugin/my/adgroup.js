/*function getAdgroupDivObj(current_dom_obj)
{
	var gid = $('#fc-global-groupid').val();
	return $('#'+gid);
}*/

function getAdgroupDivObj(obj) {
	return obj.parents('.fc-group');
}

/*******************************************************************************
 * 地域弹出层管理
 * 
 * @return
 */
function FCAdgroupArea() {
	this.dom_obj_item = null;
	this.dom_obj_pop = null;
	this.dom_obj_areaids = null;

	this.dom_radio_first = null;
	this.dom_radio_second = null;

}

FCAdgroupArea.prototype.init = function(current_dom_obj) {
	var group = getAdgroupDivObj(current_dom_obj);
	this.dom_obj_item = group.find('#fc_area_item');
	this.dom_obj_areaids = this.dom_obj_item.find('#areaids');
	this.dom_obj_pop = $('#popup_3');
	this.dom_radio_first = this.dom_obj_item.find('input[type=radio]:eq(0)');
	this.dom_radio_second = this.dom_obj_item.find('input[type=radio]:eq(1)');
}

// 将选中的地域放置到隐藏域当中
FCAdgroupArea.prototype.set = function() {
	// 获取设备id
	var areaid = new Array();
	var checkboxObj = this.dom_obj_pop.find('input[type="checkbox"]:checked')
	if (checkboxObj.size() < 1) {
		alert("您尚未选择任何地域");
		return false;
	}
	checkboxObj.each(function() {
		areaid.push($(this).val());
	});

	// 将设备id放到对应的组里的隐藏域里面
	this.dom_obj_areaids.val(areaid);

	// 将弹出框隐藏
	this.dom_obj_pop.hide();
}

FCAdgroupArea.prototype.get = function() {

}

// 关闭弹出层
FCAdgroupArea.prototype.close = function() {
	this.dom_obj_pop.hide();
	var areas = this.dom_obj_areaids.val()
	if (areas == '' || areas == null || areas==undefined) {
		this.dom_radio_first.attr('checked', true);
	}
}

// 显示弹出层
FCAdgroupArea.prototype.show = function() {
	var ids = this.dom_obj_areaids.val();
	if (ids != undefined) {
		var arrId = ids.split(',');
		var box = this.dom_obj_pop.find('input[type="checkbox"]');
		box.each(function() {
			if ($.inArray($(this).val(), arrId) != -1) {
				$(this).attr('checked', true);
			} else {
				$(this).attr('checked', false);
			}
		});
	}

	this.dom_obj_pop.show();

}

/*******************************************************************************
 * 设备弹出层管理
 * 
 * @return
 */
function FCAdgroupDev() {
	this.dom_obj_android_pop = null;
	this.dom_obj_ios_pop = null;
}

FCAdgroupDev.prototype.init = function(current_dom_obj) {
	this.current_dom_obj = current_dom_obj;
	this.group = getAdgroupDivObj(current_dom_obj);
	this.dom_obj_devids = this.group.find('#devids');
	this.dom_obj_dev_ver = this.group.find('#dev_version');
	this.dom_obj_app_item = this.group.find('#fc-app');
	this.str_dev_value = this.group.find('input[name="app"]:checked').attr(
			'data');
	if(this.str_dev_value==undefined) this.str_dev_value = '';

	this.dom_obj_android_pop = $('#popup_2');
	this.dom_obj_ios_pop = $('#popup_1');
}

FCAdgroupDev.prototype.set = function() {

	// 获取设备id
	var devid = new Array();

	if (this.isAndroid() == false)// 这里将android额设备判断干掉
	{
		var checkboxObj = this.dom_obj_ios_pop
				.find('input[type="checkbox"]:checked');
		if (checkboxObj.size() < 1) {
			alert("您尚未选择任何设备");
			return false;
		}

		checkboxObj.each(function() {
			if ($(this).attr('disabled') != 'disabled') {
				devid.push($(this).val());
			}
		});

		// 获取版本
		var arrVersion = new Array();
		this.dom_obj_ios_pop.find('select').each(function() {
			if ($(this).attr('disabled') != 'disabled') {
				arrVersion.push($(this).val());
			}
		});

	} else {
		// 获取版本
		var arrVersion = new Array();
		this.dom_obj_android_pop.find('select').each(function() {
			if ($(this).attr('disabled') != 'disabled') {
				arrVersion.push($(this).val());
			}
		});
	}

	// 将设备id放到对应的组里的隐藏域里面
	this.dom_obj_devids.val(devid);

	// 将设备版本放到对应的组里的隐藏域里面
	this.dom_obj_dev_ver.val(arrVersion);

	this.current_dom_obj.attr('data_dev', devid);
	this.current_dom_obj.attr('data_ver', arrVersion);

	this.close();
}

FCAdgroupDev.prototype.get = function() {

}

FCAdgroupDev.prototype.show = function() {
	if (this.isAndroid()==true) {
		this.showAndroid();
	} else {
		this.showIos();
	}

}

FCAdgroupDev.prototype.close = function() {
	var version = this.dom_obj_dev_ver.val();
	
	if (version == '' || version == null) {
		this.dom_obj_app_item.find('input[type=radio]:eq(0)').attr('checked',
				true);
	}
	this.dom_obj_android_pop.hide();
	this.dom_obj_ios_pop.hide();
}

FCAdgroupDev.prototype.showAndroid = function() {
	if (this.str_dev_value == '' || this.str_dev_value == null || this.str_dev_value == undefined) {
		alert('请选择所属平台');
		this.close();
		return false;
	}

	var ver = this.current_dom_obj.attr('data_ver');
	if (ver != undefined) {
		var arrVer = ver.split(',');
		this.dom_obj_android_pop.find('select:eq(0)').find(
				'option[value="' + arrVer[0] + '"]').attr('selected', true);
		this.dom_obj_android_pop.find('select:eq(1)').find(
				'option[value="' + arrVer[1] + '"]').attr('selected', true);// 最高版本设置为不限制
	}

	this.dom_obj_android_pop.show();
}

FCAdgroupDev.prototype.showIos = function() {
	
	if (this.str_dev_value == '' || this.str_dev_value == null || this.str_dev_value == undefined) {
		alert('请选择所属平台');
		this.close();
		return false;
	}

	var box = this.dom_obj_ios_pop.find('input[type="checkbox"]').attr(
			'disabled', true).attr('checked', false);

	var devids = this.dom_obj_devids.val().split(',');

	var arrDev = this.str_dev_value.split(',');

	box.each(function() {

		if ($.inArray($(this).attr('data'), arrDev) != -1) {

			$(this).attr('disabled', false);

			if (devids != '') {
				if ($.inArray($(this).attr('data'), devids) != -1) {
					$(this).attr('checked', true);
				}
			} else {
				$(this).attr('checked', true)
			}
		}
	});

	var ver = this.current_dom_obj.attr('data_ver');
	if (ver == undefined || ver == '' || ver == null) {
		this.dom_obj_ios_pop.find('select:eq(0)').find('option:eq(0)').attr(
				'selected', true);
		this.dom_obj_ios_pop.find('select:eq(1)').find('option:eq(0)').attr(
				'selected', true);// 最高版本设置为不限制
	} else {

		var arrVer = ver.split(',');
		this.dom_obj_ios_pop.find('select:eq(0)').find(
				'option[value="' + arrVer[0] + '"]').attr('selected', true);
		this.dom_obj_ios_pop.find('select:eq(1)').find(
				'option[value="' + arrVer[1] + '"]').attr('selected', true);// 最高版本设置为不限制
	}

	this.dom_obj_ios_pop.show();

}

FCAdgroupDev.prototype.isAndroid = function() {
	var arrDev = this.str_dev_value.split(',');
	if ($.inArray('android', arrDev) != -1) {
		return true;
	}
	return false;
}

function User() {
	this.obj_pop = $('#con1');
	this.data_users = {};// 原始数据
	this.data_group_list = {};// 原始数据
	this.users = {};
	this.group_list = {};
	this.all_users = [];
	this.source = this.obj_pop.find('#listA');
	this.target = this.obj_pop.find('#listB');
	this.obj_select = this.obj_pop.find('select:eq(0)');
	this.current_groupid = 0;
	this.userid_in_group = [];
}

User.prototype.init = function() {

	this.getGroupUserid();
	this.filter();
	this.clear();

	this.obj_pop.find('input[name=groupid]').val(this.current_groupid);

	for ( var gid in this.users) {
		for ( var i in this.users[gid]) {
			var uid = this.users[gid][i]['userid'];
			this.all_users[uid] = this.users[gid][i];
		}
	}
	this.fill(this.all_users);

	this.users[0] = this.all_users;// 将所有成员独立存放到一个group中，id=0

	var str = '<option value="0" >全部成员</option>';
	for ( var i in this.group_list) {
		str += '<option value="' + this.group_list[i]['groupid'] + '" >'
				+ this.group_list[i]['groupname'] + '</option>';
	}
	this.obj_select.html(str);

	var self = this;
	this.obj_select.bind('change', function() {
		self.change(this.value);
	});

}

User.prototype.fill = function(g) {
	var str = '';

	for ( var i in g) {
		if (g[i] != null && g[i] != '' && g[i] != undefined)
			str += "<a href='javascript:;' id='" + g[i]['userid'] + "'>"
					+ g[i]['username'] + "</a>";
	}

	this.source.html(str);
	this.source.find('a').toggle(function() {
		$(this).addClass("ch");
	}, function() {
		$(this).removeClass("ch");
	});
}

User.prototype.clear = function() {
	this.source.html('');
	this.target.html('');
}

User.prototype.change = function(gid) {
	this.clear();
	this.fill(this.users[gid]);

}

User.prototype.filter = function() {

	var delete_groupid = [];
	var n = 0;
	// 屏蔽掉当前的组
	for ( var i in this.data_group_list) {
		// alert(this.data_group_list[i]['groupid']+"=="+this.userid_in_group);
		if (this.data_group_list[i]['groupid'] != this.current_groupid
			&& 	this.data_group_list[i]['grouptype']!=1 ) {
			this.group_list[i] = this.data_group_list[i];
		} else {
			delete_groupid[n] = this.data_group_list[i]['groupid'];
		}
	}
	

	for ( var gid in this.data_users ) {
		
		if($.inArray(gid,delete_groupid)!=-1) {
			continue;
		}
			
		
		for ( var i in this.data_users[gid]) {
			if ($.inArray(this.data_users[gid][i]['userid'],
					this.userid_in_group) === -1) {

				if (this.users[gid] == undefined) {
					this.users[gid] = {}
				}
				this.users[gid][i] = this.data_users[gid][i];
			}

		}
	}

}

User.prototype.getGroupUserid = function() {
	if (this.data_users[this.current_groupid] != undefined)
		for ( var i in this.data_users[this.current_groupid]) {
			this.userid_in_group
					.push(this.data_users[this.current_groupid][i]['userid']);
		}
}

User.prototype.submit = function(url) {
	var arrId = new Array();
	this.target.find('a').each(function() {
		var id = $(this).attr('id');
		if (id != '' && id != null && id != undefined) {
			arrId.push($(this).attr('id'))
		}
	});

	if (arrId.length < 1) {
		alert('请选择成员');
		return false;
	}

	var groupid = this.obj_pop.find('input[name=groupid]').val()
	if (groupid == '' || groupid == null || groupid == undefined) {
		alert('参数错误');
		return false;
	}

	var self = this;
	$.post(url, 'groupid=' + groupid + '&uids=' + arrId, function(data) {
		if (data.isSucceed != true) {
			self.fail(data);
		}else
		{
			self.success(data);
		}
		
	}, 'json');

}

User.prototype.success = function(data)
{
	window.location.reload();
}


User.prototype.fail = function(data)
{
	alert('添加失败');
	window.location.reload();
}
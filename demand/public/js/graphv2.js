
var containernew = {
	chart: {
		renderTo:'container1',
		defaultSeriesType: 'line'//图表类型,line 曲线图,
	},
	title: {
		text: '',
		x: -20 //center
	},
	subtitle: {
		text: '',
		x: -20
	},
	xAxis: {
		//type:'datetime'
		categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
	},
	yAxis: {
		title: {
			text: ''
		}
	},
	tooltip: {
		formatter: function() {
			return '<b>'+ this.series.name +'</b><br/>'+this.x +': '+  Math.round(this.y*100)/100 ;
		}
	},
	legend: {
		enabled:false,
		layout: 'vertical',
		align: 'right',
		verticalAlign: 'top',
		x: -10,
		y: 100
	},
	exporting: {
		enabled: false
	}
};


var container3  = {		
		chart: {
			renderTo: 'container1',
			type: 'column',
		},
		
		title: {
			text: '',
			x: -20 //center
		},
		subtitle: {
			text: '',
			x: -20
		},
		xAxis: {
			//type:'datetime'
			categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
		},
		yAxis: {
			title: {
				text: ''
			}
		},
		plotOptions: {
			pie: { shadow: false }
		},
		tooltip: {
			formatter: function() {
					return '<b>'+ this.series.name +'</b><br/>'+
					this.x +': '+  Math.round(this.y*100)/100  ;
			}
		},
		legend: {
			enabled:false,
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -10,
			y: 100
		},
		exporting: {
			enabled: false
		}
};
var container4  = {		
		chart: {
			renderTo: 'container1',
			type: 'column',
		},
		
		title: {
			text: '',
			x: -20 //center
		},
		subtitle: {
			text: '',
			x: -20
		},
		xAxis: {
			//type:'datetime'
			categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
		},
		yAxis: {
			title: {
				text:''
			},
			labels: {
				formatter: function() {
					return this.value*100 +'%';
				}
			}
		},
		plotOptions: {
			pie: { shadow: false }
		},
		tooltip: {
			formatter: function() {
					if(this.y==0 || this.y==null || this.y == '')this.y=0
					return '<b>'+ this.series.name +'</b><br/>'+
					this.x +': '+ Math.round(this.y*10000)/100 +'%';
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -10,
			y: 100
		},
		exporting: {
			enabled: false
		}
};

var containerpie = {
		
chart: {
	renderTo: 'container',//对应的模块ID
	plotBackgroundColor: null,//图表背景颜色,默认透明
	plotBorderWidth: null,//图表背景边框
	plotShadow: false//阴影，默认2像素
	},
	//title: 标题
	title: {
		text: '广告活动'
	},
	//tooltip: 浮层内容
	tooltip: {
		formatter: function() {
			//return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
			if(this.percentage==0 || this.percentage==null || this.percentage == '')this.percentage=0
			return '<b>'+ this.point.name +'</b>: '+
			parseInt(this.percentage*100)/100 +'%';
		}
	},
	/**
	 * legend: 数据类别框
	 * @ layout 排列方式，默认horizontal横向 vertical竖向
	 * @ verticalAlign 起始位置,默认top 顶部，bottom底部
	 * @ x,y 坐标偏移量
	 * @ backgroundColor 背景颜色
	 * @ borderColor 边框颜色
	 * @ borderWidth 边框厚度
	 */
	legend: {
		align:'center',
		x: 0,//坐标偏移量
		y: 14,
		floating: true
	},
	
	plotOptions: {
		//plotOptions: 图表类型
		//pie饼图、area区域图、scatter散点图、line曲线图一、spline曲线图二、series横向圆柱图、column圆柱图
		pie: {
			allowPointSelect: true,
			cursor: 'pointer',
			dataLabels: {
				enabled: true,//图表色块描述 开关
				color: '#000000',//图表色块描述 文字颜色
				connectorColor: '#000000',//图表色块描述 线条颜色
				formatter: function() {
					return '<b>'+ this.point.name +'</b>: '+ parseInt(this.percentage*100)/100 +'%';
				}
			},
			showInLegend: true
		}
	},
	series: [{
		type: 'pie',
		name: 'Browser share',
		data: [
			['ww1',45.0],
			{
				name: 'ww2',
				y:12.8,
				sliced: true,//默认选择区域，默认为 false
				selected: true
			},
			['ww3',8.5],
			['ww4',10],
			['ww5',30]
		]
	}]
}



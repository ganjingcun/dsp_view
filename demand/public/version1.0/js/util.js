/**
 * @description util.js
 * @创建人： 陈德昭
 * @date 2011-03-08
 */
var Utils = new Object();

/**
 * 去除字符串空格
 */
Utils.trim = function(text) {
    return text.replace(/(^\s*)|(\s*$)/g, "");
}

/**
 * 去除字符串左空格
 */
Utils.ltrim = function(text) {
    return text.replace(/(^\s*)/g, "");
}

/**
 * 去除字符串右空格
 */
Utils.rtrim = function(text) {
    return text.replace(/(\s*$)/g, "");
}

/**
 * 返回字符串的实际长度, 一个汉字算2个长度
 */
Utils.len = function(text) {
    return text.replace(/[^\x00-\xff]/g, "**").length;
}

/**
 * 是否是有用户名
 */
Utils.isUserName = function (str)
{
	szMsg="[#%&'\",;:=!^@]";
	var username=/^[a-zA-Z0-9][a-zA-Z0-9_]{4,15}$/;
	if(username.test(str)) {return true;} else {return false;}
	for(i=1;i<szMsg.length+1;i++){
	     if(t.indexOf(szMsg.substring(i-1,i))>-1){
	      //alertStr="请勿包含非法字符如[#_%&'\",;:=!^]";
	      return false;
	     }
	    }
	return true;
}

/**
 * 是否是有效邮箱
 */
Utils.isEmail = function(email) {
    var email_format = /^(\d|[a-zA-Z])+(-|\.|\w)*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
    return email_format.test(email);
}

/**
 * 是否是有效QQ
 */
Utils.isQQ = function(qq) {
    var qq_format = /^[1-9][0-9]{4,12}$/;
    return qq_format.test(qq);
}

/**
 * 是否是有效密码
 */
Utils.isPassword = function(str) {
	var pattern = /^[\w-]{6,16}$/;
    return pattern.test(str);
}

/**
 * 是否是中文字符
 */
Utils.isChinese = function(str) {
    var regexp = /^[\u4e00-\u9fa5]*$/g;
    return regexp.test(str);
}

/**
 * 是否是数字
 */
Utils.isDigit = function(str) {
    var regexp = /^[0-9]+$/;
    return regexp.test(str);
}

/**
 * 是否是手机
 */
Utils.isMobile = function (str)
{
   var msg= /^1[3|4|5|8]\d{9}$/;
   if(msg.test(mobile)){return true;}
   else{return false;}
}

/**
 * 是否是整数
 */
Utils.isInteger = function(str) {
    var regexp = /^(-|\+)?\d+$/;
    return regexp.test(str);
}

/**
 * 是否是小数
 */
Utils.isFloat = function(str) {
    var regexp = /^(-|\+)?\d+\.{0,1}\d+$/;
    return regexp.test(str);
}

/**
 * 是否是有效邮编
 */
Utils.isPostalCode = function(str) {
    var regexp = /(^[0-9]{6}$)/;
    return regexp.test(str);
}

/**
 * 是否是有效电话
 */
Utils.isPhone = function(str) {
    var regexp = /(^[0-9]{3,4}\-[0-9]{3,8}$)|(^[0-9]{3,8}$)|(^\([0-9]{3,4}\)[0-9]{3,8}$)/;
    return regexp.test(str);
}

/**
 * 是否是有效手机号
 */
Utils.isMobile = function(str) {
    var regexp = /(^0{0,1}1[0-9]{10}$)/;
    return regexp.test(str);
}

/*
*是否是有日期
*/
Utils.isDate = function(strValue) {
	//yyyy-mm-dd hh:mm:ss 的 /^(\d{4})\-(\d{2})\-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/ ;
	//yyyy-mm-ddde 的 /^(\d{4})\-(\d{2})\-(\d{2})$/
	var objRegExp = /^(\d{4})\-(\d{1,2})\-(\d{1,2})$/	
	if(!objRegExp.test(strValue))
		{
		return false;
		}
	else{
		var arrayDate = strValue.split("-");
		var intDay = parseInt(arrayDate[2],10);
		var intYear = parseInt(arrayDate[0],10);
		var intMonth = parseInt(arrayDate[1],10);
		if(intMonth > 12 || intMonth < 1) {
			return false;
		}
		var arrayLookup = { '1' : 31,'3' : 31, '4' : 30,'5' : 31,'6' : 30,'7' : 31,	'8' : 31,'9' : 30,'10' : 31,'11' : 30,'12' : 31}
		if(arrayLookup[parseInt(arrayDate[1])] != null) {
		if(intDay <= arrayLookup[parseInt(arrayDate[1])] && intDay != 0)
			return true;
		}
		if (intMonth-2 ==0) {
			var booLeapYear = (intYear % 4 == 0 && (intYear % 100 != 0 || intYear % 400 == 0));
			if( ((booLeapYear && intDay <= 29) || (!booLeapYear && intDay <=28)) && intDay !=0)
				return true;
			}
		}
	return false;
} 

/**
 * Cookie操作
 */
Utils.cookie = new Object();
Utils.cookie.domain = 'punchbox.org';
Utils.cookie.path = '/';
Utils.cookie.hours = 24;//默认1天

/**
 * 设置cookie
 */
Utils.cookie.setCookie = function(name,value,hours,domain,path) {
    hours = hours ? hours : Utils.cookie.hours;
    domain = domain ? domain : Utils.cookie.domain;
    path = path ? path : Utils.cookie.path;
    var exp  = new Date();
    exp.setTime(exp.getTime() + hours*3600000);
    document.cookie = name + "=" + escape(value) + ";expires=" + exp.toGMTString() + ";domain=" + domain + ";path=" + path;
}

/**
 * 取得cookie
 */
Utils.cookie.getCookie = function(name) {
    var arr = document.cookie.match(new RegExp("(^| )" + name + "=([^;]*)(;|$)"));
    if(arr != null) return unescape(arr[2]); return null;
}

/**
 * 删除cookie
 */
Utils.cookie.delCookie = function(name,domain,path) {
    domain = domain ? domain : Utils.cookie.domain;
    path = path ? path : Utils.cookie.path;
    var exp = new Date();
    exp.setTime(exp.getTime() - 3600000);
    document.cookie = name + "=0" + ";expires=" + exp.toGMTString() + ";domain=" + domain + ";path=" + path;
}




//------------ 按钮状态设置-----------------------------//
function change_submit(zt)
{ 
     if (zt == "true"){
		//document.forms['formUser'].elements['Submit1'].disabled = 'disabled';
    	 $("#Submit1").attr("disabled","disabled");
     }else {
		//document.forms['formUser'].elements['Submit1'].disabled = '';
    	 $("#Submit1").attr("disabled","");
     }
}

//------------ 按钮状态设置-----------------------------//
function showInfo(target,Infos){
	document.getElementById(target).innerHTML = Infos;
}


//--------------------邮箱检测-----------------------------//
function check_Email(Email){
		if(Utils.isEmail(Email.value)){
			showInfo('email_notice','<b class="icon_right"></b>')
		}else{
			showInfo('email_notice','<b class="icon_error"></b><span class="red">邮箱不正确！</span>')
		}
	}

//--------------------密码检测-----------------------------//
function check_password( password )
{
    if ( password.value.length < 6 )
    {
		showInfo("password_notice",'<b class="icon_error"></b><span class="red">密码不能少于6个字符。</span>');
    }
	else if(password.value.length > 30){
		showInfo("password_notice",'<b class="icon_error"></b><span class="red">密码不能多于30个字符。</span>');
		}
    else
    {
		showInfo("password_notice",'<b class="icon_right"></b>');
		change_submit("false");//允许提交按钮
    }
}

function check_conform_password(conform_password ){
    password = document.getElementById('txtpasswd').value;    
    if ( conform_password.value.length < 6 )
    {
		showInfo("conform_password_notice",'<b class="icon_error"></b><span class="red">密码不能少于6个字符。</span>'); //密码太短
    }else if ( conform_password.value!= password ){
			showInfo("conform_password_notice",'<b class="icon_error"></b><span class="red">密码不一致。</span>');//密码不一致
    	}else{   
		showInfo("conform_password_notice",'<b class="icon_right"></b>');
		change_submit("false");//允许提交按钮
    }
}


//* *--------------------检测密码强度-----------------------------* *//

function checkIntensity(pwd)
{
  var Mcolor = "#FFF",Lcolor = "#FFF",Hcolor = "#FFF";
  var m=0;

  var Modes = 0;
  for (i=0; i<pwd.length; i++)
  {
    var charType = 0;
    var t = pwd.charCodeAt(i);
    if (t>=48 && t <=57)
    {
      charType = 1;
    }
    else if (t>=65 && t <=90)
    {
      charType = 2;
    }
    else if (t>=97 && t <=122)
      charType = 4;
    else
      charType = 4;
    Modes |= charType;
  }

  for (i=0;i<4;i++)
  {
    if (Modes & 1) m++;
      Modes>>>=1;
  }

  if (pwd.length<=4)
  {
    m = 1;
  }

  switch(m)
  {
    case 1 :
      Lcolor = "2px solid red";
      Mcolor = Hcolor = "2px solid #DADADA";
    break;
    case 2 :
      Mcolor = "2px solid #f90";
      Lcolor = Hcolor = "2px solid #DADADA";
    break;
    case 3 :
      Hcolor = "2px solid #3c0";
      Lcolor = Mcolor = "2px solid #DADADA";
    break;
    case 4 :
      Hcolor = "2px solid #3c0";
      Lcolor = Mcolor = "2px solid #DADADA";
    break;
    default :
      Hcolor = Mcolor = Lcolor = "";
    break;
  }
  document.getElementById("pwd_l").style.borderBottom  = Lcolor;
  document.getElementById("pwd_m").style.borderBottom = Mcolor;
  document.getElementById("pwd_h").style.borderBottom   = Hcolor;

}

//--------------注册协议复选框状态检测---------------------//
function check_agreement(agreement,MagId){
  if ($("#"+agreement).attr("checked")!= "checked")
  {
	 showInfo(MagId,'<b class="icon_error"></b><span class="red">您没有接受协议</span>');
     change_submit("true");//禁用提交按
}
  else
  {
	showInfo(MagId,' ');
	change_submit("false");//允许提交按
	}
}
/**
 * *   组件定义
 * *
 * *
 * *
 * *
 * */

/* ==================== 日历排期 ===================== */

//日期格式化
Date.prototype.format = function(format)//.format("yyyy-MM-dd")
{
    var o =
    {
        "M+" : this.getMonth()+1, //month
        "d+" : this.getDate(),    //day
        "h+" : this.getHours(),   //hour
        "m+" : this.getMinutes(), //minute
        "s+" : this.getSeconds(), //second
        "q+" : Math.floor((this.getMonth()+3)/3),  //quarter
        "S" : this.getMilliseconds() //millisecond
    }
    if(/(y+)/.test(format))
        format=format.replace(RegExp.$1,(this.getFullYear()+"").substr(4 - RegExp.$1.length));
    for(var k in o)
        if(new RegExp("("+ k +")").test(format))
            format = format.replace(RegExp.$1,RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length));
    return format;
}


//----------------------日期表时间设置，今天SetDate(0)，昨天SetDate(-1)

function SetDate(day,date_box){
    if(!date_box){
        var date_box = $(".calendar").eq(0);
    }
    day = Number(day)
    var thisDate = new Date();
    var pastDate = new Date();
    if(day <= -1){
        thisDate.setDate(thisDate.getDate()-1);
        pastDate.setDate(pastDate.getDate()+day);
    }else{
        thisDate.setDate(thisDate.getDate());
        pastDate.setDate(pastDate.getDate()+day);
    }
    var dataStart =  thisDate.format("yyyy-MM-dd") //String(thisDate.getFullYear()+"-"+(thisDate.getMonth()+1)+"-"+thisDate.getDate());
    var dateEnd = pastDate.format("yyyy-MM-dd") //String(pastDate.getFullYear()+"-"+(pastDate.getMonth()+1)+"-"+pastDate.getDate());
    if(day <= -1){
        //过去时间
        $(date_box).DatePickerSetDate([dateEnd,dataStart],false);
        return [dateEnd,dataStart]
    }else{
        //未来时间
        $(date_box).DatePickerSetDate([dateEnd,dataStart],false);
        return [dateEnd,dataStart]
    }

}

/**
 * 定义公用的splash : 用户提交动作后相应的弹出层
 * @param options obj
 **/
function splash(options){

    var defaults = {
        str:'5秒后将跳转到首页',
        url:'/',
        setSec:5
    }
    var options = $.extend({}, defaults, options);
    var minSeconds = options.setSec*1000;

    $('body').append('<div class="splash_wrap" ><div class="splash">'+options.str+'</div></div>');
    $('.splash_wrap').fadeIn();
    $('.setSec').html(options.setSec);
    var win_width = $(window).width();
    var left = win_width / 2-$('.splash').width()/2;
    $('.splash').css({'left':left,'top':'10%'});

    setInterval(function(){
        if(options.setSec < 0){
            return;
        }else{
            $('.setSec').html(options.setSec);
        }
        options.setSec--;
    },1000)

    setTimeout(function(){
        $('.splash_wrap').fadeOut(300,function(){
            $('body .splash_wrap').remove();
            window.location.href=options.url;
        })
    },minSeconds)
}



/**
 * 设置浏览器事件
 **/
function auto_height(){
    var winHeight = $(window).height();
    var minHeight = winHeight - 97;
    var rightContentHeight = winHeight;
    var nowThirdHeight = $('.third.active').height();
    var thirdHeight = minHeight-274;
    var addApp = $('.add_app')

    if(!addApp.length>0){
        thirdHeight = minHeight-206;
    }

    $('#leftBar').css('height',minHeight);
    $('.black_mask').css('height',minHeight);
    $('#rightContent').height(rightContentHeight);
    if(nowThirdHeight>thirdHeight){
        $('.third.active').height(thirdHeight);
        return thirdHeight;
    }else{
        $('.third.active').height(nowThirdHeight);
        return nowThirdHeight;
    }
}




/**
 * *   调用部分
 * *
 * *
 * *
 * *
 * */

$(function() {
    /**
     * 定义不支持html5的placeholder的属性
     **/
    $('input, textarea').placeholder();
    auto_height();

    $(window).resize(function(){
        auto_height();
    })


    /**
     * 平台首页  插件焦点图片效果
     **/
    var screen_width = $('body').width();
    var left_pos;
    var w_width = $(window).width();
    $('#slides .inner').css('width',w_width); //banner图片父元素定义窗体的宽度

    if(screen_width<1000){
        left_pos = 50;
        $('#banner').width(1000);
    }else{
        left_pos = (screen_width - 1000)/2;
        $('#banner').width(screen_width);
    }
    $('#slides .caption').css({'left':left_pos,opacity:1}); //初始化banner的第一个文字为显示状态
    $(window).resize(function(){
        screen_width = $('body').width();
        if(screen_width<1000){
            left_pos = 0;
            $('#banner').width(1000);
        }else{
            left_pos = (screen_width - 1000)/2;
            $('#banner').width(screen_width);
        }
        $('#slides .caption').css({'left':left_pos,opacity:1});
    });

    $('#slides').slides({
        play: 5000,
        pause: 2500,
        hoverPause: true,
        slideSpeed: 500,
        animationStart: function(){
            $('.caption').animate({
                left:left_pos+120,opacity:0
            },0);
        },
        animationComplete: function(){
            $('.caption').animate({
                left:left_pos,opacity:1
            },500);
        }
    });


    /**
     *  首页主页面 开发者主页面 广告主主页面 成功案例的hover效果
     **/
    $('.suc_examples .tab_box ul li').hoverZoom({
        zoom: 25,
        speed: 400
    });


    /**
     *  开发者主页面 sdk下载box效果
     **/
    $('.sdk_box').bind('mouseenter',function(){
        $(this).find('.details').animate({top:0},200);
        $(this).find('.front').animate({top:'-182px'},200);
    }).bind('mouseleave',function(){
        $(this).find('.front').stop(true,false).animate({top:0},200);
        $(this).find('.details').stop(true,false).animate({top:'182px'},200);
    })


    /**
     *  定义全局的tab选项卡 上传ajax tab
     **/
    $('.tab_item').each(function(i){
        $('.tab_item').eq(i).on('click','li',function(){
            var box_class = $(this).data('tab');
            $(this).addClass('active').siblings('li').removeClass('active');
            $('.'+box_class).parent().find('.tab_box').hide();
            $('.'+box_class).show();
            auto_height();
        });
    })
    $(document).on('click','.ajax_tab_item li',function(){
        var box_class = $(this).data('tab');
        $(this).addClass('active').siblings('li').removeClass('active');
        $('.'+box_class).parent().find('.tab_box').hide();
        $('.'+box_class).show();
    });


    /**
     *  平台首页 分类效果
     **/
    $('.white_box').hover(function(){
        $(this).find('.open_classify').addClass('active');
    },function(){
        $(this).find('.open_classify').removeClass('active');
    })


    /**
     *  跟随导航定位的背景效果 top_nav menu_nav
     **/
    $(".top_nav").lavaLamp({
        speed:300
    });

    $(".menu_nav").lavaLamp({
        speed:200
    });


    /**
     *  开发者 左侧导航菜单
     **/
    $('.first .a_status').bind('click',function(){
        $('.first .open').removeClass('active');
        $(this).parent('.open').addClass('active');
        if($(this).parent('.open').find('ul').length){
            $(this).next('.second').show();
        }
        auto_height();
    });

    $('.app_title .second .title').click(function(){
        $('.app_title .second .third').removeClass('active');
        $('.app_title .second .title .heng').text('+');

        $(this).next('.third').addClass('active');
        if($(this).find('.heng').length){
            $(this).find('.heng').text('-');
        }
        auto_height();
    })



    $('.app_title .second .third').each(function(){
        if($(this).find('li').length){
            var a_on = $('.app_title .second .third li a.on');
            if(a_on){
                $('.app_title .second .third').removeClass('active');
                $('.app_title .second .title .heng').text('+');
                a_on.parents('.third').prev('.title').find('.heng').text('-');
                a_on.parents('.third').addClass('active');
                auto_height();
            }
        }else{
            $(this).siblings('.title').find('.heng').text('+');
        }
    });



    /**
     *  开发者 顶部下拉菜单效果
     **/
    $('#showDropMenu').bind('mouseenter',function(){
        $('.listLink').stop(true,false).fadeIn();
    });
    $('#showDropMenu').bind('mouseleave',function(){
        $('.listLink').hide ();
    });

    /**
     *
     * SDK下载切换效果
     *
     * */

    $('#sdkSlides .inner').width($('#sdkSlides').width());
    $('#sdkSlides').slides({
        play: 5000,
        pause: 2500,
        hoverPause: true,
        slideSpeed: 500

    });

});

var xPos = 300;
var yPos = 200;
var step = 1;
var delay = 30;
var Hoffset = 0;
var Woffset = 0;
var yon = 0;
var xon = 0;
var pause = true;
var interval;

$(function(){
    var img1 = $('#img1');
    img1.css('top',yPos);
    img1.mouseover(function(){
        pause_resume();
    });
    img1.mouseout(function(){
        pause_resume();
    });
    function start()
    {
        img1.css('visibility','visible');
        interval = setInterval('changePos()', delay, img1);
    }
    function pause_resume()
    {
        if(pause)
        {
            clearInterval(interval);
            pause = false;}
        else
        {
            interval = setInterval('changePos()',delay);
            pause = true;
        }
    }
    //start();
});

function changePos()
{
    var width = $(window).width();
    var height = $(window).height();
    Hoffset = $('#img1').height();
    Woffset = $('#img1').width();
    var lef = xPos + document.body.scrollLeft;
    var rig = yPos + document.body.scrollTop;
    $('#img1').css('left',lef) ;
    $('#img1').css('top',rig) ;
    if (yon){
        yPos = yPos + step;
    }else{
        yPos = yPos - step;
    }
    if (yPos < 0){
        yon = 1;
        yPos = 0;
    }
    if (yPos >= (height - Hoffset)){
        yon = 0;yPos = (height - Hoffset);
    }
    if (xon){
        xPos = xPos + step;
    }else {
        xPos = xPos - step;
    }
    if (xPos < 0){
        xon = 1;
        xPos = 0;
    }
    if (xPos >= (width - Woffset)){
        xon = 0;
        xPos = (width - Woffset);
    }
}




<?php
require_once(PLUGINS_DIR.'thrift/Thrift.php');
require_once(PLUGINS_DIR.'thrift/transport/TSocket.php' );
require_once(PLUGINS_DIR.'thrift/transport/TSocketPool.php' );
require_once(PLUGINS_DIR.'thrift/transport/TFramedTransport.php' );
require_once(PLUGINS_DIR.'thrift/transport/TBufferedTransport.php' );
require_once(PLUGINS_DIR.'thrift/protocol/TBinaryProtocol.php' );
include_once(PLUGINS_DIR.'thrift/scribe/fb303_types.php');
include_once(PLUGINS_DIR.'thrift/scribe/FacebookService.php');
include_once(PLUGINS_DIR.'thrift/scribe/scribe.php');
include_once(PLUGINS_DIR.'thrift/scribe/scribe_types.php');


class Scribe
{
    public $host = array('localhost');
    public $port = array(1463);
    public $ScribeClient = false;
    public $debug = 0;
    public $transport = null;

    public function __construct($host = array("localhost"),$port = array(1463)){
        $this->host = $host;
        $this->port = $port;
        $this->ScribeClient = $this->connect();
    }

    public function connect(){
        try{
            $socket = new TSocketPool($this->host,$this->port);
            $socket->setDebug($this->debug);
            $socket->setSendTimeout(1000);
            $socket->setRecvTimeout(1000);
            $socket->setNumRetries(1);
            $socket->setRandomize(true);
            $socket->setAlwaysTryLast(true);

            $this->transport = new TFramedTransport($socket);
            $protocol = new TBinaryProtocol($this->transport);
            $ScribeClient = new scribeClient($protocol);
            $this->transport->open();
        }catch(Exception $x){
            exit("connect scribe server error \n or Scribe Server is stop\n");
        }
        return $ScribeClient;
    }

    public function Log($message){
        if(empty($this->ScribeClient)){
            $this->connect();
        }
        try{
            $result = $this->ScribeClient->Log($message);
            if($result != ResultCode::OK){
                return false;
            }
            return $result;
        }catch(Exception $x){
            return false;
        }
    }

    public function close(){
        if($this->transport != null){
            $this->transport->close();
        }
    }

    public function __destruct(){
        $this->close();    
    }
}


<?php if (!defined('IS_SYSTEM')) exit('access deny.');
/**
 * 控制器基类
 * Enter description here ...
 * @author Falcon.C(falcom520@gmail.com)
 *
 */
class Controller
{

    public $smarty = null;
	/**
	 * 构造函数
	 * Enter description here ...
	 */
	public function __construct(){
	    
	}

	/**
	 * 魔法方法set，在给一个未定义变量赋值时调用
	 * Enter description here ...
	 * @param unknown_type $name
	 * @param unknown_type $value
	 */
	public function __set($name,$value){
		if(property_exists($this, $name))
			$this->name = $value;
	}

	/**
	 * 魔法方法get，在给一个未定义的变量取值时调用
	 * Enter description here ...
	 * @param unknown_type $name
	 */
	public function __get($name){
		return isset($this->name) ? $this->name : null;
	}


	public function getName(){
		return CURR_MODULE.'.'.CURR_ACTION;
	}


    protected function assign($name,$value){
        $this->getSmartyInstance();
        $this->smarty->assign($name,$value);
    }
    
    protected function get_siteurl(){
        $http_host = $_SERVER['HTTP_HOST'];
        $script_name = dirname($_SERVER['SCRIPT_NAME']);
        $siteurl = "http://".$http_host.$script_name;
        return $siteurl;
    }

    protected function redirect($url){
        header("Location:".$url);
    }


    protected function display($tmpname){
        $this->getSmartyInstance();
        $this->smarty->display($tmpname);
    }
	
    protected function getSmartyInstance(){
        if(is_null($this->smarty) || empty($this->smarty)){
            require_once(PLUGINS_DIR.'smarty'.D_S.'Smarty.class.php');
            $this->smarty = new Smarty;
			$this->smarty->template_dir = ROOT_DIR."templates";
			$this->smarty->compile_dir = DATA_DIR."templates_c";
			$this->smarty->cache_dir = DATA_DIR."cache";
			$this->smarty->config_dir = CONFIG_DIR."smarty";
			$this->smarty->caching = false;
			$this->smarty->cache_lifetime = 0;
        }

    }


    /**
     * 将数组转化为对象
     * @param array $data
     * @return StdClass|unknown|boolean
     */
    protected function toObj($data){
    	if(is_array($data)){
    		return (object) $data;
    	}elseif (is_object($data)){
    		return $data;
    	}
    	return false;
    }
    
    /**
     * 将对象转化为数组
     * @param object $data
     * @return array|unknown|boolean
     */
    protected function toArr($data){
    	if (is_object($data)) {
    		return (array) $data;
    	}elseif (is_array($data)){
    		return $data;
    	}
    	return false;
    }


	/**
	 * 调式方法，共打印数组，对象，字符串到调试文件
	 * Enter description here ...
	 * @param unknown_type $data
	 */
	protected function dump($data)
	{
		$content = print_r($data,TRUE);
		file_put_contents(DATA_DIR.'debug.txt', $content,FILE_APPEND);
	}


}

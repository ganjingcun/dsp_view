<?php if (!defined('IS_SYSTEM')) exit('access deny.');
/**
 * 数据模型基类
 * Enter description here ...
 * @author Falcon.C(falcom520@gmail.com)
 *
 */
class Model
{
	public $_tname = '';//表名
	public $_dbname = '';
	public $_model = '';


	public function __construct($model = ''){
		
	    if (!empty($model)){
    	    global $db_config;
    	    $this->_dbname = $db_config[0]['db_name'];
    	    $this->_tname = $db_config[0]['db_tpre'].strtolower($model);
	    }
	}
	/**
	 * 实例化一个数据库连接类
	 * Enter description here ...
	 * @param int $flag	选择数据库服务器编号，默认为0
	 */
	public function getInstance($flag = 0){
		global $db_config;
		$config = $db_config[$flag];
		$this->_dbname = $config['db_name'];
		$this->_dbhost = $config['db_host'].':'.$config['db_port'];

		$db = MySQL::getInstance($config);
		return $db;

	}

	public function getTable($_tname = '',$_dbname = ''){
		return '`'.(empty($_dbname) ? $this->_dbname : $_dbname).'`.`'.(empty($_tname) ? $this->_tname : $_tname).'`';
	}

	/**
	 * 快捷添加单条数据的方法
	 * Enter description here ...
	 * @param string $table   通过getTable()返回值
	 * @param array $data	添加数据的数组
	 * @param enum $type  (INSERT|REPLACE)
	 */
	public function add($data,$flag = 0,$table = '',$type = 'INSERT'){

		if(empty($data) || !is_array($data)) return false;
		$field = '';
		foreach ($data as $k=>$v){
			$field .= '`'.$k."` = '".maddslashes($v)."',";
		}
		$field = rtrim($field,',');
		if(empty($field)) return false;

		$dao = $this->getInstance($flag);//实例化一个数据库连接对象
		if (empty($table)) $table = $this->getTable();
		if($type == 'INSERT'){
			$sql = "insert into ".$table." set ";
		}else{
			$sql = "replace into ".$table." set ";
		}
		$sql .= $field;
		
		if($dao->query($sql)){
			return $dao->lastId();
		}
		return false;
	}


	/**
	 * 批量添加数据到数据库，（只能对单独的一个表批量添加数据）
	 * Enter description here ...
	 * @param array $data
	 * @param int $flag
	 * @param string $table
	 * @param enum $type (INSERT|REPLACE)
	 */
	public function addBatch($data,$flag = 0,$table = '',$type = 'INSERT'){
		if (empty($data) || !is_array($data)) return false;

		$field = $value = '';
		$i = 0;
		foreach ($data as $k=>$v){
			if(!is_array($v)) continue;
			$val = '';
			foreach ($v as $k1=>$v1){
				if($i == 0){
					$field .= $k1.",";
				}
				$val .= "'".$v1."',";
			}
			$val = rtrim($val,',');
			$value .= '('.$val.'),';
			$i++;
		}
		$field = rtrim($field,',');
		$field = '('.$field.')';
		$value = rtrim($value,',');

		$dao = $this->getInstance($flag);
		if(empty($table)) $table = $this->getTable();

		if($type == 'INSERT'){
			$sql = "insert into ".$table." ".$field." values ".$value;
		}else{
			$sql = "replace into ".$table." ".$field." values ".$value;
		}
		if($dao->query($sql)){
			return $dao->affectedRows();
		}
		return false;
	}

	/**
	 * 更新指定条件的数据
	 * Enter description here ...
	 * @param array $condition 更新指定字段的条件  array('uid'=>23,'status'=>1)
	 * @param array $data	更新的数据字段和值  array('lv'=>5,'sex'=>1)
	 * @param int $flag	分表的规则编号
	 * @param string $table   `dbname_01`.`user`
	 * @return 返回值>=1则正确，返回0则更新字段值没有改变，返回-1则指定更新记录不存在
	 */
	public function save($condition,$data,$flag = 0,$table = ''){
		if (empty($condition) || !is_array($condition) || empty($data) || !is_array($data)) return false;

		$cond = '';
		foreach($condition as $k=>$v){
			$cond .= " `".$k."` = '".maddslashes($v)."' and";
		}
		$cond = rtrim($cond,'and');
		$field = '';
		foreach ($data as $k1=>$v1){
			$field .= "`".$k1."` = '".$v1."',";
		}
		$field = rtrim($field,",");

		$dao = $this->getInstance($flag);

		if(empty($table)) $table = $this->getTable();

		$sql = "update ".$table." set ".$field." where ".$cond."  limit 100";
		if ($dao->query($sql)) return $dao->affectedRows();

		return false;
	}



	/**
	 * 获取指定条件的一条记录
	 * Enter description here ...
	 * @param array $condition
	 * @param string $field 默认为全部字段
	 * @param int $flag  指定连接数据库的标识
	 * @param string $table  指定的数据库和表的名字
	 * @return array('uid'=>1,'name'=>'gameover','sex'=>'F') ....
	 */
	public function find($condition,$field = "*",$flag = 0,$table = ''){
		
		if (empty($condition) || !is_array($condition)) return false;

		$cond = '1';
		foreach ($condition as $k=>$v){
			$cond .= " and `".$k."` = '".maddslashes($v)."'";
		}

		$dao = $this->getInstance($flag);
		if (empty($table)) $table = $this->getTable();

		$sql = "select ".$field." from ".$table." where ".$cond." limit 1";
		
		$rs = $dao->query($sql);
		if($rs){
			return $dao->fetch_one($rs);
		}
		return false;
	}



	/**
	 * 批量获取指定条件的数据
	 * Enter description here ...
	 * @param array $condition
	 * @param string $field
	 * @param int $flag
	 * @param string $table
	 */
	public function findAll($condition = array(),$field='*',$order = '',$offset = 0,$limit = 1000,$flag = 0,$table = ''){

		$cond = '1';
		if (!empty($condition) && is_array($condition)){
			foreach ($condition as $k=>$v){
				$cond .= " and `".$k."` = '".maddslashes($v)."' ";
			}
		}
		$dao = $this->getInstance($flag);

		if(empty($table)) $table = $this->getTable();

		$_order = '';
		if (!empty($order)){
			$_order = " order by ".$order;
		} 

		$limit = " limit ".$offset.",".$limit;
		$sql = "select ".$field." from ".$table." where ".$cond.' '.$_order.' '.$limit;
		
		$rs = $dao->query($sql);
		if($rs){
			return $dao->fetch_array($rs);
		}
		return false;
	}

	/**
	 * 删除指定条件的数据
	 * Enter description here ...
	 * @param array $condition
	 * @param int $limit
	 * @param int $flag
	 * @param string $table
	 */
	public function delete($condition,$limit = 100,$flag = 0,$table = ''){
		if (empty($condition) || !is_array($condition)) return false;

		$cond = "";
		foreach($condition as $k=>$v){
			$cond .= " `$k` = '".maddslashes($v)."' and";
		}
		$cond = rtrim($cond,"and");
		$dao = $this->getInstance($flag);
		if (empty($table)) $table = $this->getTable();

		$sql = "delete from ".$table." where ".$cond." limit ".$limit;
		if ($dao->query($sql)){
			return $dao->affectedRows($sql);
		}
		return false;
	}


	/**
	 * 递增指定条件的数据字段的值
	 * Enter description here ...
	 * @param array $condition
	 * @param array $data
	 * @param int $flag
	 * @param string $table
	 */
	public function Inc($condition,$data,$flag = 0,$table = ''){
		if (empty($data) || !is_array($data) || empty($condition) || !is_array($condition)) return false;

		$field = $cond = '';
		foreach ($data as $k=>$v){
			$field .= "`$k` = $k+$v ,";
		}
		$field = rtrim($field,",");
		foreach ($condition as $k1=>$v1){
			$cond .= " `$k1` = '".$v1."' and";
		}
		$cond = rtrim($cond,"and");

		$dao = $this->getInstance($flag);
		if (empty($table)) $table = $this->getTable();

		$sql = "update ".$table." set ".$field." where ".$cond." limit 1";
		if ($dao->query($sql)){
			return $dao->affectedRows();
		}
		return false;
	}

	/**
	 * 递减指定条件的数据字段
	 * Enter description here ...
	 * @param array $condition
	 * @param array $data
	 * @param int $flag
	 * @param string $table
	 */
	public function Dec($condition,$data,$flag = 0,$table = ''){
		if (empty($data) || !is_array($data) || empty($condition) || !is_array($condition)) return false;

		$field = $cond = '';
		foreach ($data as $k=>$v){
			$field .= "`$k` = $k-$v ,";
		}
		$field = rtrim($field,",");
		foreach ($condition as $k1=>$v1){
			$cond .= " `$k1` = '".$v1."' and";
		}
		$cond = rtrim($cond,"and");

		$dao = $this->getInstance($flag);
		if (empty($table)) $table = $this->getTable();

		$sql = "update ".$table." set ".$field." where ".$cond." limit 1";
		if ($dao->query($sql)){
			return $dao->affectedRows();
		}
		return false;
	}

	/**
	 * 统计指定条件的表的记录数
	 * Enter description here ...
	 * @param array $condition
	 * @param int $flag
	 * @param string $table
	 */
	public function getCount($condition = array(),$flag = 0,$table = ''){
		$cond = " 1 ";
		if (!empty($condition)) {
			foreach ($condition as $k=>$v){
				$cond .= " and `$k` = '".$v."'";
			}
		}
		$dao = $this->getInstance($flag);
		if (empty($table)) $table = $this->getTable();
		$sql = "select count(*) as num from ".$table." where ".$cond;
		$rs = $dao->query($sql);
		if ($rs){
			$data = $dao->fetch_one($rs);
			if (empty($data)) return 0;
			return $data['num'];
		}
		return 0;
	}

	/**
	 * 条件为or关系的查询
	 * Enter description here ...
	 * @param array $condition  //		array( array( 'user_id'=>$user_id,'relation'=>1 ),array( 'friend_id'=>$user_id,'relation'=>3 ) );
	 * @param array $data
	 * @param int $flag
	 * @param string $table
	 */
	public function queryOr($condition,$flag = 0,$field='*',$order = '',$offset = 0,$limit = 1000,$table = ''){		
		if (empty($condition) || !is_array($condition)) {
			return false;
		}
		$cond = '1';
		foreach ($condition as $k=>$v){
			if( is_array( $v ) ){
				$cond .= ' or ( ';
						foreach( $v as $kk=>$vv ){
						if( $kk != 0 ){
						$cond .= ' and ';
						}
						$cond .= "`".$k."` = '".maddslashes($v)."' ";
						}
						$cond .= ' ) ';
			}
			else{
				$cond .= " or `".$k."` = '".maddslashes($v)."' ";
			}
		}		
		$dao = $this->getInstance($flag);
		if(empty($table)){
			$table = $this->getTable();
		}
		$_order = '';
		if (!empty($order)){
			$_order = $order;
		}	
		$limit = " limit ".$offset.",".$limit;
		$sql = "select ".$field." from ".$table." where ".$cond.' '.$_order.' '.$limit;
		$rs = $dao->query($sql);
		if($rs){
			return $dao->fetch_array($rs);
		}
		return false;		
	}

	/**
	 * 通过条件获取，其中有范围 IN 条件
	 * Enter description here ...
	 * @param string $range    //字符型需传入数组拼sql语句    array( 'buildingtype'=>array( 'type'=>'int','scope'=>'2,8,9' ) ); 
	 * @param string $field
	 * @param array $condition 
	 * @param string $order
	 * @param int $offset
	 * @param int $limit
	 */
	public function QuerybyRangeAndCon( $range,$field = "*",$flag = 0,$condition = array(),$order = '',$offset = 0,$limit = 100,$table = "")
	{
		if ( empty( $range ) || !is_array( $range ) ) {
			return false;
		}		
		$cond = '1';
		if ( is_array( $condition ) && !empty( $condition ) ) {
			foreach ( $condition as $k=>$v ){
				$cond .= " and `".$k."` = '".maddslashes($v)."' ";
			}
		}		
		if( is_array( $range ) && !empty( $range )) {
			foreach( $range as $k=>$v ){
				if( is_array( $v ) ){
					$cond .= ' and ';
					$scope = $v['scope'];
					if( $v['type'] == 'int' ){
						$cond .= " `".$k."` in (".$scope.")";
					}
					elseif( $v['type'] == 'char' ){
						$cond .= " `".$k."` in (";
						$scope_array = explode( ',',$scope );
						foreach( $scope_array as $kk=>$vv ){
							if( $kk == 0 ){
								$cond .= "'".maddslashes( $vv )."' ";
							}
							else{
								$cond .= ",'".maddslashes( $vv )."' ";
							}
						}
						$cond .= " )";
					}
				}
			}
		}			
		$dao = $this->getInstance( $flag );	
		if( empty($table ) ){
			$table = $this->getTable();
		}	
		$_order = '';
		if (!empty($order)){
			$_order = $order;
		}	
		$limit = " limit ".$offset.",".$limit;
		$sql = "select ".$field." from ".$table." where ".$cond.' '.$_order.' '.$limit;
		$rs = $dao->query( $sql) ;
		if( $rs ){
			return $dao->fetch_array($rs);
		}
		return false;
	}


	/**
	 * 魔法回调方法
	 * Enter description here ...
	 * @param string $method
	 * @param array $params
	 */
    public function __call($method,$params){
        $method = strtolower($method);
        if ($method === 'query'){//实现空Model执行SQL查询,支持跨库查询。使用方法 $this->query($sql,$flag = 0);
            $flag = isset($params[1]) ? $params[1] : 0;
            $dao = $this->getInstance($flag);
            return $dao->fetch_array($dao->query($params[0],$flag));
        }elseif (in_array($method, array('sum','count','max','min','avg'),true)){//统计查询快捷方式 使用方法 $this->avg($field,$condition=array(),$flag = 0,$table = '');
            $field = $params[0];
            $condition = $params[1] ? $params[1] : '';
            $flag = isset($params[2]) && !empty($params[2]) ? $params[2] : 0;
            $table = isset($params[3]) && !empty($params[3]) ? $params[3] : '';
            $dao = $this->getInstance($flag);
            $cond = '1 ';
            if (!empty($condition) && is_array($condition)){
                foreach ($condition as $k=>$v){
                    $cond .= " and `$k` = '".maddslashes($v)."' ";
                }
            }
            if (empty($table))
                $table = $this->getTable();
            else 
                $table = $this->getTable($table);
            $sql = "select $method(`$field`) as num from ".$table." where ".$cond;
            $rs = $dao->query($sql);
            if ($rs){
                $data = $dao->fetch_one($rs);
                if (empty($data)) return 0;
                
                return $data['num'];
            }
            return 0;
        }else{
            exit(get_class($this).'->'.$method.'() is not defined.');
        }
    }


}


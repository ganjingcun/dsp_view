<?php if (!defined('IS_SYSTEM')) exit('access deny.');
/**
 * Memcache缓存接口
 * Enter description here ...
 * @author Falcon.C
 *
 */
defined('USE_MEMCACHE') ? '' : define('USE_MEMCACHE',true);
class Mem
{
	
	public static function getInstance(){
	    if (USE_MEMCACHE == false) return false;
		static $_mem = NULL;
		if(is_null($_mem)){
			global $mem_config;
			$_mem = new Memcache();
			foreach ($mem_config as $v){
				$_mem->addServer($v['host'],$v['port'],true,$v['weight'],15);
			}
		}
		return $_mem;
	}
	
	public static function get($key){
	    if (USE_MEMCACHE === false) return false;
		if(empty($key)) return false;
		return self::getInstance()->get($key);
	}
	
	public static function set($key,$value,$expire = 3600){
	    if (USE_MEMCACHE == false) return false;
		if(empty($key) || empty($value)) return false;
		
		return self::getInstance()->set($key,$value,0,$expire);
	}
	
	public static function delete($key){
	    if (USE_MEMCACHE == false) return false;
		if(empty($key)) return false;
		
		return self::getInstance()->delete($key);
	}
	
	
	public static function flush(){
	    if (USE_MEMCACHE == false) return false;
		return self::getInstance()->flush();
	}
	
	public static function inc($key,$offset=1){
	    if (USE_MEMCACHE == false) return false;
		if(empty($key)) return false;
		
		return self::getInstance()->increment($key,$offset);
	}
	
	public static function dec($key,$offset=1){
	    if (USE_MEMCACHE == false) return false;
		if(empty($key)) return false;
		
		return self::getInstance()->decrement($key,$offset);
	}
	
	public function getExtendedStats(){
	    if (USE_MEMCACHE == false) return false;
		return self::getInstance()->getExtendedStats();
	}
	
}
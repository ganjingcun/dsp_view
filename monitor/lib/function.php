<?php
/**
 * 对象自动加载魔法方法
 * Enter description here ...
 * @param string $classname
 */
function __autoload($classname){
    if(strtolower(substr($classname, -10,10)) === 'controller'){
        $classname = substr($classname, 0, -10);
        $filename = CONTROLLER_DIR.$classname.".php";
        if (file_exists($filename)){
            include_once $filename;
        }else{
            exit($classname.' is not exists.');
        }
    }elseif (strtolower(substr($classname, -5,5)) == 'model'){
        $classname = substr($classname, 0,-5);
        $filename = MODEL_DIR.$classname.".model.php";
        if (file_exists($filename)){
            include_once $filename;
        }else{
            exit($classname.' is not exists.');
        }
    }
}


/**
 * 获取url返回值，curl方法
 *
 * @param mixed $url 请求地址
 * @param int $timeout 超时时间
 * @param array $header HTTP头信息
 * @access public
 * @return void
 */
function curlGet($url, $timeout = 3, $header = array())
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    $ret = curl_exec($ch);
    curl_close($ch);
    return $ret;
}

// 去除代码中的空白和注释
function strip_whitespace($content) {
    $stripStr = '';
    //分析php源码
    $tokens =   token_get_all ($content);
    $last_space = false;
    for ($i = 0, $j = count ($tokens); $i < $j; $i++)
    {
        if (is_string ($tokens[$i]))
        {
            $last_space = false;
            $stripStr .= $tokens[$i];
        }
        else
        {
            switch ($tokens[$i][0])
            {
                //过滤各种PHP注释
                case T_COMMENT:
                case T_DOC_COMMENT:
                    break;
                    //过滤空格
                case T_WHITESPACE:
                    if (!$last_space)
                    {
                        $stripStr .= ' ';
                        $last_space = true;
                    }
                    break;
                default:
                    $last_space = false;
                    $stripStr .= $tokens[$i][1];
            }
        }
    }
    return $stripStr;
}

/**
 * 实例化一个Model
 * Enter description here ...
 * @param unknown_type $model
 */
function D($model = ''){
    static $_model_instance = array();

    if(empty($model))
    return new Model();

    $indentify = md5($model);

    $model_file = MODEL_DIR.$model.'.model.php';

    if(isset($_model_instance[$indentify]))
    return $_model_instance[$indentify];

    if(file_exists($model_file)){
        include_once $model_file;
        //$_model = ucfirst($model).'Model'; 暂时去掉此约束
        $_model = $model.'Model';
        $_model_instance[$indentify] = new $_model;
        return $_model_instance[$indentify];
    }else{
        return new Model($model);
    }
    exit('the '.$model.' model is not exists.');
}

/**
 * 实例化一个RedisModel
 * wf
 * @param unknown_type $model
 */
function Dredis($model = '',$port=''){
    static $_model_instance = array();

    if(empty($model))
    return new Model();

    $indentify = md5($model);

    $model_file = MODEL_DIR.$model.'.model.php';

    if(isset($_model_instance[$indentify]))
    return $_model_instance[$indentify];

    if(file_exists($model_file)){
        include_once $model_file;
        //$_model = ucfirst($model).'Model'; 暂时去掉此约束
        $_model = $model.'Model';
        $_model_instance[$indentify] = new $_model($port);
        return $_model_instance[$indentify];
    }else{
        return new Model($model);
    }
    exit('the '.$model.' model is not exists.');
}

/**
 * 字符串特殊字符转义函数
 * Enter description here ...
 * @param unknown_type $string
 */
function maddslashes($string){
    !defined('MAGIC_QUOTES_GPC') && define('MAGIC_QUOTES_GPC', get_magic_quotes_gpc());
    if(!MAGIC_QUOTES_GPC) {
        if(is_array($string)) {
            foreach($string as $key => $val) {
                $string[$key] = maddslashes($val);
            }
        } else {
            $string = addslashes($string);
        }
    }
    return $string;
}

/**
 * 变量类型转换
 * Enter description here ...
 * @param mixed $data
 * @param string $type  string|bool|int|float|double
 */
function convertType($data,$type = "string"){
    if(is_array($data)) {
        foreach($data as $key => $val) {
            $data[$key] = convertType($val,$type);
        }
    } else {
        switch ($type){
            case "string":
                $data = (string) $data;break;
            case "boolean":
            case "bool":
                $data = (boolean) $data;break;
            case "array":
                $data = (array) $data;break;
            case "object":
                $data = (object) $data;break;
            case "integer":
            case "int":
                $data = (integer) $data;break;
            case "unset":
            default:
                $data = (unset) $data;break;
        }
    }
    return $data;
}




/**
 * 随机字符串
 */
function randStr($len)
{
    $chars='0123456789abcdefghijklmnopqrstuvwxyz'; // characters to build the password from
    $string="";
    for($i=$len;$i>0;$i--)
    {
        $position=rand()%strlen($chars);
        $string.=substr($chars,$position,1);
    }
    return $string;
}


// 获取客户端IP地址
function get_client_ip(){
    if (getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"), "unknown"))
    $ip = getenv("HTTP_CLIENT_IP");
    else if (getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown"))
    $ip = getenv("HTTP_X_FORWARDED_FOR");
    else if (getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown"))
    $ip = getenv("REMOTE_ADDR");
    else if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown"))
    $ip = $_SERVER['REMOTE_ADDR'];
    else
    $ip = "unknown";
    return($ip);
}



function getIpLong(){
    $ip = get_client_ip();
    if ($ip != 'unknown'){
        return sprintf("%.u",ip2long($ip));
    }
    return 0;
}


/**
 * 检测email是否合法
 * Enter description here ...
 * @param unknown_type $email
 */
function check_email($email){
    //$arr = filter_var($email, FILTER_VALIDATE_EMAIL);
    $arr = preg_match('/\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/',$email);
    if(empty($arr)){
        return false;
    }
    return true;
}

/**
 * 是否为一个合法的url
 * @param string $url
 * @return boolean
 */
function check_url($url){
    if (filter_var($url, FILTER_VALIDATE_URL )) {
        return true;
    } else {
        return false;
    }
}

/**
 * 检测用户名是否已字母开始，是否只包含字母数字和下划线,最短3位，最长20位
 * Enter description here ...
 * @param string $username
 */
function check_username($username){
    $pattern = '/^[\x80-\xffa-zA-z0-9][\x80-\xffa-zA-Z0-9-_:]{2,42}$/';
    if (!preg_match($pattern, $username)){
        return false;
    }
    return true;
}

/**
 * 发送邮件的方法
 * Enter description here ...
 * @param string $mail
 * @param array $data   $data = array('title'=>'邮件标题','content'=>'邮件正文');
 */
function send_mail($mail,$data){

    $smtpserver 	= 	SENDMAIL_STMP;//SMTP服务器
    $smtpserverport =	25;//SMTP服务器端口
    $smtpusermail 	= 	SENDMAIL_USERMAIL;//SMTP服务器的用户邮箱
    $smtpuser 		= 	SENDMAIL_USERNAME;//SMTP服务器的用户帐号
    $smtppass 		= 	SENDMAIL_PASSWORD;//SMTP服务器的用户密码

    $smtpemailto 	= 	$mail;//发送给谁
    $mailsubject 	= 	$data['title'];//邮件主题
    $mailtime		=	date("Y-m-d H:i:s");

    $utfmailbody	=	$data['content'];//转换邮件编码
    $mailtype 		= 	"HTML";//邮件格式（HTML/TXT）,TXT为文本邮件

    include(LIB_DIR."Sendmail.class.php");//发送邮件类
    $smtp = new smtp($smtpserver,$smtpserverport,true,$smtpuser,$smtppass);//这里面的一个true是表示使用身份验证,否则不使用身份验证.
    $smtp->debug = false;//是否显示发送的调试信息 FALSE or TRUE
    if($smtp->sendmail($smtpemailto, $smtpusermail, $mailsubject, $utfmailbody, $mailtype))
    {
        return 1;
    }else
    {
        return -2;//发送失败
    }
}


/**
 * 验证传递的参数是否通过合法
 * Enter description here ...
 * @param unknown_type $data
 */
function check_ssing($isCheckTime = false){
    global $_POST,$_GET;
    $subter = array_diff($_POST,$_GET);
    //时间判断
    if ($isCheckTime){
        if (!isset($subter['tkn']) || ($subter['tkn'] && TIMESTMAP - $subter['tkn'] > 60)){
            return false;
        }
    }
    $ssing = $subter['ssing'];
    unset($subter['ssing']);
    ksort($subter);
    $string = "";
    foreach($subter as $k => $file){
        $file = str_replace('\\', '', $file);
        $string .= $k.$file;
    }
    $code = 0;
    $string = base64_encode($string);

    for($i=strlen($string)-1; $i>=0; --$i){
        $v = ord($string[$i]);
        $code = ($code << 6 & 268435455) + $v + ($v << 14);
        $k = $code & 266338304;
        $code = ($k==0 ? $code : ($code ^ $k >> 21));
    }
    if (empty($ssing) || $ssing != $code){
        return false;
    }
    return true;
}

function getRedis($close=false){
    static $_redis = null;
    if($close){
        if($_redis){
            $_redis->close();
        }
        $_redis = null;
        return true;
    }
    if($_redis == null){
        global $redis_config;
        $_redis = new Redis();
        if (!($_redis->connect( $redis_config[0]['host'], $redis_config[0]['port'], 2))){
            return false;
        }
    }
    return $_redis;
}

function getRedis6383($close=false){
    static $_redis6383 = null;
    if($close){
        if($_redis6383){
            $_redis6383->close();
        }
        $_redis6383 = null;
        return true;
    }
    if($_redis6383 == null){
        global $redis_config;
        $_redis6383 = new Redis();
        if (!($_redis6383->connect( $redis_config[1]['host'], $redis_config[1]['port'], 2))){
            return false;
        }
    }
    return $_redis6383;
}
function getRedis6384($close=false){
    static $_redis6384 = null;
    if($close){
        if($_redis6384){
            $_redis6384->close();
        }
        $_redis6384 = null;
        return true;
    }
    if($_redis6384 == null){
        global $redis_config;
        $_redis6384 = new Redis();
        if (!($_redis6384->connect( $redis_config[3]['host'], $redis_config[3]['port'], 2))){
            return false;
        }
    }
    return $_redis6384;
}

function getRedis6387($close=false){
    static $_redis6387 = null;
    if($close){
        if($_redis6387){
            $_redis6387->close();
        }
        $_redis6387 = null;
        return true;
    }
    if($_redis6387 == null){
        global $redis_config;
        $_redis6387 = new Redis();
        if (!($_redis6387->connect( $redis_config[7]['host'], $redis_config[7]['port'], 2))){
            return false;
        }
    }
    return $_redis6387;
}
function getMonitorRedis($close=false){
    static $_monitorredis = null;
    if($close){
        if($_monitorredis){
            $_monitorredis->close();
        }
        $_monitorredis = null;
        return true;
    }
    if($_monitorredis == null){
        global $redis_config;
        $_monitorredis = new Redis();
        if (!($_monitorredis->connect( $redis_config[2]['host'], $redis_config[2]['port'], 2))){
            return false;
        }
    }
    return $_monitorredis;
}

function getRedis6386($close=false){
    static $_r6386 = null;
    if($close){
        if($_r6386){
            $_r6386->close();
        }
        $_r6386 = null;
        return true;
    }
    if($_r6386 == null){
        global $redis_config;
        $_r6386 = new Redis();
        if (!($_r6386->connect( $redis_config[4]['host'], $redis_config[4]['port'], 2))){
            return false;
        }
    }
    return $_r6386;
}

function getRedis6388($close=false){
    static $_r6388 = null;
    if($close){
        if($_r6388){
            $_r6388->close();
        }
        $_r6388 = null;
        return true;
    }
    if($_r6388 == null){
        global $redis_config;
        $_r6388 = new Redis();
        if (!($_r6388->connect( $redis_config[5]['host'], $redis_config[5]['port'], 2))){
            return false;
        }
    }
    return $_r6388;
}


function getRedis6389($close=false){
    static $_redis6389 = null;
    if($close){
        if($_redis6389){
            $_redis6389->close();
        }
        $_redis6389 = null;
        return true;
    }
    if($_redis6389 == null){
        global $redis_config;
        $_redis6389 = new Redis();
        if (!($_redis6389->connect( $redis_config[6]['host'], $redis_config[6]['port'], 2))){
            return false;
        }
    }
    return $_redis6389;
}


function ajaxReturn($status=0, $msg = '', $data = array()){
    echo json_encode(array('status'=>$status, 'title'=>$msg, 'data'=>$data));
    exit;
}

/**
 * 获取$_GET中的字符串
 * @param type $name
 * @return type
 */
function sget($name){
    return isset($_GET[$name]) ? trim(urldecode($_GET[$name])) : '';
}

/**
 * 获取$_POST中的字符串
 * @param type $name
 * @return type
 */
function spost($name){
    return isset($_POST[$name]) ? trim(urldecode($_POST[$name])) : '';
}

/**
 * 获取$_GET中的数字型值
 * @param type $name
 * @return type
 */
function iget($name){
    return isset($_GET[$name]) ? intval($_GET[$name]) : 0;
}

/**
 * 获取$_GET中的数字型值
 * @param type $name
 * @return type
 */
function ipost($name){
    return isset($_POST[$name]) ? intval($_POST[$name]) : 0;
}

/**
 * 数组键名交换
 * Enter description here ...
 * @param $arr
 * @param $key
 */
function getArrayByKey($arr, $key){
    $tmp = array();
    foreach($arr as $val){
        if(isset($val[$key])){
            $tmp[$key] = $val;
        }
    }
    return $tmp;
}

/**
 * 切量排期转换
 * @param $data
 * @return array
 */
function ScheduleDataToDBData($data)
{
    $format_data  = array('A'=>0,'B'=>0,'C'=>0,'D'=>0,'E'=>0,'F'=>0,'G'=>0,'H'=>0,'I'=>0,'J'=>0,'K'=>0,'L'=>0, 'M'=>0,'N'=>0,'O'=>0,'P'=>0,'Q'=>0,'R'=>0,'S'=>0,'T'=>0,'U'=>0,'V'=>0,'W'=>0,'X'=>0);
    $schedule_data = array();
    $len = strlen($data);
    for($i=0;$i<$len;$i++)
    {
        if(key_exists($data[$i],$format_data))
        {
            $format_data[$data[$i]] = 1;
        }
    }
    $schedule_data = array_values($format_data);
    $schedule_data = implode('', $schedule_data);
    return $schedule_data;
}

/**
 * 设备转换
 */
function turnDeviceModels($data)
{
    $redata = null;
    if(is_array($data))
    {
        $default['iphone1'] = array('iPhone1,1');
        $default['iphone3'] = array('iPhone1,2');
        $default['iphone3GS'] = array('iPhone2,1');
        $default['iphone4'] = array('iPhone3,1','iPhone3,3');
        $default['iphone4s'] = array('iPhone4,1');
        $default['iphone5'] = array('iPhone5,1','iPhone5,2');
        $default['iphone5c'] = array('iPhone5,3','iPhone5,4');
        $default['iphone5s'] = array('iPhone6,1','iPhone6,2');
        $default['iphone6'] = array('iPhone7,2');
        $default['iphone6plus'] = array('iPhone7,1');
        $default['ipod1'] = array('iPod1,1');
        $default['ipod2'] = array('iPod2,1');
        $default['ipod3'] = array('iPod3,1');
        $default['ipod4'] = array('iPod4,1');
        $default['ipod5'] = array('iPod5,1');
        $default['ipad1'] = array('iPad1,1');
        $default['ipad2'] = array('iPad2,1','iPad2,2','iPad2,3','iPad2,4');
        $default['ipadmini'] = array('iPad2,5','iPad2,6','iPad2,7');
        $default['ipad3'] = array('iPad3,1','iPad3,2','iPad3,3');
        $default['ipad4'] = array('iPad3,4','iPad3,5','iPad3,6');
        $default['ipadAir'] = array('iPad4,1','iPad4,2','iPad4,3');
        $default['ipadmini2'] = array('iPad4,4','iPad4,5','iPad4,6');
        $redata = array();
        foreach($data as $v)
        {
            if($v == '0')
            {
                $redata = '0';
                break;
            }
            if(isset($default[$v]))
            {
                $redata = array_merge($redata, $default[$v]);
            }
        }
    }
    else
    {
        $redata = '0';
    }
    return $redata;
}

/**
 * ios设备类型转换
 */
function turnDeviceTypes($data)
{
    $redata = null;
    if(!empty($data))
    {
        if(strpos($data, 'Android_Pad') !== false)
        {
            $redata[] = 'Android_Pad';
        }
        if(strpos($data, 'Android') !== false)
        {
            $redata[] = 'Android';
        }
        if(strpos($data, 'iphone') !== false)
        {
            $redata[] = 'iPhone';
        }
        if(strpos($data, 'ipod') !== false)
        {
            $redata[] = 'iPod';
        }
        if(strpos($data, 'ipad') !== false)
        {
            $redata[] = 'iPad';
        }
    }
    else 
    {
        $redata = array();
    }
    return $redata;
}

/**
 * 根据ccplay order置顶优先级和最后编辑时间排序,当做usort参数使用。
 * @param unknown $p1
 * @param unknown $p2
 * @return number
 */

function sortGroupByPublisher($p1,$p2) {
	if($p1['order']<$p2['order']){
		return -1;
	} else if($p1['order']==$p2['order']) {
		if(intval($p1['starttime'])>intval($p2['starttime'])) {
			return -1;
		}
	} else {
		return 1;
	}
}

function getRedisByPort($port="",$close=false){
  	static $_redis = null;
    if($close){
        if($_redis){
            $_redis->close();
        }
        $_redis = null;
        return true;
    }
    if($_redis == null){
        global $redis_config;
        $_redis = new Redis();
        foreach ($redis_config as $item) {
        	if($item['port']==$port){
	        	if (!($_redis->connect($item['host'], $item['port'], 2))){
	            	return false;
	        	}
	        }
        }  
    }
    return $_redis;
}
<?php

class appServer {

	public $params = array();
	public $m;
	public $a;

	public function __construct() {
		$this->m = ucfirst(DEFAULT_CONTROLLER);
		$this->a = strtolower(DEFAULT_ACTION);
	}

	public function init() {

	}

	public function getRequest() {
		if (PHP_SAPI === 'cli') { // substr(php_sapi_name(), 0, 3) == 'cgi'
			global $argv;
			$_SERVER['QUERY_STRING'] = isset($argv[1]) ? $argv[1] : "";
		}
		$_SERVER['QUERY_STRING'] = empty($_SERVER['QUERY_STRING']) ? trim($_SERVER['REQUEST_URI'], '/') : $_SERVER['QUERY_STRING'];
		$path = (isset($_SERVER['QUERY_STRING'])) ? $_SERVER['QUERY_STRING'] : @getenv('QUERY_STRING');
		// hack here
		$m = isset($_REQUEST['m']) ? $_REQUEST['m'] : "";
		$a = isset($_REQUEST['a']) ? $_REQUEST['a'] : "";
		if ($m != "" || $a != "") { // hacker
			// if(!(strpos($path, "&m=") === false || strpos($path, "&a=") === false)) { // hacker
			// 普通格式
			$controller = isset($_REQUEST['m']) ? $_REQUEST['m'] : '';
			$action = isset($_REQUEST['a']) ? $_REQUEST['a'] : '';
			unset($_REQUEST['m'], $_REQUEST['a']);
		} else if (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] != "") {
			// 从url获得controller 和 action
			$urlArr = explode("/", $path, 3);
			$controller = isset($urlArr[0]) ? $urlArr[0] : '';
			$action = isset($urlArr[1]) ? $urlArr[1] : '';
			$paramStr = isset($urlArr[2]) ? $urlArr[2] : '';

			// 从url获得参数
			$_GET = array();
			if (strpos($paramStr, "=")) { // 无键值方式的参数
				$paramArr = explode("&", $paramStr);
				foreach ($paramArr as $item) {
					$itemArr = explode("=", $item);
					if (count($itemArr) == 2) {
						$_GET[$itemArr[0]] = $itemArr[1];
					}
				}
			} else {
				if (strpos($paramStr, "/")) {
					$paramArr = explode("/", $paramStr);
					foreach ($paramArr as $item) {
						if ($item != "") {
							$_GET[] = $item;
						}
					}
				}
			}
		}
		$_REQUEST = array_merge($_POST, $_GET);
		$this->params = $_REQUEST;

		if (!empty($controller)) {
			$this->m = ucfirst($controller);
		}
		if (!empty($action)) {
			$this->a = strtolower($action);
		}
		define('ACTION', $this->a);
	}

	public function run() {
		$this->getRequest();
		$this->init();
		$rs = $this->exec();
		if (empty($rs))
			return false;
		echo json_encode($rs);
	}

	public function exec() {

		$controller = CONTROLLER_DIR . $this->m . ".php";
		$action = $this->a;
		if (!file_exists($controller)) {
			return array("status" => 'fail', 'message' => $this->m . " is not exists.");
		}
		require_once $controller;

		$action = $this->a;
		$this->m = $this->m . "Controller";
		if (!class_exists($this->m)) {
			return array("status" => 'fail', 'message' => $this->m . " is not exists.");
		}
		define('Module', $this->m);
		define('Action', $this->a);
		$c = new $this->m();
		if (!method_exists($c, $this->a)) {
			return array('status' => 'fail', 'message' => "$this->a don't has callable method $this->m");
		}

		//添加Reflection判断,禁止调用proteced和private成员方法
		$reflection = new ReflectionMethod($this->m, $this->a);
		if (!$reflection->isPublic()) {
			return array('status' => 'fail', 'message' => "the $this->a method is protected or private,you are not access. ");
		}

		$rs = $c->{$this->a}($this->params);
		return $rs;
	}

}

<?php if (!defined('IS_SYSTEM')) exit('access deny.');

class MySQL
{
	public $_rs = '';
	public $_linkID = 0;
	public $_db = ''; //数据库实例
	
	public function __construct($config){
		$this->connect($config);
	}
	
	public static function getInstance($config){
		static $static = array();
		$linkID = md5($config['db_host'].":".$config['db_port']);
		if(empty($static[$linkID])){
			$static[$linkID] = new MySQL($config);
		}
		return $static[$linkID];
	}

	public function connect($config)
	{
		$this->_linkID = mysqli_connect($config['db_host'],$config['db_user'],$config['db_pwd'],$config['db_name'],$config['db_port']);
		if(!$this->_linkID) die('Could not connect: ' . mysqli_error());
		mysqli_query($this->_linkID, "SET NAMES UTF8");
		
		$this->_db = $this->_linkID;
		return $this->_linkID;
	}
	
	public function query($sql){
		
		if ($this->_rs){
			@mysqli_free_result($this->_rs);
			$this->_rs = 0;
		}
		if (!IS_DEPLOY){
			file_put_contents(DATA_DIR.'debug.sql', '[SQL:'.date('Y-m-d H:i:s').'] '.$sql."\n\r",FILE_APPEND);
		}
		$this->_rs = mysqli_query($this->_linkID, $sql);
		if($this->_rs){
			return $this->_rs;
		}
		return mysqli_error($this->_linkID);
		
	}
	
	public function fetch_array($rs){
		$data = array();
		while ($row = mysqli_fetch_assoc($rs)) {
			$data[] = $row;
		}
		return $data;
	}
	
	public function fetch_one($rs){
		return mysqli_fetch_assoc($rs);
	}
	
	
	public function lastId(){
		return mysqli_insert_id($this->_linkID);
	}
	
	
	public function affectedRows(){
		return mysqli_affected_rows($this->_linkID);
	}
	
	
	public function __destruct(){
		if ($this->_rs){
			@mysqli_free_result($this->_rs);
			$this->_rs = 0;
		}
		if(!empty($this->_linkID))
			mysqli_close($this->_linkID);
	}
	
	
}

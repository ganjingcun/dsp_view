<?php
header('Content-Type: text/html; charset=utf-8');
set_time_limit(0);
class CronController extends Controller
{
    private $db = '';
    private $redis = '';//
    private $redis_6383 = '';//
    private $redis_6384 = '';//
    private $redis_6387 = '';//
    private $redis_6389 = '';//
    private $showlog = false;

    function __construct() {
        $this->redis =  getRedis();
        $this->redis_6383 =  getRedis6383();
        $this->redis_6384 =  getRedis6384();
        $this->redis_6387 =  getRedis6387();
        $this->redis_6389 = getRedis6389();
        $this->db = D("Cron");
        $this->showlog = isset($_GET['isshowlog'])? $_GET['isshowlog'] : 0; 
    }
    /**
     * 应用信息推送
     */
    function appinfo(){
        //print_r($this->db->getCutoverSys());
        //$this->redis->multi(Redis::MULTI);
        //$this->redis->exec();
        echo "appinfo ok!<br>";
    }

    /**
     * 葫芦商城积分墙数据
     */
    function hulutask()
    {
        $time_start = $this->microtime_float();
        $list = array();
        $this->redis->multi(Redis::MULTI);
        $list = $this->db->getTask();
        $globalsratio = $this->db->getcheatreturnratio();
        $tmpcount = 0;
        $this->redis->del('HLSC_AD_WALL_TASK_LIST');
        $this->redis->del('AD_WALL_TASK_INFO_IOS');
        $this->redis->del('AD_WALL_TASK_INFO_ANDROID');
        if(isset($list['tasklist'])){
            foreach($list['tasklist'] as $val){
                $val['url'] = $list[$val['adid']]['url'];
                $val['storeid'] = $list[$val['adid']]['storeid'];
                $val['wallicon'] = $list[$val['adid']]['wallicon'];
                $val['chargemode'] = $list[$val['adid']]['chargemode'];
                $val['adcontent'] = $list[$val['adid']]['adcontent'];
                $val['tasksummary'] = $list[$val['adid']]['tasksummary'];
                $val['own'] = $list[$val['adid']]['own'];
                $val['click_effect'] = $list[$val['adid']]['click_effect'];
                $val['creative_type'] = $list[$val['adid']]['creative_type'];
                $val['campaignid'] = $list[$val['adid']]['campaignid'];
                $val['adgroupid'] = $list[$val['adid']]['adgroupid'];
                $val['packagename'] = $list[$val['adid']]['packagename'];
                $val['appversion'] = $list[$val['adid']]['appversion'];
                $val['trackcampaignid'] = $list[$val['adid']]['trackcampaignid'];
                $val['starttime'] = $list[$val['adid']]['wallstarttime'];
                $val['endtime'] = $list[$val['adid']]['wallendtime'];
                $val['relaytype'] = $list[$val['adid']]['relaytype'];
                $val['adtitle'] = $list[$val['adid']]['adtitle'];
                $val['appIdfiltertype'] = $list[$val['adid']]['appIdfiltertype'];
                $val['bought_type'] = 6;
                $val['daybudget'] = $list[$val['adid']]['daybudget'];
                $val['groupbudget'] = $list[$val['adid']]['groupbudget'];
                $val['periods'] = $list[$val['adid']]['periods'];
                if($list[$val['adid']]['cheatreturnratio'] === null)
                {
                    if(isset($globalsratio[$list[$val['adid']]['ostype']]))
                    {
                        $val['cheatreturnratio'] = $globalsratio[$list[$val['adid']]['ostype']];
                    }
                    else
                    {
                        $val['cheatreturnratio'] = 0;
                    }
                }
                else
                {
                    $val['cheatreturnratio'] = $list[$val['adid']]['cheatreturnratio'];
                }
                $this->redis->hset("AD_WALL_TASK_INFO_".$list[$val['adid']]['ostype'], "{$list[$val['adid']]['campaignid']}_{$list[$val['adid']]['adgroupid']}_{$val['adid']}_{$val['taskid']}", json_encode($val));
                if($list[$val['adid']]['ostype'] != 'IOS' || $list[$val['adid']]['ishulu'] != '1')
                {
                    continue;
                }
                $this->redis->hset("HLSC_AD_WALL_TASK_LIST", "{$list[$val['adid']]['campaignid']}_{$list[$val['adid']]['adgroupid']}_{$val['adid']}_{$val['taskid']}", json_encode($val));
                //$this->redis->expire("HLSC_AD_WALL_TASK_LIST", REDIS_TTL);
                $tmpcount++;
            }
        }
        $this->redis->exec();
        $time_end = $this->microtime_float();
        if($this->showlog)
        {
            $time = $time_end - $time_start;
            echo "<br>hulu adtask推送 共计使用时间：$time <br>";
        }
        echo $tmpcount.":hulu adtask ok!<br>";
    }


    /**
     * PUNCHBOXID-APPID对应关系
     */
    function punchboxid(){
        $time_start = $this->microtime_float();
        $this->redis->multi(Redis::MULTI);
        $punchbox = $this->db->punchboxinfo();
        $app = $this->db->appinfo();
        $iosmgcate = $this->db->getIosMgTpl();
        $globalsecpa = $this->db->getwallecpa();
        $globalsratio = $this->db->getwallratio();
        $bannedappvers =  $this->db->getverswitch();
        $wallbannedappvers =  $this->db->getverwallswitch();
        $applist = array();
        $dmppidlist = array();

        foreach($app as $val){
            $val['appcategories'] = array($val['apptypeid']);
            $val['appchildtypeid'] = trim($val['appchildtypeid'],',');
           
            if($val['appchildtypeid']){
                $val['appcategories'] = array_merge($val['appcategories'],explode(',', $val['appchildtypeid']));
            }
            if(empty($val['wallecpa'])){
                if(isset($globalsecpa[$val['ostypeid']]))
                {
                    $val['wallecpa'] = (string)$globalsecpa[$val['ostypeid']];
                }
                else
                {
                    $val['wallecpa'] = '80';
                }
            }
            if(empty($val['wallreserveratio']))
            {
                if(isset($globalsratio[$val['ostypeid']]))
                {
                    $val['wallreserveratio'] = (string)$globalsratio[$val['ostypeid']];
                }
                else
                {
                    $val['wallreserveratio'] = '100';
                }
            }
            $val['appdspswitch']=$val['dspfloorsswitch'];
			$appdsp=$this->db->appdspcutoverfloors($val['appid']);
			if(!empty($appdsp)&& count($appdsp)>0 && isset($appdsp['0'])){
				$appdsparray=array();
				foreach ($appdsp as $value) {
					array_push($appdsparray, array('port'=>$value['mediaport'],'priority'=>$value['priority']));
				}
				$val['appdsp'] = $appdsparray;
			}else{
				$val['appdsp']=array();
			}
            $applist[$val['appid']] = $val;
        }

        $cutoverdata = $this->db->getCutoverSys();

        foreach($punchbox as $val){
            unset($val['isupdate']);
            if(isset($applist[$val['appid']]))
            {
                $data = array('appid'=>$val['appid'], 'appname'=>$applist[$val['appid']]['appname'], 'switch'=>$applist[$val['appid']]['switch'], 'appcategories'=>$applist[$val['appid']]['appcategories'], 'istest'=>$applist[$val['appid']]['auditing'], 'wallunit'=>$applist[$val['appid']]['wallunit'], 'wallexchange'=>$applist[$val['appid']]['wallexchange'], 'wallecpa' => $applist[$val['appid']]['wallecpa'], 'wallreserveratio' => $applist[$val['appid']]['wallreserveratio'], 'ctr'=>$val['ctr'],  'atr'=>$val['atr'], 'cpmratio'=>$val['cpmratio'], 'wallrechargetype'=>$applist[$val['appid']]['wallrechargetype'], 'appcampaignid'=>$applist[$val['appid']]['appcampaignid'], 'appori'=>$applist[$val['appid']]['appori']);
                $data['userid'] = $applist[$val['appid']]['userid'];
                $data['bannermgswitch'] = $applist[$val['appid']]['bannermgswitch'];
                $data['bannermgctdown'] = $applist[$val['appid']]['bannermgctdown'];
                $data['fullwtlistswitch'] = $applist[$val['appid']]['fullwtlistswitch'];
                $data['antifraudswitch'] = $applist[$val['appid']]['antifraudswitch'];
                $data['s2smgdetailsswitch'] = $applist[$val['appid']]['s2smgdetailsswitch'];
                $data['debugsdkswitch'] = $applist[$val['appid']]['debugsdkswitch'];
                $data['lowqualityswitch'] = $applist[$val['appid']]['lowqualityswitch'];
                $data['secretkey'] = $applist[$val['appid']]['secretkey'];
                $data['hostpkg'] = $applist[$val['appid']]['packagename'];
                $data['app_os_type'] = $applist[$val['appid']]['ostypeid'];
                $data['app_os_type_switch'] = $applist[$val['appid']]['ostypeswitch'];
                $data['currency'] = $applist[$val['appid']]['currency'];
                $data['chance_publisherID'] = $applist[$val['appid']]['chance_publisherID'];
                $data['chance_secretkey'] = $applist[$val['appid']]['chance_secretkey'];
                if(!empty($applist[$val['appid']]['s2sclkrdt']))
                {
                    $data['s2sclkrdt'] = explode(',', $applist[$val['appid']]['s2sclkrdt']);
                    if(array_search('5', $data['s2sclkrdt'])!==false)
                    {
                        $data['s2sclkrdt'][] = '6';
                    }
                }
                else
                {
                    $data['s2sclkrdt'] = array();
                }
                if(!empty($applist[$val['appid']]['s2sswitches']))
                {
                    $data['s2sswitches'] = explode(',', $applist[$val['appid']]['s2sswitches']);
                    if(array_search('5', $data['s2sswitches'])!==false)
                    {
                        $data['s2sswitches'][] = '6';
                    }
                }
                else
                {
                    $data['s2sswitches'] = array();
                }

                if(!empty($applist[$val['appid']]['feedsswitch']) && 1 == $applist[$val['appid']]['feedsswitch'])
                {
                    $feedsconfig['feedsswitch'] = $applist[$val['appid']]['feedsswitch'];
                    $feedsconfig['feedsiostpl'] = $applist[$val['appid']]['feedsiostpl'];
                    $feedsconfig['feedsandroidtpl'] = $applist[$val['appid']]['feedsandroidtpl'];
                    $feedsconfig['feedsimprdt'] = $applist[$val['appid']]['feedsimprdt'];
                    $feedsconfig['feedsclkrdt'] = $applist[$val['appid']]['feedsclkrdt'];
                    $data['feedsconfig'] = $feedsconfig;
                }
                $data['apiauthswitch'] = $applist[$val['appid']]['apiauthswitch'];
                if(!empty($bannedappvers[$val['publisherid']])) $data['bannedappvers'] = $bannedappvers[$val['publisherid']];
                if(!empty($wallbannedappvers[$val['publisherid']])) $data['newoffwallappvers'] = $wallbannedappvers[$val['publisherid']];
                if(isset($cutoverdata[$val['publisherid']]))
                {
                    $data['trafficpolicies'] = $cutoverdata[$val['publisherid']];
                }
                if(isset($iosmgcate[$val['appid']]))
                {
                    $data['lid'] = $val['appid'];
                }
                if($applist[$val['appid']]['dmpswitch'] == 1)
                {
                    $dmppidlist[$val['publisherid']] = $val['publisherid'];
                }
				$app_dsp_floors=array();
				$app_dsp_floors['dsp_floors_switch']=$applist[$val['appid']]['appdspswitch']?$applist[$val['appid']]['appdspswitch']:1;
				if(isset($applist[$val['appid']]['appdspswitch']) && $applist[$val['appid']]['appdspswitch']==1){					
					if(!empty($applist[$val['appid']]['appdsp'])&& isset($applist[$val['appid']]['appdsp'])){
		                $app_dsp_floors['dsp_floors']=$applist[$val['appid']]['appdsp'];
		            }
		            else{
						$app_dsp_floors['dsp_floors']=array();
		            }
				}
				else{	
					$app_dsp_floors['dsp_floors']=array();
				}
				$data['app_dsp_floors']=$app_dsp_floors;
            }
            else
            {
                continue;
            }
            $this->redis->hset('PUBLISHERID_APPINFO', $val['publisherid'], json_encode($data));
        }
        $cutoveraccountdata = $this->db->getCutoverAccount();
        foreach($cutoveraccountdata as $k => $v)
        {
            foreach($v as $kt => $vt)
            {
                $this->redis->hset('THIRDPARTY_APPINFO_'.$k, $kt, json_encode($vt));
            }
        }
        $this->redis->exec();
        //dmp开关
        $this->redis_6387->multi(Redis::MULTI);
        $this->redis_6387->del('PUBLISHER_DMP_CONFIG');
        if($dmppidlist)
        {
            foreach ($dmppidlist as $v)
            {
                $this->redis_6387->sadd('PUBLISHER_DMP_CONFIG', $v);
            }
             
        }
        $this->redis_6387->exec();
        $time_end = $this->microtime_float();
        if($this->showlog)
        {
            $time = $time_end - $time_start;
            echo "<br>publisherID推送 共计使用时间：$time <br>";
        }
        echo "publisherID ok!<br>";
    }

    /**
     * 切量关系推送对应关系
     */
    function CutoverSet(){
        $time_start = $this->microtime_float();
        $list = $this->db->cutoversetlist();
        $pidlist = array();
        foreach($list as $val){
            $data = array();
            $data['os'] = $val['os'];
            $data['partner'] = $val['partner'];
            $data['adtype'] = (int)$val['adtype'];
            $pidlist[$val['pid']] = json_encode($data);
        }
        $this->redis->multi(Redis::MULTI);
        $this->redis->del('SSP_POLICIES');
        foreach($pidlist as $key => $val){
            $this->redis->hset('SSP_POLICIES', $key, $val);
        }
        $this->redis->exec();
        $time_end = $this->microtime_float();
        if($this->showlog)
        {
            $time = $time_end - $time_start;
            echo "<br>CutoverSet推送 共计使用时间：$time <br>";
        }
        echo "CutoverSet ok!<br>";
    }

    /**
     * 预加载刷新周期推送
     */
    public function preloadset(){
        $info = $this->db->getPreloadSetInfo();
        $adRotator = $this->db->getAdRotatorConfig();
        $data['wall_preload_peroid'] = 3600;
        $data['mg_preload_peroid'] = 3600;
        $data['popup_preload_peroid'] = 3600;
        $data['anti_fraud_switch'] = '0';
        $data['ios_banner_rotator_length'] = 5;
        $data['android_banner_rotator_length'] = 5;
        $data['ios_interstitial_rotator_length'] = 5;
        $data['android_interstitial_rotator_length'] = 5;
        $data['dsp_cutover_count_per_request'] = 2;
//        $data['wall_task_notify_delay_time'] = 20;
//        $data['wall_task_query_delay_time'] = 10;
		$data['wall_task_installation_delay_time']=10;//安装
		$data['wall_task_activate_delay_time']=10;//激活
		$data['wall_task_register_delay_time']=10;//注册
		$data['wall_task_click_delay_time']=10;//点击
		$data['wall_task_checkin_delay_time']=10;//签到
		$data['wall_task_share_delay_time']=10;//分享
		$data['wall_task_follow_delay_time']=10;//关注公众账号
		$data['wall_task_search_delay_time']=10;//搜索
		$data['wall_task_watch_delay_time']=10;//观看视频
		$data['wall_task_customized_delay_time']=10;//自定义
		$data['dsp_cutover_floors_switch']=0;//DSP 兜底设置开关
        foreach($info as $v)
        {
            //mg预加载
            if($v['skey'] == 'mgignoredevice')
            {
                if($v['sval'] < 0)
                {
                    $data['mg_preload_peroid'] = 0;
                }
                else
                {
                    $data['mg_preload_peroid'] = $v['sval']*3600;
                }
            }
            //积分墙预加载
            if($v['skey'] == 'taskignoredevice')
            {
                if($v['sval'] < 0)
                {
                    $data['wall_preload_peroid'] = 0;
                }
                else
                {
                    $data['wall_preload_peroid'] = $v['sval']*3600;
                }
            }
            if($v['skey'] == 'antifraudswitch')
            {
                $data['anti_fraud_switch'] = $v['sval'];
            }
            //插屏广告预加载
            if($v['skey'] == 'popupignoredevice')
            {
                if($v['sval'] < 0)
                {
                    $data['popup_preload_peroid'] = 0;
                }
                else
                {
                    $data['popup_preload_peroid'] = $v['sval']*3600;
                }
            }
         	//单次DSP切量次数设置
            if($v['skey'] == 'dspcutovercount')
            {
                if($v['sval'] < 0)
                {
                    $data['dsp_cutover_count_per_request'] = 2;
                }
                else
                {
                    $data['dsp_cutover_count_per_request'] = $v['sval'];
                }
            }
            //积分墙服务器返积分延迟
//            if($v['skey'] == 'serviceaccumulatepointsdelay')
//            {
//                if($v['sval'] < 0)
//                {
//                    $data['wall_task_notify_delay_time'] = 0;
//                }
//                else
//                {
//                    $data['wall_task_notify_delay_time'] = $v['sval'];
//                }
//            }
//            //积分墙SDK返积分延迟
//            if($v['skey'] == 'sdkaccumulatepointsdelay')
//            {
//                if($v['sval'] < 0)
//                {
//                    $data['wall_task_query_delay_time'] = 0;
//                }
//                else
//                {
//                    $data['wall_task_query_delay_time'] = $v['sval'];
//                }
//            }
			//安装
            if($v['skey'] == 'trackeventtype1')
            {
                if($v['sval'] < 0)
                {
                	error_log("v['sval'] < 0");
                	error_log($v['sval']);
                    $data['wall_task_installation_delay_time'] = 10;
                }
                else
                {
                	error_log("v['sval'] >= 0");
                	error_log($v['sval']);
                    $data['wall_task_installation_delay_time'] = $v['sval'];
                }
            }
           //激活
       		if($v['skey'] == 'trackeventtype2')
            {
                if($v['sval'] < 0)
                {
                    $data['wall_task_activate_delay_time'] = 10;
                }
                else
                {
                    $data['wall_task_activate_delay_time'] = $v['sval'];
                }
            }
             //注册
        	if($v['skey'] == 'trackeventtype3')
            {
                if($v['sval'] < 0)
                {
                    $data['wall_task_register_delay_time'] = 10;
                }
                else
                {
                    $data['wall_task_register_delay_time'] = $v['sval'];
                }
            }
            //点击
        	if($v['skey'] == 'trackeventtype4')
            {
                if($v['sval'] < 0)
                {
                    $data['wall_task_click_delay_time'] = 10;
                }
                else
                {
                    $data['wall_task_click_delay_time'] = $v['sval'];
                }
            }
            //签到
        	if($v['skey'] == 'trackeventtype5')
            {
                if($v['sval'] < 0)
                {
                    $data['wall_task_checkin_delay_time'] = 10;
                }
                else
                {
                    $data['wall_task_checkin_delay_time'] = $v['sval'];
                }
            }
            //分享
        	if($v['skey'] == 'trackeventtype6')
            {
                if($v['sval'] < 0)
                {
                    $data['wall_task_share_delay_time'] = 10;
                }
                else
                {
                    $data['wall_task_share_delay_time'] = $v['sval'];
                }
            }
            //关注公众号
        	if($v['skey'] == 'trackeventtype7')
            {
                if($v['sval'] < 0)
                {
                    $data['wall_task_follow_delay_time'] = 10;
                }
                else
                {
                    $data['wall_task_follow_delay_time'] = $v['sval'];
                }
            }
            //搜索
        	if($v['skey'] == 'trackeventtype8')
            {
                if($v['sval'] < 0)
                {
                    $data['wall_task_search_delay_time'] = 10;
                }
                else
                {
                    $data['wall_task_search_delay_time'] = $v['sval'];
                }
            }
       		//观看视频
       	 	if($v['skey'] == 'trackeventtype9')
            {
                if($v['sval'] < 0)
                {
                    $data['wall_task_watch_delay_time'] = 10;
                }
                else
                {
                    $data['wall_task_watch_delay_time'] = $v['sval'];
                }
            }
            //自定义
         	if($v['skey'] == 'trackeventtype99')
            {
                if($v['sval'] < 0)
                {
                    $data['wall_task_customized_delay_time'] = 10;
                }
                else
                {
                    $data['wall_task_customized_delay_time'] = $v['sval'];
                }
            }
            
        //DSP兜底切量开关设置，默认为0
         	if($v['skey'] == 'dspcutoverfloorsswitch')
            {
                if($v['sval'] < 0)
                {
                    $data['dsp_cutover_floors_switch'] = 0;
                }
                else
                {
                    $data['dsp_cutover_floors_switch'] = $v['sval'];
                }
            }
        }
        foreach ($adRotator as $val)
        {
            switch ($val['adtype'])
            {
                case 'iosbanner':
                    $data['ios_banner_rotator_length'] = $val['rotatorlength'];
                    break;
                case 'androidbanner':
                    $data['android_banner_rotator_length'] = $val['rotatorlength'];
                    break;
                case 'iospopup':
                    $data['ios_interstitial_rotator_length'] = $val['rotatorlength'];
                    break;
                case 'androidpopup':
                    $data['android_interstitial_rotator_length'] = $val['rotatorlength'];
                    break;
            }
        }
        $this->redis->multi(Redis::MULTI);
        $this->redis->hset('AD_GLOBAL_SETTINGS', 'mg_preload_peroid', $data['mg_preload_peroid']);
        $this->redis->hset('AD_GLOBAL_SETTINGS', 'wall_preload_peroid', $data['wall_preload_peroid']);
        $this->redis->hset('AD_GLOBAL_SETTINGS', 'anti_fraud_switch', $data['anti_fraud_switch']);
        $this->redis->hset('AD_GLOBAL_SETTINGS', 'popup_preload_peroid', $data['popup_preload_peroid']);
        $this->redis->hset('AD_GLOBAL_SETTINGS', 'ios_banner_rotator_length', $data['ios_banner_rotator_length']);
        $this->redis->hset('AD_GLOBAL_SETTINGS', 'android_banner_rotator_length', $data['android_banner_rotator_length']);
        $this->redis->hset('AD_GLOBAL_SETTINGS', 'ios_interstitial_rotator_length', $data['ios_interstitial_rotator_length']);
        $this->redis->hset('AD_GLOBAL_SETTINGS', 'android_interstitial_rotator_length', $data['android_interstitial_rotator_length']);
        $this->redis->hset('AD_GLOBAL_SETTINGS', 'dsp_cutover_count_per_request', $data['dsp_cutover_count_per_request']);
//        $this->redis->hset('AD_GLOBAL_SETTINGS', 'wall_task_notify_delay_time', $data['wall_task_notify_delay_time']);
//        $this->redis->hset('AD_GLOBAL_SETTINGS', 'wall_task_query_delay_time', $data['wall_task_query_delay_time']);
		$this->redis->hset('AD_GLOBAL_SETTINGS', 'wall_task_installation_delay_time', $data['wall_task_installation_delay_time']);
		$this->redis->hset('AD_GLOBAL_SETTINGS', 'wall_task_activate_delay_time', $data['wall_task_activate_delay_time']);
		$this->redis->hset('AD_GLOBAL_SETTINGS', 'wall_task_register_delay_time', $data['wall_task_register_delay_time']);
		$this->redis->hset('AD_GLOBAL_SETTINGS', 'wall_task_click_delay_time', $data['wall_task_click_delay_time']);
		$this->redis->hset('AD_GLOBAL_SETTINGS', 'wall_task_checkin_delay_time', $data['wall_task_checkin_delay_time']);
		$this->redis->hset('AD_GLOBAL_SETTINGS', 'wall_task_share_delay_time', $data['wall_task_share_delay_time']);
		$this->redis->hset('AD_GLOBAL_SETTINGS', 'wall_task_follow_delay_time', $data['wall_task_follow_delay_time']);
		$this->redis->hset('AD_GLOBAL_SETTINGS', 'wall_task_search_delay_time', $data['wall_task_search_delay_time']);
		$this->redis->hset('AD_GLOBAL_SETTINGS', 'wall_task_watch_delay_time', $data['wall_task_watch_delay_time']);
		$this->redis->hset('AD_GLOBAL_SETTINGS', 'wall_task_customized_delay_time', $data['wall_task_customized_delay_time']);
		$this->redis->hset('AD_GLOBAL_SETTINGS', 'dsp_cutover_floors_switch', $data['dsp_cutover_floors_switch']);
        $this->redis->exec();
        echo count($info).":preloadset ok!<br>";
    }
	
	/**
     * 切量兜底设置
     */
    function dspCutoverFloors(){
        $time_start = $this->microtime_float();
		$dspFloors = $this->db->getDSPCutoverFloors();
        $this->redis->multi(Redis::MULTI);
        $this->redis->del('DSP_CUTOVER_FLOORS');
        //设置DSP兜底优先级设置
		 if(!empty($dspFloors))
        {
           $this->redis->hset('DSP_CUTOVER_FLOORS', 'dsp_cutover_floors', json_encode($dspFloors));
        }
        $this->redis->exec();
        $time_end = $this->microtime_float();
        if($this->showlog)
        {
            $time = $time_end - $time_start;
            echo "<br>dspCutoverFloors推送 共计使用时间：$time <br>";
        }
        echo "dspCutoverFloors ok!<br>";
    }

    /**
     * 预加载刷新周期推送
     */
    public function WebIntegralUrlSet(){
        $info = $this->db->getWebIntegralUrlInfo();
        if(empty($info))
        {
            echo "WebIntegralUrlSet error!<br>";
            return;
        }
        $this->redis->multi(Redis::MULTI);
        foreach ($info as $v)
        {
            if($v['skey'] == 'webintegralurl')
            {
                $this->redis->hset('AD_GLOBAL_SETTINGS', 'web_offer_wall_url', $v['sval']);
            }
            if($v['skey'] == 'webintegralerrurl')
            {
                $this->redis->hset('AD_GLOBAL_SETTINGS', 'web_offer_wall_error_page_url', $v['sval']);
            }
        }
        $this->redis->exec();
        echo count($info).":WebIntegralUrlSet ok!<br>";
    }

	
    /**
     * Banner广告信息推送
     */
    function banner(){
        $time_start = $this->microtime_float();
        $banner = $this->db->getAdInfo(1);
        $this->redis->multi(Redis::MULTI);
        $this->redis->del('AD_BANNER_INFO_ANDROID');
        $this->redis->del('AD_BANNER_INFO_IOS');
        foreach($banner as $val){
            if($val['ostype']=='ALL'){
                $val['device_os_versions'] = !empty($val['iosvset']) ? $val['iosvset'] : '';
                $val['device_sdk_versions'] = !empty($val['isdkvset']) ? $val['isdkvset'] : '';
                unset($val['iosvset']);
                unset($val['isdkvset']);
                $this->redis->hset('AD_BANNER_INFO_IOS', $val['adid'], json_encode($val));
                if(isset($val['jailbreak_types'])){
                    unset($val['jailbreak_types']);
                }
                $val['device_os_versions'] = !empty($val['aosvset']) ? $val['aosvset'] : '';
                $val['device_sdk_versions'] = !empty($val['asdkvset']) ? $val['asdkvset'] : '';
                unset($val['aosvset']);
                unset($val['asdkvset']);
                $this->redis->hset('AD_BANNER_INFO_ANDROID', $val['adid'], json_encode($val));
            }else{
                if($val['ostype'] == 'ANDROID' && isset($val['jailbreak_types'])){
                    unset($val['jailbreak_types']);
                }
                $val['device_os_versions'] = $val['osversionset'] ? ($val['ostype']=='ANDROID' ? $val['aosvset'] : $val['iosvset']) : '';
                $val['device_sdk_versions'] = $val['sdkversionset'] ? ($val['ostype']=='ANDROID' ? $val['asdkvset'] : $val['isdkvset']) : '';
                unset($val['iosvset']);
                unset($val['isdkvset']);
                unset($val['aosvset']);
                unset($val['asdkvset']);
                $this->redis->hset('AD_BANNER_INFO_'.$val['ostype'], $val['adid'], json_encode($val));
            }
        }
        $this->redis->exec();
        $time_end = $this->microtime_float();
        if($this->showlog)
        {
            $time = $time_end - $time_start;
            echo "<br>Banner推送 共计使用时间：$time <br>";
        }
        echo count($banner).":banner ok!<br>";
    }

    /**
     * Feeds广告信息推送
     */
    function feeds(){
        $time_start = $this->microtime_float();
        $feeds = $this->db->getFeedsAdInfo();
        $this->redis->multi(Redis::MULTI);
        $this->redis->del('AD_FEEDS_INFO_ANDROID');
        $this->redis->del('AD_FEEDS_INFO_IOS');
        foreach($feeds as $val){
            if($val['ostype']=='ALL'){
                $val['device_os_versions'] = !empty($val['iosvset']) ? $val['iosvset'] : '';
                $val['device_sdk_versions'] = !empty($val['isdkvset']) ? $val['isdkvset'] : '';
                unset($val['iosvset']);
                unset($val['isdkvset']);
                $this->redis->hset('AD_FEEDS_INFO_IOS', $val['adid'], json_encode($val));
                if(isset($val['jailbreak_types'])){
                    unset($val['jailbreak_types']);
                }
                $val['device_os_versions'] = !empty($val['aosvset']) ? $val['aosvset'] : '';
                $val['device_sdk_versions'] = !empty($val['asdkvset']) ? $val['asdkvset'] : '';
                unset($val['aosvset']);
                unset($val['asdkvset']);
                $this->redis->hset('AD_FEEDS_INFO_ANDROID', $val['adid'], json_encode($val));
            }else{
                if($val['ostype'] == 'ANDROID' && isset($val['jailbreak_types'])){
                    unset($val['jailbreak_types']);
                }
                $val['device_os_versions'] = $val['osversionset'] ? ($val['ostype']=='ANDROID' ? $val['aosvset'] : $val['iosvset']) : '';
                $val['device_sdk_versions'] = $val['sdkversionset'] ? ($val['ostype']=='ANDROID' ? $val['asdkvset'] : $val['isdkvset']) : '';
                unset($val['iosvset']);
                unset($val['isdkvset']);
                unset($val['aosvset']);
                unset($val['asdkvset']);
                $this->redis->hset('AD_FEEDS_INFO_'.$val['ostype'], $val['adid'], json_encode($val));
            }
        }
        $this->redis->exec();
        $time_end = $this->microtime_float();
        if($this->showlog)
        {
            $time = $time_end - $time_start;
            echo "<br>Feeds推送 共计使用时间：$time <br>";
        }
        echo count($feeds).":Feeds ok!<br>";
    }
	
	/**
     * dsp广告信息推送
     */
    function dspAds(){
        $time_start = $this->microtime_float();
        $dspAds = $this->db->getAdInfo(30);
        $this->redis->multi(Redis::MULTI);
        $this->redis->del('AD_DSP_INFO_ANDROID');
        $this->redis->del('AD_DSP_INFO_IOS');
        foreach($dspAds as $val){
            if($val['ostype']=='ALL'){
                $val['device_os_versions'] = !empty($val['iosvset']) ? $val['iosvset'] : '';
                $val['device_sdk_versions'] = !empty($val['isdkvset']) ? $val['isdkvset'] : '';
                unset($val['iosvset']);
                unset($val['isdkvset']);
                $this->redis->hset('AD_DSP_INFO_IOS', $val['adid'], json_encode($val));
                if(isset($val['jailbreak_types'])){
                    unset($val['jailbreak_types']);
                }
                $val['device_os_versions'] = !empty($val['aosvset']) ? $val['aosvset'] : '';
                $val['device_sdk_versions'] = !empty($val['asdkvset']) ? $val['asdkvset'] : '';
                unset($val['aosvset']);
                unset($val['asdkvset']);
                $this->redis->hset('AD_DSP_INFO_ANDROID', $val['adid'], json_encode($val));
            }else{
                if($val['ostype'] == 'ANDROID' && isset($val['jailbreak_types'])){
                    unset($val['jailbreak_types']);
                }
                $val['device_os_versions'] = $val['osversionset'] ? ($val['ostype']=='ANDROID' ? $val['aosvset'] : $val['iosvset']) : '';
                $val['device_sdk_versions'] = $val['sdkversionset'] ? ($val['ostype']=='ANDROID' ? $val['asdkvset'] : $val['isdkvset']) : '';
                unset($val['iosvset']);
                unset($val['isdkvset']);
                unset($val['aosvset']);
                unset($val['asdkvset']);
                $this->redis->hset('AD_DSP_INFO_'.$val['ostype'], $val['adid'], json_encode($val));
            }
        }
        $this->redis->exec();
        $time_end = $this->microtime_float();
        if($this->showlog)
        {
            $time = $time_end - $time_start;
            echo "<br>DSP推送 共计使用时间：$time <br>";
        }
        echo count($dspAds).":dsp ads ok!<br>";
    }

    /**
     * 推送FAQ信息
     */
    function faqList() {
        $faqlist = $this->db->getFaqListData();
        //推送FAQ信息
        try {
            $this->redis_6389->multi(Redis::MULTI);
            //推送客服信息
            $this->redis_6389->del('AD_CCPLAY_FAQ_INFO');
            foreach ($faqlist as $key=>$val) {
                $this->redis_6389->hset('AD_CCPLAY_FAQ_INFO',$key,json_encode($val));
            }
            // $this->redis_6389->expire('AD_CCPLAY_FAQ_INFO',ANNOUNCE_EXPIRE_SECOND);
            $this->redis_6389->exec();
            echo 'FAQ 数据推送'.count($faqlist).'.条数据<br/>';
        }catch (Exception $e) {
            error_log('faq推送有问题啊');
        }
    }


    /**
     * 推送礼包信息
     */
    function giftList() {
        $giftInfo = $this->db->getGiftDailyTaskInfo();
        //print_r($giftInfo);
        //delete key prefix
        $delkeys = $this->redis_6389->keys('AD_GIFT_PACKAGE_INFO_*');
        $this->redis_6389->multi(Redis::MULTI);
        foreach($delkeys as $dk) {
            $this->redis_6389->del($dk);
        }
        $i =0;
        //推送每日礼包
        foreach ($giftInfo as $key=>$val) {
            foreach($val as $valkey => $valone) {
                $i ++;
                $this->redis_6389->hset('AD_GIFT_PACKAGE_INFO_'.$key,$valkey,json_encode($valone));
            }
             
            $this->redis_6389->expire('AD_GIFT_PACKAGE_INFO_'.$key,ANNOUNCE_EXPIRE_SECOND);
        }
        $this->redis_6389->exec();
        echo '礼包数据推送'.count($i).'条数据.<br/>';
    }

    /**
     * 批量推送CCPLAY 相关内容
     */
    function ccplayPushTask() {
        $this->giftList();//ok
        $this->faqList();//ok
        $this->pushKefu();//ok
        $this->ccplayswitchstatus();//ok
        $this->pushGonglue();//ok
        //$this->adNews();//ok
        //$this->adNewsApp();//ok
    }


    /**
     * CCPLAY攻略推送
     */
    function pushGonglue() {
        $gonglueInfo = $this->db->getGonglueConfigData();
        $delkeys = $this->redis_6389->keys('AD_RAIDERS_INFO*');
        $this->redis_6389->multi(Redis::MULTI);
        foreach($delkeys as $dk) {
            $this->redis_6389->del($dk);
        }
         
         
        //推送客服信息
        foreach ($gonglueInfo as $key=>$val) {
            $this->redis_6389->hset('AD_RAIDERS_INFO_'.$key,$val['adid'],json_encode($val));
        }
         
        //$this->redis_6389->expire('AD_RAIDERS_INFO',ANNOUNCE_EXPIRE_SECOND);
        $this->redis_6389->exec();
        echo '攻略推送完毕<br/>';
    }
    /**
     * CCPLAY客服推送
     */
    function pushKefu() {
        $kefuInfo = $this->db->getKefuConfigData();
        $this->redis_6389->multi(Redis::MULTI);
        //推送客服信息
        $this->redis_6389->del('AD_CUSTOMER_SERVICE_INFO');
        foreach ($kefuInfo as $key=>$val) {
            $this->redis_6389->hset('AD_CUSTOMER_SERVICE_INFO',$key,json_encode($val));
        }
        //$this->redis_6389->expire('AD_CUSTOMER_SERVICE_INFO',ANNOUNCE_EXPIRE_SECOND);
        $this->redis_6389->exec();
        echo '<br/>客服推送'.count($kefuInfo).'条数据<br/>';
    }

    /**
     * 单机SDK公告广告信息推送
     */
    function adNews(){
         
        $announcement = $this->db->getAnnouncementInfo();
        $delkeys1 = $this->redis_6383->keys('AD_NEWS_INFO_ANDROID_*');
        $delkeys2 = $this->redis_6383->keys('AD_NEWS_INFO_IOS_*');
        $this->redis_6383->multi(Redis::MULTI);
        foreach($delkeys1 as $dk1) {
            $this->redis_6383->del($dk1);
        }
        foreach($delkeys2 as $dk2) {
            $this->redis_6383->del($dk2);
        }
        //推送公告
        foreach($announcement as $key => $val){
            foreach($val as $k => $v)
            {
                if($v['ostype']=='ALL'){
                    //$val['device_os_versions'] = isset($val['iosvset']) ? $val['iosvset'] : '';
                    $this->redis_6383->hset('AD_NEWS_INFO_IOS_'.$key, $v['adid'], json_encode($v));
                    //                    if(isset($val['jailbreak_types'])){
                    //                        unset($val['jailbreak_types']);
                    //                    }
                    //                    $val['device_os_versions'] = isset($val['aosvset']) ? $val['aosvset'] : '';

                    $this->redis_6383->hset('AD_NEWS_INFO_ANDROID_'.$key, $v['adid'], json_encode($v));
                    // $this->redis_6383->expire('AD_NEWS_INFO_ANDROID_'.$key, ANNOUNCE_EXPIRE_SECOND);
                    // $this->redis_6383->expire('AD_NEWS_INFO_IOS_'.$key, ANNOUNCE_EXPIRE_SECOND);
                }else{
                    //                    if($v['ostype'] == 'ANDROID' && isset($val['jailbreak_types'])){
                    //                        unset($val['jailbreak_types']);
                    //                    }
                    //                    $val['device_os_versions'] = isset($val['aosvset']) ?($val['ostype']=='ANDROID' ? $val['aosvset'] : $val['iosvset']) : '';
                    $this->redis_6383->hset('AD_NEWS_INFO_'.$v['ostype'].'_'.$key, $v['adid'], json_encode($v));
                    //$this->redis_6383->expire('AD_NEWS_INFO_'.$v['ostype'].'_'.$key, ANNOUNCE_EXPIRE_SECOND);
                }
            }
        }
        $this->redis_6383->exec();
        echo ":公告推送".count($announcement)."条数据!<br>";
    }


    /**
     * ccplay 开关状态同步到redis
     * @author yangsen
     */
    function ccplayswitchstatus() {

        $ccplaySwitchStatus = $this->db->getAppChannelCCPlaySwitchStatus();
        $delkeys  = $this->redis_6389->keys('CCPLAY_CATEGORIES_ANDROID_*');
        $this->redis_6389->multi(Redis::MULTI);
        foreach($delkeys as $dk) {
            $this->redis_6389->del($dk);
        }
        foreach($ccplaySwitchStatus as $publisher=>$property) {

            foreach ($property as $key => $val) {
                //echo 'CCPLAY_CATEGORIES_ANDROID_'.$publisher.$key.json_encode($val).'<br/>';
                $this->redis_6389->hset('CCPLAY_CATEGORIES_ANDROID_'.$publisher,$key,json_encode($val));
            }
            //$this->redis_6389->expire('CCPLAY_CATEGORIES_ANDROID_'.$publisher,CCPLAY_ONOFF_EXPIRE_SECOND);
        }
        $this->redis_6389->exec();
        echo 'ccplay开关配置推送'.count($ccplaySwitchStatus).'条数据<br/>';
    }

    /**
     * 单机SDK精品推荐信息推送
     */
    function adNewsApp(){
        $announcement = $this->db->getSubInfo();
        //        print_r($announcement);exit;

        $delkey1 = $this->redis_6383->keys('AD_NEWSAPP_INFO_ANDROID_*');
        $delkey2 = $this->redis_6383->keys('AD_NEWSAPP_INFO_IOS_*');

        $this->redis_6383->multi(Redis::MULTI);
        foreach ($delkey1 as $dk1) {
            $this->redis_6383->del($dk1);
        }
        foreach ($delkey2 as $dk2) {
            $this->redis_6383->del($dk2);
        }

        foreach($announcement as $key => $val){

            foreach($val as $k => $v)
            {
                if($v['ostype']=='ALL'){
                    //$val['device_os_versions'] = isset($val['iosvset']) ? $val['iosvset'] : '';
                    $this->redis_6383->hset('AD_NEWSAPP_INFO_IOS_'.$key, $k, json_encode($v));
                    //                    if(isset($val['jailbreak_types'])){
                    //                        unset($val['jailbreak_types']);
                    //                    }
                    //                    $val['device_os_versions'] = isset($val['aosvset']) ? $val['aosvset'] : '';
                    $this->redis_6383->hset('AD_NEWSAPP_INFO_ANDROID_'.$key, $k, json_encode($v));
                    //$this->redis_6383->expire('AD_NEWSAPP_INFO_ANDROID_'.$key, 3000);
                    //$this->redis_6383->expire('AD_NEWSAPP_INFO_IOS_'.$key, 3000);
                }else{
                    //                    if($v['ostype'] == 'ANDROID' && isset($val['jailbreak_types'])){
                    //                        unset($val['jailbreak_types']);
                    //                    }
                    //                    $val['device_os_versions'] = isset($val['aosvset']) ?($val['ostype']=='ANDROID' ? $val['aosvset'] : $val['iosvset']) : '';
                    $this->redis_6383->hset('AD_NEWSAPP_INFO_'.$v['ostype'].'_'.$key, $k, json_encode($v));
                    //$this->redis_6383->expire('AD_NEWSAPP_INFO_'.$v['ostype'].'_'.$key, 3000);
                }
            }
            echo '交叉推广'.$key.':'.count($val)."<br/>";
        }
        $this->redis_6383->exec();
    }

    function popup(){
        $time_start = $this->microtime_float();
        $popup = $this->db->getAdInfo(2);
        $this->redis->multi(Redis::MULTI);
        $this->redis->del('AD_POPUP_INFO_ANDROID');
        $this->redis->del('AD_POPUP_INFO_IOS');
        $this->redis->del('AD_S2S_POPUP_INFO_ANDROID');
        $this->redis->del('AD_S2S_POPUP_INFO_IOS');
        foreach($popup as $val){
            if($val['ostype']=='ALL'){
                $val['device_os_versions'] = !empty($val['iosvset']) ? $val['iosvset'] : '';
                $val['device_sdk_versions'] = !empty($val['isdkvset']) ? $val['isdkvset'] : '';
                unset($val['iosvset']);
                unset($val['isdkvset']);
                $this->redis->hset('AD_POPUP_INFO_IOS', $val['adid'], json_encode($val));
                if(isset($val['jailbreak_types'])){
                    unset($val['jailbreak_types']);
                }
                $val['device_os_versions'] = !empty($val['aosvset']) ? $val['aosvset'] : '';
                $val['device_sdk_versions'] = !empty($val['asdkvset']) ? $val['asdkvset'] : '';
                unset($val['aosvset']);
                unset($val['asdkvset']);
                $this->redis->hset('AD_POPUP_INFO_ANDROID', $val['adid'], json_encode($val));
            }else{
                if($val['ostype'] == 'ANDROID' && isset($val['jailbreak_types'])){
                    unset($val['jailbreak_types']);
                }
                $val['device_os_versions'] = $val['osversionset'] ? ($val['ostype']=='ANDROID' ? $val['aosvset'] : $val['iosvset']) : '';
                $val['device_sdk_versions'] = $val['sdkversionset'] ? ($val['ostype']=='ANDROID' ? $val['asdkvset'] : $val['isdkvset']) : '';
                unset($val['iosvset']);
                unset($val['isdkvset']);
                unset($val['aosvset']);
                unset($val['asdkvset']);
                $this->redis->hset('AD_POPUP_INFO_'.$val['ostype'], $val['adid'], json_encode($val));
            }

            if($val['isapi']){
                //$val['order'] = $val['apisort'];
                $this->redis->hset('AD_S2S_POPUP_INFO_'.$val['ostype'], $val['adid'], json_encode($val));
            }
        }
        $this->redis->exec();
        $time_end = $this->microtime_float();
        if($this->showlog)
        {
            $time = $time_end - $time_start;
            echo "<br>popup推送 共计使用时间：$time <br>";
        }
        echo count($popup).":popup ok!<br>";
    }

    /**
     * 精品推荐
     */
    function moregame(){
        $time_start = $this->microtime_float();
        $list = $this->db->getMoreGame();
        $delData = $this->getDelKey(__METHOD__);
        $this->redis->multi(Redis::MULTI);
        $this->redis->del('AD_MOREGAME_INFO_ANDROID');
        $this->redis->del('AD_MOREGAME_INFO_IOS');
        $this->redis->del('AD_S2S_MOREGAME_INFO_ANDROID');
        $this->redis->del('AD_S2S_MOREGAME_INFO_IOS');
        $this->redis->del('TOP_AD_MOREGAME_INFO_IOS');
        $this->redis->del('TOP_AD_MOREGAME_INFO_ANDROID');
        $this->redis->del('WALL_AD_MOREGAME_INFO_ANDROID');
        $this->redis->del('WALL_AD_MOREGAME_INFO_IOS');
        if(!empty($delData))
        {
            $delData = json_decode($delData);
            foreach ($delData as $v)
            {
                $this->redis->del($v);
            }
        }
        if(isset($list['applist'])){
            $applist = $list['applist'];
            unset($list['applist']);
        }
        $delKey = array();
        foreach($list as $val){
            $istop = '';
            if($val['istop']){
                $istop = 'TOP_';
            }
            if($val['ostype']=='ALL'){
                if($val['selfappset'] && isset($applist[$val['userid']])){
                    $val['own'] = 2;
                    $val['device_os_versions'] = !empty($val['aosvset']) ? $val['aosvset'] : '';
                    $val['device_sdk_versions'] = !empty($val['asdkvset']) ? $val['asdkvset'] : '';
                    unset($val['aosvset']);
                    unset($val['asdkvset']);
                    $android = $val;
                    if(isset($android['jailbreak_types'])){
                        unset($android['jailbreak_types']);
                    }
                    $val['device_os_versions'] = !empty($val['iosvset']) ? $val['iosvset'] : '';
                    $val['device_sdk_versions'] = !empty($val['isdkvset']) ? $val['isdkvset'] : '';
                    unset($val['iosvset']);
                    unset($val['isdkvset']);
                    $ios = $val;
                    foreach($applist[$val['userid']]['IOS'] as $myappid){
                        $this->redis->hset('AD_MOREGAME_INFO_IOS'.$myappid, $val['adid'], json_encode($ios));
                        $delKey[] = 'AD_MOREGAME_INFO_IOS'.$myappid;
                    }
                    foreach($applist[$val['userid']]['ANDROID'] as $myappid){
                        $this->redis->hset('AD_MOREGAME_INFO_ANDROID'.$myappid, $val['adid'], json_encode($android));
                        $delKey[] = 'AD_MOREGAME_INFO_ANDROID'.$myappid;
                    }
                }else{

                    $val['device_os_versions'] = !empty($val['iosvset']) ? $val['iosvset'] : '';
                    $val['device_sdk_versions'] = !empty($val['isdkvset']) ? $val['isdkvset'] : '';
                    unset($val['iosvset']);
                    unset($val['isdkvset']);
                    $this->redis->hset($istop.'AD_MOREGAME_INFO_IOS', $val['adid'], json_encode($val));
                    if(isset($val['jailbreak_types'])){
                        unset($val['jailbreak_types']);
                    }
                    $val['device_os_versions'] = !empty($val['aosvset']) ? $val['aosvset'] : '';
                    $val['device_sdk_versions'] = !empty($val['asdkvset']) ? $val['asdkvset'] : '';
                    unset($val['aosvset']);
                    unset($val['asdkvset']);
                    $this->redis->hset($istop.'AD_MOREGAME_INFO_ANDROID', $val['adid'], json_encode($val));
                }
            }else{
                if($val['selfappset']){
                    if(isset($applist[$val['userid']])){
                        $val['own'] = 2;
                        $flist = $applist[$val['userid']];
                    }else{
                        continue;
                    }
                }else{
                    $flist = array($val['ostype']=>array(0=>''));
                }
                if($val['ostype'] == 'ANDROID' && isset($val['jailbreak_types'])){
                    unset($val['jailbreak_types']);
                }
                $val['device_os_versions'] = $val['osversionset'] ? ($val['ostype']=='ANDROID' ? $val['aosvset'] : $val['iosvset']) : '';
                $val['device_sdk_versions'] = $val['sdkversionset'] ? ($val['ostype']=='ANDROID' ? $val['asdkvset'] : $val['isdkvset']) : '';
                unset($val['iosvset']);
                unset($val['isdkvset']);
                unset($val['aosvset']);
                unset($val['asdkvset']);
                foreach($flist[$val['ostype']] as $myappid){
                    $rkey = $istop.'AD_MOREGAME_INFO_'.$val['ostype'].$myappid;
                    if($val['isintegral'] == 1)
                    {
                        $this->redis->hset('WALL_AD_MOREGAME_INFO_'.$val['ostype'], $val['adid'], json_encode($val));
                        //$this->redis->expire($rkey, REDIS_TTL);
                    }
                    if($val['istop']){
                        $val['order'] = $val['torder'];
                        $this->redis->hset($rkey, $val['adid'], json_encode($val));
                    }
                    else
                    {
                        $this->redis->hset($rkey, $val['adid'], json_encode($val));
                    }
                    if($val['isapi'])
                    {
                        $val['order'] = $val['apisort'];
                        $this->redis->hset('AD_S2S_MOREGAME_INFO_'.$val['ostype'], $val['adid'], json_encode($val));
                    }
                    if(!empty($myappid))
                    {
                        $delKey[] = $rkey;
                    }
                    //$this->redis->expire($rkey, REDIS_TTL);
                }
            }
        }
        $this->setDelKey(__METHOD__, $delKey);
        $this->redis->exec();
        $time_end = $this->microtime_float();

        if($this->showlog)
        {
            $time = $time_end - $time_start;
            echo "<br>moregame推送 共计使用时间：$time <br>";
        }
        echo count($list).":moregame ok!<br>";
    }


    /**
     * 推送到微商店
     */
    function sendmgtowsd(){
        $time_start = $this->microtime_float();
        $list = $this->db->getMoreGame();
        $i = 0;
        foreach($list as $val){
            if(!isset($val['campaignid']) || !isset($val['adgroupid']) || !isset($val['adid']) || $val['selfappset'])
            {
                continue;
            }
            $data['campaignid'] = $val['campaignid'];
            $data['adgroupid'] = $val['adgroupid'];
            $data['stuffid'] = $val['adid'];
            $data['downurl'] = $val['url'];
            $data['iconurl'] = $val['moregameicon'];
            $data['starttime'] = $val['starttime'];
            $data['endtime'] = $val['endtime'];
            $data['gatherurl'] = $val['gatherurl'];
            $data['adtype'] = 3;
            if($val['ostype'] == 'IOS')
            {
                $data['ostype'] = 2;
            }
            elseif($val['ostype'] == 'ANDROID')
            {
                $data['ostype'] = 1;
            }
            else
            {
                continue;
            }
            $data['trackurl'] = '';
            $data['trackid'] = $val['trackcampaignid'];

            if(!empty($data))
            {
                ksort($data);
                $key = 'CiAelrbF1ZdYg+Ut55upnVVqeh2014cK';
                $data['key'] = md5($key . print_r($data, true));
                $sdata = http_build_query($data);
                curlGet('http://admin.anzhuoshangdian.com/api/moregame?'.$sdata);
            }
            $i++;
        }
        $time_end = $this->microtime_float();
        if($this->showlog)
        {
            $time = $time_end - $time_start;
            echo "<br>sendmgtowsd推送 共计使用时间：$time <br>";
        }
        echo count($i).":sendmgtowsd ok!<br>";
    }

    /**
     * 积分墙任务
     */
    function adTask(){
        $time_start = $this->microtime_float();
        $list = array();
        $delData = $this->getDelKey(__METHOD__);
        $this->redis->multi(Redis::MULTI);
        $list = $this->db->getTask();
        $globalsratio = $this->db->getcheatreturnratio();
        $this->redis->del('AD_WALL_INFO_IOS');
        $this->redis->del('AD_WALL_INFO_ANDROID');
        $this->redis->del('TOP_AD_WALL_INFO_IOS');
        $this->redis->del('TOP_AD_WALL_INFO_ANDROID');
        if(!empty($delData))
        {
            $delData = json_decode($delData);
            foreach ($delData as $v)
            {
                $this->redis->del($v);
            }
        }
        if(isset($list['tasklist'])){
            $delKey = array();
            foreach($list['tasklist'] as $val){
                $istop = '';
                if($list[$val['adid']]['istop']){
                    $istop = 'TOP_';
                }
                if($list[$val['adid']]['ostype']=='ALL'){
                    $list[$val['adid']]['device_os_versions'] = !empty($list[$val['adid']]['iosvset']) ? $list[$val['adid']]['iosvset'] : '';
                    $list[$val['adid']]['device_sdk_versions'] = !empty($list[$val['adid']]['isdkvset']) ? $list[$val['adid']]['isdkvset'] : '';
                    $this->redis->hset($istop.'AD_WALL_INFO_IOS', $val['adid'], json_encode($list[$val['adid']]));
                    $list[$val['adid']]['device_os_versions'] = !empty($list[$val['adid']]['aosvset']) ? $list[$val['adid']]['aosvset'] : '';
                    $list[$val['adid']]['device_sdk_versions'] = !empty($list[$val['adid']]['asdkvset']) ? $list[$val['adid']]['asdkvset'] : '';
                    $this->redis->hset($istop.'AD_WALL_INFO_ANDROID', $val['adid'], json_encode($list[$val['adid']]));
                    //                    $this->redis->expire($istop.'AD_WALL_INFO_IOS', REDIS_TTL);
                    //                    $this->redis->expire($istop.'AD_WALL_INFO_ANDROID', REDIS_TTL);
                }else{
                    $list[$val['adid']]['device_os_versions'] = $list[$val['adid']]['osversionset'] ?($list[$val['adid']]['ostype']=='ANDROID' ? $list[$val['adid']]['aosvset'] : $list[$val['adid']]['iosvset']) : '';
                    $list[$val['adid']]['device_sdk_versions'] = $list[$val['adid']]['sdkversionset'] ?($list[$val['adid']]['ostype']=='ANDROID' ? $list[$val['adid']]['asdkvset'] : $list[$val['adid']]['isdkvset']) : '';
                    $this->redis->hset($istop.'AD_WALL_INFO_'.$list[$val['adid']]['ostype'], $val['adid'], json_encode($list[$val['adid']]));
                    //                    $this->redis->expire($istop.'AD_WALL_INFO_'.$list[$val['adid']]['ostype'], REDIS_TTL);
                }
                
                $val['url'] = $list[$val['adid']]['url']; 
                $val['storeid'] = $list[$val['adid']]['storeid'];
                $val['wallicon'] = $list[$val['adid']]['wallicon'];
                $val['chargemode'] = $list[$val['adid']]['chargemode'];
                $val['adcontent'] = $list[$val['adid']]['adcontent'];
                $val['tasksummary'] = $list[$val['adid']]['tasksummary'];
                $val['own'] = $list[$val['adid']]['own'];
                $val['click_effect'] = $list[$val['adid']]['click_effect'];
                $val['creative_type'] = $list[$val['adid']]['creative_type'];
                $val['campaignid'] = $list[$val['adid']]['campaignid'];
                $val['adgroupid'] = $list[$val['adid']]['adgroupid'];
                $val['packagename'] = $list[$val['adid']]['packagename'];
                $val['appversion'] = $list[$val['adid']]['appversion'];
                $val['trackcampaignid'] = $list[$val['adid']]['trackcampaignid'];
                $val['starttime'] = $list[$val['adid']]['wallstarttime'];
                $val['endtime'] = $list[$val['adid']]['wallendtime'];
                $val['relaytype'] = $list[$val['adid']]['relaytype'];
                $val['adtitle'] = $list[$val['adid']]['adtitle'];
                $val['appIdfiltertype'] = $list[$val['adid']]['appIdfiltertype'];
                $val['bought_type'] = 6;
                $val['daybudget'] = $list[$val['adid']]['daybudget'];
                $val['groupbudget'] = $list[$val['adid']]['groupbudget'];
                $val['periods'] = $list[$val['adid']]['periods'];
                if($list[$val['adid']]['cheatreturnratio'] === null)
                {
                    if(isset($globalsratio[$list[$val['adid']]['ostype']]))
                    {
                        $val['cheatreturnratio'] = $globalsratio[$list[$val['adid']]['ostype']];
                    }
                    else
                    {
                        $val['cheatreturnratio'] = 0;
                    }
                }
                else
                {
                    $val['cheatreturnratio'] = $list[$val['adid']]['cheatreturnratio'];
                }
                $this->redis->hset("AD_WALL_TASK_{$list[$val['adid']]['campaignid']}_{$list[$val['adid']]['adgroupid']}_{$val['adid']}", $val['taskid'], json_encode($val));
                //$this->redis->expire("AD_WALL_TASK_{$list[$val['adid']]['campaignid']}_{$list[$val['adid']]['adgroupid']}_{$val['adid']}", REDIS_TTL);
                $delKey[] = "AD_WALL_TASK_{$list[$val['adid']]['campaignid']}_{$list[$val['adid']]['adgroupid']}_{$val['adid']}";
            }
            $this->setDelKey(__METHOD__, $delKey);
        }
        $this->redis->exec();
        $time_end = $this->microtime_float();
        if($this->showlog)
        {
            $time = $time_end - $time_start;
            echo "<br>adtask推送 共计使用时间：$time <br>";
        }
        echo count($list).":adtask ok!<br>";
    }

    /**
     * Retarget过滤设置。
     * 增加isall参数可以在URL推送时自动推送所有数据
     * Enter description here ...
     */
    function getRetarget(){
        $time_start = $this->microtime_float();
        if(isset($_GET['isall'])){
            $isupdate = 0;
        }else{
            $isupdate = 1;
        }
        $retarget = $this->db->getRetarget($isupdate);
        $delData = $this->getDelKey(__METHOD__);
        $this->redis->multi(Redis::MULTI);
        if(!empty($delData))
        {
            $delData = json_decode($delData);
            foreach ($delData as $v)
            {
                $this->redis->del($v);
            }
        }
        $delKey = array();
        foreach($retarget as $val){
            if(!empty($val['sysdirmacfile'])) $this->pushRetarget($val['sysdirmacfile'], $val['adgroupid'], 1);
            if(!empty($val['sysdiridfafile'])) $this->pushRetarget($val['sysdiridfafile'], $val['adgroupid'], 2);
            $delKey[] = 'AD_RETARGET_'.$val['adgroupid'];
        }
        $this->setDelKey(__METHOD__, $delKey);
        $this->redis->exec();
        $time_end = $this->microtime_float();
        if($this->showlog)
        {
            $time = $time_end - $time_start;
            echo "<br>Retarget推送 共计使用时间：$time <br>";
        }
        echo 'Retarget ok!<br>';
    }

    /**
     * 推送retarget文件内容到redis
     * @param unknown_type $filename
     * @param unknown_type $adgroupid
     * @param unknown_type $ignore
     */
    private function pushRetarget($filename, $adgroupid, $ignore){

        $handle = @fopen($filename, "r");
        if ($handle) {
            while (!feof($handle)) {
                $buffer = fgets($handle, 4096);
                $buffer = strtoupper(trim($buffer));
                if($buffer){
                    $this->redis->hset('AD_RETARGET_'.$adgroupid, $buffer, $ignore);
                }
            }
            //$this->redis->expire('AD_RETARGET_'.$adgroupid, REDIS_TTL);
            fclose($handle);
        }else{
            header("PER{$adgroupid}:$filename");
        }
    }

    /**
     * 广告主账户信息推送
     */
    function adusers(){
        $time_start = $this->microtime_float();
        $users = $this->db->aduserinfo();
        $this->redis->multi(Redis::MULTI);
        foreach($users as $val){
            $this->redis->hset('ACCOUNT_CHARGED', $val['userid'], $val['recharged']);
            $this->redis->hset('ACCOUNT_DEPOSIT', $val['userid'], $val['deposit']);
        }
        $this->redis->exec();
        $time_end = $this->microtime_float();
        if($this->showlog)
        {
            $time = $time_end - $time_start;
            echo "<br>adusers推送 共计使用时间：$time <br>";
        }
        echo "adusers ok!<br>";
    }

    /**
     * 忽略广告定向设备列表
     */
    function ignoredevice(){
        $time_start = $this->microtime_float();
        $list = $this->db->getignoredevice();
        $this->redis->multi(Redis::MULTI);
        $this->redis->del('DEVICE_LIST_IGNORE_AD_FILTER');
        foreach($list as $val){
            $this->redis->sadd('DEVICE_LIST_IGNORE_AD_FILTER', $val['ignoreid']);
        }
        $this->redis->exec();
        $time_end = $this->microtime_float();
        if($this->showlog)
        {
            $time = $time_end - $time_start;
            echo "<br>ignoredevice推送 共计使用时间：$time <br>";
        }
        echo "ignoredevice ok!<br>";
    }

    /**
     * 数据推送开始
     */
    function init(){
        $time_start = $this->microtime_float();
        $isupdate = $this->db->getIsUpdate();

        $dateMark = $this->redis->get('ADINFODATEMARK');
        if($isupdate || $dateMark < date('YmdH'))
        {
            $this->banner();//Banner广告
            $this->popup();//POPUP广告
            $this->moregame();//精品推荐
            $this->getRetarget();//广告主再定向
            $this->adgroupReTarget();//广告组再定向
            $this->adTask();
            $this->adPreDownload();
            $this->ignoredevice();
            $this->CutoverSet();
            $this->hulutask();
            $this->feeds();
			$this->dspAds();//DSP广告
            $this->db->setIsUpdate($isupdate);
            $this->redis->set('ADINFODATEMARK', date('YmdH'));
            $time_end = $this->microtime_float();
            $time = $time_end - $time_start;
            echo "<hr>完成时间：",date('Y-m-d H:i:s'),"耗时：$time";
        }
        else
        {
            echo "增量数据暂无更新！";
        }
        $this->IosAdCompetitive(); //IOS原始精品推荐
        $this->adusers();//广告主账号
        $this->punchboxid();
        $this->appinfo();
        $this->sendmgtowsd();
        $this->adBorder();
        $this->adPosition();
        $this->dmpConfigSet();
        $this->adexchange();//ADX
        $time_end = $this->microtime_float();
        $time = $time_end - $time_start;
        echo "<br />共计使用时间：$time ";
    }

    /**
     * 十分钟数据推送
     */
    function tenMinuteInit()
    {
        $time_start = $this->microtime_float();
        $this->adNews();
        $this->adNewsApp();
        //推送web积分墙应用地址
        $this->WebIntegralUrlSet();
        $time_end = $this->microtime_float();
        $this->ccplayPushTask();
        $time = $time_end - $time_start;
        echo "<br />共计使用时间：$time ";
    }


    /**
     * 每小时数据推送开始
     */
    function hourinit()
    {
        $this->appRatio();//转化率推送
        $this->preloadset();
		$this->dspCutoverFloors();//DSP切量兜底设置
		$this->gatherUserType();//获取用户标签对应的点击量
    }

    /**
     * dmp设置
     */
    function dmpConfigSet()
    {
        $globalSet = $this->db->getGlobalInfo();
        $this->redis_6387->multi(Redis::MULTI);
        $this->redis_6387->del('DMP_ADDETAILS_SWITCH');
        $this->redis_6387->del('DMP_USAGE_STRATEGY');
        if(isset($globalSet['dmpdebugswitch']))
        {
            $this->redis_6387->set('DMP_ADDETAILS_SWITCH', $globalSet['dmpdebugswitch']);
        }
        if(isset($globalSet['dmprule']))
        {
            $this->redis_6387->set('DMP_USAGE_STRATEGY', $globalSet['dmprule']);
        }
        $this->redis_6387->exec();
    }

    /**
     * 广告组媒体过滤
     */
    function adgroupReTarget(){
        $retarget = $this->db->getAdgroupRetarget();
        $subretarget = $this->db->getAdgroupRetarget(2);
        $delData = $this->getDelKey(__METHOD__);
        $this->redis->multi(Redis::MULTI);
        $adgroupid = array();
        foreach($retarget as $val){
            $adgroupid[$val['adgroupid']] = $val['adgroupid'];
        }
        if(!empty($delData))
        {
            $delData = json_decode($delData);
            foreach ($delData as $v)
            {
                $this->redis->del($v);
            }
        }
        $delKey = array();
        foreach($retarget as $val){
            $this->redis->hset('APPID_FILTER_'.$val['adgroupid'], $val['appid'], $val['adtargettype']);
            $delKey[] = 'APPID_FILTER_'.$val['adgroupid'];
        }
        $delKey_sub = array();
        foreach($subretarget as $val){
        	$this->redis->hset('EXCHANGE_SITEID_FILTER_'.$val['adgroupid'], $val['appid'].'-'.$val['siteid'], $val['adsubtargettype']);
        	$delKey[] = 'EXCHANGE_SITEID_FILTER_'.$val['adgroupid'];
        }
        //        foreach($adgroupid as $val){
        //            $this->redis->expire('APPID_FILTER_'.$val, REDIS_TTL);
        //        }
        $this->setDelKey(__METHOD__, $delKey);
        $this->redis->exec();
        echo 'adgroup retarget ok!<br />';
    }

    /**
     *
     * 广告位设置
     */
    function adPosition(){
        $time_start = $this->microtime_float();
        $list = $this->db->getPosition();
        $delData = $this->getDelKey(__METHOD__);
        $this->redis->multi(Redis::MULTI);
        if(!empty($delData))
        {
            $delData = json_decode($delData);
            foreach ($delData as $v)
            {
                $this->redis->del($v);
            }
        }
        $delKey = array();
        foreach($list as $val){
            $this->redis->hset("AD_POSITION_{$val['publisherid']}_{$val['adform']}_{$val['positionid']}", 'adclickeffect', $val['adclickeffect']);
            $this->redis->hset("AD_POSITION_{$val['publisherid']}_{$val['adform']}_{$val['positionid']}", 'banneradtype', $val['banneradtype']);
            if(isset($val['displayscale']))
            {
                $this->redis->hset("AD_POSITION_{$val['publisherid']}_{$val['adform']}_{$val['positionid']}", 'displayscale', $val['displayscale']);
                $this->redis->hset("AD_POSITION_{$val['publisherid']}_{$val['adform']}_{$val['positionid']}", 'stuffscale', $val['stuffscale']);
                $this->redis->hset("AD_POSITION_{$val['publisherid']}_{$val['adform']}_{$val['positionid']}", 'popupbdcolor', $val['popupbdcolor']);
            }
            if(isset($val['bordertype']))
            {
                $this->redis->hset("AD_POSITION_{$val['publisherid']}_{$val['adform']}_{$val['positionid']}", 'bordertype', $val['bordertype']);
                $this->redis->hset("AD_POSITION_{$val['publisherid']}_{$val['adform']}_{$val['positionid']}", 'borderpolicy', $val['borderpolicy']);
                $this->redis->hset("AD_POSITION_{$val['publisherid']}_{$val['adform']}_{$val['positionid']}", 'bordergroupid', $val['bordergroupid']);
            }
            //$this->redis->expire("AD_POSITION_{$val['publisherid']}_{$val['adform']}_{$val['positionid']}", REDIS_TTL);
            $delKey[] = "AD_POSITION_{$val['publisherid']}_{$val['adform']}_{$val['positionid']}";
        }
        $this->setDelKey(__METHOD__, $delKey);
        $this->redis->exec();
        $time_end = $this->microtime_float();
        if($this->showlog)
        {
            $time = $time_end - $time_start;
            echo "<br>adposition 推送 共计使用时间：$time <br>";
        }
        echo 'adposition ok!<br />';
    }

    /**
     *
     * 广告位边框设置
     */
    function adBorder(){
        $time_start = $this->microtime_float();
        $list = $this->db->getBorders();
        $this->redis->multi(Redis::MULTI);
        $this->redis->del('DEV_AD_BORDERS');
        $this->redis->del('GLOBAL_AD_BORDERS');
        foreach($list as $val){
            if($val['type']=='1'){
                unset($val['type']);
                $this->redis->hset('GLOBAL_AD_BORDERS', $val['bordergroupid'], json_encode($val));
            }else{
                unset($val['type']);
                $this->redis->hset('DEV_AD_BORDERS', $val['bordergroupid'], json_encode($val));
            }
        }
        $this->redis->exec();
        $time_end = $this->microtime_float();
        if($this->showlog)
        {
            $time = $time_end - $time_start;
            echo "<br>广告边框推送共计使用时间：$time <br>";
        }
        echo 'adBorder ok!<br />';
    }

    function delOld(){
        $this->redis->del('PUBLISHERID_APPINFO');
    }

    function microtime_float()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }

    /**
     * 预下载广告推送
     */
    function adPreDownload(){
        $time_start = $this->microtime_float();
        $list = $this->db->getPrePopup();
        $this->redis->multi(Redis::MULTI);
        $this->redis->del("AD_PRE_DOWNLOAD");
        foreach($list as $val){
            $this->redis->hset("AD_PRE_DOWNLOAD", $val['adid'], json_encode($val));
            //$this->redis->expire("AD_PRE_DOWNLOAD", REDIS_TTL);
        }
        $this->redis->exec();
        $time_end = $this->microtime_float();
        if($this->showlog)
        {
            $time = $time_end - $time_start;
            echo "<br>ad pre download推送 共计使用时间：$time <br>";
        }
        echo 'ad pre download ok!<br />';
    }

    /**
     * 转化率信息推送
     */
    private function appRatio(){
        $adBannerInfo = $this->db->getAdInfo(1);
        $adPopupInfo = $this->db->getAdInfo(2);
        $adMGInfo = $this->db->getMoreGame();
        unset($adMGInfo['applist']);
        $data = array_merge($adBannerInfo, $adPopupInfo, $adMGInfo);
        $adRatio = $this->db->getAdRatio();
        $tmpData = array();
        foreach($data as $val)
        {
            $tmpData[$val['adid']] = $val['chargemode'];
        }
        //print_r($adRatio);
        $this->redis->multi(Redis::MULTI);
        $this->redis->del('AD_APPID_CTR');
        $this->redis->del('AD_APPID_ATR');
        foreach ($adRatio as $k => $v)
        {
            if(isset($tmpData[$v['stuffid']]) && $tmpData[$v['stuffid']] == 2)
            {
                $this->redis->hset('AD_APPID_CTR', $v['appid'].'_'.$val['adid'], 100);
            }
            else
            {
                if($v['ctr'] > 0)
                {
                    $this->redis->hset('AD_APPID_CTR', $v['appid'].'_'.$v['stuffid'], $v['ctr']);
                }
                if($v['atr'] > 0)
                {
                    $this->redis->hset('AD_APPID_ATR', $v['appid'].'_'.$v['stuffid'], $v['atr']);
                }
            }
        }

        $this->redis->exec();
        echo count($adRatio).":ratio ok!<br>";
    }

    /**
     * 设置要删除的随即键值
     */
    private function setDelKey($key, $data)
    {
        if(empty($data))
        {
            return;
        }
        if(is_array($data))
        {
            $data = array_unique($data);
        }
        else
        {
            $data = $data;
        }
        $this->redis->hset('AD_INFO_DELKEYS', $key, json_encode($data));
    }

    /**
     * 获取要删除的随即键值
     */
    private function getDelKey($key)
    {
        return $this->redis->hget('AD_INFO_DELKEYS', $key);
    }
    function IosAdCompetitive() {
        $time_start = $this->microtime_float();
        $this->IosMgCate();
        $this->CateMgGame();
        $this->IosAdDetails();
        $this->MgSpecial();
        $time_end = $this->microtime_float();
        if($this->showlog)
        {
            $time = $time_end - $time_start;
            echo "<br>IosAdCompetitive 推送 共计使用时间：$time <br>";
        }
        echo 'IosAdCompetitive ok';
    }

    /**
     * ios 精品推荐 栏目
     */
    function IosMgCate() {
        $time_start = $this->microtime_float();
        $adMgCate = $this->db->getIosMgCate();
        $delData = $this->getDelKey(__METHOD__);
        $this->redis_6384->multi(Redis::MULTI);
        if(!empty($delData))
        {
            $delData = json_decode($delData);
            foreach ($delData as $v)
            {
                $this->redis_6384->del($v);
            }
        }
        $delKey = array();
        $this->redis_6384->del('AD_NATIVE_MOREGAME_CATEGORIES_IOS');
        $this->redis_6384->del('AD_NATIVE_MOREGAME_CATEGORIES_ANDROID');
        foreach ($adMgCate as $key => $val) {
            foreach($val as $k=>$v)
            {
                if($v['ostype']==1)
                {
                    $ostype = "ANDROID";
                }
                else
                {
                    $ostype = "IOS";
                }
                $categoryid = $v['categoryid'];
                if(empty($v['appid']))
                {
                    $this->redis_6384->hset("AD_NATIVE_MOREGAME_CATEGORIES_{$ostype}", $categoryid, json_encode($v));
                }
                else
                {
                    $this->redis_6384->hset("AD_NATIVE_MOREGAME_CATEGORIES_{$ostype}_{$v['appid']}", $categoryid, json_encode($v));
                    $delKey[] = "AD_NATIVE_MOREGAME_CATEGORIES_{$ostype}_{$v['appid']}";
                }
            }
        }
        $this->setDelKey(__METHOD__, $delKey);
        $this->redis_6384->exec();
        $time_end = $this->microtime_float();
        if($this->showlog)
        {
            $time = $time_end - $time_start;
            echo "<br>adMgCate推送 共计使用时间：$time <br>";
        }
        echo "adMgCate ok!<br>";
    }

    /**
     * 原生精品推荐广告详情
     */
    function IosAdDetails() {
        $time_start = $this->microtime_float();
        $adMgDetails = $this->db->getAdDetails();
        $this->redis_6384->multi(Redis::MULTI);
        $this->redis_6384->del('AD_NATIVE_MOREGAME_DETAILS_IOS');
        $this->redis_6384->del('AD_NATIVE_MOREGAME_DETAILS_ANDROID');
        foreach ($adMgDetails as $key => $val) {
            $storeid = $val['storeid'];
            $val['thumbs'] = json_decode($val['thumbs']);
            $val['punit'] = '￥';
            //原生精品推荐内容表中 1：iphone；2：ipad 3： ANDROID
            if($val['ostype'] == 3)
            {
                $ostype = 'ANDROID';
            }
            else
            {
                $ostype = 'IOS';
            }
            $this->redis_6384->hset("AD_NATIVE_MOREGAME_DETAILS_{$ostype}", $storeid, json_encode($val));
        }
        $this->redis_6384->exec();
        $time_end = $this->microtime_float();
        if($this->showlog)
        {
            $time = $time_end - $time_start;
            echo "<br>IosAdDetails推送 共计使用时间：$time <br>";
        }
        echo "IosAdDetails ok!<br>";
    }

    /**
     * 精品推荐
     */
    function CateMgGame() {
        $time_start = $this->microtime_float();
        $list = $this->db->getCateMgGame();
        $delData = $this->getDelKey(__METHOD__);
        $this->redis_6384->multi(Redis::MULTI);
        if(!empty($delData))
        {
            $delData = json_decode($delData);
            foreach ($delData as $v)
            {
                $this->redis_6384->del($v);
            }
        }
        $delKey = array();
        foreach ($list as $key => $val) {
            foreach ($val as $k => $v) {
                $adid = $v['adid'];
                $v['punit'] = '￥';
                $this->redis_6384->hset("AD_NATIVE_MOREGAME_INFO_{$v['ostype']}_" . $key, $adid, json_encode($v));
                $delKey[] = "AD_NATIVE_MOREGAME_INFO_{$v['ostype']}_" . $key;
            }
        }
        $this->setDelKey(__METHOD__, $delKey);
        $this->redis_6384->exec();
        $time_end = $this->microtime_float();
        if($this->showlog)
        {
            $time = $time_end - $time_start;
            echo "<br>CateMgGame推送 共计使用时间：$time <br>";
        }
        echo "CateMgGame ok!<br>";
    }

    /**
     * 原生精品推荐专题推广
     */
    function MgSpecial() {
        $time_start = $this->microtime_float();
        $delData = $this->getDelKey(__METHOD__);
        $list = $this->db->getMgSpecial();   //     var_dump($list);
        $this->redis_6384->multi(Redis::MULTI);
        $this->redis_6384->del('AD_NATIVE_MOREGAME_TOPAPP_IOS');
        if(!empty($delData))
        {
            $delData = json_decode($delData);
            foreach ($delData as $v)
            {
                $this->redis_6384->del($v);
            }
        }
        $delKey = array();
        foreach ($list as $key => $val) {
            $specialid = $val['specialid'];
            // var_dump($val);
            $val['punit'] = '￥';
            if(!empty($val['appid']))
            {
                $this->redis_6384->hset("AD_NATIVE_MOREGAME_TOPAPP_{$val['ostype']}_".$val['appid'], $specialid, json_encode($val));
                $delKey[] = "AD_NATIVE_MOREGAME_TOPAPP_{$val['ostype']}_".$val['appid'];
            }
            else
            {
                $this->redis_6384->hset("AD_NATIVE_MOREGAME_TOPAPP_{$val['ostype']}", $specialid, json_encode($val));
            }
        }
        $this->setDelKey(__METHOD__, $delKey);
        $this->redis_6384->exec();
        $time_end = $this->microtime_float();
        if($this->showlog)
        {
            $time = $time_end - $time_start;
            echo "<br>MgSpecial推送 共计使用时间：$time <br>";
        }
        echo "MgSpecial ok!<br>";
    }
    
	/**
     * ADX推送
     */
    function adexchange(){
        $time_start = $this->microtime_float();
		$list = $this->db->getAdxlist();
        $this->redis->multi(Redis::MULTI);
        $this->redis->del('ADX_CONFIG_INFO');
        //ADX推送
		 if(!empty($list))
        {
        	foreach ($list as $key => $value) {
        		$data=array();
        		$adxid=$value['adxid'];
        		$data=array('adxid'=>$value['adxid'],'iospid'=>$value['iospid'],'andpid'=>$value['androidpid']);
        		$this->redis->hset('ADX_CONFIG_INFO', $adxid, json_encode($data));
        	}
        }
        $this->redis->exec();
        $time_end = $this->microtime_float();
        if($this->showlog)
        {
            $time = $time_end - $time_start;
            echo "<br>ADX_CONFIG_INFO推送 共计使用时间：$time <br>";
        }
        echo "ADX_CONFIG_INFO ok!<br>";
    }
    
	/*
	 * 广告用户标签推荐
	 */
	function gatherUserType(){
		$stuffid = isset($_GET["stuffid"])?$_GET["stuffid"]:"";
		$limit = isset($_GET['limit'])?$_GET['limit']:5;
		$adids = array();
		if(!empty($stuffid)) {
			$adids[0]['stuffid']=$stuffid;	
		}else{
			$adusertag = D("AdUserTag");
			$adusertag->delAdidsOverDay();
			$adids = $this->db->getAdids();
		}
		$url="internal.ad-dmp.cocounion.com:811/retrieve/adusertyperatio?secretKey=zhou&secretId=123";
		foreach($adids as $v){
			$postdata['adid'] = $v['stuffid'];
			$postdata['limit']=$limit;
			$data=json_encode($postdata);
			$header=array('Content-Type:application/json','charset=utf-8','Content-Length:'.strlen($data));
			$ch = curl_init();
	        curl_setopt($ch, CURLOPT_URL, $url);
	        curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
	        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	        $ret_data=curl_exec($ch);
	        $http_retcode=curl_getinfo($ch, CURLINFO_HTTP_CODE);
	        curl_setopt($ch, CURLOPT_POST, 0); 
	        curl_close($ch);
			if ($http_retcode=='200') {
				$obj= json_decode($ret_data,true);
				if($obj['errorCode']==100){
					echo $v['stuffid']." 无数据<br/>";
				}
				elseif ($obj['errorCode'] == 200){
					echo $v['stuffid']." 内部解析错误<br/>";
				}
				elseif ($obj['errorCode']==400){
					echo $v['stuffid']." 参数错误<br/>";
				}
				elseif($obj['errorCode'] == 0){
					$this->writeToDB($obj['click'], $v['stuffid']);
					echo $v['stuffid']." ok<br/>";
				}
			}
			else 
				echo "请求失败{$http_retcode}";
		}
	}
	
	private function writeToDB($params, $adid){
		$adusertag = D("AdUserTag");
		$data['data'] = json_encode($params);
		$data['adid'] = $adid;
		$data['retrievetime'] = time();
		$adusertag->delByAdid($adid);
		$adusertag->add($data);
	}
}

<?php
class ViewsController extends CommonController
{
    function index(){

    }

    function appinfo(){
        $db = D("Redis");
        $info = $db->getAppInfo();
        $this->assign('info', $info);
        $this->assign('action', 'appinfo');
        if(isset($_GET['is_ajax']) && $_GET['is_ajax'] == 1){
            echo ajaxReturn(1, '', $info);
        }else{
            $this->display('views-appinfo.html');
        }
    }

    function account(){
        $db = D("Redis");
        $recharged = $db->getAccountRecharged();
        $deposit = $db->getAccountDeposit();
        $tmp = array();
        foreach($recharged as $key=> $val){
            $tmp[$key]['cost'] = $val;
            if(isset($deposit[$key])){
                $tmp[$key]['deposit'] = $deposit[$key];
            }else{
                $tmp[$key]['deposit'] = 0;
            }
        }
        $this->assign('list', $tmp);
        $this->assign('action', 'account');
        $this->display('views-account.html');
    }


    function banner(){
        $db = D("Redis");
        $list = $db->getBanner();
        $_tmp = array();
        foreach($list as $key=>$val){
            $val = json_decode($val, true);
            $val['ccost'] = $db->getHashValue('CAMPAIGNID_DAILY_COST', $val['campaignid']);
            $val['gcost'] = $db->getHashValue('GROUP_DAILY_COST', $val['campaignid'].'_'.$val['adgroupid']);
            $val['dclick'] = $db->getHashValue('ADID_DAILY_CLICK_COUNTER', $val['adid']);
            $_tmp[$val['adid']] = $val;
        }
        if(isset($_GET['original'])){
            print_r(json_encode($_tmp));exit;
        }
        $this->assign('action', 'adlist');
        $this->assign('list', $_tmp);
        $this->display('views-banner.html');
    }

    function popup(){
        $db = D("Redis");
        $list = $db->getPopup();
        $_tmp = array();
        foreach($list as $key=>$val){
            $val = json_decode($val, true);
            $val['ccost'] = $db->getHashValue('CAMPAIGNID_DAILY_COST', $val['campaignid']);
            $val['gcost'] = $db->getHashValue('GROUP_DAILY_COST', $val['campaignid'].'_'.$val['adgroupid']);
            $val['dclick'] = $db->getHashValue('ADID_DAILY_CLICK_COUNTER', $val['adid']);
            $_tmp[$val['adid']] = $val;
        }
        if(isset($_GET['original'])){
            print_r(json_encode($_tmp));exit;
        }
        $this->assign('action', 'adlist');
        $this->assign('list', $_tmp);
        $this->display('views-popup.html');
    }
    
    function feeds(){
        $db = D("Redis");
        $list = $db->getFeeds();
        $_tmp = array();
        foreach($list as $key=>$val){
            $val = json_decode($val, true);
            $val['ccost'] = $db->getHashValue('CAMPAIGNID_DAILY_COST', $val['campaignid']);
            $val['gcost'] = $db->getHashValue('GROUP_DAILY_COST', $val['campaignid'].'_'.$val['adgroupid']);
            $val['dclick'] = $db->getHashValue('ADID_DAILY_CLICK_COUNTER', $val['adid']);
            $_tmp[$val['adid']] = $val;
        }
        if(isset($_GET['original'])){
            print_r(json_encode($_tmp));exit;
        }
        $this->assign('action', 'adlist');
        $this->assign('list', $_tmp);
        $this->display('views-feeds.html');
    }

    function adtask(){
        $db = D("Redis");
        $list = $db->getAdTask();
        $_tmp = array();
        foreach($list as $key=>$val){
            $val = json_decode($val, true);
            $val['ccost'] = $db->getHashValue('CAMPAIGNID_DAILY_COST', $val['campaignid']);
            $val['gcost'] = $db->getHashValue('GROUP_DAILY_COST', $val['campaignid'].'_'.$val['adgroupid']);
            $val['dclick'] = $db->getHashValue('ADID_DAILY_CLICK_COUNTER', $val['adid']);
            $val['dcompleted'] = $db->getHashValue('WALL_TASK_DAILY_COMPLETED_COUNTER', $val['adid']);
            $_tmp[$val['adid']] = $val;
        }
        if(isset($_GET['original'])){
            print_r(json_encode($_tmp));exit;
        }
        $this->assign('action', 'adlist');
        $this->assign('list', $_tmp);
        $this->display('view-adtask.html');
    }

    function adpredown(){
        $db = D("Redis");
        $list = $db->getAdPredown();
        $_tmp = array();
        foreach($list as $key=>$val){
            $val = json_decode($val, true);
            $_tmp[$val['adid']] = $val;
        }
        if(isset($_GET['original'])){
            print_r(json_encode($_tmp));exit;
        }
        $this->assign('action', 'adlist');
        $this->assign('list', $_tmp);
        $this->display('view-adpredown.html');
    }
    //积分墙监控
    function adtaskmonitor(){
        $db = D("Redis");
        $dvid = isset($_POST['dvid'])? trim($_POST['dvid']):'';
        $sdvid = $dvid;
        $ostype = isset($_POST['ostype'])?trim($_POST['ostype']):'';
        $list = array();
        $where['ostype'] = $ostype;
        if(!empty($dvid) && !empty($ostype))
        {
            if($ostype == "ANDROID")
            {
                $dvid = strtolower($dvid);
                $rldvid = $db->getRelationDvid($dvid);
                if(empty($rldvid))
                {
                    $dvid = strtoupper($dvid);
                    $rldvid = $db->getRelationDvid($dvid);
                }
                if(!empty($rldvid))
                {
                    $where['dvid'] = $rldvid;
                }
                else
                {
                    $where['dvid'] = $dvid;
                }
            }
            else
            {
                $where['dvid'] = $dvid;
            }
            $where['dvid'] = strtolower($where['dvid']);
            $list = $db->getAdTaskMonitor($where);
            if(empty($list))
            {
                $where['dvid'] = strtoupper($where['dvid']);
                $list = $db->getAdTaskMonitor($where);
            }
            $where['hkey'] = "{$where['ostype']}_{$where['dvid']}_WALL_TASK_LIST";
            $where['dvid'] = $sdvid;
            $this->assign('where', $where);
        }
        $this->assign('list', $list);
        $this->display('adtaskmonitor.html');
    }
    //详细积分墙监控
    function adtaskmonitordetail(){
        $db = D("Redis");
        $hkey = isset($_GET['hkey'])? trim($_GET['hkey']):'';
        $key = isset($_GET['key'])?trim($_GET['key']):'';
        $list = array();
        if(!empty($hkey) && !empty($key))
        {
            $where['hkey'] = $hkey;
            $where['key'] = $key;
            $list = $db->getAdTaskMonitorDetail($where);
            $this->assign('where', $where);
        }
        $this->assign('list', $list);
        $this->display('adtaskmonitordetail.html');
    }

    //详细积分墙监控
    function httpmonitordetail(){
        $db = D("Redis");
        $list = array();
        $list = $db->getHTTPMonitorDetail();
        $this->assign('list', $list);
        $this->display('httpmonitordetail.html');
    }

    //redis监控
    function redislist(){
        $this->display('redislist.html');
    }

    //redis监控
    function redisinfo(){
        $h=$_GET['h'];
        $p=$_GET['p'];
        $_redis = new Redis();
        if (!($_redis->connect( $h, $p, 2))){
            $info = '此redis不存在。';
        }
        else 
        {
            $info = $_redis->info();
        }
        
        $this->assign('info', $info);
        $this->assign('h', $h);
        $this->assign('p', $p);
        $this->display('redisinfo.html');
    }

    function moregame(){
        $db = D("Redis");
        $list = $db->getMoregame();
        $_tmp = array();
        foreach($list as $key=>$val){
            $val = json_decode($val, true);
            $val['ccost'] = $db->getHashValue('CAMPAIGNID_DAILY_COST', $val['campaignid']);
            $val['gcost'] = $db->getHashValue('GROUP_DAILY_COST', $val['campaignid'].'_'.$val['adgroupid']);
            $val['dclick'] = $db->getHashValue('ADID_DAILY_CLICK_COUNTER', $val['adid']);
            $_tmp[$val['adid']] = $val;
        }
        if(isset($_GET['original'])){
            print_r(json_encode($_tmp));exit;
        }
        $this->assign('action', 'adlist');
        $this->assign('list', $_tmp);
        $this->display('views-moregame.html');
    }

    function command(){
        $this->assign('action', 'command');
        $this->display('views-command.html');
    }

    function getcommand(){
        $command = isset($_POST['command']) ? trim($_POST['command']) : '';
        $port=$_POST['port'];
        $ports=array("6379","6380","6381","6383","6384","6386","6387","6388","6389");
        if(empty($command)){
            ajaxReturn('0', '参数错误！');
        }
        $pre = strtolower(substr($command,0,6));
        $pres=array("select","delete","insert");
        if($port=='3306'){
        	if($pre == 'select'){
	            $db = D("Cron");
	            $data = $db->getDbQuery($command);
	            ajaxReturn('1','ok!', $data);
        	}
        	else{
        		ajaxReturn('0', '数据库：仅允许执行select命令！');
        	}
        }else if(in_array($port, $ports)){
            if(in_array($pre, $pres)){
            	ajaxReturn('0', 'Redis：仅允许执行redis命令！');
            }
            else{
            	$arr = explode(' ', $command);
	            $db = Dredis("Redis",$port);
	            $data = $db->getRedisData($arr);
	            ajaxReturn('1','ok.', $data);
            }
        }
        else{
        	ajaxReturn('0', '参数错误！');
        }
    }

    function phpinfo(){
        phpinfo();
    }
    
    /**
     * 菜单栏查看DSP广告列表
     */
    function dsp(){
        $db = D("Redis");
        $list = $db->getDsp();
        $_tmp = array();
        foreach($list as $key=>$val){
            $val = json_decode($val, true);
            $val['ccost'] = $db->getHashValue('CAMPAIGNID_DAILY_COST', $val['campaignid']);
            $val['gcost'] = $db->getHashValue('GROUP_DAILY_COST', $val['campaignid'].'_'.$val['adgroupid']);
            $val['dclick'] = $db->getHashValue('ADID_DAILY_CLICK_COUNTER', $val['adid']);
            $_tmp[$val['adid']] = $val;
        }
        if(isset($_GET['original'])){
            print_r(json_encode($_tmp));exit;
        }
        $this->assign('action', 'adlist');
        $this->assign('list', $_tmp);
        $this->display('views-dsp.html');
    }
}

<?php
class DefaultController extends CommonController
{
	function index(){
		$this->display('default.html');
	}

	function reset(){
		$db = D("User");
		$db->userSet();
	}

	function login(){
		$db = D("User");
		if($db->login()){
			//$_SESSION['uid'] = 1;
			setcookie("uid",'1',time() + 86400000, '/');
			ajaxReturn(1, '登录成功');
		}
		ajaxReturn(0, '登录失败');
	}

	function logout(){
		//$_SESSION['uid'] = null;
		setcookie("uid",'',time()-3600);
		header("Location: /");
	}

	function userpassedit(){
		exit('暂不可用');
	}
}
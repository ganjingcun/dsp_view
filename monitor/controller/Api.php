<?php
header('Content-Type: text/html; charset=utf-8');
set_time_limit(0);
class ApiController extends Controller
{
    function iosOfferWallList(){
        $db = D("Redis");
		$_tmp = array();

		//普通iOS积分墙
		$wallist = $db->getIosOfferWallList();
		if(!empty($wallist)){

			foreach($wallist as $key=>$val){
				$val = json_decode($val, true);
				$_tmp[$val['adid']] = $val;
			}
		}

		//Top iOS积分墙
		$topwallist = $db->getTopIosOfferWallList();
		if(!empty($topwallist)){

			foreach($topwallist as $key=>$val){
				$val = json_decode($val, true);
				$_tmp[$val['adid']] = $val;
			}
		}
		
		return (($_tmp));
	}
	
	
	/**
	 * 王飞
	 * 2015-03-31
	 * 获取广告分类对应的用户标签入口
	 */
	function getAdcategory(){
		$limit=100;
		$ostypeid=array(1,2);
		foreach ($ostypeid as $key=>$os) {
			$this->getAdcategoryByJSON($os,$limit);
		}
	}
	
	/**
	 * 王飞
	 * 2015-03-31
	 * 获取广告分类对应的用户标签主要函数
	 */
	function getAdcategoryByJSON($os,$limit){
		$url="http://ad-dmp.cocounion.com:8080/retrieve/relatedadtype?secretKey=zhou&secretId=123";
		$postdata['adcategory']=array();
		$postdata['limit']=$limit;
		$postdata['os']=$os;
		$data=json_encode($postdata);
		$header=array('Content-Type:application/json','charset=utf-8','Content-Length:'.strlen($data));
		$ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);  //这是你想用PHP取回的URL地址
        curl_setopt($ch, CURLOPT_POST, 1);  //启用POST提交,启用时会发送一个常规的POST请求，类型为：application/x-www-form-urlencoded，就像表单提交的一样
//        curl_setopt($ch, CURLOPT_HEADER, false);    //启用时会将头文件的信息作为数据流输出。表示需要response header
//        curl_setopt($ch, CURLOPT_NOBODY, false); //	启用时将不对HTML中的BODY部分进行输出。
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //将curl_exec()获取的信息以文件流的形式返回，而不是直接输出
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data); //传递一个作为HTTP “POST”操作的所有数据的字符串 
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);//一个用来设置HTTP头字段的数组
        $ret_data=curl_exec($ch); 
        $http_retcode=curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_setopt($ch, CURLOPT_POST, 0); //当前面设置set ($ch, curlopt_post, 1)时，用curl_exec  post数据后，需要重新设置CURLOPT_POST为0，否则，会有500或者403错误。
        curl_close($ch);  
		if ($http_retcode=='200') {
			$obj= json_decode($ret_data,true);
			if($obj['errorCode']==100){
				print "<h3>无数据</h3>";
			}
			elseif ($obj['errorCode']==400){
				print "<h3>参数错误</h3>";
			}
			else {
				$where["ostypeid"]=$os;//为了防止清空数据库，仅在取到数据时才删除对应os的数据
				$this->deleteTb($where);
				print ("<h3>成功删除ostypeid为{$os}的数据</h3>");
				$db = D("RelatedCategoryMapping");
				$insert['ostypeid']=$os;
				foreach ($obj['categories'] as $categoryI) {
					foreach ($categoryI as $adcateid => $value) {
						$insert['adcateid']=$adcateid;
						$insert['relatedadcateid']=$value['category'];
						$insert['weight']=$value['weight'];
						$insert['createtime']=time();
						$db->addOne($insert);
					}
				}
				print ("<h3>成功保存 ostypeid为{$os}的数据</h3>");
			}
		}
		else 
			print("<h3>请求失败{$http_retcode}</h3>");
	}
	
	
	/**
	 * 王飞
	 * 2015-03-31
	 * 清空Model:RelatedCategoryMapping对应的table
	 */
	function clearTb(){
		$db = D("RelatedCategoryMapping");
		$db->deleteAll();
	}
	
	/**
	 * 删除数据库表related_category_mapping中,ostypeid为1或者2的数据
	 * @param $data  
	 * array('ostypeid':1/2)
	 * wangfei
	 * 2015-04-01
	 */
	function deleteTb($data){
		$db = D("RelatedCategoryMapping");
		$count=$db->getCount();
		if($count>0){
			$db->delete($data,$count);	
		}
	}
	
	 
	
        
}

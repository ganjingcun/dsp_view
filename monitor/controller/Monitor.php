<?php
class MonitorController extends CommonController
{

    function index(){

    }

    //详细积分墙监控
    function httpmonitordetail(){
        $db = D("Redis");
        $list = array();
        $list = $db->getHTTPMonitorDetail();
        $this->assign('action', 'monitor');
        $this->assign('list', $list);
        $this->display('httpmonitordetail.html');
    }

    //redis监控
    function redislist(){
        $this->assign('action', 'monitor');
        $this->display('redislist.html');
    }

    //redis监控
    function redisinfo(){
        $h=$_GET['h'];
        $p=$_GET['p'];
        $_redis = new Redis();
        if (!($_redis->connect( $h, $p, 2))){
            $info = '此redis不存在。';
        }
        else
        {
            $info = $_redis->info();
        }
        $this->assign('action', 'monitor');
        $this->assign('info', $info);
        $this->assign('h', $h);
        $this->assign('p', $p);
        $this->display('redisinfo.html');
    }

}

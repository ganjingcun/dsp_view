<?php
class CommonController extends Controller
{
	public function __construct() {
		parent::__construct();
		if(Module != 'DefaultController'){
			if(!isset($_COOKIE['uid']) || $_COOKIE['uid'] < 1){
				header("Location: /\n");
				exit;
			}
		}
		$this->assign('action', '');
		if(isset($_COOKIE['uid'])){
			$this->assign('islogin', 1);
		}else{
			$this->assign('islogin', 0);
		}
	}
}

<?php
class RedisModel{
    protected $_redis = null;
    protected $taskmonitormark = array("lct"=>"返还积分前作弊类型(在返积分之前，可能会更新状态，该字段记录更改前的状态)", "transactionid" => "UUID用户点击任务时生成的唯一ID", "callbackUrl" => "", "device" => "", "mac" => "", "os" => "", "os_version" => "", "ip" => "客户端ip地址", "idfa" => "", "imei" => "", "uniqid" => "", "fingerprint" => "", "clt" => "点击时间戳[可以提供但是如果时间格式不对则无法转为时间戳以后建议SDK直接上传时间戳", "dn" => "DeviceName设备名称", "model" => "设备型号", "brand" => "设备品牌", "mcc" => "运营商国家代码", "mnc" => "运营商网络代码", "ca" => "运营商显示名", "kst" => "设备启动时间", "net" => "网络类型(1、wifi2、2G3、3G4、代理、5其他)", "rm" => "接入点路由器的MAC", "rs" => "接入点路由器的SSD", "events" => "", "params" => "", "status" => "任务状态", "triggerevent" => "触发任务的事件", "endTime" => "任务有效期", "dvid" => "系统选中的唯一设备ID", "taskid" => "广告平台分配的内部任务ID(目前只支持一个任务)", "coins" => "积分墙需要返还的积分数量", "coinunit" => "积分墙需要返还的积分单位", "sid" => "sessionid", "faduid" => "feedAdUniqueId", "app" => "", "pid" => "publisherID", "appid" => "appid", "appcampaignid" => "app在追踪服务器中的id", "userid" => "userID", "wallexchange" => "积分兑换比例", "rechargetype" => "积分墙积分返还方式(0 => sdk 1 => 服务器对接)", "test" => "测试模式标识(0、正常1、测试)", "token" => "app需要返还的token", "rr" => "积分墙积分保留(扣量)系数", "ad" => "", "adowner" => "广告所属用户组", "campaignid" => "广告活动ID", "grpid" => "广告组ID", "adid" => "创意ID", "adtitle" => "文字广告标题", "pkg" => "包名", "unitprice" => "广告单价", "appwallecpa" => "分成比例", "relaytype" => "追踪类型  (1 CocoData  3 广告SDK 4 广告主激活)", "trackeventtype" => "追踪的事件类型(1下载2激活3注册4点击99自定义)", "cr" => "作弊返还比例", "trackcampaignid" => "追踪服务器分配的追踪ID", "devicetype" => "设备类型", "idfv" => "", "taskname" => "任务名称", "taskcontent" => "任务描述");
    function __construct() {
    	$a = func_get_args();
        $i = func_num_args();
        if (method_exists($this,$f='__construct'.$i)) {
            call_user_func_array(array($this,$f),$a);
        } 
        else{
        	$this->_redis = getRedis();
        }
    }
    
    function __construct1($port){
    	 $this->_redis =getRedisByPort($port,false);
    }

    /**
     * 获取指定publisherID的应用开关数据
     * @return boolean
     */
    function getAppInfo(){
        $publisherID = spost('pid');
        $canalid = $this->_redis->hget("PUBLISHERID_APPINFO", $publisherID);
        if($canalid){
            $info = $this->_redis->hget('APP_INFO', $canalid);
            if($info){
                $info = json_decode($info,true);
                $info['publisherID'] = $publisherID;
                return $info;
            }
        }
        return false;
    }

    /**
     * 获取所有账户信息
     * @return type
     */
    function getAccountRecharged(){
        return $this->_redis->hgetall("ACCOUNT_CHARGED");
    }

    function getAccountDeposit(){
        return $this->_redis->hgetall("ACCOUNT_DEPOSIT");
    }

    /**
     * 获取所有banner广告
     * @return type
     */
    function getBanner(){
        $android = $this->_redis->hgetall('AD_BANNER_INFO_ANDROID');
        $ios = $this->_redis->hgetall("AD_BANNER_INFO_IOS");
        return array_merge($android, $ios);
    }
    
	/**
     * 获取所有feeds广告
     * @return type
     */
    function getFeeds(){
        $android = $this->_redis->hgetall('AD_FEEDS_INFO_ANDROID');
        $ios = $this->_redis->hgetall("AD_FEEDS_INFO_IOS");
        return array_merge($android, $ios);
    }

    /**
     * 获取hash的只
     * @return type
     */
    function getHashValue($hkey, $fkey){
        $value = $this->_redis->hget($hkey, $fkey);
        return $value;
    }

    function getPopup(){
        $android = $this->_redis->hgetall('AD_POPUP_INFO_ANDROID');
        $ios = $this->_redis->hgetall("AD_POPUP_INFO_IOS");
        return array_merge($android, $ios);
    }

    function getMoregame(){
        $android = $this->_redis->hgetall('AD_MOREGAME_INFO_ANDROID');
        $androidtop = $this->_redis->hgetall('TOP_AD_MOREGAME_INFO_ANDROID');
        $ios = $this->_redis->hgetall("AD_MOREGAME_INFO_IOS");
        $iostop = $this->_redis->hgetall("TOP_AD_MOREGAME_INFO_IOS");
        return array_merge($android, $ios, $androidtop, $iostop);
    }

    function getRedisData($arr){
        $data = '';
        if(isset($arr[0]) && in_array(strtolower($arr[0]),array('del', 'flushall', 'flushdb'))){
            return '不允许执行该命令';
        }
        if(count($arr) ==2){
            //$data = call_user_func_array(array($this->_redis, $arr[0]),$arr[1]);
            $data = $this->_redis->$arr[0]($arr[1]);
        }
        if(count($arr) == 3){
            //$data = call_user_func_array(array($this->_redis, $arr[0]), array($arr[1], $arr[2]));
            $data = $this->_redis->$arr[0]($arr[1], $arr[2]);
        }
		if(count($arr) == 4){
            $data = $this->_redis->$arr[0]($arr[1], $arr[2], $arr[3]);
        }
        if(count($arr)==1){
            $data = $this->_redis->$arr[0]();
        }
        if(!is_array($data)){
            $data = $data;
        }else{
            $data = print_r($data, true);
        }
        return '<pre>'.$data.'</pre>';
    }

    function getAdTask(){
        $list = array();
        $android = $this->_redis->hgetall('AD_WALL_INFO_ANDROID');
        $androidtop = $this->_redis->hgetall('TOP_AD_WALL_INFO_ANDROID');
        $ios = $this->_redis->hgetall("AD_WALL_INFO_IOS");
        $iostop = $this->_redis->hgetall("TOP_AD_WALL_INFO_IOS");
        return array_merge($android, $ios, $androidtop, $iostop);
    }

    function getAdTaskMonitor($where){
        $list = array();
        if(empty($where))
        {
            return $list;
        }
        $_monitorredis = getMonitorRedis();
        $keys = "{$where['ostype']}_{$where['dvid']}_WALL_TASK_LIST";
        $list = $_monitorredis->hgetall($keys);
        if(empty($list))
        {
            return array();
        }
        $data = array();
        foreach ($list as $k=>$v)
        {
            $tmp = json_decode($v, true);
            //$data[$k] = $this->formatmonitor($tmp);
            $tmp['params']['statustxt'] = '未知';
            if(isset($tmp['params']['status']))
            {
                switch ($tmp['params']['status'])
                {
                    case 1:
                        $tmp['params']['statustxt'] = '进行中';
                        break;
                    case 2:
                        $tmp['params']['statustxt'] = '任务完成';
                        break;
                    case 3:
                        $tmp['params']['statustxt'] = '充值中(已领取)';
                        break;
                }
            }
            $tmp['params']['app']['rechargetypetxt'] = '未知';
            if(isset($tmp['params']['app']['rechargetype']))
            {
                switch ($tmp['params']['app']['rechargetype'])
                {
                    case 0:
                        $tmp['params']['app']['rechargetypetxt'] = 'SDK';
                        break;
                    case 1:
                        $tmp['params']['app']['rechargetypetxt'] = '服务器返还';
                        break;
                    case 3:
                        break;
                    case 4:
                        $tmp['params']['app']['rechargetypetxt'] = '广告主激活';
                        break;
                }
            }
            $tmp['params']['cttxt'] = '未作弊';
            if(isset($tmp['params']['ct']))
            {
                switch ($tmp['params']['ct'])
                {
                    case '':
                        break;
                    case 0:
                    	$tmp['params']['cttxt'] = '未作弊';
                        break;
                    case 1:
                    	$tmp['params']['cttxt'] = '作弊';
                        break;
                    case 2:
                        $tmp['params']['cttxt'] = '重复';
                        break;
                    case 3:
                        $tmp['params']['cttxt'] = '点击判定作弊';
                        break;
                    case 4:
                        $tmp['params']['cttxt'] = '激活作弊';
                        break;
                    case 5:
                        $tmp['params']['cttxt'] = '请求作弊';
                        break;
                    case 6:
                        $tmp['params']['cttxt'] = '视频作弊';
                        break;
                    case 100:
                        $tmp['params']['cttxt'] = '核心随机扣量';
                        break;
                }
            }
            if(isset($tmp['params']['stime']))
            {
                $tmp['clt'] = $tmp['params']['stime'];
            }
            else
            {
                $tmp['clt'] = $tmp['fingerprint']['clt'];
            }
            $data[$k] = $tmp;
        }
        return $data;
    }

    function getAdTaskMonitorDetail($where){
        $list = array();
        if(empty($where))
        {
            return $list;
        }
        $_monitorredis = getMonitorRedis();
        $hkeys = $where['hkey'];
        $keys = $where['key'];
        $list = $_monitorredis->hget($hkeys, $keys);
        if(empty($list))
        {
            return array();
        }
        $data = array();
        $tmp = json_decode($list, true);
        $data = $this->formatmonitor($tmp);
        return $data;
    }

    function getHTTPMonitorDetail(){
        $list = array();
        $_monitorredis = getRedis6388();
        $keys = $_monitorredis->keys('HOSTNAME*');
        $data = array();
        foreach ($keys as $k=>$v)
        {
            $tmpdata = $_monitorredis->hgetall($v);
            $data[$v] = $tmpdata;
        }
        return $data;
    }


    function getRelationDvid($dvid){
        if(empty($dvid))
        {
            return '';
        }
        $_monitorredis = getRedis6386();
        $val = $_monitorredis->get($dvid);
        if(empty($val))
        {
            return '';
        }
        return $val;
    }

    function formatmonitor($data)
    {
        foreach($data as $k=>$v)
        {
            if(is_array($v))
            {
                $data[$k] = $this->formatmonitor($v);
            }
            else
            {
                if(isset($this->taskmonitormark[$k]))
                {
                    $data[$k] = $v . ' (' .$this->taskmonitormark[$k].')';
                }
                else
                {
                    $data[$k] = $v;
                }
                if("$k" == 'confirmtime')
                {
                    $data[$k] = date('Y-m-d H:i:s', $v/1000) . ' (积分墙任务完成时间)'.$v;
                }
                if("$k" == 'clt')
                {

                    $data[$k] = date('Y-m-d H:i:s', $v/1000) . ' (sdk上传的点击时间)'.$v.'|'.$k;
                }
                if("$k" == 'stime')
                {

                    $data[$k] = date('Y-m-d H:i:s', $v/1000) . ' (服务器的点击时间)'.$v.'|'.$k;
                }
            }
        }
        return $data;
    }



    function getAdPredown(){
        $list = array();
        return $this->_redis->hgetall('AD_PRE_DOWNLOAD');
    }

    function getIosOfferWallList(){
        $list = array();
        return $this->_redis->hgetall('AD_WALL_INFO_IOS');
    }

    function getTopIosOfferWallList(){
        $list = array();
        return $this->_redis->hgetall('TOP_AD_WALL_INFO_IOS');
    }
    
    
    /**
     * 获取所有DSP广告
     */
    function getDsp(){
        $android = $this->_redis->hgetall('AD_DSP_INFO_ANDROID');
        $ios = $this->_redis->hgetall("AD_DSP_INFO_IOS");
        return array_merge($android, $ios);
    }
}
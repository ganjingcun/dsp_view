<?php
class CronModel extends Model{
    protected $_redis = null;
    protected $_db = '';
    private $_isupdate = '';
    private $_os = array("2"=>"IOS", "1"=>"ANDROID");
    private $lastsql = '';
    private $_where = ' and ads.status=1 and ag.status=1 and ac.status < 3 ';
    private $_cpm_ratio = 1000;
    private $_nowtime = '';
    private $_ac_fields = " ac.currency,ac.newdeviceset,ac.dmpset,ac.targettype,ac.adchildtypeid adcategories, ac.apptypesetid appcategories,ac.isupdate, ac.apptypeset, ac.campaignid,ac.devicetypeset device_types,ac.ostypeid ostype,ac.userid,ac.daybudget,ac.osversionset,ac.aosvset,ac.iosvset,ac.sdkversionset,ac.asdkvset,ac.isdkvset,ac.nettypeset network_types,ac.operatorsset carrieroperators,ac.areaset country_codes,ac.areaids domain_codes ,ac.domainfiltertype domainfiltertype, ac.prisonbreak jailbreak_types,ac.periodset periods ,'1' own,";
    private $_ag_fields = " ag.targetwalls, ag.`pattern`,ag.`exclusivegda`,ag.`gdatag`,ag.`gdapriority`,ag.`adgroupid`,ag.defaultbudget,ag.scheduletype,ag.`chargemode`,ag.`price` unit_price,ag.adform bought_type,ag.`starttime`,ag.`endtime`+86399 endtime,ag.adtargettype appIdfiltertype,ag.adsubtargettype siteidfiltertype,ag.clickfrequency,ag.effecttype click_effect,ag.frequencyperiod,ag.frequencyperiod,ag.frequencyset,ag.frequencytype,ag.frequencytype,ag.ignoretype retargettype,ag.showfrequency,ag.showfrequency,ag.sysdiridfafile,ag.sysdirmacfile,ag.daybudget groupbudget,ag.totalfrequency,ag.totalfrequency,ag.adsensortrackurl trackurl,";
    private $_ads_fields = " ads.usewake, ads.wakeurl, ads.orientation, ads.storeid, ads.`adstuffname` creative_title,ads.`stuffid` adid,ads.`target` url,ads.appversioncode appversion,ads.click_rate,ads.packagename,ads.appsize,ads.stufftype creative_type,ads.appversionname,ads.callbackurl,ads.istrackidfa,ads.lowerlimit,ads.upperlimit,ads.isapi,";

    function __construct() {
        parent::__construct();
        $this->_db = $this->getInstance(0);
        $this->_redis = getRedis();
        $this->_nowtime = strtotime(date('Y-m-d'));
    }

    /**
     * 广告主账户信息
     * @return type
     */
    function aduserinfo(){
        $sql = "select userid,totalmoney recharged,totalcost deposit from `user_member` where 1" . $this->_isupdate;
        $res = $this->_db->query($sql);
        return $this->_db->fetch_array($res);
    }

    /**
     * 获取广告是否有更新
     * @return type
     */
    function getIsUpdate(){
        $sql = "select campaignid,isupdate from `ad_campaign` where isupdate != 0";
        $res = $this->_db->query($sql);
        return $this->_db->fetch_array($res);
    }

    /**
     * 还原广告更新键值
     * @return type
     */
    function setIsUpdate($data){
        $sql = '';
        if(empty($data))
        {
            return ;
        }
        foreach ($data as $k=>$v)
        {
            if($v['isupdate'] < 0)
            {
                $sql = "update ad_campaign set isupdate = 0 where campaignid = ".$v['campaignid'].";";
            }
            else
            {
                $sql = "update ad_campaign set isupdate = (isupdate - ".$v['isupdate'].") where campaignid = ".$v['campaignid'].";";
            }
            $this->_db->query($sql);
        }
    }

    /**
     * 获取切量PID列表信息
     * @return type
     */
    function cutoversetlist(){
        $sql = "SELECT  * FROM cutover_set WHERE isdel = 0 ";
        $res = $this->_db->query($sql);
        return $this->_db->fetch_array($res);
    }

    /**
     * 预加载刷新周期
     * @return type
     */
    function getPreloadSetInfo(){
        /*$sql = "select skey,sval from `system_config` where skey ='mgignoredevice' 
            or skey = 'taskignoredevice' or skey = 'antifraudswitch' or skey = 'popupignoredevice' 
            or skey = 'sdkaccumulatepointsdelay' or skey = 'serviceaccumulatepointsdelay'";*/
        $sql = "select skey,sval from `system_config` where skey ='mgignoredevice' 
            or skey = 'taskignoredevice' or skey = 'antifraudswitch' or skey = 'popupignoredevice' 
            or skey LIKE 'trackeventtype%' or skey ='dspcutovercount' or skey = 'dspcutoverfloorsswitch'";
        $res = $this->_db->query($sql);
        return $this->_db->fetch_array($res);
    }

    /**
     * web积分墙应用地址
     * @return type
     */
    function getWebIntegralUrlInfo(){
        $sql = "select skey,sval from `system_config` where skey in ('webintegralurl','webintegralerrurl')";
        $res = $this->_db->query($sql);
        return $this->_db->fetch_array($res);
    }

    /**
     * 预加载刷新周期
     * @return type
     */
    function getAdRotatorConfig(){
        $sql = "select * from `ad_rotator_config` where status = 0";
        $res = $this->_db->query($sql);
        return $this->_db->fetch_array($res);
    }

    /**
     * 忽略广告定向设备信息
     * @return type
     */
    function getignoredevice(){
        $sql = "select ignoreid from `ignore_device` ORDER BY `edittime` DESC LIMIT 0, 500;";
        $res = $this->_db->query($sql);
        return $this->_db->fetch_array($res);
    }

    /**
     * App广告信息
     * @return type
     */
    function appinfo(){
        $sql = "select app_info.ostypeswitch,app_info.appname,app_info.dmpswitch, app_info.s2sswitches, 
        app_info.s2sclkrdt, app_info.secretkey,app_info.packagename, app_info.lowqualityswitch, 
        app_info.appid,app_info.userid,app_info.debugsdkswitch,app_info.antifraudswitch,app_info.fullwtlistswitch,app_info.s2smgdetailsswitch,
        app_info.ostypeid,app_info.bannermgswitch,app_info.bannermgtime as bannermgctdown,
        case app_info.adminswitch when 0 then 0 when 1 then app_info.switch end switch,app_info.appori,
        app_info.uptime,app_info.isupdate,app_info.apptypeid,app_info.appchildtypeid,app_info.auditing,
        app_info.unit wallunit,app_info.integration wallexchange,app_info.wallrechargetype wallrechargetype,
        app_info.appcampaignid appcampaignid,ad_ecpc_app.wall wallecpa,ad_ecpc_app.wallratio wallreserveratio,
        app_info.feedsswitch, app_info.feedsiostpl, app_info.feedsandroidtpl,app_info.feedsimprdt, app_info.feedsclkrdt,
        app_info.apiauthswitch,app_info.dspfloorsswitch, account_info.currency,
        app_info.chance_publisherID, app_info.chance_secretkey from app_info";
        $sql .= " left join ad_ecpc_app on ad_ecpc_app.appid=app_info.appid and `status`=1 left join account_info on app_info.userid = account_info.userid where 1 ". $this->_isupdate;
        $res = $this->_db->query($sql);
        return $this->_db->fetch_array($res);
    }

    /**
     * publisherID to appid
     */
    function punchboxinfo(){
        $sql = "select appid,publisherid,ctr,atr,cpmratio,isupdate from app_channel where 1". $this->_isupdate;
        $res = $this->_db->query($sql);
        return $this->_db->fetch_array($res);
    }

    /**
     * appclickeffectinfo to appid
     */
    function getappclickeffectinfo(){
        $sql = "select appid, adform, effecttype from app_clickeffect";
        $res = $this->_db->query($sql);
        $resdata = $this->_db->fetch_array($res);
        $data = array();
        if(!empty($resdata))
        {
            foreach($resdata as $k=>$v)
            {
                $data[$v['appid']][$v['adform']] = $v['effecttype'];
            }
        }
        return $data;
    }


    /**
     * 获取所有OS版本信息
     * Enter description here ...
     */
    function getOsVersion(){
        static $osversion;
        if(!$osversion){
            $res = $this->_db->query("select * from os_version order by ostype asc,osversion asc");
            $tmp = $this->_db->fetch_array($res);
            foreach($tmp as $val){
                $osversion[$val['ostype']][$val['osversion']] = $val['osversion'];
            }
        }
        return $osversion;
    }
    /**
     * 获取所有SDK版本信息
     * Enter description here ...
     */
    function getSDKVersion(){
        static $sdkversion;
        if(!$sdkversion){
            $res = $this->_db->query("select * from sdkversion_log");
            $tmp = $this->_db->fetch_array($res);
            foreach($tmp as $val){
                $sdkversion[$val['type']][$val['id']] = $val['version'];
            }
        }
        return $sdkversion;
    }

    /**
     * 取版本的开始结束区间
     * @param $os
     * @param $osset
     */
    function getOsVersionSection($ostype, $osset){
        $os = $this->getOsVersion();
        if(!isset($os[$ostype]) || !is_array($os[$ostype])) return '';
        $osexp = explode(',', $osset);
        if(!(count($osexp) == 2)){
            return '';
        }
        $tmp = array();
        foreach($os[$ostype] as $val){
            if($val >= $osexp[0] && $val <= $osexp[1]){
                $tmp[] = $val;
            }
        }
        if(!empty($tmp))
        return $tmp;
        return '';
    }

    /**
     * 取版本的开始结束区间
     * @param $os
     * @param $osset
     */
    function getSDKVersionSection($ostype, $sdkv){
        $os = $this->getSDKVersion();
        if(!isset($os[$ostype]) || !is_array($os[$ostype]) || empty($sdkv)) return '';
        $osexp = explode(',', $sdkv);
        $tmp = array();
        foreach($osexp as $val){
            if(isset($os[$ostype][$val])){
                $tmp[] = $os[$ostype][$val];
            }
        }
        if(!empty($tmp))
        return $tmp;
        return '';
    }

    /**
     * 广告信息
     */
    function getAdInfo($adform = 1){
        $isupdate = '';
        if($this->_isupdate){
            $isupdate = " and ac.isupdate > 0 ";
        }
        $time = $this->_nowtime;
        $sql = "SELECT {$this->_ac_fields} {$this->_ag_fields} {$this->_ads_fields}
              ag.ignoretype retargettype,
              IFNULL(ag.otherid,'') AS trackcampaignid,
              asis.`width`,
              asis.`height`,
              asi.`path`
                FROM
              `ad_stuff_img` asi 
              LEFT JOIN `ad_stuff_img_size` asis 
                ON asi.sizeid = asis.`sizeid` 
              LEFT JOIN `ad_stuff` ads 
                ON ads.`stuffid` = asi.`stuffid` 
              LEFT JOIN ad_group ag 
                ON ag.adgroupid = ads.`adgroupid` 
              LEFT JOIN ad_campaign ac 
                ON ac.campaignid = ag.campaignid 
            WHERE 1 and ads.ispause = 0 and ag.adform = $adform and ag.starttime <= $time and ag.endtime >= $time {$this->_where} ".$isupdate;
        /*SDK版本，本期无，加SDK版本时或定向修改时，请注意文字 广告处理*/
        $res = $this->_db->query($sql);
        $this->lastsql = $sql;
        //echo $sql;
        //print_r($res);
        $list = $this->_db->fetch_array($res);
        $tmp = array();
        $text = '';//print_r($list);exit;
        foreach($list as $val){
            $val['ostype'] = $this->getOsType($val['ostype']);
            $val['path'] = PIC_URL.$val['path'];
            $val = $this->getUnitPrice($val);
            if($val['click_effect'] == 3){//应用内打开页面。
                $val['url'] = 'coco://openh5?redirect='.urlencode($val['url']);
            }
            $val = $this->getBothOsVersion($val);
            $val = $this->getBothSDKVersion($val);
            if($val['creative_type'] == 2){
                $text .= $val['adid'].',';
            }
            if($val['apptypeset'] == 0)
            {
                $val['appcategories'] = array();
            }
            else
            {
                $val['appcategories'] = explode(',', $val['appcategories']);
            }
            if(empty($val['adcategories']))
            {
                $val['adcategories'] = array();
            }
            else
            {
                $val['adcategories'] = explode(',', $val['adcategories']);
            }
            if($val['istrackidfa'] == 1)
            {
                if(strpos('?', $val['url']) === false)
                {
                    $val['url'] .= '?idfa=@COCOIDFA@';
                }
                else
                {
                    $val['url'] .= '&idfa=@COCOIDFA@';
                }
            }
            $val = $this->getCommon($val);
            if($val == false)
            {
                continue;
            }
            $file_path = pathinfo($val['path']);
            if(isset($tmp[$val['adid']])){
                $tmp[$val['adid']]['imgs'][$val['width'].'X'.$val['height']] = $val['path'];
                if($adform == 2)
                {
                    $tmp[$val['adid']]['imgs'][$val['width'].'X'.$val['height'].'L'] = $file_path['dirname'].'/'.$file_path['filename'].'L.jpg';
                }
            }else{
                $val['imgs'][$val['width'].'X'.$val['height']] = $val['path'];
                if($adform == 2)
                {
                    $val['imgs'][$val['width'].'X'.$val['height'].'L'] = $file_path['dirname'].'/'.$file_path['filename'].'L.jpg';
                }
                unset($val['path']);
                unset($val['width']);
                unset($val['height']);
                $tmp[$val['adid']] = $val;
            }
        }

        //文字广告
        if($adform == 1){
            $tmp = $this->getBannerText($tmp);
        }
        //popup图文广告补充文字部分
        if($adform == 2){
        	$tmp = $this->getPopText($tmp);
        }
        return $tmp;
    }
    
    function getPopText($tmp){
    	$adids='';
    	if($tmp){
	    	foreach($tmp as $v){
	    		$adids .= $v['adid'].",";
	    	}
    	}
    	$sql = "select stuffid, adtitle, adcontent, path from ad_stuff_text where stuffid in (".trim($adids,',').")";
    	$res = $this->_db->query($sql);
        $this->lastsql = $sql;
        $list = $this->_db->fetch_array($res);
        if($list){
        	foreach($list as $v){
        		$tmp[$v['stuffid']]['adtexttitle'] = $v['adtitle'];
        		$tmp[$v['stuffid']]['adtext'] = $v['adcontent'];
        		$tmp[$v['stuffid']]['adtextpath'] = PIC_URL.$v['path'];
        	}
        }
        return $tmp;
    }
    
    /**
     * 文字广告内容
     * Enter description here ...
     * @param unknown_type $tmp
     */
    function getBannerText($tmp){
        $isupdate = '';
        if($this->_isupdate){
            $isupdate = " and ac.isupdate > 0 ";
        }
        $time = $this->_nowtime;
        $sql = "SELECT {$this->_ac_fields} {$this->_ag_fields} {$this->_ads_fields}
              IFNULL(ag.otherid,'') AS trackcampaignid,
              ast.`path` adtextpath,
              ast.adcontent adtext,
              ast.adtitle adtexttitle,
              ast.adstylecolor bg_color,
              ast.adeffect ad_effect
            FROM
              `ad_stuff_text` ast 
              LEFT JOIN `ad_stuff` ads 
                ON ads.`stuffid` = ast.`stuffid` 
              LEFT JOIN ad_group ag 
                ON ag.adgroupid = ads.`adgroupid` 
              LEFT JOIN ad_campaign ac 
                ON ac.campaignid = ag.campaignid 
            WHERE 1 and ads.ispause = 0 and ag.adform = 1 and ag.starttime <= $time and ag.endtime >= $time {$this->_where} ".$isupdate;
        /*SDK版本，本期无，加SDK版本时或定向修改时，请注意文字 广告处理*/
        $res = $this->_db->query($sql);
        $this->lastsql = $sql;
        //echo $sql;
        $list = $this->_db->fetch_array($res);
        $text = '';
        foreach($list as $val){
            $val['ostype'] = $this->getOsType($val['ostype']);
            if($val['click_effect'] == 3){//应用内打开页面。
                $val['url'] = 'coco://openh5?redirect='.urlencode($val['url']);
            }
            $val = $this->getUnitPrice($val);
            $val = $this->getBothOsVersion($val);
            $val = $this->getBothSDKVersion($val);
            if($val['apptypeset'] == 0)
            {
                $val['appcategories'] = array();
            }
            else
            {
                $val['appcategories'] = explode(',', $val['appcategories']);
            }
            if(empty($val['adcategories']))
            {
                $val['adcategories'] = array();
            }
            else
            {
                $val['adcategories'] = explode(',', $val['adcategories']);
            }
            if($val['creative_type'] == 2){
                $text .= $val['adid'].',';
            }
            $val['adtextpath'] = PIC_URL.$val['adtextpath'];
            if($val['istrackidfa'] == 1)
            {
                if(strpos('?', $val['url']) === false)
                {
                    $val['url'] .= '?idfa=@COCOIDFA@';
                }
                else
                {
                    $val['url'] .= '&idfa=@COCOIDFA@';
                }
            }
            $val = $this->getCommon($val);
            if($val == false)
            {
                continue;
            }
            if($val['creative_type'] == 3)
            {
                $val['moving_txt'] = json_decode($val['adtext'], true);
                if(is_array($val['moving_txt']))
                {
                    $val['moving_txt'] = array_filter(array_values($val['moving_txt']));
                    $val['adtext'] = implode(',', $val['moving_txt']);
                }
            }
            $tmp[$val['adid']] = $val;
        }
        return $tmp;
    }

    /*精品推荐列表推送*/
    function getMoreGame(){
        $isupdate = '';
        if($this->_isupdate){
            $isupdate = " and ac.isupdate > 0 ";
        }
        $time = $this->_nowtime;
        $this->_ac_fields .= "ag.selfappset,";
        $sql = "SELECT {$this->_ac_fields} {$this->_ag_fields} {$this->_ads_fields}
              IFNULL(ag.otherid,'') AS trackcampaignid,
              ads.gatherurl,
              asm.appname adtitle,
              asm.appcontent adcontent,
              asm.path moregameicon,
              asm.istop,
              asm.isintegral,
              asm.integralsort as `order`,
              asm.topsort as `torder`,
              asm.apisort,
              asm.star,
              asm.isapi
            FROM
              `ad_stuff_moregame` asm 
              LEFT JOIN `ad_stuff` ads 
                ON ads.`stuffid` = asm.`stuffid` 
              LEFT JOIN ad_group ag 
                ON ag.adgroupid = ads.`adgroupid` 
              LEFT JOIN ad_campaign ac 
                ON ac.campaignid = ag.campaignid 
            WHERE 1 and ads.ispause = 0 and ag.adform = 3 and ag.starttime <= $time and ag.endtime >= $time {$this->_where} ".$isupdate;
        /*SDK版本，本期无，加SDK版本时或定向修改时，请注意文字 广告处理*/
        $res = $this->_db->query($sql);
        $this->lastsql = $sql;
        $list = $this->_db->fetch_array($res);
        $tmp = array();
        $text = '';
        $users = '';
        foreach($list as $val){
            $val['ostype'] = $this->getOsType($val['ostype']);
            $val = $this->getUnitPrice($val);
            $val = $this->getBothOsVersion($val);
            $val = $this->getBothSDKVersion($val);
            if($val['creative_type'] == 2){
                $text .= $val['adid'].',';
            }
            if($val['selfappset']){
                $users[$val['userid']] = $val['userid'];
            }
            $val['moregameicon'] = PIC_URL.$val['moregameicon'];
            if($val['apptypeset'] == 0)
            {
                $val['appcategories'] = array();
            }
            else
            {
                $val['appcategories'] = explode(',', $val['appcategories']);
            }
            if(empty($val['adcategories']))
            {
                $val['adcategories'] = array();
            }
            else
            {
                $val['adcategories'] = explode(',', $val['adcategories']);
            }
            $val = $this->getCommon($val);
            if($val == false)
            {
                continue;
            }
            $tmp[$val['adid']] = $val;
        }
        if(!empty($users)){//用户APp获取，用于自家广告推送时使用。
            $sql = "select appid,userid,ostypeid from app_info where userid in (".implode(',',$users).")";
            $res = $this->_db->query($sql);
            $applist = $this->_db->fetch_array($res);
            $applist = $this->getUserApp($applist);
            $tmp['applist'] = $applist;
        }
        return $tmp;
    }

    /**
     * 获取广告投放时间段
     * Enter description here ...
     * @param unknown_type $string
     */
    function getPeriodset($val){
        $sql = "SELECT * FROM ad_schedules WHERE adgroupid = {$val['adgroupid']} AND daydate = ".strtotime(date('Y-m-d'))." limit 1";
        $res = $this->_db->query($sql);
        $schedulelist = $this->_db->fetch_array($res);
        if(empty($schedulelist))
        {
            return false;
        }
        return array('schedules'=>$schedulelist[0]['schedules'], 'daybudget'=>$schedulelist[0]['daybudget']);
    }
    
    /*
     * 得到活跃的广告活动的ID
     */
    function getAdids(){
        $time = $this->_nowtime;
    	$sql = "select asf.stuffid from ad_stuff as asf, ad_group as ag, ad_campaign as ac
			where ac.status=2 and ac.campaignid=ag.campaignid
			and ag.status=1 and ag.starttime <= $time and ag.endtime >= $time
			and asf.status=1 and asf.ispause=0 and asf.adgroupid=ag.adgroupid";
    	$res = $this->_db->query($sql);
    	$list = $this->_db->fetch_array($res);
    	return $list;
    }

    /**
     * Feeds广告信息
     */
    function getFeedsAdInfo(){
        $isupdate = '';
        if($this->_isupdate){
            $isupdate = " and ac.isupdate > 0 ";
        }
        $time = $this->_nowtime;
        $sql = "SELECT {$this->_ac_fields} {$this->_ag_fields} {$this->_ads_fields}
              ag.ignoretype retargettype,
              IFNULL(ag.otherid,'') AS trackcampaignid,
              asis.`width`,
              asis.`height`,
              asi.`path`,
              ast.adcontent adtext,
              ast.adtitle adtexttitle
                FROM
              `ad_stuff_img` asi 
              LEFT JOIN `feeds_img_size` asis 
                ON asi.sizeid = asis.`sizeid` 
              LEFT JOIN `ad_stuff_text` ast 
                ON asi.stuffid = ast.`stuffid`
              LEFT JOIN `ad_stuff` ads 
                ON ads.`stuffid` = asi.`stuffid` 
              LEFT JOIN ad_group ag 
                ON ag.adgroupid = ads.`adgroupid` 
              LEFT JOIN ad_campaign ac 
                ON ac.campaignid = ag.campaignid 
            WHERE 1 and ads.ispause = 0 and ag.adform = 20 and ag.starttime <= $time and ag.endtime >= $time {$this->_where} ".$isupdate;
        /*SDK版本，本期无，加SDK版本时或定向修改时，请注意文字 广告处理*/
        $res = $this->_db->query($sql);
        $this->lastsql = $sql;
        //echo $sql;
        //print_r($res);
        $list = $this->_db->fetch_array($res);
        $tmp = array();
        $text = '';//print_r($list);exit;
        foreach($list as $val){
            $val['ostype'] = $this->getOsType($val['ostype']);
            $val['path'] = PIC_URL.$val['path'];
            $val = $this->getUnitPrice($val);
            if($val['click_effect'] == 3){//应用内打开页面。
                $val['url'] = 'coco://openh5?redirect='.urlencode($val['url']);
            }
            $val = $this->getBothOsVersion($val);
            $val = $this->getBothSDKVersion($val);
            if($val['creative_type'] == 2){
                $text .= $val['adid'].',';
            }
            if($val['apptypeset'] == 0)
            {
                $val['appcategories'] = array();
            }
            else
            {
                $val['appcategories'] = explode(',', $val['appcategories']);
            }
            if(empty($val['adcategories']))
            {
                $val['adcategories'] = array();
            }
            else
            {
                $val['adcategories'] = explode(',', $val['adcategories']);
            }
            if($val['istrackidfa'] == 1)
            {
                if(strpos('?', $val['url']) === false)
                {
                    $val['url'] .= '?idfa=@COCOIDFA@';
                }
                else
                {
                    $val['url'] .= '&idfa=@COCOIDFA@';
                }
            }
            $val = $this->getCommon($val);
            if($val == false)
            {
                continue;
            }
            if(isset($tmp[$val['adid']]))
            {

                if(!isset($tmp[$val['adid']]['imgs'][$val['width'].'X'.$val['height']])){

                  $tmp[$val['adid']]['imgs'][$val['width'].'X'.$val['height']] = array();
                }
                array_push($tmp[$val['adid']]['imgs'][$val['width'].'X'.$val['height']], $val['path']);

            }else
            {
                $val['imgs'][$val['width'].'X'.$val['height']] = array();
                array_push($val['imgs'][$val['width'].'X'.$val['height']], $val['path']);
                unset($val['path']);
                unset($val['width']);
                unset($val['height']);
                $tmp[$val['adid']] = $val;
            }
        }

        return $tmp;
    }

    /**
     * 开发者应用信息
     */
    function developer(){
        return array("0"=>array());
    }

    function setIsUptime(){
        $this->_isupdate = " and isupdate > 0 ";
    }

    function getOsType($ostype){
        if(isset($this->_os[$ostype])){
            return $this->_os[$ostype];
        }
        return 'ALL';
    }

    function getlastsql(){
        return $this->lastsql;
    }

    function getRetarget($isupdate=0){
        $where = '';
        if($isupdate){
            $time = time() - 600;
            $where = " and status = 1 ";
        }
        $sql = "select adgroupid,sysdirmacfile,sysdiridfafile from ad_group where (sysdirmacfile is not null or sysdiridfafile is not null) $where";
        $res = $this->_db->query($sql);
        $this->lastsql = $sql;
        $list = $this->_db->fetch_array($res);
        return $list;
    }

    function getAdgroupRetarget($type=1){
        $time = time();
        if($type==2) 
        	$select="select ad_retarget.adgroupid,ad_retarget.appid,ad_retarget.siteid,ad_group.adsubtargettype ";
        else
        	$select="select ad_retarget.adgroupid,ad_retarget.appid,ad_group.adtargettype ";
        $sql = $select."from ad_retarget left join ad_group on ad_group.adgroupid=ad_retarget.adgroupid left join ad_campaign on ad_campaign.campaignid=ad_group.campaignid where ad_group.starttime<= $time and ad_group.endtime >= $time-86399 and ad_retarget.type=".$type;
        $res = $this->_db->query($sql);
        $this->lastsql = $sql;
        return $this->_db->fetch_array($res);
    }

    private function getUserApp($applist){
        $tmp = array();
        foreach($applist as $val){
            $osname = $this->getOsName($val['ostypeid']);
            $tmp[$val['userid']][$osname][$val['appid']] = '_'.$val['appid'];
        }
        return $tmp;
    }

    private function getOsName($ostypeid){
        return isset($this->_os[$ostypeid]) ? $this->_os[$ostypeid] : 'ALL';
    }

    /**
     * 获取广告位信息
     * Enter description here ...
     */
    function getPosition(){
        $sql = "select appid,positionid,adform,confirmation,banneradtype,adstyle,adstylesize,adstyleimgsize,adstylecolor,bordertype,borderpolicy,
            app_position.bordergroupid,ad_border.status,ad_border.type from app_position left join ad_border on 
            app_position.bordergroupid=ad_border.bordergroupid and ad_border.status = 1 and ad_border.sizeid=12 
            and app_position.bordertype = ad_border.type where 1 ";
        $res = $this->_db->query($sql);
        $this->lastsql = $sql;
        $pbinfo = $this->_db->fetch_array($res);
        $tmp = array();
        foreach ($pbinfo as $v)
        {
            $tmp[$v['appid']][] = $v;
        }
        $psinfo = $this->punchboxinfo();
        $appclickeffectinfo = $this->getappclickeffectinfo();
        $list = array();
        foreach ($psinfo as $v)
        {
            if(isset($tmp[$v['appid']]) && count($tmp[$v['appid']]) > 0)
            {
                foreach ($tmp[$v['appid']] as $vv)
                {
                    if($vv['adform'] == 4)
                    {
                        $adform = 5;
                    }
                    elseif($vv['adform'] == 5)
                    {
                        $adform = 4;
                    }
                    else
                    {
                        $adform = $vv['adform'];
                    }
                    if(isset($appclickeffectinfo[$v['appid']][$adform]))
                    {
                        $adclickeffect = $appclickeffectinfo[$v['appid']][$adform];
                    }
                    else
                    {
                        $adclickeffect = "0";
                    }
                    if($vv['banneradtype'] > 0)
                    {
                        $adclickeffect = "0";
                    }
                    $data = array('publisherid'=>$v['publisherid'],
                                    'positionid'=>$vv['positionid'],
                                    'adform'=>$adform,
                    				'adclickeffect'=>$adclickeffect,
                                    'banneradtype'=>$vv['banneradtype']);
                    if($vv['adform'] == 2)
                    {
                        $data['bordertype'] = $vv['bordertype'];
                        $data['borderpolicy'] = $vv['borderpolicy'];
                        $data['bordergroupid'] = $vv['bordergroupid'];
                        //如果设置了bordergroupid但是状态不正常，重置边框设置
                        if(isset($vv['borderpolicy']) && $vv['borderpolicy'] == 1 && !isset($vv['status'])){

                            unset($data['bordertype']);
                            unset($data['borderpolicy']);
                            unset($data['bordergroupid']);
                        }

                        if($vv['adstyle']==1)
                        {
                            $data['popupbdcolor'] = $vv['adstylecolor'];
                            $data['stuffscale'] = $vv['adstyleimgsize'];
                            $data['displayscale'] = $vv['adstylesize'];
                        }
                        else
                        {
                            $data['popupbdcolor'] = '';
                            $data['stuffscale'] = '';
                            $data['displayscale'] = '100';
                        }
                    }
                    $list[] = $data;
                }
            }
        }
        return $list;
    }

    private function FilterData($list){
        foreach($list as $val){

        }
        return $list;
    }

    /**
     * 返回改写广告位信息更新状态
     * Enter description here ...
     */
    function setPosition(){

    }

    function getDbQuery($sql){
        $res = $this->_db->query($sql);
        if(empty($this->_db->_rs)){
            return '<pre>'.$res.'</pre>';
        }
        $list = $this->_db->fetch_array($res);
        if(count($list) > 100){
            return "返回的结果超过100条，请减少数据查询输出量后再次执行。";
        }
        return '<pre>'.print_r($list, true).'</pre>';
    }

    function getTask(){
        $isupdate = '';
        if($this->_isupdate){
            $isupdate = " and ac.isupdate > 0 ";
        }
        $time = $this->_nowtime;
        $sql = "SELECT {$this->_ac_fields} {$this->_ag_fields} {$this->_ads_fields}
                ag.tracktype,
                IFNULL(ag.otherid,'') AS trackcampaignid,
                ag.istracktype,
                ag.starttime wallstarttime,
                ag.endtime+86399 wallendtime,
                ag.antifraud as cheatreturnratio,
              asm.appname adtitle,
              asm.appcontent adcontent,
              asm.tasksummary,
              asm.path wallicon,
              asm.istop,
              asm.integralsort as `order`,
              asm.ishulu,
              asm.star,
              asm.`img900`,
              asm.`img600`,
              asm.`videofileurl`
            FROM
              `ad_stuff_integral` asm 
              LEFT JOIN `ad_stuff` ads 
                ON ads.`stuffid` = asm.`stuffid` 
              LEFT JOIN ad_group ag 
                ON ag.adgroupid = ads.`adgroupid` 
              LEFT JOIN ad_campaign ac 
                ON ac.campaignid = ag.campaignid 
            WHERE 1 and ads.ispause = 0 and ag.adform = 4 and ag.starttime<= $time and ag.endtime >= $time {$this->_where} ".$isupdate;
        //echo $sql;
        $res = $this->_db->query($sql);
        $this->lastsql = $sql;
        $list = $this->_db->fetch_array($res);
        $tmp = array();
        $stuffids = '';
        $users = '';
        foreach($list as $val){
            $val['ostype'] = $this->getOsType($val['ostype']);
            $val = $this->getUnitPrice($val);
            $val = $this->getBothOsVersion($val);
            $val = $this->getBothSDKVersion($val);
            $val['wallicon'] = PIC_URL.$val['wallicon'];
            if($val['istracktype'] == 1 && $val['chargemode'] == 3)
            {
                $val['relaytype'] = '1';
            }
            else if($val['chargemode'] == 3 && ($val['tracktype'] == 4 || $val['istracktype'] == 0))
            {
                $val['relaytype'] = '4';
            }
            else
            {
                $val['relaytype'] = '3';
            }
            $val = $this->getCommon($val);
            if($val == false)
            {
                continue;
            }
            if($val['apptypeset'] == 0)
            {
                $val['appcategories'] = array();
            }
            else
            {
                $val['appcategories'] = explode(',', $val['appcategories']);
            }
            if(empty($val['adcategories']))
            {
                $val['adcategories'] = array();
            }
            else
            {
                $val['adcategories'] = explode(',', $val['adcategories']);
            }
            $val['bought_type'] = 5;
            //是否支持积分墙签到
            $val['supportcheckin'] = "0";
            //视频积分墙
            if($val['targettype'] == 4)
            {
                $val['videoinfo'] = array('videourl'=>$val['videofileurl'], 'verticalimg'=>PIC_URL.$val['img600'], 'horizontalimg'=>PIC_URL.$val['img900']);
            }
			if($val['targetwalls'] == 0){
				$val['targetwalls'] = "1,2";
			}
            //处理积分墙类型
            if(!empty($val['targetwalls']))
            {
                $val['targetwalls'] = explode(',', $val['targetwalls']);
            }
            $stuffids .= $val['adid'].',';
            $tmp[$val['adid']] = $val;
        }
        if($stuffids){
            $stuffids = trim($stuffids, ',');
            $sql = "select taskid,taskid as `order`,stuffid adid,tasktype trackeventtype,eventid trackid,taskprice,taskname,taskdescription taskcontent, keyword, appranking from ad_task where 1 and stuffid in ($stuffids) order by taskid asc";
            $res = $this->_db->query($sql);
            $tasklist = $this->_db->fetch_array($res);
            $tmpadgroupprice = array();
            foreach($tasklist as $k=>$v)
            {
                $tasklist[$k]['unit_prices'] = $this->formattaskprice($v['taskprice']);
                if(empty($tmpadgroupprice[$v['adid']]))
                {
                    $tmpadgroupprice[$v['adid']] = $tasklist[$k]['unit_prices'];
                }
                else
                {
                    foreach($tmpadgroupprice[$v['adid']] as $tmpk => $tmpv)
                    {
                        $tmpadgroupprice[$v['adid']][$tmpk] = $tmpv + $tasklist[$k]['unit_prices'][$tmpk];
                    }
                }
                $tmp[$v['adid']]['unit_prices'] = $tmpadgroupprice[$v['adid']];
                if($v['trackeventtype'] == 5)
                {
                    $tmp[$v['adid']]['supportcheckin'] = "1";
                }


                //视频积分墙
                if(isset($tmp[$v['adid']]['targettype']) &&  $tmp[$v['adid']]['targettype'] == 4)
                {
                    $tasklist[$k]['videoinfo'] = $tmp[$v['adid']]['videoinfo'];
                }
                //处理积分墙类型
                if(isset($tmp[$v['adid']]['targetwalls']))
                {
                    $tasklist[$k]['targetwalls'] = $tmp[$v['adid']]['targetwalls'];
                }

            }
            $tmp['tasklist'] = $tasklist;
        }
        /*if(!empty($users)){
         $sql = "select appid,userid,ostypeid from app_info where userid in (".implode(',',$users).")";
         $res = $this->_db->query($sql);
         $applist = $this->_db->fetch_array($res);
         $applist = $this->getUserApp($applist);
         $tmp['applist'] = $applist;
         }*/
        return $tmp;
    }
    /**
     * 转换积分墙排期数据
     */
    private function formattaskprice($data)
    {
        if(empty($data))
        {
            $data = 0;
        }
        $tmp = json_decode($data, true);
        if(isset($tmp[0]['time']) && isset($tmp[0]['data']))
        {
            $timemk = 0;
            $keymk = 0;
            $newtmp = null;
            foreach ($tmp as $k=>$v)
            {
                if(strtotime($v['time'][0]) < time() && strtotime($v['time'][1]) >= (time()-86400))
                {
                    foreach ($v['data'] as $vt)
                    {
                        $newtmp[$vt[0].','.$vt[1]] = $vt[2];
                    }
                }
                if($timemk <= $v['time'][1])
                {
                    $timemk = $v['time'][1];
                    $keymk = $k;
                }
            }
            if(empty($newtmp))
            {
                foreach ($tmp[$keymk]['data'] as $vt)
                {
                    $newtmp[$vt[0].','.$vt[1]] = $vt[2];
                }
            }
            $tmp = $newtmp;
        }
        $redata = array();
        if(!is_array($tmp) && is_numeric($data))
        {
            for($i = 0; $i < 24; $i++)
            {
                $redata[] = (int)$data;
            }
            return $redata;
        }
        foreach($tmp as $k=>$v)
        {
            $tpkey = explode(',', $k);
            if(count($tpkey) == 2)
            {
                for($i = $tpkey[0]; $i < $tpkey[1]; $i++)
                {
                    $redata[] = (int)$v;
                }
            }
        }
        return $redata;
    }

    private function getUnitPrice($val){
        if($val['chargemode'] == 2){
            $val['unit_prices'] = $this->formattaskprice(($val['unit_price'] / $this->_cpm_ratio));
        }
        else
        {
            $val['unit_prices'] = $this->formattaskprice($val['unit_price']);
        }
        return $val;
    }

    private function getBothOsVersion($val){
        if($val['osversionset'] == 1){//版本过滤
            $val['aosvset'] = $this->getOsVersionSection(0,$val['aosvset']);
            $val['iosvset'] = $this->getOsVersionSection(1,$val['iosvset']);
        }
        return $val;
    }

    private function getBothSDKVersion($val){
        if($val['sdkversionset'] == 1){//版本过滤
            $val['asdkvset'] = $this->getSDKVersionSection(2,$val['asdkvset']);
            $val['isdkvset'] = $this->getSDKVersionSection(1,$val['isdkvset']);
        }
        return $val;
    }

    /**
     * 通用过滤器
     * Enter description here ...
     * @param $val
     */
    private function getCommon($val){
        if($val['device_types']){
            $device_types = $val['device_types'];
            $val['device_types'] = explode(',',trim($val['device_types'],','));
            $val['device_models'] = turnDeviceModels($val['device_types']);
            $val['device_types'] = turnDeviceTypes($device_types);
        }
        else
        {
            $val['device_types'] = array();
        }
        if(stripos($val['url'],'.apk') !== false){//添加版本号和包名
            $val['url'] .= stripos($val['url'],'#') !== false ? "&pkg={$val['packagename']}&dlappv={$val['appversion']}" : "#pkg={$val['packagename']}&dlappv={$val['appversion']}";
        }
        if($val['targettype'] == 3)
        {
            $val['url'] = "coco://cocosplay?pkg=".$val['packagename'].'&name='.$val['creative_title'].'&orientation='.$val['orientation'];
        }
        if($val['scheduletype'] == 0)
        {
            $val['periods'] = '111111111111111111111111';
        }
        else
        {
            $tmp = $this->getPeriodset($val);
            if($tmp == false)
            {
                return false;
            }
            $val['periods'] = $tmp['schedules'];
            if($tmp['daybudget'] !== null)
            {
                $val['groupbudget'] = $tmp['daybudget'];
            }
            else
            {
                $val['groupbudget'] = $val['defaultbudget'];
            }
        }
        if($val['newdeviceset'] > 0)
        {
            $val['atime'] = (string)($val['newdeviceset']*86400);
        }
        else
        {
            $val['atime'] = '';
        }
        if($val['network_types']) $val['network_types'] = explode(',',$val['network_types']);
        if(empty($val['dmpset']))
        {
            $val['dmpmatch_types'] = array();
        }
        else
        {
            $val['dmpmatch_types'] = explode(',',$val['dmpset']);
        }
        if($val['carrieroperators'])
        {
            $tmp = explode(',',$val['carrieroperators']);
            $val['carrieroperators'] = array();
            foreach($tmp as $v)
            {
                switch ($v) {
                    case 1:
                        array_push($val['carrieroperators'],'46000','46002','46007','46020');
                        break;
                    case 2:
                        array_push($val['carrieroperators'],'46003','46005','46013','46014');
                        break;
                    case 3:
                        array_push($val['carrieroperators'],'46001','46006');
                        break;
                    default:
                        break;
                }
            }
        }
        $val['country_codes'] = strtoupper($val['country_codes']);
        if($val['country_codes'])
        {
            $val['country_codes'] = explode(',',$val['country_codes']);
	        if($val['domainfiltertype']=='1'){
	        	$val['domain_codes'] = $this->getDomesticAreaIds($val['domain_codes'],implode(',', $val['country_codes']));
	        }
            else if ($val['domainfiltertype']=='2'){
            	 $sAreaIds = json_decode($val['domain_codes'], true);
            	 if(empty($sAreaIds)){ 
            	 	$val['domain_codes']= array();
            	 }
            	 else{
            	 	$keys = array_search('-1',$sAreaIds);
					if($keys!== false){
						array_splice($sAreaIds, $keys, 1);
					}
            	 	$val['domain_codes']=$sAreaIds;
            	 }
            }
        }
        else
        {
            $val['domain_codes'] = '';
        }
        //        print_r($val['domain_codes']);
        //        print_r($val['country_codes']);
        //        die();
        if($val['jailbreak_types'] == 0){
            unset($val['jailbreak_types']);
        }else{
            if($val['jailbreak_types'] == 1){
                $val['jailbreak_types'] = array('1');
            }else{
                $val['jailbreak_types'] = array('0');
            }
        }
        //redis中创意类型需要中控做适配，规则如下：
        //redis中创意类型 creative_type  :  (1：图片 2：图文(原文字) 3：文字(纯文字)，4：动效，5：视频)
        //此处以广告组广告形式和创意类型共同决定。
        // bought_type  1:Banner广告.2:弹出广告.3:精品推荐.4:广告墙
        // creative_type 1：Banner图片  2：文字  3：pop图片  4：动效广告  5：精品推荐  6：积分墙
        switch ($val['bought_type'])
        {
            case 1:
                //默认正常
                if($val['creative_type'] == 7)
                {
                    $val['creative_type'] = 3;
                }
                break;
            case 2:
                if($val['creative_type'] == 3)
                {
                    $val['creative_type'] = 1;
                }
                elseif ($val['creative_type'] == 10)
                {
                	$val['creative_type'] = 2;
                }
                break;
            case 3:
                $val['creative_type'] = 2;
                break;
            case 4:
                //积分墙需要根据广告活动判断是否是视频广告
                if($val['targettype']==4)
                {
                    $val['creative_type'] = 5;
                }
                else
                {
                    $val['creative_type'] = 2;
                }
                break;
            case 5:
                $val['creative_type'] = 2;
                break;
            case 6:
                $val['creative_type'] = 2;
                break;
            case 20: //Feeds 广告形式
                $val['creative_type'] = 2;
                break;
			case 30: //DSP 广告形式
                $val['creative_type'] = 2;
                break;
        }
        return $val;
    }

    /**
     * 地域处理
     * @param  $areaIds:json,
     * @param  $areaset:string,逗号分割的国家码
     */
	private function getDomesticAreaIds($areaIds,$areaset)
    {
        $areaData = $this->getArea();
        $sAreaIds = json_decode($areaIds, true);
        if(empty($sAreaIds)) return array();
        
        $keys = array_search('-1',$sAreaIds);
        if($keys!== false)
        {
        	if($areaset!="CN,TW,HK,MO"){
        		//由于需要兼容旧的数据：如：areaset='CN',不包含港澳台，但是areaids里存在-1全选的情况，对地域定向调整增加港澳台的种情况下，这种数据是有误的，为了使旧的数据也可以推送，特意加了验证
				array_splice($sAreaIds, $keys, 1);
        	}
        	else{
        		return array('12');
        	}
        }
        $baseAreaData = array();
        $markAreaDate = array();
        foreach($areaData as $v)
        {
            if($v['cid']!=null && $v['cid']!=""){
            	$baseAreaData[$v['pid']][] = $v['cid'];
	            $keys = array_search($v['cid'],$sAreaIds);
	            if($keys !== false)
	            {
	                $markAreaDate[$v['pid']][] = $v['cid'];
	            }
            }
            else{
            	$baseAreaData[$v['pid']] = array();
            	$keys = array_search($v['pid'],$sAreaIds);
            	if($keys !== false)
	            {
	                $markAreaDate[$v['pid']] = array();
	            }
            }
        }
        $returnData = array();
        foreach ($markAreaDate as $k=>$v)
        {
            if(count($v) == count($baseAreaData[$k]))
            {
            	$returnData[] = '12_'.$k;
            }
            else
            {
            	foreach($v as $val)
                {
                    $returnData[] ='12_'.$k.'_'.$val;
                }
            }
        }
        return $returnData;
    }
    
    private function getArea()
    {
        $sql = "SELECT `pid`,`pname`,`cid`,`cname` FROM `ad_area`";
        $res = $this->_db->query($sql);
        $this->lastsql = $sql;
        $list = $this->_db->fetch_array($res);
        return $list;
    }
    //积分墙全局分成比例
    function getwallecpa(){
        $sql = "select ostype,ratio from ad_ecpc where 1 and adtype=5 and status=1";
        $res = $this->_db->query($sql);
        $this->lastsql = $sql;
        $list = $this->_db->fetch_array($res);
        $relist = array('1'=>80, '2'=>80);
        $relist = array();
        foreach ($list as $v)
        {
            $relist[$v['ostype']] = $v['ratio'];
        }
        return $relist;
    }
    //积分墙全局扣量系数
    function getwallratio(){
        $sql = "select ostype,ratio from ad_click_ratio where 1 and adtype=5 and status=1";
        $res = $this->_db->query($sql);
        $this->lastsql = $sql;
        $list = $this->_db->fetch_array($res);
        $relist = array('1'=>100, '2'=>100);
        $relist = array();
        foreach ($list as $v)
        {
            $relist[$v['ostype']] = $v['ratio'];
        }
        return $relist;
    }
    //积分墙全局作弊返还
    function getcheatreturnratio(){
        $sql = "select skey,sval from `system_config` where skey ='antifraudios' or skey = 'antifraudandroid'";
        $res = $this->_db->query($sql);
        $data = $this->_db->fetch_array($res);
        $redata = array('ios'=>0, 'android'=>0);
        foreach ($data as $v)
        {
            if($v['skey'] == 'antifraudios')
            {
                $redata['IOS'] = $v['sval'];
            }
            if($v['skey'] == 'antifraudandroid')
            {
                $redata['ANDROID'] = $v['sval'];
            }
        }
        return $redata;
    }

    //dmp设置
    function getGlobalInfo(){
        $sql = "select skey,sval from `system_config` where skey in ('dmpdebugswitch', 'dmprule')";
        $res = $this->_db->query($sql);
        $data = $this->_db->fetch_array($res);
        $redata = array('ios'=>0, 'android'=>0);
        foreach ($data as $v)
        {
            if($v['skey'] == 'dmpdebugswitch')
            {
                $redata['dmpdebugswitch'] = $v['sval'];
            }
            if($v['skey'] == 'dmprule')
            {
                $redata['dmprule'] = $v['sval'];
            }
        }
        return $redata;
    }

    function getPrePopup(){
        $sql = "SELECT {$this->_ac_fields} {$this->_ag_fields} {$this->_ads_fields}
              ag.ignoretype retargettype,
              asis.`width`,
              asis.`height`,
              asi.`path`
                FROM
              `ad_stuff_img` asi 
              LEFT JOIN `ad_stuff_img_size` asis 
                ON asi.sizeid = asis.`sizeid` 
              LEFT JOIN `ad_stuff` ads 
                ON ads.`stuffid` = asi.`stuffid` 
              LEFT JOIN ad_group ag 
                ON ag.adgroupid = ads.`adgroupid` 
              LEFT JOIN ad_campaign ac 
                ON ac.campaignid = ag.campaignid 
            WHERE 1 and ads.ispause = 0 and ac.ostypeid<2 and ag.adform = 2 and ads.ispredown=1 ";
        $res = $this->_db->query($sql);
        $this->lastsql = $sql;
        $list = $this->_db->fetch_array($res);
        $tmp = array();
        $text = '';//print_r($list);
        foreach($list as $val){
            $val['ostype'] = $this->getOsType($val['ostype']);
            $val['path'] = PIC_URL.$val['path'];
            $val = $this->getUnitPrice($val);
            if($val['click_effect'] == 3){//应用内打开页面。
                $val['url'] = 'coco://openh5?redirect='.urlencode($val['url']);
            }
            if(stripos($val['url'],'.apk') !== false){//添加版本号和包名
                $val['url'] .= stripos($val['url'],'#') !== false ? "&pkg={$val['packagename']}&dlappv={$val['appversion']}" : "#pkg={$val['packagename']}&dlappv={$val['appversion']}";
            }
            if($val['targettype'] == 3)
            {
                $val['url'] = "coco://cocosplay?pkg=".$val['packagename'].'&name='.$val['creative_title'].'&orientation='.$val['orientation'];
            }
            //$val = $this->getBothOsVersion($val);
            if($val['creative_type'] == 2){
                $text .= $val['adid'].',';
            }
            if($val['apptypeset'] == 0)
            {
                $val['appcategories'] = array();
            }
            else
            {
                $val['appcategories'] = explode(',', $val['appcategories']);
            }
            if(empty($val['adcategories']))
            {
                $val['adcategories'] = array();
            }
            else
            {
                $val['adcategories'] = explode(',', $val['adcategories']);
            }
            //$val = $this->getCommon($val);
            if(isset($tmp[$val['adid']])){
                $tmp[$val['adid']]['imgs'][] = array("size" => $val['width'].'X'.$val['height'], "url" => $val['path']);
            }else{
                $val['imgs'][] = array("size" => $val['width'].'X'.$val['height'], "url" => $val['path']);
                unset($val['path']);
                unset($val['width']);
                unset($val['height']);
                $tmp[$val['adid']] = $val;
            }
        }
        return $tmp;
    }

    /**
     * 获取转化率
     * Enter description here ...
     */
    function getAdRatio(){
        $sql = "SELECT * from app_stuff_ratio";
        $res = $this->_db->query($sql);
        $this->lastsql = $sql;
        $list = $this->_db->fetch_array($res);
        return $list;
    }

    /**
     * 按照渠道号获得 开关关闭的 版本
     */
    function getverswitch($publisherid=''){
        $sql = " SELECT pkPublisherId,pkAppVer FROM `ad_hour_appver` where verswitch =1";
        $res = $this->_db->query($sql);
        $data = $this->_db->fetch_array($res);
        $redata = array();
        foreach($data as $key=>$val){
            $pkPublisherId = $val['pkPublisherId'];
            $redata[$pkPublisherId][] = $val['pkAppVer'];
        }
        return $redata;
    }

    /**
     * 按照渠道号获得 新版积分墙的版本
     */
    function getverwallswitch($publisherid=''){
        $sql = " SELECT pkPublisherId,pkAppVer FROM `ad_hour_appver` where verwallswitch =1";
        $res = $this->_db->query($sql);
        $data = $this->_db->fetch_array($res);
        $redata = array();
        foreach($data as $key=>$val){
            $pkPublisherId = $val['pkPublisherId'];
            $redata[$pkPublisherId][] = $val['pkAppVer'];
        }
        return $redata;
    }

    /**
     * 获取切量信息
     */
    function getCutoverSet()
    {
        $time = $this->_nowtime;
        $sql = " SELECT ai.appid,cr.`pids`,mr.* FROM `app_info` ai LEFT JOIN `cutover_rule` cr ON ai.`appid` = cr.`appid` LEFT JOIN `media_rule` mr ON ai.`appid` = mr.`appid` WHERE ai.`cutoverstatus` = 2 and mr.isstatus = 0 AND mr.starttime <= $time and mr.endtime >= $time  AND cr.`adtype` = mr.`adtype` AND cr.`ostype` = mr.`ostype`";
        $res = $this->_db->query($sql);
        $appdata = $this->_db->fetch_array($res);
        if(empty($appdata)) return array();
        $fmtappdata = array();
        foreach($appdata as $v)
        {
            $tmppids = explode(',', $v['pids']);
            if(empty($tmppids))
            {
                continue;
            }
            foreach($tmppids as $tv)
            {
                $fmtappdata[$v['appid']][$tv][] = $v;
            }
        }
        return $fmtappdata;
    }

    /**
     * 获取切量数据
     */
    function getCutoverSys()
    {
        $fmtappdata = $this->getCutoverSet();
        $redata = array();
        $cutoverportlist = $this->getCutoverPort();
        $tmpmin = array();
        foreach($fmtappdata as $fk => $fv)
        {
            foreach($fv as $pk => $pv)
            {
                foreach ($pv as $k => $v)
                {
                    if($v['adtype'] == 1)
                    {
                        $adtype = 'banner';
                    }
                    else
                    {
                        $adtype = 'popup';
                    }
                    $data['port'] = $cutoverportlist[$v['mediaid']];
                    $data['priority'] = (int)$v['priority'];
                    if($v['ostypeset'] == 0)
                    {
                        $data['device_os_versions'] = array();
                    }
                    else
                    {
                        if($v['ostype'] == 1)
                        {
                            $ostype = 0;
                        }
                        else
                        {
                            $ostype = 1;
                        }
                        $data['device_os_versions'] = $this->getOsVersionSection($ostype, $v['lowosv'].','.$v['highosv']);
                    }
                    if($v['devicetypeset'] == 0)
                    {
                        $data['device_types'] = array();
                    }
                    else
                    {
                        $data['device_types'] = json_decode($v['devicetype'], true);
                    }
                    if($v['sdkversionset'] == 0)
                    {
                        $data['device_sdk_versions'] = array();
                    }
                    else
                    {
                        $data['device_sdk_versions'] = json_decode($v['sdkvset'], true);
                    }
                    if($v['nettypeset'] == 0)
                    {
                        $data['network_types'] = array();
                    }
                    else
                    {
                        $data['network_types'] = json_decode($v['nettype'], true);
                    }
                    $data['carrieroperators'] = array();
                    if($v['operatorsset'] == 1)
                    {
                        $tmpcs = json_decode($v['operators'], true);
                        foreach($tmpcs as $csv)
                        {
                            switch ($csv) {
                                case 1:
                                    array_push($data['carrieroperators'],'46000','46002','46007','46020');
                                    break;
                                case 2:
                                    array_push($data['carrieroperators'],'46003','46005','46013','46014');
                                    break;
                                case 3:
                                    array_push($data['carrieroperators'],'46001','46006');
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    
                	if(empty($v['areaset']))
                    {
                        $data['country_codes'] = array();
                        $data['domain_codes'] = array();
                    }
                    else
                    {
                        $data['country_codes'] = strtoupper($v['areaset']);
                        if($data['country_codes'])
                        {
                            $data['country_codes'] = explode(',', $data['country_codes']);
                            if($v['domainfiltertype']=='1')
                            {
                                $data['domain_codes'] = $this->getDomesticAreaIds($v['areaids'],$v['areaset']);
                            }
                            elseif ($v['domainfiltertype']=='2'){
                            	$sAreaIds = json_decode($v['areaids'], true);
                            	if(empty($sAreaIds)){ 
                            		$data['domain_codes']= array();
                            	}
                            	else{
                            		$keys = array_search('-1',$sAreaIds);
									if($keys!== false){
										array_splice($sAreaIds, $keys, 1);
									}
				            	 	$data['domain_codes']=$sAreaIds;
                            	}
                            }
                            else
                            {
                                $data['domain_codes'] = array();
                            }
                        }
                        else
                        {
                            $data['domain_codes'] = array();
                        }
                    }
                    $data['domainfiltertype']=$v['domainfiltertype'];
                    
                    if($v['scheduletypeset'] == 0)
                    {
                        $data['periods'] = '';
                        $data['percent'] = (int)$v['ratio'];
                    }
                    else
                    {
                        $tmpsp = json_decode($v['timelist'], true);
                        if(isset($tmpsp['data'][date('Y-m-d')]))
                        {
                            $data['periods'] = ScheduleDataToDBData($tmpsp['data'][date('Y-m-d')][0]);
                            $data['percent'] = (int)$tmpsp['data'][date('Y-m-d')][1];
                        }
                    }
                    if(isset($tmpmin[$pk][$adtype]))
                    {
                        //如果比例累计超过100%，则从100开始往下设置比例区间
                        if($tmpmin[$pk][$adtype] + $data['percent'] <= 100){

                            $data['min'] = $tmpmin[$pk][$adtype] + 1;
                            $data['max'] = $tmpmin[$pk][$adtype] + $data['percent'];
                            $tmpmin[$pk][$adtype] += $data['percent'];
                        }
                        else{

                            $data['min'] = 100 - $data['percent'] + 1;
                            $data['max'] = 100;
                            //预留已有的累计比例空间，后续可能有小比例的切量比例
                        }
                        
                    }
                    else
                    {
                        $data['min'] = 0;
                        $data['max'] = $data['percent'];
                        $tmpmin[$pk][$adtype] = $data['percent'];
                    }
                    $redata[$pk][$adtype][] = $data;
                }
                //默认补全
                if(isset($tmpmin[$pk]['banner']))
                {
                    $defdata['port'] = 80;
                    $defdata['priority'] = 0;
                    $defdata['device_types'] = array();
                    $defdata['network_types'] = array();
                    $defdata['device_sdk_versions'] = array();
                    $defdata['carrieroperators'] = array();
                    $defdata['country_codes'] = array();
                    $defdata['domain_codes'] = array();
                    $defdata['domainfiltertype']=0;
                    $defdata['periods'] = '';
                    $defdata['percent'] = 100 - $tmpmin[$pk]['banner'];
                    $defdata['min'] = $tmpmin[$pk]['banner'] + 1;
                    $defdata['max'] = 100;
                    $redata[$pk]['banner'][] = $defdata;
                }
                if(isset($tmpmin[$pk]['popup']))
                {
                    $defdata['port'] = 80;
                    $defdata['priority'] = 0;
                    $defdata['device_types'] = array();
                    $defdata['network_types'] = array();
                    $defdata['device_sdk_versions'] = array();
                    $defdata['carrieroperators'] = array();
                    $defdata['country_codes'] = array();
                    $defdata['domain_codes'] = array();
                    $defdata['domainfiltertype']=0;
                    $defdata['periods'] = '';
                    $defdata['percent'] = 100 - $tmpmin[$pk]['popup'];
                    $defdata['min'] = $tmpmin[$pk]['popup'] + 1;
                    $defdata['max'] = 100;
                    $redata[$pk]['popup'][] = $defdata;
                }
            }
        }
        return $redata;
    }

    /**
     * 获取切量账号和广告位
     */
    function getCutoverAccount()
    {
        $fmtappdata = $this->getCutoverSet();
        $appida = array();
        foreach($fmtappdata as $k => $v)
        {
            $appida[$k] = $k;
        }
        $appids = implode(',', $appida);
        $sql = " SELECT * FROM `media_cutover_account` where appid in ($appids)";
        $res = $this->_db->query($sql);
        $cutoveraccount = $this->_db->fetch_array($res);
        if(empty($cutoveraccount)) return array();
        $fmtcutoveraccount = array();
        foreach($cutoveraccount as $v)
        {
            $fmtcutoveraccount[$v['appid']] = $v;
        }
        $cutoverportlist = $this->getCutoverPort();
        $fmtaccountdata = array();
        foreach($fmtappdata as $k => $v)
        {
            if(isset($fmtcutoveraccount[$k]))
            {
                if(isset($cutoverportlist[$fmtcutoveraccount[$k]['mediacutoverid']]))
                {
                    //echo $cutoverportlist[$accountv['mediacutoverid']];
                    foreach ($v as $kp => $vp)
                    {
                        $fmtaccountdata[$cutoverportlist[$fmtcutoveraccount[$k]['mediacutoverid']]][$kp] = array(
                                'banner' => array("thdposid" => $fmtcutoveraccount[$k]['bannerplacementid'], "thdappid" => $fmtcutoveraccount[$k]['mediumid']),
                                'popup' => array("thdposid" => $fmtcutoveraccount[$k]['pupopplacementid'], "thdappid" => $fmtcutoveraccount[$k]['mediumid']),
                        );
                    }
                }
            }
        }
        return $fmtaccountdata;
    }

    /**
     * 获取切量系统端口号
     */
    function getCutoverPort()
    {
        $sql = "SELECT `adtype`,`mediaport`,`id`,`dspappaccount` FROM `media_cutover_manage`";
        $res = $this->_db->query($sql);
        $data = $this->_db->fetch_array($res);
        $redata = array();
        foreach($data as $key=>$val){
            $redata[$val['id']] = $val['mediaport'];
        }
        return $redata;
    }

    /**
     * 获取切量数据处理
     */
    function getCutoverList($data, $bdata, $adtype)
    {
        $redata = array();
        $tmprule = explode(',', $data);
        $min = 0;
        $max = 0;
        foreach($tmprule as $val)
        {
            $tmpdata = array();
            $tmps = explode('|', $val);
            if(count($tmps) != 3)
            {
                continue;
            }
            if(!isset($bdata[$tmps[0]][$adtype]))
            {
                continue;
            }
            $tmpdata['port'] = $bdata[$tmps[0]][$adtype];
            $tmpdata['percent'] = $tmps[2];
            $tmpdata['min'] = $min;
            $max += $tmps[2];
            $tmpdata['max'] = $max;
            $min = $max + 1;
            $redata[] = $tmpdata;
        }
        if($max < 100)
        {
            $default['port'] = 80;
            $default['percent'] = 100 - $max;
            $default['min'] = $max + 1;
            $default['max'] = 100;
            $redata[] = $default;
        }
        return $redata;
    }


    /**
     * 返回ccplay各模块开关数据供中控载入数据
     * @author yangsen
     * @return multitype:|Ambigous <multitype:multitype: , multitype:string >
     */
    function getAppChannelCCPlaySwitchStatus() {
        $sql = 'select ai.is_ccplay_gift tgift,ai.is_ccplay_announce tann,ai.is_ccplay_gonglue agl ,ai.is_ccplay_kefu akf ,app_channel.* from `app_channel` inner join `app_info` ai on ai.appid = `app_channel`.appid where ai.is_ccplay=1 and app_channel.is_ccplay=1';
        $res= $this->_db->query($sql);
        $data = $this->_db->fetch_array($res);
        if(empty($data)) return array();
        $output_array = array();
         
        /**
         * 目录ID     cateid: 1,
         目录名称   catename  :  公告,
         显示模板ID catetpl: "1",
         顺序ID     sortid: "0",
         首页标识   isindex: "0", (0:非首页，1：首页)

         */
        foreach($data as $k=>$v) {
            $output_array[$v['publisherID']] = array();
             
            if($v['is_ccplay_gift']==1 && $v['tgift']==1) {
                $output_array[$v['publisherID']]['2'] = array(
    					'cateid'=>'2',
    					'catename'=>'礼包',
    					'catetpl'=>'2',
    					'sortid'=>'2',
    					'isindex'=>'0'
    					);
            }
            if($v['is_ccplay_announce']==1 && $v['tann']==1) {
                $output_array[$v['publisherID']]['1'] = array(
    					'cateid'=>'1',
    					'catename'=>'公告',
    					'catetpl'=>'1',
    					'sortid'=>'1',
    					'isindex'=>'1'
    					);
            }
            if($v['is_ccplay_gonglue']==1 && $v['agl']==1) {
                $output_array[$v['publisherID']]['3'] = array(
    					'cateid'=>'3',
    					'catename'=>'攻略',
    					'catetpl'=>'3',
    					'sortid'=>'3',
    					'isindex'=>'0'
    					);
            }
            if($v['is_ccplay_kefu']==1 && $v['akf']==1) {
                $output_array[$v['publisherID']]['4'] = array(
    					'cateid'=>'4',
    					'catename'=>'客服',
    					'catetpl'=>'4',
    					'sortid'=>'4',
    					'isindex'=>'0'
    					);
            }

            /* if($v['is_ccplay_xapp']==1) {
             $output_array[$v['publisherID']]['5'] = array(
             'cateid'=>'5',
             'catename'=>'交叉换量',
             'catetpl'=>'5',
             'sortid'=>'5',
             'isindex'=>'0'
             );
             } */
        }
        return $output_array;
    }



    /**
     * 获取渠道公告数据
     */
    function getAnnouncementInfo()
    {
        $time = time();
        $sql = "SELECT pline.`appid`, pline.`publisherID`, pline.`campaignid`, pline.`title`, pline.`announcement`, pline.`status`, pline.`publish_time`, pline.`lastupdatetime`, pline.`time`,pline.`dingorder`,pline.`showstyle`,pline.`picsmallsize` FROM `ad_channel_public` pline inner join app_info api on api.appid = pline.appid  WHERE api.is_ccplay=1 and api.is_ccplay_announce=1 and pline.`status` = 3 AND pline.publish_time <=$time";
        // echo $sql.'<br>';
        $res = $this->_db->query($sql);
        $data = $this->_db->fetch_array($res);
        if(empty($data)) return array();
        $pidsdata = array();
        $publishrowsdata = array();
        $campaignidsdata = array();

        //渠道上的开关打开状态过滤
        $sql_4_channel = 'select publisherID from app_channel where is_ccplay=1 and is_ccplay_announce=1';
        $res_channel = $this->_db->query($sql_4_channel);
        $open_channel = $this->_db->fetch_array($res_channel);
        $tmp_opened_channels = array();
        foreach($open_channel as $row) {
            $tmp_opened_channels[] = $row['publisherID'];
        }

        foreach($data as $key=>$val)  {
            $tmpPids = explode(',', $val['publisherID']);

            $tmpPids = array_intersect($tmpPids,$tmp_opened_channels);
            foreach ($tmpPids as $v)
            {
                //pid[campaignid][publisherids]
                $pidsdata[$val['campaignid']][] = $v;

            }
            $campaignidsdata[] = $val['campaignid'];
            $publishrowsdata[$val['campaignid']] = $val;
        }
        $campaigndata = $this->getAnnouncementData($campaignidsdata);
        if(empty($campaigndata))
        {
            return array();
        }
        foreach($campaigndata as $k=>$v)
        {
            if(isset($pidsdata[$k]))
            {
                foreach ($pidsdata[$k] as $pid)
                {
                    if(isset($publishrowsdata[$k]))
                    {
                        $v['starttime'] = $publishrowsdata[$k]['publish_time'];
                    }

                    //设置指定排序
                    $v['order'] = $publishrowsdata[$k]['dingorder'];

                    //设置是否是置顶
                    if($publishrowsdata[$k]['dingorder']!=99) {
                        $v['istop']='1';
                    } else {
                        $v['istop']='0';
                    }

                    //设置是什么style类型
                    $v['creative_type']=$publishrowsdata[$k]['showstyle'];


                    //设置图片宽度和url
                    if(!empty($publishrowsdata[$k]['picsmallsize'])) {
                        $v['imgs'] = array(($publishrowsdata[$k]['picsmallsize']) => $v['adtextpath']);
                    }

                    //unset($v['adtextpath']);
                    if($v['adid']=='0') {
                        echo '擦，遇到个adid是0的<br/>';
                    }
                    $redata[$pid][$v['adid']] = $v;
                }
            }
        }

        //按渠道分组内按照order和starttime(发布日期)排序[ccplay]
        foreach($redata as $k => $v) {
            $k1 = array_keys($redata[$k]);
            $k1 = $k1[0];
            usort($redata[$k],'sortGroupByPublisher');
        }
         
        //echo json_encode($redata);
        if((!empty($data)) && empty($redata)) {
            error_log('数据库查询出有数据，经过camp组装后结果是空。');
        }

        return $redata;
    }

    /**
     * 获取公告数据
     */
    private function getAnnouncementData($cids)
    {
        $time = $this->_nowtime;
        if(empty($cids))
        {
            return array();
        }
        $where = " and ac.campaignid in (".implode(',', $cids).")";
        $sql = "SELECT {$this->_ac_fields} {$this->_ag_fields} {$this->_ads_fields}
              IFNULL(ag.otherid,'') AS trackcampaignid,
              ast.`path` adtextpath,
              ast.adcontent adtext,
              ast.adtitle adtexttitle
            FROM
              `ad_stuff_text` ast 
              LEFT JOIN `ad_stuff` ads 
                ON ads.`stuffid` = ast.`stuffid` 
              LEFT JOIN ad_group ag 
                ON ag.adgroupid = ads.`adgroupid` 
              LEFT JOIN ad_campaign ac 
                ON ac.campaignid = ag.campaignid 
            WHERE 1 and ads.ispause = 0 and ag.adform = ".CONST_ANNOUNCE_BOUGHT_TYPE." and ag.starttime <= $time and ag.endtime >= $time {$this->_where} {$where}";
        //echo $sql;
        /*SDK版本，本期无，加SDK版本时或定向修改时，请注意文字 广告处理*/
        $res = $this->_db->query($sql);
        $this->lastsql = $sql;
        //echo $sql;
        $list = $this->_db->fetch_array($res);
        $text = '';
        foreach($list as $val){
            $val['ostype'] = $this->getOsType($val['ostype']);
            if($val['click_effect'] == 3){//应用内打开页面。
                $val['url'] = 'coco://openh5?redirect='.urlencode($val['url']);
            } else if (in_array($val['click_effect'],array(20,21)) && empty($val['url'])) {
                $val['click_effect']=999999;
            }
            $val = $this->getUnitPrice($val);
            $val = $this->getBothOsVersion($val);
            $val = $this->getBothSDKVersion($val);
            if($val['apptypeset'] == 0)
            {
                $val['appcategories'] = array();
            }
            else
            {
                $val['appcategories'] = explode(',', $val['appcategories']);
            }
            if(empty($val['adcategories']))
            {
                $val['adcategories'] = array();
            }
            else
            {
                $val['adcategories'] = explode(',', $val['adcategories']);
            }
            if($val['creative_type'] == 2){
                $text .= $val['adid'].',';
            }
            if(!empty($val['adtextpath'])) {
                $val['adtextpath'] = PIC_URL.$val['adtextpath'];
            }

            $val = $this->getCommon($val);
            if($val == false)
            {
                continue;
            }
            $tmp[$val['campaignid']] = $val;
        }
        return $tmp;
    }



    /**
     * 获取礼包数据
     * @adform 11
     * @author ys
     * @link http://mediawiki.punchbox.org/index.php/Ccplay%E5%B9%BF%E5%91%8A%E4%B8%AD%E6%8E%A7%E6%95%B0%E7%BB%84%E9%94%AE%E5%80%BC#ccplay_.E7.A4.BC.E5.8C.85
     */
    function getGiftDailyTaskInfo() {
         
        $sql = 'select cd.* from `ccplay_gift_define` cd inner join app_info ai on ai.appid=cd.appid where `ai`.`is_ccplay_gift`=1 and `cd`.`status`=1 and `isdailytask`>0 order by ai.appid asc,isdailytask asc';
        $res = $this->_db->query($sql);
        $data = $this->_db->fetch_array($res);
         
        $appids = array();
        $campaignids = array();
        $appidPids=array();//appid=>array(pid)
        $appidRowMap = array(array());
        //gift table info .
        foreach($data as $key =>$val) {
            $appids[] = $val['appid'];
            $campaignids[] = $val['campaignid'];
            $val['content'] = json_decode($val['content']);
            $appidRowMap[$val['appid']][$val['campaignid']] = $val;
        }
         
        $appids = array_unique($appids);
        $result = $this->getAdFlowByCampaignIds($campaignids,CONST_GIFT_BOUGHT_TYPE);
        $sql4queryChannels='select * from app_channel where is_ccplay_gift=1 and appid in ('.implode(',', array_merge(array('-1'),$appids)).') ';
        //echo $sql4queryChannels;
         
        $appchannelsDs = $this->_db->query($sql4queryChannels);
        $appchannels = $this->_db->fetch_array($appchannelsDs);
         
        $campaignMap = array();
        foreach($result as $key=>$val) { //result = ad stream info .

            $campaignMap[$val['campaignid']] = $val;
        }
         
         
        $finalResult = array();
        foreach($appchannels as $val) {
            $appid = $val['appid'];
            foreach($appidRowMap[$appid] as $campid=>$v) {
                //check adid is not null
                if(!isset($campaignMap[$campid])){
                    continue;
                }
                $camrow = $campaignMap[$campid];

                //print_r($camrow);
                $adid = $camrow['adid'];
                if(!empty($adid)) {
                    $finalResult[$val['publisherID']][$adid] = array(
	    					'campaignid'=>$campid.'',
	    					'targettype'=>$camrow['targettype'],
                            'adgroupid'=>$camrow['adgroupid'],
                        	'adid'=>$camrow['adid'],
                        	'bought_type'=>CONST_GIFT_BOUGHT_TYPE,
                        	'userid'=>$camrow['userid'],
                        	'own'=>2,
                        	'starttime'=>$camrow['starttime'],
                        	'endtime'=>$camrow['endtime'],
	    					'adtexttitle'=>$v['giftpackagename'],
	    					'order'=>intval($v['isdailytask']),
	    					'giftpkg'=>$v['content']
                    );
                }
            }
        }
         
        //print_r($finalResult);
        return $finalResult;
    }



    /**
     * 获取礼包、客服数据(gift)
     * @param array $cid campaignid
     * @param adform
     * @param bool virtual 如果是virtual不查询text要素表
     *
     */
    private function getAdFlowByCampaignIds($cids,$adform=CONST_GIFT_BOUGHT_TYPE,$virtual=false) {
        $time = $this->_nowtime;
        if(empty($cids))
        {
            return array();
        }
        $where = " and ac.campaignid in (".implode(',', $cids).")";
         
        if($virtual) {
            $sql = "SELECT {$this->_ac_fields} {$this->_ag_fields} {$this->_ads_fields}
	    	IFNULL(ag.otherid,'') AS trackcampaignid 
	    	FROM
	    	 `ad_stuff` ads 
	    	LEFT JOIN ad_group ag
	    	ON ag.adgroupid = ads.`adgroupid`
	    	LEFT JOIN ad_campaign ac
	    	ON ac.campaignid = ag.campaignid
	    	WHERE 1 and ads.ispause = 0 and ag.adform = $adform 
	    	{$this->_where} {$where}";
	    	 
        } else {
            $sql = "SELECT {$this->_ac_fields} {$this->_ag_fields} {$this->_ads_fields}
	    	IFNULL(ag.otherid,'') AS trackcampaignid,
	    	ast.`path` adtextpath,
	    	ast.adcontent adtext,
	    	ast.adtitle adtexttitle
	    	FROM
	    	`ad_stuff_text` ast
	    	LEFT JOIN `ad_stuff` ads
	    	ON ads.`stuffid` = ast.`stuffid`
	    	LEFT JOIN ad_group ag
	    	ON ag.adgroupid = ads.`adgroupid`
	    	LEFT JOIN ad_campaign ac
	    	ON ac.campaignid = ag.campaignid
	    	WHERE 1 and ads.ispause = 0 and ag.adform = $adform 
	    	{$this->_where} {$where}";
        }
        //echo $sql;
         
        //echo $sql;
        /*SDK版本，本期无，加SDK版本时或定向修改时，请注意文字 广告处理*/
        $res = $this->_db->query($sql);
        $this->lastsql = $sql;
        //     	echo $sql;
        $list = $this->_db->fetch_array($res);
        $text = '';
        foreach($list as $val){
            //$val['adtextpath'] = PIC_URL.$val['adtextpath'];
            $val = $this->getCommon($val);
            if($val == false)
            {
                continue;
            }
            $tmp[$val['campaignid']] = $val;
        }
        return $tmp;
    }





    /**
     * 获取渠道六加一数据
     * 需要按照PID asc和position desc排序
     */
    function getSubInfo()
    {
        $time = time();
        //select @rownum:=@rownum+1 as romnum,m.* from (select * from ad_sub_channel ,(select @rownum:=0) t order by position desc) m
        $sql = "SELECT `id` as `subid`,`appid`, `publisherID`, `packagename`,`campaignid`,`adgameid`, `adcontent`,`gametype`,`position`, `put_time`, `lastupdatetime`, `time`, `status` FROM `ad_sub_channel` WHERE `status` = 1 order by publisherID asc,position desc";
        //                echo $sql.'<br>';
        $res = $this->_db->query($sql);
        $data = $this->_db->fetch_array($res);
        if(empty($data)) return array();
        $pidsdata = array();
        $campaignidsdata = array();
        $adnameids = array();
        $campSubRow = array();

        $_tmp_order = 1;
        $_tmp_last_pid = null;
        foreach($data as $key=>&$val){
            // echo '<br/>'.$val['publisherID'];
            if($_tmp_last_pid==null) {
                $_tmp_last_pid = $val['publisherID'];
            } else {
                if($_tmp_last_pid == $val['publisherID']) {
                    $_tmp_order++;
                } else {
                    $_tmp_order=1;
                    $_tmp_last_pid = $val['publisherID'];
                }
            }

            $val['order'] = $_tmp_order;
            $campaignidsdata[$val['campaignid']] = $val['campaignid'];
            $pidsdata[$val['publisherID']][$val['position']] = array($val['campaignid'], $val['gametype']);
            $adnameids[] = $val['adgameid'];
            $campSubRow[$val['campaignid']] = $val;
        }



        //查询adnames的packagelist信息
        $sql4adgame = "select * from ad_game_list where id in (".implode(',', $adnameids).")";
        $adgameDs = $this->_db->query($sql4adgame);
        $adgameRs = $this->_db->fetch_array($adgameDs);
        $adgameRsArray = array();
        foreach($adgameRs as $val) {

            $adgameRsArray[$val['id']] = $val;
        }

        $campaigndata = $this->getSubData($campaignidsdata);
        //print_r($campaigndata);
        if(empty($campaigndata))
        {
            return array();
        }
        $redata = array();
        //print_r(array_keys($campaigndata));

        foreach($pidsdata as $key => $val)
        {
            foreach($val as $k => $v)
            {
                if(isset($campaigndata[$v[0]]))
                {
                    $campaigndata[$v[0]]['apptype'] = $v[1];
                    //$campaigndata[$v[0]]['order'] = $k;//数据库真实position值
                    $campid = $campaigndata[$v[0]]['campaignid'];
                    $camp_adgameid = $campSubRow[$campid]['adgameid'];
                    //print_r($campSubRow[$campid]);
                    $pkgliststr=$adgameRsArray[$camp_adgameid]['pkglist'];
                    $_tmppgklist = array();
                    if(!empty($pkgliststr)) {
                        $_tmppgklist = explode(';', $pkgliststr);
                        array_filter($_tmppgklist);
                        // print_r($campSubRow[$campid]);
                    }

                    $_tmppgklist[] =$campSubRow[$campid]['packagename'];
                    array_filter($_tmppgklist);
                    
                    $_tmppgklist = array_unique($_tmppgklist);
                    $pkglist = array();
                    foreach($_tmppgklist as $_tmppkg) {
                        $pkglist[] = $_tmppkg;
                    }
                    unset($_tmppgklist);
                    
                    $campaigndata[$v[0]]['packnamelist'] =$pkglist ;
                    $campaigndata[$v[0]]['packagename'] =$campSubRow[$campid]['packagename'];
                    $campaigndata[$v[0]]['order'] = $campSubRow[$campid]['order'];//倒叙排序的rowid 新添加的显示到第一个
                    $redata[$key][$campaigndata[$v[0]]['adid']] = $campaigndata[$v[0]];
                }
            }
        }

        // print_r($pidsdata['100011-0A2E90-7CDB-6FD3-A9B983ABBBBA']);
        //print_r($redata);
        return $redata;
    }

    /**
     * 获取交叉换量数据
     */
    function getSubData($cids)
    {
        $time = $this->_nowtime;
        $this->_ac_fields .= "ag.selfappset,";
        if(empty($cids))
        {
            return array();
        }
        $where = " and ac.campaignid in (".implode(',', $cids).")";
        $sql = "SELECT {$this->_ac_fields} {$this->_ag_fields} {$this->_ads_fields}
              IFNULL(ag.otherid,'') AS trackcampaignid,
              ads.gatherurl,
              asm.appname adtitle,
              asm.appcontent adcontent,
              asm.path moregameicon,
              asm.istop,
              asm.isintegral,
              asm.integralsort as `order`,
              asm.topsort as `torder`,
              asm.star
            FROM
              `ad_stuff_moregame` asm 
              LEFT JOIN `ad_stuff` ads 
                ON ads.`stuffid` = asm.`stuffid` 
              LEFT JOIN ad_group ag 
                ON ag.adgroupid = ads.`adgroupid` 
              LEFT JOIN ad_campaign ac 
                ON ac.campaignid = ag.campaignid 
            WHERE 1 and ads.ispause = 0 and ag.adform = 10 and ag.starttime <= $time and ag.endtime >= $time {$this->_where} {$where}";
        //                echo $sql;
        /*SDK版本，本期无，加SDK版本时或定向修改时，请注意文字 广告处理*/
        $res = $this->_db->query($sql);
        $this->lastsql = $sql;
        $list = $this->_db->fetch_array($res);
        $tmp = array();
        $text = '';
        $users = '';
        foreach($list as $val){
            $val['ostype'] = $this->getOsType($val['ostype']);
            $val = $this->getUnitPrice($val);
            $val = $this->getBothOsVersion($val);
            $val = $this->getBothSDKVersion($val);
            if($val['creative_type'] == 2){
                $text .= $val['adid'].',';
            }
            if($val['selfappset']){
                $users[$val['userid']] = $val['userid'];
            }
            $val['moregameicon'] = PIC_URL.$val['moregameicon'];
            if($val['apptypeset'] == 0)
            {
                $val['appcategories'] = array();
            }
            else
            {
                $val['appcategories'] = explode(',', $val['appcategories']);
            }
            if(empty($val['adcategories']))
            {
                $val['adcategories'] = array();
            }
            else
            {
                $val['adcategories'] = explode(',', $val['adcategories']);
            }
            $val = $this->getCommon($val);
            if($val == false)
            {
                continue;
            }
            $tmp[$val['campaignid']] = $val;
        }
        return $tmp;
    }

    /**
     * 获取攻略配置数据
     * adform 12
     */
    function getGonglueConfigData() {


        $sql = "select apc.* from `app_channel_config` apc inner join `app_info` ai on ai.appid = apc.appid where `ai`.`is_ccplay_gonglue`=1 and `configname`='gonglueurl'";
        $res = $this->_db->query($sql);
        $data = $this->_db->fetch_array($res);

        $appids = array();
        $campaignids = array();
        $appidPids=array();//appid=>array(pid)
        //kefu table info .
        foreach($data as $key =>$val) {
            $appids[] = $val['appid'];
            $campaignids[] = $val['campaignid'];
            $appidRowMap[$val['appid']] = $val;
        }

        $appids = array_unique($appids);
         
        $result = $this->getAdFlowByCampaignIds($campaignids,CONST_GONGLUE_BOUGHT_TYPE,true);
        $sql4queryChannels='select * from app_channel where is_ccplay_gonglue=1 and appid in ('.implode(',', array_merge(array('0'),$appids)).')';
        //echo $sql4queryChannels;

        $appchannelsDs = $this->_db->query($sql4queryChannels);
        $appchannels = $this->_db->fetch_array($appchannelsDs);
        //print_r($appchannels);
        $campaignMap = array();
        foreach($result as $key=>$val) { //result = ad stream info .
            $campaignMap[$val['campaignid']] = $val;
        }
        //var_dump($campaignMap);exit();

        $finalResult = array();
        foreach($appchannels as $val) {
            $appid = $val['appid'];

            $row = $appidRowMap[$appid];
            $campid = $row['campaignid'];//MUST HAVE
            $adid = $row['adid'];

            if($appid!=null && $adid==null) {
                $adid = $campaignMap[$campid]['adid'];
            }
            //check adid is not null
             
            if($adid!=null) {
                $finalResult[$val['publisherID']] = array(
            				        'campaignid'=>$row['campaignid'].'',
            				        'adgroupid'=>$campaignMap[$campid]['adgroupid'],
            				        'adid'=>$adid,
            				        'userid'=>$campaignMap[$campid]['userid'],
            				        'starttime'=>$campaignMap[$campid]['starttime'],
            				        'endtime'=>$campaignMap[$campid]['endtime'],
            				        'trackcampaignid'=>$campaignMap[$campid]['trackcampaignid'],
            				        'url'=>'http://'.$row['configvalue']
                );
            }
        }
         
        return $finalResult;

    }

    /**
     * 获取客服配置数据
     * adform 13
     */
    final function getKefuConfigData() {

        $sql = "select * from `app_channel_config` inner join app_info ai on `ai`.`appid` = `app_channel_config`.`appid` where `ai`.`is_ccplay_kefu`=1 and `configname`='kefutel'";
        $res = $this->_db->query($sql);
        $data = $this->_db->fetch_array($res);

        $appids = array();
        $campaignids = array();
        $appidPids=array();//appid=>array(pid)
        //kefu table info .
        foreach($data as $key =>$val) {
            $appids[] = $val['appid'];
            $campaignids[] = $val['campaignid'];
            $appidRowMap[$val['appid']] = $val;
        }

        $appids = array_unique($appids);
         
        $result = $this->getAdFlowByCampaignIds($campaignids,CONST_KEFU_BOUGHT_TYPE,true);
        $sql4queryChannels='select * from app_channel where  is_ccplay_kefu=1 and appid in ('.implode(',', array_merge(array('-1'),$appids)).')';
        //echo $sql4queryChannels;

        $appchannelsDs = $this->_db->query($sql4queryChannels);
        $appchannels = $this->_db->fetch_array($appchannelsDs);
        //print_r($appchannels);
        $campaignMap = array();
        foreach($result as $key=>$val) { //result = ad stream info .
            $campaignMap[$val['campaignid']] = $val;
        }
        //var_dump($campaignMap);exit();

        $finalResult = array();
        foreach($appchannels as $val) {
            $appid = $val['appid'];

            $row = $appidRowMap[$appid];
            $campid = $row['campaignid'];//MUST HAVE
            $adid = $row['adid'];

            if($appid!=null && $adid==null) {
                $adid = $campaignMap[$campid]['adid'];
            }
            //check adid is not null
             
            if($adid!=null) {
                if($row['configvalue']=='') {
                    $row['configvalue'] = '400-666-1551';
                } else {
                    $row['configvalue'] = trim($row['configvalue'],'-');
                }
                $finalResult[$val['publisherID']] = array(
    						'campaignid'=>$row['campaignid'].'',
    						'adgroupid'=>$campaignMap[$campid]['adgroupid'],
    						'adid'=>$adid,
    						'userid'=>$campaignMap[$campid]['userid'],
    						'starttime'=>$campaignMap[$campid]['starttime'],
    						'endtime'=>$campaignMap[$campid]['endtime'],
    						'call'=>$row['configvalue'],
    						'click_effect'=>$campaignMap[$campid]['click_effect'],
    				        'trackcampaignid'=>$campaignMap[$campid]['trackcampaignid']
                );
            }
        }
        return $finalResult;
    }

    /**
     * 获取ios 精品推荐 栏目信息
     */
    function getIosMgCate(){
        $sql = "SELECT id as categoryid,catename as categoryname ,tplid,sortid,isindex,appid,ostype FROM  `ad_mg_cate` Where status=1 and type=1";
        $res = $this->_db->query($sql);
        $this->lastsql = $sql;
        //echo $sql;
        $list = $this->_db->fetch_array($res);
        if(empty($list))
        {
            return null;
        }
        $reData = array();
        $dataOneCate = array();
        foreach($list as $v)
        {
            $dataOneCate[$v['categoryid']] = $v;
            $reData[$v['appid']][$v['categoryid']] = $v;
        }
        $sqlt = "SELECT am.id,am.pid,am.sortid,am.cateid,am.cate_alias,ac.catename,ac.tplid,ac.appid,ac.ostype FROM `ad_mg_cate_relation` am LEFT JOIN `ad_mg_cate` ac ON am.cateid=ac.`id` WHERE ac.`status` = 1;";
        $rest = $this->_db->query($sqlt);
        $this->lastsql = $sqlt;
        //echo $sql;
        $listt = $this->_db->fetch_array($rest);
        $dataTwoCate = array();
        if(!empty($listt))
        {
            foreach($listt as $v)
            {
                if(isset($dataOneCate[$v['pid']]))
                {
                    $reData[$dataOneCate[$v['pid']]['appid']][$v['cateid']]['categoryid'] = $v['cateid'];
                    if(!empty($v['cate_alias']))
                    {
                        $reData[$dataOneCate[$v['pid']]['appid']][$v['cateid']]['categoryname'] = $v['cate_alias'];
                    }
                    else
                    {
                        $reData[$dataOneCate[$v['pid']]['appid']][$v['cateid']]['categoryname'] = $v['catename'];
                    }
                    $reData[$dataOneCate[$v['pid']]['appid']][$v['cateid']]['tplid'] = $v['tplid'];
                    $reData[$dataOneCate[$v['pid']]['appid']][$v['cateid']]['sortid'] = $v['sortid'];
                    $reData[$dataOneCate[$v['pid']]['appid']][$v['cateid']]['isindex'] = 0;
                    $reData[$dataOneCate[$v['pid']]['appid']][$v['cateid']]['parentid'] = $v['pid'];
                    $reData[$dataOneCate[$v['pid']]['appid']][$v['cateid']]['appid'] = $dataOneCate[$v['pid']]['appid'];
                    $reData[$dataOneCate[$v['pid']]['appid']][$v['cateid']]['ostype'] = $v['ostype'];
                }
            }
        }
        return $reData;

    }
    /**
     * 获取单独设置ios 精品推荐 的AppId信息
     */
    function getIosMgTpl(){
        $sql = "SELECT appid FROM ad_mg_cate WHERE `status`=1 GROUP BY appid";
        $res = $this->_db->query($sql);
        $this->lastsql = $sql;
        //echo $sql;
        $list = $this->_db->fetch_array($res);
        $relist = array();
        if(!empty($list))
        {
            foreach ($list as $v)
            {
                if(!empty($v['appid']))
                {
                    $relist[$v['appid']] = $v['appid'];
                }
            }
        }
        return $relist;
    }
    /**
     * 原生精品推荐广告详情 details
     */
    function getAdDetails(){
        $sql = "SELECT info.othername, info.othertype,'0' praise, '0' unpraise,imgpath thumbs,versionname appver,
                IFNULL(ads.target, info.downloadurl) downurl, appname name,info.num,info.size,info.icon,info.packagename packname ,info.apptype,
                info.versioncode version_code,info.appdesc intro,info.price appprice,info.oldprice oldprice,info.storeid,info.releasenotes rnotes ,
                info.android_api_level api_level, info.level,
                IF(app.stuffid = 0,3,1) own,
                info.ostype
                 FROM ad_mg_info info 
                LEFT JOIN `ad_stuff` ads ON info.storeid = ads.storeid
                LEFT JOIN `ad_mg_apply` app ON info.storeid = app.storeid WHERE info.storeid != '' GROUP BY info.storeid 
        ";
        $res = $this->_db->query($sql);
        $this->lastsql = $sql;
        //echo $sql;
        $list = $this->_db->fetch_array($res);
        foreach($list as $key=>$val){
            if(!$val['thumbs'] or $val['thumbs'] == 'null'){
                $list[$key]['thumbs'] = NULL;
            }
            $val['thumbs'] = json_decode($val['thumbs']);
            if(!empty($val['thumbs']))
            {
                foreach($val['thumbs'] as $k=>$v){
                    if(check_url($v) == false)
                    {
                        $val['thumbs'][$k] =  PIC_URL .$v;
                    }
                    else
                    {
                        $val['thumbs'][$k] =  $v;
                    }
                }
            }
            $val['thumbs'] = json_encode( $val['thumbs']);
            if(check_url($val['icon']) == false)
            {
                $val['icon'] = PIC_URL .$val['icon'];
            }
            else
            {
                $val['icon'] = $val['icon'];
            }
            if(!empty($val['othername']))
            {
                $val['name'] = $val['othername'];
            }
            if(!empty($val['othertype']))
            {
                $val['apptype'] = $val['othertype'];
            }
            $list[$key] = $val;
        }
        return $list;
    }



    /**
     * 获取FAQ列表数据
     * FAQ adform 14
     */
    function getFaqListData() {
        $sql = "select f.* from `ccplay_faq` f inner join `app_info` ai on ai.appid=f.appid where ai.is_ccplay_kefu=1";
        $res = $this->_db->query($sql);

        $list = $this->_db->fetch_array($res);
        $appids = array();
        $campaignids = array();

        $appidRowMapAggregate = array();
        $appidRowMap = array();

        //filter kefu switch on PID.

        //kefu table info .
        foreach($list as $key =>$val) {
            $appids[] = $val['appid'];
            $campaignids[] = $val['campaignid'];

            if(!isset($appidRowMapAggregate[$val['appid']])) {
                $appidRowMapAggregate[$val['appid']] = $val;
            }

            $appidRowMap[$val['appid']][] = $val;
        }

        foreach ($appidRowMapAggregate as $rk=>&$rv) {
            $rv['faqs'] =array();
            foreach($appidRowMap[$rv['appid']] as $inner_rk =>$inner_rv) {
                $rv['faqs'][] = array('q'=>$inner_rv['q'],'a'=>$inner_rv['a'],'order'=>$inner_rv['orderpriority']);
            }
        }

        $appids = array_unique($appids);
         
        $result = $this->getAdFlowByCampaignIds($campaignids,CONST_FAQ_BOUGHT_TYPE,true);
        $sql4queryChannels='select * from app_channel where  is_ccplay_kefu=1 and appid in ('.implode(',', array_merge(array('-1'),$appids)).')';
        //echo $sql4queryChannels;

        $appchannelsDs = $this->_db->query($sql4queryChannels);
        $appchannels = $this->_db->fetch_array($appchannelsDs);
         
        $finalResult = array();


        /**
         * 广告活动ID campaignid  :  1
         投放类型 targettype : 1：App 2：Website 3：cocosplay
         广告组ID adgroupid  :  1
         创意ID adid  :  1
         投放类型 bought_type  :  14
         日预算 daybudget  :  100000000
         组预算 groupbudget  :  1000000000
         广告主ID userid : 1234
         广告所属用户组 own  :  (1：广告主，2：开发者）
         投放开始时间 starttime  :  1381939200
         投放结束 endtime  :  1387209600
         trackcampaignid      追踪推广ID
         faqs：[{\"q\":\"ccc\",\"a\":\"bbbb\"}]

         */
        foreach($appchannels as $val) {
            $row = $appidRowMapAggregate[$val['appid']];

            $campid = $row['campaignid'];
            $camrow = $result[$campid];
            $finalResult[$val['publisherID']] = array(
            	'campaignid'=>$campid,
            	'targettype'=>$camrow['targettype'],
                'adgroupid'=>$camrow['adgroupid'],
            	'adid'=>$camrow['adid'],
            	'bought_type'=>CONST_FAQ_BOUGHT_TYPE,
            	'userid'=>$camrow['userid'],
            	'own'=>2,
            	'starttime'=>$camrow['starttime'],
            	'endtime'=>$camrow['endtime'],
                'trackcampaignid'=>$camrow['trackcampaignid'],
            	'faqs'=>$row['faqs']
            );
        }
        return $finalResult;
    }


    /**
     * 精品推荐列表推送
     */
    function getCateMgGame() {
        $isupdate = '';
        if ($this->_isupdate) {
            $isupdate = " and ac.isupdate > 0 ";
        }
        $sql = "SELECT id FROM `ad_mg_cate` WHERE tplid =2 AND status = 1";
        $res = $this->_db->query($sql);
        $list = $this->_db->fetch_array($res);
        $cateid_str = '';
        foreach ($list as $key => $val) {
            $cateid_str .= $val['id'] . ',';
        }

        $where_notin = '';
        $where_in = '';
        $cateid_str = trim($cateid_str, ',');
        if ($cateid_str) {
            $where_notin = "AND app.cateid not in($cateid_str)";
            $where_in = " or (app.cateid in($cateid_str)) ";
        }

        $time = $this->_nowtime;
        $this->_ac_fields .= "ag.selfappset,";
        $sql = "SELECT {$this->_ac_fields} {$this->_ag_fields} {$this->_ads_fields}
              IFNULL(ag.otherid,'') AS trackcampaignid,
               ac.iosvset device_os_versions,
              ads.gatherurl,
              info.appname adtitle,
              info.othername,
              info.othertype,
              info.appdesc adcontent,
              IFNULL(asm.path, info.icon) moregameicon,
              asm.istop,
              asm.isintegral,
              app.sortid as `order`,
              asm.topsort as `torder`,
              asm.star,
              IF(app.stuffid = 0,3,1) own,
              IFNULL(ads.stuffid,app.storeid) adid,
              app.storeid,
              app.cateid,
              info.price appprice,
              info.packagename mgpackagename,
              info.versionname mgversionname,
              info.oldprice,
              info.apptype,
              info.ostype dataostype,
              info.size appsize,
              IFNULL(ads.target, info.downloadurl) url
            FROM ad_mg_apply app
              LEFT JOIN `ad_stuff` ads 
                ON ads.`storeid` = app.`storeid`   AND app.stuffid = ads.stuffid              
              LEFT JOIN  `ad_stuff_moregame` asm             
                ON app.stuffid = asm.`stuffid`         
              LEFT JOIN ad_group ag 
                ON ag.adgroupid = ads.`adgroupid` 
              LEFT JOIN ad_campaign ac 
                ON ac.campaignid = ag.campaignid       
             LEFT JOIN ad_mg_info info
                ON app.`storeid` = info.`storeid`
            WHERE  app.status = 1 AND app.isgather = 1 AND app.storeid != '' AND (( ((1 and ads.ispause = 0 and ag.adform = 3 and ag.starttime <= $time and ag.endtime >= $time {$this->_where} AND ads.stuffid IS NOT NULL) OR ( ads.stuffid IS  NULL)) " . $isupdate . " $where_notin) $where_in ) GROUP BY app.cateid, app.storeid";
        /* SDK版本，本期无，加SDK版本时或定向修改时，请注意文字 广告处理 */
        //echo $sql;
        $res = $this->_db->query($sql);
        $this->lastsql = $sql;
        $list = $this->_db->fetch_array($res);
        $tmp = array();
        $text = '';
        $users = '';
        foreach ($list as $key => $val) {
            //$val['ostype'] = $this->getOsType($val['ostype']);
            if(empty($val['packagename']))
            {
                $val['packagename'] = $val['mgpackagename'];
                $val['appversion'] = $val['mgversionname'];
            }
            if($val['dataostype'] == 3)
            {
                $val['ostype'] = $this->getOsType(1);
            }
            else
            {
                $val['ostype'] = $this->getOsType(2);
            }
            $val = $this->getUnitPrice($val);
            $val = $this->getBothOsVersion($val);
            $val = $this->getBothSDKVersion($val);
            if($val['osversionset'] == 1)
            {
                $val['device_os_versions'] = $val['iosvset'];
            }
            else
            {
                $val['device_os_versions'] = array();
            }
            if ($val['creative_type'] == 2) {
                $text .= $val['adid'] . ',';
            }
            if ($val['selfappset']) {
                $users[$val['userid']] = $val['userid'];
            }
            if(check_url($val['moregameicon']) == false)
            {
                $val['moregameicon'] = PIC_URL . $val['moregameicon'];
            }
            else
            {
                $val['moregameicon'] = $val['moregameicon'];
            }
            if ($val['apptypeset'] == 0) {
                $val['appcategories'] = array();
            } else {
                $val['appcategories'] = explode(',', $val['appcategories']);
            }
            if(empty($val['adcategories']))
            {
                $val['adcategories'] = array();
            }
            else
            {
                $val['adcategories'] = explode(',', $val['adcategories']);
            }
            if(!empty($val['othername']))
            {
                $val['adtitle'] = $val['othername'];
            }
            if(!empty($val['othertype']))
            {
                $val['apptype'] = $val['othertype'];
            }
            //原生精品推荐android的下载地址不需要加 #pkg... 等信息
            $url = $val['url'];
            $val = $this->getCommon($val);
            if($val['ostype'] == 'ANDROID')
            {
                $val['url'] = $url;
            }
            if ($val == false) {
                continue;
            }
            $tmp[$val['cateid']][] = $val;
        }

        return $tmp;
    }
    /**
     * 原生精品推荐专题推广
     */
    function getMgSpecial() {
        $sql = "SELECT * FROM `ad_mg_special` WHERE status = 1 order by id DESC";
        $res = $this->_db->query($sql);
        $this->lastsql = $sql;
        //echo $sql;
        $ret = $this->_db->fetch_array($res);
        $lists = array();
        foreach ($ret as $key => $var) {
            $list = array();
            $spec_type = $var['spec_type'];
            $subjectid = $var['subjectid'];
            if ($spec_type == '1') {
                $list = $this->getMySpecialByid($subjectid, $var['ostype']);
            } elseif ($spec_type == '2') {
                $list['cateid'] = $subjectid;
            }
            $list['specialid'] = $var['id'];
            $list['adtitle'] = $var['title'];
            $list['specialtype'] = $var['spec_type'];
            $list['specialimg'] = PIC_URL . $var['pic'];
            $list['order'] = $var['sortid'];
            $list['appid'] = $var['appid'];
            $list['ostype'] = $this->getOsType($var['ostype']);
            $lists[] = $list;
        }
        return $lists;
    }

    function getMySpecialByid($subjectid = '0', $ostype) {
        $tmp = array();
        $isupdate = '';
        if ($this->_isupdate) {
            $isupdate = " and ac.isupdate > 0 ";
        }
        $text = '';
        $time = $this->_nowtime;
        $this->_ac_fields .= "ag.selfappset,";
        $sql = "SELECT {$this->_ac_fields} {$this->_ag_fields} {$this->_ads_fields}
                IFNULL(ag.otherid,'') AS trackcampaignid,
                ac.iosvset device_os_versions,
                ads.gatherurl,
                info.appname adtitle,
                info.appdesc adcontent,
                IFNULL(asm.path, info.icon) moregameicon,
                asm.istop,
                asm.isintegral,
                asm.integralsort as `order`,
                asm.topsort as `torder`,
                asm.star,
                IF(ads.stuffid = 0,3,1) own,
                info.price appprice,
                info.oldprice,
                info.apptype,
                info.size appsize
              FROM
                `ad_stuff_moregame` asm 
                LEFT JOIN `ad_stuff` ads 
                  ON ads.`stuffid` = asm.`stuffid` 
                LEFT JOIN ad_group ag 
                  ON ag.adgroupid = ads.`adgroupid` 
                LEFT JOIN ad_campaign ac 
                  ON ac.campaignid = ag.campaignid 
                LEFT JOIN ad_mg_info info
                   ON ads.`storeid` = info.`storeid`
              WHERE 1 and ads.ispause = 0 and ag.adform = 3 and ag.starttime <= $time and ag.endtime >= $time {$this->_where} " . $isupdate . " AND ads.storeid = " . $subjectid ." and ac.ostypeid=".$ostype;
        /* SDK版本，本期无，加SDK版本时或定向修改时，请注意文字 广告处理 */
        // echo $sql;exit;
        $res = $this->_db->query($sql);
        $temp = $this->_db->fetch_array($res);
        if (!empty($temp)) {
            $val = $temp[0];
            $val['ostype'] = $this->getOsType($val['ostype']);
            $val = $this->getUnitPrice($val);
            $val = $this->getBothOsVersion($val);
            $val = $this->getBothSDKVersion($val);
            if($val['osversionset'] == 1)
            {
                $val['device_os_versions'] = $val['iosvset'];
            }
            else
            {
                $val['device_os_versions'] = array();
            }
            if ($val['creative_type'] == 2) {
                $text .= $val['adid'] . ',';
            }

            $val['moregameicon'] = PIC_URL . $val['moregameicon'];
            if ($val['apptypeset'] == 0) {
                $val['appcategories'] = array();
            } else {
                $val['appcategories'] = explode(',', $val['appcategories']);
            }
            if(empty($val['adcategories']))
            {
                $val['adcategories'] = array();
            }
            else
            {
                $val['adcategories'] = explode(',', $val['adcategories']);
            }
            $val = $this->getCommon($val);
        } else {
            $val['own'] = '3';
            $val['adid'] = $subjectid;
            $val['storeid'] = $subjectid;
        }
        return $val;
    }

    /**
     * 框架信息
     */
    function getBorders(){
      
        $sql = "SELECT ab.`bordergroupid`,
              ab.`path`,
              ab.`type`,
			  ab.`isicon`,
              asis.`width`,
              asis.`height`
              FROM `ad_border` ab
              LEFT JOIN `ad_stuff_img_size` asis 
                ON ab.`sizeid` = asis.`sizeid` 
              WHERE 1 and ab.status = 1";
        $res = $this->_db->query($sql);
        $this->lastsql = $sql;
        $borderlist = $this->_db->fetch_array($res);
        $tmp = array();
        foreach($borderlist as $val){
            $val['path'] = PIC_URL.$val['path'];
            
            $file_path = pathinfo($val['path']);
            if(isset($tmp[$val['bordergroupid']])){
				if(isset($val['isicon']) && 1 == $val['isicon']){
					$tmp[$val['bordergroupid']]['CLOSEICON'] = $val['path'];
				}
				else{
					$tmp[$val['bordergroupid']][$val['width'].'X'.$val['height']] = $val['path'];
				}
            }else{
				if(isset($val['isicon']) && 1 == $val['isicon']){
					$val['CLOSEICON'] = $val['path'];
				}
				else{
					$val[$val['width'].'X'.$val['height']] = $val['path'];
				}
                unset($val['path']);
                unset($val['width']);
                unset($val['height']);
                $tmp[$val['bordergroupid']] = $val;
            }
        }
        return $tmp;
    }
	
	/**
     * 获取dsp切量兜底优先级设置
     */
    function getDSPCutoverFloors()
    {
        $sql = "SELECT `medianame` AS name,`mediaport` AS port,`dspcutoverfloors` AS priority FROM `media_cutover_manage`";
        $res = $this->_db->query($sql);
        $data = $this->_db->fetch_array($res);
        $retdata=array();
        $retdata['dsp_floors']=$data;
        return $retdata;
    }
    


	/**
     * 获取Adx系统列表
     */
    function getAdxlist()
    {
        $sql = "SELECT * FROM `ad_exchange` where status = '1'";
        $res = $this->_db->query($sql);
        $data = $this->_db->fetch_array($res);
        return $data;
    }
    
    
    /**
     * 获取appdspcutoverfloors
     */
    function appdspcutoverfloors($appid){
        $sql = "select appid, mediaport,priority from app_dsp_cutover_floors where status='1' and appid='".$appid."'";
        $res = $this->_db->query($sql);
        return $this->_db->fetch_array($res);
    }
}
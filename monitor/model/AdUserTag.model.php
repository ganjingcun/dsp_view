<?php
class AdUserTagModel extends Model{
	public $_tname = 'ad_user_tag';
	private $_db;
	function __construct(){
		parent::__construct();
		$this->_db = $this->getInstance(0);
	}
	
	function delByAdid($adid){
		$sql = "delete from ad_user_tag where adid=".$adid;
		$this->_db->query($sql);
	}
	
	function delAdidsOverDay(){
		$time = strtotime(date('Y-m-d'))-86400;
		$sql = "delete from ad_user_tag where retrievetime<=".$time;
		$this->_db->query($sql);
	}
}
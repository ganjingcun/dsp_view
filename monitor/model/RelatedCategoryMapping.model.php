<?php
class RelatedCategoryMappingModel extends Model{
    protected $_redis = null;
    protected $_db = '';
    private $_nowtime = '';
    public $_tname = 'related_category_mapping';//表名
	public $_dbname = 'adcocodb';
	
	function __construct() {
        parent::__construct();
        $this->_db = $this->getInstance(0);
        $this->_redis = getRedis();
        $this->_nowtime = strtotime(date('Y-m-d'));
    }
    
    function deleteAll(){
    	$sql = "truncate table related_category_mapping";
        $res = $this->_db->query($sql);
        return $this->_db->fetch_array($res);
    }
    
    function addOne($data) {
    	$res = $this->add($data, 0, '','INSERT');
    	return $this->_db->fetch_array($res); 
    }
}
<?php
//系统常量，框架所有加载文件必须通过它来调用
define('IS_SYSTEM', true);

//是否为部署模式（部署模式会生存核心文件缓存，减少加载文件导入数量,非部署模式会记录每次执行的SQL文件到data/debug.sql中）
define('IS_DEPLOY', false);
date_default_timezone_set('PRC');
//框架根目录
define('ROOT_DIR', substr(dirname(__FILE__), 0,-3));
require ROOT_DIR.'lib/init.php';

$app = new appServer();
$app->run();
<?php

class Connection
{
	static $connectionPool = array();

	private $DBSource = null;
	private $defaultDBSourceName = '';
	private $name = '';
	
	private $defaultDBSource = 'PDO';

	private function __construct($name='default',$defaultDBSourceName = 'PDODBSource')
	{
		$this->name = $name;
		$this->defaultDBSourceName = $defaultDBSourceName;
	}


	public static function getConnection($name,$defaultDBSourceName = 'PDODBSource')
	{
		$id = $name.$defaultDBSourceName;
		if(!isset(self::$connectionPool[$id]))
		{
			self::$connectionPool[$id] = new Connection($name,$defaultDBSourceName);
			self::$connectionPool[$id]->connect();
		}

		return self::$connectionPool[$id];
	}


	public static function closeConnection($name = null,$defaultDBSourceName = 'PDODBSource')
	{
		if( $name )
		{
			$id = $name.$defaultDBSourceName;
			if(isset(self::$connectionPool[$id]))
			{
				self::$connectionPool[$id]->close();
				unset(self::$connectionPool[$id]);
			}
		}
		else
		{
			foreach(self::$connectionPool as $con)
			{
//				$con->close();
			}
			self::$connectionPool = array();
		}
		
		

	}



	private function connect()
	{
		if($this->DBSource) return true;

		$config = $this->getDbConfig();
		try{
			
			$pdo = new PDO( $config['dsn'],$config['username'],$config['password'],
			array(
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_PERSISTENT => false,//短连接
			PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"
			));
			
			if( $this->defaultDBSourceName=='PDO' )
			{
				$this->DBSource = &$pdo;
			}
			else
			{
				$this->DBSource = new $this->defaultDBSourceName($pdo);
			}
			
			return $this;
			
		}
		catch(Exception $e)
		{
			echo 'MYSQL ERROR: '.$e->getMessage();
		}

	}


	public function getDbConfig()
	{
		include(APPPATH.'config/database.php');
		return $pdo_drivers[$this->name];
	}
	
	public function getName()
	{
		return $this->name;
	}
	
	public function getDBSource()
	{
		return $this->DBSource;
	}
	
	public function setDBSource($DBSource)
	{
		$this->DBSource = $DBSource;
	}

	public function close()
	{
//		$this->DBSource->close();
	}

}
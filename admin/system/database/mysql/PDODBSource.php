<?php

class PDODBSource implements DBSource{
	private $stmt;
	private $pagesize;
	private $pageno = 1;
	private $main_row;
	private $query_row;
	private $charset = '';
	private $log = null;

	private $pdo = null;


	function __construct(PDO &$pdo) {
		$this->pdo = $pdo;

	}

	public function prepare($sql) {
		$this->stmt = $this->pdo->prepare($sql);
		return $this;
	}

	public function query_prepare($sql) {
		$this->stmt = $this->pdo->prepare($sql);
		return $this;
	}

	public function bind($param,$var) {
		$this->stmt->bindParam($param,$var);
		return $this;
	}

	public function query_bind($param,$var) {
		$this->stmt->bindParam($param,$var);
		return $this;
	}

	public function bindlob($param,&$var) {
		$this->stmt->bindParam($param,$var,PDO::PARAM_LOB);
		return $this;
	}

	public function query_bindlob($param,&$var) {
		$this->stmt->bindParam($param,$var,PDO::PARAM_LOB);
	}

	public function execute() {
		$this->stmt->execute();
		return $this;
	}

	public function query_execute() {
		$this->stmt->execute();
		return $this;
	}

	public function query($sql){
		
		if(defined('SQL_DEBUG') and SQL_DEBUG==true)
		{
			$timer = new Timer();
			$timer->start();
		}

		$msg = '';
		try
		{
		    $this->stmt = '';
			$this->stmt = $this->pdo->query($sql);
		}
		catch(Exception $e)
		{
			$msg = $e->getMessage();
		}
		

		if(defined('SQL_DEBUG') and SQL_DEBUG==true)
		{
			$timer->stop();
			$timer->setSql($sql);
			$timer->setError($msg);
			SQLLog::write($timer);
			
		}
		
		if(!empty($msg))
		{
			throw new Exception($msg);	
		}

		return $this;
	}


	public function next() {
		$this->main_row->$this->stmt->fetch();
		return $this->main_row;
	}

	public function query_next() {
		$this->query_row = $this->stmt->fetch();
		return $this->query_row;
	}

	public function getall() {
		return $this->stmt->fetchAll();
	}

	public function query_getall( $type=PDO::FETCH_ASSOC ) {
		return $this->stmt->fetchAll($type);
	}

	public function getsingle($sql) {
		$this->query_prepare($sql);
		$this->query_execute();
		if($row = $this->stmt->fetch()) {
			$this->stmt = null;
			return $row;
		} else {
			return null;
		}
	}

	public function query_getsingle( $type=PDO::FETCH_ASSOC ) {
		$row = $this->stmt->fetch($type);
		$this->stmt = null;
		return $row;
	}

	public function begintrans() {
		$this->pdo->beginTransaction();
		return $this;
	}

	public function commit(){
		$this->pdo->commit();
		return $this;
	}

	public function rollback(){
		$this->pdo->rollBack();
		return $this;
	}

	public function stmt_close() {
		$this->stmt = null;
		return $this;
	}

	public function close() {
		$this->pdo = null;
	}

	public function query_close() {
		$this->pdo = null;
	}

	public function lastInsertID() {
		return $this->pdo->lastInsertId();
	}

	public function getrowcount() {
		return $this->stmt->rowCount();
	}
	public function getrowcount_query() {
		return $this->stmt->rowCount();
	}

	public function get($var) {
		return $this->main_row[$var];
	}

	public function query_get($var) {
		return $this->query_row[$var];
	}

	/**
	 * 获取sql执行失败的 错误码 例如 00121
	 */
	public function get_errno()
	{
		return $this->pdo->errorCode();
	}

}
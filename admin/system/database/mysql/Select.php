<?php
class Select
{

	protected $select_sql_columns;
	protected $select_sql_from_where;
	protected $select_sql_group_having;
	protected $select_sql_order_limit;

	public function select($columns = '*')
	{
		$this->select_sql_columns = $columns;
		$this->select_sql_from_where = '';
		$this->select_sql_group_having = '';
		$this->select_sql_order_limit = '';
		return $this;
	}

	public function getSelectSql()
	{
		return "SELECT {$this->select_sql_columns} {$this->select_sql_from_where} {$this->select_sql_group_having} {$this->select_sql_order_limit}";
	}


	public function from($table)
	{
		$table = $this->quoteTableName($table);
		$this->select_sql_from_where .= " FROM $table ";
		return $this;
	}

	protected function joinInternal($join, $table, $cond)
	{
		$table = $this->quoteTableName($table);
		$this->select_sql_from_where .= " $join $table ";
		if (is_string($cond)
		&& (strpos($cond, '=') === false && strpos($cond, '<') === false && strpos($cond, '>') === false)
		) {
			$column = $this->quoteColumnName($cond);
			$this->select_sql_from_where .= " USING ($column) ";
		}
		else
		{
			$cond = $this->buildCondition($cond);
			$this->select_sql_from_where .= " ON $cond ";
		}
		return $this;
	}


	public function join($table, $cond)
	{
		return $this->joinInternal('JOIN', $table, $cond);
	}


	public function leftJoin($table, $cond)
	{
		return $this->joinInternal('LEFT JOIN', $table, $cond);
	}


	public function rightJoin($table, $cond)
	{
		return $this->joinInternal('RIGHT JOIN', $table, $cond);
	}

	public function quoteTableName($s)
	{
		return $this->quoteSqlName($s);
	}

	public function quoteColumnName($s)
	{
		return $this->quoteSqlName($s);
	}

	public function quoteSqlName($s)
	{
		$s = trim($s);
		if (strpos($s, '`') === false && strpos($s, ' ') === false && strpos($s, '.') === false && $s[0] != '(')
		{
			return "`{$s}`";
		}
		return $s;
	}

	protected  function sqlParam($v)
	{
		return (is_int($v) || is_float($v)) ? $v : $this->quote($v);
	}

	public function quote($s)
	{
		if(is_array($s) || is_object($s))
		{
			throw new Exception('value to quote can not be array or object');
		}
			
		//
		//        if($this->connect())
		//        {
		//        	return $this->pdo->quote("$s");
		//        }



		return "'" . str_replace("'", "''", $s) . "'";
	}


	public function where($cond)
	{
		$cond = $this->buildCondition($cond);
		$this->select_sql_from_where .= " WHERE $cond ";
		return $this;
	}


	public function group($group)
	{
		$this->select_sql_group_having .= " GROUP BY $group ";
		return $this;
	}

	public function having($having)
	{
		$this->select_sql_group_having .= " HAVING $having ";
		return $this;
	}


	public function order($order)
	{
		$this->select_sql_order_limit .= " ORDER BY $order ";
		return $this;
	}
	
    public function limit($a, $b = null)
    {
        if (is_null($b)) {
            $a = intval($a);
            $this->select_sql_order_limit .= " LIMIT $a ";
        }
        else
        {
            $a = intval($a);
            $b = intval($b);
            $this->select_sql_order_limit .= " LIMIT $a, $b ";
        }
        return $this;
    }


	public function buildCondition($condition = array(), $logic = 'AND')
	{
		if( ! is_array($condition))
		{
			if (is_string($condition))
			{
				//forbid to use a CONSTANT as condition
				if(strpos($condition, '>') === false && strpos($condition, '<') === false && strpos($condition, '=') === false && strpos($condition, ' ') === false)
				{
					throw new Exception('bad sql condition: must be a valid sql condition');
				}
				return $condition;
			}

			throw new Exception('bad sql condition: ' . gettype($condition));
		}
		$logic = strtoupper($logic);
		$content = null;
		foreach ($condition as $k => $v)
		{
			$v_str = null;
			$v_connect = '';

			if (is_int($k))
			{
				if ($content)
				{
					$content .= $logic . ' (' . $this->buildCondition($v, $logic) . ') ';
				}
				else
				{
					$content = '(' . $this->buildCondition($v, $logic) . ') ';
				}

				continue;
			}

			$maybe_logic = strtoupper($k);
			if (in_array($maybe_logic, array('AND', 'OR')))
			{
				if ($content)
				{
					$content .= $logic . ' (' . $this->buildCondition($v, $maybe_logic) . ') ';
				}
				else
				{
					$content = '(' . $this->buildCondition($v, $maybe_logic) . ') ';
				}

				continue;
			}

			$k_upper = strtoupper($k);
			//the order is important, longer fist, to make the first break correct.
			$maybe_connectors = array('>=', '<=', '<>', '>', '<', '=', 'BETWEEN', 'LIKE', 'IS NOT', 'NOT IN', 'IS', 'IN');

			foreach ($maybe_connectors as $maybe_connector)
			{
				$l = strlen($maybe_connector);
				if (substr($k_upper, -$l) == $maybe_connector)
				{
					$k = trim(substr($k, 0, -$l));
					$v_connect = $maybe_connector;
					break;
				}
			}
			if (is_null($v))
			{
				$v_str = ' NULL';
			}
			else if (is_array($v))
			{
				if($v_connect == 'BETWEEN')
				{
					$v_str = $this->sqlParam($v[0]) . ' AND ' . $this->sqlParam($v[1]);
				}
				else if (isset($v[0]))
				{
					// 'key' => array(v1, v2)
					$v_str = null;
					foreach ($v AS $one)
					{
						if(is_array($one))
						{
							// (a,b) in ( (c, d), (e, f) )
							$sub_items = '';
							foreach($one as $sub_value)
							{
								$sub_items .= ',' . $this->sqlParam($sub_value);
							}
							$v_str .= ',(' . substr($sub_items, 1) . ')' ;
						}
						else
						{
							$v_str .= ',' . $this->sqlParam($one);
						}
					}
					$v_str = '(' . substr($v_str, 1) . ')';
					if (empty($v_connect))
					{
						$v_connect = 'IN';
					}

				}
				else if (empty($v))
				{
					// 'key' => array()
					$v_str = $k;
					$v_connect = '<>';
				}
			}
			else
			{
				$v_str = $this->sqlParam($v);
			}

			if(empty($v_connect))
			{
				$v_connect = '=';
			}


			$quoted_k = $this->quoteColumnName($k);
			if ($content)
			{
				$content .= " $logic ( $quoted_k $v_connect $v_str ) ";
			}
			else
			{
				$content = " ($quoted_k $v_connect $v_str) ";
			}

		}
		return $content;
	}
}
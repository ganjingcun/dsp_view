<?php
class DBDispatcher
{
	protected $masters = array();
	protected $slaves = array();

	public function addMaster(Connection $conn)
	{
		$this->masters[$conn->getName()] = $conn;
	}

	public function addSlave(Connection $conn)
	{
		$this->slaves[$conn->getName()] = $conn;
	}


	/**
	 * 暂时没有添加对Slave库的派发算法
	 * @param unknown_type $name
	 * @return unknown_type
	 */
	public function read($name='')
	{
		if(empty($this->slaves) or ($name and ! isset($this->slaves[$name])))
		{
			return $this->readFromMaster($name);
		}

		if(empty($name))
		{
			reset($this->slaves);
			return current($this->slaves)->getDBSource();
		}
		
		$this->$this->slaves[$name]->getDBSource();
	}

	public function master($name='')
	{
		if(empty($name))
		{
			reset($this->masters);
			return current($this->masters)->getDBSource();
		}
		return $this->masters[$name]->getDBSource();
	}


	private function readFromMaster($name='')
	{
		if(empty($name) or ! isset($this->masters[$name]))
		{
			reset($this->masters);
			return current($this->masters)->getDBSource();
		}
		return $this->masters[$name]->getDBSource();
	}

}
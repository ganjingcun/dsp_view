<?php
/*


$timer = new Timer();
$timer->start();
//...程序运行的代码
$timer->stop();
echo "程序运行时间为:".$timer->spent();


*/
class Timer 
{
	private $StartTime = 0;//程序运行开始时间
	private $StopTime  = 0;//程序运行结束时间
	private $TimeSpent = 0;//程序运行花费时间
	private $error = '';
	
	static $QqueryCount = 0;//执行了多少次调用
	static $TotalTime = 0;//总共花了多长时间

	private $sql = '';

	//程序运行开始
	public function start()
	{
		
		$this->StartTime = microtime();
		self::$QqueryCount++;
	}

	//程序运行结束
	public function stop()
	{
		$this->StopTime = microtime();
		$this->spent();
		self::$TotalTime +=$this->TimeSpent;
	}

	private function spent()
	{//程序运行花费的时间
		if ($this->TimeSpent)
		{
			return $this->TimeSpent;
		}
		else
		{
			list($StartMicro, $StartSecond) = explode(" ", $this->StartTime);
			list($StopMicro, $StopSecond) = explode(" ", $this->StopTime);
			$start = doubleval($StartMicro) + $StartSecond;
			$stop = doubleval($StopMicro) + $StopSecond;
			$this->TimeSpent = $stop - $start;//返回获取到的程序运行时间差 
		}
	}
	
	public static function getQqueryCount()
	{
		return self::$QqueryCount;
	}
	
	public static function getTotalTime()
	{
		return self::$TotalTime;
	}
	
	public function getTimeSpent()
	{
		return $this->TimeSpent;
	}

	public function getStartTime()
	{
		return $this->StartTime;
	}

	public function getStopTime()
	{
		return $this->StopTime;
	}

	public function getSql()
	{
		return $this->sql;
	}

	public function setSql($sql)
	{
		$this->sql = $sql;
	}
	
	public function setError($strError)
	{
		$this->error = $strError;
	}
	
	public function getError()
	{
		return $this->error;
	}


}


<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Initialize the database
 *
 * @category	Database
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/database/
 * @param 	string
 * @param 	bool	Determines if active record should be used or not
 */
function &DB($params = '', $active_record_override = NULL)
{
	//	// Load the DB config file if a DSN string wasn't passed
	if (is_string($params) AND strpos($params, '://') === FALSE)
	{
		// Is the config file in the environment folder?
		if ( ! defined('ENVIRONMENT') OR ! file_exists($file_path = APPPATH.'config/'.ENVIRONMENT.'/database.php'))
		{
			if ( ! file_exists($file_path = APPPATH.'config/database.php'))
			{
				show_error('The configuration file database.php does not exist.');
			}
		}

		include($file_path);

		if ( ! isset($db) OR count($db) == 0)
		{
			show_error('No database connection settings were found in the database config file.');
		}

		if ($params != '')
		{
			$active_group = $params;
		}

		if ( ! isset($active_group) OR ! isset($db[$active_group]))
		{
			show_error('You have specified an invalid database connection group.');
		}

		$params = $db[$active_group];
	}
	elseif (is_string($params))
	{

		/* parse the URL from the DSN string
		 *  Database settings can be passed as discreet
		 *  parameters or as a data source name in the first
		 *  parameter. DSNs must have this prototype:
		 *  $dsn = 'driver://username:password@hostname/database';
		 */

		if (($dns = @parse_url($params)) === FALSE)
		{
			show_error('Invalid DB Connection String');
		}

		$params = array(
							'dbdriver'	=> $dns['scheme'],
							'hostname'	=> (isset($dns['host'])) ? rawurldecode($dns['host']) : '',
							'username'	=> (isset($dns['user'])) ? rawurldecode($dns['user']) : '',
							'password'	=> (isset($dns['pass'])) ? rawurldecode($dns['pass']) : '',
							'database'	=> (isset($dns['path'])) ? rawurldecode(substr($dns['path'], 1)) : ''
							);

							// were additional config items set?
							if (isset($dns['query']))
							{
								parse_str($dns['query'], $extra);

								foreach ($extra as $key => $val)
								{
									// booleans please
									if (strtoupper($val) == "TRUE")
									{
										$val = TRUE;
									}
									elseif (strtoupper($val) == "FALSE")
									{
										$val = FALSE;
									}

									$params[$key] = $val;
								}
							}
	}

	// No DB specified yet?  Beat them senseless...
	if ( ! isset($params['dbdriver']) OR $params['dbdriver'] == '')
	{
		show_error('You have not selected a database type to connect to.');
	}

	/*----------------------------------此处注释,采用pdo方式返回链接----------------*/
	// Load the DB classes.  Note: Since the active record class is optional
	// we need to dynamically create a class that extends proper parent class
	// based on whether we're using the active record class or not.
	// Kudos to Paul for discovering this clever use of eval()
	/*
	if ($active_record_override !== NULL)
	{
	$active_record = $active_record_override;
	}

	require_once(BASEPATH.'database/DB_driver.php');

	if ( ! isset($active_record) OR $active_record == TRUE)
	{
	require_once(BASEPATH.'database/DB_active_rec.php');

	if ( ! class_exists('CI_DB'))
	{
	eval('class CI_DB extends CI_DB_active_record { }');
	}
	}
	else
	{
	if ( ! class_exists('CI_DB'))
	{
	eval('class CI_DB extends CI_DB_driver { }');
	}
	}

	require_once(BASEPATH.'database/drivers/'.$params['dbdriver'].'/'.$params['dbdriver'].'_driver.php');

	// Instantiate the DB adapter
	$driver = 'CI_DB_'.$params['dbdriver'].'_driver';
	$DB = new $driver($params);

	if ($DB->autoinit == TRUE)
	{
	$DB->initialize();
	}

	if (isset($params['stricton']) && $params['stricton'] == TRUE)
	{
	$DB->query('SET SESSION sql_mode="STRICT_ALL_TABLES"');
	}
	*/
	/*---------重写结束---------*/

	//采用pdo方式返回数据库连接
	try{
		//to connect
		$DB = new My_Pdo($params['dbdriver'].':host='.$params['hostname'].'; dbname='.$params['database'], $params['username'], $params['password'],$params['char_set']);
	}
	catch(PDOException $e) {
		echo 'Please contact Admin: '.$e->getMessage();
	}
	return $DB;
}



class My_Pdo{

	private $scheme = '';
	private $username = '';
	private $password = '';
	private $charset = '';

	function My_Pdo($scheme,$username,$password,$charset = 'utf8'){
		$this->scheme = $scheme;
		$this->username = $username;
		$this->password = $password;
		$this->charset = $charset;
	}

	private function instance($mod){
		return new PDODB($mod,$this->scheme,$this->username,$this->password,$this->charset);
	}

	/**
	 * 返回一个写库的对象
	 * 因为也可以执行查询操作，故命名为master而不是write
	 * @return PDODB
	 */
	public function master(){
		return $this->instance(PDODB::TYPE_WRITE_CONNECTION);
	}

	/**
	 * 返回一个读库对象
	 * @return PDODB
	 */
	public function read(){
		return $this->instance(PDODB::TYPE_READ_CONNECTION);
	}
	
	/**
	 * 为兼容原生pdo而设计，用此方法可以直接操作原生pdo
	 * @return PDO
	 */
	public function pdo(){
		return new PDO($this->scheme, $this->username, $this->password,array (PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES '".$this->charset."'"));
	}

}





class PDODB {
	private $main_conn;
	private $query_conn;
	private $main_stmt;
	private $query_stmt;
	private $pagesize;
	private $pageno = 1;
	private $main_row;
	private $query_row;
	//    private $main_arr = array("mysql:host=10.101.2.231;dbname=MY-DBNAME","root","MY-PASSWORD");
	private $query_arr = array("mysql:host=10.101.2.231;dbname=MY-DBNAME","root","MY-PASSWORD",);
	private $charset = '';

	const TYPE_WRITE_CONNECTION = 1;
	const TYPE_READ_CONNECTION = 2;
	

	function __construct($mod = self::TYPE_CONNECTION,$param,$username,$password,$charset = 'utf8') {
		 
		$this->query_arr = array($param,$username,$password);
		$this->charset = $charset;
		 
		if($mod==self::TYPE_WRITE_CONNECTION){
			$this->init_main();
		}else{
			$this->init_query();
		}
	}

	/**
	 * init write connection
	 */
	private function init_main() {
		if(! isset($this->main_conn)) {
			$this->init(1);
		}
	}
	/**
	 * init query connection
	 */
	private function init_query() {
		if(! isset($this->query_conn)) {
			$this->init(2);
		}
	}

	/**
	 * init connection
	 * $type connection type 1 main connection 2 query connection
	 */
	private function init($type) {
		if($type === 2) {
			//Random Query Server
			if(count($this->query_arr)%3 == 0 && count($this->query_arr) >=3) {
				$conn_count = count($this->query_arr)/3;
				$used_conn = rand(0,$conn_count-1);
				$url = $this->query_arr[$used_conn*3];
				$user = $this->query_arr[$used_conn*3 + 1];
				$passwd = $this->query_arr[$used_conn*3 + 2];
				//create connection
				$this->query_conn = $this->get_connection($url,$user,$passwd);
			} else {
				log("db connection set is error!");
			}
		} else {
			$this->main_conn = $this->get_connection($this->query_arr[0],$this->query_arr[1],$this->query_arr[2]);
		}
	}

	/**
	 *	create connection
	 *	$url connect to db url
	 *	$user connect to db user
	 *	$passwd connect to db password
	 */
	/*
	 private function get_connection($url,$user,$passwd) {
	 return new PDO($url, $user, $passwd,
	 array(
	 PDO::ATTR_PERSISTENT => true,
	 PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
	 ));
	 }
	 */





	private function get_connection($url,$user,$passwd) {
		return new PDO($url, $user, $passwd,
		array(
		PDO::ATTR_PERSISTENT => true,
		PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
		// by zixia 2006-05-31
		// disable persistent connection, so we can use multi query_stmt as usual.
		// and for maxmum database performance
		PDO::ATTR_PERSISTENT => false,
		PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES '".$this->charset."'"
		));
	}
	public function prepare($sql) {
		$this->init_main();
		$this->main_stmt = $this->main_conn->prepare($sql);
		$i = 0;
		return $this;
	}

	public function query_prepare($sql) {
		$this->init_query();
		$this->query_stmt = $this->query_conn->prepare($sql);
		return $this;
	}

	public function bind($param,$var) {
		$this->main_stmt->bindParam($param,$var);
		return $this;
	}

	public function query_bind($param,$var) {
		$this->query_stmt->bindParam($param,$var);
		return $this;
	}

	public function bindlob($param,&$var) {
		$this->main_stmt->bindParam($param,$var,PDO::PARAM_LOB);
		return $this;
	}

	public function query_bindlob($param,&$var) {
		$this->query_stmt->bindParam($param,$var,PDO::PARAM_LOB);
	}

	public function execute() {
		$this->main_stmt->execute();
		return $this;
	}

	public function query_execute() {
		$this->query_stmt->execute();
		return $this;
	}


	public function next() {
		$this->main_row->$this->main_stmt->fetch();
		return $this->main_row;
	}

	public function query_next() {
		$this->query_row = $this->query_stmt->fetch();
		return $this->query_row;
	}

	public function getall() {
		return $this->main_stmt->fetchAll();
	}

	public function query_getall( $type=PDO::FETCH_ASSOC ) {
		return $this->query_stmt->fetchAll($type);
	}

	public function getsingle($sql) {
		$this->init_query();
		$this->query_prepare($sql);
		$this->query_execute();
		if($row = $this->query_stmt->fetch()) {
			$this->query_stmt = null;
			return $row;
		} else {
			return null;
		}
	}

	public function query_getsingle( $type=PDO::FETCH_ASSOC ) {
		$row = $this->query_stmt->fetch($type);
		$this->query_stmt = null;
		return $row;
	}

	public function begintrans() {
		$this->main_conn->beginTransaction();
		return $this;
	}

	public function commit(){
		$this->main_conn->commit();
		return $this;
	}

	public function rollback(){
		$this->main_conn->rollBack();
		return $this;
	}

	public function stmt_close() {
		$this->main_stmt = null;
		return $this;
	}

	public function query_stmt_close() {
		$this->query_stmt = null;
		return $this;
	}

	public function close() {
		$this->main_conn = null;
	}

	public function query_close() {
		$this->query_conn = null;
	}

	public function lastInsertID() {
		return $this->main_conn->lastInsertId();
	}

	public function getrowcount() {
		return $this->main_stmt->rowCount();
	}
	public function getrowcount_query() {
		return $this->query_stmt->rowCount();
	}

	public function get($var) {
		return $this->main_row[$var];
	}

	public function query_get($var) {
		return $this->query_row[$var];
	}

	private function log($info) {
		NovaDebug::write($info);
	}
}
<?php

$lang['required']			= "%s不能为空。";
$lang['isset']				= "%s不能为空。";
$lang['valid_email']		= "请填写正确的%s.";
$lang['valid_emails']		= "请填写正确的%s.";
$lang['valid_url']			= "请填写正确的%s格式。";
$lang['valid_ip']			= "请填写正确的%s格式。";
$lang['min_length']			= "%s不能小于%s个字符。";
$lang['max_length']			= "%s不能大于%s个字符。";
$lang['exact_length']		= "%s的长度必须是%s位.";
$lang['alpha']				= "%s只能包含字母";
$lang['alpha_numeric']		= "%s只能包括数字和字母字符";
$lang['alpha_dash']			= "%s只能包括数字、字母、下划线、破折号等";
$lang['numeric']			= "%s必须是数字。";
$lang['is_numeric']			= "%s必须是数字。";
$lang['integer']			= "%s必须是整数。";
$lang['regex_match']		= "%s格式不正确";
$lang['matches']			= "%s与%s不一致。";
$lang['is_unique'] 			= "The %s field must contain a unique value.";
$lang['is_natural']			= "%s必须是整数类型";
$lang['is_natural_no_zero']	= "%s必须是大于0的整数";
$lang['decimal']			= "%s必须是一个十进制数字";
$lang['less_than']			= "%s必须小于%s。";
$lang['greater_than']		= "%s必须大于%s。";


/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */
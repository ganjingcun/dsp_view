<?php

$lang['upload_userfile_not_set'] = "不能找到用户上传的图片参数.";
$lang['upload_file_exceeds_limit'] = "上传文件大小超过了系统环境配置允许的最大值.";
$lang['upload_file_exceeds_form_limit'] = "上传文件大小超过了表单提交允许的最大值";
$lang['upload_file_partial'] = "只有部分上传的文件.";
$lang['upload_no_temp_directory'] = "临时文件夹不见了.";
$lang['upload_unable_to_write_file'] = "文件无法写入磁盘.";
$lang['upload_stopped_by_extension'] = "文件上传因为文件扩展名不被允许.";
$lang['upload_no_file_selected'] = "没有选择要上传的文件.";
$lang['upload_invalid_filetype'] = "你正试图上传的文件类型是不允许的.";
$lang['upload_invalid_filesize'] = "您正试图上传的文件是大于允许的最大值.";
$lang['upload_invalid_dimensions'] = "你正试图上传图片超过了允许的最大高度或宽度.";
$lang['upload_destination_error'] = "遇到问题而试图将上传的文件移动到最终目的地时.";
$lang['upload_no_filepath'] = "上传文件夹不存在.";
$lang['upload_no_file_types'] = "你没有指定任何允许的文件类型.";
$lang['upload_bad_filename'] = "你提交的文件名已经存在在服务器上.";
$lang['upload_not_writable'] = "上传目标文件夹没有读写权限";


/* End of file upload_lang.php */
/* Location: ./system/language/english/upload_lang.php */
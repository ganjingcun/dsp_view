<?php
/*
 使用PDO方法调用存储过程并且接收结果集
 1.数据库名称：ads_report
 2.存储过程功能说明：相当于SQL查询代理，如果要查找的表名在系统中存在分组统计表则直接获取数据，否则去原表中查询
 3.存储过程名称：
   proc_search_data(
       IN tableName VARCHAR(50),    #被查询的数据表名称
	   IN columnName VARCHAR(100),  #取数据表字段名称，例如：click, sum，不加“SELECT”关键字
	   IN criteria VARCHAR(1000),   #查询条件字段名称，例如：pkDay>="2012-3-31"，不加“WHERE”关键字
	   IN groupName VARCHAR(50),    #分组字段名称，例如：pkDay，不加“GROUP BY”关键字
	   IN orderName VARCHAR(50)     #排序字段名称，例如：pkDay DESC，不加“ORDER BY”关键字
   )
 4.使用方法：
   $db_config['host'] = '';
   $db_config['dbname'] = '';
   $db_config['user'] = '';
   $db_config['password'] = '';
      
   $test = new SingtonSearch($db_config);
   $test->setColumnName('pkDay,useUser,newUser');
   $test->setGroupName('user');
   $test->setOrderName('pkDay DESC');
   $test->setCriteria('1=1');
   $sth = $test->callProc();
   $col = $sth->columnCount();
   echo $col."<br>";
   while($row = $sth->fetch()){
       for($i=0; $i<$col; $i++)
       print $row[$i]." ";
       print "<br>";
   }
*/
class SingtonSearch {
	private $db;
	private $tableName;
	private $columnName;
	private $groupName;
	private $orderName; 
	private $criteria;
	private $tableMap = array(
		"t_day_user" => array('newUser')
	);
	/**
	 * 构造函数
	 * @param array $db_config 数据库配置数组 
	 */
	public function __construct($db_config) {
		$dsn = 'mysql:dbname='.$db_config['dbname'].';host='.$db_config['host'];
		$user = $db_config['user'];
		$password = $db_config['password'];
		try {
			$this->db = new PDO($dsn, $user, $password);
		} catch (PDOException $e) {
			exit('Connection failed: '.$e->getMessage());
		}
	}
	
	/**
	 * @param string $columnName 列名
	 */
	public function setColumnName($columnName) {
		$this->columnName = $columnName;
	}

	/**
	 * @param string $groupName 分组名
	 */
	public function setGroupName($groupName) {
		$this->groupName = $groupName;
	}

	/**
	 * @param string $orderName 排序名
	 */
	public function setOrderName($orderName) {
		$this->orderName = $orderName;
	}

	/**
	 * @param string $criteria 查询条件
	 */
	public function setCriteria($criteria) {
		$this->criteria = $criteria;
	}
	
	/**
	 * @param string $tableName 表名
	 */
	public function setTableName($tableName) {
		$this->tableName = $tableName;
	}
	
	/**
	 * 调用存储过程
	 * @return PDOStatement
	 */
	public function callProc() {
		$this->groupName = str_replace(' ', '', $this->groupName);
		$result = array_search($this->groupName, $this->tableMap);
		if ($result !== false) {
			$this->tableName = $result;
		} else {
			$this->tableName = 't_day_user';
		}
		$sth  = $this->db->prepare('call proc_search_data(?, ?, ?, ?, ?)');
		$sth->bindParam(1, $this->tableName);
		$sth->bindParam(2, $this->columnName);
		$sth->bindParam(3, $this->criteria);
		$sth->bindParam(4, $this->groupName);
		$sth->bindParam(5, $this->orderName);
		$sth->execute();
		return $sth;
	}
}
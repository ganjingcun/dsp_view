 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style type="text/css">
body { font-size:12px; padding:0; margin:0; }
a { text-decoration:none; color:#1b72af; }
a:hover { text-decoration:underline; }
ol { list-style-type:decimal; margin-left:24px; color:#888; }
.fl { float:left; }
.fr { float:right; }
.tar { text-align:right; }
.mt10 { margin-top:10px; }
.mr20 { margin-right:20px; }
.mb20 { margin-bottom:20px; }
.lh2 { line-height:2; }
.vt { vertical-align:top; }
.tip-page { width:600px; margin:100px auto 30px; }
.tip-table { margin:0 1px; background:#ffffff; width:598px; }
.tip-top { background:url(public/admin/images/tip-top.png) center no-repeat; height:50px; }
.tip-bgA, .tip-bgB, .tip-bgC, .tip-bgC { background:#1b72af; }
.tip-bgA, .tip-bgB { height:1px; overflow:hidden; }
.tip-bgA { margin:0 2px; }
.tip-bgB { margin:0 1px; }
.tip-bgC { padding-top:1px; }
.tip-content { padding:0 0 0 67px; }
.tip-content li{margin-top:9px;}
.tip-content tr td { padding:5px 10px 5px 0; line-height:25px; }
</style>  
</head>
<body>
<script> 
function redirect(url) {
      window.location.replace(url);
}
</script>
<div class="tip-page">
  <div class="tip-bgA"></div>
  <div class="tip-bgB"></div>
  <div class="tip-bgC">
    <div class="tip-top"></div>
    <table cellpadding="0" cellspacing="0" class="tip-table">
      <tr>
        <td height="170">
			<div class="tip-content">
				<?php echo $message?>
				<li class="li_nostyle">
					<span id="stime" style="color:#cc0000"></span>
					<a href="<?php echo $redirectto; ?>">秒后自动跳转到相关页面</a>
				</li>
           </div>
          <div class="tar cc mb20"> <a href="javascript:history.back();" class="mr20" tabindex="20">返回继续操作</a><a href="/admin/admin" target='_top' class="mr20" tabindex="20" id="showindex">返回首页</a> </div></td>
      </tr>
    </table>
  </div>
  <div class="tip-bgB"></div>
</div>
<script language="javascript">    
var i='<?php echo $time?>';      
function myclock(){     
        document.getElementById("stime").innerHTML = i;    
        if(i>0){   
                setTimeout("myclock()",1000);    
        }else{      
                window.location='<?php echo $redirectto?>';   
        } 
        i--;  
}   
myclock(); 
</script>
</body>
</html>
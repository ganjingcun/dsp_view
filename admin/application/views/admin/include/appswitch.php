<link href="{base_url()}public/admin/css/admin.css" rel="stylesheet"/>
<style>
 .on_off {
	float: left;background: url(/public/admin/images/on_off.png) no-repeat;	background: url(/public/admin/images/on_off.gif) 9;	width: 52px;height: 20px;border-radius: 9px;-moz-transition: all 0.1s ease-in;-webkit-transition: all 0.1s ease-in;	-o-transition: all 0.1s ease-in;
	transition: all 0.1s ease-in;
}
.on_off.on {
	background-position: 0 0;
}
:root .on_off.off {
	background-position: -32px 0;
}
 .on_off_new {
	float: left;background: url(/public/admin/images/on_off.png) no-repeat;	background: url(/public/admin/images/on_off.gif) 9;	width: 52px;height: 20px;border-radius: 9px;-moz-transition: all 0.1s ease-in;-webkit-transition: all 0.1s ease-in;	-o-transition: all 0.1s ease-in;
	transition: all 0.1s ease-in;
}
.on_off_new.on {
	background-position: 0 0;
}
:root .on_off_new.off {
	background-position: -32px 0;
}
.icon_warning{
width:35px;
}
</style>

<table class='table table-bordered table-hover table-striped'>
    <tr>
        <td width="10%" style="text-align: right"><b>前台开关状态：</b></td>
        <td><a href="javascript:void(0);"><div name=""  class="on_off_new {if $info['switch'] == 1}on{else}off{/if}"></div></a></td>
    </tr>
    <tr>
        <td width="10%" style="text-align: right"><b>后台开关控制：</b></td>
        <td>
            <a href="javascript:void(0);">
                <div name="adminswitch" data-type="adminswitch"  data-log="{$info['appid']}" class="on_off {if $info['adminswitch'] == 1}on{else}off{/if}"></div>
            </a>
        </td>
    </tr>
    <tr>
        <td width="10%" style="text-align: right;"><b>应用横竖屏控制：</b></td>
        <td>
            <div class="pull-left form-inline"><label class="radio inline help-inline"><input type="radio" class="appori" name="appori" value="0" {if $info['appori'] == 0}checked{/if} />自适应</label>&nbsp;&nbsp;<label class="radio inline help-inline"><input type="radio" class="appori" name="appori" value="1"  {if $info['appori'] == 1}checked{/if} />竖屏</label>&nbsp;&nbsp;<label class="radio inline help-inline"><input type="radio" class="appori" name="appori" value="2"  {if $info['appori'] == 2}checked{/if} />横屏</label></div><div class="left-oriented alert alert-danger apporimsg pull-left" style="display:none;margin-bottom:0;margin-left:20px;"></div>
        </td>
    </tr>
    <tr>
        <td width="10%" style="text-align: right"><b>强制填充的开关：</b></td>
        <td>
            <a href="javascript:void(0);">
                <div name="forcepaddingswitch" data-type="forcepaddingswitch"  data-log="{$info['appid']}" class="on_off {if $info['forcepaddingswitch'] == 1}on{else}off{/if}"></div>
            </a>
        </td>
    </tr>
    <tr>
        <td width="10%" style="text-align: right"><b>插屏广告分辨率约束：</b></td>
        <td>
            <a href="javascript:void(0);">
                <div name="lowqualityswitch" data-type="lowqualityswitch"  data-log="{$info['appid']}" class="on_off {if $info['lowqualityswitch'] == 1}on{else}off{/if}"></div>
            </a>
        </td>
    </tr>
    <tr>
        <td width="10%" style="text-align: right"><b>系统类型过滤：</b></td>
        <td>
            <a href="javascript:void(0);">
                <div name="ostypeswitch" data-type="ostypeswitch"  data-log="{$info['appid']}" class="on_off {if $info['ostypeswitch'] == 1}on{else}off{/if}"></div>
            </a>
        </td>
    </tr>
    <tr>
        <td width="10%" style="text-align: right;"><b>支持广告分类：</b></td>
        <td>
            <div class="pull-left form-inline">
            	<label class="radio inline help-inline">
            		<input type="radio" class="supportadtypeid" name="supportadtypeid" value="0" {if $info['supportadtypeid'] == 0}checked{/if} />全部
            	</label>&nbsp;&nbsp;
            	<label class="radio inline help-inline">
            		<input type="radio" class="supportadtypeid" name="supportadtypeid" value="27"  {if $info['supportadtypeid'] == 27}checked{/if} />游戏
            	</label>&nbsp;&nbsp;
            	<label class="radio inline help-inline">
            		<input type="radio" class="supportadtypeid" name="supportadtypeid" value="1"  {if $info['supportadtypeid'] == 1}checked{/if} />应用
            	</label>
            	&nbsp;&nbsp;&nbsp;&nbsp;
            	<div class='inline help-inline'><i class="icon icon_warning inline help-inline"></i><b class='inline help-inline'>仅限平台广告(不包括DSP广告）</b></div>
            	 
            </div>
            <div class="left-oriented alert alert-danger supportadtypeidmsg pull-left" style="display:none;margin-bottom:0;margin-left:20px;"></div>
        </td>
    </tr>
</table>
<script>
	
	$(function(){

		$(".on_off").click(function(){
			var url = $(this).attr("href");
			var appid = "{$info['appid']}";
			var switchtype = $(this).attr('data-type');
			var onandoff = $(this).hasClass("off") ? 1 : 0;
			var obj = this;
			$.get("/admin/app/changestatus", { appid:appid,'switch':onandoff,switchtype:switchtype}, function(data){
				if(data && data['status'] == 1){
					if(onandoff){
					    $(obj).removeClass("off");
					    $(obj).addClass("on");
					}else{
					    $(obj).removeClass("on");
					    $(obj).addClass("off");
					}
				}else{
					if(data && data['info']){
						alert(data['info']);
					}else{
                        alert(data['info']);
						alert('开关修改失败');
					}
				}
			}, 'json');
			return false;
		});
		$(".appori").change(function(){
			var appori = $(this).val();
			$.ajax({
				url:'/admin/app/saveappori',
				type:"post",
				data:{ appid:"{$info['appid']}", appori:appori},
				success:function(data){
					if(!data || !data['status'] || data['status'] !=1){
						$(".apporimsg").removeClass("alert-success");
						$(".apporimsg").addClass("alert-danger");
					}else{
						$(".apporimsg").removeClass("alert-danger");
						$(".apporimsg").addClass("alert-success");
					}
					$(".apporimsg").html(data['info']);
					$(".apporimsg").show();
					setTimeout(function(){ $(".apporimsg").hide();}, 3000);
				},
				dataType:"json",
				error:function(){
					alert("网络异常或服务器响应错误，应用屏幕方向设置失败，请手动刷新页面并重试");
					return false;
				}
			});
		});

		$(".supportadtypeid").change(function(){
			var supportadtypeid = $(this).val();
			$.ajax({
				url:'/admin/app/savesupportadtypeid',
				type:"post",
				data:{ appid:"{$info['appid']}", supportadtypeid:supportadtypeid},
				success:function(data){
					if(!data || !data['status'] || data['status'] !=1){
						$(".supportadtypeidmsg").removeClass("alert-success");
						$(".supportadtypeidmsg").addClass("alert-danger");
					}else{
						$(".supportadtypeidmsg").removeClass("alert-danger");
						$(".supportadtypeidmsg").addClass("alert-success");
					}
					$(".supportadtypeidmsg").html(data['info']);
					$(".supportadtypeidmsg").show();
					setTimeout(function(){ $(".supportadtypeidmsg").hide();}, 2000);
				},
				dataType:"json",
				error:function(){
					alert("网络异常或服务器响应错误，支持广告分类设置失败，请手动刷新页面并重试");
					return false;
				}
			});
		});
	});
</script>

<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/**
	 * 接口返回值
	 * Enter description here ...
	 * @param array $data
	 * @param String $status
	 */
	function returnStatus($data, $status = 'fail'){
		echo json_encode(array('status'=>$status, 'data'=>$data));
		exit();
	}
	
	/**
	 * 
	 * get返回值 web
	 * @param 
	 * @param array $data
	 * @param String $status
	 */
	function returnCallbackStatus($callback,$data, $status = 'fail'){
		echo $callback.'('.json_encode(array('status'=>$status, 'data'=>$data)).')';
		exit();
	}
	
	/**
	 * 
	 * 处理错误信息
	 * @param String $line
	 * @param Array $data
	 */
	function langy($line, $data = array()){
		return array(
				'error'=>$line,
				'error_no'=>lang($line)
		);	
	}
	
	/**
	 * 
	 * 解析json数组
	 * @param String $json_data
	 * @return Array
	 */
	function parseJsonData($json_data){
//			if(ini_get("magic_quotes_gpc")=="1"){
//				$data = (array)json_decode(stripslashes($json_data));
//			}else{
				$data = (array)json_decode($json_data);
//			}	
			return $data;
	}
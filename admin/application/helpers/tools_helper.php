<?php
/**
 * 时间生成函数
 * @param $start
 * @param $end
 * @param $needStart 返回的日期是否包括 $start
 * @param $needEnd 返回的日期是否包括 $end
 * @return array
 */
function mktimes($start,$end,$needStart = false,$needEnd = false)
{
    $day = (strtotime($end)-strtotime($start))/86400;
    $y = date('Y',strtotime($start));
    $m = date('m',strtotime($start));
    $d = date('d',strtotime($start));

    $startIndex = $needStart?0:1;
    $day = $needEnd?$day+1:$day;

    $t = array();
    for($i=$startIndex;$i<$day;$i++)
    {
        $t[] = date('Y-m-d', mktime(0,0,0,$m,$d+$i,$y));
    }
    return $t;
}
/**
 * 循环创建目录
 * @param $path 路径
 * @return null
 */
function createDir($path){
    if(!is_readable($path)){
        createDir( dirname($path) );
        if(!is_file($path)) mkdir($path,0777);
    }
}

/**
 * 身份类型
 */
function getAccountType($type)
{
    $data = '';
    switch ($type)
    {
        case 1:
            $data = '个人/团体';
            break;
        case 2:
            $data = '公司';
            break;
        case 3:
            $data = '个体工商';
            break;
    }
    return $data;
}
/**
 * 状态
 */
function getAccountTag($type)
{
    $data = '';
    switch ($type)
    {
        case 1:
            $data = '<span class="buff gra">注册</span>';
            break;
        case 2:
            $data = '<span class="buff gre">升级</span>';
            break;
        case 3:
            $data = '<span class="buff red">取消</span>';
            break;
        case 4:
            $data = '<span class="buff gre">降级</span>';
            break;
    }
    return $data;
}
/**
 * 状态
 */
function getAccountAuditStatus($type)
{
    $data = '';
    switch ($type)
    {
        case 0:
            $data = '<span class="buff gra">待审核</span>';
            break;
        case 1:
            $data = '<span class="buff gre">审核通过</span>';
            break;
        case 2:
            $data = '<span class="buff red">驳回</span>';
            break;
    }
    return $data;
}
/**
 * 证件类型
 */
function getCertType($type)
{
    $data = '';
    switch ($type)
    {
        case 1:
            $data = '中国大陆身份证';
            break;
        case 2:
            $data = '中国港澳台身份证';
            break;
        case 3:
            $data = '海外证件';
            break;
    }
    return $data;
}

function mkhours()
{
    $hour = array();
    for($ii=0;$ii<24;$ii++)
    {
        if($ii<10)
        $hour[] = "0$ii";
        else
        $hour[] = $ii;
    }
    return $hour;
}

/**
 * 自动生成时间数据,该函数只需要一个开始时间与一个结束时间  中间如果还含有数据的日期不会覆盖，并倒序排列数组
 * 注：函数会自动判断的日期最大值与最小值，处于中间日期的数据不做处理，在函数返回时会自动加上数据
 * @param $data
 * @param $pkcol  为日期字段，排序也一样
 * @param $datafilter  此处为数组，需要数据变为'--‘的
 * @param $is_hour  是否强制按时间  如果开始时间与结束时间不一样，则不起作用
 * @return array
 */
function getData($data,$pkcol='pkday',$datafilter=array(),$sDate="",$eDate="",$is_hour=false)
{
    global $pkcols;
    $pkcols = $pkcol;

    if(empty($data)) return array();
    $start = '';
    $end   = '';
    $keys  = array();
    foreach($data as &$value)
    {
        $start   = min($start?$start:$value[$pkcol],$value[$pkcol]);
        $end     = max($end?$end:$value[$pkcol],$value[$pkcol]);
        $keys    = array_keys($value);
        $vDate[] = $value[$pkcol];
        if($is_hour && $sDate==$eDate)
        {
            $value[$pkcol] = ($value[$pkcol]<10?"0".(int)$value[$pkcol]:$value[$pkcol]).":00 - ".(($value[$pkcol]+1)==24?"00:00":((($value[$pkcol]+1)<10?"0".($value[$pkcol]+1):($value[$pkcol]+1)).":00"));
            $value['pkday_str'] = (int)$value['pkday_str']<10?"0".(int)$value['pkday_str']:$value['pkday_str'];
        }
    }

    if($is_hour && $sDate==$eDate)
    {
        $date = mkhours();
    }
    else
    $date = mktimes($sDate?$sDate:$start,$eDate?$eDate:$end,true,true);

    if($date)
    foreach($date as $value2)
    {
        $vflag = true;
        $data2 = array();
        if($is_hour && $sDate==$eDate)
        {
            $vflag = in_array(trim($value2),$vDate);
            $data2[$pkcol] = $value2.":00 - ".(($value2+1)==24?"00:00":((($value2+1)<10?"0".($value2+1):($value2+1)).":00"));
            $data2[$pkcol.'_str'] = (int)$value2<10?"0".(int)$value2:$value2;
        }
        else
        {
            $vflag = in_array(trim($value2),$vDate);
            $data2[$pkcol] = $value2;
        }
        if(!$vflag)
        {
            foreach($keys as $values3)
            {
                if($values3!=$pkcol)
                {
                    if($values3 == $pkcol.'_str')
                    {
                        if(!($is_hour && $sDate==$eDate))
                        $data2[$pkcol.'_str'] = substr($value2,5,10);
                    }
                    else if(!in_array($values3,$datafilter))
                    $data2[$values3] = 0;
                    else
                    $data2[$values3] = -1;
                }

            }
            array_push($data,$data2);
        }
    }

    usort($data,"pkdaypaixu");
    return $data;
}

function pkdaypaixu($a,$b)
{
    global $pkcols;
    if($a[$pkcols]==$b[$pkcols]) return 0;
    return $a[$pkcols]<$b[$pkcols]?1:-1;
}


function authcode($string, $operation = 'DECODE', $key = '', $expiry = 0) {
    $ckey_length = 4;

    $key = md5($key ? $key : "punchbox.org");
    $keya = md5(substr($key, 0, 16));
    $keyb = md5(substr($key, 16, 16));
    $keyc = $ckey_length ? ($operation == 'DECODE' ? substr($string, 0, $ckey_length): substr(md5(microtime()), -$ckey_length)) : '';

    $cryptkey = $keya.md5($keya.$keyc);
    $key_length = strlen($cryptkey);

    $string = $operation == 'DECODE' ? base64_decode(substr($string, $ckey_length)) : sprintf('%010d', $expiry ? $expiry + time() : 0).substr(md5($string.$keyb), 0, 16).$string;
    $string_length = strlen($string);

    $result = '';
    $box = range(0, 255);

    $rndkey = array();
    for($i = 0; $i <= 255; $i++) {
        $rndkey[$i] = ord($cryptkey[$i % $key_length]);
    }

    for($j = $i = 0; $i < 256; $i++) {
        $j = ($j + $box[$i] + $rndkey[$i]) % 256;
        $tmp = $box[$i];
        $box[$i] = $box[$j];
        $box[$j] = $tmp;
    }

    for($a = $j = $i = 0; $i < $string_length; $i++) {
        $a = ($a + 1) % 256;
        $j = ($j + $box[$a]) % 256;
        $tmp = $box[$a];
        $box[$a] = $box[$j];
        $box[$j] = $tmp;
        $result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));
    }

    if($operation == 'DECODE') {
        if((substr($result, 0, 10) == 0 || substr($result, 0, 10) - time() > 0) && substr($result, 10, 16) == substr(md5(substr($result, 26).$keyb), 0, 16)) {
            return substr($result, 26);
        } else {
            return '';
        }
    } else {
        return $keyc.str_replace('=', '', base64_encode($result));
    }

}

/**
 * 随机字符串
 */
function randStr($len)
{
    $chars='0123456789abcdefghijklmnopqrstuvwxyz'; // characters to build the password from
    $string="";
    for($i=$len;$i>0;$i--)
    {
        $position=rand()%strlen($chars);
        $string.=substr($chars,$position,1);
    }
    return $string;
}
/**
 * 供ajax使用的采集过程。
 */
function Acquisition($appid) {
    $storeurl = "http://moregame.punchbox.ads/index.php?m=Gather&a=getAppinfo&appid=";
    $info = file_get_contents($storeurl . $appid);
    $status = json_decode($info, true);
    if (!(!empty($status) && is_array($status) && $status['success'] == true)) {
        if(isset($_SERVER['HTTP_NJH_AGENT'])){
            print_r($status);
        }
        $return = array('isSucceed'=>false,'error'=>array('抱歉，您填写的地址采集不到信息，请检查并重新填写。'),'data'=>array());
    }
    $return = array('isSucceed'=>true,'error'=>array(''),'data'=>'');
    return $return;
}
/**
 * 解析传入的URL中的appid
 */
function getAppidFromUrl($url){
    $url = parse_url($url);
    if(empty($url['path'])){
        return false;
    }
    $query = $url['path'];
    $url = explode('/', $query);
    $url = array_pop($url);
    if (strpos($url, 'id') !== false) {
        $appid = str_replace("id", "", $url);
    }else{
        return false;
    }
    return $appid;
}
function get_client_ip(){
    if (getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"), "unknown"))
    $ip = getenv("HTTP_CLIENT_IP");
    else if (getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown"))
    $ip = getenv("HTTP_X_FORWARDED_FOR");
    else if (getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown"))
    $ip = getenv("REMOTE_ADDR");
    else if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown"))
    $ip = $_SERVER['REMOTE_ADDR'];
    else
    $ip = "unknown";
    return($ip);
}



function getIpLong(){
    $ip = get_client_ip();
    if ($ip != 'unknown'){
        return sprintf("%.u",ip2long($ip));
    }
    return 0;
}

/**
 * 去除引号
 */
function strip_str($str)
{
    return preg_replace("/\\'/", "", $str);
}


/**
 * 获取url返回值，post方法
 *
 * @param mixed $url 请求地址
 * @param int $timeout 超时时间
 * @param array $header HTTP头信息
 * @access public
 * @return void
 */
function curlPost($c_url, $c_url_data, $method='POST', $is_json=false) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $c_url);
    curl_setopt($ch, CURLOPT_USERAGENT,"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method); //设置请求方式
    if ($is_json)
    {
        $data_string = json_encode($c_url_data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    }
    else
    {
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($c_url_data));
    }
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($ch);
    
    $errno = curl_errno( $ch );
    $info  = curl_getinfo( $ch );
    $info['errno'] = $errno;
    error_log('info: '.json_encode($info));
    error_log('curl_multi_getcontent: '.curl_multi_getcontent($ch));
    curl_setopt($ch, CURLOPT_POST, 0); //当前面设置set ($ch, curlopt_post, 1)时，用curl_exec  post数据后，需要重新设置CURLOPT_POST为0，否则，会有500或者403错误。
    
    curl_close($ch);
    unset($ch);
    return $result;
}
/**
 * 推送消息
 * @author ronghua
 * @param String url
 * @param array post
 * @return
 */
function curl_push($url,$post)
{
    $ps_data['json_data'] = json_encode($post);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $ps_data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    error_log('curl_multi_getcontent:'.curl_multi_getcontent($ch));
    curl_close($ch);
    return $result;
}
/**
 * curl推送数据接口
 * @param string $data
 * @param string $url
 * @return
 */
function curlSend($data,$url)
{

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);//启用POST提交
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data); //设置POST提交的字符串
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    $ret = curl_exec($ch);
    error_log('curl_multi_getcontent:'.curl_multi_getcontent($ch));
    curl_close($ch);
    return $ret;
}

/**
 * curl推送数据接口
 * @param string $data
 * @param string $url
 * @return
 */
function curlSendJson($data,$url)
{

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);//启用POST提交
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data); //设置POST提交的字符串
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	    'Content-Type: application/json',
	    'Content-Length: ' . strlen($data))
	);
    $ret = curl_exec($ch);
     error_log('curl_multi_getcontent:'.curl_multi_getcontent($ch));
    curl_close($ch);
    return $ret;
}

/**
 * 把data2的数据换到data1上
 * @param array $data1 array(1,2,4,5)
 * @param array $data2 array(1,3,4,5)
 */
function changeArray($data1,$data2)
{
    if(empty($data1) || empty($data2))
    return false;
    return array_replacer($data1,$data2);
}

/**
 *
 * 替换函数   详细用法参照 array_replace
 * @param array $array1
 * @param array $array2
 */
function array_replacer($array1,$array2)
{
    if(empty($array1))
    return false;
    foreach ($array1 as $k1=>&$v1)
    {
        if(array_key_exists($k1, $array2))
        {
            $v1 = $array2[$k1];
        }
    }
    return $array1;
}

/**
 * 排序
 * @params $data1  array(1,3,4,)
 * @param  $data2  array(array("xx"=>"xx"));
 * @param  cols
 * @return
 */
function sortArray($data1,$data2,$cols='user_id')
{
    if(empty($data1) || empty($data2))
    return false;

    $data = array();
    foreach ($data1 as $v1)
    {
        foreach ($data2 as $v2)
        {
            if($v1==$v2[$cols])
            {
                $data[] = $v2;
            }
        }
    }
    return $data;
}
/**
 * 获取redis连接
 * @author jianhui
 */
function getRedis($rhost='redis.mg.punchbox.ads', $port=6379){
    static $_redis = null;
    if($_redis == null){
        $_redis = new Redis();
        if (!($_redis->connect($rhost ,$port))){
            return false;
        }
    }
    return $_redis;
}

/**
 * 获取redis连接
 * @author jianhui
 */
function getBannerRedis(){
    static $_redis = null;
    if($_redis == null){
        $_redis = new Redis();
        if (!($_redis->connect( 'redis.core.punchbox.ads',6379))){
            return false;
        }
    }
    return $_redis;
}

function ajaxReturn($info, $status, $data = array()){
    exit(json_encode(array('data'=>$data, 'info'=>$info, 'status'=>$status)));
}

/**
 * 数据源
 * @param $condition array
 */
function getDataApi($condition=array(),$is_array = true,$is_debug=false)
{
    $config = &get_config();
    $url = $config['data_api'].implode("&",$condition);
    if($is_debug)
    return $url;
    if($is_array){
        $tmp = @json_decode(@curlGet($url,1),true);
        if(!isset($tmp['data'])){
            return array('data'=>array());
        }else{
            return $tmp;
        }
    }else{
        return @curlGet($url,1);
    }
}


function sget($name){
    if(isset($_GET[$name])){
        if(!is_array($_GET[$name])){
            return trim(strip_tags($_GET[$name]));
        }
        foreach($_GET[$name] as $key=>$val){
            $_GET[$key] = sarray($val);
        }
    }
    return '';
}

function spost($name){
    if(isset($_POST[$name])){
        if(!is_array($_POST[$name])){
            return trim(strip_tags($_POST[$name]));
        }
        foreach($_POST[$name] as $key=>$val){
            $_POST[$key] = sarray($val);
        }
    }
    return '';
}

/**
 * 获取url返回值，curl方法
 *
 * @param mixed $url 请求地址
 * @param int $timeout 超时时间
 * @param array $header HTTP头信息
 * @access public
 * @return void
 */
function curlGet($url, $timeout = 3, $header = array())
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    curl_setopt($ch, CURLOPT_ENCODING ,'gzip');
    $ret = curl_exec($ch);
    curl_close($ch);
    return $ret;
}

/**
 * 获取数组中的一项。
 * @param type $arr
 * @param type $key
 * @return type
 */
function getArrayVal($arr, $key){
    return isset($arr[$key]) ? $arr[$key] : '';
}

function getOsType($ostypeid){
    $ostype = array('0'=>"Android",'1'=>"Android",'2'=>"iPhone",'3'=>"iPad",'4'=>"Universal");
    return isset($ostype[$ostypeid]) ? $ostype[$ostypeid] : '未知';
}

/*
 * 获取广告组投放状态 0:未完成 1：投放中 2：暂停 3 删除
 */
function getAdStatus($status){
    $arr = array(
    0=>'未完成',
    1=>'投放中',
    2=>'已暂停',
    3=>'删除',
    );
    return isset($arr[$status]) ? $arr[$status] : $arr[0];
}
/*
 * 获取广告活动投放状态
 */
function getAdCampaignStatus($status){
    $arr = array(
    0=>'投放中',
    2=>'投放中',
    3=>'已暂停',
    );
    return isset($arr[$status]) ? $arr[$status] : $arr[0];
}

/**
 *
 * Enter 获取广告投放类型
 * @param unknown_type $typeid
 */
function getTargetType($typeid){
    $arr = array(
    0=>'未知',
    1=>'App下载',
    2=>'WebSite打开',
    3=>'Cocosplay',
    4=>'视频广告',
    );
    return isset($arr[$typeid]) ? $arr[$typeid] : $arr[0];
}

function getAdOsType($ostypeid){
    $arr = array(
    0=>'不限',
    1=>'Android',
    2=>'iOS',
    );
    return isset($arr[$ostypeid]) ? $arr[$ostypeid] : $arr[0];
}

function getAdAmount($amount, $type=1){
    $unit = AMOUNT_RATIO;//单位大小
    if($type==1){
        return number_format($amount / $unit, 3, '.', '');
    }
    return $amount * $unit;
}
//1:Banner广告.2:弹出广告.3:精品推荐.4:广告墙
function getAdFrom($adfromid){
    $_arr = array(1=>"Banner广告",
    2=>'弹出广告',
    3=>'精品推荐',
    5=>'积分墙',
    20=>'信息流',
    40=>'视频广告',
    );
    return isset($_arr[$adfromid]) ? $_arr[$adfromid] : '未知类型';
}

function getValuationFrom($adfromid){
    $_arr = array(1=>"eCPM",
    2=>'eCPC',
    3=>'eCPA',
    );
    return isset($_arr[$adfromid]) ? $_arr[$adfromid] : '未知类型';
}

function getChargemode($chargemode){
    $arr = array(
    1=>'CPC',
    2=>'CPM',
    3=>'CPA',
    );
    return isset($arr[$chargemode]) ? $arr[$chargemode] : '未知状态';
}

function getStuffStatus($status){
    $arr = array(
    1=>'',
    );
    return isset($arr[$status]) ? $arr[$status] : '未知状态';
}

function getArrayByKey($arr, $key){
    $_tmp = array();
    foreach($arr as $val){
        if(isset($val[$key]))
        $_tmp[$val[$key]] = $val;
    }
    return $_tmp;
}

function getUserType($usertypeid){
    $_arr = array(
    1=>'广告主',
    2=>'开发者',
    3=>'自研开发者',
    );
    return isset($_arr[$usertypeid]) ? $_arr[$usertypeid] : '双重身份';
}

/**
 * 获取二维配置中的某一项。
 * Enter description here ...
 * @param unknown_type $config
 * @param unknown_type $key
 * @param unknown_type $val
 */
function getConfigVal($config, $key, $val){
    return isset($config[$key][$val]) ? $config[$key][$val] : '未知数据['.$val.']';
}

/**
 * 获取二维数据中某KEY下的一项值。
 * 实际过程为调用 ：getConfigVal(),可理解为别名函数 ...
 * @param unknown_type $arr
 * @param unknown_type $key
 * @param unknown_type $val
 */
function getArrayValByKey($arr, $key, $val){
    return getConfigVal($arr, $key, $val);
}

function getCpcType($type){
    $_arr = array(1=>"eCPC", 2=>"eCPM", 3=>"eCPA");
    return isset($_arr[$type]) ? $_arr[$type] : '未设置';
}
/**
 *
 * Enter description here ...
 * @param $price
 * @param $type
 */
function formatmoney($money, $type='get', $num = 2, $replace = '')
{
    if(!is_numeric($money))
    {
        return $money;
    }
    if($type == 'get')
    {
        if($replace == '')
        {
            $data = number_format($money / 1000000, $num);
        }
        else
        {
            $data = number_format($money / 1000000, $num, $replace,'');
        }
    }
    elseif($type == 'set')
    {
        $data = $money * 1000000;
    }
    return $data;
}
/**
 * 广告素材状态 状态,0:审核中，1，审核通过，2，审核驳回，3：广告删除
 */
function getadstuffstatus($status=0){
    $arr = array(
    0=>'审核中',
    1=>'审核通过',
    2=>'审核驳回',
    3=>'广告删除',
    );
    return isset($arr[$status]) ? $arr[$status] : $arr[0];
}
/**
 * 获取用户状态  用户状态(0待激活,1正常激活,2锁定,4删除)
 */
function getuserstatus($status=0){
    $arr = array(
    0=>'待激活',
    1=>'正常激活',
    2=>'锁定',
    3=>'删除',
    );
    return isset($arr[$status]) ? $arr[$status] : $arr[0];
}
/**
 * 获取网络类型定向 网络类型定向 1 Wi-Fi, 2 2g, 3 3g, 4 其他
 */
function getnettypeset($status=''){
    $arr = array(
    0=>'所有',
    1=>'Wi-Fi',
    2=>'2g',
    3=>'3g',
    4=>'其他',
    );
    return isset($arr[$status]) ? $arr[$status] : $arr[0];
}
/**
 * 模拟浏览器获取文件头信息
 */
function get_header($url){
    $ch  = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_NOBODY,true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
    //curl_setopt($ch, CURLOPT_FOLLOWLOCATION,true);
    curl_setopt($ch, CURLOPT_AUTOREFERER,true);
    curl_setopt($ch, CURLOPT_TIMEOUT,30);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Accept: */*',
    'User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)',
    'Connection: Keep-Alive'));
    $header = curl_exec($ch);
    //分解响应头部信息
    $tmp = explode("\r\n", $header);
    //print_r($tmp);
    //分割数组
    $header_assoc = array();
    foreach ($tmp as $v) {
        $kv = explode(': ', $v);
        if(count($kv) != 2)
        {
            if(strpos($v, '404'))
            {
                return false;
            }
            continue;
        }
        if($kv[0] == 'Set-Cookie'){
            $header_assoc['Set-Cookie'][] = $kv[1];
        }else{
            $header_assoc[$kv[0]] = trim($kv[1]);
        }
        if($kv[0] == 'Location')
        {
            $header_assoc = array_merge($header_assoc , get_header(trim($kv[1])));
            break;
        }
    }
    return $header_assoc;
}

/**
 * 链接摩羯的 redis 取appid 和 punchBoxid对应
 */
function getKtRedis(){
    static $_redis = null;
    if($_redis == null){
        $_redis = new Redis();
        if (!($_redis->connect( '10.10.12.163',6381))){
            return false;
        }
    }
    return $_redis;
}

/**
 * 返回数组去除0和重复的结果
 * @param array $arr
 * @return array 过滤后的值
 */
function array_uniq_filter_zero($arr=array()) {
    if(!is_array($arr) && $arr!=0) {
        return array($arr);
    }

    if(!empty($arr)) {
        return array_filter(array_unique($arr));
    } else {
        return array();
    }
}

/*
 //获取微博字符长度
 */
function WeiboLength($str)
{
    $arr = arr_split_zh($str);   //先将字符串分割到数组中
    $len=0;
    foreach ($arr as $v){
        $temp = ord($v);        //转换为ASCII码
        if ($temp > 0 && $temp < 127) {
            $len = $len+0.5;
        }else{
            $len ++;
        }
    }
    return ceil($len);        //加一取整
}

/*
 //拆分字符串函数,只支持 gb2312编码
 //参考：http://u-czh.iteye.com/blog/1565858
 */
function arr_split_zh($tempaddtext){
    $tempaddtext = @iconv("UTF-8", "GBK//IGNORE", $tempaddtext);
    $cind = 0;
    $arr_cont=array();

    for($i=0;$i<strlen($tempaddtext);$i++)
    {
        if(strlen(substr($tempaddtext,$cind,1)) > 0){
            if(ord(substr($tempaddtext,$cind,1)) < 0xA1 ){ //如果为英文则取1个字节
                array_push($arr_cont,substr($tempaddtext,$cind,1));
                $cind++;
            }else{
                array_push($arr_cont,substr($tempaddtext,$cind,2));
                $cind+=2;
            }
        }
    }

    foreach ($arr_cont as &$row)
    {
        $row=iconv("gb2312","UTF-8",$row);
    }

    return $arr_cont;
}

function deldir($dir)
{
    $dh = opendir($dir);
    while ($file = readdir($dh))
    {
        if ($file != "." && $file != "..")
        {
            $fullpath = $dir . "/" . $file;
            if (!is_dir($fullpath))
            {
                unlink($fullpath);
            } else
            {
                deldir($fullpath);
            }
        }
    }
    closedir($dh);
    if (rmdir($dir))
    {
        return true;
    }
    else
    {
        return false;
    }
}

//判断是否是json字符串
function is_json($string) {
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
}

/** 
 * 投放项目归属平台，1：平台，2：DSP，3：平台+DSP  
 */ 
function getadprojectplatform($platform=1){ 
    $arr = array( 
    0=>'未知', 
    1=>'平台', 
    2=>'DSP', 
    3=>'平台+DSP ', 
    ); 
    return isset($arr[$platform]) ? $arr[$platform] : $arr[0]; 
} 


/** 
 * 投放项目执行平台，1：Android，2：IOS，  
 */ 
function getadprojectostype($ostype=1){ 
    $arr = array( 
    1=>'Android', 
    2=>'IOS', 
    3=>'Android+IOS', 
    0=>'未知', 
    ); 
    return isset($arr[$ostype]) ? $arr[$ostype] : $arr[0]; 
} 

/** 
 * 投放项目归属平台，1：平台，2：DSP，3：平台+DSP  
 */ 
function getadprojectcomputation($computation=1){ 
    $arr = array( 
    0=>'未知', 
    1=>'收入', 
    2=>'成本', 
    3=>'收入+成本', 
    ); 
    return isset($arr[$computation]) ? $arr[$computation] : $arr[0]; 
}
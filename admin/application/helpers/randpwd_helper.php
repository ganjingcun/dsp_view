<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * 获取随机字符串做密码用
 */
if ( ! function_exists('generate_password'))
{
	
	/*
	 * 返回随机字符串
	 * @param $length 字符长度
	 * @param $type 区分大小写
	 */
	function generate_password( $length = 8 ,$type='0') {
		if($type==0){
			$chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
		}
		else
		{
			$chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		}
		#$chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_ []{}<>~`+=,.;:/?|';
		$password = '';
		for ( $i = 0; $i < $length; $i++ )
		{
			// 这里提供两种字符获取方式
			// 第一种是使用 substr 截取$chars中的任意一位字符；
			// 第二种是取字符数组 $chars 的任意元素
			// $password .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
			$password .= $chars[ mt_rand(0, strlen($chars) - 1) ];
		}
		return $password;
	}
}
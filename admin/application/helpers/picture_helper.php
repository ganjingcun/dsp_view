<?php
/**
	 * 
	 * 图片处理
	 * @param String $rpath 读取路径
	 * @param String $wpath 写路径
	 * @param array $row    存储像素
	 */
	function img($rpath,$wpath,array $row)
	{
		$img = new Imagick();
		
		$origin_size = getimagesize($rpath);
	
		if($row['autocalcWithWidth']) {
			$row['height'] = intval($origin_size[1]/$origin_size[0]*$row['width']); 	
		}
		
		$img->readImage($rpath);
		$img->setImageCompression(imagick::COMPRESSION_JPEG);
		$img->setImageCompressionQuality(100);
		
		$img->thumbnailImage($row['width'], $row['height']);
		$img->stripImage();
		$img->writeImage($wpath);
		$img->clear();
		$img->destroy();
		$row['path'] = $wpath;
		return $row;
	}
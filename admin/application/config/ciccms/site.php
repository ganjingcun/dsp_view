<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['site']=array(
		'web_title' 		=> 'CICCMS',
		'web_cache'			=> '0',
		'web_cache_time'	=> '10',
		'web_template'		=> 'default',
		'web_keywords'		=> 'CICCMS|CIC|CMS',
		'web_description'	=> 'CICCMS是专业的企业建站系统！',
		'web_beian'			=> '',
		'web_powerby'		=> 'CICCMS企业网站管理系统_企业网站制作更便利,企业网站建设和管理更方便',
		'web_state'			=> '',
		'web_search_hot'	=> 'CICCMS|CMS|CIC',
);
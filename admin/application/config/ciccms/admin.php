<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['admin']=array(
	'nav' => array(
				'广告管理' => array(
					array('广告主管理','admin/advertmanage/index'),
					//array('统计分析','admin/advertreport/index'),
				),
				'媒体管理' => array(
					array('开发者管理','admin/mediaapp/users/0'),
					array('App管理','admin/mediaapp/app/2'),
					//array('统计分析','admin/mediareport/index'),
				),
				'统计分析' => array(
					array('广告投放分析','admin/adsreport/index'),
					array('流量消耗分析','admin/advflowreport/index'),
				),
				'运营管理' => array(
						array('iOS采集列表','admin/app/index'),
						array('iOS专题管理','admin/subject/index'),
						array('iOS日常推送及设置','admin/app/normal'),
						array('iOS专题推送及设置','admin/app/theme'),	
						array('Android推送列表','admin/android/index'),
						array('Android推送设置','admin/android/normal'),
						array('换量点击监测管理','admin/appspreadmanage/index'),
						array('百度视频推广','admin/baiduapplist/index'),
						
				),
				'全局控制' => array(
						array('投放控制','admin/systemset/index'),
						array('自有客户','admin/systemset/users'),
						array('ECPC设置','admin/ecpc/index'),
				),
				'客服审核' => array(
						array('账号激活','admin/userinfo/index'),
						array('广告审核','admin/adgroupverify/index',array(array('定向设置','admin/advert/detail'))),
						array('应用审核(测试)', 'admin/appverify/test'),
						array('应用审核(技术)', 'admin/appverify/service'),
				),
				'财务管理' => array(
						array('账号充值','admin/financial/offlinerecharge'),
						array('财务结算','admin/financial/payment'),
						array('资质审核','admin/financial/showverify'),
						array('线下支付','admin/financial/withdraw'),
				),
				'CPS管理' => array(
						array('渠道管理','admin/netunion/nuchannelmanage'),
						array('升级管理','admin/netunion/updateappmanage'),
						array('项目管理及分成设置 ','admin/netunion/nupromanage'),
						array('包名对照关系维护 ','admin/netunion/apkpromanage'),
				),
				'A/B Test' => array(
							array('项目管理','admin/projectmanage/index'),
				),
				'管理员' => array(
							array('管理员管理','admin/user_admin/index'),
							array('添加管理员','admin/user_admin/content_add'),
							array('管理分组','admin/user_admin/group'),
							array('添加分组','admin/user_admin/group_add'),
							array('权限管理','admin/user_admin/group_pris'),
							array('操作日志','admin/record_log/index'),
				),
				'版本管理' => array(
						array('版本设置','admin/version/index'),
 				),
		),
		
		
		
		
		
	// 系统设置
	'sys' => array(
			'thumb_width'			=> '300',
			'thumb_height'			=> '200',
			'image_type'			=> 'png|jpg|gif|jpeg',
			'file_type'				=> 'zip|gz|rar|iso|doc|xsl|ppt|wps|swf|mpg|mp3|rm|rmvb|wmv|wma|wav|mid|mov',
			'member_register'		=> '0',
			'water_status'			=> '0',
			'water_type'			=> 'image',
			'water_path'			=> 'public/common/images/mark_water.png',
			'water_position'		=> 'middle_center',
			'water_word'			=> 'www.ciccms.net',
			'water_word_color'		=> '#000000',
			'water_word_size'		=> '12',
			'validate_register'		=> '1',
			'validate_login'		=> '1',
			'validate_admin_login'	=> '1',
			'content_title_length'	=> '50',
			'content_info_length'	=> '50',
			'hits'					=> '0',
			'filter_words'			=> '他妈|你妈',
	),
);

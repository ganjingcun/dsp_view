<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config = array(
		'kefu' => array(
					array(
						'field' => 'user_name',
                        'label' => '客服姓名',
                        'rules' => 'required|min_length[3]|max_length[25]'	
					),
					array(
						'field' => 'user_type',
						'label' => '客户类型',
						'rules' => 'required|callback_is_null'
					),		
					array(
						'field' => 'user_number',
						'label' => '客户号码',
						'rules' => 'required|numeric'
					),
					array(
						'field' => 'user_sort',
						'label' => '客户排序',
						'rules' => 'required|integer'
					),
		),
		'ad'		  => array(
					array(
						'field' => 'category_id',
						'label' => '图片分类',
						'rules' => 'required|callback_is_null'
					),
					array(
						'field' => 'ad_img',
						'label' => '图片地址',
						'rules' => 'required'
					),
					array(
						'field' => 'ad_sort',
						'label' => '图片排序',
						'rules' => 'required|integer'
					),
		),
		'ad_category' => array(
					array(
						'field' => 'category_name',
						'label' => '分类名称',
						'rules' => 'required|min_length[1]|max_length[15]|callback_is_unique[ad_category,category_name]'
					),	
		),
		'ad_style'	=> array(
					array(
						'field' => 'category_name',
						'label' => '分类名称',
						'rules' => 'required'
					),
					array(
						'field' => 'ad_width',
						'label' => '广告宽度',
						'rules' => 'required|min_length[1]|max_length[5]'
					),
					array(
						'field' => 'ad_height',
						'label' => '广告高度',
						'rules' => 'required|min_length[1]|max_length[5]'
					),
					array(
						'field' => 'ad_style',
						'label' => '广告样式',
						'rules' => 'required'
					),
		),
		'category' => array(
					array(
						'field' => 'model_id',
						'label' => '内容模型',
						'rules' => 'required|callback_is_null'
					),
					array(
						'field' => 'category_name',
						'label' => '栏目名称',
						'rules' => 'required|min_length[1]|max_length[25]'
					),
					array(
						'field' => 'category_sort',
						'label' => '栏目排序',
						'rules' => 'required|integer'
					),
					array(
						'field' => 'category_tpl',
						'label' => '页面类型',
						'rules' => 'required'
					),
					array(
						'field' => 'model_id',
						'label' => '内容模型',
						'rules' => 'required'
					),
					array(
						'field' => 'tpl_list_num',
						'label' => '列表页显示数量',
						'rules' => 'required|integer'
					),
					array(
						'field' => 'tpl_list',
						'label' => '列表页模板',
						'rules' => 'required'
					),
					array(
						'field' => 'tpl_content',
						'label' => '内容页模板',
						'rules' => 'required'
					),
		),
		'model'	=> array(
					array(
						'field' => 'model_name',
						'label' => '模型名称',
						'rules' => 'required|min_length[1]|max_length[18]'
					),
					array(
						'field' => 'model_table_name',
						'label' => '模型数据表',
						'rules' => 'required|min_length[1]|max_length[30]'
					),
					array(
						'field' => 'model_list_tpl',
						'label' => '列表页程序',
						'rules' => 'required|min_length[1]|max_length[30]'
					),
					array(
						'field' => 'model_content_tpl',
						'label' => '内容页程序',
						'rules' => 'required|min_length[1]|max_length[30]'
					),
					array(
						'field' => 'model_sort',
						'label' => '模型排序',
						'rules' => 'required|integer'
					),
		),
		'article'	=> array(
					array(
						'field' => 'category_id',
						'label' => '发布栏目',
						'rules' => 'callback_is_null'
					),
					array(
						'field' => 'article_title',
						'label' => '内容标题',
						'rules' => 'required'
					),
					array(
						'field' => 'article_content',
						'label' => '内容',
						'rules' => 'required'
					),

		),
		'product'	=> array(
				array(
						'field' => 'category_id',
						'label' => '发布栏目',
						'rules' => 'callback_is_null'
				),
				array(
						'field' => 'product_title',
						'label' => '内容标题',
						'rules' => 'required'
				),
				array(
						'field' => 'product_content',
						'label' => '内容',
						'rules' => 'required'
				),
		
		),
		'job'		=> array(
				array(
						'field' => 'category_id',
						'label' => '发布栏目',
						'rules' => 'callback_is_null'
				),
				array(
						'field' => 'job_title',
						'label' => '内容标题',
						'rules' => 'required'
				),
				array(
						'field' => 'job_content',
						'label' => '内容',
						'rules' => 'required'
				),
		),
		'alone'		=> array(
				array(
						'field' => 'category_id',
						'label' => '发布栏目',
						'rules' => 'callback_is_null'
				),
				array(
						'field' => 'alone_title',
						'label' => '内容标题',
						'rules' => 'required'
				),
				array(
						'field' => 'alone_content',
						'label' => '内容',
						'rules' => 'required'
				),
		),
		'user'		=> array(
				array(
						'field' => 'user_name',
						'label' => '帐号',
						'rules' => 'required|min_length[3]|max_length[25]|callback_is_unique[user,user_name]'
				),	
				array(
						'field' => 'user_password',
						'label' => '密码',
						'rules' => 'required|min_length[6]'
				),
		),
		'user_admin'	=> array(
				array(
						'field' => 'admin_name',
						'label' => '管理员帐号',
						'rules' => 'required|min_length[3]|max_length[25]|callback_is_unique[AdminUserModel,admin_name]'
				),
				array(
						'field' => 'admin_password',
						'label' => '管理员密码',
						'rules' => 'required|min_length[6]'
				),
				array(
						'field' => 'group_id',
						'label' => '所属管理组',
						'rules' => 'required|callback_is_null'
				),
		),
		'block'		=> array(
				array(
						'field' => 'block_name',
						'label' => '片段名称',
						'rules' => 'required|callback_is_unique[block,block_name]'
				),
				array(
						'field' => 'block_content',
						'label' => '片段内容',
						'rules' => 'required'
				),
		),
		'edit_block'		=> array(
				array(
						'field' => 'block_name',
						'label' => '片段名称',
						'rules' => 'required'
				),
				array(
						'field' => 'block_content',
						'label' => '片段内容',
						'rules' => 'required'
				),
		),
		'reply_ask'		=> array(
				array(
						'field' => 'ask_reply_content',
						'label' => '回复内容',
						'rules' => 'required'
				),
		),
		'reply_msg'		=> array(
				array(
						'field' => 'msg_reply_content',
						'label' => '回复内容',
						'rules' => 'required'
				),
		),
		'admin_login'	=> array(
				array(
						'field' => 'admin_name',
						'label' => '管理员姓名',
						'rules' => 'required'
				),
				array(
						'field' => 'admin_password',
						'label' => '管理员密码',
						'rules' => 'required'
				),
		),
		'admin_group'	=> array(
				array(
						'field' => 'group_name',
						'label' => '分组名称',
						'rules' => 'required'
				),
		),
		'announce_form'=>array(
				array(
						'field'=>'title',
						'label'=>'标题',
						'rules'=>'max_length[18]'
				),
				array(
						'field'=>'content1',
						'label'=>'内容',
						'rules'=>'max_length[250]'
				)
		)

);
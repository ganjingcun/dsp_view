<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 继承类
 *
 * @brief 验证登陆
 * @author 郭亚超
 * @date 2012.09.17
 * @note 详细说明及修改日志
 */
class MY_Controller  extends CI_Controller
{
	var $per_page = 12;		//后台默认分页每页显示条数
	function __construct($isCheckLogin=true)
	{
		parent::__construct();
		$this->load->helper("returnstatus");
		$this->load->helper("language");
		$this->load->helper("tools");
		$this->load->library('session');
	}

	/**
	 * 显示页面模板，跟$this->load->display（。。）的用法一样，不过这里集成了$this->smarty->assign,
	 * 使页面传值更方便
	 * @param $tpl
	 * @param $param
	 * @return unknown_type
	 */
	protected function display($tpl,array $param=array() )
	{
		if( !empty($param) )
		foreach($param as $k=>$v)
		{
			$this->smarty->assign($k,$v);
		}

		$this->load->display($tpl);
	}

	/**
	 * 获取配置项
	 * @param $config 			'ciccms/site'
	 * @param $item				'web_title'
	 * @param $child_item		'web_title'数组下的元素名称
	 */
	protected  function get_config($config,$item='',$child_item='')
	{
		$this->load->config($config);
		$config = array_pop(explode('/',$config));
		if($item == ''){
			$config = $this->config->item($config);
		}else{
			$config = $this->config->item($item,$config);
			if($child_item != '')
			$config = $config[$child_item];
		}
		return $config;
	}

	/**
	 * 表单验证
	 * @param $array  		config下的验证配置项目
	 * @param $delimiters	array('<p>','</p>')
	 */
	protected function validate($type,$delimiters='')
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_message('required', '%s 不能为空！');
		$this->form_validation->set_message('min_length', '%s 最小长度错误！');
		$this->form_validation->set_message('max_length', '%s 长度过长！');
		$this->form_validation->set_message('integer', '%s 必须为数字！');
		if ($this->form_validation->run($type) == FALSE)
		{
			if($delimiters != '')
			$this->form_validation->set_error_delimiters($delimiters[0],$delimiters[1]);
			return validation_errors();
		}
		return TRUE;
	}

	/**
	 * 表单验证回调函数：验证select是否选择
	 */
	public function is_null($value)
	{
		if(empty($value))
		{
			$this->form_validation->set_message('is_null', '请选择 %s！');
			return false;
		}else{
			return true;
		}
	}
	/**
	 * 表单验证回调函数：验证唯一性
	 * @param $param [table_name,$field]
	 */
	public function is_unique($value,$param)
	{
		$param = explode(',', $param);
		$this->load->model($param[0]);
		$obj = new $param[0];		
		$row = $obj->getOne(array($param[1]=>$value),$param[1]);
		if(empty($row))
		{
			return true;
		}else{
			$this->form_validation->set_message('is_unique', $value.' 已存在！');
			return false;
		}
	}
	/**
	 * 获取无限分类
	 * @param int	 $model_id
	 * @return string
	 */
	protected function get_category_tree($model_id='')
	{
		if($model_id === '')
			$where = '';
		else
			$where = array('model_id'=>$model_id);
		
		$category = $this->cic_model->get_all('category',$where,array('category_sort','asc','category_id','asc'));
		if($category)
		{
			foreach($category->result() as $key=>$value)
			{
				$rows[$key]['category_id'] = $value->category_id;
				$rows[$key]['category_pid'] = $value->category_pid;
				$rows[$key]['category_name'] = $value->category_name;
			}
			$unique_category = $this->get_unique_category($rows);
			return $this->category_tree($rows).$unique_category;
		}
	}
	/**
	 * 获取无限分类遗漏的没有上级分类的分类
	 */
	function get_unique_category($rows)
	{
		$unique_category = $category_id = $category_pid ='';
		foreach($rows as $value)
		{
			$category_id[] = $value['category_id'];
			if($value['category_pid'] != 0)
			{
				$category_pid[$value['category_id']] = $value;
			}	
		}
		if(! empty($category_pid))
		{
			foreach($category_pid as $key=>$value)
			{
				if(! in_array($value['category_pid'], $category_id))
				{
					$unique_category .= '<option value="'.$value['category_id'].'">- - - - - - -'.$value['category_name'].'</option>';
				}
			}
		}
		return $unique_category;
	}
	/**
	 * 递归生成分类
	 * <option></option>
	 */
	private function category_tree($rows,$category_pid=0,$n=0)
	{
		$space = str_repeat('- - ', $n);
		$n++;
		$str='';
		$rows_length = count($rows);
		for($i=0;$i<$rows_length;$i++)
		{
			if($rows[$i]['category_pid'] == $category_pid)
			{
				$str .= '<option value="'.$rows[$i]["category_id"].'">'.$space.$rows[$i]['category_name'].'</option>';
				$str .=$this->category_tree($rows,$rows[$i]['category_id'],$n);
			}
		}
		return $str;	
	}
	/**
	 * 分页
	 */
	protected function page($base_url,$total_rows,$per_page='',$page_query_string=FALSE,$uri_segment=3,$cur_tag_open='<span class="current">',$cur_tag_close='</span>')
	{
		$this->load->library('pagination');
		$config = array(
				'base_url'			=> $base_url,
				'total_rows'		=> $total_rows,
				'per_page'			=> $per_page,
				'uri_segment'		=> $uri_segment,
				'page_query_string'	=> $page_query_string,
				'cur_tag_open'		=> $cur_tag_open,
				'cur_tag_close' 	=> $cur_tag_close,
		);
		$this->pagination->initialize($config);
		return $this->pagination->create_links();
	}
	
	/**
	 * 提示跳转
	 * @param $msg 提示信息
	 * @param $location 跳转链接
	 */
	function pageMsgBack($msg,$location=0) {
		$this->smarty->assign('errormsg',$msg);
		$this->smarty->assign('location',$location);
		$this->load->display('admin/error.html');
		return;
	}
}

/**
 * 后台继承类
 */
define('UPLOAD_PATH', UPLOAD_DIR . '/stuff/img/' . date('Ym'));
class MY_A_Controller extends MY_Controller
{
	var $admin_config;					//后台menu配置数组		
	var $per_page			= 12;		//后台默认分页每页显示条数
	function __construct()
	{
		parent::__construct();			
		$this->init();
		$this->assign('PIC_URL', $this->config->item('pic_url'));
		$this->load->model('SystemConfigModel');
		$this->load->model('AdminNodeModel');
		$this->load->model('AdminGroupModel');
		$user = $this->session->userdata("admin");
		$this->load->model('CommonModel');
		$flag = $this->AdminNodeModel->getAdminNode($user['admin_id']);
		$public = array('admin'=>'','index'=>'', 'main'=>'','top'=>'', 'center'=>'', 'bottom'=>'','left'=>'');
		$flag = array_merge($flag, $public);
		 
                $class = "/".$this->uri->segment(2)."/".$this->uri->segment(3);//echo $class; && !isset($flag[$this->router->class])
                if(!$this->AdminNodeModel->check_privilege($class)){
                       exit('no privilege !');  
                }
 		$this->assign('AMOUNT_RATIO', AMOUNT_RATIO);
	}
	/**
	 * 初始化
	 */
	private function init()
	{		
		//载入position类：后台获取当前位置与提示		message类：执行成功失败显示的消息类
		$this->load->library(array('admin/position','admin/message'));
		//获取后台top的菜单与left的子菜单
		$this->admin_config 	= $this->get_config('ciccms/admin');
		//判断是否登录
		$this->check_login();
	}

	/**
	 * 头部
	 */
	public function top()
	{		
		$this->smarty->assign('admin',$this->session->userdata('admin'));
		$this->smarty->assign('admin_config',$this->admin_config);
		$this->load->library('session');
		$adminInfo = $this->session->userdata('admin');
		$this->smarty->assign('username',$adminInfo['admin_name']);
		$this->load->display('admin/include/top.html');
		//$this->load->view('admin/include/top',$data);
	}
	/**
	 * 主体
	 */
	public function center()
	{
		$this->load->display('admin/include/center.html');
	}
	/**
	 * 左边
	 */
	public function left()
	{
		$out_nav=$this->admin_config['nav'];
		$admin = $this->session->userdata('admin');
		$admin['group_privilege']['admin'] = array('index', 'main','top', 'center', 'bottom','left');//需要默认都可以访问的公共页面。
		$out_nav_new = array();
		
		$this->load->library('session');
		$user = $this->session->userdata("admin");
		$out_nav = $this->AdminNodeModel->getNav($user['admin_id']);


 		$this->smarty->assign('out_nav',$out_nav);
		$this->load->display('admin/include/left.html');
 	}
	/**
	 * 底部
	 */
	public function bottom()
	{
		$this->load->display('admin/include/bottom.html');
	}
	
	/**
	 * 表单验证
	 * @param $array  config下的验证配置项目
	 * @param $url	 'admin/admin/index'
	 */
	protected function validate($type,$url)
	{
		$result = parent::validate($type,array('<li>','</li>'));
		if($result !== TRUE)
			$this->message->msg($result,'/'.($url),5);
	}
	
	/**
	 * 获取ckeditor 配置项
	 */
	protected function ckeditor_config($id)
	{
		return array(
				'id'		=> $id,
				'path'  	=> 'public/common/js/plug/ckeditor',
				'config'	=> array(
						'width'		=> '900px',
						'height'	=> '200px',
						'filebrowserImageBrowseUrl'	=> ('/admin/image/box_show'),
				),		
		);
	}
	/**
	 * 判断是否登录
	 */
	protected function check_login()
	{
		$is_login = $this->session->userdata('is_login');
		if(! $is_login)
		{
			redirect('/admin/login');
		}
		//$this->admin_priv();
	}
	/*
	 * 提示跳转
	 * @param $msg 提示信息
	 */
	function pageMsgBack($msg,$url='') {
		header("Content-type: text/html; charset=utf-8"); 
		$str = '<script type="text/javascript">'; 
		$str.="alert('".$msg."');"; 
		
		if ($url != '') 
		{ 
			$str.="window.location.href='{$url}';"; 
		}else{ 
			$str.="window.history.back();"; 
		} 
			echo $str.='</script>'; 
			exit;
	}
	
	/**
	 * 会自动根据ajax或者返回页面的相应函数
	 * @param unknown $msg
	 * @param unknown $status
	 * @param string $url
	 */
	function respToClient($msg,$status=-1,$url='') {
	    $isajax = $this->input->is_ajax_request();
	    if($isajax) {
	        ajaxReturn($msg, $status);
	    } else {
	        pageMsgBack($msg,$url);
	    }
	}
	
	protected function admin_priv()
	{
		if($this->uri->segment(1) == 'admin')
		{
			$class 		= $this->uri->segment(2);
			$function	= $this->uri->segment(3);
			if(!$function){
				$function = 'index';
			}
			$admin = $this->session->userdata('admin');
			if(!isset($admin['group_privilege']['all']))
			{
				$admin['group_privilege']['admin'] = array('index', 'main','top', 'center', 'bottom','left');//需要默认都可以访问的公共页面。
				if(isset($admin['group_privilege'][$class]))
				{
					if(! in_array($function,$admin['group_privilege'][$class]))
					{
						$this->message->msg('无此权限',('/admin/admin/main'));
					}
				}else{
					$this->message->msg('无此权限',('/admin/admin/main'));
				}
			}
		}
	}
	
	protected function display($tpl = ''){
		return $this->load->display($tpl);
	}

	protected function assign($key, $val){
		return $this->smarty->assign($key, $val);
	}
	/**
	 * 获取时间控件的值并设置到模板
	 * @return type
	 */
	function getDateRangeByControl($form_input_name='inputDate', $field_startdate='sDate',$field_enddate='eDate'){
		$where = array();
		$inputDate = null;
		if($this->input->post($form_input_name)!=null) {
			$inputDate = $this->input->post($form_input_name)?$this->input->post($form_input_name):'~';
		} else if($this->input->get($form_input_name)!=null){
			$inputDate = $this->input->get($form_input_name)?$this->input->get($form_input_name):'~';
		}
		if($inputDate!=null) {
			$timeDate = explode('~', $inputDate);
		}
		if(isset($timeDate[0]))    $where[$field_startdate] = trim($timeDate[0]);
		if(isset($timeDate[1]))   $where[$field_enddate] = trim($timeDate[1]);
		
		if(empty($where[$field_startdate] ))
		{
			$where[$field_startdate] = date('Y-m-d' , strtotime('-30 day'));
		}
		if(empty($where[$field_enddate]))
		{
			$where[$field_enddate] = date('Y-m-d',strtotime('+1 day'));
		}
		$this->assign('enddate', $where[$field_enddate]);
		$this->assign('startdate', $where[$field_startdate]);
		return $where;
	}
    
    public function uploadImg(){
        $postdata = $this->input->get();
		$this->load->library('MyUpload', $_FILES['Filedata']);
        $type = isset($postdata['type'])?$postdata['type']:'';
        $uploadpath = isset($postdata['uploadpath'])?UPLOAD_DIR . $postdata['uploadpath'] . date('Ym'):UPLOAD_PATH;
        $fileExt = isset($postdata['fileExt'])?$postdata['fileExt']:array('jpg', 'png');
        $maxsize = isset($postdata['maxsize'])?$postdata['maxsize']:120 * 1024;    
		$upload = new MyUpload($_FILES['Filedata']);
		$upload->setUploadPath($uploadpath);
		
		$upload->setFileExt($fileExt);

		$upload->setMaxsize($maxsize);

		$strError = '';
		$data = array('isReceived'=>false,'imgInfo'=>array(),'errorMessage'=>array());
		if (!$upload->isAllowedTypes()) {
			$strError = '您上传的文件格式错误 !';
		} elseif ($upload->isBigerThanMaxSize()) {
			$strError = '此处的文件最大不能超过 ' . intval($upload->getMaxsize() / 1024) . 'KB';
		}
		
		if(!empty($type)){
        $imgSize = explode("*", $type);
        $width = $imgSize[0];
        $height = $imgSize[1];
        
		if(!$upload->isAllowedSize($width, $height)){
			$strError .= " 图片的尺寸必须为：".$width." X ".$height;
		}
		}
		if (empty($strError) and $upload->upload()) {
			$imgPath = $upload->getUplodedFilePath();
			$data['imgInfo']['newImg'] = $imgPath;
			$data['newImg'] = $imgPath;
			$data['isReceived'] = 1;
		} else {
			$data['errorMessage'] = $strError;
			$data['isReceived'] = 0;
		}
		
		
		if ($data['isReceived']) {
            $data['imgs_id'] = $type;
        }

        echo json_encode(array($data));
        return;
    }
}
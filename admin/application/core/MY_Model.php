<?php

require_once(BASEPATH.'database/mysql/config.php');

class MY_Model extends CI_Model{
    protected $comparison = array('eq'=>'=','neq'=>'<>','gt'=>'>','egt'=>'>=','lt'=>'<','elt'=>'<=','notlike'=>'NOT LIKE','like'=>'LIKE');//查询条件
    protected $lastsql = '';

    protected $DB = null;
    function __construct()
    {
        parent::__construct();
        $this->DB = new DBDispatcher();
        $this->DB->addMaster(Connection::getConnection(DATABASE_MAIN_W));
        $this->DB->addSlave(Connection::getConnection(DATABASE_MAIN_R));

    }



    /**
     * 表名称
     * @var tring
     */
    protected $table = '';

    /**
     * 表id
     * @var string
     */
    protected $id = '';
    /**
     * 查询条件
     * @var string
     */
    protected $sqlStrWhere = '';
    /**
     * 分组条件
     * @var string
     */
    protected $groupBy = '';
    /**
     * 更改当前表名称
     * @param $table
     * @return null
     */
    protected function setTable( $table )
    {
        $this->table = $table;
    }

    /**
     * 更改当前表的主键id
     * @param $id
     * @return null
     */
    protected function setId( $id )
    {
        $this->id = $id;
    }
    /**
     * 更改当前查询条件
     * @param $where
     * @return null
     */
    public function setStrWhere( $where )
    {
        $this->sqlStrWhere = $this->getWhere($where);
    }
    /**
     * 更改当前分组条件
     * @param $where
     * @return null
     */
    public function setGroupBy( $groupBy='' )
    {
        $this->groupBy = $groupBy;
    }
    /**
     * 此方法用户保存到数据库的更新，是更新操作还是插入操作，完全取决于是否$id 参数是空
     *
     * @param mixed $id 混合数据类型，可以是数字id，也可以是数组
     * @param array $row
     * @return int|null
     */
    public function save($id='',array $row)
    {
        if(!empty($id))
        {
            return $this->edit($id,$row);
        }
        else
        {
            return $this->add($row);
        }
    }



    /**
     * 添加一条记录
     * @param array $row  形如：array('name'=>'张三',age='25')
     * @param bool $isReplace 是否使用replace
     * @param bool $isDuplicate 是否使用DUPLICATE
     * @param string  $field 要更新的字段$field=$field+1
     * @return insertId|null
     */
    public function add(array $row,$isReplace=false,$isDuplicate=false,$field='')
    {
        try{
            $arrComumn 	= array_keys($row);//字段名数组
            $arrValue 	= array_values($row);//值数组
            $lengh 		= count($arrComumn);//字段个数
            $placeholders = array();//占位符数组

            for($i=0;$i<$lengh;$i++)
            {
                $placeholders[] = '?';
            }
            $db = $this->DB->master();
            $sql = $isReplace?'REPLACE':'INSERT';
            $keys = "`".implode('`,`',$arrComumn)."`";
            if($isReplace == false && $isDuplicate ==true && $field){
                $sql .= " INTO `{$this->table}`(".$keys.") VALUES(".implode(',',$placeholders).")  ON DUPLICATE KEY UPDATE ".$field ;
            }else{
                $sql .= " INTO `{$this->table}`(".$keys.") VALUES(".implode(',',$placeholders).")";
            }

            $db = $db->prepare($sql);
            $this->lastsql = $sql;
            for($i=1;$i<=$lengh;$i++)
            {
                $db->bind($i,$arrValue[$i-1]);
            }


            $db->execute();

            $lastId = NULL;
            if($db->getrowcount()>0)
            {
                $lastId = $db->lastInsertID();
            }
            //插入日志
            $admin = $this->session->userdata("admin");
            $adminid = isset($admin['admin_id']) ? $admin['admin_id'] : 0;
            $ip = $this->input->ip_address();
            $db = $db->prepare("INSERT INTO record_log (sqltable, sqltype, sqlstr, sqlvalue, createtime, adminid, ip) VALUES ('{$this->table}', 'INSERT', '".addslashes(json_encode($sql))."', '".addslashes(json_encode($arrValue))."', UNIX_TIMESTAMP(), '".$adminid."', '".$ip."');");
            @$db->execute();
            //插入日志结束
            return $lastId;
        }
        catch(Exception $e)
        {
            $this->showError($e);
        }
    }


    /**
     * 向一个表中同时批量添加多条记录
     * @param array $list 列表数组
     * @param unknown_type $isReplace 是否执行 replace into 。。。
     * @return affected count 返回受影响的行数
     */
    public function addBatch(array $list,$isReplace = false)
    {
        try
        {
            $tmp = $list;
            $cols = @implode(',',array_keys(array_pop($tmp)));

            $values = array();
            foreach( $list as $k=>&$v )
            {
                foreach($v as $kk=>&$val)
                {
                    $val = "'".$val."'";
                }
                $values[] = "(".@implode(',',$v).")";
            }
            $sql = $isReplace?'REPLACE':'INSERT';
            $sql .= " INTO `{$this->table}` ($cols) VALUES ".@implode(',',$values);
            $db = $this->DB->master()->prepare($sql)->execute();
            return $db->getrowcount();
        }
        catch(Exception $e)
        {
            $this->showError($e);
        }

    }


    /**
     * 根据条件编辑记录
     *
     * @param mixed $condition 条件数组
     * @param array $row 要更新的数组 形如：array('name'=>'aaa',....);
     * @param array $cumulation 要累加的字段数组 形如：array('reply_num','coins',....);
     * @return int 返回受影响的记录总数
     */
    public function edit($condition,array $row,$cumulation=array())
    {
        if(empty($condition) or empty($row)) return ;

        try{

            //组织where条件
            $strWhere = '';
            if(is_array($condition))
            {
                $strWhere = $this->getWhere($condition);
            }
            else
            {
                $strWhere = $this->id." = '".$condition."'";
            }

            $arrComumn 	= array_keys($row);//字段名数组
            $arrValue 	= array_values($row);//值数组
            $lengh 		= count($arrComumn);//字段个数

            foreach($arrComumn as $k=>&$col)
            {
                if(!empty($cumulation) && in_array($col,$cumulation)){
                    $col = $col." = ".$col."+?";
                }else{
                    $col = $col." = ?";
                }
            }

            $sql = "UPDATE {$this->table} SET ".implode(',',$arrComumn)." WHERE ".$strWhere;
            $db = $this->DB->master();
            $db = $db->prepare($sql);
            $this->lastsql = $sql;
            for($i=1;$i<=$lengh;$i++)
            {
                $db->bind($i,$arrValue[$i-1]);
            }
            $db->execute();
            $rowcount = $db->getrowcount();
            //插入日志
            if (isset($this->session))
            {
                $admin = $this->session->userdata("admin");
                $adminid = isset($admin['admin_id']) ? $admin['admin_id'] : 0;
            }
            else
            {
                $admin = 'chancedsp';
                $adminid = 0;
            }

            $ip = $this->input->ip_address();
            $db = $db->prepare("INSERT INTO record_log (sqltable, sqltype, sqlstr, sqlvalue, createtime, adminid, ip) VALUES ('{$this->table}', 'UPDATE', '".addslashes(json_encode($sql))."', '".addslashes(json_encode($arrValue))."', UNIX_TIMESTAMP(), '".$adminid."', '".$ip."');");
            	
            @$db->execute();
            //插入日志结束
            return $rowcount;
        }
        catch(Exception $e)
        {
            $this->showError($e);
        }
    }

    function query($sql){
        $db = $this->DB->master();
        $db = $db->prepare($sql);
        return $db->execute();
    }


	function getWhere($where,$operate=' AND '){
        $whereStr='';
        $otherCondition =' ';
        if(is_string($where)){
            $whereStr=$where;
        }else{
            foreach ($where as $key=>$val){
                if($key=='othercondition'){
                    $otherCondition.= $val;
                }else{
                    $whereStr .= '( ';
                    $whereStr .= $this->parseWhereItem($key,$val);
                    $whereStr .= ' )'.$operate;
                }
            }
            $whereStr = trim( substr($whereStr,0,-strlen($operate)).$otherCondition,$operate);
        }
        return empty($whereStr)?'':' '.$whereStr;
    }

    /**
     * 解析where子单元，需要配合上面的getWhere使用
     * @param $key
     * @param $val
     */
    protected function parseWhereItem($key,$val) {
        $whereStr = '';
        $keys = "`$key`";
        if(strpos($key, "."))
        $keys = "$key";
        if(is_array($val)) {
            if(is_string($val[0])) {
                if(preg_match('/^(EQ|NEQ|GT|EGT|LT|ELT|NOTLIKE|LIKE)$/i',$val[0])) { // 比较运算
                    $whereStr .= "`$key` ".$this->comparison[strtolower($val[0])]." '".addslashes($val[1])."'";
                }elseif('exp'==strtolower($val[0])){ // 使用表达式
                    $whereStr .= ' (`'.$key.'` '.$val[1].') ';
                }elseif(preg_match('/IN/i',$val[0])){ // IN 运算
                    if(isset($val[2]) && 'exp'==$val[2]) {
                        $whereStr .= " $keys ".strtoupper($val[0]).' '.$val[1];
                    }else{
                        if(is_string($val[1])) {
                            $val[1] =  explode(',',$val[1]);
                        }
                        $zone   =   implode("','",array_map('addslashes',$val[1]));
                        $whereStr .= "$keys ".strtoupper($val[0])." ('".$zone."')";
                    }
                }elseif(preg_match('/BETWEEN/i',$val[0])){ // BETWEEN运算
                    $data = is_string($val[1])? explode(',',$val[1]):$val[1];
                    $whereStr .=  ' ('.$keys.' '.strtoupper($val[0]).' '.addslashes($data[0]).' AND '.addslashes($data[1]).' )';
                }else{
                    exit('解析错误');
                }
            }else {
                $count = count($val);
                if(!is_array($val[$count-1])){
                    if(in_array(strtoupper(trim($val[$count-1])),array('AND','OR','XOR'))) {
                        $rule = strtoupper(trim($val[$count-1]));
                        $count   =  $count -1;
                    }else{
                        $rule = 'AND';
                    }
                }else{
                    $rule = 'AND';
                }
                for($i=0;$i<$count;$i++) {
                    $data = is_array($val[$i])?$val[$i][1]:$val[$i];
                    if('exp'==strtolower($val[$i][0])) {
                        $whereStr .= '(`'.$key.'` '.$data.') '.$rule.' ';
                    }else{
					   // echo 'data'.$data;
                        $op = is_array($val[$i])?$this->comparison[strtolower($val[$i][0])]:'=';
						//echo '#'.$op.'#';
						$whereStr .= '(`'.$key.'` '.$op.' \''.addslashes($data).'\') '.$rule.' ';
                    }
                }
                $whereStr = substr($whereStr,0,-4);
            }
        }else{
            $whereStr .= "`$key` = '".addslashes($val)."'";
        }
        return $whereStr;
    }




    /**
     *
     * 获取单个表的list列表
     *
     * @param array $condition 条件数组
     * @param string $fileds 要获取的列名
     * @param string $orderBy sql语句中“order by ”之后的部分,形如：‘id desc’
     * @param string $groupBy sql语句中group by 之后的部分
     * @param int $pageId 当前页码
     * @param int $pageSize 每一页显示的条数
     * @return array  返回从数据库中查询获得的二维数组
     */
	public function getList($condition=array(),$fileds = '*',$orderBy='',$groupBy='', $pageId = 1, $pageSize = 0)
    {

        try
        {
            $sql = "SELECT {$fileds} FROM `{$this->table}` WHERE 1 ";
            $strWhere = $this->getWhere($condition);
            $this->sqlStrWhere = $this->sqlStrWhere!='' ? $this->sqlStrWhere . $strWhere : $strWhere;
            $sql.= empty($strWhere)?'':' AND '.$strWhere;
            $sql.= $this->groupBy = empty($groupBy)?'':' GROUP BY '.$groupBy;
            $sql.= empty($orderBy)?'':' ORDER BY '.$orderBy;
            $sql.= $this->limit($pageId,$pageSize);
            //						echo $sql."\r\n";
            //			return $this->DB->read()->query_prepare($sql)->query_execute()->query_getall();
            $this->lastsql = $sql;
            return $this->DB->read()->query($sql)->query_getall();
        }
        catch(Exception $e)
        {
            $this->showError($e);
        }
    }

    function setDec($condition, $fileds, $num=1){
        $sql = "update `{$this->table}` set {$fileds}=({$fileds}+$num) WHERE 1 ";
        $strWhere = $this->getWhere($condition);
        $this->sqlStrWhere = $strWhere;
        $sql.= empty($strWhere)?'':' AND '.$strWhere;
        $this->lastsql = $sql;
        $db = $this->DB->master();
        $db = $db->prepare($sql);
        return $db->execute();
    }


    /**
     * 根据条件获取记录数量，此方法可配合getList使用，在调用getList之后调用，用来查询符合条件的总记录数
     * @return int
     */
    public function getCount($where = '')
    {
        if($where){//增加查询条件
            $this->sqlStrWhere = $this->getWhere($where);
        }
        try
        {
            $sql = "SELECT count(*) num FROM `{$this->table}` WHERE 1 ";
            if(isset($this->sqlStrWhere))
            {
                $sql.= empty($this->sqlStrWhere)?'':' AND '.$this->sqlStrWhere;
            }
            else
            {
                return 0;
            }

            $sql .= $this->groupBy;
            $this->lastsql = $sql;
            $data = $this->DB->read()->query($sql)->query_getsingle();
            return $data['num'];
        }
        catch(Exception $e)
        {
            $this->showError($e);
        }
    }

    /**
     * 组织sql语句的limit部分
     * @param $pageId 当前页码
     * @param $pageSize 页面记录数
     * @return SQL LIMIT sql语句的limit部分
     */
    protected function limit($pageId=1,$pageSize=0)
    {
        if((int)$pageSize==0) return '';

        $offset = ($pageId-1)*$pageSize;

        return ' LIMIT '.$offset.','.$pageSize;
    }


    /**
     * 获取一行数据
     * @param $condition
     * @param $fileds
     * @return unknown_type
     */
	public function getRow(array $condition=array(),$fileds = '*',$orderBy='')
    {
        try{
            $sql = "SELECT {$fileds} FROM `{$this->table}` WHERE 1 ";
            $strWhere = $this->getWhere($condition);
            $where = $this->sqlStrWhere = $strWhere;
            $sql.= empty($where)?'':'AND '.$where;
			if(!empty($orderBy)) {
				$sql.='order by '.$orderBy;
			}
            $this->lastsql = $sql;
            return $this->DB->read()->query($sql)->query_getsingle();
        }
        catch(Exception $e)
        {
            $this->showError($e);
        }
    }


    /**
     *
     * 根据条件删除一条记录
     *
     * @param array $condition 查询条件数组
     * @return int 返回受影响的行数
     */
    public function delete(array $condition)
    {
        try
        {
            if(empty($condition)) return false;
            $strWhere = $this->getWhere($condition);

            $sql = "DELETE FROM `$this->table` WHERE {$strWhere}";
            $this->lastsql = $sql;
            return $this->DB->master()->prepare($sql)->execute()->getrowcount();
        }
        catch(Exception $e){
            $this->showError($e);
        }
    }

    /**
     * 获取一列数据
     * @param array $condition
     * @param string $columnName 要获取的单列的名称
     * @return array 一位数组
     */
    public function getCol( array $condition = array() ,$columnName, $orderBy = '')
    {
        $data = $this->getList($condition,$columnName,$orderBy);
        if( ! empty($data) )
        {
            $tmp = array();

            foreach( $data as $k=>$v )
            {
                $tmp[] = $v[$columnName];
            }
            return $tmp;
        }
    }

    /**
     * 获取一行记录级的第一个字段
     * @param array $condition
     * @param string $columnName
     * @return string
     */
	public function getOne(array $condition = array() ,$columnName,$orderBy ='')
    {
		$data = $this->getRow( $condition ,$columnName,$orderBy);
        return $data[$columnName];
    }

    /**
     * 获取两列数据，将第一个字段作为键，第二个字段作为值
     * @param array $condition 查询条件
     * @param string $columnNames 数据库字段，必须为两个
     * @return array 返回一维数组
     */
    public function getTwo(array $condition = array() ,$columnNames)
    {
        $data = $this->getList( $condition ,$columnNames);

        if(!empty($data))
        {
            $tmp = array();
            foreach( $data as $k=>$row)
            {
                reset($row);
                $tmp[current($row)] = next($row);
            }
            return $tmp;
        }
        return $data;
    }

    /**
     * 检查某条记录是否存在,存在返回true ，不存在返回false
     * @param $condition
     * @return unknown_type
     */
    public function isExists( array $condition )
    {
        $num = $this->getOne($condition,'count(*)');
        return ($num>0);
    }

    public function showError(Exception $e)
    {
        if(defined('ENVIRONMENT') and ENVIRONMENT!='production')
        {
            echo "<p>".$e->getMessage()."<p/>";
        }
    }

    /**
     * 联合查询
     * @param $condition
     * @param String $table 联合查询表名
     * @param String $fileds 查询字段
     * @param String $type   INNER LEFT JOIN
     * @param array  $joinon 连接条件  array("a.user_id"=>"b.user_id")
     * @param String $orderBy
     * @param String $groupBy
     * @param Int    $pageId
     * @param Int    $pageSize
     * @param String $cb 当前表别名
     * @param String $ub 联接表别名
     */
    public function getUnitList(array $condition=array(),$table,$fileds = '*',$type="INNER",$joinon=array(),$orderBy='',$groupBy='',$pageId = 1, $pageSize = 0,$cb="a",$ub="b")
    {

        try
        {
            $sql = "SELECT {$fileds} FROM `{$this->table}` ".$cb." ".$type." JOIN `".$table."` ".$ub." ON (";
            $sql .= implode("=", $joinon).") WHERE 1 ";
            $strWhere = $this->getWhere($condition);
            $this->sqlStrWhere = $strWhere;
            $sql.= empty($strWhere)?'':' AND '.$strWhere;
            $sql.= $this->groupBy = empty($groupBy)?'':' GROUP BY '.$groupBy;
            $sql.= empty($orderBy)?'':' ORDER BY '.$orderBy;
            $sql.= $this->limit($pageId,$pageSize);
            return $this->DB->read()->query($sql)->query_getall();
        }
        catch(Exception $e)
        {
            $this->showError($e);
        }
    }

    /**
     * 返回最后执行的SQL
     */
    public function getLastSql(){
        return $this->lastsql;
    }

    public function getSqlQuery($sql){
        return $this->DB->read()->query($sql)->query_getall();
    }
}
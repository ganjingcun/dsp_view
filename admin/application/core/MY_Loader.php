<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_Loader extends CI_Loader {
  function __construct(){
    parent::__construct();
    $CI =& get_instance();
    $CI->config->load('smarty');
    $config_smarty = $CI->config->item('smarty');
    $CI->smarty = new Smarty();
    $CI->smarty->setTemplateDir($config_smarty['TemplateDir']);
    $CI->smarty->setCompileDir($config_smarty['CompileDir']);
    $CI->smarty->setConfigDir($config_smarty['ConfigDir']);
    $CI->smarty->setCacheDir($config_smarty['CacheDir']);
  }
  public function display($template = null, $data = false, $to_string = false, $cache_id = null, $compile_id = null, $parent = null){
    $CI =& get_instance();
    if ($data) foreach ($data as $key => $value)
      $CI->smarty->assign($key, $value);
    ob_start();
    $CI->smarty->display($template, $cache_id, $compile_id, $parent);
    if (ob_get_level() > $this->_ci_ob_level + 1){
      ob_end_flush();
    }else{
      $CI->output->append_output(ob_get_contents());
      @ob_end_clean();
    }
  }
}
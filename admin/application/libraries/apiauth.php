<?php

/**
 * 开放api操作基类
 * 
 * @package 
 * @version $id$
 * @copyright Tommato 2.0
 * @author lihao <lihao@chukong-inc.com> 
 * @info 所有接口必须遵循以下规范
         1.所有接口必须包含 $_notVoidKeys 中规定的字段
 * @errno 所有错误类型
         -1001 帐号无权限
         -1002 必填字段验证未通过
         -1003 数据被篡改
         -1    验证规则未通过
 */
class apiAuth
{
    
    /**
     * 是否开启 debug模式，debug模式开启后将不会输出json格式数据而是 以 var_dump 形式 打印结果
     * 
     * @var Boolean
     * @access private
     */
    private $_debug = false;

    /**
     * 接口授权码
     * 
     * @var string
     * @access private
     */
    private $_pinCode = '';

    /**
     * 存放用户名PIN码的对应关系
     * 
     * @var array 格式：array(username => '32位pin码');
     * @access private
     */
    private $_pinCodeMap = array();

    /**
     * 用户id 
     * 
     * @var int 
     * @access public
     */
    public $userID = 0;

    /**
     * 用户组id
     * 
     * @var int
     * @access public
     */
    public $groupID = 0;

    /**
     * 所有接口均不能为空的KEY
     * 
     * @var array
     * @access private
     */
    private $_notVoidKeys = array('scode', 'username');

    /**
     * 构造函数 请在这里设置帐号的PIN码
     * 
     * @access public
     * @return void
     * @author lihao <lihao@chukong-inc.com> 
     */
    public function __construct()
    {
        $this->_pinCodeMap = array(
            'panjuan@chukong-inc.com' => array('id' => 137, 'groupId' => 148, 'pin' => '11f17cf35dd773b5548bf13b486ef789'),
        );
    }

    /**
     * 向接口请求端输出结果
     * 
     * @param int $errno 错误代码 如果小于 0 为出现错误
     * @param string $error 错误信息
     * @param array $data 返回数据
     * @access public
     * @return string JSON
     * @author lihao <lihao@chukong-inc.com> 
     */
    public function showResult($errno = 1, $error = 'Succ', $data = array())
    {
        $res = array(
            'status' => $errno,
            'msg' => $error,
            'data' => $data,
        );

        if($this->_debug)
        {
            var_dump($res);
        }else
        {
            $res = json_encode($res);
            echo $res;
        }
        exit;
    }

    /**
     * 验证数据是否合法 保证数据没有被篡改过 未篡改则返回 rawurldecode后的数据
     * 
     * @access public
     * @return void
     * @author lihao <lihao@chukong-inc.com> 
     * return array
     */
    public function checkSign($data)
    {
        //检查必要字段
        $this->checkVoid($this->_notVoidKeys, $data);
        //获取用户名和scode
        $username = $data['username'];
        $scode = $data['scode'];
        unset($data['scode']);
        //设置pin码
        $this->setPinByUsername($username);
        //获取pin码
        $pin = $this->_pinCode;
        //验证数据是否有效
        ksort($data);
        $key = http_build_query($data);
        $key = md5($key . $pin);
        //断言
        $this->assert($scode === $key, '-1003', ' scode 验证失败，请检查数据是否被篡改,或者是否使用了未被允许的字段。');
        foreach($data as $k => $v)
        {
            $data[$k] = rawurldecode($v);
        }
        return $data;
    }

    /**
     * 获取接口keys 中 规定的参数
     * 
     * @param mixed $keys array(key1, key2, key3);
     * @param mixed $args 接收到的参数;
     * @access public
     * @return void
     * @author lihao <lihao@chukong-inc.com> 
     */
    public function getRequest($keys, $args)
    {
        //加入必要参数
        $keys = array_merge($keys, $this->_notVoidKeys);
        $params = array();
        foreach($args as $k => $v)
        {
            if(in_array($k, $keys))
            {
                $params[$k] = $v;
            }
        }
        return $params;
    }

    /**
     * xss过滤
     * 
     * @param mixed $value 
     * @access public
     * @return void
     * @author lihao <lihao@chukong-inc.com> 
     */
    public function clearXss($value)
    {
        $value = is_array($value) ? array_map(array($this, 'clearXss'), $value) : stripslashes(htmlspecialchars($value, ENT_QUOTES));
        return $value;
    }

    /**
     * 如果 data 不存在 keys 中key对应的数据则 抛出错误
     * 
     * @param mixed $keys 
     * @param mixed $data 
     * @access public
     * @return void
     * @author lihao <lihao@chukong-inc.com> 
     */
    public function checkVoid($keys, $data)
    {
        $keys = is_array($keys) ? $keys : array();
        $data = is_array($data) ? $data : array();
        foreach($keys as $key)
        {
            $this->assert(isset($data[$key]) && (!empty($data[$key]) || $data[$key] === 0), '-1002', '字段 '.$key.' 不允许为空值');
        }
    }

    /**
     * 根据用户名 设置用户对应的pin码
     * 
     * @param mixed $username 
     * @access private
     * @return void
     * @author lihao <lihao@chukong-inc.com> 
     */
    private function setPinByUsername($username)
    {
        //断言
        $username = rawurldecode($username);
        $this->assert(isset($this->_pinCodeMap[$username]), '-1001', '帐号: '.$username.' 没有权限,请联系PunchBox管理员申请权限！');
        $info = $this->_pinCodeMap[$username];
        $this->_setPinCode($info['pin']);
        $this->userID = $info['id'];
        $this->groupID = $info['groupId'];
        return $this->userID;
    }

    /**
     * 断言 测试表达式如果非 TRUE 抛出错误 
     * 
     * @param mixed $exp 表达式
     * @param string $errno 错误代号
     * @param string $error 错误信息
     * @access public
     * @return void
     * @author lihao <lihao@chukong-inc.com> 
     */
    public function assert($exp, $errno = '-1', $error = '')
    {
        if(!$exp)
        {
            $this->showResult($errno, $error);
        }
    }

    /**
     * 设置接口的授权码 
     * 
     * @param string<32> $pin 接口授权码
     * @access private
     * @return void
     * @author lihao <lihao@chukong-inc.com> 
     */
    private function _setPinCode($pin)
    {
        $this->_pinCode = $pin;
    }

    /**
     * 设置debug信息
     * 
     * @param Boolean $debug 
     * @access public
     * @return void
     * @author lihao <lihao@chukong-inc.com> 
     */
    public function setDebug($debug = false)
    {
        $this->_debug = $debug;
    }

}

<?php 

class MyFormValidation extends CI_Form_validation
{
	
	private $error = array();
	
	public function get_error($field = '')
	{
		
		if(!empty($field))
		{
			return error($field, $prefix, $suffix);
		}
		else
		{
			if( !empty($this->_field_data) )
			{
				foreach($this->_field_data as $k=>$v)
				{
					if(!empty($v['error']))
					$this->error[$k] = $v['error'];
				}
			
			}
			
			return $this->error;
		}
		
	}
	
	/**
	 * 添加一个自定义错误，只有在调用get_error函数的时候才能获取到这条错误消息
	 * @param unknown_type $field
	 * @param unknown_type $message
	 * @return null
	 */
	public function add_error($field,$message)
	{
		$this->error[$field] = $message;
	}
	
}
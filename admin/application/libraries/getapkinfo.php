<?php
/**
 *
 * 远程获取apk信息类
 * @company  chukong
 * @des 需要安装http扩展
 * @author why
 *
 */
require('apkparser.php');
class  GetApkInfo{

    public function __construct(){}

    private function httpRequet( $apkurl , $http_header ){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $apkurl);
        curl_setopt($ch, CURLOPT_RANGE, $http_header['range']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20000);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept: */*',
            'User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)',
            'Connection: Keep-Alive'));
        $section = curl_exec($ch);
        curl_close($ch);
        return $section ;
    }


    public function getApkInfoXml( $apkurl ){
        $pkgHeader = get_header($apkurl);
        if(!isset($pkgHeader['Content-Length']) || empty($pkgHeader['Content-Length']))
        {
            return ;
        }
        $fileLength = $pkgHeader['Content-Length'];
         
        $http_header = array (
		        "range" => ($fileLength-200).'-'.$fileLength  
        );
        if(isset($pkgHeader['Location']) && !empty($pkgHeader['Location']))
        {
            $apkurl = $pkgHeader['Location'];
        }
        $resection    = self::httpRequet( $apkurl , $http_header ) ;
        $str = '';
        for($i = 0;$i<200;$i++)
        {
            if(isset($resection[$i])) {
                $str .= bin2hex($resection[$i]);
            }
        }
        if(strpos($str, '504b0506') === false)
        {
            return false;
        }
        $str = strstr($str,'504b0506');
        $str = substr($str,0, 44);
        $section = null;
        for ($a=0; $a<strlen($str); $a+=2)
        {
            $section.=chr(hexdec($str{$a}.$str{($a+1)}));
        }
        $filenum    = hexdec( bin2hex( $section[9] ).bin2hex( $section[8] ) ) ;//目录文件数
        $dirsize    = hexdec( bin2hex( $section[15] ).bin2hex( $section[14] ).bin2hex( $section[13] ).bin2hex( $section[12] ) ) ;//目录区域大小
        $dirsoffset = hexdec( bin2hex($section[19] ).bin2hex( $section[18] ).bin2hex( $section[17] ).bin2hex( $section[16] ) ) ;//目录区域相对于整个文件的偏移
        $range 		= $dirsoffset.'-'.( $dirsize + $dirsoffset )  ;

        $http_header = array (
		        "range" => $range 
        );

        $section         = self::httpRequet( $apkurl , $http_header ) ;
        $offset          = 0  ;
        $compressed_len  = 0  ;
        $orginal_len     = 0  ;
        $dst_file_offset = 0  ;
        $dst_extra_len   = 0  ;
        for( $i=0 ; $i < $filenum ; $i++ ){
            $namelen     = hexdec( bin2hex( $section[$offset + 29] ).bin2hex( $section[$offset + 28] ) ) ; //文件名长度
            $extra_len   = hexdec( bin2hex( $section[$offset + 31] ).bin2hex( $section[$offset + 30] ) ) ; //扩展域长度
            $comment_len = hexdec( bin2hex( $section[$offset + 33] ).bin2hex( $section[$offset + 32] ) ) ; //注释长度
            $thum 		 = '' ;

            for ( $j=0 ; $j < $namelen ; $j++ ){
                $k    = $offset + 46 + $j ;
                $thum = $thum.$section[$k];  //拼接的文件名
            }

            if ($thum == 'AndroidManifest.xml') {
                $compressed_len  = hexdec( bin2hex( $section[$offset + 23] ).bin2hex( $section[$offset + 22] ).bin2hex( $section[$offset + 21] ).bin2hex( $section[$offset + 20] ) ) ;//压缩后的大小
                $orginal_len     = hexdec( bin2hex( $section[$offset + 27] ).bin2hex( $section[$offset + 26] ).bin2hex( $section[$offset + 25] ).bin2hex( $section[$offset + 24] ) ) ;//原始大小
                $dst_file_offset = hexdec( bin2hex( $section[$offset + 45] ).bin2hex( $section[$offset + 44] ).bin2hex( $section[$offset + 43] ).bin2hex( $section[$offset + 42] ) ) ;//目标文件在整个文件的偏移位置
                $dst_extra_len   = $extra_len; //扩展的长度
                break;
            }
            $offset += (46 + $namelen + $extra_len + $comment_len);//累加单个目录文件的长度
        }


        $range = ($dst_file_offset + 30 + 19 + $dst_extra_len).'-'.($compressed_len+$dst_file_offset + 30 + 19 + $dst_extra_len)  ;
        $http_header = array (
		        "range" => $range 
        );
        $section         = self::httpRequet( $apkurl , $http_header ) ;
        $section 		 =	gzinflate( $section ) ;

        return $section ;

    }

    public static  function getApkInfo( $apkurl ){
        $section = @self::getApkInfoXml($apkurl) ;
        $p		 = new ApkParser();
        $p -> parseString( $section );
        $xml =  @$p->getXML();

        if(preg_match('/<manifest(.*)?versionCode=\"(.*?)\"(.*)?versionName=\"(.*?)\"(.*)?package=\"(.*?)\"/', $xml,$m)){
            $versioncode  = trim($m[2]);
            $version  = trim($m[4]);
            $packname = trim($m[6]);
            $result = array('error'=>0,'package'=>$packname,'ver'=>$version,'code'=>$versioncode);
        }else{
            $result = array('error'=>'解析失败') ;
        }

        return  json_encode($result) ;
    }

}
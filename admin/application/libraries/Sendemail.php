<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 邮件发送类
 *
 * @brief 辅助类
 * @author 王栋
 * @date 2012.03.09
 * @note 详细说明及修改日志
 */
class Sendemail
{
    /**
     * 管理用户发邮件
     *
     * @param $userId
     * @param $key
     * @access public
     * @return page
     */
    function send_income_mail($userName='',$title='尊敬的开发者',$msg=''){
        if(empty($userName)||e)
        {
            return false;
        }
        $static_url = site_url();
        $msg = '<table border="0" cellspacing="0" cellpadding="0" width="100%" style="background:#f5f5f5; padding: 20px 0;">
        <tbody>
            <tr>
                <td>
                    <table border="0" cellspacing="0" cellpadding="0" width="750" style="margin:0 auto; font-size:14px; margin:0 auto; padding-bottom: 20px; border:1px solid #ccc; background: #fff;">
                        <tbody>
                            <tr>
                                <td>
                                    <br>
                                    <strong style="padding:0 25px; font-size:16px;">'.$title.'</strong>
                                    <p style="padding:0 25px; line-height:24px; color:#48cfad; font-size: 24px;">尊敬的开发者</p>
                                    <p style="padding:0 25px; line-height:24px;">触控广告平台基于对大数据的分析和不断优化的投放策略，为应用开发者提供可观的广告收益和推广效果，帮助各类广告主实现市场活动。触控广告平台，您最得力的助手。</p>

                                     
                                    <p style="padding:0 25px; line-height:18px;">
                                         '.$msg.'
                                    </p>

                                    <p style="padding:0 25px; line-height:24px;">如果您还有其他问题,我们的工作人员一直在此恭候您的到来：</p>
                                    <p style="padding:0 25px; line-height:24px;"><a href="'.$static_url.'" style="display:block; width:100px; border-radius: 3px; text-decoration: none; padding: 6px 0px; text-align: center; color: #ffffff; background: #2b99f8;">联系我们</a></p>
                                    <p style="padding:0 25px; line-height:24px; text-align: right;">触控广告平台&nbsp;&nbsp;敬上</p>
                                    <div style="width:700px; border-top:1px solid #ccc; font-size:12px; color: #999; margin:0 auto;">
                                        Copyright © 2012-2013 www.cocounion.com.
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>';
			$this->mailSend($userName,$title,$msg);
    }
 
    /**
     * 发送邮件
     *
     * @param $userName
     * @param $title
     * @param $msg
     * @access public
     * @return page
     */
    function mailSend($userName='',$title='',$msg=''){
        if(empty($userName))
        {
            return false;
        }
        //		$title = "=?UTF-8?B?".base64_encode($title)."?=";
        //
        //		$cmd = "/usr/local/bin/sendEmail -f 'service@punchobx.org' -t '$userName' -u '$title' -o message-content-type=html -m '".$msg."' -o message-charset=UTF-8 >/dev/null";
        //if ( TRUE == $ERROR)
        //system($cmd);
        //$title = "=?UTF-8?B?".base64_encode($title)."?=";
        $CI =& get_instance();
        $CI->load->library('email');
        $CI->email->initialize();
        $CI->email->from('message@cocounion.com', '触控广告平台');
        $CI->email->to($userName);
        $CI->email->reply_to('message@cocounion.com', '触控广告平台');
        $CI->email->subject($title);
        $CI->email->message($msg);
        if ( ! $CI->email->send())
        {
//            print_r($CI->email->print_debugger());
//            exit;
            //            $CI->load->model('adminv2/EmailLogClass');
            //            $data = array(
            //				'objtype' => 2,
            //				'add_time' => time(),
            //				'status' => 0,
            //				'title' => $title,
            //				'contents' => $msg,
            //				'email' => $userName,
            //				'status' => 1,
            //            );
            //            print_r($data);
            //            $CI->EmailLogClass->add($data);
            //            return false;
        }
        return true;
    }
 
}
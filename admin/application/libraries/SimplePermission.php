<?php

class SimplePermission
{

	private $config = array();

	private $action = array();


	public function SimplePermission()
	{

	}


	/**
	 * 设置权限
	 * @param string $action 代表一个功能的字符串，用户可自己定义
	 * @param bool $isAllowed 允许$permission用户有访问权为true，不允许则为false
	 * @param unknown_type $permission 形如：array(userid_1,userid_2,....)
	 * @return bool
	 */
	public function setConfig($action,$isAllowed=true,$permission)
	{
		if(empty($permission))
		{
			return false;
		}

		$permissionSymbol = $isAllowed?'allow':'deny';

		$arr = $this->config[$action][$permissionSymbol] = isset($this->config[$action][$permissionSymbol])?$this->config[$action][$permissionSymbol]:array();


		if(is_string($permission) and !in_array($permission,$arr))
		{
			array_push($this->config[$action][$permissionSymbol],$permission);
			return true;
		}

		$this->config[$action][$permissionSymbol] = @array_merge($arr,$permission);
		return true;
	}

	public function getConfig($action,$isAllowed)
	{
		$permissionSymbol = $isAllowed?'allow':'deny';

		return isset($this->config[$action][$permissionSymbol])?$this->config[$action][$permissionSymbol]:null;
	}

	/**
	 * 获取一个用户是否对指定的action有访问权，跟
	 * @param string $action
	 * @param int $userid
	 * @return bool 
	 */
	public function isAllowed($action,$userid)
	{
		$allowed = $this->getAllowed($action);
		return in_array($userid,$allowed);
	}

	/**
	 * 获取一个用户是否对制定的action没有访问权 ，跟isAllowed正好相反
	 * @param string $action
	 * @param int $userid
	 * @return bool 没有返回 true 有返回false
	 */
	public function isDeny($action,$userid)
	{
		$deny = $this->getDeny($action);
		return in_array($userid,$deny);
	}


	public function getDeny($action)
	{
		return isset($this->config[$action]['deny'])?$this->config[$action]['deny']:array();
	}

	public function getAllowed($action)
	{
		return (isset($this->config[$action]['allow'])?$this->config[$action]['allow']:array());
	}

	/**
	 * 获取所有的功能项列表
	 * @return array 形如：array('fee','targeting')
	 */
	public function getAllAction()
	{
		if(!empty($this->config)){

			foreach($this->config as $action=>$v)
			{
				$this->action[] = $action;
			}
		}
		return $this->action;

	}

	/**
	 * 获取一个用户所有的功能项的访问权
	 * @param unknown_type $userid
	 * @return array 形如：array('fee'=>true,'retargeting'=>false)
	 */
	public function getAllAllowed($userid)
	{
		$arrAction = $this->getAllAction();
		
		$return = array();
		if(!empty($arrAction))
		{
			foreach($arrAction as $k=>$act)
			{
				$return[$act] = $this->isAllowed($act,$userid);
			}
		}
		return $return;
		
		
	}



}
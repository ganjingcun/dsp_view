<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page
{
	var $ci;
	function __construct()
	{
		$this->ci = & get_instance();
		$this->ci->load->library('pagination');
	}
	/**
	 * 分页
	 * @param $base_url   连接地址
	 * @param $total_rows 记录总数
	 * @param $per_page   每页显示记录数
	 * @param $uri_segment 连接中当前页码的位置。http://域名/admin/admin/index/页码
	 * @return 分页字符串
	 */
	function page_show($base_url,$total_rows,$per_page,$uri_segment=4)
	{
		$config['base_url'] 		= $base_url;
		$config['total_rows'] 		= $total_rows;
		$config['per_page'] 		= $per_page;
		$config['uri_segment'] 		= $uri_segment;
		$config['use_page_numbers']  = TRUE;
        $config['full_tag_open'] ='<div class="pagination"><ul>';
        $config['full_tag_close'] ='</ul></div>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] ='<li class="active"><a>';
        $config['cur_tag_close'] ='</a></li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
		$config ['first_link'] = '首页'; // 第一页显示
		$config ['last_link'] = '末页'; // 最后一页显示  
		$config ['next_link'] = '下一页 >'; // 下一页显示  
		$config ['prev_link'] = '< 上一页'; // 上一页显示
		$config ['page_query_string'] = true; 
		$this->ci->pagination->initialize($config);
		return $this->ci->pagination->create_links();
	}
}
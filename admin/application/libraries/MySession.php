<?php

class MySession
{
	private $prefix = '';
	private $session = '';
	private $is_saved_db = true;

	private $session_id = '';
	
	private $session_life = 15;
	private $current_time = 0;



	function __construct($prefix='default')
	{
		$this->prefix = $prefix;

		$this->session_id = 0;
		$this->current_time = time();

		if($this->is_saved_db)
		{
			$this->sess_open();
		}
		else
		{
			@session_start();
			$_SESSION[$prefix] = isset($_SESSION[$prefix])?$_SESSION[$prefix]:array();
		}


	}
	public function set($key,$val)
	{
		if($this->is_saved_db)
		{
			$data = $this->sess_read();
			$data[$this->prefix][$key] = $val;
			return $this->sess_write($data);
		}


		$_SESSION[$this->prefix][$key] = $val;
		return true;

	}

	public function get($key=null)
	{
		if($this->is_saved_db)
		{
			$data = $this->sess_read();
			if( !$key )
			{
				return $data[$this->prefix];
			}
				
			if( isset($data[$this->prefix][$key]) )
			{
				return $data[$this->prefix][$key];
			}
			
			return false;
		}
		
		
		
		if( !$key )
		{
			return $_SESSION[$this->prefix];
		}

		if( isset($_SESSION[$this->prefix][$key]) )
		{
			return $_SESSION[$this->prefix][$key];
		}

	}


	public function clear($key=null)
	{
		if($this->is_saved_db)
		{
			$data = $this->sess_read();
			if( !$key )
			{
				$data[$this->prefix] = '';
			}
			
			if( isset($data[$this->prefix][$key]) )
			{
				unset($data[$this->prefix][$key]);
			}
			
			return $this->sess_write($data);
		}
		
		

		if( !empty($key) )
		{
			$_SESSION[$this->prefix][$key] = null;
		}
		else
		{
			$_SESSION[$this->prefix] = null;
		}


	}




	private function sess_open()
	{
		$CI = CI_Controller::get_instance();
		$CI->load->helper('tools');
		$this->DB = DB();

		$this->session_id = getSessionId();
		
		$config = &get_config();
		$this->session_life = $config['sess_expiration'];
		
		return true;
	}

	private function sess_close()
	{
		return true;
	}

	public static function sess_gc()
	{
		$CI = CI_Controller::get_instance();
		$CI->load->helper('tools');
		
		
		$now = time();
		$sql = "DELETE FROM db_session WHERE expiry<'{$now}' ";
		return DB()->prepare($sql)->execute();
	}
	
	/**
	 * session更新
	 * @return bool
	 */
	public static function sess_update()
	{
		$sessionid 	= getSessionId();
		if( empty($sessionid) ) return false;
		
		$config 	= &get_config();
		$session_life = $config['sess_expiration'];
		
		$time 		= time();
		$expiry 	= $time+$session_life;
		
		$db = DB();
		$sql = "UPDATE db_session SET expiry='{$expiry}' WHERE sessionid='{$sessionid}' and expiry>'{$time}'";	
		$db->prepare($sql)->execute();
		return true;
	}


	
	private function sess_read()
	{
		if( empty($this->session_id) ) return false;
		
		$key = $this->session_id;
		$data = $this->DB->query("SELECT sessiondata FROM db_session WHERE sessionid='{$key}' and expiry>'{$this->current_time}'")->query_getsingle();
		if(isset($data['sessiondata']) and !empty($data['sessiondata']))
		{
			return json_decode($data['sessiondata'],true);
		}
	}

	private function sess_write(array $value)
	{
		if( empty($this->session_id) ) return false;
		
		$key = $this->session_id;
		$session_data = json_encode($value);
		$expiry = $this->current_time+$this->session_life;
		$sql = "INSERT INTO `db_session` (`sessionid`, `expiry`, `sessiondata`) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE expiry=?,sessiondata=? ";
		
		$this->DB->prepare($sql);
		$this->DB->bind(1,$key);
		$this->DB->bind(2,$expiry);
		$this->DB->bind(3,$session_data);
		$this->DB->bind(4,$expiry);
		$this->DB->bind(5,$session_data);
		
		return $this->DB->execute();
	}
	

}




<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Position 
{
	var $ci;
	function __construct()
	{
		$this->ci = & get_instance();
	}
	function get_position()
	{
		if($this->ci->uri->total_segments() > 3)
		{
			$uri = implode('/',array_slice($this->ci->uri->segment_array(),0,3));
		}else{
			$uri = $this->ci->uri->uri_string();
		}

		foreach($this->ci->admin_config['nav'] as $key=>$value)
		{
				if($key !== 0){
					foreach($value as $_key=>$val)
					{
						if($val[1] == $uri)
						{
							$data['position']=array($key,$val[0]);
						}
					}
				}
		}
		if(empty($data))
		{
			$data = array();
		}
		//取消tips加载 
		//2012-12-04 王栋
		//$data['tips'] = $this->get_tips();
		return $this->ci->load->view('admin/include/position',$data,TRUE);
	}

	function get_tips()
	{
		$this->ci->load->config('ciccms/tips');
		$tips_config = $this->ci->config->item('tips');
		@$tips = $tips_config[$this->ci->uri->rsegment(1)][$this->ci->uri->rsegment(2)];
		return @$tips[array_rand($tips)];
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class NewSendEmail {
 
    var $mail;
 	var $send_to;
    public function __construct()
    {
        require_once('PHPMailer/class.phpmailer.php');
        include_once dirname(FCPATH).'/config/email.php';

        // the true param means it will throw exceptions on errors, which we need to catch
        $this->mail = new PHPMailer(true);
 
        $this->mail->IsSMTP(); // telling the class to use SMTP
 
        $this->mail->CharSet = "UTF-8";                  // 一定要設定 CharSet 才能正確處理中文
        $this->mail->SMTPDebug  = 1;                     // enables SMTP debug information
        $this->mail->SMTPAuth   = true;                  // enable SMTP authentication
        //$this->mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
        $this->mail->IsHTML(true); 
        $this->mail->Host       = $config['smtp_host'];//"smtp.exmail.qq.com";      // sets GMAIL as the SMTP server
        $this->mail->Port       = $config['smtp_port'];                   // set the SMTP port for the GMAIL server
        $this->mail->Username   = $config['smtp_user'];//"ad.services@chukong-inc.com";// GMAIL username
        $this->mail->Password   = $config['smtp_pass'];//"qwe123!@#";       // GMAIL password
        $this->mail->AddReplyTo($config['smtp_user'],'管理员');
        $this->mail->SetFrom($config['smtp_user'], '管理员');
        $this->send_to	=$config['channel_switch_to']; 
    }
 
    public function sendmail($to, $subject, $body){
        try{
            $this->mail->AddAddress($to);
 
            $this->mail->Subject = $subject;
            $this->mail->Body    = $body; 
            $this->mail->Send();
 
        } catch (phpmailerException $e) {
            echo $e->errorMessage(); //Pretty error messages from PHPMailer
        } catch (Exception $e) {
            echo $e->getMessage(); //Boring error messages from anything else!
        }
    }
}
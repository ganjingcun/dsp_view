<?php
/** 
 * cypt & check password 
 * author: zweiwei 
 * email: lnnujxxy@gmail.com 
 * date: 2012/01/30 
 */  
class PwdSecurity {  
    private static $defaultSalt = '9de3ugYWM(9hpo!ux$.3^^=wj8SsvjWZ-UIm-1L*!OsN6rmJ@p!VmueW)E';
      
    public static function cryptPassword($password, $uid=null) {  
        //self::isVaildPassword($password);
        
        $salt = self::generateSalt($uid);
        return md5(sha1($salt.$password));
          
    }
      
    public static function checkPassword($cryptPassword, $password, $uid=null) {  
        if(strlen($cryptPassword) !== 32) {  
            throw new Exception("cryptPassword :".$cryptPassword." length is wrong!");
        }
        self::isVaildPassword($password);
          
        $salt = self::generateSalt($uid);
        if(md5(sha1($salt.$password)) === $cryptPassword) {  
            return true;
        }
        return false;
    }
      
    private static function generateSalt($uid=null) {  
        $md5Str = is_null($uid) ? md5($uid) : md5(self::$defaultSalt);
        return substr($md5Str, 8, 16);
    }
      
    private static function isVaildPassword($password) {  
        if(!$password || strlen($password) < 8) {  
            throw new Exception("password :".$password." must be longer than 8");
        }
        // contain ~!@#$%^&*  
        if(!preg_match('/[~!@#$%^&]/', $password)) {
            throw new Exception("password :".$password." must contain special characters(~!@#$%^&)");
        }
    }
}
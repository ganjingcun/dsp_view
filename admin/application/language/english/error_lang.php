<?php
$lang['token_no_match']				 = "10000";//token验证失败
$lang['sign_no_match']				 = "10001";//签名不匹配
$lang['jsondata_empty']              = "10002";//jsondata数据为空
$lang['params_no_exists']			 = "10003";//jsondata有必需参数不存在

$lang['email_error']				 ="10004";//邮箱验证错误
$lang['username_exists']			 ="10005";//通行证账号已经存在
$lang['register_fail']				 ="10006";//通行证注册失败
$lang['username_no_exists']			 ="10007";//通行证账号不存在
$lang['username_password_error']	 ="10008";//通行证密码错误
$lang['username_no_activate']		 ="10009";//通行证还没有激活
$lang['username_forbidden']			 ="10010";//通行证已被禁止访问
$lang['user_login_exprie']			 ="10011";//通行证登陆已过期
$lang['login_token_error']			 ="10012";//通行证登陆token错误
$lang['username_email_exists']		 ="10013";//通行证邮箱已经绑定
$lang['email_binding']				 ="10014";//邮箱已经被其他账号绑定
$lang['send_email_fail']			 ="10015";//找回密码的邮件发送失败
$lang['app_game_error']              ="10017";//游戏与appid映射错误
$lang['file_not_exists']             ="10018";//文件不存在
$lang['file_md5_no_match']           ="10019";//文件md5值不符
$lang['upload_file_error']           ="10020";//上传文件失败
$lang['chat_error']                  ="10021";//不能聊天
$lang['data_exist']                  ="10022";//数据已经存在

$lang['data_not_exist']				 ='10033';//数据不存在 存储数据的文件不存在
$lang['data_delete_error']			 ='10034';//数据删除错误 删除数据文件的时候，出现异常，删除失败

$lang['username_no_email']			 ='10035';//用户名不是邮箱，或邮箱格式不正确
$lang['username_len_limit']			 ='10036';//用户名字符长度超过限制 @前30个字符

$lang['data_save_error']			 ='10037';//数据添加/更新异常，操作失败

$lang['username_exist_chinese']		 ='10038';//用户名存在中文

$lang['password_error']			     ='10039';//密码格式不正确
$lang['file_not_found']              ='10040';//找不到文件

$lang['user_login_out']			     ='10041';//通行证未登陆
$lang['user_login_popup']            ='10042';//通行证被强制退出

$lang['email_exists']				 ="10060";  //邮箱已存在
$lang['nickname_error']				 ="10061";  //昵称格式错误
$lang['nickname_exists']			 ="10062";  //昵称已存在
$lang['username_email_not_exsit']    ="10063";   //邮件不存在
$lang['two_password_not_same']       ="10064";   //两次输入密码不同

$lang['update_userinfo_error']		 ="10065";  //修改用户信息失败
$lang['set_avatar_error']			 ="10066";  //头像设置失败
$lang['del_album_error']			 ="10067";  //删除相册失败
$lang['album_not_del']				 ="10068";  //头像不能删除

$lang['addattention_failed']         = "10069";   //添加关注失败
$lang['exist_blacklist']			 = "10070";   //在黑名单里
$lang['delattention_failed']         = "10071";   //删除关注失败
$lang['addblacklist_failed']         = "10072";   //添加黑名单失败
$lang['delblacklist_failed']         = "10073";   //删除黑名单失败
$lang['attention_exists']         	 = "10074";   //已经添加关注
$lang['blacklist_exists']         	 = "10075";   //已经添加黑名单

$lang['title_empty']         		 = "10076";   //标题不能为空
$lang['subject_empty']         		 = "10077";   //内容不能为空
$lang['addTheme_failed']         	 = "10078";   //添加话题失败
$lang['delTheme_failed']         	 = "10079";   //删除话题失败
$lang['addReply_failed']         	 = "10080";   //添加回复失败
$lang['delReply_failed']         	 = "10081";   //删除回复失败
$lang['addComment_failed']         	 = "10082";   //添加评论失败
$lang['delComment_failed']         	 = "10083";   //删除评论失败
$lang['addAttention_failed']         = "10084";   //添加话题关注失败
$lang['delAttention_failed']         = "10085";   //删除话题关注失败
$lang['attentioned']         	     = "10086";   //你已经关注此话题
$lang['character_error']         	 = "10087";   //含有特殊字符
$lang['title_length_error']          = "10088";   //话题标题应该小于30个字符
$lang['subject_length_error']        = "10089";   //话题内容应该小于等于140个字符

$lang['top_query_game_failed']       = "10090";   //榜单[捕鱼榜]获取失败
$lang['top_query_hot_failed']        = "10091";   //榜单[人气榜]获取失败
$lang['top_query_battle_failed']     = "10092";   //榜单[对战榜]获取失败
$lang['top_query_app_failed']        = "10093";   //榜单[推荐榜]获取失败
$lang['top_commit_failed']           = "10094";   //提交榜单成绩失败
$lang['top_query_from_game_failed']  = "10095";   //从游戏中获取榜单失败

$lang['theme_num_overflow']  		 = "10096";   //发布的话题数超过后台限制
$lang['theme_num_everyday_overflow'] = "10097";   //每天发布的话题数超过后台限制
$lang['user_id_empty'] 				 = "10098";   //用户id为空
$lang['id_illegal'] 				 = "10099";   //id非法
$lang['theme_type_empty'] 		     = "10100";   //话题类型为空
$lang['tid_empty'] 		     		 = "10101";   //话题id为空
$lang['rid_empty'] 		     		 = "10102";   //回复id为空
$lang['addattention_friend_overflow']= "10103";   //添加关注超过限制了
$lang['had_joined']					 = "10104";   //已经加入话题
$lang['applyTheme_failed']			 = "10105";   //申请失败
$lang['apply_yourself_error']		 = "10105";   //不能申请自己的话题
$lang['joinin_secret_theme_error']	 = "10106";   //无权限进入私密话题

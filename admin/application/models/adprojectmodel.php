<?php
class AdprojectModel extends MY_Model
{
    protected  $table = "ad_project";

    function __construct() {
        parent::__construct();
        $this->load->model('CaseManageLogModel');
        $this->load->model('AdprojectCampaignModel');
        $this->load->model('AdprojectDaylevelModel');
        $this->load->model("AdCampaignModel");
    }

    /**
     * 添加订单信息
     */
    function doAdd($params)
    {
    	
        $params['starttime'] = strtotime($params['starttime'] . ' ' . '00:00:00');
        $params['endtime'] = strtotime($params['endtime'] . ' ' . '00:00:00');
        $chk = $this->chkAddInfo($params);
        if($chk['status'] == 0)
        {
            return $chk;
        }
        $data = $params;
        $casetype = $params['casetype'];
        if($casetype == 1){
        	$data['daylevel'] = $params['daylevel'] == '' ? '不限' : $params['daylevel'];	
        }
        else{
        	unset($data['daylevel']);
        }
        
        $data['price'] = formatmoney($params['price'], 'set');
        $data['createtime'] = time();
        $admin = $this->session->userdata("admin");
        $data['adminid'] = $admin['admin_id'];
        $data['adstatus'] = 0;
		unset($data['adcampaignlist']);
        $projectid = $this->add($data);
        if($projectid)
        {
        	if($params['casetype'] == 1){
	        	$adcampaignlist = $params['adcampaignlist'];//json_decode($params['adcampaignlist']);
	        	foreach ($adcampaignlist as $item){
	        		$campaignname=$this->AdCampaignModel->getRow(array('campaignid'=>$item),"campaignname","");
	        		$adprojectcampaign = array('projectid'=>$projectid,'campaignid'=>$item,'campaignname'=>$campaignname['campaignname'],'status'=>0);
	        		$this->AdprojectCampaignModel->add($adprojectcampaign);
	        	}	
        	}
        	elseif($params['casetype'] == 2){
	        	$daylevel = $params['daylevel'];
	        	$eachday = explode("|",$daylevel);
	        	if($eachday){
	        		foreach ($eachday as $item){
	        			$myday = explode(':',$item);
	        			if($item && count($myday)>1){
		        			$insertmydaylevel = array('projectid'=>$projectid,'casedate'=>$myday[0]?$myday[0]:"",'daylevel'=>$myday[1]?$myday[1]:'不限','createtime'=>time(),'status'=>0);
			        		$this->AdprojectDaylevelModel->add($insertmydaylevel);
	        			}
	        			
	        		}
	        	}
	        	//添加外放，如果该外方有相关id，则修改该外方的内投的relateid（拼接字符串，逗号分割），如果没有，则不处理。
	        	$newrelateid = $data['relateid'];
	        	if($newrelateid != '0'){
					$newrelateinfo = $this->getRow(array('projectid'=>$newrelateid,'casetype'=>1));
					if(isset($newrelateinfo)){
						$editrelateid = $newrelateinfo['relateid'] == 0?$projectid:$newrelateinfo['relateid'].",".$projectid;
						$addid = $this->edit(array('projectid'=>$newrelateid), array('relateid'=>$editrelateid));
						if($addid <= 0){
		        			return array('status' => 0, 'data'=>array('', '修改失败！'), 'info' => '修改失败！');//A
		        		}
					}
				}
        	}
            return array('status' => 1);
        }
        return array('status' => 0, 'data'=>array('', '未知错误！'), 'info' => '未知错误！');
    }

    /**
     * 编辑订单信息
     */
    function doEdit($where, $params)
    {
 		$params['starttime'] = strtotime($params['starttime'] . ' ' . '00:00:00');
        $params['endtime'] = strtotime($params['endtime'] . ' ' . '00:00:00');
        $chk = $this->chkAddInfo($params);
        if($chk['status'] == 0)
        {
            return $chk;
        }
        $historyData = $this->getRow($where);
        if(empty($historyData))
        {
            return array('status' => 0, 'data'=>array('', '未知错误！'), 'info' => '未知错误！');
        }
        
        
        $data = $params;
        $casetype = $params['casetype'];
    	if($casetype == 1){
        	$data['daylevel'] = $params['daylevel'] == '' ? '不限' : $params['daylevel'];	
        }
        else{
        	unset($data['daylevel']);
        }
        $data['price'] = formatmoney($params['price'], 'set');
        $data['edittime'] = time();
        $admin = $this->session->userdata("admin");
        $data['adminid'] = $admin['admin_id'];
        unset($data['adcampaignlist']);
        
        $admin = $this->session->userdata("admin");
        $this->AdProjectLogModel->add(array('data'=>json_encode($historyData), 'adminid'=>$admin['admin_id'], 'createtime'=>time()));
        
        $num = $this->edit($where, $data);
        $addid=0;
		$projectid = $where['projectid'];
		$newrelateid = $data['relateid'];
		$historyrelateid = $historyData['relateid']?$historyData['relateid']:'0';
    	if($casetype == 1)
        {
        	if($params['adcampaignlist']){
        		$list = $this->AdprojectCampaignModel->getList($where);
        		$this->AdProjectLogModel->add(array('data'=>json_encode($list), 'adminid'=>$admin['admin_id'], 'createtime'=>time()));
        		$deletestatus = $this->AdprojectCampaignModel->edit($where,array('status'=>1));
        		
	        	$adcampaignlist = $params['adcampaignlist'];
	        	
	        	foreach ($adcampaignlist as $item){
	        		$campaignname=$this->AdCampaignModel->getRow(array('campaignid'=>$item),"campaignname","");
	        		$adprojectcampaign = array('projectid'=>$where['projectid'],'campaignid'=>$item,'campaignname'=>$campaignname['campaignname'],'status'=>0);
	        		$addid = $this->AdprojectCampaignModel->add($adprojectcampaign);
	        		if($addid <= 0){
	        			return array('status' => 0, 'data'=>array('', '修改活动失败！'), 'info' => '修改活动失败！');
	        		}
	        	}
        	}
        }
        elseif($casetype == 2){
        	$list = $this->AdprojectDaylevelModel->getList($where);
        	$this->AdProjectLogModel->add(array('data'=>json_encode($list), 'adminid'=>$admin['admin_id'], 'createtime'=>time()));
        	
        	$deletestatus = $this->AdprojectDaylevelModel->edit($where,array('status'=>1));
        	$daylevel = $params['daylevel'];
        	$eachday = explode("|",$daylevel);
        	if($eachday){
        		foreach ($eachday as $item){
        			$myday = explode(':',$item);
        			if($item && count($myday)>1){
	        			$insertmydaylevel = array('projectid'=>$where['projectid'],'casedate'=>$myday[0]?$myday[0]:"",'daylevel'=>$myday[1]?$myday[1]:'不限','createtime'=>time(),'status'=>0);
	        			$addid = $this->AdprojectDaylevelModel->add($insertmydaylevel);
		        		if($addid <= 0){
		        			return array('status' => 0, 'data'=>array('', '修改每日发生量失败！'), 'info' => '修改每日发生量失败！');
		        		}
        			}
        		}
        	}
        }
        if($num || $addid > 0)
        {
            return array('status' => 1, 'data' => '修改成功！');
        }
        return array('status' => 0, 'data'=>array('', '未做任何修改！'), 'info' => '未做任何修改！');
    }

    /**
     *
     */
    function chkTime($data, $historyData, $casetype)
    {
        if($casetype == 1)
        {
            $dataList = $this->getList(array('projectid'=>$data['projectid'],'status'));
        }
        elseif($casetype == 2)
        {
            $dataList = $this->getList(array('projectid'=>$data['projectid']));
        }
        foreach($dataList as $v)
        {
            if($v['projectid'] == $historyData['projectid'])
            {
                continue;
            }
            if(($data['starttime'] >= $v['starttime'] && $data['starttime'] <= $v['endtime']) )
            {
                return array('status' => 0, 'data'=>array('', '设定的开始时间与现有订单冲突！'), 'info' => '设定的开始时间与现有订单冲突！');
            }
            if(($data['endtime']>=$v['starttime'] && $data['endtime'] <= $v['endtime']))
            {
                return array('status' => 0, 'data'=>array('', '设定的结束时间与现有订单冲突！'), 'info' => '设定的结束时间与现有订单冲突！');
            }
            if(($data['starttime']<=$v['starttime'] && $data['endtime'] >= $v['endtime']))
            {
                return array('status' => 0, 'data'=>array('', '设定的结束时间与现有订单冲突！'), 'info' => '设定的结束时间与现有订单冲突！');
            }
            if( $historyData['id'] == 0 )
            {
                continue;
            }
        }
        return array('status' => 1);
    }

    /**
     * 检测信息
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    private function chkAddInfo($params)
    {
        $error = array('status' => 1, 'data' => '');
    	if(empty($params['projectname']))
        {
            $error['data'] = array('projectnamemsg', '项目名称不能为空。');
            $error['info'] = 'campaignlisterr';
            $error['status'] = 0;
            return $error;
        }
    	
    	if(empty($params['ostype']))
        {
            $error['data'] = array('ostypemsg', '所属平台不能为空。');
            $error['info'] = 'ostypeerr';
            $error['status'] = 0;
            return $error;
        }
   		
        if($params['casetype'] == 1)
        {
	        if(empty($params['operations']))
	        {
	            $error['data'] = array('operationsmsg', '运营接口人不能为空。');
	            $error['info'] = 'operationserr';
	            $error['status'] = 0;
	            return $error;
	        }
	        if(empty($params['adcampaignlist']))
	        {
	            $error['data'] = array('adcampaignlistmsg', '广告活动不能为空。');
	            $error['info'] = 'adcampaignlisterr';
	            $error['status'] = 0;
	            return $error;
	        }
            if(!is_numeric($params['platform']))
            {
                $error['data'] = array('platformmsg', '执行平台不能为空。');
                $error['info'] = 'platformerr';
                $error['status'] = 0;
                return $error;
            }
            if(empty($params['sales']))
            {
                $error['data'] = array('salesmsg', '销售负责人不能为空。');
                $error['info'] = 'saleserr';
                $error['status'] = 0;
                return $error;
            }
            if(empty($params['company']))
	        {
	            $error['data'] = array('companymsg', '接单公司全称不能为空。');
	            $error['info'] = 'companyerr';
	            $error['status'] = 0;
	            return $error;
	        }
        }
        elseif ($params['casetype'] == 2)
        {
        	if(empty($params['company']))
	        {
	            $error['data'] = array('companymsg', '外放公司全称不能为空。');
	            $error['info'] = 'companyerr';
	            $error['status'] = 0;
	            return $error;
	        }
        	if(empty($params['shortname']))
	        {
	            $error['data'] = array('shortnamemsg', '外放公司简称不能为空。');
	            $error['info'] = 'shortnameerr';
	            $error['status'] = 0;
	            return $error;
	        }
            if(empty($params['computation']))
            {
                $error['data'] = array('computationmsg', '记账方式不能为空。');
                $error['info'] = 'computationerr';
                $error['status'] = 0;
                return $error;
            }
        	if(empty($params['outers']))
            {
                $error['data'] = array('outersmsg', '外放执行人不能为空。');
                $error['info'] = 'outerserr';
                $error['status'] = 0;
                return $error;
            }
        }
        if($params['starttime'] > $params['endtime'])
        {
            $error['data'] = array('starttimemsg', '开始时间不能大于结束时间。');
            $error['info'] = 'starttimeerr';
            $error['status'] = 0;
            return $error;
        }
        if(!is_numeric($params['price']) || $params['price'] <= 0)
        {
            $error['data'] = array('pricemsg', '接单价格必须是大于0的数字。');
            $error['info'] = 'priceerr';
            $error['status'] = 0;
            return $error;
        }
              
        return $error;
    }
}
<?php
class AdSubChannelModel extends MY_Model
{
	protected  $table = "ad_sub_channel";

	function __construct() {
        parent::__construct();
    }

    function getAppCount($where){
        $sql = "SELECT count(1) as count FROM `{$this->table}` app_info WHERE 1 $where";
        $row = $this->DB->read()->query($sql)->query_getsingle();
        if(isset($row['count'])){
            return $row['count'];
        }
        return 0;
    }
    //媒体名称
    function getAppName(){
        $sql = "select distinct app_info.appid,appname from ad_sub_channel left join app_info on ad_sub_channel.appid=app_info.appid";
        return $this->DB->read()->query($sql)->query_getall();
    }
    //渠道
    function getpublisher(){
        $sql = "select distinct ad_sub_channel.publisherID,channelname from ad_sub_channel left join app_channel on ad_sub_channel.publisherID=app_channel.publisherID where ad_sub_channel.status=1";
        return $this->DB->read()->query($sql)->query_getall();
    }

    function getgameId(){
        $sql = "select distinct adgameid from ad_sub_channel";
        return $this->DB->read()->query($sql)->query_getall();
    }

    function getgameName($gameid_arr){
        $str = implode(',', $gameid_arr);
        $sql = "select id,name from ad_game_list where id in (".$str.")";
        return $this->DB->read()->query($sql)->query_getall();
    }
    
	 
}

?>
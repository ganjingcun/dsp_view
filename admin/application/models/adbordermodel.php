<?php
define('UPLOAD_BORDER_PATH', UPLOAD_DIR . '/stuff/border/img/' . date('Ym'));

class AdBorderModel extends MY_Model
{
    protected  $table = "ad_border";

    function __construct() {
        parent::__construct();
    }
    
    function getSysBorderList(){
    	$data = $this->getList(array('type'=>1));//, 'status'=>1
        if(empty($data))
        {
            return false;
        }
        return $data;
    }
    
	function getSysBorders(){
    	$data = $this->getList(array('type'=>1, 'status'=>1),'*','borderid','bordergroupid');
        if(empty($data))
        {
            return false;
        }
        return $data;
    }

	/**
     * 上传边框
     */
	public function upLoad($type) {
		$this->load->library('MyUpload', $_FILES['Filedata']);
		$upload = new MyUpload($_FILES['Filedata']);
		$upload->setUploadPath(UPLOAD_BORDER_PATH);
		
		$upload->setFileExt(array('jpg', 'png'));

		$upload->setMaxsize(120 * 1024);

		$strError = '';
		$data = array('isReceived'=>false,'imgInfo'=>array(),'errorMessage'=>array());
		if (!$upload->isAllowedTypes()) {
			$strError = '您上传的文件格式错误 !';
		} elseif ($upload->isBigerThanMaxSize()) {
			$strError = '此处的文件最大不能超过 ' . intval($upload->getMaxsize() / 1024) . 'KB';
		}
		
        $imgSize = explode("*", $type);
        $width = $imgSize[0];
        $height = $imgSize[1];
        
		if(!$upload->isAllowedSize($width, $height)){
			$strError .= " 图片的尺寸必须为：".$width." X ".$height;
		}

		if (empty($strError) and $upload->upload()) {
			$imgPath = $upload->getUplodedFilePath();
			$data['imgInfo']['newImg'] = $imgPath;
			$data['newImg'] = $imgPath;
			$data['isReceived'] = 1;
		} else {
			$data['errorMessage'] = $strError;
			$data['isReceived'] = 0;
		}
		return $data;
	}
	
	public function addBorder($borderImg){
		$sql = "select max(bordergroupid) as maxgroupid from ad_border";
		$result = $this->DB->read()->query_prepare($sql)->query_execute()->query_getsingle();
		if(!$result)
		{
			return array();
		}
		$maxgroupid = is_null($result['maxgroupid'])?0:$result['maxgroupid'];
		$bordergroupid = $maxgroupid + 1;
       	$createtime = time();
		$border[] = array('bordergroupid' => $bordergroupid, 'sizeid' => 12, 'type' => 1, 'path' => $borderImg['img_border1'], 'createtime' => $createtime, 'lastupdatetime' => $createtime,'isicon'=>'0');
		$border[] = array('bordergroupid' => $bordergroupid, 'sizeid' => 13, 'type' => 1, 'path' => $borderImg['img_border2'], 'createtime' => $createtime, 'lastupdatetime' => $createtime,'isicon'=>'0');
		$border[] = array('bordergroupid' => $bordergroupid, 'sizeid' => 8, 'type' => 1, 'path' => $borderImg['img_border3'], 'createtime' => $createtime, 'lastupdatetime' => $createtime,'isicon'=>'0');
		$border[] = array('bordergroupid' => $bordergroupid, 'sizeid' => 9, 'type' => 1,  'path' => $borderImg['img_border4'], 'createtime' => $createtime, 'lastupdatetime' => $createtime,'isicon'=>'0');
		$border[] = array('bordergroupid' => $bordergroupid, 'sizeid' => 10, 'type' => 1,  'path' =>$borderImg['img_border5'], 'createtime' => $createtime, 'lastupdatetime' => $createtime,'isicon'=>'0');
		$border[] = array('bordergroupid' => $bordergroupid, 'sizeid' => 11, 'type' => 1, 'path' => $borderImg['img_border6'], 'createtime' => $createtime, 'lastupdatetime' => $createtime,'isicon'=>'0');
//		if(!empty($borderImg['img_border7']) && $borderImg['img_border7']!=""){
//			$border[] = array('bordergroupid' => $bordergroupid, 'sizeid' => null, 'type' => 1, 'path' => $borderImg['img_border7'], 'createtime' => $createtime, 'lastupdatetime' => $createtime,'isicon'=>'1');
//		}
		if(empty($border))
        {
            return array('status' => 0);
        }
        $rowCount = $this->addBatch($border);
        if($rowCount <= 0){ 
        	return array('status' => 0);
        }
        //这里分开插入数据是为了避免插入null的数据变为0
		if(!empty($borderImg['img_border7']) && $borderImg['img_border7']!=""){
			$border7[] = array('bordergroupid' => $bordergroupid, 'type' => 1, 'path' => $borderImg['img_border7'], 'createtime' => $createtime, 'lastupdatetime' => $createtime,'isicon'=>'1');
		}
		if(empty($border7))
        {
            return array('status' => 0);
        }
        $rowCount7 = $this->addBatch($border7);
        if($rowCount7 <= 0){ 
        	return array('status' => 0);
        }
        return array('status' => 1);
	}
	
	function getSysBorderCount(){
		$sql="select count(DISTINCT (bordergroupid)) as count from ad_border where type=1 and status=1";
		$query = $this->DB->read()->query($sql)->query_getsingle();
        if(isset($query['count'])){
            return $query['count'];
        }
        return 0;
	}
	
	/*
	 * 删除一组边框
	 */
	function delBorder($bordergroupid){
        if($this->getRow(array('bordergroupid'=>$bordergroupid)) == false)
        {
          	 array('status' => 0);
        }
        $data['bordergroupid'] = $bordergroupid;
        $status = $this->edit($data, array('status' => 2));
        if($status){
        	return array('status' => 1);
        }
        return array('status' => 0);
    }
    /**
	 * 获取一组边框全部信息
	 * @param $params：参数
	 * wf
	 */
    function getBorderInfo($where){
		$data = $this->getList($where);
        if(empty($data))
        {
            return false;
        }
        return $data;
    }
    
/**
 * 编辑边框
 * @param $params：参数
 * wf
 */
    function doEdit($params){
    	$borderUpdate = json_decode($params['updateBorderJson'], TRUE);
    	$bordergroupid=$params['bordergroupid'];
        foreach($borderUpdate as $key=>$val)
	    {
	    	$updateData = explode("*", $val);
	    	$where['borderid'] = $updateData[0];
	    	$updatePath['path'] = $updateData[1];
	    	$updatePath['lastupdatetime'] = time();
	    	if($updateData[0]!='undefined'){
			    $upNum=$this->edit($where,$updatePath);
	        	if($upNum <= 0) return array('status' => 0);
	    	}
	    	else{
	    		if(!empty($params['img_border7']) && $params['img_border7']!=""){
	    			//如果原来没有上传关闭按钮，现在又重新上传了，需要保存这条数据。
					$createtime = time();
					$defaultborder = empty($params['defaultborder'])?0:$params['defaultborder'];
					$border= array('bordergroupid' => $bordergroupid, 'sizeid' => null,'type' => 1, 'path' => $params['img_border7'], 'createtime'=>$createtime, 'lastupdatetime' => $createtime,'isicon'=>'1', 'defaultborder'=>$defaultborder);
					$this->add($border);
	    		}
	    		else{
	    			return  array('status' => 0);
	    		}
	    	}
	    }
	    return array('status' => 1);
    }
    
    /**
     * 修改边框状态
     * @param $params：array(bordergroupid,status)
     * wf
     */
    function dosetborderstatus($params){
    	$bordergroupid=$params['bordergroupid'];
    	$status=$params['status'];
    	if($this->getRow(array('bordergroupid'=>$bordergroupid)) == false)
        {
          	 array('status' => 0);
        }
        $data['bordergroupid'] = $bordergroupid;
        $status = $this->edit($data, array('status' => $status));
        if($status){
        	return array('status' => 1);
        }
        return array('status' => 0);
    }
    
    /**
     * 默认边框设置
     */
    function doSetDefaultBorder($params){
    	$bordergroupid = $params['bordergroupid'];
    	$defaultborder = $params['defaultborder'];
    	$borderInfo = $this->getRow(array('bordergroupid'=>$bordergroupid));
    	if(!$borderInfo){
    		return array('status'=>0);
    	}
    	if($defaultborder==1){
    		$this->edit(array('defaultborder'=>1), array('defaultborder'=>0));
    	}
    	$status = $this->edit(array('bordergroupid'=>$bordergroupid), array('defaultborder'=>$defaultborder));
    	if($status) return array('status'=>1);
    	return array('status'=>0);
    }
}

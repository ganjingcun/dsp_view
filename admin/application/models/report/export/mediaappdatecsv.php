<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @Title: file_name 
 * @Description: todo( ) 
 * @author : niejianhui
 * @$Date : 2013-2-19
*/
require_once 'dowloadabstract.php';
class MediaappDateCsv extends DowloadAbstract
{
	protected function getTitle()
	{
		return array('日期','收入(￥)','点击量');
	}	
	
	protected function getKey()
	{
		return array('pkKey','cost','lCli');
	}
}
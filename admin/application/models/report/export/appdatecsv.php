<?php 
/**
 * 开发者-按应用汇总的数据的下载模板
 * @author wangf
 *
 */
require_once 'dowloadabstract.php';
class AppDateCsv extends DowloadAbstract
{
	protected function getTitle()
	{
		return array('日期','点击量','收入');
	}	
	
	protected function getKey()
	{
		return array('pkKey','lCli','sum');
	}
}
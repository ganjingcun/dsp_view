<?php 
/**
 * 开发者-按应用汇总的数据的下载模板
 * @author wangf
 *
 */
require_once 'dowloadabstract.php';
class AdVertUserPopCsv extends DowloadAbstract
{
	protected function getTitle()
	{
		return array('UID','广告主名称','账号','消费(￥)','展现量','点击量','avg.CPC','avg.CPM');
	}	
	
	protected function getKey()
	{
		return array('id','pkKey','username','cost','lImp','lCli','cpc','cpm');
	}
}

<?php 
/**
 * 开发者-按应用汇总的数据的下载模板
 * @author wangf
 *
 */
require_once 'dowloadabstract.php';
class AdVertUserCsv extends DowloadAbstract
{
	protected function getTitle()
	{
		return array('UID','广告主名称','账号','消费(￥)','点击量','avg.CPC');
	}	
	
	protected function getKey()
	{
		return array('id','realname','username','cost','lCli','cpc');
	}
}
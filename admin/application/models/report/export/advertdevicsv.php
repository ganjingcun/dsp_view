<?php 
/**
 * 开发者-按应用汇总的数据的下载模板
 * @author wangf
 *
 */
require_once 'dowloadabstract.php';
class AdVertDeviCsv extends DowloadAbstract
{
	protected function getTitle()
	{
		return array('平台','消费(￥)','点击量','avg.CPC');
	}	
	
	protected function getKey()
	{
		return array('pkKey','cost','lCli','cpc');
	}
}
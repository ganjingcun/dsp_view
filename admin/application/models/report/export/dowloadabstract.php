<?php
/**
 * 导出抽象类
 * @author ronghua
 *
 */
abstract class DowloadAbstract
{
	//获取标题
	protected abstract function getTitle();
	
	//key字段
	protected abstract function getKey();
	
	/*
	 * 执行 
	 * $_arr1 为需要截取的数组字段，一般保留2位小数
	 * $_arr2 为需要加百分号的数组
	 */ 
	final public function exportCsv($data,$_arr1=array('cpc','cpm','sum_cost'),$_arr2=array('click_ratio'),$is_format=false,$_arr3=array('sum_imp','sum_cleanImp','sum_click','sum_cleclick'),$filename='') 
	{
		$filename = empty($filename)?date('YmdHis').".csv":$filename;
		header("Content-type:text/csv");
		header("Content-Disposition:attachment;filename=".$filename);
		header('Cache-Control:must-revalidate,post-check=0,pre-check=0');
		header('Expires:0');
		header('Pragma:public');
		$tlist = $this->arrayToString($data,$_arr1,$_arr2,$is_format,$_arr3);
		if($tlist)
		{
			//echo "\xEF\xBB\xBF";
			//echo $tlist;
			echo mb_convert_encoding($tlist, "GBK", "UTF-8"); 
		}else{
			echo '';
		}
		exit;
	}

	//数据转为字符串类型
	private function arrayToString($data,$_arr1=array(),$_arr2=array(),$is_format,$_arr3=array()) 
	{
		if(empty($data)) return array();
		$tmp = array();
		$i = 0;	
		$keys = $this->getKey();
		foreach($data as $k=>$row)
		{
			foreach($keys as $value)
			{
				if(in_array($value,$_arr1))
					$tmp[$i][] = $row[$value]==-1?'--':'"'.round($row[$value],2).'"';
				else if(in_array($value,$_arr2))
					$tmp[$i][] = $row[$value]==-1?'--':round(($is_format?($row[$value]*100):$row[$value]),2)."%";
				else if(in_array($value,$_arr3))
					$tmp[$i][] = $row[$value]==-1?'--':'"'.round($row[$value]).'"';
				else
					$tmp[$i][] = $row[$value]==-1?'--':$row[$value];
			}
			$i++;
		}
		$result = @array_merge(array($this->getTitle()),$tmp);
		
		if(empty($result)) return array();
		
		$data = '';
		foreach( $result as $k=>$row )
		{
			$data .= @implode(",",$row)."\n";
		}
		return $data;
	}
}
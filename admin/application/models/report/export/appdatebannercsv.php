<?php 
/**
 * 开发者-按应用汇总的数据的下载模板
 * @author wangf
 *
 */
require_once 'dowloadabstract.php';
class AppDateBannerCsv extends DowloadAbstract
{
	protected function getTitle()
	{
		return array('日期','请求量','展现量','点击量','点击率(CTR)','收入');
	}	
	
	protected function getKey()
	{
		return array('pkKey','uAsk','lImp','lCli','radio','sum');
	}
}
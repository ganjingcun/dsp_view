<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @Title: file_name 
 * @Description: todo( ) 
 * @author : niejianhui
 * @$Date : 2013-2-19
*/
require_once 'dowloadabstract.php';
class MediaappVertAppCsv extends DowloadAbstract
{
	protected function getTitle()
	{
		return array('ID','App名称','账号','收入','点击量');
	}	
	
	protected function getKey()
	{
		return array('id','realname','username','cost','lCli');
	}
}
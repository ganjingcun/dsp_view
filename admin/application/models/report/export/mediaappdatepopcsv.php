<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @Title: file_name 
 * @Description: todo( ) 
 * @author : niejianhui
 * @$Date : 2013-2-19
*/
require_once 'dowloadabstract.php';
class MediaappDatePopCsv extends DowloadAbstract
{
	protected function getTitle()
	{
		return array('日期','收入(￥)','展现量','点击量','avg.CPC','avg.CPM');
	}	
	
	protected function getKey()
	{
		return array('pkKey','cost','lImp','lCli','cpc','cpm');
	}
}
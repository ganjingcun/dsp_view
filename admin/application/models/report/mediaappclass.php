<?php /**
 * @filename	: mediaappclass.php
 * @encoding	: UTF-8
 * @author		: niejianhui
 * @datetime	: 2013-1-28 11:50:15
 * @Description  : 
 */
require_once "application/models/report/reportbaseclass.php";
class Mediaappclass extends ReportBaseClass
{
	private $sumData = array();
	const UNIT = 100000;

	function __construct()
	{
		parent::__construct();
	}

	/**
	 * 获取数据源
	 * @param array $params
	 */
	function getDataSource($params)
	{
		//获取广告墙的信息
		$data = @getDataApi($this->getCondition($params), true);
		// echo $data;
		if(!empty($data['data']))
			return $data['data'];
		else
			return array();
	}

	/**
	 * 获取数据(时间)
	 * @param array $params
	 */
	function getDataByDate($params)
	{
		//获取的信息
		$rdata = $this->getDataSource($params);//print_r($rdata);
		if(!empty($rdata))
		{
			foreach ($rdata as $key=>&$val)
			{
				if($params['tag']==1)
					$val['pkKey_str'] = substr($val['pkKey'], 5,5);
				else
					$val['pkKey_str'] = $val['pkKey'];
				$val['lImp'] = (int)$val['lImp'];
				$val['lCli'] = (int)$val['lCli'];
				$val['cost'] = (float)round(($val['cost']/self::UNIT),2);
				if(!empty($val['lCli']))
					$val['cpc'] = number_format($val['cost']/$val['lCli'],2);
				else
					$val['cpc'] = -1;

				if(!empty($val['lImp']))
					$val['cpm'] = number_format($val['cost']/$val['lImp']*1000,2);
				else
					$val['cpm'] = -1;

				if(!empty($val['lImp']))
					$val['radio'] = number_format($val['lCli']*100/$val['lImp'],2);
				else
					$val['radio'] = -1;
			}
		}
		else if($params['tag']==1)
		{
			$rdata = array(array(
				"pkKey"=>$params['sDate'],
				"pkKey_str"=>substr($params['sDate'],5,10),
				"lImp" =>0,
				"lCli" =>0,
				"cost" =>0,
				"radio" =>-1,
				"cpc"   =>-1,
				"cpm"   =>-1
			));
		}
		return $rdata;
	}


	/**
	 * 汇总
	 * @see ReportBaseClass::getSum()
	 */
	public function getSum(array $condition=array(), array $data = array(),$filter = array("pkKey","pkKey_str","cpm","radio","cpc"))
	{
		$sumData = array(
			"lImp"=>0,
			"lCli"=>0,
			"cost"=>0,
			"luImp"=>0,
			"luCli"=>0,
			'ask'=>0,
			'imp'=>0,
		);

		if(empty($data))
			return $sumData;
		$arr_keys = array_keys($data[0]);
		foreach ($data as $val)
		{
			foreach ($arr_keys as $key){
				if(!in_array($key,$filter)){
					$sumData[$key] += (int)$val[$key];
				}
			}
		}
		return $sumData;
	}

	/**
	 *
	 * 组合条件
	 * @param array $params
	 */
	private function getCondition($params,$tab=1)
	{
		if(empty($params)) return false;
		if(!isset($params['sDate']) || empty($params['sDate']))
			return false;
		if(!isset($params['eDate']) || empty($params['eDate']))
			return false;
		if(!isset($params['t']) || $params["t"] == ''){
			$params['t'] = 'mt_developer_info';
		}
		$condition = array();
		$condition['server']       = "server=moregame";
		$condition['t']            = "t=ads_report_mgexchange.".$params['t'];
		$ct =  "pkDay>='".$params['sDate']."'";
		$ct .= " and pkDay<='".$params['eDate']."'";
		if($params['tid'])
			$ct .= " and pkAdType=".$params['tid'];
		if(!empty($params['pkAdGroupId']))
		{
			$ct .= " and pkAdGroupId in(".implode(",", $params['pkAdGroupId']).") ";
		}
		if(!empty($params['appstr'])){
			$ct .= " and pkAppId in ({$params['appstr']}) ";
		}
		$condition['w'] = "w=".urlencode($ct);
		$groupby = $this->getTag($params['tag']);
		if($groupby == '' && isset($params['g'])){
			$groupby = $params['g'];
		}
		$other = '';
		if(isset($params['s'])){
			$other .= $params['s'];
		}
		$condition['s']  = "s=".urlencode($groupby." pkKey, "). $other .$this->getCols($tab);
		$condition['g']  = "g=".$groupby;

		return $condition;
	}

	/**
	 *
	 * 获取设备类型
	 */
	function getDtTag()
	{
		return array(
			1=>"iPhone",
			2=>"iPad",
			4=>"Android",
//			4=>"Android_Pad"
		);

	}

	/**
	 * 获取表名
	 */
	function getTable($key)
	{
		$value = array(
			  1=>"mt_developer_info",
			  //1=>"mt_apps_income_all",
		);
		return $value[$key];
	}

	/**
	 * 获取Tag标签
	 */
	function getTag($key)
	{
		$value = array(
			  1=>"pkDay",            //时间
			  2=>"pkDeviceType",     //平台
			  3=>"pkAppId",      //开发者
			  4=>"pkAppId",		 //广告组
		);
		return $value[$key];
	}

	function getCols($key)
	{
		$array = array(
			1=>urlencode("sum(imp) lImp,sum(clk) lCli,0 cost,sum(ask) ask,sum(imp) imp"),
			10=>urlencode("sum(uidImp) luImp,sum(uidClk) luCli"),
		);
		return $array[$key];
	}

	/**
	 *
	 * 获取udid
	 * @param unknown_type $params
	 */
	function getUdidSource($params)
	{
		//获取广告墙的信息
		$data = @getDataApi($this->getCondition($params,10),true); //echo $data;exit();
		if(!empty($data['data']))
			return $data['data'];
		else
			return array();
	}

	function format_rate($data = array(), $name){
		$arr = array();
		if(is_array($data))
		foreach($data as $val){
			if($val['ask']>0):
				$val['imprate'] = number_format($val['lImp'] / $val['ask'] * 100, 2, '.' , '').'%';
			else:
				$val['imprate'] = '0.00%';
			endif;
			if($val['lImp']>0):
				$val['clickrate'] = number_format($val['lCli'] / $val['lImp'] *100, 2, '.' , '').'%';
			else:
				$val['clickrate'] = 0.00;
			endif;
			$arr[$val[$name]][] = $val;
		}
		return $arr;
	}

	/**
	 * 将指定数据 $data 按 $key 组成 $num 数量的多维数组
	 * @param string $data
	 * @param type $key
	 * @param type $num
	 * @return string
	 */
	function fill_array ($data=array(), $key, $num){
		foreach($data as &$val){
			foreach($num as $numv){
				if(!isset($data[$numv])){
					foreach($key as $v){
						$data[$numv][$v] = '';
					}
				}
			}
		}
		return $data;
	}
}
<?php
/**
 * banner消费分析
 */
require_once "application/models/report/reportbaseclass.php";
class BannerReportClass extends ReportBaseClass
{
	private $sumData = array();
	const UNIT = 100000;
	
	function __construct()
	{
		parent::__construct();
	}
	
	/**
	 * 获取数据源
	 * @param array $params
	 */
	public function getDataSource($params,$tab=1)
	{
		//获取广告墙的信息
		$data = @getDataApi($this->getCondition($params,$tab),true); //echo $data;exit();
		if(!empty($data['data']))
			return $data['data'];
		else 
			return array();	
	}
	
	/**
	 * 获取时间格式化数据
	 */
	public function getDateDataFormat($params,$tab=1)
	{
		$rdata = $this->getDataSource($params,$tab);
		if(!empty($rdata))
			return $rdata;
		else 
			return array(
					array(
						"pkKey"=>$params['sDate'],
						"lImp" =>0,
						"lCli" =>0,
						"cost" =>0,
						"luCli" =>0,
						"luImp" =>0,
					)
			);	
	}
	
	/**
	 * 获取平台格式化数据
	 */
	public function getPlatDataFormat($params)
	{
		$rdata = $this->getDataSource($params);
		$ptag = $this->getDtTag();
		if(!empty($rdata))
		{
			foreach ($rdata as $val)
			{
				$rkey[] = $val['pkKey'];
			}
			$pr_key = array_diff($ptag,$rkey);
			if($pr_key)
			foreach ($pr_key as $v)
			{
				$rdata[] = array(
							"pkKey"=>$v,
							"lImp" =>0,
							"lCli" =>0,
							"cost" =>0,
						);
			}
			return $rdata;
		}
			
		else 
		{
			$rk = array();
			foreach ($ptag as $va)
			{
				$rk[] = array(
							"pkKey"=>$va,
							"lImp" =>0,
							"lCli" =>0,
							"cost" =>0,
						);
			}
		}
		return $rk;
	}
	
	/**
	 * 获取数据(时间)
	 * @param array $params
	 */
	function getDataByDate($params)
	{
		//获取的信息
		$rdata = $this->getDataSource($params);//print_r($rdata);
		if(!empty($rdata))
		{
			foreach ($rdata as $key=>&$val)
			{
				if($params['tag']==1)
					$val['pkKey_str'] = substr($val['pkKey'], 5,5);	
				else 
					$val['pkKey_str'] = $val['pkKey'];			
				$val['lImp'] = (int)$val['lImp'];
				$val['lCli'] = (int)$val['lCli'];
				$val['cost'] = (float)round(($val['cost']/self::UNIT),2);
				if(!empty($val['lCli']))
					$val['cpc'] = number_format($val['cost']/$val['lCli'],2);
				else
					$val['cpc'] = -1;
	
				if(!empty($val['lImp']))
					$val['cpm'] = number_format($val['cost']/$val['lImp']*1000,2);
				else 
					$val['cpm'] = -1;
					
				if(!empty($val['lImp']))
					$val['radio'] = number_format($val['lCli']*100/$val['lImp'],2);
				else 
					$val['radio'] = -1;	
			}
		}
		else if($params['tag']==1)
		{
			$rdata = array(array(
				"pkKey"=>$params['sDate'],
				"pkKey_str"=>substr($params['sDate'],5,10),
				"lImp" =>0,
				"lCli" =>0,
				"cost" =>0,
				"radio" =>-1,
				"cpc"   =>-1,
				"cpm"   =>-1
			));
		}
		return $rdata;
	}
	
	
	/**
	 * 汇总
	 * @see ReportBaseClass::getSum()
	 */
	public function getSum(array $condition=array(), array $data = array(),$filter = array("pkKey","pkKey_str","cpm","radio","cpc"))
	{
		$sumData = array(
			"lImp"=>0,
			"lCli"=>0,
			"cost"=>0,
			"luImp"=>0,
			"luCli"=>0,
		);
		
		if(empty($data))
			return $sumData;
		$arr_keys = array_keys($data[0]);	
		foreach ($data as $val)
		{
			foreach ($arr_keys as $key)
			if(!in_array($key,$filter))
				$sumData[$key] += $val[$key];
			
		}
		return $sumData;
	}
	
	/**
	 * 
	 * 组合条件
	 * @param array $params
	 */
	private function getCondition($condition=array(),$tab=1)
	{
		$cond['sd'] = "sd=ge|".strtotime($condition['sDate']);
		$cond['ed'] = "ed=le|".strtotime($condition['eDate']);
		$cond['t']  = "t=d_groups";	
		$ttag = $this->getTag($condition['tag']);		
		if($condition['pkAdGroupId'])
			$cond['w'] .= "gid=".$condition['pkAdGroupId'];
		$cond['s'] = "s=".urlencode($ttag." pkKey,").$this->getCols($tab);
		$cond['g'] = "g=".$ttag;
		return $cond;
	}
	
	/**
	 * 
	 * 获取设备类型
	 */
	function getDtTag()
	{
		return array(
			1=>"iPhone",
			2=>"iPad",
			4=>"Android",
//			4=>"Android_Pad"
		);
		
	}
	
	
	/**
	 * 获取Tag标签
	 */
	function getTag($key)
	{
		$value = array(
			  1=>"pkDay",            //时间
			  2=>"pkDeviceType",     //平台
			  3=>"pkCampaignId",      //广告主
			  4=>"pkAdGroupId",		 //广告组
		);
		return $value[$key];
	}
	
	function getCols($key)
	{
		$array = array(
			1=>urlencode("sum(imp) lImp,sum(click) lCli,sum(cost) cost"),
			10=>urlencode("sum(imp) lImp,sum(click) lCli,sum(uidImp) luImp,sum(uidClick) luCli"),
		);
		return $array[$key];
	}
	
	/**
	 * 
	 * 获取udid
	 * @param unknown_type $params
	 */
	function getUdidSource($params)
	{
		//获取广告墙的信息
		$data = @getDataApi($this->getCondition($params,10),true); //echo $data;exit();
		if(!empty($data['data']))
			return $data['data'];
		else 
			return array();	
	}
}
<?php
/**
 * banner消费分析
 */
require_once "application/models/report/reportbaseclass.php";
class BannerAppReportClass extends ReportBaseClass
{
	private $sumData = array();
	function __construct()
	{
		parent::__construct();
	}
	
	/**
	 * 获取数据源
	 * @param array $params
	 */
	public function getDataSource($params,$tab=1)
	{
		//获取广告墙的信息
		$data = @getDataApi($this->getCondition($params,$tab),true); //echo $data;exit();
		if(!empty($data['data']))
			return $data['data'];
		else 
			return array();	
	}
	
	/**
	 * 获取时间格式化数据
	 */
	public function getDateDataFormat($params,$tab=1)
	{
		$rdata = $this->getDataSource($params,$tab);
		if(!empty($rdata))
			return $rdata;
		else 
			return array(
					array(
						"pkKey"=>$params['sDate'],
						"lImp" =>0,
						"lCli" =>0,
						"lAsk" =>0,
					)
			);	
	}
	

	/**
	 * 汇总
	 * @see ReportBaseClass::getSum()
	 */
	public function getSum(array $condition=array(), array $data = array(),$filter = array("pkKey","pkKey_str","cpm","radio","cpc"))
	{
		$sumData = array(
			"lImp"=>0,
			"lCli"=>0,
			"lAsk"=>0,
		);
		
		if(empty($data))
			return $sumData;
		$arr_keys = array_keys($data[0]);	
		foreach ($data as $val)
		{
			foreach ($arr_keys as $key)
			if(!in_array($key,$filter))
				$sumData[$key] += $val[$key];
			
		}
		return $sumData;
	}
	
	/**
	 * 
	 * 组合条件
	 * @param array $params
	 */
	private function getCondition($condition=array(),$tab=1)
	{
		$cond['sd'] = "sd=ge|".strtotime($condition['sDate']);
		$cond['ed'] = "ed=le|".strtotime($condition['eDate']);
		$cond['t']  = "t=d_groups";	
		$ttag = $this->getTag($condition['tag']);		
		if($condition['pid'])
			$cond['w'] .= "gid=".$condition['gid'];
		$cond['s'] = "s=".urlencode($ttag." pkKey,").$this->getCols($tab);
		$cond['g'] = "g=".$ttag;
		return $cond;
	}
	
	/**
	 * 
	 * 获取设备类型
	 */
	function getDtTag($pid)
	{
		$pt = array(
			1=>"iPhone",
			2=>"iPad",
			4=>"Android",
//			5=>"Android_Pad"
		);
		return $pt[$pid];
	}
	
	
	/**
	 * 获取Tag标签
	 */
	function getTag($key)
	{
		$value = array(
			  1=>"pkDay",            //时间
			  2=>"pkCanalId",        //app
		);
		return $value[$key];
	}
	
	/**
	 * 
	 * 获取查询字段
	 * @param int $key
	 */
	function getCols($key)
	{
		$array = array(
			1=>urlencode("sum(imp) lImp,sum(clk) lCli,sum(ask) lAsk"),
		);
		return $array[$key];
	}
}
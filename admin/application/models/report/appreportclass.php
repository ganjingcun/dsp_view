<?php
include dirname(__FILE__).'/reportbaseclass.php';
class AppReportClass extends ReportBase{
    function getHourData($publisherID){
        if(empty($publisherID))
            return false;
        $condition = array();
        $condition['server']       = "server=ads_union";
        $condition['t']            = "t=h_developer_banner";
        $condition['pkPublisherId'] = 'pkPublisherId=in|"'.$publisherID.'"';
        $condition['sd'] = 'sd=ge|'.strtotime(date('Y-m-d'));
        $condition['sss'] = 'ed=le|'.strtotime(date('Y-m-d'));
        $condition['s']  = "s=".urlencode('pkPublisherId,pkHour,sum(impSucc) imp,sum(ask) ask,sum(click) click');
        $condition['g']  = "g=pkHour";
        $list = $this->getDataSource($condition);
        $tmp = array();
        foreach($list as $val){
            $tmp[$val['pkHour']] = $val;
        }
        ksort($tmp);
        return $tmp;
    }

}
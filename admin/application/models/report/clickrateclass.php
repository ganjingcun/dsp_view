<?php
class ClickRateClass{

    function __construct() {

    }

    /**
     * 获取广告创意点击率
     * @param type $ids
     */
    function getDataAdReport(){
        $adActivationData = $this->getDataAdActivation();
        $t='h_uid';
        $sdate = date('Y-m-d', time() - 86400);
        $edate = date('Y-m-d');
        $condition = array();
        $condition['server']       = "server=ads_union";
        $condition['t']            = "t={$t}";
        $condition['spkDay'] = 'sd=ge|'.strtotime($sdate);
        $condition['epkDay'] = 'ed=le|'.strtotime($edate);
        $condition['s']  = "s=".urlencode("pkCanalId,pkAdId,SUM(`uidClick`) as clk,SUM(`uidImp`) as imp");
        $condition['g']  = "g=pkCanalId,pkAdId";
        $list = $this->getDataSourceOnce($condition);
        $tmp = array();
        $tmpSum = array();
        foreach($list as $val)
        {
            //点击转化率
            if(!empty($val['pkCanalId']) && !empty($val['pkAdId']))
            {
                if(!empty($val['imp']))
                {
                    $tmp[$val['pkCanalId']][$val['pkAdId']]['ctr'] = $val['clk']*100 / $val['imp'];
                }
                if(isset($tmpSum[$val['pkCanalId']]['imp']))
                {
                    $tmpSum[$val['pkCanalId']]['imp'] += $val['imp'];
                }
                else
                {
                    $tmpSum[$val['pkCanalId']]['imp'] = $val['imp'];
                }
                if(isset($tmpSum[$val['pkCanalId']]['clk']))
                {
                    $tmpSum[$val['pkCanalId']]['clk'] += $val['clk'];
                }
                else
                {
                    $tmpSum[$val['pkCanalId']]['clk'] = $val['clk'];
                }
            }
            //激活转化率
            if(isset($adActivationData[$val['pkCanalId']][$val['pkAdId']]))
            {
                if($val['clk'])
                {
                    $tmp[$val['pkCanalId']][$val['pkAdId']]['atr'] = $adActivationData[$val['pkCanalId']][$val['pkAdId']]*100 / $val['clk'];
                }
                if(isset($tmpSum[$val['pkCanalId']]['cmp']))
                {
                    $tmpSum[$val['pkCanalId']]['cmp'] += $adActivationData[$val['pkCanalId']][$val['pkAdId']];
                }
                else
                {
                    $tmpSum[$val['pkCanalId']]['cmp'] = $adActivationData[$val['pkCanalId']][$val['pkAdId']];
                }
            }

        }
        foreach($tmpSum as $k => $v)
        {
            if($v['imp'] > 0)
            {
                $tmp[$k][0]['ctr'] = $v['clk'] * 100 / $v['imp'];
            }
            if($v['clk'] > 0 && isset($v['cmp']))
            {
                $tmp[$k][0]['atr'] = $v['cmp'] * 100 / $v['clk'];
            }
        }
        return $tmp;
    }

    /**
     * 获取广告创意激活数
     *
     * @param type $ids
     */
    function getDataAdActivation(){

        $t='h_wall_ader_event_usr';
        $sdate = date('Y-m-d', time() - 86400);
        $edate = date('Y-m-d');
        $condition = array();
        $condition['server']       = "server=ads_union";
        $condition['t']            = "t={$t}";
        $condition['spkDay'] = 'sd=ge|'.strtotime($sdate);
        $condition['epkDay'] = 'ed=le|'.strtotime($edate);
        $condition['s']  = "s=".urlencode("pkCanalId,pkAdId,SUM(`complate`) as complate");
        $condition['g']  = "g=pkCanalId,pkAdId";
        $list = $this->getDataSourceOnce($condition);
        $tmp = array();
        if(!empty($list))
        {
            foreach($list as $val)
            {
                $tmp[$val['pkCanalId']][$val['pkAdId']] = $val['complate'];
            }
        }
        return $tmp;
    }

    function getDataSourceOnce($condition){
        $data = getDataApi($condition,true);
        //$data = getDataApi($condition,true,true);print_r($data);
        if(!empty($data['data']))
        return $data['data'];
        else
        return array();
    }
}
<?php
/**
 * @author ronghua
 * app分析报表
 */
require_once "application/models/report/reportbaseclass.php";
class AppIncomeReportClass extends ReportBaseClass
{
	private $sumData = array();
	
	private $incomeCli = array();
	
	function __construct()
	{
		parent::__construct();
	}
	
	/**
	 * 获取数据
	 * @param array $params
	 * @param int $limit
	 */
	function getDataByDate($params,$tab=1)
	{
		//获取广告墙的信息
		$data = @getDataApi($this->getCondition($params,$tab),true);   //echo $data;exit();
		$rdata = $data['data'];
		$this->incomeCli = $rdata;
		
		$rmdata = array();
		if(!empty($rdata))
		foreach ($rdata as $key1=>$val1)
		{
			@$rmdata[$val1['pkKey']]['lAsk'] += $val1['lAsk'];
			@$rmdata[$val1['pkKey']]['lImp'] += $val1['lImp'];
			@$rmdata[$val1['pkKey']]['lCli'] += $val1['lCli'];
			if(isset($val1['cAsk']) && $tab == 2)
				@$rmdata[$val1['pkKey']]['cAsk'] += $val1['cAsk'];
		}
		
		
		if(!empty($rmdata))
		foreach ($rmdata as $key=>&$val)
		{
			$val['pkKey_str'] = substr($key, 5,5);
			$val['pkKey']     = $key;
			$val['lAsk'] = (int)$val['lAsk'];	
			$val['lImp'] = (int)$val['lImp'];
			$val['lCli'] = (int)$val['lCli'];
			if(isset($val['cAsk']) && $tab == 2)
				$val['cAsk'] = (int)$val['cAsk'];
			if(!empty($val['lImp']))
				$val['radio'] = round($val['lCli']*100/$val['lImp'],2);
			else 
				$val['radio'] = -1;	
			
		}
		if(empty($rdata))
		{
			$rmdata[0] = array(
				'pkKey'=>$params['sDate'],
				'pkKey_str'=>substr($params['sDate'], 5,5),
				'lAsk'=>0,
				'uAsk'=>0,
				'lImp'=>0,
				'lCli'=>0,
				'radio'=>-1
			);
		}
			
		//$this->sumData = $rdata;
		return $rmdata;
	}
	
	/**
	 * 汇总
	 * @see ReportBaseClass::getSum()
	 */
	public function getSum(array $condition=array(), array $data = array())
	{
		$sumData = array(
			'lAsk'=>0,
			'uAsk'=>0,
			"lImp"=>0,
			"lCli"=>0,
			"luidImp"=>0,
			"luidClk"=>0,
			"radio" =>-1,
			"sum"   =>0
		);
		
		//$data     = $this->sumData;
		if(empty($data))
			return $sumData;
		$arr_keys = array_keys($data[0]);
		foreach ($data as $val)
		{
			foreach ($arr_keys as $key)
			if(!in_array($key, array("pkKey","pkKey_str","appid","name")))
			{
					if(isset($val[$key]))
						$sumData[$key] += $val[$key];
			}
			
		}
		return $sumData;
	}
	
	/**
	 * 获取income
	 */
	function getIncomeCli()
	{
		return $this->incomeCli;
	}
	
	/**
	 * 
	 * 组合条件
	 * @param array $params
	 */
	private function getCondition($params,$tab=1) 
	{ 
		if(empty($params)) return false;
		if(!isset($params['sDate']) || empty($params['sDate']))
			return false;
		if(!isset($params['eDate']) || empty($params['eDate']))
			return false;
		if(empty($params['canalid']))	
			return false;
		$condition = array();
		$ct = "";
		if($tab==1)	
		{
			$condition['server']       = "server=moregame";		
			$condition['t']            = "t=ads_report_mgexchange.mt_developer_all";
			$ct 					  .= " pkAdType=".$params['tid'];
			$ct                       .= " and pkUserType in(0,1,3,4) and ";  //广告主
		}else if($tab==2)
			$condition['t']            = "t=ads_report.t_developer_all";
		$ct .=  " pkDay>='".$params['sDate']."'";
		$ct .= " and pkDay<='".$params['eDate']."'";
		if(!empty($params['canalid']))
		{
			$ct .= " and pkCanalId in(".implode(",", $params['canalid']).") ";
		}
		$condition['w'] = "w=".urlencode($ct);		
		$condition['s']  = "s=".$this->getCols($tab);
		$condition['g']  = "g=pkDay,pkCanalId";
		return $condition;
		
	}
	
	/**
	 * 
	 * 获取查询字段
	 * @param int $key
	 */
	function getCols($key)
	{
		$array = array(
			1=>urlencode("pkDay pkKey,pkCanalId CanalId,sum(ask) lAsk,sum(imp) lImp,sum(clkUid) lCli"),
			2=>urlencode("pkDay pkKey,pkCanalId CanalId,sum(imp) lImp,sum(uidClk) lCli,sum(ask) lAsk,sum(closeAsk) cAsk")
		);
		return $array[$key];
	}
}
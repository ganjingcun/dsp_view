<?php
class AdminGroupModel extends MY_Model
{
	protected  $table = "admin_group";

	function __construct() {
		parent::__construct();
	}

	function getGroup($data=array()){
		$list = $this->getList($data);
		$tmp = array();
		foreach($list as $val){
			$tmp[$val['group_id']] = $val['group_name'];
		}
		return $tmp;
	}
}
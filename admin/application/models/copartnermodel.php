<?php
class CoPartnerModel extends MY_Model{
    public $table = 'co_partner';
    
	public function doAdd($params)
   	{
   	 	$chk = $this->chkAddInfo($params);
        if($chk['status'] == 0)
        {
            return $chk;
        }
        $params['createtime'] = time();
        $id = $this->add($params);
        if($id <= 0) 
        	return array('status' => 0, 'data' => '','info'=>'');
        return array('status' => 1, 'data' => '','info'=>'');
    }
    
    public function doEdit($params,$id)
    {
    	$chk = $this->chkEditInfo($params);
    	if($chk['status'] == 0)
    	{
    		return $chk;
    	}
    	$params['edittime'] = time();
    	$affectedrows = $this->edit(array('id'=>$id),$params);
    	if($affectedrows<=0)
        	return array('status' => 0, 'data' => '','info'=>'');
        return array('status' => 1, 'data' => '','info'=>'');
    }
	
    private function chkAddInfo($params)
    {
        $error = array('status' => 1, 'data' => '');
        if(empty($params['title']))
        {
            $error['data'] = array('titlemsg', '请填写公司名称。');
            $error['info'] = 'titleerr';
            $error['status'] = 0;
            return $error;	
        }
   		if(!empty($params['priority']) && !ctype_digit($params['priority']))
    	{
    		$error['data'] = array('prioritymsg', '优先级只可以为整数。');
    		$error['info'] = 'priorityerr';
    		$error['status'] = 0;
    		return $error;
    	}
    	if(empty($params['logo']))
        {
            $error['data'] = array('imgpathmsg', '请上传公司Logo。');
            $error['info'] = 'imgpatherr';
            $error['status'] = 0;
            return $error;	
        }
        return $error;
    }
    
	private function chkEditInfo($params)
    {
        $error = array('status' => 1, 'data' => '');
        if(empty($params['title']))
        {
            $error['data'] = array('edittitlemsg', '请填写公司名称。');
            $error['info'] = 'edittitleerr';
            $error['status'] = 0;
            return $error;	
        }
   		if(!empty($params['priority']) && !ctype_digit($params['priority']))
    	{
    		$error['data'] = array('editprioritymsg', '优先级只可以为整数。');
    		$error['info'] = 'editpriorityerr';
    		$error['status'] = 0;
    		return $error;
    	}
    	if(isset($params['logo']) && empty($params['logo']))
        {
            $error['data'] = array('editimgpathmsg', '请上传公司Logo。');
            $error['info'] = 'editimgpatherr';
            $error['status'] = 0;
            return $error;	
        }
        return $error;
    }
}
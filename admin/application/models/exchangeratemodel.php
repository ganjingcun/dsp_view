<?php

class ExchangeRateModel extends MY_Model
{
    protected  $table = "exchange_rate";

    function __construct() {
        parent::__construct();
    }
    
    function disableRate($rateid, $from, $to){
    	$sql = "update exchange_rate set status=0 where currFrom='".$from."' and currTo='".$to."' and id!=".$rateid;
    	$this->DB->read()->query($sql);
    }
    
    function getRates($status){
    	$sql = "select * from exchange_rate where status=".$status." order by createtime desc";
    	return $this->DB->read()->query($sql)->query_getall();
    }
}

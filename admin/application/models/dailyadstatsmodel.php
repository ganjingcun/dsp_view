<?php
class DailyAdStatsModel extends MY_Model
{
     function __construct(){
         parent::__construct();
    }
 
    /**
    *
     * KT时间分析报表
     */
    function getListByTime($params=array(),$tab_key='1'){
         //获取广告墙的信息
 		$data = @getDataApi($this->getCondition($params,$tab_key),false);  
        $data = json_decode($data);
         if($data->status != '1') return ;
        $rdata = $data->data;     
        if(empty($rdata)) return $rdata;
        
        $start_time = strtotime($params['sDate']);
        foreach($rdata as $key=>$val){
            if(empty($val->pkDay))   continue;
            $stime = strtotime($val->pkDay." 00:00:00");
            $etime = strtotime($val->pkDay." 23:59:59");
            $rdata[$key]->newpublisher = $this->getPubliser($stime,$etime); 
            $rdata[$key]->allpublisher = $this->getPubliser($start_time,$etime);  
        } 
        return $rdata;
    }
        /**
     * 媒体游戏分析 
     */
    function getListByMedia($params=array(),$tab_key='2'){
        $data = @getDataApi($this->getCondition($params,$tab_key),false); 
      // var_dump($data);
        $data = json_decode($data);
        if($data->status != '1') return ;
        $rdata = $data->data;  
        $appids = array();
        foreach($rdata as $key=>$val){
             if(empty($val->pkCanalId))    continue;
             if(!empty($val->pkCanalId)) $appids[] = $val->pkCanalId;
        }
        $app_info = $this->getAppName($appids);  
        $ad_game_ids = $this->getAdGameId($appids);
        $start_time = strtotime($params['sDate']);
        $end_time = strtotime($params['eDate']);
        $allday = (int)((strtotime($params['eDate'])-strtotime($params['sDate']))/86400);  
        foreach($rdata as $key=>$val){
            if(empty($val->pkCanalId)) {
                unset($rdata[$key]);
                continue;
            }
            $rdata[$key]->app_name = $app_info[$val->pkCanalId];
            $rdata[$key]->adgameidnum = $ad_game_ids[$val->pkCanalId];
            $rdata[$key]->allpublisher = $this->getPubliser($start_time,$end_time);   
            $rdata[$key]->dayDeliv =  (int)($rdata[$key]->Deliv/$allday);
        }
         return $rdata;
    }
    /**
     * 渠道分析 
     */
    function getListByChannel($params=array(),$tab_key='3'){
         $data = @getDataApi($this->getCondition($params,$tab_key),false); 
         //var_dump($data);
        $data = json_decode($data);
        if($data->status != '1') return ;
        $rdata = $data->data;  
        $appids = array();
        $pkPublisherIds = array();
        foreach($rdata as $key=>$val){
             if(empty($val->pkCanalId))    continue;
             if(empty($val->pkPublisherId))    continue;
             if(!empty($val->pkCanalId)) $appids[] = $val->pkCanalId;
             if(!empty($val->pkPublisherId)) $pkPublisherIds[] = $val->pkPublisherId;
         }
        $app_info = $this->getAppName($appids);  
        $Publisher_info = $this->getPublisherName($pkPublisherIds);  
        $ad_game_ids = $this->getAdGameId($appids);
         $start_time = strtotime($params['sDate']);
        $end_time = strtotime($params['eDate']);
        $allday = (int)((strtotime($params['eDate'])-strtotime($params['sDate']))/86400);  
        foreach($rdata as $key=>$val){
            if(empty($val->pkCanalId)) {
                unset($rdata[$key]);
                continue;
            }
            $rdata[$key]->app_name = $app_info[$val->pkCanalId];
            $rdata[$key]->publisher_name = $Publisher_info[$val->pkPublisherId];
            $rdata[$key]->adgameidnum = $ad_game_ids[$val->pkCanalId];
            $rdata[$key]->allpublisher = $this->getPubliser($start_time,$end_time);   
            $rdata[$key]->dayDeliv =  (int)($rdata[$key]->Deliv/$allday);
        }
        return $rdata;
    }
    /**
    * 网络分析
    */
     function getListByNet($params=array(),$tab_key='4'){
         //获取广告墙的信息
 		$data = @getDataApi($this->getCondition($params,$tab_key),false);  
        //var_dump($data);
        $data = json_decode($data);
         if($data->status != '1') return ;
        $rdata = $data->data;     
        if(empty($rdata)) return $rdata;
         $netarr = array('0'=>'未知','1'=>'wifi','2'=>'2G','3'=>'3G','4'=>'代理','5'=>'其他');
        $start_time = strtotime($params['sDate']);
        foreach($rdata as $key=>$val){
            if(empty($val->pkDay))   continue;
            $stime = strtotime($val->pkDay." 00:00:00");
            $etime = strtotime($val->pkDay." 23:59:59");
            $rdata[$key]->newpublisher = $this->getPubliser($stime,$etime); 
            $rdata[$key]->allpublisher = $this->getPubliser($start_time,$etime);  
            $rdata[$key]->netTypeName = $netarr[$val->pkNet];
        } 
        return $rdata;
     }
    /**
     * 广告游戏数
     */
    private function getAdGameId($appids=array()){
         //广告游戏数
        if(empty($appids)) return ; 
        $appids_str = implode($appids,",");
        $sql = "SELECT COUNT(DISTINCT adgameid )adgameidnum ,appid FROM ad_sub_channel WHERE STATUS = 1 AND appid IN (".$appids_str.") GROUP BY appid"; 
        $info = $this->DB->read()->query($sql)->query_getall();  
        foreach($info as $val){
                $info[$val['appid']] = $val['adgameidnum'];
        }
        return $info;
    }

    /**
     * 根据$pkPublisherIds取得渠道的名称数组
     */
    private function getPublisherName($publisherids=array()){
        
       if(empty($publisherids)) return ; 
         foreach($publisherids as $key=>$val){
            $publisherids_str .= $val."','";
        }
        $publisherids_str = trim($publisherids_str);
        
         //游戏名称
        $sql = "SELECT channelname,publisherID FROM app_channel WHERE publisherID in ('".$publisherids_str."')"; 
        $info = $this->DB->read()->query($sql)->query_getall();  
        foreach($info as $val){
                $info[$val['publisherID']] = $val['channelname'];
        }
        return $info;
    }
    /**
     * 根据appids 取得游戏名称
     */
    private function getAppName($appids=array()){
        
       if(empty($appids)) return ; 
        $appids_str = implode($appids,",");
         //游戏名称
        $sql = "SELECT appname,appid FROM app_info WHERE appid in (".$appids_str.")";
        $info = $this->DB->read()->query($sql)->query_getall();  
        foreach($info as $val){
                $info[$val['appid']] = $val['appname'];
        }
        return $info;
    }
    /**
    *按照时间取得公告数量
     **/
   private function getPubliser($stime='',$etime=''){
        $sql = " SELECT  count(id) AS newpublisher FROM `ad_channel_public` where status = 3 AND publish_time >= $stime AND publish_time <= $etime"; 
        $info = $this->DB->read()->getsingle($sql); 
        return $info['newpublisher'];        
   }

   /**
     *
	 * 组合条件
	 * @param array $params
	 */
	private function getCondition($params,$tab_key='1') 
	{  
		if(empty($params)) return false;
		if(!isset($params['sDate']) || empty($params['sDate']))
			return false;
		if(!isset($params['eDate']) || empty($params['eDate']))
			return false;
		 
		$condition = array();
        $condition['server'] = "server=ads_union";		
        if($tab_key == '4') $condition['t'] = "t=d_json_info_android";
        else $condition['t'] = "t=d_json_info";
        $condition['sd'] = "sd=ge|".strtotime($params['sDate']);
		$condition['ed'] = "ed=le|".strtotime($params['eDate']);
        $condition['pkAdType'] = "pkAdType=10";		
		 
 		$condition['s']  = "s=".$this->getCols($tab_key);
		$condition['g']  =  $this->getGroupBy($tab_key);
 		return $condition;
 	}
    	/**
	 * 
	 * 获取查询字段
	 * @param int $key
	 */
	function getCols($key='1')
	{
		$array = array(
			1=>urlencode("pkDay,sum(deliv) Deliv,sum(click) Click,sum(download) Download,sum(install) Install"),
            2=>urlencode("pkDay,pkCanalId,sum(deliv) Deliv,sum(click) Click,sum(download) Download,sum(install) Install"),
            3=>urlencode("pkDay,pkCanalId,pkPublisherId,sum(deliv) Deliv,sum(click) Click,sum(download) Download,sum(install) Install"),
            4=>urlencode("pkDay,pkNet,sum(deliv) Deliv,sum(click) Click,sum(download) Download,sum(install) Install"),
 		);
		return $array[$key];
	}
    /**
     * 取得group by字段
     */
    function getGroupBy($key='1'){
        $array = array(
			1=>"g=pkDay",
            2=>'g=pkCanalId',
            3=>'g=pkCanalId,pkPublisherId',
            4=>"g=pkDay,pkNet"
 		);
		return $array[$key];
    }
     
}
<?php
class AppPositionModel extends MY_Model{
    public $table = 'app_position';
    
    /**
     * 生成广告位id
     */
    public function createPositionId($appid)
    {
        //时间戳转换成36进制
        $hexadecimal = time();
        $tmp = base_convert($hexadecimal, 10, 36);
        $positionId = $appid.$tmp;
        
        return $positionId;
    }
    
    public function doAdd($params)
    {
        $params['positionname'] = trim($params['positionname']);
        $data['positionid'] = $params['positionid'];
        $data['positionname'] = $params['positionname'];
        $data['positiondesc'] = $params['positiondesc'];
        $data['adform'] = $params['adform'];
        $data['appid']= $params['appid'];
        $data['createtime'] = time();
        $id = $this->add($data);
        if($id <= 0) return array('status' => 0, 'info' => 'positionnameerr', 'data' => array('positionnamemsg', '未知错误。'));
        return array('status' => 1, 'id' => $id);
    }
    
    public function doDel($params)
    {
        if($this->getRow(array('id'=>$params['id'])) == false)
        {
            $error['data'] = array('positionidmsg', '广告位ID不存在，请刷新页面重试！');
            $error['info'] = 'postioniderr';
            return $error;
        }

        $data['id'] = $params['id'];
        $data['appid']= $params['appid'];
        $status = $this->edit($data, array('status' => 1));
        if($status)
            return array('status' => 1);
        return array('status' => 0);
    }
	
    public function editPositionBorder($params)
    {
    	$chk = $this->chkEditInfo($params);
        if($chk !== true)
        {
            $err['status'] = 0;
            $err = array_merge($err, $chk);
            return $err;
        }
    	$condition['id'] = $params['id'];
    	$set['bordertype'] = $params['bordertype'];
    	$set['adstylesize'] = $params['adstylesize'];
    	$set['adstyleimgsize'] = $params['adstyleimgsize'];
    	$set['adstyle'] = 1;
    	$bordergroupid=0;
        if($params['bordertype'] == 1)
        {
        	$set['borderpolicy'] = $params['borderpolicy'];
            $bordergroupid = empty($params['sysBorderGroupId'])?0:$params['sysBorderGroupId'];
        }
        elseif($params['bordertype'] == 2)
        {
	        $sql = "select max(bordergroupid) as maxgroupid from ad_border";
	    	$result = $this->query($sql);
	        $re = $result->query_getsingle();
	        $maxgroupid = is_null($re['maxgroupid'])?0:$re['maxgroupid'];
	        $bordergroupid = $maxgroupid + 1;
	       	$createtime = time();
			  	
			$border[] = array('bordergroupid' => $bordergroupid, 'sizeid' => 12, 'type' => 2, 'path' => $params['img_border12'], 'createtime' => $createtime,'isicon'=>0);
			$border[] = array('bordergroupid' => $bordergroupid, 'sizeid' => 13, 'type' => 2, 'path' => $params['img_border13'], 'createtime' => $createtime,'isicon'=>0);
		    $border[] = array('bordergroupid' => $bordergroupid, 'sizeid' => 8, 'type' => 2, 'path' => $params['img_border8'], 'createtime' => $createtime,'isicon'=>0);
		    $border[] = array('bordergroupid' => $bordergroupid, 'sizeid' => 9, 'type' => 2,  'path' => $params['img_border9'], 'createtime' => $createtime,'isicon'=>0);
		    $border[] = array('bordergroupid' => $bordergroupid, 'sizeid' => 10, 'type' => 2,  'path' => $params['img_border10'], 'createtime' => $createtime,'isicon'=>0);
		    $border[] = array('bordergroupid' => $bordergroupid, 'sizeid' => 11, 'type' => 2,  'path' => $params['img_border11'], 'createtime' => $createtime,'isicon'=>0);
		    if(isset($params['img_border7']) && !empty($params['img_border7'])){
		    	$border[] = array('bordergroupid' => $bordergroupid, 'sizeid' => null,'type' => 2,  'path' => $params['img_border7'], 'createtime' => $createtime, 'isicon'=>1);
		    }
			$this->load->model("AdBorderModel");
	        $this->AdBorderModel->addBatch($border);
        }
        $set['bordergroupid'] = $bordergroupid;
        $this->edit($condition, $set);
        return array('status' => 1);
    }
    
 	private function chkEditInfo($params)
    {
    	if($params['bordertype'] == 2){
    		if(empty($params['img_border8'])||empty($params['img_border9'])||empty($params['img_border10'])||empty($params['img_border11'])||empty($params['img_border12'])||empty($params['img_border13'])){
	            $error['info'] = '请上传所有边框';
	            return $error;
    		}
    	}
    	if($params['bordertype'] == 1 && $params['borderpolicy'] == 1){
    		if(empty($params['sysBorderGroupId'])){
    			$error['info'] = '请选择边框';
	            return $error;
    		}
    	}
        return true;
    }
}
<?php
/**
 * @encoding	: UTF-8
 * @author		: wangdong
 * @datetime	: 2013.11.06
 * @Description  : 应用类型model
 */
class AdAreaModel extends MY_Model{
    //当前主要操作表
    protected $table = 'ad_area';
    function __construct()
    {
        parent::__construct();
    }

    function getAreaTree()
    {
        $data = $this->getList(array(), '*', 'psort asc, csort asc');
        $list = array();
        if(!empty($data))
        {
            foreach ($data as $v)
            {
                if(!isset($list[$v['pid']]))
                {
                    $list[$v['pid']] = array('pid'=>$v['pid'], 'pname'=>$v['pname']);
                }
            }
            foreach ($data as $v)
            {
                if(isset($list[$v['pid']]))
                {
                    $list[$v['pid']]['citylist'][] = array('cid'=>$v['cid'], 'cname'=>$v['cname']);
                }
            }
        }
        return $list;
    }
}
<?php
class AccountInfoModel extends MY_Model{
    public $table = 'account_info';

    public function getAccountSearchList($data)
    {
        $where = '';
        if(isset($data['auditstatus']))
        {
            if($data['auditstatus'] == 0){
                $where .= " and account_info.status = 1 and account_info.auditstatus = 0";
            }
            else
            {
                $where .= " and account_info.auditstatus = {$data['auditstatus']}";
            }
        }
        if(isset($data['search']) && $data['search'] != ''){
            $where .= "  and user_member.username like '%{$data['search']}%' ";
        }
        if(isset($data['auditstatus']) && $data['auditstatus'] != 1)
        {
            $orderby = 'order by createtime desc';
        }
        else
        {
            $orderby = 'order by createtime desc';
        }
        $sql = "SELECT `user_member`.`userid`, `user_member`.`username`, `account_info`.`id`, `account_info`.`accounttype`, `account_info`.`status`, `account_info`.`auditstatus`, `account_info`.`createtime` , `account_info`.`audittime`, `account_info`.`tag` FROM `user_member` INNER JOIN `account_info` ON `user_member`.`userid` = `account_info`.`userid` where 1 $where $orderby ";
        return $this->DB->read()->query($sql)->query_getall();
    }

    public function getAccountHistory($userid)
    {
        $where = 'account_info.userid = '.$userid;
        $orderby = 'order by account_info.createtime desc';
        $sql = "SELECT `user_member`.`userid`, `user_member`.`username`, `account_info`.* FROM `user_member` INNER JOIN `account_info` ON `user_member`.`userid` = `account_info`.`userid` where $where $orderby ";
        return $this->DB->read()->query($sql)->query_getall();
    }
    public function doDel($params)
    {
        if($this->getRow(array('id'=>$params['id'])) == false)
        {
            $error['data'] = array('positionidmsg', '广告位ID不存在，请刷新页面重试！');
            $error['info'] = 'postioniderr';
            return $error;
        }

        $data['id'] = $params['id'];
        $data['appid']= $params['appid'];
        $status = $this->edit($data, array('status' => 1));
        if($status)
        return array('status' => 1);
        return array('status' => 0);
    }

    public function getAccountInfo($id)
    {
        $accountInfo = $this->getRow(array('id'=>$id));
        if($accountInfo == false)
        {
            $error['data'] = array('该用户不存在！');
            $error['info'] = '';
            $error['status'] = 0;
        }
        else
        {
            $error['data'] = $accountInfo;
            $error['info'] = '';
            $error['status'] = 1;
        }
        return $error;
    }
}
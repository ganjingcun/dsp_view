<?php
class MarketModel extends MY_Model
{
    protected  $table = "market";

    function __construct() {
        parent::__construct();
    }

    /**
     * 添加应用商店
     */
    function doAdd($params)
    {
        $chk = $this->chkAddInfo($params);
        if($chk['status'] == 0)
        {
            return $chk;
        }
        $data = $params;
        $data['createtime'] = time();
        $admin = $this->session->userdata("admin");
        $data['adminid'] = $admin['admin_id'];
        $data['status'] = 0;
        $marketid = $this->add($data);
        if($marketid)
        {
            return array('status' => 1);
        }
        return array('status' => 0, 'data'=>array('', '未知错误！'), 'info' => '未知错误！');
    }

    /**
     * 编辑应用商店
     */
    function doEdit($where, $params)
    {
        $chk = $this->chkAddInfo($params);
        if($chk['status'] == 0)
        {
            return $chk;
        }
        $historyData = $this->getRow($where);
        if(empty($historyData))
        {
            return array('status' => 0, 'data'=>array('', '未知错误！'), 'info' => '未知错误！');
        }
        $data = $params;
        $data['edittime'] = time();
        $admin = $this->session->userdata("admin");
        $data['adminid'] = $admin['admin_id'];
        $num = $this->edit($where, $data);
        if($num)
        {
            return array('status' => 1, 'data' => '修改成功！');
        }
        return array('status' => 0, 'data'=>array('', '未做任何修改！'), 'info' => '未做任何修改！');
    }


    /**
     * 检测信息
     */
    private function chkAddInfo($params)
    {
        $error = array('status' => 1, 'data' => '');
    	if(empty($params['marketname']))
        {
            $error['data'] = array('marketnamemsg', '应用商店名称不能为空。');
            $error['info'] = 'marketnameerr';
            $error['status'] = 0;
            return $error;
        }
    	
    	if(empty($params['marketratio']))
        {
            $error['data'] = array('marketratiomsg', '应用商店分成比例不能为空。');
            $error['info'] = 'marketratioerr';
            $error['status'] = 0;
            return $error;
        }
   		if(!is_numeric($params['marketratio']) || $params['marketratio'] < 0){
   			$error['data'] = array('marketratiomsg', '应用商店分成比例必须是大于等于0的数字。');
            $error['info'] = 'marketratioerr';
            $error['status'] = 0;
            return $error;
   		}
    	if(!preg_match("/^(\d{1,3})(\.\d{0,2})?$/", $params['marketratio'])){
            $error['data'] = array('marketratiomsg', '应用商店分成比例格式错误！。');
            $error['info'] = 'marketratioerr';
            $error['status'] = 0;
            return $error;
        }
        
        
        if(empty($params['devratio']))
        {
            $error['data'] = array('devratiomsg', '开发者分成比例不能为空。');
            $error['info'] = 'devratioerr';
            $error['status'] = 0;
            return $error;
        }
    	if(!is_numeric($params['devratio']) || $params['devratio'] < 0){
    	 	$error['data'] = array('devratiomsg', '开发者分成比例必须是大于等于0的数字。');
            $error['info'] = 'devratioerr';
            $error['status'] = 0;
            return $error;
        }
    	if(!preg_match("/^(\d{1,3})(\.\d{0,2})?$/", $params['devratio'])){
            $error['data'] = array('devratiomsg', '开发者分成比例格式错误。');
            $error['info'] = 'devratioerr';
            $error['status'] = 0;
            return $error;
        }
   		
        if(empty($params['platformratio']))
        {
            $error['data'] = array('platformratiomsg', '平台分成比例不能为空。');
            $error['info'] = 'platformratioerr';
            $error['status'] = 0;
            return $error;
        }
    	if(!is_numeric($params['platformratio']) || $params['platformratio'] < 0){
    		$error['data'] = array('platformratiomsg', '平台分成比例必须是大于等于0的数字。');
            $error['info'] = 'platformratioerr';
            $error['status'] = 0;
            return $error;
        }
   	 	if(!preg_match("/^(\d{1,3})(\.\d{0,2})?$/", $params['platformratio'])){
            $error['data'] = array('platformratiomsg', '平台分成比例格式错误。');
            $error['info'] = 'platformratioerr';
            $error['status'] = 0;
            return $error;
        }
        if((floatval($params['marketratio'])+floatval($params['devratio'])+floatval($params['platformratio'])) != 100){
        	$error['data'] = array('marketnamemsg', '三者分成比例之和为100。');
            $error['info'] = 'marketnameerr';
            $error['status'] = 0;
            return $error;
        }
        return $error;
    }
}
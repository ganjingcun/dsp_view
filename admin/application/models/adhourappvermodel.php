<?php
class AdHourAppverModel extends MY_Model
{
    protected  $table = "ad_hour_appver";

    function __construct() {
        parent::__construct();
    }
    function getappver($appid=''){
        $this->load->model('AppChannelModel');
        $punchboxids = $this->AppChannelModel->getlist(array('appid'=>$appid),"publisherID");
        foreach ($punchboxids as $key=>$val){
            $pkPublisherIdInfo[] = $val['publisherID'];
        }
        $pkAppVer = array();
        if(!empty($pkPublisherIdInfo)){
            $pkPublisherIdsarr = implode(",", $pkPublisherIdInfo);
            $where['pkPublisherId'] = array('in',$pkPublisherIdsarr);
            $pkAppVer = $this->getList($where,"DISTINCT pkAppVer");
            foreach ($pkAppVer as $key=>$val){
                $appver = $val['pkAppVer'];
                $condition = array("pkPublisherId"=>array('in',$pkPublisherIdsarr),'pkAppVer'=>$appver,"verswitch"=>'1');
                $list = $this->getUnitList($condition,'app_channel','appver.pkPublisherId,channel.channelname','LEFT',array('appver.pkPublisherId','channel.publisherID') ,'appver.id DESC','','','','appver','channel');
                if(empty($list)) {
                    $pkAppVer[$key]['flg'] = "无";
                }else{
                    $pkAppVer[$key]['channel'] = $list;
                }
            }
        }
        return $pkAppVer;

    }
    function getappverwall($appid=''){
        $this->load->model('AppChannelModel');
        $punchboxids = $this->AppChannelModel->getlist(array('appid'=>$appid),"publisherID");
        foreach ($punchboxids as $key=>$val){
            $pkPublisherIdInfo[] = $val['publisherID'];
        }
        $pkAppVer = array();
        if(!empty($pkPublisherIdInfo)){
            $pkPublisherIdsarr = implode(",", $pkPublisherIdInfo);
            $where['pkPublisherId'] = array('in',$pkPublisherIdsarr);
            $pkAppVer = $this->getList($where,"DISTINCT pkAppVer");
            foreach ($pkAppVer as $key=>$val){
                $appver = $val['pkAppVer'];
                $condition = array("pkPublisherId"=>array('in',$pkPublisherIdsarr),'pkAppVer'=>$appver,"verwallswitch"=>'1');
                $list = $this->getUnitList($condition,'app_channel','appver.pkPublisherId,channel.channelname','LEFT',array('appver.pkPublisherId','channel.publisherID') ,'appver.id DESC','','','','appver','channel');
                if(empty($list)) {
                    $pkAppVer[$key]['flg'] = "无";
                }else{
                    $pkAppVer[$key]['channel'] = $list;
                }
            }
        }
        return $pkAppVer;
    }
    function getappverstatus($appid='',$appver=''){
        $this->load->model('AppChannelModel');
        $punchboxids = $this->AppChannelModel->getlist(array('appid'=>$appid),"publisherID");
        foreach ($punchboxids as $key=>$val){
            $pkPublisherIdInfo[] = $val['publisherID'];
        }
        $pkPublisherIdsarr = implode(",", $pkPublisherIdInfo);
        $where['pkPublisherId'] = array('in',$pkPublisherIdsarr);
        $pkAppVer = $this->getList($where,"DISTINCT pkAppVer");

        $condition = array("pkPublisherId"=>array('in',$pkPublisherIdsarr),'pkAppVer'=>$appver);
        $list = $this->getUnitList($condition,'app_channel','appver.id,appver.verswitch,appver.verwallswitch,appver.pkPublisherId,channel.channelname','LEFT',array('appver.pkPublisherId','channel.publisherID') ,'appver.id DESC','','','','appver','channel');
        return $list;
    }
    /**
     * 应用版本列表
     */
    function getvarlist($appid=''){
        $this->load->model('AppChannelModel');
        $punchboxids = $this->AppChannelModel->getlist(array('appid'=>$appid),"publisherID");
        foreach ($punchboxids as $key=>$val){
            $pkPublisherIdInfo[] = $val['publisherID'];
        }
        $pkAppVer = array();
        if(!empty($pkPublisherIdInfo)){
            $pkPublisherIdsarr = implode(",", $pkPublisherIdInfo);
            $where['pkPublisherId'] = array('in',$pkPublisherIdsarr);
            $pkAppVer = $this->getList($where,"DISTINCT pkAppVer");
        }
        return  $pkAppVer;
    }
}
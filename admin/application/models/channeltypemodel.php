<?php
class ChannelTypeModel extends MY_Model
{
    protected  $table = "channel_type";

    function __construct() {
        parent::__construct();
    }
    
    function getchanneltypelist($condition,$fileds = '*',$orderBy='',$groupBy='', $pageId = 1, $pageSize = 0){
	     try{
            $sql = "SELECT {$fileds} FROM `{$this->table}` ct left join `app_channel` ac on ct.`channelid`=ac.`channelid` left join `user_member` um on ct.`userid`=um.`userid` WHERE 1 ";
            $strWhere = $this->getWhere($condition);
            $this->sqlStrWhere = $this->sqlStrWhere!='' ? $this->sqlStrWhere . $strWhere : $strWhere;
            $sql.= empty($strWhere)?'':' AND '.$strWhere;
            $sql.= $this->groupBy = empty($groupBy)?'':' GROUP BY '.$groupBy;
            $sql.= empty($orderBy)?'':' ORDER BY '.$orderBy;
            $sql.= $this->limit($pageId,$pageSize);
            $this->lastsql = $sql;
            return $this->DB->read()->query($sql)->query_getall();
        }
        catch(Exception $e)
        {
            $this->showError($e);
        }
    }
    
    
 	function getchanneltypelistcount($where){
 	 	if($where){//增加查询条件
            $this->sqlStrWhere = $this->getWhere($where);
        }
        try
        {
            $sql = "SELECT count(*) num FROM `{$this->table}` WHERE 1 ";
            if(isset($this->sqlStrWhere))
            {
                $sql.= empty($this->sqlStrWhere)?'':' AND '.$this->sqlStrWhere;
            }
            else
            {
                return 0;
            }

            $sql .= $this->groupBy;
            $this->lastsql = $sql;
            $data = $this->DB->read()->query($sql)->query_getsingle();
            return $data['num'];
        }
        catch(Exception $e)
        {
            $this->showError($e);
        }
    }
    
    
    /**
     * edit android publisher devratio
     */
    function doEdit($where, $params)
    {
        $chk = $this->chkAddInfo($params);
        if($chk['status'] == 0)
        {
            return $chk;
        }
        $historyData = $this->getRow($where);
        if(empty($historyData))
        {
            return array('status' => 0, 'data'=>array('', '未知错误！'), 'info' => '未知错误！');
        }
        $data = $params;
        $data['ratioswitch'] = 1;
        $num = $this->edit($where, $data);
        if($num)
        {
            return array('status' => 1, 'data' => '修改成功！');
        }
        return array('status' => 0, 'data'=>array('', '未做任何修改！'), 'info' => '未做任何修改！');
    }
    
    
/**
     * 检测信息
     */
    private function chkAddInfo($params)
    {
        $error = array('status' => 1, 'data' => '');
   	 	if(empty($params['marketratio']))
        {
            $error['data'] = array('marketratiomsg', '应用商店分成比例不能为空。');
            $error['info'] = 'marketratioerr';
            $error['status'] = 0;
            return $error;
        }
   		if(!is_numeric($params['marketratio']) || $params['marketratio'] < 0){
   			$error['data'] = array('marketratiomsg', '应用商店分成比例必须是大于等于0的数字。');
            $error['info'] = 'marketratioerr';
            $error['status'] = 0;
            return $error;
   		}
    	if(!preg_match("/^(\d{1,3})(\.\d{0,2})?$/", $params['marketratio'])){
            $error['data'] = array('marketratiomsg', '应用商店分成比例格式错误！。');
            $error['info'] = 'marketratioerr';
            $error['status'] = 0;
            return $error;
        }
        
        
        if(empty($params['devratio']))
        {
            $error['data'] = array('devratiomsg', '开发者分成比例不能为空。');
            $error['info'] = 'devratioerr';
            $error['status'] = 0;
            return $error;
        }
    	if(!is_numeric($params['devratio']) || $params['devratio'] < 0){
    	 	$error['data'] = array('devratiomsg', '开发者分成比例必须是大于等于0的数字。');
            $error['info'] = 'devratioerr';
            $error['status'] = 0;
            return $error;
        }
    	if(!preg_match("/^(\d{1,3})(\.\d{0,2})?$/", $params['devratio'])){
            $error['data'] = array('devratiomsg', '开发者分成比例格式错误。');
            $error['info'] = 'devratioerr';
            $error['status'] = 0;
            return $error;
        }
   		
        if(empty($params['platformratio']))
        {
            $error['data'] = array('platformratiomsg', '平台分成比例不能为空。');
            $error['info'] = 'platformratioerr';
            $error['status'] = 0;
            return $error;
        }
    	if(!is_numeric($params['platformratio']) || $params['platformratio'] < 0){
    		$error['data'] = array('platformratiomsg', '平台分成比例必须是大于等于0的数字。');
            $error['info'] = 'platformratioerr';
            $error['status'] = 0;
            return $error;
        }
   	 	if(!preg_match("/^(\d{1,3})(\.\d{0,2})?$/", $params['platformratio'])){
            $error['data'] = array('platformratiomsg', '平台分成比例格式错误。');
            $error['info'] = 'platformratioerr';
            $error['status'] = 0;
            return $error;
        }
        if((floatval($params['marketratio'])+floatval($params['devratio'])+floatval($params['platformratio'])) != 100){
        	$error['data'] = array('marketnamemsg', '三者分成比例之和为100。');
            $error['info'] = 'marketnameerr';
            $error['status'] = 0;
            return $error;
        }
        return $error;
    }
}
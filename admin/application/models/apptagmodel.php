<?php

class AppTagModel extends MY_Model
{
    protected  $table = "app_tag";
    
    function __construct(){
        parent::__construct();
    }
    
    public function doAdd($params)
    {
        $params['tagname'] = trim($params['tagname']);
        $chk = $this->chkAddInfo($params);
        if($chk !== true)
        {
            $err['status'] = 0;
            $err = array_merge($err, $chk);
            return $err;
        }
        $appinfo = $this->CommonModel->table("app_info")->getRow(array("appid"=>$params['appid']));
        if(empty($appinfo)){
            return array('status' => 0, 'info' => 'tagiderr', 'data' => array('tagidmsg', '应用不存在。'));
        }
        $data['tagname'] = $params['tagname'];
        $data['status'] = 0;
        $data['userid'] = $appinfo['userid'];
        $data['createtime'] = time();
        $tagid = $this->add($data);
        if($tagid <= 0) return array('status' => 0, 'info' => 'tagnameerr', 'data' => array('tagnamemsg', '添加失败。'));
        return array('status' => 1, 'id' => $tagid);
    }
    
    private function chkAddInfo($params)
    {
        if(empty($params['tagname']))
        {
            $error['data'] = array('tagnamemsg', '标签名称不能为空。');
            $error['info'] = 'tagnameerr';
            return $error;
        }
        if(mb_strlen($params['tagname']) > 50)
        {
            $error['data'] = array('tagnamemsg', '标签名称不能超过50个字符。');
            $error['info'] = 'tagnameerr';
            return $error;
        }
        return true;
    }
}
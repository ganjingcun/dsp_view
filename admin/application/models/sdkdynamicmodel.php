<?php
class SdkDynamicModel extends MY_Model
{
    protected  $table = "sdkdynamic";

    function __construct() {
        parent::__construct();
    }
    
    public function getmaxversion($sdktype, $baseid=''){
    	$sql = "select version as maxversion from sdkdynamic where status=1 and sdktype=".$sdktype;
    	if($baseid){
    		$sql.=" and baseid=".$baseid;
    	}
    	$sql.=" order by createtime desc limit 1";
		$query = $this->DB->read()->query($sql)->query_getsingle();
        if(isset($query['maxversion'])){
            return $query['maxversion'];
        }
        return 0;
    }
    
    public function getLatestRow($sdktype, $baseid=''){
    	$sql="select * from sdkdynamic where status=1 and sdktype=".$sdktype;
    	if($baseid){
    		$sql.=" and baseid=".$baseid;
    	}
    	$sql.=" order by createtime desc limit 1";
		$query = $this->DB->read()->query($sql)->query_getsingle();
		return $query;
    }
}
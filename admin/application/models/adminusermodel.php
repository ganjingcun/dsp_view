<?php
class AdminUserModel extends MY_Model
{
	protected  $table = "admin_user";
	
	function getAdminName(){
	    $list = $this->getList(array());
	    return getArrayByKey($list,'admin_id');
	}
}
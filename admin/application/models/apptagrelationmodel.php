<?php

class AppTagRelationModel extends MY_Model
{
    protected  $table = "app_tag_relation";
    
    function __construct(){
        parent::__construct();
    }
    
    private function chkAddInfo($params)
    {
        if(empty($params['appid']))
        {
            $error['data'] = array('tagidmsg', '未知错误。');
            $error['info'] = 'tagiderr';
            return $error;
        }
        if(empty($params['tagid']))
        {
            $error['data'] = array('tagidmsg', '请选择标签。');
            $error['info'] = 'tagiderr';
            return $error;
        }
        return true;
    }
    
    public function doAdd($params)
    {
        $chk = $this->chkAddInfo($params);
        if($chk !== true)
        {
            $err['status'] = 0;
            $err = array_merge($err, $chk);
            return $err;
        }
        $appinfo = $this->CommonModel->table("app_info")->getRow(array("appid"=>$params['appid']));
        if(empty($appinfo)){
            return array('status' => 0, 'info' => 'tagiderr', 'data' => array('tagidmsg', '应用不存在。'));
        }
        $tagId = explode(',', $params['tagid']);
        $appTagList = $this->getList(array("appid"=>$params['appid']));
        if(!empty($appTagList))
        {
            foreach ($appTagList as $value) {
                $appTagIdArray[$value['tagid']] = $value['tagname'];
            }
        }
        $tagList = $this->CommonModel->table("app_tag")->getList(array());
        foreach ($tagList as $value) {
            $tagIdArray[$value['tagid']] = $value['tagname'];
        }
        foreach ($tagId as $v)
        {
            if(!empty($v))
            {
                if(isset($appTagIdArray[$v]))
                {
                    continue;
                }
                $data['userid'] = $appinfo['userid'];
                $data['appid'] = $params['appid'];
                $data['tagid'] = $v;
                $data['status'] = 1;
                if(!isset($tagIdArray[$v]))
                {
                    continue;
                }
                $data['tagname'] = $tagIdArray[$v];
                $data['createtime'] = time();
            }
            else
            {
                continue;
            }
            $dataBatch[] = $data;
        }
        if(empty($dataBatch))
        {
            return array('status' => 0, 'info' => 'tagiderr', 'data' => array('tagidmsg', '标签选择不能为空。'));
        }
        $id = $this->addBatch($dataBatch);
        if($id <= 0) return array('status' => 0, 'info' => 'tagiderr', 'data' => array('tagidmsg', '服务器保存失败。'));
        return array('status' => 1, 'id' => $id);
    }
}
<?php
class AdMgApplyModel extends MY_Model {
    public $table = 'ad_mg_apply';

    function getMoregameListByCateId($cateid, $cate){
        $time = strtotime(date('Y-m-d'));
        $endtime = '';
        if($cate['tplid'] != 2){
            $endtime = " and ag.endtime>=$time ";
        }
        $sql = "SELECT ami.*,ama.*,ads.adstuffname FROM ad_mg_apply ama LEFT JOIN `ad_mg_info` ami ON ami.storeid=ama.storeid left join ad_stuff ads on ads.storeid=ama.storeid AND ama.stuffid=ads.stuffid left join ad_group ag on ag.adgroupid=ads.adgroupid WHERE ama.status=1 and ama.cateid=$cateid  $endtime AND ama.`stuffid` != 0 order by ama.sortid desc,ama.uptime desc";
        //echo $sql;
        return $this->DB->read()->query_prepare($sql)->query_execute()->query_getall();
    }

    function getMoregameListByFree($cateid, $wyapptype = null, $order = '', $ostype = 2, $ismanual=0){
        $sql = "select id,catename from ad_mg_cate where type=1 and status=1 and tplid=2 limit 1";
        $cate = $this->DB->read()->query_prepare($sql)->query_execute()->query_getall();
        if(!$cate) return array();
        $pcateid = $cate[0]['id'];
        $where = " 1 = 1 ";
        if($ostype == 2)
        {
            if(!empty($wyapptype))
            {
                $where .= " and ai.wangyitype = {$wyapptype} and ai.ostype in (1,2) ";
            }
        }
        else
        {
        	if($ismanual){
        		$where .= " and ai.manual = 1 and ai.ostype = 3 ";
        	}elseif (!empty($wyapptype))
            {
                $where .= " and ai.wangyitype = {$wyapptype} and ai.ostype=3 ";
            }
        }
        if(!empty($order))
        {
            $order = "ORDER BY {$order} ";
        }
        $sql = "SELECT
                ai.id AS id,
                ai.appname,
                ai.`storeid`,
                ai.apptype,
                ai.`othername`,
                ai.`othertype`,
                ai.size,
                ai.icon,
                ai.price,
                ai.versioncode,
                ai.`wangyitype`,
                ai.`ostype`,
                am.id AS amaid,
                am.`status`,
                am.`isgather`,
                am.`cateid`,
  				am.sortid,
  				am.addtime,
  				am.uptime,
  				0 stuffid
              FROM
                `ad_mg_info` ai 
                LEFT JOIN 
                  (SELECT 
                    * 
                  FROM
                    `ad_mg_apply` 
                  WHERE `cateid` = {$cateid} 
                    AND `status` = 1 AND `stuffid`=0) am 
                  ON ai.`storeid` = am.`storeid` 
              where {$where} {$order} ";
        //echo $sql;
        return $this->DB->read()->query_prepare($sql)->query_execute()->query_getall();
    }
}
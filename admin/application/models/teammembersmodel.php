<?php
class TeamMembersModel extends MY_Model{
    public $table = 'team_members';
        
    public function doAdd($params)
    {
        $params['positionname'] = trim($params['positionname']);
        $data['positionid'] = $params['positionid'];
        $data['positionname'] = $params['positionname'];
        $data['positiondesc'] = $params['positiondesc'];
        $data['adform'] = $params['adform'];
        $data['appid']= $params['appid'];
        $data['createtime'] = time();
        $id = $this->add($data);
        if($id <= 0) return array('status' => 0, 'info' => 'positionnameerr', 'data' => array('positionnamemsg', '未知错误。'));
        return array('status' => 1, 'id' => $id);
    }
    
    public function doDel($params)
    {
        if($this->getRow(array('id'=>$params['id'])) == false)
        {
            $error['data'] = array('positionidmsg', '广告位ID不存在，请刷新页面重试！');
            $error['info'] = 'postioniderr';
            return $error;
        }

        $data['id'] = $params['id'];
        $data['appid']= $params['appid'];
        $status = $this->edit($data, array('status' => 1));
        if($status)
            return array('status' => 1);
        return array('status' => 0);
    }
    public function getMemberSearchList($data)
    {
        $where = '';
        if(isset($data['auditstatus']))
        {
            if($data['auditstatus'] == 0){
                $where .= " and team_members.status = 1 and team_members.auditstatus = 0";
            }
            else
            {
                $where .= " and team_members.auditstatus = {$data['auditstatus']}";
            }
        }
        if(isset($data['search']) && $data['search'] != ''){
            $where .= "  and user_member.username like '%{$data['search']}%' ";
        }
        if(isset($data['auditstatus']) && $data['auditstatus'] != 1)
        {
            $orderby = 'order by createtime desc';
        }
        else
        {
            $orderby = 'order by createtime desc';
        }
        $sql = "SELECT `user_member`.`userid`, `user_member`.`username`, `team_members`.`id`, `team_members`.`certtype`, `team_members`.`status`, `team_members`.`auditstatus`, `team_members`.`createtime` , `team_members`.`audittime` FROM `user_member` INNER JOIN `team_members` ON `user_member`.`userid` = `team_members`.`userid` where 1 $where $orderby ";
        return $this->DB->read()->query($sql)->query_getall();
    }
    
    public function getMemberHistory($userid)
    {
        $where = 'team_members.userid = '.$userid;
        $orderby = 'order by team_members.createtime desc';
        $sql = "SELECT `user_member`.`userid`, `user_member`.`username`, `team_members`.* FROM `user_member` INNER JOIN `team_members` ON `user_member`.`userid` = `team_members`.`userid` where  $where $orderby ";
        return $this->DB->read()->query($sql)->query_getall();
    }
    
    public function getMemberInfo($id)
    {
        $memberInfo = $this->getRow(array('id'=>$id));
        if($memberInfo == false)
        {
            $error['data'] = array('该用户不存在！');
            $error['info'] = '';
            $error['status'] = 0;
        }
        else
        {
            $error['data'] = $memberInfo;
            $error['info'] = '';
            $error['status'] = 1;
        }
        return $error;
    }
}
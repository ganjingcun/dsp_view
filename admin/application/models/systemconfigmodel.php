<?php
class SystemConfigModel extends MY_Model{
    public $table = 'system_config';
    function getSystemConfig($codename){
        $parentid = $this->getOne(array("skey"=>$codename), "id");
        $list = array();
        if($parentid){
            $list = $this->getList(array("parentid"=>$parentid),'skey,sval');
        }
        $tmp = array();
        foreach($list as $val){
            $tmp[$val['skey']] = $val['sval'];
        }
        return $tmp;
    }
    
    function getAllConfig(){
        $list = $this->getList(array(),"id,skey,sval,parentid","parentid asc");
        $tmp = array();
        $arr = array();
        foreach($list as $val){
            if($val['parentid']==0){
                $arrkey[$val['id']] = $val['skey'];
                $tmp[$val['skey']] = array();                
            }else{
                if(isset($arrkey[$val['parentid']])){
                    $tmp[$arrkey[$val['parentid']]][$val['skey']] = $val['sval'];
                }
            }
        }
        return $tmp;
    }
}
<?php
class AdGameListModel extends MY_Model
{
	protected  $table = "ad_game_list";
	
	function __construct() {
        parent::__construct();
    }

    /*function getCount(){
        $sql = "SELECT count(1) as count FROM `{$this->table}` id";// WHERE 1 $where
        $row = $this->DB->read()->query($sql)->query_getsingle();
        if(isset($row['count'])){
            return $row['count'];
        }
        return 0;
    }*/

    public function upLoad($type) {
        $this->load->library('MyUpload',$_FILES['Filedata']);
        $Img = new Img(new MyUpload($_FILES['Filedata']), $type);

        $return = array('isReceived'=>false,'imgInfo'=>array(),'errorMessage'=>array());
        if( $Img->upload() and $Img->handleImg())
        {
            $return['isReceived'] = true;
            $return['imgInfo'] = $Img->getImgConfig();
        }
        else
        {
            $return['errorMessage'] = $Img->error();
        }

        return $return;
    }

}

define('UPLOAD_STUFF_TMP_PATH',UPLOAD_DIR.'/stuff/tmp/'.date('Ym'));
define('UPLOAD_STUFF_IMG_PATH',UPLOAD_DIR.'/stuff/img/'.date('Ym'));
define('UPLOAD_STUFF_OVERLAY_PATH',UPLOAD_DIR.'/stuff/img/overlay');//水印小图标
define('STUFF_IMG_WIDTH',1280);
define('STUFF_IMG_HEIGHT',200);
define('IMAGICK_BIN', '/usr/local/ImageMagick/bin/');

class Img {
    private $image_lib = null;

    private $MyUpload = null;

    private $img = array();

    private $sourceImgPath = '';

    private $error = array();

    private $imgConfig = array();

    private $size = array('640*100'=>'100', '1280*200'=>'200', '180*180'=>180, '120*120'=>120, '900*600'=>600, '600*900'=>900, '1280*720'=>720, '720*1280'=>1280);

    private $height = 0;

    private $width =  0;

    /**
     *
     * @param CI_Image_lib $image_lib
     * @param MyUpload $MyUpload
     * @param unknown_type $imgType 上传的图片的比例 目前有两种  ‘1280*200’ ‘1456*180’
     * @return unknown_type
     */
    public function __construct(MyUpload $MyUpload,$imgType='120*120')
    {
        $this->MyUpload = $MyUpload;
        $imgSize = explode("*", $imgType);
        if($imgSize[1] == $this->size[$imgType])
        {
            $this->width = $imgSize[0];
            $this->height = $imgSize[1];
        }
    }

    public function upload()
    {

        $upload = $this->MyUpload;
        $upload->setUploadPath(UPLOAD_STUFF_TMP_PATH);

        $strError = '';

        if( !$upload->isAllowedTypes() )
        {
            $strError = '此处上传的文件不是规定的图片格式';
        }
        elseif( !$upload->isAllowedSize($this->width,$this->height))
        {
            $strError = '此处的图片尺寸必须是 '.$this->width.'*'.$this->height;
        }
        elseif( $upload->isBigerThanMaxSize() )
        {
            $strError = '此处的图片最大不能超过 '.intval($upload->getMaxsize()/1024) .'KB';
        }

        if(empty($strError) and $upload->upload())
        {
            $this->sourceImgPath = UPLOAD_DIR.$upload->getUplodedFilePath();
            $this->setConfig();
            return true;

        }
        $this->error[] = $strError;
        return false;
    }

    public function error()
    {
        return $this->error;
    }


    /**
     * 需要处理的目标图片的配置函数
     * @return unknown_type
     */
    public function setConfig()
    {
        $sorceImgExt = $this->getFileExt($this->sourceImgPath);
        $this->imgConfig = array(
	        array(
					'newImg' => UPLOAD_STUFF_TMP_PATH.'/'.$this->MyUpload->createKey().'.'.$sorceImgExt,//新图片存放路径
					'overlayImgPath' => UPLOAD_STUFF_OVERLAY_PATH.'/320_50.png',//水印图片路径
					'width' => $this->width,
					'height' => $this->height
		        )
        );

        if($this->width==120) {
        	$this->imgConfig = array(
                array(
                        'newImg'        => UPLOAD_STUFF_TMP_PATH.'/'.MyUpload::createKey().'.'.$sorceImgExt,//新图片存放路径
                        'overlayImgPath'=> UPLOAD_STUFF_OVERLAY_PATH.'/50_50.png',//水印图片路径
                        'width'         => 120,
                        'height'        => 120
                    ),
		        array(
						'newImg'		=> UPLOAD_STUFF_TMP_PATH.'/'.MyUpload::createKey().'.'.$sorceImgExt,//新图片存放路径
						'overlayImgPath'=> UPLOAD_STUFF_OVERLAY_PATH.'/50_50.png',//水印图片路径
						'width' 		=> 50,
						'height'		=> 50
			        )
	        );
        }
        elseif($this->width==180) {
            $this->imgConfig = array(
                array(
                        'newImg'        => UPLOAD_STUFF_TMP_PATH.'/'.MyUpload::createKey().'.'.$sorceImgExt,//新图片存放路径
                        'overlayImgPath'=> UPLOAD_STUFF_OVERLAY_PATH.'/162_162.png',//水印图片路径
                        'width'         => 162,
                        'height'        => 162
                    ),
                array(
                        'newImg'        => UPLOAD_STUFF_TMP_PATH.'/'.MyUpload::createKey().'.'.$sorceImgExt,//新图片存放路径
                        'overlayImgPath'=> UPLOAD_STUFF_OVERLAY_PATH.'/86_86.png',//水印图片路径
                        'width'         => 86,
                        'height'        => 86
                    )
            );
        }
        elseif($this->width==640) {
            //1280尺寸的图片配置
            $this->imgConfig = array(
	            array(
						'newImg'		=> UPLOAD_STUFF_TMP_PATH.'/'.MyUpload::createKey().'.'.$sorceImgExt,//新图片存放路径
						'overlayImgPath'=> UPLOAD_STUFF_OVERLAY_PATH.'/320_50.png',//水印图片路径
						'width' 		=> 320,
						'height'		=> 50
		            ),
	            array(
						'newImg'		=> UPLOAD_STUFF_TMP_PATH.'/'.MyUpload::createKey().'.'.$sorceImgExt,
						'overlayImgPath'=> UPLOAD_STUFF_OVERLAY_PATH.'/384_60.png',
						'width' 		=> 480,
						'height'		=> 75
		            ),
	            array(
						'newImg'		=> UPLOAD_STUFF_TMP_PATH.'/'.$this->MyUpload->createKey().'.'.$sorceImgExt,
						'overlayImgPath'=> UPLOAD_STUFF_OVERLAY_PATH.'/576_90.png',
						'width' 		=> 640,
						'height'		=> 100
		            ),
            );
        }
        elseif($this->width==1456) {
            //1456*180尺寸的图片配置
            $this->imgConfig = array(
	            array(
						'newImg'		=> UPLOAD_STUFF_TMP_PATH.'/'.MyUpload::createKey().'.'.$sorceImgExt,//新图片存放路径
						'overlayImgPath'=> UPLOAD_STUFF_OVERLAY_PATH.'/728_90.png',//水印图片路径
						'width' 		=> 728,
						'height'		=> 90
		            ),
	            array(
						'newImg'		=> UPLOAD_STUFF_TMP_PATH.'/'.$this->MyUpload->createKey().'.'.$sorceImgExt,//新图片存放路径
						'overlayImgPath'=> UPLOAD_STUFF_OVERLAY_PATH.'/320_50.png',//水印图片路径
						'width' 		=> 1456,
						'height'		=> 180
		            ),
            );
        }
        elseif($this->width==1280 && $this->height == 200){
            //1456*180尺寸的图片配置
            $this->imgConfig = array(
	            array(
						'newImg'		=> UPLOAD_STUFF_TMP_PATH.'/'.MyUpload::createKey().'.'.$sorceImgExt,//新图片存放路径
						'overlayImgPath'=> UPLOAD_STUFF_OVERLAY_PATH.'/728_90.png',//水印图片路径
						'width' 		=> 1080,
						'height'		=> 170
		            ),
	            array(
						'newImg'		=> UPLOAD_STUFF_TMP_PATH.'/'.$this->MyUpload->createKey().'.'.$sorceImgExt,//新图片存放路径
						'overlayImgPath'=> UPLOAD_STUFF_OVERLAY_PATH.'/320_50.png',//水印图片路径
						'width' 		=> 1280,
						'height'		=> 200
		            ),
            );
        }
        elseif($this->width==1280 && $this->height == 720) {
            //1456*180尺寸的图片配置
            $this->imgConfig = array(
	            array(
						'newImg'		=> UPLOAD_STUFF_TMP_PATH.'/'.$this->MyUpload->createKey().'.'.$sorceImgExt,//新图片存放路径
						'overlayImgPath'=> UPLOAD_STUFF_OVERLAY_PATH.'/320_50.png',//水印图片路径
						'width' 		=> 1280,
						'height'		=> 720
		            ),

	            );
        }
		elseif($this->width==1536) {
			//1456*180尺寸的图片配置
			$this->imgConfig = array(
				array(
						'newImg'		=> UPLOAD_STUFF_TMP_PATH.'/'.MyUpload::createKey().'.'.$sorceImgExt,//新图片存放路径
						'overlayImgPath'=> UPLOAD_STUFF_OVERLAY_PATH.'/728_90.png',//水印图片路径
						'width' 		=> 480,
						'height'		=> 60
					),
    			array(
		    			'newImg'		=> UPLOAD_STUFF_TMP_PATH.'/'.$this->MyUpload->createKey().'.'.$sorceImgExt,//新图片存放路径
		    			'overlayImgPath'=> UPLOAD_STUFF_OVERLAY_PATH.'/320_50.png',//水印图片路径
		    			'width' 		=> 1536,
		    			'height'		=> 190
	    			),
			);
		}
    }

    /**
     * 处理图片
     * @return unknown_type
     */
    public function handleImg() {

        if(empty($this->sourceImgPath)) return false;

        foreach( $this->imgConfig as $k => &$row )
        {
            $tmp = dirname($row['newImg']).'/'.basename($row['newImg'],'.jpg').$k.'-tmp.jpg';

            //图片缩放，命令行
            exec("convert -scale  {$row['width']}x{$row['height']}! {$this->sourceImgPath} {$tmp}",$out);

            //加水印命令
            //取消添加水印 王栋 2012.12.13
            //if(time()>strtotime('2012-11-12')){
            //exec("/usr/local/ImageMagick/bin/composite -gravity SouthEast {$row['overlayImgPath']} {$tmp} {$row['newImg']}");
            //}else{
            exec("cp {$tmp} {$row['newImg']}");
            //}
            }
            return true;
    }

    public function getImgConfig($isAbsolute=false) {
        $sourceImg = array(
				'newImg'	=> $this->sourceImgPath,
				'width'		=> $this->width,
				'height'	=> $this->height,
				'overlayImgPath'=>''
			);
		$img = $this->imgConfig;

		if(!$isAbsolute) {
		    foreach( $img as $k => $row ) {
		        unset($img[$k]['overlayImgPath']);
		        $img[$k]['newImg'] = str_replace(UPLOAD_DIR,'',$row['newImg']);
		    }
		    return $img;
		}
		return $img;
    }

    public function unlinkAllImg() {
        foreach( $this->imgConfig as $k => $row ){
            @unlink($row['newImg']);
        }
    }

    /**
     * 获取文件后缀名
     * @return string|null
     */
    public function getFileExt($file) {
        $x = explode('.', $file);
        return strtolower(end($x));
    }

}

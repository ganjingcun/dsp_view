<?php
/**
 * @encoding	: UTF-8
 * @author		: wf
 * @datetime	: 2015.08.03
 * @Description  : 应用类型model
 */
class AdAreaCountryModel extends MY_Model{
    //当前主要操作表
    protected $table = 'ad_area_country';
    function __construct()
    {
        parent::__construct();
    }

    function getAreaList()
    {
        $data = $this->getList(array(), '*', 'internalid asc');
        if(empty($data))
        {
            return false;
        }
        return $data;
    }
}
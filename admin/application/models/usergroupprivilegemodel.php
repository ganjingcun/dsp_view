<?php
class UserGroupPrivilegeModel extends MY_Model
{
	protected  $table = "user_group_privilege";
	
	function __construct() {
		parent::__construct();
	}
	
	function getGroup($data=array()){
		$list = $this->getList($data);
		$tmp = array();
		foreach($list as $val){
			$tmp[$val['groupid']] = $val['groupname'];
		}
		return $tmp;
	}

}


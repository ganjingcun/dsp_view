<?php
class AdStuffMoregameModel extends MY_Model
{
    protected  $table = "ad_stuff_moregame";

    function __construct() {
        parent::__construct();
    }

    function getMoregameTop($os, $istop, $search=''){
        if($search){
            $search = " and ag.adgroupname like '%$search%'";
        }
        $sql = "select ac.campaignid,ag.adgroupid,ac.campaignname,ag.adgroupname,ag.starttime,ag.endtime,asm.appname,asm.appcontent,asm.path,asm.textid,asm.integralsort,asm.isintegral,asm.integraltime from ad_stuff_moregame asm left join ad_stuff ads on ads.stuffid=asm.stuffid left join ad_group ag on ag.adgroupid=ads.adgroupid left join ad_campaign ac on ac.campaignid=ag.campaignid where ads.status = 1 AND ag.`adform` = 3 and  ac.ostypeid=$os and asm.istop=$istop   and ag.endtime>= ".(TIME-86400)." $search order by asm.integraltime DESC";//echo $sql;
        $list = $this->DB->read()->query($sql)->query_getall();
        return $list;
    }

    function getAppendMoregame($os, $istop, $search=''){
        if($search){
            $search = " and ag.adgroupname like '%$search%'";
        }
        $sql = "select ac.campaignid,ag.adgroupid,ac.campaignname,ag.adgroupname,ag.starttime,ag.createtime,ag.endtime,asm.appname,asm.appcontent,asm.path,asm.textid,asm.integralsort,asm.isintegral,asm.integraltime from ad_stuff_moregame asm left join ad_stuff ads on ads.stuffid=asm.stuffid left join ad_group ag on ag.adgroupid=ads.adgroupid left join ad_campaign ac on ac.campaignid=ag.campaignid where ads.status = 1 AND ag.`adform` = 3 and ac.ostypeid=$os and asm.isintegral=$istop  and ag.endtime>= ".(TIME-86400)." $search order by asm.integralsort asc,asm.integraltime desc";//echo $sql;
        $list = $this->DB->read()->query($sql)->query_getall();
        return $list;
    }

    function getIsIntegralMoregame($os){
        $sql = "select ac.campaignid,ag.adgroupid,ac.campaignname,ag.adgroupname,ag.starttime,ag.endtime,asm.appname,asm.appcontent,asm.path,asm.textid,asm.integralsort,asm.isintegral,asm.integraltime from ad_stuff_moregame asm left join ad_stuff ads on ads.stuffid=asm.stuffid left join ad_group ag on ag.adgroupid=ads.adgroupid left join ad_campaign ac on ac.campaignid=ag.campaignid where ac.ostypeid=$os and asm.isintegral=1 AND ag.`adform` = 3 order by asm.integralsort asc,asm.integraltime desc";//echo $sql;
        $list = $this->DB->read()->query($sql)->query_getall();
        return $list;
    }

    function getMaxOrder($os)
    {
        $sql = "select max(asm.integralsort) as `max` from ad_stuff_moregame asm left join ad_stuff ads on ads.stuffid=asm.stuffid left join ad_group ag on ag.adgroupid=ads.adgroupid left join ad_campaign ac on ac.campaignid=ag.campaignid where ac.ostypeid=$os and  asm.isintegral=1  AND ag.`adform` = 3 and ag.endtime>= ".(TIME-86400);//echo $sql;
        $list = $this->DB->read()->query($sql)->query_getall();
        if(!empty($list))
        {
            return $list[0]['max'];
        }
        return 0;
    }
    /**
     * 广告组 修改 标示 +1
     */
    function updateisupdate($textid='',$table=''){
        $sql = "SELECT stuffid FROM ".$table." WHERE textid IN (".$textid.");";
        $stuffid = $this->DB->read()->query($sql)->query_getall();
        foreach($stuffid as $key=>$val){
            $stuffidarr[] = $val['stuffid'];
        }
        $stuffids = implode(",", $stuffidarr);
        $sql = "SELECT campaignid FROM ad_stuff WHERE stuffid IN (".$stuffids.");";
        $campaignid = $this->DB->read()->query($sql)->query_getall();
        foreach($campaignid as $key=>$val){
            $campaignarr[] = $val['campaignid'];
        }
        $campaignids = implode(",", $campaignarr);
        $sql = "UPDATE ad_campaign SET isupdate = isupdate+1 where campaignid in (".$campaignids.")";
        $this->DB->read()->query($sql);
    }
    function updateisupdate2($stuffids='',$table=''){
        if($table == 'ad_stuff'){
            $sql = "SELECT campaignid FROM ".$table." WHERE stuffid IN (".$stuffids.");";
        }else if($table == 'ad_group'){
            $sql = "SELECT campaignid FROM ".$table." WHERE adgroupid IN (".$stuffids.");";
        }
        $campaignid = $this->DB->read()->query($sql)->query_getall();
        foreach($campaignid as $key=>$val){
            $campaignarr[] = $val['campaignid'];
        }
        $campaignids = implode(",", $campaignarr);
        $sql = "UPDATE ad_campaign SET isupdate = isupdate+1 where campaignid in (".$campaignids.")";
        $this->DB->read()->query($sql);
    }

    /**
     * 获取应用列表
     * Enter description here ...
     * @param unknown_type $os
     * @param unknown_type $istop
     * @param unknown_type $search
     */
    function getAPIList($os, $isapi, $search=''){
        if($search){
            $search = " and ag.adgroupname like '%$search%'";
        }
        $sql = "select ag.price,ac.campaignid,ag.adgroupid,ac.campaignname,ag.adgroupname,ag.starttime,ag.endtime,asm.appname,asm.appcontent,asm.path,asm.textid,asm.stuffid,asm.apitime,asm.apisort,asm.isapi from `ad_stuff_moregame` asm left join ad_stuff ads on ads.stuffid=asm.stuffid left join ad_group ag on ag.adgroupid=ads.adgroupid left join ad_campaign ac on ac.campaignid=ag.campaignid where ads.status = 1 AND ag.`adform` = 3 and ac.targettype = 1 and ac.ostypeid=$os and asm.isapi=$isapi and ag.starttime<=".TIME." and ag.endtime>= ".(TIME-86400)." $search order by asm.apitime desc,ads.lastupdatetime desc";
        //echo $sql;
        $list = $this->DB->read()->query($sql)->query_getall();
        return $list;
    }


}
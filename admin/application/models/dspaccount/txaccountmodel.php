<?php
class TXAccountModel extends MY_Model
{
    //腾讯申请地址
    private $txurl = "http://api.e.qq.com/luna/v2/adnetwork_general/initial?auth_type=TokenAuth";
    private $agid = '844656';//203643';
    private $appid = '844656';//'203643';
    private $key = 'a2158d3d28e77403d178ef74316798f7';//ec09540cddb3453c3c20eb9e3ea96147';
    private $time = null;
    function __construct() {
        parent::__construct();
        $this->time = time();
    }

    //注册banner广告位
    function getBannerPlacement($data)
    {
        $data['agid'] = $this->agid;
        $data['appid'] = $this->appid;
        $data['key'] = $this->key;
        return $this->getAccount($data);
    }

    //注册插屏广告位
    function getPupopPlacement($data)
    {
        $data['agid'] = $this->agid;
        $data['appid'] = $this->appid;
        $data['key'] = $this->key;
        return $this->getAccount($data);
    }

    //注册
    private function getAccount($data)
    {
        $data['token'] = $this->getQQToken();
        $str = http_build_query($data);
        $getData = curlSend($data, $this->txurl);
        if(empty($getData))
        {
            return false;
        }
        //placement_id为int64的数字，php解析时有可能会溢出，需要先替换成字符串在解析。
        $getData = preg_replace('/"placement_id":([0-9]+)/', '"placement_id":"$1"', $getData);
        $reData = json_decode($getData, true);
        return $reData;
    }

    /**
     * 生成QQ接口Token
     * //agid=203643,appkey=ec09540cddb3453c3c20eb9e3ea96147
     */
    function getQQToken()
    {
        $sign = $this->getQQSign();
        $token = base64_encode($this->agid .','. $this->appid .','. $this->time .','. $sign);
        return $token;
    }

    /**
     *
     */
    function getQQSign()
    {
        $sign = sha1($this->appid. $this->key. $this->time);
        return $sign;
    }
}
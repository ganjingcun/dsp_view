<?php
class CommonModel extends MY_Model
{
    protected  $table = "";

    function __construct() {
        parent::__construct();
    }
    
    function table($table){
        $this->table = $table;
        return $this;
    }
}
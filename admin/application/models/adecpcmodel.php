<?php
class AdEcpcModel extends MY_Model
{
    protected  $table = "ad_ecpc";

    function __construct() {
        parent::__construct();
    }

    /**
     * 获取当前有效的ecpc设置
     * Enter description here ...
     */
   function getDefaultRatio(){
       $list = $this->getList(array('status'=>1), "*", "ostype asc,addtime desc, adtype asc");
       $tmp = array();
       foreach($list as $val){
           $tmp[$val['ostype']][$val['adtype']] = $val;
       }
       return $tmp;
   }
}
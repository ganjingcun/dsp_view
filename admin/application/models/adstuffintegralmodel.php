<?php
class AdStuffIntegralModel extends MY_Model
{
    protected  $table = "ad_stuff_integral";

    function __construct() {
        parent::__construct();
    }

    function getMoregameTop($os, $istop, $search='', $where = 1){
        if($search){
            $search = " and ag.adgroupname like '%$search%'";
        }
        if($istop === 0 or $istop == 1) $istop = "and asm.istop= $istop";
        if($where)
        {
            $where = "and ag.starttime<=".TIME." and ag.endtime>= ".(TIME-86400);
        }
        $sql = "select ac.campaignid,ag.adgroupid,ac.campaignname,ag.antifraud,ag.adgroupname,ag.starttime,ag.endtime,asm.appname,asm.appcontent,asm.path,asm.textid,asm.stuffid,asm.integralsort,asm.istop,asm.integraltime from ad_stuff_integral asm left join ad_stuff ads on ads.stuffid=asm.stuffid left join ad_group ag on ag.adgroupid=ads.adgroupid left join ad_campaign ac on ac.campaignid=ag.campaignid where ads.status = 1 and  ac.ostypeid=$os $istop $where $search order by asm.integralsort asc,asm.integraltime desc";//echo $sql;
        $list = $this->DB->read()->query($sql)->query_getall();
        return $list;
    }
    function getMoregamebyid($adgroupid=''){
        
    $sql = "select ac.campaignid,ag.adgroupid,ac.campaignname,ag.antifraud,ag.adgroupname,ag.starttime,ag.endtime,asm.appname,asm.appcontent,asm.path,asm.textid,asm.stuffid,asm.integralsort,asm.istop,asm.integraltime from ad_stuff_integral asm left join ad_stuff ads on ads.stuffid=asm.stuffid left join ad_group ag on ag.adgroupid=ads.adgroupid left join ad_campaign ac on ac.campaignid=ag.campaignid where ads.status = 1 and  ag.adgroupid=$adgroupid"; ;
    $list = $this->DB->read()->getsingle($sql);
        return $list;
    }
    function getIsIntegralMoregame($os){
    $sql = "select ac.campaignid,ag.adgroupid,ac.campaignname,ag.adgroupname,ag.starttime,ag.endtime,asm.appname,asm.appcontent,asm.path,asm.textid,asm.stuffid,asm.integralsort,asm.istop,asm.integraltime from ad_stuff_integral asm left join ad_stuff ads on ads.stuffid=asm.stuffid left join ad_group ag on ag.adgroupid=ads.adgroupid left join ad_campaign ac on ac.campaignid=ag.campaignid where ac.ostypeid=$os and asm.integralsort > 0 order by asm.integralsort asc,asm.integraltime desc";//echo $sql;
    $list = $this->DB->read()->query($sql)->query_getall();
        return $list;
    }


    function getMaxOrder($os)
    {
    $sql = "select max(asm.integralsort) as max from ad_stuff_integral asm left join ad_stuff ads on ads.stuffid=asm.stuffid left join ad_group ag on ag.adgroupid=ads.adgroupid left join ad_campaign ac on ac.campaignid=ag.campaignid where ac.ostypeid=$os and asm.istop=1 and ag.starttime<=".TIME." and ag.endtime>= ".(TIME-86400);//echo $sql;
    $list = $this->DB->read()->query($sql)->query_getall();
        if(!empty($list))
        {
            return isset($list[0]['max'])?$list[0]['max']:0;
        }
        return 0;
    }
    
/**
 * 葫蘆商城設置
 * Enter description here ...
 * @param unknown_type $os
 * @param unknown_type $istop
 * @param unknown_type $search
 */
    function getHuLuStore($os, $ishulu, $search=''){
        if($search){
        $search = " and ag.adgroupname like '%$search%'";
        }
        $sql = "select ac.campaignid,ag.adgroupid,ac.campaignname,ag.adgroupname,ag.starttime,ag.endtime,asm.appname,asm.appcontent,asm.path,asm.textid,asm.stuffid,asm.integralsort,asm.istop,asm.hulutime,asm.`tasksummary` from ad_stuff_integral asm left join ad_stuff ads on ads.stuffid=asm.stuffid left join ad_group ag on ag.adgroupid=ads.adgroupid left join ad_campaign ac on ac.campaignid=ag.campaignid where ads.status = 1 and  ac.ostypeid=$os and asm.ishulu=$ishulu and ag.starttime<=".TIME." and ag.endtime>= ".(TIME-86400)." $search order by asm.integralsort asc,asm.integraltime desc";//echo $sql;
        $list = $this->DB->read()->query($sql)->query_getall();
        return $list;
    }
}
<?php
class AdexchangeModel extends MY_Model
{
	protected  $table = "ad_exchange";
	function __construct() {
		parent::__construct();
	}
	
	/*
	 * 添加adx
	 */
	public function doAdd($params){
		$user = $this->session->userdata("admin");
		$data['admin_id'] = $user['admin_id'];
        $data['adxname'] = trim($params['adxname']);
        $data['adxid'] = $params['adxid'];
        $data['iospid'] = $params['iospid'];
        $data['androidpid'] = $params['androidpid'];
        $data['createtime'] = time();
        $id = $this->add($data);
        if($id <= 0) 
        	return false;
        return true;
    }
	
}
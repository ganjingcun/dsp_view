<?php
class AdMgcateRelationModel extends MY_Model {
	public $table = 'ad_mg_cate_relation';
	private $_ac_fields = " ac.campaignid,ac.devicetypeset device_types,ac.ostypeid ostype,";
	private $_ag_fields = " ag.`adgroupid`,";
	private $_ads_fields = " ads.`adstuffname` creative_title,ads.`stuffid` adid,ads.storeid,";

	private $_where = ' and ads.status=1 and ag.status=1 and ac.status < 3 ';//and ac.ostypeid=2 ';

	function getApplyList($cateid) {
		$fields = '*';
		$sql = "select $fields from ad_mg_apply ama left join ad_mg_info ami on ama.storeid=ami.storeid where ama.cateid = $cateid and ama.`status`=1 order by ama.sortid desc, ama.addtime desc";
		return $this->DB->read()->query_prepare($sql)->query_execute()->query_getall();
	}

	function getRellationList($pid='0') {
 		$sql = "SELECT  r.cateid FROM ad_mg_cate_relation r WHERE pid!= $pid and pid IN(SELECT id  FROM ad_mg_cate  WHERE STATUS = 1 AND TYPE = 1 AND ostype = 2 AND appid = (SELECT appid FROM ad_mg_cate WHERE id = $pid AND STATUS = 1 AND TYPE = 1 AND ostype = 2))";
		return $this->DB->read()->query_prepare($sql)->query_execute()->query_getall();
	}
    /**
     * 获取一级栏目对应的二级栏目
     */
    function getCateKidList($pid='0'){
        $sql = "SELECT r.* ,cate.catename,cate.tplid FROM ad_mg_cate_relation r LEFT JOIN ad_mg_cate cate ON r.cateid = cate.id WHERE r.pid=".$pid;
        return $this->DB->read()->query_prepare($sql)->query_execute()->query_getall();
    }
}
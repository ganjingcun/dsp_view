<?php
class AdEcpcAppModel extends MY_Model
{
    protected  $table = "ad_ecpc_app";
    
    function getOSAppList($os = 1)
    {
        $sql = "SELECT aa.* FROM `ad_ecpc_app` aa INNER JOIN app_info ai ON aa.appid = ai.appid WHERE ai.`ostypeid` = {$os} AND aa.`status` = 1";
        $list = $this->DB->read()->query_prepare($sql)->query_execute()->query_getall();
        return $list;
    }
    
    
}
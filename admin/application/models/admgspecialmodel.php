<?php

define('UPLOAD_SPECIAL_PATH', UPLOAD_DIR . '/app/spec' . date('Ym'));

class AdMgSpecialModel extends MY_Model {

	public $table = 'ad_mg_special';

	public function upLoad($type) {
		$this->load->library('MyUpload', $_FILES['Filedata']);
		$upload = new MyUpload($_FILES['Filedata']);
		$upload->setUploadPath(UPLOAD_SPECIAL_PATH);
		
		$upload->setFileExt(array('jpg', 'png'));

		$upload->setMaxsize(120 * 1024);

		$strError = '';
		$data = array('isReceived'=>false,'imgInfo'=>array(),'errorMessage'=>array());
		if (!$upload->isAllowedTypes()) {
			$strError = '您上传的文件格式错误 !';
		} elseif ($upload->isBigerThanMaxSize()) {
			$strError = '此处的文件最大不能超过 ' . intval($upload->getMaxsize() / 1024) . 'KB';
		}
		if(!$upload->isAllowedSize('640', '220')){
			$strError .= " 图片的尺寸必须为：640 X 220";
		}

		if (empty($strError) and $upload->upload()) {
			$imgPath = $upload->getUplodedFilePath();
			$data['imgInfo']['newImg'] = $imgPath;
			$data['newImg'] = $imgPath;
			$data['isReceived'] = 1;
		} else {
			$data['errorMessage'] = $strError;
			$data['isReceived'] = 0;
		}
		return $data;
	}

	function getApplyList(){
		$sql = "select ami.storeid,ami.appname from ad_mg_info ami left join ad_mg_apply ama on ama.storeid = ami.storeid where ama.status=1 group by ami.storeid ";
		return $this->DB->read()->query_prepare($sql)->query_execute()->query_getall();
	}

}
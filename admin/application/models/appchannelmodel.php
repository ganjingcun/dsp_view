<?php
class AppChannelModel extends MY_Model
{
    protected  $table = "app_channel";

    function __construct() {
        parent::__construct();
    }

    /**
     * 获取渠道基本信息
     * Enter description here ...
     * @param $appId
     * @param $userId
     */
    public function getChannelTypeList($ostypeId, $userId)
    {
        if($ostypeId == 1)
        {
            $table = 'channel_type';
        }
        else
        {
            $table = 'channel_ios_type';
        }
        return $this->getSqlQuery("select * from $table where 1 and userid = $userId order by status asc,parentid ,channelid desc");
    }


    /**
     * 获取渠道基本信息
     * Enter description here ...
     * @param $appId
     * @param $userId
     */
    public function getParentChannelList($ostypeId, $userId)
    {
        if($ostypeId == 1)
        {
            $this->table = 'channel_type';
        }
        else
        {
            $this->table = 'channel_ios_type';
        }
        return $this->getList(array('userid'=>0, 'parentid'=> 0, 'status'=>0), '*' ,' sort desc');
    }

    /**
     * 获取渠道基本信息
     * Enter description here ...
     * @param $appId
     * @param $userId
     */
    public function getChannelInfo($channelId, $ostypeId)
    {
        if($ostypeId == 1)
        {
            $this->table = 'channel_type';
        }
        else
        {
            $this->table = 'channel_ios_type';
        }
        if(empty($channelId) || empty($ostypeId))
        {
            return array('status' => 0);
        }
        $where['channelid'] = $channelId;
        $info = $this->getRow($where, 'channelid,channelname,parentid,inchlid');
        if(empty($info))
        {
            return array('status' => 0);
        }
        return array('status' => 1, 'info'=>$info);
    }

    
    /**
     * 根据条件查询app_channel表数据
     * @param array $cond
     * @return multitype:
     */
    function getChannelByCond(array $cond) {
    	return $this->getList($cond);
    }
    
    /**
     * 获取渠道基本信息
     * Enter description here ...
     * @param $appId
     * @param $userId
     */
    public function getChannelSearchList($ostypeId, $key, $userId='',$channeltypes='')
    {
        $data = $this->getChannelTypeList($ostypeId, 0);

        $parentdata = array();
        $childdata = array();

        foreach($data as $val)
        {
            if($val['parentid'] > 0)
            {
                if(!empty($channeltypes) && in_array($val['channelid'],$channeltypes)){
                    $childdata[$val['channelid']] = $val;
                }elseif(empty($channeltypes)){
                    $childdata[$val['channelid']] = $val;
                }
            }
            else
            {
                $parentdata[$val['channelid']] = $val;
            }
        }
        $returndata = array();
        foreach($childdata as $val)
        {
            if(!empty($key) && strpos($val['channelname'], $key) === false && strpos($val['inchlid'], $key) === false )
            {
                continue;
            }
            $returndata[$val['channelid']] = $val;
            $returndata[$val['channelid']]['parentname'] = $parentdata[$val['parentid']]['channelname'];
        }
        return $returndata;
    }

    /**
     * 申请渠道PunchboxId
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    public function doAddChannel($params)
    {
        $chk = $this->chkAddChannelInfo($params);
        if($chk !== true)
        {
            $err['status'] = 0;
            $err = array_merge($err, $chk);
            return $err;
        }
        $channelList = explode(',', $params['channelid']);
        $this->load->model('AppInfoModel');
        $appInfo = $this->AppInfoModel->getRow(array('appid'=>$params['appid']));
        $channelTyepList = $this->getChannelTypeList($appInfo['ostypeid'], $appInfo['userid']);
        foreach ($channelTyepList as $value) {
            $channelIdsArray[$value['channelid']] = $value['channelname'];
        }

        $this->load->library('UUID');
        foreach ($channelList as $v)
        {
            if(!empty($v))
            {
                $data['userid'] = $appInfo['userid'];
                $data['appid'] = $params['appid'];
                $data['channelid'] = $v;
                if(!isset($channelIdsArray[$v]))
                {
                    continue;
                }
                $data['channelname'] = $channelIdsArray[$v];
                $data['publisherID'] = $params['appid'].'-'.UUID::getUUID();;
                $data['createtime'] = time();
            }
            else
            {
                continue;
            }
            $dataBatch[] = $data;
        }
        if(empty($dataBatch))
        {
            return array('status' => 0, 'info' => 'channeliderr', 'data' => array('channelidmsg', '未知错误。'));
        }
        $id = $this->addBatch($dataBatch);
        if($id <= 0) return array('status' => 0, 'info' => 'channeliderr', 'data' => array('channelidmsg', '未知错误。'));
        return array('status' => 1, 'id' => $id);
    }

    /**
     * 检测渠道信息
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    private function chkAddChannelInfo($params)
    {
        if(empty($params['channelid']) )
        {
            $error['data'] = '';
            $error['info'] = 'channeliderr';
            return $error;
        }
        if( empty($params['appid']))
        {
            $error['data'] = array('channelidmsg', '未知错误！');
            $error['info'] = 'channeliderr';
            return $error;
        }
        return true;
    }

    /**
     * 保存渠道信息
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    function save($params)
    {
        if($params['ostypeid'] == 1)
        {
            $this->table = "channel_type";
        }
        else
        {
            $this->table = "channel_ios_type";
        }
        if($params['subtype'] == 'edit')
        {
            $row = $this->getRow(array('channelname'=>$params['channelname'], 'userid'=>0, 'status' => 0, 'channelid' => array('NEQ',$params['channelid'])));
            if(empty($params['inchlid']))
            {
                $rownum = 0;
            }
            else
            {
                $rownum = $this->getRow(array('inchlid'=>$params['inchlid'], 'userid'=>0, 'status' => 0, 'channelid' => array('NEQ',$params['channelid'])));
            }
            if($row > 0 || $rownum > 0)
            {
                return -1;
            }
            $num = $this->edit(array('channelid'=>$params['channelid']), array('channelname'=>$params['channelname'], 'inchlid'=>$params['inchlid'], 'parentid'=>$params['channeltype']));
        }
        else
        {
            $row = $this->getRow(array('channelname'=>$params['channelname'], 'status' => 0, 'userid'=>0));
            if(empty($params['inchlid']))
            {
                $rownum = 0;
            }
            else
            {
                $rownum = $this->getRow(array('inchlid'=>$params['inchlid'], 'status' => 0, 'userid'=>0));
            }
            if($row > 0 || $rownum > 0)
            {
                return -1;
            }
            $num = $this->add(array('channelname'=>$params['channelname'], 'inchlid'=>$params['inchlid'], 'parentid'=>$params['channeltype']));
        }
        return $num;
    }

    function del($channelid, $ostypeid){
        if($ostypeid == 1)
        {
            $this->table = "channel_type";
        }
        else
        {
            $this->table = "channel_ios_type";
        }
        $data['status'] = 1;
        $where['channelid'] = $channelid;
        $num = $this->edit($where, $data);
        if($num <= 0) return array('status' => 0);
        return array('status' => 1);
    }
    /**
     * 获取渠道类别
     */
    function getChannelType($channeltype='',$ostypeId=''){
        $info= array();
        if(!empty($channeltype)){
            if($ostypeId == 1) {
                $this->table = 'channel_type';
            }else{
                $this->table = 'channel_ios_type';
            }
            $info = $this->getList(array('userid'=>0,  'status'=>0,'parentid'=>$channeltype), 'channelid');

        }
        return $info;
    }      
    
    /**
     * 搜索时获取匹配的pid列表
     */
    public function getPidList($data){
    	$where = '';
        if(isset($data['publisherID'])){
            $where .= " and app_channel.publisherID like '%{$data['publisherID']}%'";
        }
        if($data['ostypeid']) $where .= " and app_info.ostypeid = ".$data['ostypeid'];
        $orderby = 'order by app_channel.id desc';
        $sql = "select app_channel.publisherID from app_channel right join app_info on app_channel.appid = app_info.appid where 1 $where $orderby ";
        return $this->DB->read()->query($sql)->query_getall();
    }
    
    function getAdexchangeAppidList(){
    	$sql = "select ac.appid from app_channel ac inner join ad_exchange ae on ac.publisherID=ae.iospid or ac.publisherID=ae.androidpid";
    	return $this->DB->read()->query($sql)->query_getall();
    }
    
    
    
    function getchanneluserlist($condition,$fileds = '*',$orderBy='',$groupBy='', $pageId = 1, $pageSize = 0){
	     try{
            $sql = "SELECT {$fileds} FROM `{$this->table}` ac left join `user_member` um on ac.`userid`=um.`userid` WHERE 1 ";
            $strWhere = $this->getWhere($condition);
            $this->sqlStrWhere = $this->sqlStrWhere!='' ? $this->sqlStrWhere . $strWhere : $strWhere;
            $sql.= empty($strWhere)?'':' AND '.$strWhere;
            $sql.= $this->groupBy = empty($groupBy)?'':' GROUP BY '.$groupBy;
            $sql.= empty($orderBy)?'':' ORDER BY '.$orderBy;
            $sql.= $this->limit($pageId,$pageSize);
            $this->lastsql = $sql;
            return $this->DB->read()->query($sql)->query_getall();
        }
        catch(Exception $e)
        {
            $this->showError($e);
        }
    }
    
    
	function getchanneluserlistcount($where){
 	 	if($where){//增加查询条件
            $this->sqlStrWhere = $this->getWhere($where);
        }
        try
        {
            $sql = "SELECT count(*) num FROM `{$this->table}` WHERE 1 ";
            if(isset($this->sqlStrWhere))
            {
                $sql.= empty($this->sqlStrWhere)?'':' AND '.$this->sqlStrWhere;
            }
            else
            {
                return 0;
            }

            $sql .= $this->groupBy;
            $this->lastsql = $sql;
            $data = $this->DB->read()->query($sql)->query_getsingle();
            return $data['num'];
        }
        catch(Exception $e)
        {
            $this->showError($e);
        }
    }
 	/**
     * edit android publisher ratio
     */
    function doEdit($where, $params)
    {
        $chk = $this->chkAddInfo($params);
        if($chk['status'] == 0)
        {
            return $chk;
        }
        $historyData = $this->getRow($where);
        if(empty($historyData))
        {
            return array('status' => 0, 'data'=>array('', '未知错误1！'), 'info' => '未知错误！');
        }
        $data = $params;
        $data['ratioswitch'] = 1;
        $num = $this->edit($where, $data);
        if($num)
        {
            return array('status' => 1, 'data' => '修改成功！');
        }
        return array('status' => 0, 'data'=>array('', '未做任何修改！'), 'info' => '未做任何修改！');
    }
    
    
/**
     * 检测信息
     */
    private function chkAddInfo($params)
    {
        $error = array('status' => 1, 'data' => '');
   	 	if(empty($params['marketratio']))
        {
            $error['data'] = array('marketratiomsg', '应用商店分成比例不能为空。');
            $error['info'] = 'marketratioerr';
            $error['status'] = 0;
            return $error;
        }
   		if(!is_numeric($params['marketratio']) || $params['marketratio'] < 0){
   			$error['data'] = array('marketratiomsg', '应用商店分成比例必须是大于等于0的数字。');
            $error['info'] = 'marketratioerr';
            $error['status'] = 0;
            return $error;
   		}
    	if(!preg_match("/^(\d{1,3})(\.\d{0,2})?$/", $params['marketratio'])){
            $error['data'] = array('marketratiomsg', '应用商店分成比例格式错误！。');
            $error['info'] = 'marketratioerr';
            $error['status'] = 0;
            return $error;
        }
        
        
        if(empty($params['devratio']))
        {
            $error['data'] = array('devratiomsg', '开发者分成比例不能为空。');
            $error['info'] = 'devratioerr';
            $error['status'] = 0;
            return $error;
        }
    	if(!is_numeric($params['devratio']) || $params['devratio'] < 0){
    	 	$error['data'] = array('devratiomsg', '开发者分成比例必须是大于等于0的数字。');
            $error['info'] = 'devratioerr';
            $error['status'] = 0;
            return $error;
        }
    	if(!preg_match("/^(\d{1,3})(\.\d{0,2})?$/", $params['devratio'])){
            $error['data'] = array('devratiomsg', '开发者分成比例格式错误。');
            $error['info'] = 'devratioerr';
            $error['status'] = 0;
            return $error;
        }
   		
        if(empty($params['platformratio']))
        {
            $error['data'] = array('platformratiomsg', '平台分成比例不能为空。');
            $error['info'] = 'platformratioerr';
            $error['status'] = 0;
            return $error;
        }
    	if(!is_numeric($params['platformratio']) || $params['platformratio'] < 0){
    		$error['data'] = array('platformratiomsg', '平台分成比例必须是大于等于0的数字。');
            $error['info'] = 'platformratioerr';
            $error['status'] = 0;
            return $error;
        }
   	 	if(!preg_match("/^(\d{1,3})(\.\d{0,2})?$/", $params['platformratio'])){
            $error['data'] = array('platformratiomsg', '平台分成比例格式错误。');
            $error['info'] = 'platformratioerr';
            $error['status'] = 0;
            return $error;
        }
        if((floatval($params['marketratio'])+floatval($params['devratio'])+floatval($params['platformratio'])) != 100){
        	$error['data'] = array('marketnamemsg', '三者分成比例之和为100。');
            $error['info'] = 'marketnameerr';
            $error['status'] = 0;
            return $error;
        }
        return $error;
    }
    
	function getchannelusermarketlist($offset, $limit , $where){
		$sqlWithoutLimit = "select ac.id id, ac.appid appid,ac.publisherID publisherID,ac.channelname channelname,ac.createtime createtime,market.marketname marketname,user.username username 
				from app_channel ac
				left join app_info ai on ac.appid = ai.appid
				left join user_member user on ac.userid = user.userid
				left join market on ac.marketid = market.marketid
				where ai.ostypeid = 1 and ac.channelid!=0 ".$where." order by ac.id";
		$sqlcount = "select count(*) as count from (".$sqlWithoutLimit.") tmp";
		$sql = $sqlWithoutLimit." limit ".$offset.", ".$limit;
		$count = $this->DB->read()->query($sqlcount)->query_getall();
		$list = $this->DB->read()->query($sql)->query_getall();
		if($count){
			$count = $count[0]['count'];
		}else{
			$count = 0;
		}
		return array('count'=>$count, 'list'=>$list);
	}
}
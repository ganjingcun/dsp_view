<?php

class AdSchedulesModel extends MY_Model
{
    //当前主要操作表
    protected $table = 'ad_schedules';

    protected $id = 'scheduleid';

    private $format_data = array(
				'A'=>0,'B'=>0,'C'=>0,'D'=>0,'E'=>0,'F'=>0,'G'=>0,'H'=>0,'I'=>0,'J'=>0,'K'=>0,'L'=>0,
				'M'=>0,'N'=>0,'O'=>0,'P'=>0,'Q'=>0,'R'=>0,'S'=>0,'T'=>0,'U'=>0,'V'=>0,'W'=>0,'X'=>0
    );




    function __construct()
    {
        parent::__construct();
    }

    /**
     * 根据用户输入的时段排期，转化成数据库可接受的格式
     * @param $data
     * @return array
     */
    public function UserDataToDBData($data)
    {
        $data = json_decode($data,true);
        $format_data = $this->format_data;
        $schedule_data = array();
        foreach($data as $day=>$v)
        {
            $len = strlen($v[0]);
            $tmp = $format_data;
            for($i=0;$i<$len;$i++)
            {
                if(key_exists($v[0][$i],$tmp))
                {
                    $tmp[$v[0][$i]] = 1;
                }
            }
            if($v[1] == '')
            {
                $schedule_data[$day] = array(implode('',$tmp), NULL);
            }
            else
            {
                $schedule_data[$day] = array(implode('',$tmp), $v[1]);
            }
        }
        return $schedule_data;
    }

    /**
     * 将从库里搜索出来的排期，转化成前台可以接受的json形式
     * @param array $dbData
     * @return unknown_type
     */
    public function DBDataToUserData(array $dbData)
    {
        $tmp = array();
        foreach( $dbData as $k=>&$row )
        {
            $key = date('Y-m-d',$row['daydate']);
            $schedules = $this->reverse($row['schedules']);
            if($row['daybudget'] === null)
            {
                $daybudget = '';
            }
            else
            {
                $daybudget = (int)($row['daybudget'] / 1000000);
            }
            $tmp[$key] = array($schedules, $daybudget);
        }
        return json_encode($tmp);
    }

    /**
     * 例如：将000000000000000000001111的值转化成 UVWX
     * @param $val 形如000000000000000000001111
     * @return string
     */
    private function reverse($val)
    {
        $keys = array_keys($this->format_data);
        $arr = array();
        $len = strlen($val);
        for($i=0;$i<$len;$i++)
        {
            if($val[$i]==1)
            {
                $arr[$keys[$i]] = $val[$i];
            }
        }
        if(!empty($arr))
        {
            return implode('',array_keys($arr));
        }
        return '';
    }

    public function getUserData(array $where)
    {
        $data = $this->getList($where);
        if(!empty($data))
        {
            $data = $this->DBDataToUserData($data);
        }
        return $data;
    }
}

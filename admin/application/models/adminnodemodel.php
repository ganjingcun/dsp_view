<?php
class AdminNodeModel extends MY_Model
{
	protected  $table = "admin_node";
	private $group = '';
	private $res = array();

	function getAdminNode($adminid){
	    $this->load->model('AdminUserModel');
        $group_id = $this->AdminUserModel->getOne(array('admin_id'=>$adminid),'group_id');
        $this->group = $this->AdminGroupModel->getOne(array('group_id'=>$group_id),'group_privilege');
        $this->group = trim($this->group,',');
        $this->res = $this->getList(array("id"=>array("in",$this->group)),"*","sortid ASC");
        $tmp = array();
        foreach($this->res as $val){
            $tmp[$val['class']] = $val['class'];
        }
        return $tmp;
	}
	
    function getNav($adminid){
        $child = array();
        if($this->group){
            $sql = "select * from admin_node where pid in ($this->group) order by sortid ASC";
            $child = $this->DB->read()->query_prepare($sql)->query_execute()->query_getall();
        }
        $res = array_merge($this->res, $child);
        $arr = array();
        foreach($res as $key=>$val){
            if($val['pid'] > 0){
                $arr[$val['pid']]['child'][$val['id']] = $val;
            }else{
                if(isset($arr[$val['id']]['child'])){
                    $child = $arr[$val['id']]['child'];
                    $arr[$val['id']] = $val;
                    $arr[$val['id']]['child'] = $child;
                }else{
                    $arr[$val['id']] = $val;
                }
            }
        }
        return $arr;
    }
    /**
    * 根据 /类名/方法名  判断是不是有权限
    */
     function check_privilege($class=''){
          $nodeinfo = $this->getRow(array('class'=>$class),'*');
          if(empty($nodeinfo)) return true;
          $pid = $nodeinfo['pid'];
          $public = array('admin'=>'/admin/admin','index'=>'/admin/index', 'main'=>'/admin/main','top'=>'/admin/top', 'center'=>'/admin/center', 'bottom'=>'/admin/bottom','left'=>'/admin/left');
          if(in_array($class,$public))  return true;
          if($pid){
                  $this->group;
                  $grouparr = explode(",",$this->group);
                  if(in_array($pid,$grouparr)){
                          return true;
                  }else{
                          return false;
                  }
          }
     }
}
<?php
class AdIncomeModel extends MY_Model
{
	protected  $table = "ad_month_app_income";
	
	function getIncomeList($offset, $limit){
		$lastmonth = date("Y-m",mktime(0, 0 , 0,date("m")-1,1,date("Y")));
		$sqlWithoutLimit = "select ai.userid, sum(income) as sum_income, sum(cincome) as sum_cincome, account.accounttype, account.contact, user.username, user.accountmark
				from ad_month_app_income income
				left join app_info ai on income.pkCanalId = ai.appid
				left join (select userid, accounttype, contact from account_info group by userid) account on ai.userid = account.userid
				left join user_member user on account.userid = user.userid
				where income.pkMonth = '".$lastmonth."' and income.incomeid = 0 and user.userid is not null and user.userid != ''
				group by ai.userid
				having sum(cincome)<>0
				order by ai.userid";
		$sqlcount = "select count(*) as count from (".$sqlWithoutLimit.") tmp";
		$sql = $sqlWithoutLimit." limit ".$offset.", ".$limit;
		$count = $this->DB->read()->query($sqlcount)->query_getall();
		$list = $this->DB->read()->query($sql)->query_getall();
		if($count){
			$count = $count[0]['count'];
		}else{
			$count = 0;
		}
		return array('count'=>$count, 'list'=>$list);
	}
}

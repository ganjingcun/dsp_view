<?php
class AppDspCutoverFloorsModel extends MY_Model
{
    protected  $table = "app_dsp_cutover_floors";

    function __construct() {
        parent::__construct();
    }

    /**
     * 添加
     */
    function addData($params)
    {
        $id = $this->save('', $params);
        if($id)
        {
            return array('status' => 1, 'msg' => '录入成功');
        }
        return array('status' => 0,'msg' => '录入失败');

    }
}
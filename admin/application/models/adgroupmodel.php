<?php
class AdGroupModel extends MY_Model
{
	protected  $table = "ad_group";

	function __construct() {
		parent::__construct();
	}
    
    /**
     * 获取广告活动小时数据
     * @param type $ids
     */
    function getAdgroup($groups, $campaignid){
        if(empty($groups))
        {
            return false;
        }
        $groupids = '';
        foreach($groups as $val){
            
        }
        $condition = array();
        $condition['server'] = "server=ads_report_union";
        $condition['t'] = "t=t_day_all_banner";
        $condition['pkCampId'] = 'pkCampId='.$campaignid;
        $condition['s']  = "s=".urlencode("pkCampId,sum(ask) ask,sum(impSucc) imp,sum(click) click, sum(cost) cost");
        $condition['g']  = "g=pkCampId";
        $list1 = $this->getDataSourceOnce($condition);
        $condition['t'] = "t=t_day_all_moregame";
        $list2 = $this->getDataSourceOnce($condition);
        $condition['t'] = "t=t_day_all_pop";
        $list3 = $this->getDataSourceOnce($condition);
        $list = array_merge($list1, $list2, $list3);
        $tmp = array();
        foreach($list as $val){
            $tmp[$val['pkCampId']] = $val;
        }
        $tmp = $this->getFormatData($tmp);
        return $tmp;
    }

    function getDataSourceOnce($condition){
        $data = getDataApi($condition,true);
        if(!empty($data['data']))
        return $data['data'];
        else
        return array();
    }
    
    function getFormatData($data)
    {
        if(empty($data))
        {
            return $data;
        }
        foreach ($data as $k=>$v)
        {
            $this->sumClick += $v['click'];
            $this->sumImp += $v['imp'];
            $this->sumCost += $v['cost'];
            if($v['imp'] > 0 )
            {
                $data[$k]['clickrate'] = number_format($v['click']*100/$v['imp'],2)." %";
            }
            else
            {
                $data[$k]['clickrate'] = '--';
            }
            if($v['click'] > 0)
            {
                $data[$k]['cpc'] = '￥' . formatmoney($v['cost']/$v['click']);
            }
            else
            {
                $data[$k]['cpc'] = '--';
            }
            if($v['imp'] > 0)
            {
                $data[$k]['cpm'] = '￥' . formatmoney($v['cost']/$v['imp']);
            }
            else
            {
                $data[$k]['cpm'] = '--';
            }
            $data[$k]['rescost'] = $v['cost'];
            if($v['cost'] > 0)
            {
                $data[$k]['cost'] = '￥' . formatmoney($v['cost']);
            }
            else
            {
                $data[$k]['cost'] = '--';
            }
        }

        if($this->sumImp > 0)
        {
            $this->sumClickRate = number_format($this->sumClick*100/$this->sumImp,2)." %";
        }
        else
        {
            $this->sumClickRate = '--';
        }
        if($this->sumClick > 0)
        {
            $this->sumCpc = '￥' . formatmoney($this->sumCost/$this->sumClick);
        }
        else
        {
            $this->sumCpc = '--';
        }
        if($this->sumImp > 0)
        {
            $this->sumCpm = '￥' . formatmoney($this->sumCost/$this->sumImp);
        }
        else
        {
            $this->sumCpm  = '--';
        }
        if($this->sumCost > 0)
        {
            $this->sumCost = '￥' . formatmoney($this->sumCost);
        }
        else
        {
            $this->sumCost = '--';
        }
        return $data;
    }
	
    function getById($select, $adgroupid){
    	$sql = "select ".$select." from ad_group where adgroupid=".$adgroupid;
    	return $this->DB->read()->query($sql)->query_getall();
    }
}
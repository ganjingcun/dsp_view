<?php
class ContactUsModel extends MY_Model{
    public $table = 'contactus';
    
    public function doAdd($params){
    	$chk = $this->chkAddInfo($params);
    	if($chk['status'] == 0)
    	{
    		return $chk;
    	}
    	$params['createtime'] = time();
    	$params['status'] = 1;
    	$id = $this->add($params);
        if($id <= 0) 
        	return array('status' => 0, 'data' => '','info'=>'');
        return array('status' => 1, 'data' => '','info'=>'');
    }
    
    public function doEdit($params, $id){
    	$chk = $this->chkEditInfo($params);
    	if($chk['status'] == 0)
    	{
    		return $chk;
    	}
    	$params['edittime'] = time();
    	$affectedrows = $this->edit(array('id'=>$id),$params);
    	if($affectedrows<=0)
        	return array('status' => 0, 'data' => '','info'=>'');
        return array('status' => 1, 'data' => '','info'=>'');
    }
    
    private function chkAddInfo($params){
    	$error=array('status'=>1, 'data'=>'');
    	if(empty($params['title']))
    	{
    		$error['data'] = array('titlemsg', '请填写标题');
    		$error['info'] = 'titleerr';
    		$error['status'] = 0;
    		return $error;
    	}
    	if(empty($params['email']) && empty($params['qq']) && empty($params['qqgroup']) && empty($params['phonenum']))
    	{
    		$error['data'] = array('contactmsg', '请至少填写一项联系方式');
    		$error['info'] = 'contacterr';
    		$error['status'] = 0;
    		return $error;
    	}
    	if(!empty($params['priority']))
    	{
    		if(!ctype_digit($params['priority']))
    		{
	    		$error['data'] = array('prioritymsg', '优先级只可以为整数。');
	    		$error['info'] = 'priorityerr';
	    		$error['status'] = 0;
	    		return $error;
    		}
    	}
    	return $error;
    }
	private function chkEditInfo($params){
    	$error=array('status'=>1, 'data'=>'');
    	if(empty($params['title']))
    	{
    		$error['data'] = array('edittitlemsg', '请填写标题');
    		$error['info'] = 'edittitleerr';
    		$error['status'] = 0;
    		return $error;
    	}
    	if(empty($params['email']) && empty($params['qq']) && empty($params['qqgroup']) && empty($params['phonenum']))
    	{
    		$error['data'] = array('editcontactmsg', '请至少填写一项联系方式');
    		$error['info'] = 'editcontacterr';
    		$error['status'] = 0;
    		return $error;
    	}
    	if(!empty($params['priority']))
    	{
    		if(!ctype_digit($params['priority']))
    		{
	    		$error['data'] = array('editprioritymsg', '优先级只可以为整数。');
	    		$error['info'] = 'editpriorityerr';
	    		$error['status'] = 0;
	    		return $error;
    		}
    	}
    	return $error;
    }
}
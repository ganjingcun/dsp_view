<?php
class DspAdEcpcAppModel extends MY_Model
{
    protected  $table = "dsp_ad_ecpc_app";
    
	/**
	 * 添加dsp_ad_ecpc_app
	 */
	public function doAdd($params){
		$user = $this->session->userdata("admin");
        $data['siteid'] = trim($params['siteid']);
        $data['appid'] = $params['appid'];
        $data['dsp'] = $params['dsp'];
        $data['dsptype'] = $params['dsptype'];
        $data['dspratio'] = $params['dspratio'];
        $data['addtime'] = time();
        $data['admin_id'] = $user['admin_id'];
        $data['status'] = 0;
        $id = $this->add($data);
        if($id <= 0) 
        	return false;
        return true;
    }
}
<?php
define('UPLOAD_APP_PATH',UPLOAD_DIR.'/app/'.date('Ym'));
class AppInfoModel extends MY_Model
{
    private $urlmatch='/^(https?:\/\/).?([\w\d-]+\.)+[\w-]+(\/[\d\w-.\/\!\+?%&=]*)?$/Ui';
    protected  $table = "app_info";

    function __construct(){
        parent::__construct();
    }

    function getAppCpc($where, $fileds='*', $pageId = 1, $pageSize = 0){
        $sql = "SELECT {$fileds} FROM `{$this->table}` app_info left join ad_ecpc_app on ad_ecpc_app.appid=app_info.appid and ad_ecpc_app.status = 1 WHERE 1 $where";
        $sql.= $this->limit($pageId,$pageSize);
        return $this->DB->read()->query($sql)->query_getall();
    }

    function getAppCount($where){
        $sql = "SELECT count(1) as count FROM `{$this->table}` app_info WHERE 1 $where";
        $row = $this->DB->read()->query($sql)->query_getsingle();
        if(isset($row['count'])){
            return $row['count'];
        }
        return 0;
    }

    function getAppSearchList($data){
        $where = '';
        if(isset($data['auditstatus'])){
            $where .= " and app_info.auditstatus = {$data['auditstatus']} ";
        }
        if(isset($data['search']) && $data['search'] != ''){
            $where .= "  and ( app_info.appname like '%{$data['search']}%' or app_info.appid like '%{$data['search']}%' or user_member.realname like '%{$data['search']}%'";
            if(strtolower($data['search']) == 'android'){
                $where .= " or app_info.ostypeid = 1 ";
            }elseif(strtolower($data['search']) == 'ios'){
                $where .= " or app_info.ostypeid = 2 ";
            }
            $where .= ")";
        }
        if(isset($data['auditstatus']) && $data['auditstatus'] != 1)
        {
            $orderby = 'order by auditime desc';
        }
        else
        {
            $orderby = 'order by audisubtime desc';
        }
        if($data['ostypeid']) $where .= " and ostypeid = ".$data['ostypeid'];
        $sql = "select app_info.appid,app_info.appname,app_info.auditstatus,app_info.ostypeid,app_info.auditime,app_info.appurl,app_info.audiurl,app_info.auditype,app_info.audisubtime,app_info.auditmemo,app_info.auditid,user_member.realname,user_member.username,user_member.telephone,user_member.qq from app_info left join user_member on user_member.userid=app_info.userid where 1 $where $orderby ";

        return $this->DB->read()->query($sql)->query_getall();
    }

    /**
     * 编辑应用
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    public function doEdit($params)
    {
        $params['appname'] = trim($params['appname']);
        $chk = $this->chkEditInfo($params);
        if($chk !== true)
        {
            $err['status'] = 0;
            $err = array_merge($err, $chk);
            return $err;
        }
        $data['appname'] = $params['appname'];
        $data['packagename'] = $params['packagename'];
        $data['appurl'] = $params['appurl'];
        //$data['apptypeid'] = $params['apptypeid'];
        $data['devicetypeid'] = isset($params['devicetypeid']) ? $params['devicetypeid'] : 0;
        $data['appchildtypeid']= ',' . $params['appchildtypeid'];
        $data['appdescription']= $params['appdescription'];
        $data['uptime'] = time();
        $data['isupdate'] = 1;
        $upNum = $this->edit(array('appid'=>$params['appid']), $data);
        if($upNum <= 0) return array('status' => 0, 'info' => 'appnameerr', 'data' => array('appnamemsg', '未知错误。'));
        return array('status' => 1, 'appid' => $params['appid'], '');
    }

    /**
     * 更新开关状态
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    public function doSwitch($params)
    {
        $where['appid'] = $params['appid'];
        $data['uptime'] = time();
        $data['isupdate'] = 1;
        $data['switch'] = $params['switch'];
        $upNum = $this->edit($where, $data);
        if($upNum <= 0) return array('status' => 0, 'info' => 'appnameerr', 'data' => array('appnamemsg', '开关状态更新失败。'));
        return array('status' => 1);
    }

    /**
     * 更新积分墙状态
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    public function doWallSet($params)
    {
        $params['appcampaignid'] = trim($params['appcampaignid']);
        $params['unit'] = trim($params['unit']);
        $chk = $this->chkIntegralInfo($params);
        if($chk !== true)
        {
            $err['status'] = 0;
            $err = array_merge($err, $chk);
            return $err;
        }
        if(isset($params['isintegral']) && $params['isintegral'] == 1)
        {
            $data['isintegral'] = 1;
            $data['integration'] = $params['integration'];
            $data['unit'] = $params['unit'];
            $data['wallrechargetype'] = $params['wallrechargetype'];
            $data['appcampaignid'] = trim($params['appcampaignid']);
                $this->load->model('AppPositionModel');
                $sql = "SELECT * FROM `app_position` WHERE 1 AND  ( `appid` = ".$params['appid']." ) AND ( `positionid` IS NULL ) AND ( `adform` = '4' )";
                $pos_info = $this->DB->read()->query($sql)->query_getall();
                 if(empty($pos_info)){
                        //添加默认积分墙广告位
                        $tmp['positionname'] = '默认广告位';
                        $tmp['positiondesc'] = '默认广告位';
                        $tmp['adform'] = 4;
                        $tmp['appid']= $params['appid'];
                        $tmp['trusteeshiptype'] = 2;
                        $this->load->model('AppInfoModel');
                        $app_data = $this->AppInfoModel->getRow(array('appid'=>$params['appid']));
                        $tmp['userid'] = $app_data['userid'];
                        $tmp['createtime'] = time();
                        $this->AppPositionModel->add( $tmp);
                }
        }
        $where['appid'] = $params['appid'];
        $data['uptime'] = time();
        $data['isupdate'] = 1;
        $upNum = $this->edit($where, $data);
        if($upNum <= 0) return array('status' => 0, 'info' => 'appnameerr', 'data' => array('appnamemsg', '更新失败。'));
        return array('status' => 1);
    }

    /**
     * 检测广告组信息
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    private function chkIntegralInfo($params)
    {

        if(empty($params['appid']) )
        {
            $error['data'] = array('appnamemsg', '未知的appid！');
            $error['info'] = 'appnameerr';
            return $error;
        }
        if(isset($params['isintegral']) && $params['isintegral'] == 1)
        {
            if(empty($params['integration']))
            {
                $error['data'] = array('sintegrationmsg', '请填写兑换比例。');
                $error['info'] = 'sintegrationerr';
                return $error;
            }
            if(!is_numeric($params['integration']))
            {
                $error['data'] = array('sintegrationmsg', '兑换比例必须是数字。');
                $error['info'] = 'sintegrationerr';
                return $error;
            }
            if($params['integration'] <= 0)
            {
                $error['data'] = array('sintegrationmsg', '兑换比例必须大于0。');
                $error['info'] = 'sintegrationerr';
                return $error;
            }
            if(empty($params['unit']))
            {
                $error['data'] = array('sintegrationmsg', '请填写兑换单位。');
                $error['info'] = 'sintegrationerr';
                return $error;
            }
            if($params['wallrechargetype'] == 1 && empty($params['appcampaignid']))
            {
                $error['data'] = array('sintegrationmsg', '请填写对接ID。');
                $error['info'] = 'sintegrationerr';
                return $error;
            }
        }
        return true;
    }
    
    /**
     * 更新提审
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    public function doAutit($appid, $url, $auditype = 1, $userInfo)
    {
        if($auditype == 2 && !preg_match($this->urlmatch, $url))
        {
            return array('status' => 0, 'info' => 'appnameerr', 'data' => '请输入正确的地址 !');
        }
        $where['appid'] = $appid;
        $where['userid']= $userInfo['userid'];
        $data['audisubtime'] = time();
        $data['auditstatus'] = 1;
        $data['audiurl'] = $url;
        $data['auditype'] = $auditype;
        $this->db->where($where);
        $this->db->update('app_info', $data);
        $upNum = $this->db->affected_rows();
        if($upNum <= 0) return array('status' => 0, 'info' => 'appnameerr', 'data' => '未知错误。');

        return array('status' => 1, 'data'=>'');
    }
    /**
     * 检测广告组信息
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    private function chkAddInfo($params)
    {
        if(empty($params['ostypeid']))
        {
            $error['data'] = '';
            $error['info'] = 'ostypeiderr';
            return $error;
        }
        if($params['ostypeid'] == 2 && empty($params['devicetypeid']))
        {
            $error['data'] = '';
            $error['info'] = 'devicetypeiderr';
            return $error;
        }
        if(empty($params['appname']))
        {
            $error['data'] = array('appnamemsg', 'App名称不能为空。');
            $error['info'] = 'appnameerr';
            return $error;
        }
        if(mb_strlen($params['appname']) > 50)
        {
            $error['data'] = array('appnamemsg', 'App名称不能超过50个字符。');
            $error['info'] = 'appnameerr';
            return $error;
        }
        if(empty($params['packagename']))
        {
            $error['data'] = array('packagenamemsg', 'App包名称不能为空。');
            $error['info'] = 'packagenameerr';
            return $error;
        }
        if(!empty($params['appurl']) && !preg_match($this->urlmatch, $params['appurl']))
        {
            $error['data'] = '';
            $error['info'] = 'appurlerr';
            $error['status'] = 0;
            return $error;
        }
        if($params['apptypeid'] == 0)
        {
            $error['data'] = '';
            $error['info'] = 'apptypeiderr';
            return $error;
        }
        return true;
    }

    /**
     * 检测广告组信息
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    private function chkEditInfo($params)
    {

        if(empty($params['appid']))
        {
            $error['data'] = array('appnamemsg', '未知的appid！');
            $error['info'] = 'appnameerr';
            return $error;
        }
        if(empty($params['appname']))
        {
            $error['data'] = array('appnamemsg', 'App名称不能为空。');
            $error['info'] = 'appnameerr';
            return $error;
        }
        if(mb_strlen($params['appname']) > 50)
        {
            $error['data'] = array('appnamemsg', 'App名称不能超过50个字符。');
            $error['info'] = 'appnameerr';
            return $error;
        }
        if(empty($params['packagename']))
        {
            $error['data'] = array('packagenamemsg', 'App包名称不能为空。');
            $error['info'] = 'packagenameerr';
            return $error;
        }
         $appchildtypeid = trim($params['appchildtypeid'], ',');
        if(empty($appchildtypeid))
        {
            $error['data'] = array('appchildtypeidmsg', '请选择应用子分类。');
            $error['info'] = 'appchildtypeiderr';
            return $error;
        }
        $appchildtypeida = explode(',', $appchildtypeid);
        if(count($appchildtypeida) > 3)
        {
            $error['data'] = array('appchildtypeidmsg', '应用分类不能超过3个。');
            $error['info'] = 'appchildtypeiderr';
            return $error;
        }
        if(!empty($params['appurl']) && !preg_match($this->urlmatch, $params['appurl']))
        {
            $error['data'] = '';
            $error['info'] = 'appurlerr';
            $error['status'] = 0;
            return $error;
        }
         
        return true;
    }

    /**
     * 获取app类别
     */
    public function getAppType()
    {
        $typeList = $this->getAppTypeList();
        if($typeList == false)
        {
            return false;
        }
        $data = array();
        foreach ($typeList as $v)
        {
            if($v['parentid'] == 0)
            {
                $data[$v['apptypeid']] = $v;
            }
        }
        foreach ($data as $k => $v)
        {
            foreach ($typeList as $pv)
            {
                if($pv['parentid'] == $k)
                {
                    $data[$k]['childtype'][] = $pv;
                }
            }
        }
        return $data;
    }

    /**
     * 获取app父类别
     */
    public function getAppTypeList($appTypeId = '', $parentId = '')
    {
        $where = array();
        if(!empty($appTypeId))
        {
            $where['apptypeid'] = $appTypeId;
        }
        if(!empty($parentId))
        {
            $where['parentid'] = $parentId;
        }
        $this->load->model('AppTypeModel');
        $data = $this->AppTypeModel->getList($where);
        if(empty($data))
        {
            return false;
        }
        return $data;
    }

    /**
     * 获取应用列表
     */
    public function getAppList($userInfo)
    {
        $data = $this->getAppInfoList($userInfo['userid']);
        if(!$data) return false;
        $dataType = $this->getAppTypeList('', 0);
        $dataTypeTmp = array();
        foreach ($dataType as $value) {
            $dataTypeTmp[$value['apptypeid']] = $value['typename'];
        }
        foreach ($data as $k => $v)
        {
            if(isset($dataTypeTmp[$v['apptypeid']]))
            {
                $data[$k]['typename'] = $dataTypeTmp[$v['apptypeid']];
            }
            $punchbox = $this->getAppChannelRow($v['appid']);
            $data[$k]['publisherID'] = isset($punchbox['publisherID']) ? $punchbox['publisherID'] : '';
        }
        return $data;
    }

    /**
     * 获取应用详细信息
     */
    public function getDetailInfo($appId)
    {
        $data = $this->getAppInfoRow($appId);
        if(!$data) return false;
        $this->load->model('AppTypeModel');
        $dataType = $this->getAppTypeList();
        foreach ($dataType as $value) {
            if($value['apptypeid'] == $data['apptypeid'])
            {
                $data['typename'] = $value['typename'];
                continue;
            }
            if(strrpos($data['appchildtypeid'], ','.$value['apptypeid'].',') !== false)
            {
                $data['childtypename'][] = $value['typename'];
            }
        }
        $dataChannel = $this->getAppChannelRow($appId);
        if($dataChannel)
        $data = array_merge($data, $dataChannel);
        return $data;
    }
    /**
     * 获取App基本信息
     * Enter description here ...
     * @param $appId
     * @param $userId
     */
    public function getAppInfoRow($appId)
    {
        $data = $this->getRow(array('appid' => $appId));
        if(empty($data))
        {
            return false;
        }
        return $data;
    }

    /**
     * 获取App基本信息列表
     * Enter description here ...
     * @param unknown_type $userId
     */

    public function getAppInfoList($userId, $osTypeId = '')
    {
        $where['userid'] = $userId;
        $data = $this->getList($where, '*','createtime desc');
        if(empty($data))
        {
            return false;
        }
        return $data;
    }

    public function getAppChannelRow($appId)
    {
        $where['appid'] = $appId;
        $this->load->model('AppChannelModel');
        $data = $this->AppChannelModel->getList($where);
        if(empty($data))
        {
            return false;
        }
        return $data;
    }

    /**
     * 上传应用
     * @return unknown_type
     */
    public function upLoad($type)
    {
        $this->load->library('MyUpload',$_FILES['uploadata']);
        $upload = new MyUpload($_FILES['uploadata']);
        $upload->setUploadPath(UPLOAD_APP_PATH);
        $upload->setFileExt(array('apk','ipa'));
        $upload->setMaxsize(200*1024*1024);

        $strError = '';

        if( !$upload->isAllowedTypes() )
        {
            $strError = '请上传后缀为.apk或.ipa的文件类型 !';
        }
        elseif( $upload->isBigerThanMaxSize() )
        {
            $strError = '此处的文件最大不能超过 '.intval($upload->getMaxsize()/1024) .'KB';
        }

        if(empty($strError) and $upload->upload())
        {
            $imgPath = $upload->getUplodedFilePath();
            $data['data'] = $imgPath;
            $data['status'] = 1;
        }
        else
        {
            $data['data'] = $strError;
            $data['status'] = 0;
        }
        return $data;
    }
    
    public function update($where, $data)
    {
		$upNum = $this->edit($where, $data);
		return $upNum;
    }

    
    public function getAppReport()
    {
    	try{
    		$sql = "SELECT `user_member`.`userid`, `username`, `companyname`, `account_info`.`realname`, `bankusername`, `account_info`.`accounttype`, `appid`, `appname` FROM `app_info` INNER JOIN `user_member` ON `user_member`.`userid` = `app_info`.`userid` INNER JOIN `account_info` ON `account_info`.`userid` = `app_info`.`userid` WHERE `account_info`.`status` = 1 order by `user_member`.`userid` asc";
		    return $this->DB->read()->query($sql)->query_getall();
        }
        catch(Exception $e)
        {
            $this->showError($e);
        }
    }

    public function doSyncappinfochance($params)
    {
        $data = $this->AppInfoModel->getAppInfoRow($params['appid']);
        if(empty($data))
        {
            return array('status' => 0, 'info' => 'appnameerr', 'data' => array('appnamemsg', '应用不存在。'));
        }

        $postdata = array();
        $postdata['appname'] = $data['appname'];
        $postdata['packagename'] = $data['packagename'];
        $postdata['appurl'] = $data['appurl'];
        $postdata['appchildtypeid']= ',' . $data['appchildtypeid'];
        $postdata['appdescription']= $data['appdescription'];
        // $postdata['isintegral']= $data['isintegral'];
        // $postdata['integration']= $data['integration'];
        // $postdata['unit']= $data['unit'];

        if (isset($data['chance_appid']))
        {
            $postdata['appid'] = $data['chance_appid'];
            $postdata = array('params' => $postdata, 'userinfo' => $this->config->item('dsp_api_userinfo'));
            $result = curlPost($this->config->item('dsp_api_server') . 'doeditapp', $postdata, 'POST', true);
            $result = json_decode($result, true);
        }
        else
        {
            $postdata['thirdpartyid'] = $data['appid'];
            $postdata['ostypeid'] = $data['ostypeid'];
            $postdata['devicetypeid'] = $data['devicetypeid'];
            $postdata['apptypeid'] = $data['apptypeid'];
            $postdata = array('params' => $postdata, 'userinfo' => $this->config->item('dsp_api_userinfo'));
            $result = curlPost($this->config->item('dsp_api_server') . 'doaddapp', $postdata, 'POST', true);
            $result = json_decode($result, true);
            if (!isset($result))
            {
                $result = array('statis'=>0, 'data'=>null, 'info'=>'与远程API通讯失败');
            }

            if ($result['status'] == 1)
            {
                $updata = array();
                $updata['chance_appid'] = $result['data']['appid'];
                $updata['chance_publisherID'] = $result['data']['publisherID'];
                $updata['chance_position'] = json_encode($result['data']['position']);
                $updata['chance_secretkey'] = $result['data']['secretkey'];

                $upNum = $this->edit(array('appid'=>$params['appid']), $updata);
                if($upNum <= 0) return array('status' => 0, 'info' => 'appnameerr', 'data' => array('appnamemsg', '更新数据库失败。'));
            }
        }

       return $result;
    }
}
<?php
class AdMgcateModel extends MY_Model {
	public $table = 'ad_mg_cate';
	private $_ac_fields = " ac.campaignid,ac.devicetypeset device_types,ac.ostypeid ostype,";
	private $_ag_fields = " ag.`adgroupid`,";
	private $_ads_fields = " ads.`adstuffname` creative_title,ads.`stuffid` adid,ads.storeid,";

	private $_where = ' and ads.status=1 and ag.status=1 and ac.status < 3 ';//and ac.ostypeid=2 ';

	function getApplyList($cateid) {
		$fields = '*';
		$sql = "select $fields from ad_mg_apply ama left join ad_mg_info ami on ama.storeid=ami.storeid where ama.cateid = $cateid and ama.`status`=1 order by ama.sortid desc, ama.addtime desc";
		return $this->DB->read()->query_prepare($sql)->query_execute()->query_getall();
	}

	function getMoregameList($cateid, $ostype = 1) {
	    if($ostype == 2)
	    {
	        $where = " ac.ostypeid=2 ";
	    }
	    else 
	    {
	        $where = " ac.ostypeid=1 ";
	    }
		$time = strtotime(date('Y-m-d'));
		$sql = "SELECT {$this->_ac_fields} {$this->_ag_fields} {$this->_ads_fields}
              IFNULL(ag.otherid,'') AS trackcampaignid,
              asm.appname adtitle,
              asm.path moregameicon,
              asm.istop,
              info.apptype,
              info.othername,
              info.othertype,
              asm.star,ama.sortid,ama.id amaid,ama.status
            FROM
              `ad_stuff_moregame` asm
              LEFT JOIN `ad_stuff` ads
                ON ads.`stuffid` = asm.`stuffid`
              LEFT JOIN ad_group ag
                ON ag.adgroupid = ads.`adgroupid`
              LEFT JOIN ad_campaign ac
                ON ac.campaignid = ag.campaignid
		left join ad_mg_apply ama on ama.storeid=ads.storeid and ama.cateid=$cateid and ads.stuffid=ama.stuffid
        left join ad_mg_info info on info.storeid=ama.storeid
            WHERE 1 and ads.ispause = 0 and ag.adform = 3 and {$where} and ag.starttime <= $time and ag.endtime >= $time {$this->_where} group by ads.stuffid ";
		//echo $sql;
		return $this->DB->read()->query_prepare($sql)->query_execute()->query_getall();
	}

}
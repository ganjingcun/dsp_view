<?php
class MediaRuleModel extends MY_Model
{
    protected  $table = "media_rule";

    function __construct() {
        parent::__construct();
    }

    /**
     * 添加
     */
    function addData($params)
    {
        $datalist = $this->getList(array('appid' => $params['appid'], 'adtype' => $params['adtype'], 'ostype'=>$params['ostype'],'isstatus'=>0));
        $status = $this->chkItem($params, $datalist);

        if($status['status'] == 0)
        {
            return $status;
        }
        $chkData = $this->getRow(array('appid' => $params['appid'], 'mediaid' => $params['mediaid'], 'adtype' => $params['adtype'], 'ostype'=>$params['ostype'],'isstatus'=>0));
        if(!empty($chkData))
        {
        	
            return array('status'=>0,'data'=>'该记录已存在！');
        }
        $data['priority'] = $params['priority'];
        $data['ostypeset'] = $params['ostypeset'];
        if($data['ostypeset'])
        {
            $data['lowosv'] = $params['lowosv'];
            $data['highosv'] = $params['highosv'];
        }
        $data['devicetypeset'] = $params['devicetypeset'];
        if($data['devicetypeset'])
        {
            $data['devicetype'] = json_encode($params['devicetypes']);
        }
        $data['nettypeset'] = $params['nettypeset'];
        if($data['nettypeset'])
        {
            $data['nettype'] = json_encode($params['nettypes']);
        }
        $data['sdkversionset'] = $params['sdkversionset'];
        if($data['sdkversionset'])
        {
            $data['sdkvset'] = json_encode($params['sdkvset']);
        }
        $data['operatorsset'] = $params['operatorsset'];
        if($data['operatorsset'])
        {
            $data['operators'] = json_encode($params['operatorss']);
        }
		if(strtoupper($params['areaset']) == 'CN'){
        	$TW=122;
        	$HK=112;
        	$MO=997;
        	if(in_array($TW, $params['areaids'])){
        		$params['areaset'] .=",TW"; 
        	}
        	if(in_array($HK, $params['areaids'])){
        		$params['areaset'] .=",HK"; 
        	}
        	if(in_array($MO, $params['areaids'])){
        		$params['areaset'] .=",MO"; 
        	}
            $data['areaset'] = $params['areaset'];
            $data['areaids'] = json_encode($params['areaids']);
        }
        elseif (strtoupper($params['areaset']) == 'OVERSEA'){
	        if(count($params['areaids'])>0 && isset($params['areaids'])){
	        	$this->load->model("AdAreaCountryModel");
	        	$countrycodes=array();
		         foreach ($params['areaids'] as $tmp){
					if($tmp!='-1'){
						$tmpcountry=$this->AdAreaCountryModel->getRow(array('internalid'=>$tmp));
						array_push($countrycodes, $tmpcountry['countrycode']);
					}else {
						continue;
					}
		        }
		        $data['areaset']=implode(',', $countrycodes);
	        }
	        else{
	        	$data['areaset']='';
	        }
            $data['areaids'] = json_encode($params['areaids']);
        }
        else
        {
        	$data['areaset']='';
            $data['areaids'] = '';
        }
        $data['domainfiltertype']=$params['domainfiltertype'];
        $data['scheduletypeset'] = $params['scheduletypeset'];
        if($data['scheduletypeset'])
        {
            $data['timelist'] = $params['timelist'];
        }
        else
        {
            $data['ratio'] = $params['ratio'];
        }
        $data['mediaid'] = $params['mediaid'];
        $data['starttime'] = strtotime($params['starttime']);
        $data['endtime'] = strtotime($params['endtime']);
        $data['ostype'] = $params['ostype'];
        $data['adtype'] = $params['adtype'];
        $data['appid'] = $params['appid'];
        $data['adminid'] = $params['adminid'];
        $data['createtime'] = time();
        $id = $this->save('', $data);
        if($id)
        {
            return array('status' => 1, 'data' => $id);
        }
        return array('status' => 0,'data' => '录入失败。');

    }

    /**
     * 编辑
     */
    function eidtData($params)
    {
        $datalist = $this->getList(array('appid' => $params['appid'], 'adtype' => $params['adtype'], 'ostype'=>$params['ostype'], 'id' => array('neq', $params['id']), 'isstatus'=>0));
        $status = $this->chkItem($params, $datalist);
        if($status['status'] == 0)
        {
            return $status;
        }
        $data['priority'] = $params['priority'];
        $data['ostypeset'] = $params['ostypeset'];
        if($data['ostypeset'])
        {
            $data['lowosv'] = $params['lowosv'];
            $data['highosv'] = $params['highosv'];
        }
        else
        {
            $data['lowosv'] = '';
            $data['highosv'] = '';
        }
        $data['devicetypeset'] = $params['devicetypeset'];
        if($data['devicetypeset'])
        {
            $data['devicetype'] = json_encode($params['devicetypes']);
        }
        $data['sdkversionset'] = $params['sdkversionset'];
        if($data['sdkversionset'])
        {
            $data['sdkvset'] = json_encode($params['sdkvset']);
        }
        $data['nettypeset'] = $params['nettypeset'];
        if($data['nettypeset'])
        {
            $data['nettype'] = json_encode($params['nettypes']);
        }
        $data['operatorsset'] = $params['operatorsset'];
        if($data['operatorsset'])
        {
            $data['operators'] = json_encode($params['operatorss']);
        }
    	if(strtoupper($params['areaset']) == 'CN'){
        	$TW=122;
        	$HK=112;
        	$MO=997;
        	if(in_array($TW, $params['areaids'])){
        		$params['areaset'] .=",TW"; 
        	}
        	if(in_array($HK, $params['areaids'])){
        		$params['areaset'] .=",HK"; 
        	}
        	if(in_array($MO, $params['areaids'])){
        		$params['areaset'] .=",MO"; 
        	}
            $data['areaset'] = $params['areaset'];
            $data['areaids'] = json_encode($params['areaids']);
        }
        elseif (strtoupper($params['areaset']) == 'OVERSEA'){
	        if(count($params['areaids'])>0 && isset($params['areaids'])){
	        	$this->load->model("AdAreaCountryModel");
	        	$countrycodes=array();
		         foreach ($params['areaids'] as $tmp){
					if($tmp!='-1'){
						$tmpcountry=$this->AdAreaCountryModel->getRow(array('internalid'=>$tmp));
						array_push($countrycodes, $tmpcountry['countrycode']);
					}else {
						continue;
					}
		        }
		        $data['areaset']=implode(',', $countrycodes);
	        }
	        else{
	        	$data['areaset']='';
	        }
            $data['areaids'] = json_encode($params['areaids']);
        }
        else
        {
        	$data['areaset']='';
            $data['areaids'] = '';
        }
        $data['domainfiltertype']=$params['domainfiltertype'];
        $data['scheduletypeset'] = $params['scheduletypeset'];
        if($data['scheduletypeset'])
        {
            $data['timelist'] = $params['timelist'];
        }
        else
        {
            $data['ratio'] = $params['ratio'];
        }
        $data['mediaid'] = $params['mediaid'];
        $data['starttime'] = strtotime($params['starttime']);
        $data['endtime'] = strtotime($params['endtime']);
        $data['ostype'] = $params['ostype'];
        $data['adtype'] = $params['adtype'];
        $data['appid'] = $params['appid'];
        $data['adminid'] = $params['adminid'];
        $data['createtime'] = time();
        $this->save(array('id'=>$params['id']), array('isstatus' => 1));
        $id = $this->save('', $data);
        if($id)
        {
            return array('status' => 1, 'data' => $id);
        }
        return array('status' => 0,'data' => '录入失败。');

    }

    /**
     * 检测输入项
     */
    function chkItem($params, $datalist)
    {
    	if(empty($params['priority']))
        {
        	return array('status' => 0, 'data' => '请填写优先级。');
        }
        if(!is_numeric($params['priority']))
        {
            return array('status' => 0, 'data' => '请填写数字。');
        }
        if($params['ostypeset'])
        {
            if(empty($params['lowosv']) || empty($params['highosv']))
            {
                return array('status' => 0, 'data' => '请选择系统版本定向。');
            }
            if($params['lowosv'] > $params['highosv'])
            {
                return array('status' => 0, 'data' => '最低版本不能大于最高版本。');
            }
        }
        if($params['devicetypeset'])
        {
            if(empty($params['devicetypes']))
            {
                return array('status' => 0, 'data' => '请选择设备类型定向。');
            }
        }
        if($params['nettypeset'])
        {
            if(empty($params['nettypes']))
            {
                return array('status' => 0, 'data' => '请选择网络类型定向。');
            }
        }
        if($params['operatorsset'])
        {
            if(empty($params['operatorss']))
            {
                return array('status' => 0, 'data' => '请选择运营商定向。');
            }
        }
        if($params['areaset'] == 'CN')
        {
            if(empty($params['areaids']))
            {
                return array('status' => 0, 'data' => '请选择地域定向。');
            }
        }
        if($params['sdkversionset'])
        {
            if(empty($params['sdkvset']))
            {
                return array('status' => 0, 'data' => '请选择SDK版本定向。');
            }
        }
        if($params['scheduletypeset'])
        {
            $tmp = json_decode($params['timelist'], true);
            if(empty($tmp))
            {
                return array('status' => 0, 'data' => '请选择时段。');
            }
        }
        else
        {
            if(empty($params['ratio']))
            {
                return array('status' => 0, 'data' => '请填写切量比例。');
            }
            if(!is_numeric($params['ratio']) || $params['ratio'] == 0 || $params['ratio'] > 100)
            {
                return array('status' => 0, 'data' => '切量比例必须是大于0小于等于100的正整数。');
            }
        }
        if(strtotime($params['endtime']) < strtotime($params['starttime']))
        {
            return array('status' => 0, 'data' => '切量周期开始时间不能大于结束时间。');
        }
        $tmp = $this->chkSchedule($params, $datalist);
        return $tmp;
    }

    /**
     * 排期检测
     */
    function chkSchedule($params, $datalist)
    {
        $starttime = strtotime($params['starttime']);
        $endtime = strtotime($params['endtime']);
        if($params['scheduletypeset'])
        {
            $tmp = json_decode($params['timelist'], true);
            if(!is_numeric($tmp['daybudget']) || $tmp['daybudget'] > 100 || $tmp['daybudget'] < 0)
            {
                return array('status' => 0, 'data' => '默认切量比例必须是大于0小于等于100的正整数。');
            }
            foreach($tmp['data'] as $k=>$v)
            {
                $data[$params['mediaid']][$k] = $v[1];
                if(!is_numeric($v[1]) || $v[1] > 100 || $v[1] < 0)
                {
                    return array('status' => 0, 'data' => '切量比例必须是大于0小于等于100的正整数。');
                }
            }
        }
        else
        {
            $data[$params['mediaid']]['ratio'] = $params['ratio'];
        }
        $data[$params['mediaid']]['starttime'] = strtotime($params['starttime']);
        $data[$params['mediaid']]['endtime'] = strtotime($params['endtime']);
        if(empty($datalist))
        {
            return array('status'=>1);
        }
        foreach($datalist as $vf)
        {
            if($vf['starttime'] < $starttime)
            {
                $starttime = $vf['starttime'];
            }
            if($vf['endtime'] > $endtime)
            {
                $endtime = $vf['endtime'];
            }

            $data[$vf['mediaid']]['starttime'] = $vf['starttime'];
            $data[$vf['mediaid']]['endtime'] = $vf['endtime'];
            if($vf['scheduletypeset'])
            {
                $tmp = json_decode($vf['timelist'], true);
                foreach($tmp['data'] as $k=>$v)
                {
                    $data[$vf['mediaid']][$k] = $v[1];
                }
            }
            else
            {
                $data[$vf['mediaid']]['ratio'] = $vf['ratio'];
            }
        }
        /*
        for($i = $starttime; $i <= $endtime; $i += 86400)
        {
            $ratio = 0;
            foreach($data as $v)
            {
                if($v['endtime'] >= time()-86400)
                {
                    if(isset($v['ratio']))
                    {
                        if($v['starttime'] <= $i && $v['endtime'] >= $i)
                        {
                            $ratio += $v['ratio'];
                        }
                    }
                    else if(isset($v[date('Y-m-d', $i)]))
                    {
                        $ratio += $v[date('Y-m-d', $i)];
                    }
                }
            }
            if($ratio > 100)
            {
                return array('status' => 0, 'data' => date('Y-m-d', $i).' 号的切量比例超过了100。');
            }
        }
        */
        return array('status' => 1);
    }

    
    function editDomainFilterType($params){
    	$upNum=$this->edit(array('id'=>$params['id']),array('domainfiltertype'=>$params['domainfiltertype']));
        if($upNum <= 0) 
        	return array('status' => 0);
        else 
        	return array('status' => 1);
    }
}
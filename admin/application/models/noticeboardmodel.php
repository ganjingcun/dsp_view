<?php
/**
 * @encoding	: UTF-8
 * @author		: wf
 * @datetime	: 2015-04-23
 * @Description  : 应用类型model
 */
class NoticeBoardModel extends MY_Model
{
    protected  $table = "notice_board";

    function __construct() {
        parent::__construct();
    }
    
    function getNoticeByWhere($where,$orderby){
    	$data = $this->getList($where,'*',$orderby);
    	//array('status'=>1)
        if(empty($data))
        {
            return false;
        }
        return $data;
    }
    
    function getNoticeList(){
    	$data = $this->getList();
    	
        if(empty($data))
        {
            return false;
        }
        return $data;
    }
    
    
    
   	public function doAdd($params)
   	{
   	 	$chk = $this->chkAddInfo($params);
        if($chk['status'] == 0 || $chk['status'] == 3)
        {
            return $chk;
        }
   		$params['createtime'] = time();
        $id = $this->add($params);
        if($id <= 0) 
        	return array('status' => 0, 'data' => '','info'=>'');
        return array('status' => 1, 'data' => '','info'=>'');
    }
    
    public function doEdit($params, $id){
   	 	$chk = $this->chkEditInfo($params);
        if($chk['status'] == 0)
        {
            return $chk;
        }
   		$params['edittime'] = time();
        $id = $this->edit(array('id'=>$id),$params);
        if($id <= 0)
        	return array('status' => 0, 'data' => '','info'=>'');
        return array('status' => 1, 'data' => '','info'=>'');
    }
    
 	private function chkAddInfo($params)
    {
        $error = array('status' => 1, 'data' => '');
        if($params['type'] != 1 && $params['type'] != 2)
        {
            $error['data'] = array();
            $error['info'] = '添加失败，请刷新重试';
            $error['status'] = 3;
            return $error;
        }
        if(empty($params['title']))
        {
            $error['data'] = array('titlemsg', '标题不能为空。');
            $error['info'] = 'titleerr';
            $error['status'] = 0;
            return $error;	
        }
        if($params['type'] == 2)
        {
        	if(empty($params['abstract']))
        	{
	            $error['data'] = array('abstractmsg', '新闻简介不能为空。');
	            $error['info'] = 'abstracterr';
	            $error['status'] = 0;
	            return $error;	
        	}
        	if(empty($params['imgpath']))
        	{
	            $error['data'] = array('imgpathmsg', '请上传图片。');
	            $error['info'] = 'imgpatherr';
	            $error['status'] = 0;
	            return $error;	
        	}
	   		if(!empty($params['priority']) && !ctype_digit($params['priority']))
	    	{
	    		$error['data'] = array('prioritymsg', '优先级只可以为整数。');
	    		$error['info'] = 'priorityerr';
	    		$error['status'] = 0;
	    		return $error;
	    	}
       	}
        return $error;
    }
    
	private function chkEditInfo($params)
    {
        $error = array('status' => 1, 'data' => '');
        if(empty($params['title']))
        {
            $error['data'] = array('edittitlemsg', '标题不能为空。');
            $error['info'] = 'edittitleerr';
            $error['status'] = 0;
            return $error;	
        }
        if(empty($params['abstract']))
        {
            $error['data'] = array('editabstractmsg', '新闻简介不能为空。');
            $error['info'] = 'editabstracterr';
            $error['status'] = 0;
            return $error;	
        }
   		if(!empty($params['priority']) && !ctype_digit($params['priority']))
    	{
    		$error['data'] = array('editprioritymsg', '优先级只可以为整数。');
    		$error['info'] = 'editpriorityerr';
    		$error['status'] = 0;
    		return $error;
    	}
       	
        return $error;
    }
	
	/*
	 * 删除
	 */
	function doDelete($id){
        if($this->getRow(array('id'=>$id)) == false)
        {
          	 array('status' => 0);
        }
        $data['id'] = $id;
        $status = $this->delete($data);
        if($status){
        	return array('status' => 1);
        }
        return array('status' => 0);
    }
}

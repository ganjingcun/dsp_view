<?php
class CaseManageModel extends MY_Model
{
    protected  $table = "case_manage";

    function __construct() {
        parent::__construct();
        $this->load->model('CaseManageLogModel');
    }

    /**
     * 添加订单信息
     */
    function doAdd($params)
    {
        $params['starttime'] = strtotime($params['starttime'] . ' ' . $params['starthour'] . ':00:00');
        $params['endtime'] = strtotime($params['endtime'] . ' ' . $params['endhour'] . ':00:00');
        $chk = $this->chkAddInfo($params);
        if($chk['status'] == 0)
        {
            return $chk;
        }
        $data = $params;
        $data['daylevel'] = $params['daylevel'] == '' ? '不限' : $params['daylevel'];
        $data['price'] = formatmoney($params['price'], 'set');
        $data['createtime'] = time();
        $admin = $this->session->userdata("admin");
        $data['adminid'] = $admin['admin_id'];
        $data['adstatus'] = 0;
        unset($data['starthour']);
        unset($data['endhour']);
        $historyData['id'] = 0;
        $chk = $this->chkTime($data,$historyData,$params['casetype']);
        if($chk['status'] == 0)
        {
            return $chk;
        }
        $id = $this->add($data);
        if($id)
        {
            return array('status' => 1, 'id' => $id);
        }
        return array('status' => 0, 'data'=>array('', '未知错误！'), 'info' => '未知错误！');
    }

    /**
     * 编辑订单信息
     */
    function doEdit($where, $params)
    {
        $params['starttime'] = strtotime($params['starttime'] . ' ' . $params['starthour'] . ':00:00');
        $params['endtime'] = strtotime($params['endtime'] . ' ' . $params['endhour'] . ':00:00');
        $chk = $this->chkAddInfo($params);
        if($chk['status'] == 0)
        {
            return $chk;
        }
        $historyData = $this->getRow($where);
        if(empty($historyData))
        {
            return array('status' => 0, 'data'=>array('', '未知错误！'), 'info' => '未知错误！');
        }
        $data = $params;
        $data['daylevel'] = $params['daylevel'] == '' ? '不限' : $params['daylevel'];
        $data['price'] = formatmoney($params['price'], 'set');
        //$data['createtime'] = time();
        $admin = $this->session->userdata("admin");
        $data['adminid'] = $admin['admin_id'];
        //$data['adstatus'] = 0;
        unset($data['starthour']);
        unset($data['endhour']);

        $chk = $this->chkTime($data,$historyData,$params['casetype']);
        if($chk['status'] == 0)
        {
            return $chk;
        }
        $this->CaseManageLogModel->add(array('data'=>json_encode($historyData), 'adminid'=>$data['adminid'], 'createtime'=>time()));

        //        if($historyData['endtime'] < time())
        //        {
        //            $data['starttime'] = $historyData['starttime'];
        //            $data['endtime'] = $historyData['endtime'];
        //            $data['createtime'] = $historyData['createtime'];
        //            $num = $this->edit($where, $data);
        //            if($num)
        //            {
        //                return array('status' => 1, 'data' => '修改成功！历史订单不能修改开始和结束时间，请注意！');
        //            }
        //            else
        //            {
        //                return array('status' => 0, 'data'=>array('', '11未知错误！'), 'info' => '11未知错误！');
        //            }
        //        }
        //
        //        if($data['starttime'] < time() && $historyData['starttime'] > time())
        //        {
        //            $data['createtime'] = $historyData['createtime'];
        //            $num = $this->edit($where, $data);
        //            if($num)
        //            {
        //                return array('status' => 1, 'data' => '修改成功！待投放订单修改开始时间不会拆分订单，请注意！');
        //            }
        //            else
        //            {
        //                return array('status' => 0, 'data'=>array('', '2未知错误！'), 'info' => '2未知错误！');
        //            }
        //        }
        //        elseif($data['starttime'] < time())
        //        {
        //            $data['starttime'] = strtotime(date('Y-m-d H:00:00', time()+3600));
        //        }
        //        else
        //        {
        //            if($data['starttime'] <= $historyData['starttime'])
        //            {
        //                $num = $this->edit($where, $data);
        //                if($num)
        //                {
        //                    return array('status' => 1, 'data' => '修改成功！待投放订单修改开始时间不会拆分订单，请注意！');
        //                }
        //                else
        //                {
        //                    return array('status' => 0, 'data'=>array('', '3未知错误！'), 'info' => '3未知错误！');
        //                }
        //            }
        //        }
        //        $historyData['endtime'] = $data['starttime']-3600;
        //        $id = $this->add($data);
        //        if($id)
        //        {
        $num = $this->edit($where, $data);
        //        }
        if($num)
        {
            return array('status' => 1, 'data' => '修改成功！');
        }
        return array('status' => 0, 'data'=>array('', '未做任何修改！'), 'info' => '未做任何修改！');
    }

    /**
     *
     */
    function chkTime($data, $historyData, $casetype)
    {
        if($casetype == 1)
        {
            $dataList = $this->getList(array('adgroupid'=>$data['adgroupid']));
        }
        elseif($casetype == 2)
        {
            $dataList = $this->getList(array('channelname'=>$data['channelname'], 'campaignid'=>$data['campaignid']));
        }
        foreach($dataList as $v)
        {
            if($v['id'] == $historyData['id'])
            {
                continue;
            }
            if(($data['starttime'] >= $v['starttime'] && $data['starttime'] <= $v['endtime']) )
            {
                return array('status' => 0, 'data'=>array('', '设定的开始时间与现有订单冲突！'), 'info' => '设定的开始时间与现有订单冲突！');
            }
            if(($data['endtime']>=$v['starttime'] && $data['endtime'] <= $v['endtime']))
            {
                return array('status' => 0, 'data'=>array('', '设定的结束时间与现有订单冲突！'), 'info' => '设定的结束时间与现有订单冲突！');
            }
            if(($data['starttime']<=$v['starttime'] && $data['endtime'] >= $v['endtime']))
            {
                return array('status' => 0, 'data'=>array('', '设定的结束时间与现有订单冲突！'), 'info' => '设定的结束时间与现有订单冲突！');
            }


            if( $historyData['id'] == 0 )
            {
                continue;
            }
            //            if(($historyData['starttime'] >= $v['starttime'] && $historyData['starttime'] <= $v['endtime']))
            //            {
            //                return array('status' => 0, 'data'=>array('', '2设定的开始时间与现有订单冲突！'), 'info' => '2设定的开始时间与现有订单冲突！');
            //            }
            //            if(($historyData['endtime']>=$v['starttime'] && $historyData['endtime'] <= $v['endtime']))
            //            {
            //                return array('status' => 0, 'data'=>array('', '2设定的结束时间与现有订单冲突！'), 'info' => '2设定的结束时间与现有订单冲突！');
            //            }
        }
        return array('status' => 1);
    }

    /**
     * 检测信息
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    private function chkAddInfo($params)
    {
        $error = array('status' => 1, 'data' => '');
        if(empty($params['campaignid']))
        {
            $error['data'] = array('campaignidmsg', '接单项目Campaign ID不能为空。');
            $error['info'] = 'campaigniderr';
            $error['status'] = 0;
            return $error;
        }
        if($params['casetype'] == 1)
        {
            if(!is_numeric($params['adgroupid']))
            {
                $error['data'] = array('adgroupidmsg', '广告组ID不能为空。');
                $error['info'] = 'adgroupiderr';
                $error['status'] = 0;
                return $error;
            }
            if(empty($params['adgroupname']))
            {
                $error['data'] = array('adgroupnamemsg', '广告组名称不能为空。');
                $error['info'] = 'adgroupnameerr';
                $error['status'] = 0;
                return $error;
            }
        }
        elseif ($params['casetype'] == 2)
        {
            if(empty($params['adgroupname']))
            {
                $error['data'] = array('adgroupnamemsg', '外放项目名称不能为空。');
                $error['info'] = 'adgroupnameerr';
                $error['status'] = 0;
                return $error;
            }
            if(empty($params['channelname']))
            {
                $error['data'] = array('channelnamemsg', '外放渠道名称不能为空。');
                $error['info'] = 'channelnameerr';
                $error['status'] = 0;
                return $error;
            }
        }
        if($params['starttime'] > $params['endtime'])
        {
            $error['data'] = array('campaignidmsg', '开始时间不能大于结束时间。');
            $error['info'] = 'campaigniderr';
            $error['status'] = 0;
            return $error;
        }
        if($params['starthour'] < 0)
        {
            $error['data'] = array('campaignidmsg', '请选择开始小时时间。');
            $error['info'] = 'campaigniderr';
            $error['status'] = 0;
            return $error;
        }
        if($params['endhour'] < 0)
        {
            $error['data'] = array('campaignidmsg', '请选择结束小时时间。');
            $error['info'] = 'campaigniderr';
            $error['status'] = 0;
            return $error;
        }

        if(empty($params['companyname']))
        {
            $error['data'] = array('companynamemsg', '公司名称不能为空。');
            $error['info'] = 'companynameerr';
            $error['status'] = 0;
            return $error;
        }
        if(!is_numeric($params['price']) || $params['price'] <= 0)
        {
            $error['data'] = array('pricemsg', '接单价格必须是大于0的数字。');
            $error['info'] = 'priceerr';
            $error['status'] = 0;
            return $error;
        }
        //        if(!is_numeric($params['daylevel']) || $params['daylevel'] <= 0)
        //        {
        //            $error['data'] = array('daylevelmsg', '每日量级必须是大于0的数字。');
        //            $error['info'] = 'daylevelerr';
        //            $error['status'] = 0;
        //            return $error;
        //        }
        if(empty($params['contact']))
        {
            $error['data'] = array('contactmsg', '业务接口人不能为空。');
            $error['info'] = 'contacterr';
            $error['status'] = 0;
            return $error;
        }
        return $error;
    }
}
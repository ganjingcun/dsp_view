<?php
class ClickRatioModel extends MY_Model
{
    protected  $table = "ad_click_ratio";
    function __construct() {
        parent::__construct();
    }

    /**
     * 获取当前有效的ecpc设置
     * Enter description here ...
     */
   function getDefaultRatio(){
       $list = $this->getList(array('status'=>1));
       $tmp = array();
       foreach($list as $val){
           $tmp[$val['ostype']][$val['adtype']][$val['chargemode']] = $val;
       }
       return $tmp;
   }
}
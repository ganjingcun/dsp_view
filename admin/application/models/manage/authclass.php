<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 登陆用户信息获取
 *
 * @brief M层
 * @author 王栋
 * @date 2012.03.20
 * @note 详细说明及修改日志
 */
class AuthClass extends MY_Model
{
	private $username = '';
	private $password = '';

	public function __construct($username='',$password='')
	{
		$this->username = $username;
		$this->password = $password;
	}


	public function authentication()
	{
		return true;
	}





	public static function getCurrentUser()
	{
		AuthClass::instance()->load->library('session');
		$session= new CI_Session();
		return $session->userdata;
	}

	public static function getGroupByUserId()
	{
		/*	AuthClass::instance()->load->model('manage/UserClass');
		 $userClass= new UserClass();
		 return $userClass;*/

		AuthClass::instance()->load->model('manage/UserGroupClass');
		return new UserGroupClass();
	}
	/**
	 * 创建一个对象
	 * @return AuthClass
	 */
	public static function instance()
	{
		return new AuthClass;
	}

	/**
	 * 获取登陆用户Id
	 */
	public static function getUserId()
	{
		$userInfo= self::getCurrentUser();
		return isset($userInfo['userid'])? $userInfo['userid'] : (isset($userInfo['id'])? $userInfo['id'] : 0);
	}

	/**
	 * 获取登陆用户账号
	 */
	public static function getUserName()
	{
		$userInfo= self::getCurrentUser();
		return $userInfo['username'];
	}

	/**
	 * 获取登陆用户类型 1,3管理员,2普通用户
	 */
	public static function getUserType()
	{
		$userInfo= self::getCurrentUser();
		return $userInfo['usertype'];
	}

	/**
	 * 获取登陆用户姓名
	 */
	public static function getRealName()
	{
		$userInfo= self::getCurrentUser();
		return $userInfo['realname'];
	}
	
	/**
	 * 获取登陆用户组信息 Array('id','groupname','groupstatus','grouptype')
	 */
	public static function getUserGroup()
	{

		$UserGroupClass = self::getGroupByUserId();
		$userInfo= self::getCurrentUser();

		$usergroup = array();

		$userid = $userInfo['userid'];
		$self = new AuthClass();
		$self->load->model('manage/UserMemberClass');
		$UserMemberClass = UserMemberClass::instance();

		if($UserMemberClass->isAdminUser($userid) )
		{
			$usergroup = $UserGroupClass->getUserGroupsByAdmin($userid);
		}
		else if($UserMemberClass->isGlobalUser($userid))
		{
			$usergroup = $UserGroupClass->getUserGroupByGlobalUser($userid);
		}
		else
		{
			$usergroup = $UserGroupClass->getUserGroups($userid);
		}
		

		return $usergroup;
		
		
		
		return;
		$userInfo= self::getCurrentUser();
		$userGroup = self::getGroupByUserId();
		if($userInfo['usertype']==2)
		{
				
				
			$userGroup = $userGroup->getUserGroup($userInfo['userid']);
			$groupArray = array();
			if($userGroup===false)
			{
				$chkForm=false;
				header("Content-type: text/html; charset=utf-8");
				echo "<script language=javascript>alert('您的账号未分组,请联系管理员!');self.location='/index';</script>";
				exit;
			}
			else
			{

				foreach ($userGroup as $row)
				{
					$groupArray[]=$row;
				}

				if(count($groupArray)==0)
				{
					$chkForm=false;
					header("Content-type: text/html; charset=utf-8");
					echo "<script language=javascript>alert('您的账号未分组,请联系管理员!');self.location='/index';</script>";
					exit;
				}
				$userInfo['usergroup']=$groupArray;

			}
		}
		else if($userInfo['usertype']==1 || $userInfo['usertype']==3)
		{
			$userGroup = $userGroup->getUserGroupByAdmin($userInfo['parentid']);
			$groupArray = array();
			if($userGroup===false)
			{
				$chkForm=false;
				header("Content-type: text/html; charset=utf-8");
				echo "<script language=javascript>alert('您的账号未分组,请联系管理员!');self.location='/index';</script>";
				exit;
			}
			else
			{
				foreach ($userGroup as $row)
				{
					$groupArray[]=$row;
				}
				$userInfo['usergroup']=$groupArray;
			}
		}
		else
		{
			header("Content-type: text/html; charset=utf-8");
			echo "<script language=javascript>alert('异常账号,请联系管理员!');self.location='/index';</script>";
			exit;
		}
		return $userInfo['usergroup'];
	}

	/**
	 * 获取当前用户组id
	 * @return array
	 */
	public static function getUserGroupIds()
	{
		$groupInfo= self::getUserGroup();
//		print_r($groupInfo);
		$tmp = array();
		$object = new self;
		$object->load->helper('array');
		foreach($groupInfo as $k=>$v)
		{
			if($v['groupstatus']!=4){//by niejianhui 2012-06-19 不显示已删除的组
				$tmp[] = element('id',$v);
			}
		}

		return $tmp;
	}

	/**
	 * 获取登陆用户父id
	 */
	public static function getParentId()
	{
		$userInfo= self::getCurrentUser();
		return $userInfo['parentid'];
	}

	/**
	 * 获取当前用户所属的超级管理组id
	 */
	public static function getGlobalGroupId()
	{
		$userInfo= self::getCurrentUser();
		return $userInfo['globalGroupId'];
	}

	/**
	 * 获取登陆用户电话
	 */
	public static function getTelephone()
	{
		$userInfo= self::getCurrentUser();
		return $userInfo['telephone'];
	}

	/**
	 * 设置登陆用户电话
	 */
	public static function setCookies($data)
	{		
		AuthClass::instance()->load->library('session');
		$session= new CI_Session();
		$session->set_userdata($data);
	}
	
	/**
	 * 获取登陆用户身份
	 */
	public static function getUserIdentity()
	{
		$userInfo= self::getCurrentUser();
		return $userInfo['useridentity'];
	}


	/**
	 * 设置登陆用户身份
	 */
	public static function setUserIdentity($tmp=1)
	{
		$userInfo= self::getCurrentUser();
		$userInfo['useridentity'] = $tmp;
		AuthClass::instance()->load->library('session');
		$arr = json_decode(CI_Controller::get_instance()->input->cookie('useridentity'),true);
		if(!is_array($arr)) $arr = array();
		$arr[$userInfo['userid']] = $tmp;

		$cookie = json_encode($arr);

		CI_Controller::get_instance()->input->set_cookie('useridentity',$cookie,time()+8640000);
		$session= new CI_Session();
		$session->set_userdata($userInfo);
	}

	/**
	 * 获取登陆用户getGroupId  类型 1：外部； 2：大客户
	 */
	public static function getGroupId()
	{
		$userInfo= self::getCurrentUser();
		return isset($userInfo['groupid'])? $userInfo['groupid'] : 1;
	}
}
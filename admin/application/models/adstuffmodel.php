<?php
class AdStuffModel extends MY_Model
{
    protected $table = "ad_stuff";

    function getStuffList($where , $pageId = 1, $pageSize = 0){
        $sql = "SELECT ad_stuff.audittime,ad_stuff.`adstuffname`,ad_stuff.status,ad_stuff.stuffid,ad_group.`adform`,ad_group.`starttime`,ad_group.`endtime`,ad_group.effecttype,ad_stuff.stufftype,ad_stuff.target,ad_stuff.lowerlimit,ad_stuff.upperlimit,ad_stuff.promotionratio,ad_group.`otherid`,ad_group.`adsensortrackurl`, ad_stuff.speedupscale FROM ad_stuff LEFT JOIN ad_group ON ad_group.adgroupid=ad_stuff.`adgroupid` LEFT JOIN ad_campaign ON ad_campaign.`campaignid`=ad_group.`campaignid` where 1 $where order by ad_stuff.createtime asc limit ".($pageId-1)*$pageSize.",".$pageSize;
        $this->lastsql = $sql;
        $list = $this->DB->read()->query($sql)->query_getall();
        $integralids = '';
        $tmp = array();
        foreach($list as $key=>$val){
            if($val['adform'] == 4){
                $integralids .= $val['stuffid'].',';
            }
            $val['tasklist'] = array();
            $tmp[$val['stuffid']] = $val;
        }
        if($integralids){
            $integralids = rtrim($integralids, ',');
            $sql = "select * from ad_task where stuffid in ($integralids)";
            $integral = $this->DB->read()->query($sql)->query_getall();
            foreach($integral as $val){
                $tpstring = json_decode($val['taskprice'], true);
                $retpstring = '';
                if(isset($tpstring[0]['time']))
                {
                    foreach($tpstring as $kt=>$vt)
                    {
                        $retpstring .= "{$vt['time'][0]}~{$vt['time'][1]}"."<br/>";
                        $tmptimemk = 0;
                        foreach($vt['data'] as $dv)
                        {
                            $retpstring .= "&nbsp;&nbsp;&nbsp;&nbsp;{$dv[0]}至{$dv[1]}时&nbsp;&nbsp;￥".formatmoney($dv[2])."<br/>";
                        }
                    }
                }
                else
                {
                    $deftaskprice = array(array('time'=>array(date('Y-m-d', $tmp[$val['stuffid']]['starttime']), date('Y-m-d', $tmp[$val['stuffid']]['endtime']))));
                    $retpstring .= date('Y-m-d', $tmp[$val['stuffid']]['starttime'])."~".date('Y-m-d', $tmp[$val['stuffid']]['endtime'])."<br/>";
                    if(!is_array($tpstring))
                    {
                        $retpstring .= "&nbsp;&nbsp;&nbsp;&nbsp;0至24时&nbsp;&nbsp;￥".formatmoney($tpstring);
                    }
                    else
                    {
                        foreach($tpstring as $ktp=>$vtp)
                        {
                            $tpkey = explode(',', $ktp);
                            $retpstring .= "&nbsp;&nbsp;&nbsp;&nbsp;{$tpkey[0]}至{$tpkey[1]}时&nbsp;&nbsp;￥".formatmoney($vtp)."<br>";
                        }
                    }
                }
                $val['taskprice'] = $retpstring;
                $tmp[$val['stuffid']]['tasklist'][$val['taskid']] = $val;
            }
        }
        return $tmp;
    }

    function getPredown($where , $pageId = 1, $pageSize = 0){
        $sql = "SELECT ad_stuff.audittime,ad_stuff.`adstuffname`,ad_group.adgroupname,ad_stuff.status,ad_stuff.stuffid,ad_group.`adform`,ad_group.starttime,ad_group.endtime,ad_group.createtime,ad_group.effecttype,ad_stuff.stufftype,ad_stuff.target,ad_stuff.packagename,ad_stuff.appversionname,ad_group.adgroupid,ad_campaign.`campaignid` FROM ad_stuff LEFT JOIN ad_group ON ad_group.adgroupid=ad_stuff.`adgroupid` LEFT JOIN ad_campaign ON ad_campaign.`campaignid`=ad_group.`campaignid` where 1 $where group by ad_stuff.packagename,ad_stuff.target order by ad_stuff.stuffid asc limit ".($pageId-1)*$pageSize.",".$pageSize;
        $this->lastsql = $sql;
        $list = $this->DB->read()->query($sql)->query_getall();
        return $list;
    }

    function getPreCount($where){
        $sql = "select count(1) as nums from (SELECT 1 nums FROM ad_stuff LEFT JOIN ad_group ON ad_group.adgroupid=ad_stuff.`adgroupid` LEFT JOIN ad_campaign ON ad_campaign.`campaignid`=ad_group.`campaignid` where 1 $where group by ad_stuff.packagename,ad_stuff.target ) t";
        $this->lastsql = $sql;
        $list = $this->DB->read()->query($sql)->query_getall();
        if(empty($list)){
            return 0;
        }
        return $list[0]['nums'];
    }
    function getSendData($stuffid)
    {
        $this->load->model('AdGroupModel');
        $this->load->model('AdCampaignModel');
        $this->load->model('AdStuffMoregameModel');
        $stuffInfo = $this->getRow(array('stuffid'=>$stuffid));
        if(empty($stuffInfo))
        {
            return '';
        }
        $adgroupInfo = $this->AdGroupModel->getRow(array('adgroupid'=>$stuffInfo['adgroupid']));
        if(empty($adgroupInfo) || $adgroupInfo['adform'] != 3)
        {
            return '';
        }
        $campaignInfo = $this->AdCampaignModel->getRow(array('campaignid'=>$stuffInfo['campaignid']));
        if(empty($campaignInfo))
        {
            return '';
        }
        $mgInfo = $this->AdStuffMoregameModel->getRow(array('stuffid'=>$stuffid));
        if(empty($mgInfo))
        {
            return '';
        }
        $data['campaignid'] = $stuffInfo['campaignid'];
        $data['adgroupid'] = $stuffInfo['adgroupid'];
        $data['stuffid'] = $stuffInfo['stuffid'];
        $data['downurl'] = $stuffInfo['target'];
        $data['iconurl'] = 'http://res.cocounion.com'.$mgInfo['path'];
        $data['starttime'] = $campaignInfo['starttime'];
        $data['endtime'] = $campaignInfo['endtime'];
        $data['adtype'] = 3;
        $data['ostype'] = $campaignInfo['ostypeid'];
        $data['trackurl'] = '';
        $data['trackid'] = $adgroupInfo['otherid'];
        $data['gatherurl'] = $stuffInfo['gatherurl'];
        if( $campaignInfo['status'] == 1 || $campaignInfo['status'] == 2 ||  $adgroupInfo['status'] == 1 ||  $stuffInfo['ispause'] == 0)
        {
            $data['status'] = 1;
        }
        else
        {
            $data['status'] = 0;
        }
        return $data;
    }
	
	/**
     * 获取插屏图片广告列表
     * Enter description here ...
     * @param unknown_type $os
     * @param unknown_type $istop
     * @param unknown_type $search
     */
    function getAPIPopupList($os, $isapi, $search=''){
        if($search){
            $search = " and ag.adgroupname like '%$search%'";
        }
        $sql = "select ag.price,ac.campaignid,ag.adgroupid,ac.campaignname,ag.adgroupname,ag.starttime,ag.endtime,ads.stuffid, 
		ads.adstuffname, ads.apitime,ads.isapi from ad_stuff ads left join ad_group ag on ag.adgroupid=ads.adgroupid left join 
		ad_campaign ac on ac.campaignid=ag.campaignid where ads.status = 1 AND ads.stufftype = 3 AND ads.isapi=$isapi AND 
		ag.`adform` = 2 and ac.targettype = 1 and ac.ostypeid=$os and ag.starttime<=".TIME." and ag.endtime>= ".(TIME-86400)." 
		$search order by ads.lastupdatetime desc";
        //echo $sql;
        $list = $this->DB->read()->query($sql)->query_getall();
        return $list;
    }
}
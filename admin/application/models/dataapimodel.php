<?php

/**
 * Created by PhpStorm.
 * User: chukong
 * Date: 2/26/16
 * Time: 2:32 PM
 */
class DataApiModel extends MY_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper("tools");
    }

    /**
     * 将畅思DSP中的应用日志导入到本地数据库
     * @return array|mixed|void
     */
    public function appGather()
    {
        $this->setTable('system_config');
        $strLastDate = $this->getOne(array("skey"=>'appgatherlastsync'), "sval");
        $lastDate = strtotime($strLastDate);
        $minDate = strtotime('2016-01-01');
        if ($lastDate < $minDate) {$lastDate = $minDate;}
        $today = strtotime("today");

        //删除lastDate当天及之后的数据, 防止数据重复
        $sql = 'DELETE FROM app_gatherlog WHERE logdate>="' . date('Y-m-d', $lastDate) .'"';
        $db = $this->DB->master();
        $db = $db->prepare($sql);
        $db->execute();

        //拉取lastDate到今天的数据
        $strUrl = $this->config->item('dsp_data_api') . '&sdate=' .date('Y-m-d', $lastDate). '&edate=' . date('Y-m-d', $today);
        echo $strUrl . '<hr>';
        $result = curlGet($strUrl);
//        $result = '{"status":1,"data":{"812743709":{"1":{"appname":"test","adform":"Banner","list":{"2016-01-27":{"ask":"825","imp":"543","click":"0","income":"0.00","imprate":"65.82","clickrate":"0.00","coins":"0","complate":"0"},"2016-01-28":{"ask":"85","imp":"64","click":"1","income":"0.05","imprate":"75.29","clickrate":"1.56","coins":"0","complate":"0"},"2016-01-29":{"ask":"123","imp":"66","click":"0","income":"0.00","imprate":"53.66","clickrate":"0.00","coins":"0","complate":"0"},"2016-02-01":{"ask":"156","imp":"13","click":"2","income":"0.10","imprate":"8.33","clickrate":"15.38","coins":"0","complate":"0"},"2016-02-02":{"ask":"165","imp":"2","click":"1","income":"0.05","imprate":"1.21","clickrate":"50.00","coins":"0","complate":"0"},"2016-02-03":{"ask":"39","imp":"2","click":"0","income":"0.00","imprate":"5.13","clickrate":"0.00","coins":"0","complate":"0"},"2016-02-04":{"ask":"21","imp":"2","click":"0","income":"0.00","imprate":"9.52","clickrate":"0.00","coins":"0","complate":"0"},"2016-02-05":{"ask":"9","imp":"1","click":"1","income":"0.05","imprate":"11.11","clickrate":"100.00","coins":"0","complate":"0"},"2016-02-14":{"ask":"592","imp":"1","click":"0","income":"0.00","imprate":"0.17","clickrate":"0.00","coins":"0","complate":"0"},"2016-02-15":{"ask":"1219","imp":"2","click":"1","income":"0.05","imprate":"0.16","clickrate":"50.00","coins":"0","complate":"0"},"2016-02-16":{"ask":"456","imp":"2","click":"2","income":"0.10","imprate":"0.44","clickrate":"100.00","coins":"0","complate":"0"},"2016-02-17":{"ask":"161","imp":"2","click":"1","income":"0.05","imprate":"1.24","clickrate":"50.00","coins":"0","complate":"0"},"2016-02-18":{"ask":"37","imp":"2","click":"0","income":"0.00","imprate":"5.41","clickrate":"0.00","coins":"0","complate":"0"},"2016-02-19":{"ask":"9","imp":"1","click":"0","income":"0.00","imprate":"11.11","clickrate":"0.00","coins":"0","complate":"0"},"2016-02-24":{"ask":"16","imp":"0","click":"0","income":"0.00","imprate":"0.00","clickrate":"0","coins":"0","complate":"0"},"2016-02-25":{"ask":"0","imp":"1","click":"0","income":"0.00","imprate":"0","clickrate":"0.00","coins":"0","complate":"0"}}},"2":{"appname":"test","adform":"插屏","list":{"2016-01-26":{"ask":"10","imp":"10","click":"0","income":"0.00","imprate":"100.00","clickrate":"0.00","coins":"0","complate":"0"},"2016-01-27":{"ask":"13","imp":"13","click":"1","income":"0.08","imprate":"100.00","clickrate":"7.69","coins":"0","complate":"0"},"2016-01-28":{"ask":"35","imp":"34","click":"1","income":"0.08","imprate":"97.14","clickrate":"2.94","coins":"0","complate":"0"},"2016-01-29":{"ask":"25","imp":"25","click":"0","income":"0.00","imprate":"100.00","clickrate":"0.00","coins":"0","complate":"0"},"2016-02-01":{"ask":"131","imp":"29","click":"2","income":"0.16","imprate":"22.14","clickrate":"6.90","coins":"0","complate":"0"},"2016-02-02":{"ask":"91","imp":"2","click":"1","income":"0.08","imprate":"2.20","clickrate":"50.00","coins":"0","complate":"0"},"2016-02-03":{"ask":"86","imp":"2","click":"0","income":"0.00","imprate":"2.33","clickrate":"0.00","coins":"0","complate":"0"},"2016-02-04":{"ask":"35","imp":"2","click":"0","income":"0.00","imprate":"5.71","clickrate":"0.00","coins":"0","complate":"0"},"2016-02-05":{"ask":"2","imp":"1","click":"0","income":"0.00","imprate":"50.00","clickrate":"0.00","coins":"0","complate":"0"},"2016-02-14":{"ask":"3","imp":"1","click":"0","income":"0.00","imprate":"33.33","clickrate":"0.00","coins":"0","complate":"0"},"2016-02-15":{"ask":"119","imp":"1","click":"1","income":"0.08","imprate":"0.84","clickrate":"100.00","coins":"0","complate":"0"},"2016-02-16":{"ask":"14","imp":"2","click":"1","income":"0.08","imprate":"14.29","clickrate":"50.00","coins":"0","complate":"0"},"2016-02-17":{"ask":"12","imp":"1","click":"1","income":"0.08","imprate":"8.33","clickrate":"100.00","coins":"0","complate":"0"},"2016-02-18":{"ask":"9","imp":"1","click":"0","income":"0.00","imprate":"11.11","clickrate":"0.00","coins":"0","complate":"0"},"2016-02-19":{"ask":"5","imp":"1","click":"0","income":"0.00","imprate":"20.00","clickrate":"0.00","coins":"0","complate":"0"}}}}},"error":""}';
        $result = json_decode($result);

        if ($result->status == 0)
        {
            return $result;
        }

        $data = array();
        $resultstr = '上次更新日志时间:<span style="color:#ff0000">' . date('Y-m-d', $lastDate) . '</span><br><br>';

        foreach($result->data as $appid => $appdata)
        {
            foreach($appdata as $adform => $formdata)
            {
                $count = 0;
                foreach($formdata->list as $logdate => $logdata)
                {
                    $item = array();
                    $item['appid'] = $appid;
                    $item['adform'] = $adform;
                    $item['logdate'] = $logdate;
                    $item['ask'] = $logdata->ask;
                    $item['imp'] = $logdata->imp;
                    $item['click'] = $logdata->click;
                    $item['imprate'] = $logdata->imprate;
                    $item['clickrate'] = $logdata->clickrate;
                    $item['income'] = $logdata->income;
                    $item['coins'] = $logdata->coins;
                    $item['complate'] = $logdata->complate;
                    $data[] = $item;
                    $count++;
                }

                $resultstr .= '应用名称:' . $formdata->appname . '  --  ' . $formdata->adform . '  -- ' . $count . '条<br>';
            }
        }

        $logcount = count($data);
        $resultstr .= '<br>拉取到<span style="color:#ff0000">' . $logcount . '</span>行日志数据, ';
        if ($logcount > 0) {
            $this->setTable('app_gatherlog');
            $writecount = $this->addBatch($data);
        } else {
            $writecount = 0;
        }
        $resultstr .= '写入<span style="color:#ff0000">' . $writecount . '</span>行到数据库<br>';

        if ($logcount == $writecount)
        {
            if (isset($strLastDate))
            {
                $sql = 'UPDATE system_config SET sval="' .date('Y-m-d', $today). '" WHERE skey="appgatherlastsync"';
            }
            else
            {
                $sql = 'INSERT INTO system_config (skey, sval, status, addtime, parentid) VALUES("appgatherlastsync", "' .date('Y-m-d', $today). '", 1, now(), 0)';
            }
            $db = $this->DB->master();
            $db = $db->prepare($sql);
            $db->execute();

            return array('status'=>1, 'data'=>null, 'error'=>$resultstr);
        } else {
            return array('status'=>1, 'data'=>null, 'error'=>$resultstr);
        }
    }
}
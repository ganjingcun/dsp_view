<?php

class CcplayBiz extends CI_Model {
    public function flushCCplayPIDFileByAppId($appid=null) {
        $filepath = $this->config->item('ccplay_data_briage_path');
        
        $this->load->model('AppChannelConfigModel');
        $this->load->model('AppChannelModel');
        $cond= array('configname'=>'ccplay_autopop','configvalue'=>'1');
        $this->load->model('AppInfoModel');
        $appcond = array();
        $chcond = array();
        if(!empty($appid)) {
            $chcond['appid']=$appid;
            $cond['appid']=$appid;
            $appcond['appid']=$appid;
        }
        
        //query appinfo switch status
        $apps = $this->AppInfoModel->getList($appcond,'appid,is_ccplay');
        $appinfomap = array();
        if(!empty($apps)) {
            foreach ($apps as $app) {
                $appinfomap[$app['appid']]=$app['is_ccplay'];
            }
        }
        
        
        $forceOpenPids = $this->AppChannelConfigModel->getList($cond,'publisherID,appid');
        
        $configitem=array();
        if($forceOpenPids==null) {
            $forceOpenPids = array();
        } else {
            foreach($forceOpenPids as $grprow) {
                $configitem[$grprow['appid']][] = $grprow['publisherID'];
            }
        }
        //print_r($forceOpenPids);
        
        
        
        $allchannels = $this->AppChannelModel->getList($chcond);
        
        $this->load->helper('tools');
        
        if(!file_exists($filepath)) {
            deldir($filepath);
            createDir($filepath);
        }
        
        foreach ($allchannels as $one) {
            $pidfilename = $filepath.$one['publisherID'].'.html';
            $retjson = array();
                if($one['is_ccplay']>0 && $appinfomap[$one['appid']]>0) {
                    $retjson['ccplay']=1;
                } else {
                   $retjson['ccplay']=0;
                }
                
                $autopop_parent = (isset($configitem[$one['appid']])) &&in_array(0, $configitem[$one['appid']])?true:false;
            
                if(isset($configitem[$one['appid']]) && is_array($configitem[$one['appid']]) && in_array($one['publisherID'],$configitem[$one['appid']]) && $autopop_parent) {
                    $retjson['fceopen']=1;
                } else {
                    $retjson['fceopen']=0;
                }
            file_put_contents($pidfilename,json_encode($retjson));
        }
        
    }
}
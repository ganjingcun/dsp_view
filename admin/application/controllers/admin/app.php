<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class App extends MY_A_Controller
{
    private $urlmatch='/^(http:\/\/)?(https:\/\/)?([\w\d-]+\.)+[\w-]+(\/[\d\w-.\/\!\+?%&=]*)?$/Ui';
    private $adminInfo;
    private $adminId;
    function __construct(){
        header("Cache-control:no-cache,no-store,must-revalidate");
        header("Pragma:no-cache");
        header("Expires:0");
        parent::__construct();
        $this->load->model('UserMemberModel');
        $this->load->model('AppInfoModel');
        $this->load->model('OsVersionModel');
        $configlist = $this->SystemConfigModel->getAllConfig();
        $this->load->library('Security');
        $this->load->model('MediaCutoverAccountModel');
        $this->load->helper('tools');
        $this->assign('configlist', $configlist);
        $this->adminInfo = $this->session->userdata('admin');
        $this->adminId = $this->adminInfo['admin_id'];
    }

    function applist(){
        $data = array();
        $show = $this->input->get_post('show') ?$this->input->get_post('show') : '';
        $os =  $this->input->get('os') ?$this->input->get('os'):2 ;
        $devicetypeid = $this->input->get('devicetypeid');
        $search = $this->input->get('search');
        $stype = $this->input->get('stype') ?  intval($this->input->get('stype')) : 1 ;
        $p=trim($this->input->get('per_page'));
        if(!$p){
            $p=1;
        }
        $data=$users=array();
        $data['ostypeid']=$os;
        $where = " ostypeid=".$os;
        if(!$devicetypeid && $devicetypeid !== '0') $devicetypeid = 2;
        $data['devicetypeid']=$devicetypeid;
        $where .=" and devicetypeid=".$devicetypeid;
        if($stype == 1){
            if(!empty($search)){
                $data['appname']=array('LIKE',"%".$search."%");
                $where = $where." and  appname like '%$search%'";
            }
        }elseif($stype == 2){
            if(!empty($search)){
                $datas['username']=array('LIKE',"%".$search."%");
            }
            else
            {
                $datas['username']='';
            }
            $info = $this->UserMemberModel->getList($datas,'userid');

            while(list($k,$v) = each($info)){
                $users[]=$v['userid'];
            }
        }else{
            $datas['publisherid']='';
            if(!empty($search)){
                $datas['publisherid']=array('LIKE',"%".$search."%");
            }
            $this->load->model("AppChannelModel");
            $info = $this->AppChannelModel->getRow($datas);
            if(!empty($info))
            {
                $where .= " and appid = ".$info['appid'];
                $data['appid'] = $info['appid'];
            }
            else
            {
                $data['appid'] = '';
            }
        }

        if(count($users)){
            $data['userid']=array("in",implode(",",$users));
            $where .= " and userid in ({$data['userid'][1]}) ";
        }
        $this->AppInfoModel->setStrWhere($where);
        $config['per_page'] = 20;
        $count = $this->AppInfoModel->getCount();
        $applist = $this->AppInfoModel->getList($data,"*","appid desc", "", $p,$config['per_page']);
        //获取媒体分组
        $this->load->model("AppGroupModel");
        $appgrouparray = $this->AppGroupModel->getList(array('status'=>0));

        foreach ($appgrouparray as $val)
        {
            $appgrouplist[$val['id']] = $val;
        }
        if($count){
            foreach($applist as $k=>$v){
                $info = $this->UserMemberModel->getRow(array("userid"=>$v['userid']));
                $applist[$k]['realname']=$info['realname'];
                $applist[$k]['username']=$info['username'];
                $applist[$k]['appchildtypeid'] = explode(',', trim($v['appchildtypeid']));
                if(isset($appgrouplist[$v['appgroupid']]))
                {
                    $applist[$k]['appgroupname'] = $appgrouplist[$v['appgroupid']]['groupname'];
                }
                else
                {
                    $applist[$k]['appgroupname'] = '暂未设置';
                }
            }
        }
        $this->load->library('page');
        $current_page =  $p==0 ? 1 : $p;
        $var = 'stype='.$stype.'&os='.$os.'&search='.$search.'&devicetypeid='.$devicetypeid.'&show='.$show;
        $page=$this->page->page_show('/admin/app/applist?'.$var,$count,$config['per_page']);
        $this->pagination->create_links();
        $this->load->model("AppTypeModel");
        $apptype = $this->AppTypeModel->getList();
        $app = array();
        foreach($apptype as $val){
            $app[$val['apptypeid']] = $val['typename'];
        }
        $this->smarty->assign('search', $search);
        $this->smarty->assign('appgrouplist', $appgrouplist);
        $this->smarty->assign('os', $os);
        $this->smarty->assign('devicetypeid', $devicetypeid);
        $this->smarty->assign('page', $page);
        $this->smarty->assign('per_page', $config['per_page']);
        $this->smarty->assign('count', $count);
        $this->smarty->assign('app', $app);
        $this->smarty->assign('stype', $stype);
        $this->assign('applist', $applist);
        if($show == 'dmpset')
        {
            $dmpdebugswitch = $this->SystemConfigModel->getOne(array("skey"=>'dmpdebugswitch'), "sval");
            $dmprule = $this->SystemConfigModel->getOne(array("skey"=>'dmprule'), "sval");
            $this->smarty->assign('dmprule', $dmprule);
            $this->smarty->assign('dmpdebugswitch', $dmpdebugswitch);
            $this->display('admin/app/dmpset.html');
        }
        else
        {
            $this->display('admin/app/applist.html');
        }
    }

    /**
     * 媒体分组管理
     * Enter description here ...
     */
    function appgroupmanage(){
        $this->load->model('AppGroupModel');
        $list = $this->AppGroupModel->getList(array('status'=>0), '*', ' max desc');
        $groupdata = $this->AppInfoModel->getList(array(), 'count(*) as num,appgroupid', '', 'appgroupid');
        $groupappnum = array();
        foreach ($groupdata as $v)
        {
            $groupappnum[$v['appgroupid']] = $v['num'];
        }
        foreach ($list as $k => $v)
        {
            if(isset($groupappnum[$v['id']]))
            {
                $list[$k]['appnum'] = $groupappnum[$v['id']];
            }
            else
            {
                $list[$k]['appnum'] = 0;
            }
        }
        $this->assign('list', $list);
        $this->display('admin/app/appgroupmanage.html');
    }

    /**
     * 设置媒体分组
     * Enter description here ...
     */
    function ajaxsaveappinfogroup(){
        $appgroupid	= $this->input->post("appgroupid");
        $appid = $this->input->post("appid");
        $status = $this->AppInfoModel->save(array('appid' => $appid), array('appgroupid' => $appgroupid));
        $this->load->model("AppInfoGroupLogModel");
        $status = $this->AppInfoGroupLogModel->add(array('appid' => $appid, 'appgroupid' => $appgroupid, 'addtime' => time(), 'adminid' => $this->adminId));
        if($status)
        {
            ajaxReturn(1, 1, '');
        }
        else
        {
            ajaxReturn("修改错误，请刷新重试！", 0, '');
        }
    }

    /**
     * 获取单个媒体分组信息字典
     * Enter description here ...
     */
    function ajaxgetappgroupinfo(){
        $this->load->model('AppGroupModel');
        $id = $this->input->get('id');
        $status = 0;
        if(empty($id))
        {
            $data = "id错误！";
        }
        else
        {
            $info = $this->AppGroupModel->getRow(array('id'=>$id));
            if(empty($info))
            {
                $data = "id错误！";
            }
            else
            {
                $data['id'] = $info['id'];
                $data['groupname'] = $info['groupname'];
                $data['max'] = $info['max'];
                $data['min'] = $info['min'];
                $status = 1;
            }
        }
        ajaxReturn($data, $status, '');
    }

    /**
     * 设置单个媒体分组信息字典
     * Enter description here ...
     */
    function ajaxsaveappgroup(){
        $this->load->model('AppGroupModel');
        $id = trim($this->input->post('id'));
        $groupname = trim($this->input->post('groupname'));
        $max = trim($this->input->post('max'));
        $min = trim($this->input->post('min'));
        $subtype = $this->input->post('subtype');
        if(empty($groupname))
        {
            ajaxReturn('分组名称不能为空！', 0, '');
        }
        if(!is_numeric($max) || $max < 0 || !is_numeric($min) || $min < 0)
        {
            ajaxReturn("上下限请输入数字！", 0, '');
        }

        if($subtype == 'edit')
        {
            $chkInfo = $this->AppGroupModel->getRow(array('groupname'=>$groupname, 'id'=>array('neq', $id), 'status'=>0));
        }
        else
        {
            $chkInfo = $this->AppGroupModel->getRow(array('groupname'=>$groupname, 'status'=>0));
        }
        if(!empty($chkInfo))
        {
            ajaxReturn("分组名称不能重复！", 0, '');
        }
        $status = 1;
        $data['groupname'] = $groupname;
        $data['max'] = $max;
        $data['min'] = $min;
        $data['edittime'] = time();
        $data['adminid'] = $this->adminInfo['admin_id'];
        if($subtype == 'edit' && $id > 0)
        {
            $this->AppGroupModel->save(array('id'=>$id),$data);
            $newid = $id;
        }
        else
        {
            $data['addtime'] = time();
            $newid = $this->AppGroupModel->add($data);
        }
        $data['addtime'] = time();
        $data['logid'] = $newid;
        $data['status'] = 1;
        $this->AppGroupModel->add($data);
        ajaxReturn($data, $status, '');
    }
    
/**
     * edit password for developers
     */
    function ajaxresetpwd(){
        $userid = trim($this->input->post('userid'));
        $pwd = trim($this->input->post('pwd'));
        $pwdConf = trim($this->input->post('pwdConf'));
        if(empty($userid))
        {
        	ajaxReturn("未知错误", 0, "");
        }
        if(empty($pwd))
        {
            ajaxReturn('密码不能为空！', 0, '');
        }
        if(empty($pwdConf))
        {
        	ajaxReturn('确认密码不能为空！', 0, '');
        }
        if(strlen($pwd) < 6 || strlen($pwd) > 16)
        {
            ajaxReturn("新密码不能少于6位或大于16位！", 0, "");
        }
        if($pwd != $pwdConf)
        {
           	ajaxReturn("两次输入密码不一致，请确认新密码！", 0, "");
        }
        
        $password = Security::cryptPassword($pwd);
        $this->UserMemberModel->edit(array('userid'=>$userid), array('password'=>$password));
        ajaxReturn('', 1, '');
    }
    
    /**
     * 删除单个媒体分组信息字典
     * Enter description here ...
     */
    function ajaxdelappgroup(){
        $this->load->model('AppGroupModel');
        $id = trim($this->input->post('id'));
        if($id > 0)
        {
            $this->AppGroupModel->save(array('id'=>$id), array('edittime'=>time(), 'status'=>2, 'adminid'=>$this->adminId));
            $this->AppInfoModel->save(array('appgroupid'=>$id), array('appgroupid'=>''));
        }
        ajaxReturn('成功删除！', 1, '');
    }

    //切量媒体管理
    function mediacutovermanage()
    {
        $keys = $this->input->get('search');
        $p=trim($this->input->get('per_page'));
        if(!$p){
            $p=1;
        }
        $this->load->model("MediaCutoverManageModel");
        $where = array();
        if(!empty($keys))
        {
            $where = array('medianame'=>array('LIKE',"%".$keys."%"));
        }
        $medialist = $this->MediaCutoverManageModel->getList($where);
        foreach($medialist as $k=>$v)
        {
            $medialist[$k]['supporttype'] = str_replace('1', "Banner广告;", $v['adtype']);
            $medialist[$k]['supporttype'] = str_replace('2', "插屏广告;", $medialist[$k]['supporttype']);
            $medialist[$k]['supporttype'] = str_replace(',', "", $medialist[$k]['supporttype']);
            $medialist[$k]['supportos'] = str_replace('1', "Android;", $v['ostype']);
            $medialist[$k]['supportos'] = str_replace('2', "iOS;", $medialist[$k]['supportos']);
            $medialist[$k]['supportos'] = str_replace(',', "", $medialist[$k]['supportos']);
        }
        $this->load->library('page');
        $current_page =  $p==0 ? 1 : $p;
        $var = '&search='.$keys;
        $config['per_page'] = 20;
        $count = $this->MediaCutoverManageModel->getCount();
        $page=$this->page->page_show('/admin/app/mediacutovermanage?'.$var,$count,$config['per_page']);
        $this->pagination->create_links();
        $this->smarty->assign('page', $page);
        $this->smarty->assign('per_page', $config['per_page']);
        $this->smarty->assign('search', $keys);
        $this->smarty->assign('medialist', $medialist);
        $this->smarty->assign('count', count($medialist));
        $this->display('admin/app/mediacutovermanage.html');
    }

    //录入编辑媒体
    function ajaxsavemedia()
    {
        $ostype = $this->input->post('ostype');
        $adtype = $this->input->post('adtype');
        $id = $this->input->post('id');
        $medianame = $this->input->post('medianame');
        $mediaurl = $this->input->post('mediaurl');
        $mediaport = $this->input->post('mediaport');
        $mediapath = $this->input->post('mediapath');
        $dspappaccount = $this->input->post('dspappaccount');
        $subtype = $this->input->post('subtype');

        $postData['adtype'] = $adtype;
        $postData['ostype'] = $ostype;
        $postData['medianame'] = $medianame;
        $postData['mediaurl'] = $mediaurl;
        $postData['mediaport'] = $mediaport;
        $postData['mediapath'] = $mediapath;
        $postData['dspappaccount'] = $dspappaccount;
        $this->load->model("MediaCutoverManageModel");
        if($subtype == 'edit' && $id > 0)
        {
            $where['id'] = $id;
            $postData['edittime'] = time();
            $check = $this->MediaCutoverManageModel->getRow(array('mediaport' => $mediaport, 'id'=>array('neq', $id)));
            if($check)
            {
                ajaxReturn('端口号不能重复！', 0, '端口号不能重复！');
            }


            $num = $this->MediaCutoverManageModel->edit($where, $postData);
        }
        else
        {
            $check = $this->MediaCutoverManageModel->getRow(array('mediaport' => $mediaport));
            if($check)
            {
                ajaxReturn('端口号不能重复！', 0, '端口号不能重复！');
            }
            $postData['createtime'] = time();
            $postData['edittime'] = time();
            $num = $this->MediaCutoverManageModel->Add($postData);
        }

        if($num == 0)
        {
            ajaxReturn('修改错误，请刷新重试！', 0, '修改错误，请刷新重试！');
        }
        ajaxReturn('', 1, '');
    }

    //录入编辑媒体
    function ajaxsaveclickefect()
    {
        $appid = $this->input->post('appid');
        $adform = $this->input->post('adform');
        $effecttype = $this->input->post('effecttype');
        $this->load->model("AppClickEffectModel");
        $id = $this->AppClickEffectModel->getOne(array('appid'=>$appid, 'adform'=>$adform), 'id');
        $data['appid'] = $appid;
        $data['adform'] = $adform;
        $data['effecttype'] = $effecttype;
        $data['edittime'] = time();
        $data['adminid'] = $this->adminInfo['admin_id'];
        if($id>0)
        {
            $num = $this->AppClickEffectModel->save(array('id'=>$id), $data);
        }
        else
        {
            $num = $this->AppClickEffectModel->Add($data);
        }
        if($num)
        {
            ajaxReturn('', 1, '');
        }
        else
        {
            ajaxReturn('', 0, '');
        }
    }
    
    function ajaxsaveghostclick(){
    	$appid = $this->input->post('appid');
    	$switchkey=$this->input->post('switchkey');
        $switchval=$this->input->post('switchval');
   		$this->load->model("AppInfoModel");
        $num_ghostclick=$this->AppInfoModel->update(array("appid"=>$appid),array($switchkey=>$switchval));
        if($num_ghostclick)
        {
            ajaxReturn('', 1, '');
        }
        else
        {
            ajaxReturn('', 0, '');
        }
    }
    
    function chkSaveFeeds($data)
    {
    	$error = array('status' => 1, 'data'=>'');
    	if($data['feedsswitch'] == 0)
    	{
    		return $error;
    	}
        if(empty($data['feedsiostpl']))
        {
            $error['data'] = array('iostplmsg', 'IOS模板不能为空。');
            $error['info'] = 'iostplerr';
            $error['status'] = 0;
            return $error;
        }
    	elseif(empty($data['feedsandroidtpl']))
        {
            $error['data'] = array('androidtplmsg', 'Android模板不能为空。');
            $error['info'] = 'androidtplerr';
            $error['status'] = 0;
            return $error;
        }
        return $error;
    }

    //录入编辑banner 三栏设置。
    function ajaxappsetbannermgswitch()
    {
        $appid = $this->input->post('appid');
        $bannermgtime = $this->input->post('bannermgtime');
        $bannermgswitch = $this->input->post('bannermgswitch');

        if(empty($appid))
        {
            ajaxReturn('APPID错误，请刷新重试！', 0, '');
        }

        if($bannermgswitch == 1 && !is_numeric($bannermgtime))
        {
            ajaxReturn('请设置正确的时间！', 0, '');
        }

        if($bannermgswitch == 1 && $bannermgtime < 10)
        {
            ajaxReturn('时间不能小于10秒！', 0, '');
        }
        if($bannermgswitch == 1)
        {
            $data['bannermgtime'] = $bannermgtime;
        }
        $this->load->model("AppInfoModel");
        $where['appid'] = $appid;
        $data['bannermgswitch'] = $bannermgswitch;
        $data['uptime'] = time();
        $num = $this->AppInfoModel->save($where, $data);
        if($num == 0)
        {
            ajaxReturn('未知错误，请刷新重试！', 0, '修改错误，请刷新重试！');
        }
        ajaxReturn('', 1, '');
    }

    //删除切量设置。
    function ajaxdelcutoverset()
    {
        $id = $this->input->get('id');
        if(empty($id))
        {
            ajaxReturn('', 0, '');
        }
        $this->load->model("MediaRuleModel");
        $num = $this->MediaRuleModel->save(array('id'=>$id), array('isstatus' => 1));
        if($num == 0)
        {
            ajaxReturn('', 0, '');
        }
        ajaxReturn('', 1, '');
    }
    //录入编辑媒体
    function ajaxgetmediacutoverinfo()
    {
        $id = $this->input->get('id');

        if(empty($id))
        {
            ajaxReturn('错误的ID，请刷新重试！', 0, '错误的ID，请刷新重试！');
        }
        $this->load->model("MediaCutoverManageModel");
        $data = $this->MediaCutoverManageModel->getRow(array('id'=>$id));

        if(empty($data))
        {
            ajaxReturn('错误的ID，请刷新重试！', 0, '错误的ID，请刷新重试！');
        }

        $postData['adtype'] = explode(',', $data['adtype']);
        $postData['ostype'] = explode(',', $data['ostype']);
        $postData['medianame'] = $data['medianame'];
        $postData['mediaurl'] = $data['mediaurl'];
        $postData['mediaport'] =  $data['mediaport'];
        $postData['mediapath'] = $data['mediapath'];
        $postData['dspappaccount'] = $data['dspappaccount'];
        $postData['id'] = $data['id'];
        ajaxReturn($postData, 1, '');
    }
    
	function ajaxgetmediacutoveraccountinfo()
    {
        $id = $this->input->get('id');

        if(empty($id))
        {
            ajaxReturn('错误的ID，请刷新重试！', 0, '错误的ID，请刷新重试！');
        }
        $this->load->model("MediaCutoverAccountModel");
        $info = $this->MediaCutoverAccountModel->getRow(array('id'=>$id,'status'=>1));

        if(empty($info))
        {
            ajaxReturn('错误的ID，请刷新重试！', 0, '错误的ID，请刷新重试！');
        }

        $data['id'] = $info['id'];
        $data['appid'] = $info['appid'];
        $data['mediacutoverid'] = $info['mediacutoverid'];
        $data['mediumid'] = $info['mediumid'];
        $data['bannerplacementid'] = $info['bannerplacementid'];
        $data['popupplacementid'] = $info['pupopplacementid'];
        ajaxReturn($data, 1, '');
    }
    
    function cutovermanage(){
        $data = array();
        $os = intval($this->input->get('os')) == 0 ? 1 : intval($this->input->get('os'));
        $search = $this->input->get('search');
        $stype = $this->input->get('stype') ?  intval($this->input->get('stype')) : 1 ;
        $p=trim($this->input->get('per_page'));
        $adtype = $this->input->get('adtype');
        if(empty($adtype))
        {
            $adtype = 1;
        }
        if(!$p){
            $p=1;
        }
        $data=$users=array();
        $data['ostypeid']=$os;
        $where = " ostypeid=".$os;
        if($stype == 1){
            if(!empty($search)){
                $data['appname']=array('LIKE',"%".$search."%");
                $where = $where." and  appname like '%$search%'";
            }
        }else{
            if(!empty($search)){
                $datas['username']=array('LIKE',"%".$search."%");
            }
            $info = $this->UserMemberModel->getList($datas,'userid');
             
            while(list($k,$v) = each($info)){
                $users[]=$v['userid'];
            }
        }
        if(count($users)){
            $data['userid']=array("in",implode(",",$users));
            $where .= " and userid in ({$data['userid'][1]}) ";
        }

        $this->AppInfoModel->setStrWhere($where);
        $config['per_page'] = 20;
        $count = $this->AppInfoModel->getCount();
        $applist = $this->AppInfoModel->getList($data,"*","cutoverstatus desc", "", $p,$config['per_page']);
        $appids = array();
        if($count){
            foreach($applist as $k=>$v){
                $info = $this->UserMemberModel->getRow(array("userid"=>$v['userid']));
                $applist[$k]['realname']=$info['realname'];
                $applist[$k]['username']=$info['username'];
                $applist[$k]['appchildtypeid'] = explode(',', trim($v['appchildtypeid']));
                $appids[] = $v['appid'];
            }
        }
        $this->load->library('page');
         
        $current_page =  $p==0 ? 1 : $p;
        $var = 'stype='.$stype.'&os='.$os.'&search='.$search.'&adtype='.$adtype;
        $page=$this->page->page_show('/admin/app/cutovermanage?'.$var,$count,$config['per_page']);

        $this->pagination->create_links();
        $this->load->model("AppTypeModel");
        $apptype = $this->AppTypeModel->getList();
        $app = array();
        foreach($apptype as $val){
            $app[$val['apptypeid']] = $val['typename'];
        }

        $this->load->model('MediaRuleModel');
        $this->load->model('MediaCutoverManageModel');

        $ruledata = $this->MediaRuleModel->getList(array('appid' => array('in',$appids), 'adtype' => $adtype, 'ostype' => $os, 'isstatus' => 0), '*', ' edittime desc');
        $medialist = $this->MediaCutoverManageModel->getList(array('adtype' =>array('LIKE' , '%'.$adtype.'%'), 'ostype' => array('LIKE', '%'.$os.'%')));
        $mediadata = array();
        if(!empty($medialist))
        {
            foreach($medialist as $v)
            {
                $mediadata[$v['id']] = $v['medianame'];
            }
        }
        $apptomediarule = array();
        if(!empty($ruledata))
        {
            foreach($ruledata as $v)
            {
                if(isset($mediadata[$v['mediaid']]))
                {
                    $apptomediarule[$v['appid']][] = $mediadata[$v['mediaid']];
                }
            }
        }
        foreach($applist as $k=>$v){
            if(isset($apptomediarule[$v['appid']]))
            {
                $applist[$k]['rulename'] = implode(',', $apptomediarule[$v['appid']]);
            }
            else
            {
                $applist[$k]['rulename'] = '';
            }
        }

        $ruledataarray = array();
        $this->smarty->assign('search', $search);
        $this->smarty->assign('ruledataarray', $ruledataarray);
        $this->smarty->assign('os', $os);
        $this->smarty->assign('page', $page);
        $this->smarty->assign('per_page', $config['per_page']);
        $this->smarty->assign('count', $count);
        $this->smarty->assign('app', $app);
        $this->smarty->assign('stype', $stype);
        $this->smarty->assign('adtype', $adtype);
        $this->assign('applist', $applist);
        $this->display('admin/app/cutovermanage.html');
    }

    /**
     * 添加切量设置
     * Enter description here ...
     */
    function showaddcutoverset()
    {
        $appid = $this->input->get('appid');
        $os = intval($this->input->get('os')) == 0 ? 1 : intval($this->input->get('os'));
        $search = $this->input->get('search');
        $stype = $this->input->get('stype') ?  intval($this->input->get('stype')) : 1 ;
        $p=trim($this->input->get('per_page'));
        $adtype = $this->input->get('adtype');

        $channeldata = $this->AppInfoModel->getAppChannelRow($appid);
        $this->load->model('CutoverRuleModel');
        $this->load->model('MediaRuleModel');
        $this->load->model('MediaCutoverManageModel');
        $this->load->model('MediaCutoverAccountModel');
        $ruledata = $this->CutoverRuleModel->getRow(array('appid' => $appid, 'adtype' => $adtype, 'ostype'=>$os));
        if(!empty($ruledata))
        {
            $tmppids = explode(',', $ruledata['pids']);
            $ruledata['channelids'] = $tmppids;
        }
        $mediadata = $this->MediaCutoverManageModel->getList(array('adtype' =>array('LIKE' , '%'.$adtype.'%'), 'ostype' => array('LIKE', '%'.$os.'%')));
        $mediadspdata = $this->MediaCutoverAccountModel->getList(array('appid' =>$appid,'status'=>1));
        $mediaDSPList = array();
        foreach($mediadspdata as $v)
        {
            $mediaDSPList[$v['mediacutoverid']] = $v;
        }
        $mediaList = array();
        $mediaDSPSelectList = array();
        if(!empty($mediadata))
        {
            foreach ($mediadata as $v)
            {
                $mediaList[$v['id']] = $v['medianame'];
                if(isset($mediaDSPList[$v['id']]) && $appid == $mediaDSPList[$v['id']]['appid'])
                {
                    $mediaDSPList[$v['id']]['medianame'] = $v['medianame'];
                }
                if($v['dspappaccount'] == 1 && ((isset($mediaDSPList[$v['id']]) &&  $appid != $mediaDSPList[$v['id']]['appid']) || !isset($mediaDSPList[$v['id']])))
                {
                    $mediaDSPSelectList[$v['id']] = $v['medianame'];
                }
            }
        }
        $mediaruledata = $this->MediaRuleModel->getList(array('appid' => $appid, 'adtype' => $adtype, 'isstatus' => array('eq',0)));
        $mediarulelist = array();
        if(!empty($mediaruledata))
        {
            foreach ($mediaruledata as $v)
            {
                $mediarulelist[$v['id']] = $v;
                $mediarulelist[$v['id']]['starttimestr'] = date('Y-m-d', $v['starttime']);
                $mediarulelist[$v['id']]['endtimestr'] = date('Y-m-d', $v['endtime']);
                if($v['ostypeset'])
                {
                    $mediarulelist[$v['id']]['ostypeset'] = '自定义：'.$v['lowosv'].'~'.$v['highosv'];
                }
                else
                {
                    $mediarulelist[$v['id']]['ostypeset'] = '所有版本';
                }
                if($v['devicetypeset'])
                {
                    $mediarulelist[$v['id']]['devicetypeset'] = implode(',', json_decode($v['devicetype'], true));
                }
                else
                {
                    if($os == 1)
                    {
                        $mediarulelist[$v['id']]['devicetypeset'] = 'Android 所有设备';
                    }
                    else
                    {
                        $mediarulelist[$v['id']]['devicetypeset'] = 'iOS 所有设备';
                    }
                }
                if($v['nettypeset'])
                {
                    $tmps = json_decode($v['nettype'], true);
                    $tmpl = array();
                    foreach ($tmps as $tv)
                    {
                        switch ($tv)
                        {
                            case 1:
                                $tmpl[] = 'Wi-Fi';
                                break;
                            case 2:
                                $tmpl[] = '2G';
                                break;
                            case 3:
                                $tmpl[] = '3G';
                                break;
                            case 4:
                                $tmpl[] = '其他';
                                break;
                        }
                    }
                    $mediarulelist[$v['id']]['nettypeset'] = implode(',', $tmpl);
                }
                else
                {
                    $mediarulelist[$v['id']]['nettypeset'] = '所有网络类型';
                }
                if($v['operatorsset'])
                {
                    $tmps = json_decode($v['operators'], true);
                    $tmpl = array();
                    foreach ($tmps as $tv)
                    {
                        switch ($tv)
                        {
                            case 1:
                                $tmpl[] = '中国移动';
                                break;
                            case 2:
                                $tmpl[] = '中国电信';
                                break;
                            case 3:
                                $tmpl[] = '中国联通';
                                break;
                        }
                    }
                    $mediarulelist[$v['id']]['operatorsset'] = implode(',', $tmpl);
                }
                else
                {
                    $mediarulelist[$v['id']]['operatorsset'] = '所有运营商';
                }
	            if($v['domainfiltertype']!="" && $v['domainfiltertype']!=null){
					switch ($v['domainfiltertype']){
						case '1':
							$mediarulelist[$v['id']]['areaset']='自定义中国大陆及港澳台地区';
					        break;
						case '2':
							$mediarulelist[$v['id']]['areaset']='自定义海外地域';
					        break;
						case '0':
							$mediarulelist[$v['id']]['areaset']='所有国家和地区';
					        break;
					    default:
					        $mediarulelist[$v['id']]['areaset']='所有国家和地区';
					        break;
					}
				}
				else{
					if($v['areaset']==null || $v['areaset']==""){
						$mediarulelist[$v['id']]['areaset']='所有国家和地区';
					}
					if($v['areaset']!=null && $v['areaset']!="" && strpos($v['areaset'],'CN')!==false){
						$mediarulelist[$v['id']]['areaset']= '自定义中国大陆及港澳台地区';
					}
					if($v['areaset']!=null && $v['areaset']!="" && strpos($v['areaset'],'CN')===false){
						$mediarulelist[$v['id']]['areaset']= '自定义海外地域';
					}
				}
                
                if($v['scheduletypeset'])
                {
                    $mediarulelist[$v['id']]['scheduletypesetdes'] = '自行选择';
                    $mediarulelist[$v['id']]['ratio'] = '分时段设置';
                }
                else
                {
                    $mediarulelist[$v['id']]['scheduletypesetdes'] = '全时段切量';
                }
                if(isset($mediaList[$v['mediaid']]))
                {
                    $mediarulelist[$v['id']]['medianame'] = $mediaList[$v['mediaid']];
                }
                else
                {
                    $mediarulelist[$v['id']]['medianame'] = '未知';
                }
            }
        }
        $this->load->model("AppDspCutoverFloorsModel");
        $appdspcutoverfloors = $this->AppDspCutoverFloorsModel->getList(array('appid' => $appid, 'status' => 1),"*","id DESC");
        
    	$this->load->model("MediaCutoverManageModel");
    	$medialist = $medialisttmp = $this->MediaCutoverManageModel->getList("","*","id DESC");
        $othermedia=array();
        if(!empty($medialisttmp) && isset($medialisttmp)){
        	if(!empty($appdspcutoverfloors) &&  isset($appdspcutoverfloors)){
        		foreach($appdspcutoverfloors as  $appdsp){
        			foreach ($medialisttmp as $kk=>$media){
	        			if($media['id']==$appdsp['meidiacutovermanageid']){
	        				 unset($medialist[$kk]);
	        			}
		        	}
		        }
		        $othermedia=$medialist;
        		
        	}else{
        		$othermedia=$medialist;
        	}
        }
        $appinfo=$this->AppInfoModel->getList(array('appid'=>$appid));
        $this->assign('dspfloorsswitch',$appinfo[0]['dspfloorsswitch']);
        $this->assign('othermedia', $othermedia);
        $this->assign('appdspcutoverfloors',$appdspcutoverfloors);
        $this->assign('channeldata', $channeldata);
        $this->assign('ruledata', $ruledata);
        $this->assign('mediaDSPList', $mediaDSPList);
        $this->assign('mediaDSPSelectList', $mediaDSPSelectList);
        $this->assign('mediarulelist', $mediarulelist);
        $this->assign('appid', $appid);
        $this->assign('os', $os);
        $this->assign('search', $search);
        $this->assign('stype', $stype);
        $this->assign('p', $p);
        $this->assign('adtype', $adtype);
        $this->display('admin/app/showaddcutoverset.html');
    }

    function showcutoverlist()
    {
        $appid = $this->input->get('appid');
        $os = intval($this->input->get('os')) == 0 ? 1 : intval($this->input->get('os'));
        $search = $this->input->get('search');
        $stype = $this->input->get('stype') ?  intval($this->input->get('stype')) : 1 ;
        $p=trim($this->input->get('per_page'));
        $adtype = $this->input->get('adtype');

        $this->load->model('MediaRuleModel');
        $this->load->model('MediaCutoverManageModel');
        $mediarulelist = $this->MediaRuleModel->getList(array('appid' => $appid, 'adtype' => $adtype, 'ostype'=>$os, 'isstatus' => 0));
        $medialist = $this->MediaCutoverManageModel->getList(array('adtype' =>array('LIKE' , '%'.$adtype.'%'), 'ostype' => array('LIKE', '%'.$os.'%')));
        $mediadspdata = $this->MediaCutoverAccountModel->getList(array('appid' =>$appid,'status'=>1));
        $mediaDSPList = array();
        foreach($mediadspdata as $v)
        {
            $mediaDSPList[$v['mediacutoverid']] = $v;
        }
        $mediaruledata = array();
        $mediadata = array();
        if(!empty($mediarulelist))
        {
            foreach($mediarulelist as $v)
            {
                $mediaruledata[$v['mediaid']] = $v;
            }
        }
        if(!empty($medialist))
        {
            foreach($medialist as $v)
            {
                if(isset($mediaruledata[$v['id']]))
                {
                    continue;
                }
                if($v['dspappaccount'] == 1  && ((isset($mediaDSPList[$v['id']]) &&  $appid != $mediaDSPList[$v['id']]['appid']) || !isset($mediaDSPList[$v['id']])))
                {
                    continue;
                }
                $mediadata[$v['id']] = $v;
            }
        }
        $this->load->model('AdAreaModel');
        $arealist = $this->AdAreaModel->getAreaTree();
        $this->load->model('AdAreaCountryModel');
        $countryarealist=$this->AdAreaCountryModel->getAreaList();
        //类型转换
        if($os==1)
        {
            $oswhere = array('ostype'=>0);
            $sdkoswhere = array('type'=>2);
        }
        else
        {
            $oswhere = array('ostype'=>1);
            $sdkoswhere = array('type'=>1);
        }
        $osversionlist = $this->OsVersionModel->getList($oswhere);
        $this->load->model("SdkversionLogModel");
        $sdkversionlist = $this->SdkversionLogModel->getList($sdkoswhere);
        $this->assign('osversionlist', $osversionlist);
        $this->assign('mediadata', $mediadata);
        $this->assign('sdkversionlist', $sdkversionlist);
        $this->assign('appid', $appid);
        $this->assign('arealist', $arealist);
        $this->assign('countryarealist', $countryarealist);
        $this->assign('os', $os);
        $this->assign('search', $search);
        $this->assign('stype', $stype);
        $this->assign('p', $p);
        $this->assign('adtype', $adtype);
        $this->display('admin/app/showcutoverlist.html');
    }

    /**
     * 编辑切量规则
     * Enter description here ...
     */
    function showeditcutoverlist()
    {
        $appid = $this->input->get('appid');
        $id = $this->input->get('id');
        $os = intval($this->input->get('os')) == 0 ? 1 : intval($this->input->get('os'));
        $search = $this->input->get('search');
        $stype = $this->input->get('stype') ?  intval($this->input->get('stype')) : 1 ;
        $p=trim($this->input->get('per_page'));
        $adtype = $this->input->get('adtype');

        $this->load->model('MediaRuleModel');
        $this->load->model('MediaCutoverManageModel');
        $mediarulerow = $this->MediaRuleModel->getRow(array('id' => $id, 'adtype' => $adtype, 'ostype'=>$os, 'isstatus' => 0));
        $medialist = $this->MediaCutoverManageModel->getList(array('adtype' =>array('LIKE' , '%'.$adtype.'%'), 'ostype' => array('LIKE', '%'.$os.'%')));
        $mediaruledata = array();
        $mediadata = array();
        if(empty($mediarulerow))
        {
            return ;
        }
        if(!empty($medialist))
        {
            foreach($medialist as $v)
            {
                if($mediarulerow['mediaid'] == $v['id'])
                {
                    $mediarulerow['medianame'] = $v['medianame'];
                    break;
                }
            }
        }

        $mediarulerow['devicetype'] = empty($mediarulerow['devicetype'])? array():json_decode($mediarulerow['devicetype'], true);
        $mediarulerow['nettype'] = empty($mediarulerow['nettype'])? array():json_decode($mediarulerow['nettype'], true);
        $mediarulerow['operators'] = empty($mediarulerow['operators'])? array():json_decode($mediarulerow['operators'], true);
        $mediarulerow['sdkvset'] = empty($mediarulerow['sdkversionset'])? array():json_decode($mediarulerow['sdkvset'], true);
        $mediarulerow['schedule'] = "''";
        $mediarulerow['daybudget'] = "''";
        if($mediarulerow['scheduletypeset'])
        {
            $timelist = json_decode($mediarulerow['timelist'], true);
            $mediarulerow['schedule'] = $timelist['data'] == 0 ? "''" : json_encode($timelist['data']);
            $mediarulerow['daybudget'] = $timelist['daybudget'] == 0 ? "''" : "'".$timelist['daybudget']."'";
        }
        if( $mediarulerow['areaset'] != "")
        {
            $mediarulerow['areaids'] = json_decode($mediarulerow['areaids'], true);
        }
        else
        {
            $mediarulerow['areaids'] = array();
        }
        $this->load->model('AdAreaModel');
        $arealist = $this->AdAreaModel->getAreaTree();
        //类型转换
        if($os==1)
        {
            $oswhere = array('ostype'=>0);
            $sdkoswhere = array('type'=>2);
        }
        else
        {
            $oswhere = array('ostype'=>1);
            $sdkoswhere = array('type'=>1);
        }
        $osversionlist = $this->OsVersionModel->getList($oswhere);
        $this->load->model("SdkversionLogModel");
        $sdkversionlist = $this->SdkversionLogModel->getList($sdkoswhere);
        
        $this->load->model('AdAreaCountryModel');
        $countryarealist=$this->AdAreaCountryModel->getAreaList();
        
		$areaIdsArray = array();
		if(!empty($mediarulerow['areaids']))
		{
		    foreach($mediarulerow['areaids'] as $v)
		    {
		        $areaIdsArray[$v] = $v;
		    }
		}
		
		$this->assign('gareaids',implode(',',$mediarulerow['areaids']));
        $this->assign('areaIdsArray', $areaIdsArray);
        $this->assign('countryarealist',$countryarealist);
        $this->assign('sdkversionlist', $sdkversionlist);
        $this->assign('osversionlist', $osversionlist);
        $this->assign('mediarulerow', $mediarulerow);
        $this->assign('appid', $appid);
        $this->assign('arealist', $arealist);
        $this->assign('os', $os);
        $this->assign('search', $search);
        $this->assign('stype', $stype);
        $this->assign('p', $p);
        $this->assign('adtype', $adtype);
        $this->display('admin/app/showeditcutoverlist.html');
    }

    function changestatus(){
        $data = array();
        $switch = (int)$this->input->get('switch');
        $switchtype = $this->input->get('switchtype');
        if($switchtype == 'switch')
        {
            $data['switch'] = $switch ? 1 : 0;
        }
        elseif($switchtype == 'adminswitch')
        {
            $data['adminswitch'] = $switch ? 1 : 0;
        }
        elseif($switchtype == 'fullwtlistswitch')
        {
            $data['fullwtlistswitch'] = $switch ? 1 : 0;
        }
        elseif($switchtype == 'antifraudswitch')
        {
            $data['antifraudswitch'] = $switch ? 1 : 0;
        }
        elseif($switchtype == 'debugsdkswitch')
        {
            $data['debugsdkswitch'] = $switch ? 1 : 0;
        }
        elseif($switchtype == 'lowqualityswitch')
        {
            $data['lowqualityswitch'] = $switch ? 1 : 0;
        }
        elseif($switchtype == 'dmpswitch')
        {
            $data['dmpswitch'] = $switch ? 1 : 0;
            if(1 == $data['dmpswitch']){
            	$query = $this->AppInfoModel->getRow(array('appid'=>$this->input->get('appid')), 'ctrswitch');
            	if(!$query){
            		ajaxReturn('未知错误', 0, '');
            	}
            	$ctrswitch = $query['ctrswitch'];
            	if(1 == $ctrswitch){
            		ajaxReturn('DMP开关与效果预测开关不可同时打开', 0, '');
            	}
            }
        }
        elseif($switchtype == 'ctrswitch')
        {
            $data['ctrswitch'] = $switch ? 1 : 0;
            if(1 == $data['ctrswitch']){
            	$query = $this->AppInfoModel->getRow(array('appid'=>$this->input->get('appid')), 'dmpswitch');
            	if(!$query){
            		ajaxReturn('未知错误', 0, '');
            	}
            	$dmpswitch = $query['dmpswitch'];
            	if(1 == $dmpswitch){
            		ajaxReturn('DMP开关与效果预测开关不可同时打开', 0, '');
            	}
            }
        }
        elseif($switchtype == 'ostypeswitch')
        {
            $data['ostypeswitch'] = $switch ? 1 : 0;
        }
        elseif($switchtype == 'dmpdebugswitch')
        {
            //dmp全局debug开关
            $data['dmpdebugswitch'] = $switch ? 1 : 0;
            $status = $this->SystemConfigModel->edit(array('skey'=>'dmpdebugswitch'), array('sval'=>$data['dmpdebugswitch']));
            if($status)
            {
                ajaxReturn('修改成功', 1, '');
            }
            ajaxReturn('修改失败', 0, '');
        }
        elseif($switchtype == 'forcepaddingswitch')
        {
            $data['forcepaddingswitch'] = $switch ? 1 : 0;
        }
        else
        {
            ajaxReturn('修改失败,请确认操作类型。', 0, '');
        }
        $data['appid'] = $this->input->get('appid');
        $where = array();
        $where = array("appid"=>$data['appid']);
        $status = $this->AppInfoModel->edit($where, $data);
        if($status){
            $this->AppInfoModel->setDec($where, 'isupdate');
            ajaxReturn('修改成功', 1, '');
        }
        ajaxReturn('修改失败', 0, '');
    }

    //服务器对接开关
    function stschangestatus(){
        $data = array();
        $switch = (int)$this->input->get('switch');
        $switchtype = $this->input->get('switchtype');
        $data['appid'] = $this->input->get('appid');
        $switchvalue = $this->input->get('switchvalue');
        $where = array();
        $where = array("appid"=>$data['appid']);

        //获取开关值
        if($switchtype != 's2sswitches' && $switchtype != 's2sclkrdt')
        {
            ajaxReturn('修改失败', 0, '');
        }
        $s2sswitches = $this->AppInfoModel->getOne($where, $switchtype);
        $dataarray = array();
        if(!empty($s2sswitches))
        {
            $dataarray = explode(',', $s2sswitches);
        }
        if($switch == 1)
        {
            $dataarray[] = $switchvalue;
        }
        else
        {
            $mk = array_search($switchvalue, $dataarray);
            if($mk !== false)
            {
                unset($dataarray[$mk]);
            }
        }
        $dataarray = array_unique($dataarray);
        if(empty($dataarray))
        {
            $fmtdata = 0;
        }
        else
        {
            $fmtdata = implode(',', $dataarray);
        }
        $data[$switchtype] = $fmtdata;

        $status = $this->AppInfoModel->edit($where, $data);
        //echo $this->AppInfoModel->getLastSql();
        if($status){
            $this->AppInfoModel->setDec($where, 'isupdate');
            ajaxReturn('修改成功', 1, '');
        }
        ajaxReturn('修改失败', 0, '');
    }
    
	//Feeds开关
    function feedschangestatus(){
    	
        $data = array();
        $switch = (int)$this->input->get('switch');
        $switchtype = $this->input->get('switchtype');
        $appid= $this->input->get('appid');
        $where = array();
        $where = array("appid"=>$appid);
        $data[$switchtype] = $switch;
        //获取开关值
        if($switchtype != 'feedsswitch' && $switchtype != 'feedsclkrdt' && $switchtype != 'feedsimprdt' && $switchtype != 'apiauthswitch')
        {
            ajaxReturn('开关修改失败', 0, '');
        }
        $status = $this->AppInfoModel->edit($where, $data);
        if($status){
            $this->AppInfoModel->setDec($where, 'isupdate');
            ajaxReturn('开关修改成功', 1, '');
        }
        ajaxReturn('开关修改失败', 0, '');
    }


/**
 * 
 * s2s推荐墙支持详情 
 */
    function s2smgdetailchange(){
        $data = array();
        $switch = (int)$this->input->post('switch');
        $switchtype = $this->input->post('switchtype');
        $appid = $this->input->post('appid');
        $data[$switchtype]=$switch;
        $where = array();
        $where = array("appid"=>$appid);
        if(strcmp($switchtype, 's2smgdetailsswitch')!=0)
        {
            ajaxReturn('开关修改失败', 0, '');
        }
        $status = $this->AppInfoModel->edit($where, $data);
        if($status){
            $this->AppInfoModel->setDec($where, 'isupdate');
            ajaxReturn('开关修改成功', 1, '');
        }
        ajaxReturn('开关修改失败', 0, '');
    }
    
    function ajaxsetdmprule(){
        $dmprule = (int)$this->input->post('dmprule');
        $status = $this->SystemConfigModel->edit(array('skey'=>'dmprule'), array('sval'=>$dmprule));
        if($status)
        {
            ajaxReturn('修改成功', 1, '');
        }
        ajaxReturn('修改失败', 0, '');
    }

    function developer(){
        $search = $this->input->get_post('search');
        $usertype = $this->input->get('usertype');
        $this->assign('search', $search);
        $p=trim($this->input->get('per_page'));
        if(!$p){
            $p=1;
        }
        $where = $users = array(); 
        $var = "";       
        $this->assign('usertype', $usertype);
        if(!empty($usertype)){
        	if($usertype == 1){
	        	if($search){
	            	$where['username'] = array (
	                        'LIKE',
	                        "%" . $search . "%" 
	                );
	                $var .= "usertype=".$usertype."search=".$search;
	        	}
        	}
        	if($usertype == 2 || $usertype == 3){
        		if($search){
	            	$where['username'] = array (
	                        'LIKE',
	                        "%" . $search . "%" 
	                );
	                $var .= "search=".$search;
	        	}
	        	$where['usertype'] = array (
                        'LIKE',
                        "%" . $usertype . "%" 
                );
                 $var .= "usertype=".$usertype;
        	}
        	if($usertype == 4){
        		$datas = array();
        		if($search){
	            	$datas['currency'] = array (
	                        'LIKE',
	                        "%" . $search . "%" 
	                );
	               $var .= "usertype=".$usertype."search=".$search;
	        	}
	        	$this->load->model("AccountInfoModel");
	            $info = $this->AccountInfoModel->getList($datas,'userid');
	            while(list($k,$v) = each($info)){
	                $users[]=$v['userid'];
	            }
        	}
        }
    	if(count($users)){
            $where['userid']=array("in",implode(",",$users));
        }
        $config['per_page'] = 20;
        $count = $this->UserMemberModel->getCount($where);
		$users = $this->UserMemberModel->getList($where,"*","userid DESC", "", $p,$config['per_page']);
        $this->load->library('page');
        $current_page =  $p==0 ? 1 : $p;
        $page=$this->page->page_show(base_url().'/admin/app/developer?'.$var,$count,$config['per_page']);
        $this->assign('users', $users);
        $this->smarty->assign('page', $page);
        $this->smarty->assign('per_page', $config['per_page']);
        $this->smarty->assign('count', $count);
        $this->display('admin/app/developer.html');
    }

    /**
     * 展现App信息。
     * @author wangdong
     */
    function view(){
        $appId = $this->input->get('appid');
        $info = $this->AppInfoModel->getRow(array("appid"=>$appId));
        $info['appchildtypeid'] = explode(',', trim($info['appchildtypeid'], ','));
        $this->assign('info', $info);
        $this->assign('data', $info);
        $auditstatus = $this->SystemConfigModel->getSystemConfig('app_audit_status');
        $this->load->model("AppTypeModel");
        $apptype = $this->AppTypeModel->getList();
        $app = array();
        foreach($apptype as $val){
            $app[$val['apptypeid']] = $val['typename'];
        }
        $this->assign('app', $app);
        $this->assign('auditstatus', $auditstatus);

        $this->assign('tab', 'view');
        $this->display('admin/app/view.html');
    }

    function apppositioninfo(){
        $appId = $this->input->get('appid');
        $id = $this->input->get('id');
        if(empty($appId))
        {
            redirect("/admin/app/applist");
            exit;
        }
        $this->assign('id', $id);
        $this->load->model('AppPositionModel');
        $data = $this->AppInfoModel->getDetailInfo($appId);
        $this->assign('info', $data);
        $data['positionList'] = $this->AppPositionModel->getList(array("appid"=>$appId, "status"=>0));
        $data['positionInfo'] = '';
        if($data['positionList'] && $id)
        {
            foreach ($data['positionList'] as $v)
            {
                if($v['id'] == $id)
                {
                    $data['positionInfo'] = $v;
                    break;
                }
            }
        }
        $data['appid'] = $appId;
        $data['positionid'] = $this->AppPositionModel->createPositionId($appId);
        $this->load->model("AdBorderModel");
        $sysBorderList = $this->AdBorderModel->getSysBorders();
        $this->assign('data', $data);
        $this->assign('tab', 'apppositioninfo');
        $this->assign('sysBorderList', $sysBorderList);
        $this->display('admin/app/apppositioninfo.html');
    }

    function ajaxpositiondoadd(){
        $this->load->model('AppPositionModel');
        $postData = $this->input->post();
        if(!isset($postData['positionname']) || empty($postData['positionname'])){
            ajaxReturn('positionnameerr', 0, array('positionnamemsg','广告位名称不能为空'));
        }
        if(!isset($postData['adform']) || empty($postData['adform'])){
            ajaxReturn('adformerr', 0, array('adformmsg', '广告形式不能为空'));
        }
        if($postData['isdefault'] == 0){
        	$info = $this->AppPositionModel->getList(array("appid"=>$postData['appid'],'status'=>0,'adform'=>$postData['adform']));
	        if($info && count($info)>0){
	        	foreach ($info as $val){
	        		if(empty($val['positionid']) || $val['positionid'] == ""){
		        		$msg = "";
			        	if($postData['adform'] == 1){
			        		$msg="Banner广告";
			        	}
			        	elseif ($postData['adform'] == 2){
			        		$msg="弹出广告";
			        	}
			        	elseif ($postData['adform'] == 3){
			        		$msg="精品推荐";
			        	}
			        	elseif ($postData['adform'] == 4){
			        		$msg="积分墙广告";
			        	}
			        	elseif ($postData['adform'] == 20){
			        		$msg="信息流广告";
			        	}
			        	ajaxReturn('adformerr', 0, array('adformmsg','广告形式为'.$msg.'的默认广告位已存在。'));
	        		}
	        		else {
	        			continue;
	        		}
	        	}
	        }
	        $postData['positionid']="";
        }
        else{
        	$info = $this->AppPositionModel->getRow(array("positionname"=>$postData['positionname'],"appid"=>$postData['appid'],'status'=>0));
	        if($info){
	            ajaxReturn('positionnameerr', 0, array('positionnamemsg','广告位名称已存在。'));
	        }
	        $info = $this->AppPositionModel->getRow(array("positionid"=>$postData['positionid'],"appid"=>$postData['appid'],'status'=>0));
	        if($info){
	            ajaxReturn('positioniderr', 0, array('positionidmsg','广告位ID已存在。'));
	        }
        }
        unset($postData['isdefault']);
        $positionInfo = $this->AppPositionModel->doAdd($postData);
        if($positionInfo['status'] == 0)
        {
            ajaxReturn($positionInfo['info'], 0, $positionInfo['data']);
        }
        ajaxReturn('', 1, '');
    }

    function ajaxeditposition(){
        $this->load->model('AppPositionModel');
        $data = array();
        $id = spost('id');
        $data['adform'] = spost('adform');
        $data['appid'] = spost('appid');
        $data['positiondesc'] = spost('positiondesc');
        $data['edittime'] = time();
        $data['positionname'] = spost('positionname');
        if($data['appid'] < 1 || $id == ''){
            ajaxReturn('参数错误！', 0, print_r($_POST, true));
        }
        $status = $this->AppPositionModel->edit(array("appid"=>$data['appid'],'id'=>$id), $data);
        if($status)
        {
            ajaxReturn('成功', 1, '');
        }
        ajaxReturn('失败，没有数据被修改！', 0, '');
    }

    /**
     * 广告位模板设置
     */
    function ajaxappsettpl(){
        $this->load->model('AppPositionModel');
        $where = array();
        $data = array();
        $where['appid'] = spost('appid');
        $where['id'] = spost('id');
        //$data['confirmation'] = spost('confirmation');
        $data['banneradtype'] = spost('banneradtype');
        if(!$where['appid']){
            ajaxReturn('参数错误！', 0, print_r($where, true));
        }
        $status = $this->AppPositionModel->edit($where, $data);
        if($status)
        {
            ajaxReturn('成功！', 1, '');
        }
        else
        {
            ajaxReturn('没有更新！', 1, $this->AppPositionModel->getlastsql());
        }
    }

    function apppositiondel(){
        $this->load->model('AppPositionModel');
        $appId = $this->input->get('appid');
        $Id = sget('id');
        if(empty($appId) || empty($Id))
        {
            redirect("/admin/app/applist");
            exit;
        }
        $positionInfo = $this->AppPositionModel->doDel($this->input->get());
        redirect("/admin/app/apppositioninfo?appid=".$appId);
    }

    function appchannel(){
        $appId = $this->input->get('appid');
        $publisherID = $this->input->get('publisherID');
        if(empty($appId))
        {
            redirect("/admin/app/applist");
            exit;
        }
        $this->load->model('AppInfoModel');
        $data = $this->AppInfoModel->getDetailInfo($appId);
        $this->assign('info', $data);
        $this->load->model('AppChannelModel');
        $data['channellist'] = $this->AppChannelModel->getList(array("appid"=>$appId));

        $channelTypeList = $this->AppChannelModel->getChannelTypeList($data['ostypeid'], $data['userid']);
        $data['channeltype'] = null;
        if($channelTypeList != false)
        {
            foreach ($channelTypeList as $value)
            {
                $data['channeltype'][$value['channelid']] = $value;
            }
        }
        foreach($data['channellist'] as $v)
        {
            if(isset($v['channelid']) && isset($data['channeltype'][$v['channelid']]))
            {
                unset($data['channeltype'][$v['channelid']]);
            }
        }
        $data['appid'] = $appId;
        $data['publisherID'] = $publisherID;
        $this->assign('publisherID', $publisherID);
        $this->assign('appid', $appId);
        $this->assign('channeltype', $data['channeltype']);
        $this->assign('channellist', $data['channellist']);
        $this->assign('data', $data);
        $this->assign('tab', 'appchannel');
        $this->display('admin/app/appchannel.html');
    }

    function apptag(){
        $this->assign('tab', 'apptag');
        $appId = $this->input->get('appid');
        if(empty($appId))
        {
            redirect("/admin/app/applist");
            exit;
        }
        $data = $this->AppInfoModel->getDetailInfo($appId);
        $data['apptaglist'] = $this->CommonModel->table("app_tag_relation")->getList(array('appid'=>$appId));
        $allTagList = $this->CommonModel->table("app_tag")->getList(array());
        $data['alltaglist'] = null;
        if($allTagList != false)
        {
            foreach ($allTagList as $value)
            {
                $data['alltaglist'][$value['tagid']] = $value;
            }
        }
        if(!empty($data['apptaglist']))
        {
            foreach($data['apptaglist'] as $v)
            {
                if(isset($data['alltaglist'][$v['tagid']]))
                {
                    unset($data['alltaglist'][$v['tagid']]);
                }
            }
        }
        $data['appid'] = $appId;

        $this->assign('apptaglist', $data['apptaglist']);
        $this->assign('alltaglist', $data['alltaglist']);
        $this->assign('appid', $appId);
        $this->assign('data', $data);
        $this->assign('info', $data);
        $this->display('admin/app/apptag.html');
    }

    function ajaxapptagAdd(){
        $postData = $this->input->post();
        $this->load->model('AppTagRelationModel');
        $appTagInfo = $this->AppTagRelationModel->doAdd($postData);
        if($appTagInfo['status'] == 0)
        {
            ajaxReturn($appTagInfo['info'], 0, $appTagInfo['data']);
        }
        ajaxReturn('', 1, '');
    }

    function ajaxtagadd(){
        $postData = $this->input->post();
        $this->load->model('AppTagModel');
        $AppTagInfo = $this->AppTagModel->doAdd($postData);
        if($AppTagInfo['status'] == 0)
        {
            ajaxReturn($AppTagInfo['info'], 0, $AppTagInfo['data']);
        }
        ajaxReturn('', 1, $AppTagInfo['id']);
    }

    function appswitch(){
        $appId = $this->input->get('appid');
        $this->load->model('AppInfoModel');
        $info = $this->AppInfoModel->getRow(array("appid"=>$appId));
        $this->assign('info', $info);
        $this->assign('data', $info);
        $this->assign('tab', 'appswitch');
        $this->assign('appid', $appId);
        $this->display('admin/app/appswitch.html');
    }

    function appwall(){
        $appId = $this->input->get('appid');
        $this->load->model('AppInfoModel');
        $info = $this->AppInfoModel->getRow(array("appid"=>$appId));
        $this->assign('info', $info);
        $this->assign('data', $info);
        $this->assign('tab', 'appwall');
        $this->assign('appid', $appId);
        $this->display('admin/app/appwall.html');
    }

    function ownset(){
        $userId = $this->input->get('userid');
        $userType = $this->input->get('usertype');
        $this->UserMemberModel->edit( array('userid' => $userId), array('usertype' => $userType));
        redirect('/admin/app/developer');
    }
    /**
     * 解锁 锁定用户
     */
    function setuserstatus(){
        $userId = $this->input->get('userid');
        $userstatus = $this->input->get('userstatus');
        $this->UserMemberModel->edit( array('userid' => $userId), array('userstatus' => $userstatus));
        redirect('/admin/app/developer');
    }
    function ajaxSwitch(){
        $postData = $this->input->post();
        $appInfo = $this->AppInfoModel->doSwitch($postData);
        if($appInfo['status'] == 0)
        {
            ajaxReturn($appInfo['info'], 0, $appInfo['data']);
        }
        ajaxReturn('', 1, '');
    }

    function editWallSet(){
        $postData = $this->input->post();
        $appInfo = $this->AppInfoModel->doWallSet($postData);
        if($appInfo['status'])
        {
            ajaxReturn('', 1, '');
        }
        else
        {
            ajaxReturn('', 0, $appInfo['data']);
        }
    }


    function ajaxdoAddChannel(){
        $postData = $this->input->post();
        $this->load->model('AppChannelModel');//appid   100370 channelid   1112,
        if(!isset($_POST['appid']) || !isset($_POST['channelid'])){
            ajaxReturn('参数错误！', 0, print_r($postData, true));
        }
        $postData['channelid'] = trim($postData['channelid'], ',');
        $info = $this->AppChannelModel->getRow(array("appid"=>$postData['appid'], 'channelid'=>array("in", $postData['channelid'])));
        if(!empty($info)){
            ajaxReturn('该应用已添加相同渠道！', 0, print_r($postData, true));
        }
        $channelInfo = $this->AppChannelModel->doAddChannel($postData);
        if($channelInfo['status'] == 0)
        {
            ajaxReturn($channelInfo['info'], 0, $channelInfo['data']);
        }
        ajaxReturn('', 1, '');
    }

    /**
     * 设置渠道配置
     * Enter description here ...
     */
    function ajaxsetpidconfig(){
        $pid = $this->input->post('pid');
        $key = $this->input->post('key');
        $value = $this->input->post('value');
        $this->load->model('AppChannelModel');//appid   100370 channelid   1112,
        $this->load->model('PidConfigLogModel');
        if(empty($pid)){
            ajaxReturn('参数错误！', 0, '');
        }
        if(empty($key))
        {
            ajaxReturn('key值参数错误！', 0, '');
        }
        $keys = explode('||', $key);
        $values = explode('||', $value);
        if(count($keys) != count($values))
        {
            ajaxReturn('key-value值参数错误！', 0, '');
        }
        $data=null;
        foreach($keys as $k=>$v)
        {
            if(empty($v))
            {
                continue;
            }
            $tmpv = '';
            if(is_json($values[$k]))
            {
                $tmpv = json_decode($values[$k], true);
            }
            else
            {
                $tmpv = $values[$k];
            }
            $data[$v] = $tmpv;
        }
        if(empty($data))
        {
            ajaxReturn('key-value值参数错误！', 0, '');
        }
        $num = $this->AppChannelModel->edit(array("publisherID"=>$pid), array('publisherconfig'=>json_encode($data)));
        $this->PidConfigLogModel->add(array("publisherid"=>$pid, 'publisherconfig'=>json_encode($data), 'createtime'=>time(), 'adminid'=>$this->adminId));
        if($num){
            $filepath = $this->config->item('pidconfig_file_path');
            if(!file_exists($filepath)) {
                deldir($filepath);
                createDir($filepath);
            }
            $pidfilename = $filepath.$pid.".html";
            file_put_contents($pidfilename,json_encode($data));
            ajaxReturn('保存成功！', 1, '');
        }
        ajaxReturn('保存失败或值未修改', 0, '');
    }
    /**
     * 获取渠道配置数据
     * Enter description here ...
     */
    function ajaxgetpidconfig()
    {
        $pid = $this->input->post('pid');
        $this->load->model('AppChannelModel');//appid   100370 channelid   1112,
        if(empty($pid)){
            ajaxReturn('参数错误！', 0, '');
        }
        $info = $this->AppChannelModel->getRow(array("publisherID"=>$pid));
        if(empty($info))
        {
            ajaxReturn('', 0, '');
        }
        $pidconfig = $info['publisherconfig'];
        if(empty($pidconfig))
        {
            ajaxReturn('', 0, '');
        }
        $pidconfig = json_decode($pidconfig, true);
        ajaxReturn('', 1, $pidconfig);
    }

    function ajaxappchanneldoadd(){
        $postData = $this->input->post();
        $this->load->model('AppChannelModel');
        $channelInfo = $this->AppChannelModel->doAdd($postData);
        if($channelInfo['status'] == 0)
        {
            ajaxReturn($channelInfo['info'], 0, $channelInfo['data']);
        }
        ajaxReturn('', 1, $channelInfo['id']);
    }

    /**
     * 增加媒体切量账号申请
     * Enter description here ...
     */
    function ajaxmediacotoveraccountadd(){
        $appId = $this->input->post('appid');
        $mediaIdDSP = $this->input->post('mediaiddsp');
        $this->load->model('MediaCutoverManageModel');
        $this->load->model('MediaCutoverAccountModel');
        $this->load->model('AppInfoModel');
        if(empty($appId))
        {
            ajaxReturn('', 0, '数据错误，请刷新重试！');
        }
        if(empty($mediaIdDSP) || $mediaIdDSP == 0)
        {
            ajaxReturn('', 0, '请选择申请账号的切量媒体！');
        }
        $appInfo = $this->AppInfoModel->getDetailInfo($appId);
        if(empty($appInfo))
        {
            ajaxReturn('', 0, '数据错误，请刷新重试！');
        }
        $mediaInfo = $this->MediaCutoverManageModel->getRow(array('id'=>$mediaIdDSP));
        if(empty($mediaInfo))
        {
            ajaxReturn('', 0, '数据错误，请刷新重试！');
        }
        $mediaAccountInfo = $this->MediaCutoverAccountModel->getRow(array('appid'=>$appId, 'mediacutoverid'=>$mediaInfo['id'],'status'=>1));
        if(!empty($mediaAccountInfo))
        {
            ajaxReturn('', 0, '该切量媒体已申请账号，请刷新页面重试！');
        }
        if($mediaInfo['mediaport'] == 4007)
        {
            $this->load->model('dspaccount/TXAccountModel');
            //memberId 商务对接的QQ号
            //203643	 ec09540cddb3453c3c20eb9e3ea96147
            $data['memberId'] = 2654604821;//613253813,已经在 e.qq.com/dev下注册的会员，只支持跟商务协商后的 QQ 号
            $data['medium_name'] = mb_substr($appInfo['appname'], 0, 7);
            $data['meidum_type'] = 1;
            $data['biztype'] = "SecondUnion";
            $data['industryId'] = 40403;
            $data['os'] = empty($appInfo['ostypeid']) ? 1 : $appInfo['ostypeid'];
            $data['placement_name'] = $appInfo['appid'].'banner_1';
            $data['placement_type'] = 1;
            $data['placement_sub_type'] = 1;
            $data['ad_pull_mode'] = 'API';
            $bannerplacement = $this->TXAccountModel->getBannerPlacement($data);
            if(empty($bannerplacement) || $bannerplacement['ret'] != 0)
            {
                $msg = '申请失败，请刷新重试！';
                if($bannerplacement['ret'] == 5000)
                {
                    $msg = '广点通服务器繁忙，申请失败，请刷新重试！';
                }
                ajaxReturn(0, 0, $msg);
            }
            $data['placement_name'] = $appInfo['appid'].'popup_13';
            $data['placement_type'] = 2;
            $data['placement_sub_type'] = 13;
            $pupopplacement = $this->TXAccountModel->getPupopPlacement($data);
            if(empty($pupopplacement) || $pupopplacement['ret'] != 0)
            {
                $msg = '申请失败，请刷新重试！';
                if($pupopplacement['ret'] == 5000)
                {
                    $msg = '广点通服务器繁忙，申请失败，请刷新重试！';
                }
                ajaxReturn(0, 0, $msg);
            }
             
            $mediaAccountData['appid'] = $appId;
            $mediaAccountData['mediacutoverid'] = $mediaIdDSP;
            $mediaAccountData['memberid'] = 2654604821;
            $mediaAccountData['mediumid'] = $bannerplacement['data']['app_id'];
            $mediaAccountData['bannerplacementid'] = $bannerplacement['data']['placement_id'];
            $mediaAccountData['pupopplacementid'] = $pupopplacement['data']['placement_id'];
            $mediaAccountData['createtime'] = time();
            $mediaAccountData['adminid'] = $this->adminId;
            $mediaAccountData['status']=1;
        }
        else
        {
            ajaxReturn(0, 0, '暂未开通申请！');
        }
        $num = $this->MediaCutoverAccountModel->add($mediaAccountData);
        if($num)
        {
            ajaxReturn(0, 1, 0);
        }
        ajaxReturn('', 0, '申请失败，请刷新重试！');
    }

    /**
     * 添加切量规则
     * Enter description here ...
     */
    function ajaxcutoverruledoadd(){
        $postData = $this->input->post();
        $this->load->model('CutoverRuleModel');
        $data['appid'] = trim($postData['appid']);
        $data['adtype'] = trim($postData['adtype']);
        $data['ostype'] = trim($postData['ostype']);
        $data['createtime'] = time();
        $data['edittime'] = time();
        $data['userid'] = $this->adminInfo['admin_id'];
        $data['pids'] = trim($postData['channelids']);
        if(empty($data['appid']))
        {
            ajaxReturn('APPID不能为空，请刷新重试！', 0, "APPID不能为空，请刷新重试！");
        }
        if(empty($data['adtype']))
        {
            ajaxReturn('广告类型不能为空，请选择！', 0, '广告类型不能为空，请选择！');
        }
        if(empty($data['pids']))
        {
            ajaxReturn('渠道不能为空，请选择！', 0, '渠道不能为空，请选择！');
        }
        $row = $this->CutoverRuleModel->getRow(array('appid' => $data['appid'], 'adtype' => $data['adtype'], 'ostype' => $data['ostype']));
        if(empty($row))
        {
            $num = $this->CutoverRuleModel->add($data);
        }
        else
        {
            unset($data['createtime']);
            $num = $this->CutoverRuleModel->edit(array('appid' => $data['appid'], 'adtype' => $data['adtype'], 'ostype' => $data['ostype']), $data);
            $this->load->model('CutoverRuleLogModel');
            $this->CutoverRuleLogModel->add(array('appid' => $data['appid'], 'adtype' => $data['adtype'], 'ostype' => $data['ostype'], 'editlog' => json_encode($row), 'createtime' => time(), 'userid' => $this->adminInfo['admin_id']));
        }
        if($num)
        {
            $this->load->model('AppInfoModel');
            $this->AppInfoModel->save(array('appid'=>$data['appid']), array('cutoverstatus'=>2));
            ajaxReturn('', 1, '');
        }
        ajaxReturn('未知错误，请刷新重试！', 0, "未知错误，请刷新重试！");
    }
    
	/**
     * add feeds templates
     */
    function ajaxsavefeedstpl(){
    	$where['appid'] = $this->input->post('appid');
    	$feedstpl['feedsiostpl'] = $this->input->post('iostpl');
    	$feedstpl['feedsandroidtpl'] = $this->input->post('androidtpl');
    	$ostypeid = $this->input->post('ostypeid');
        $this->load->model('AppInfoModel');
    	if(empty($where['appid']))
        {
            ajaxReturn('', 0, '数据错误，请刷新重试！');
        }
	    if(empty($feedstpl['feedsiostpl']) && $ostypeid == 2)
	    {
	        ajaxReturn('', 0, 'IOS模板不能为空！');
	    }
	    if(empty($feedstpl['feedsandroidtpl']) && $ostypeid == 1)
	    {
	        ajaxReturn('', 0, 'Android模板不能为空！');
	    }
	    $appinfo = $this->AppInfoModel->getAppInfoRow($where['appid']);
    	if(empty($appinfo))
        {
            ajaxReturn('', 0, '数据错误，请刷新重试！');
        }
        elseif(($ostypeid == 2 && $appinfo['feedsiostpl'] == $feedstpl['feedsiostpl']) ||($ostypeid== 1 && $appinfo['feedsandroidtpl'] == $feedstpl['feedsandroidtpl'])) {
        	ajaxReturn(0, 1, 0);
        }
        $num = $this->AppInfoModel->update($where, $feedstpl);
        if($num)
        {
            ajaxReturn(0, 1, 0);
        }
        ajaxReturn('', 0, '数据更新错误');
    }
    
	/**
     * add media cutover account for Baidu
     */
    function ajaxsavebaiducotoveraccount(){
    	$subtype = $this->input->post('subtype');
        $baiduAcc['appid'] = $this->input->post('appid');
        $baiduAcc['mediacutoverid'] = $this->input->post('mediaiddsp');
    	$medianame = trim($this->input->post('medianame'));
    	$baiduAcc['mediumid'] = trim($this->input->post('mediumid'));
    	$baiduAcc['bannerplacementid'] = trim($this->input->post('bannerplacementid'));
    	$baiduAcc['pupopplacementid'] = trim($this->input->post('pupopplacementid'));
    	$baiduAcc['createtime']= time();
        $baiduAcc['adminid'] = $this->adminId;
        $baiduAcc['status']=1;
        $this->load->model('MediaCutoverManageModel');
        $this->load->model('MediaCutoverAccountModel');
        $this->load->model('AppInfoModel');
    	
    	if(empty($baiduAcc['appid']))
        {
            ajaxReturn('', 0, '请填写App ID');
        }
        if(empty($baiduAcc['mediacutoverid']) || $baiduAcc['mediacutoverid'] == 0)
        {
            ajaxReturn('', 0, '请选择申请账号的切量媒体！');
        }
        $appInfo = $this->AppInfoModel->getDetailInfo($baiduAcc['appid']);
        if(empty($appInfo))
        {
            ajaxReturn('', 0, 'App ID不存在');
        }
        $mediaInfo = $this->MediaCutoverManageModel->getRow(array('id'=>$baiduAcc['mediacutoverid']));
        if(empty($mediaInfo))
        {
            ajaxReturn('', 0, '目标系统不存在');
        }
    	$mediaAccountInfo = $this->MediaCutoverAccountModel->getRow(array('appid'=>$baiduAcc['appid'], 'mediacutoverid'=>$mediaInfo['id'],'status'=>1));
        if(!empty($mediaAccountInfo) && $subtype == "add")
        {
            ajaxReturn('', 0, '该切量媒体已申请账号');
        }
        if($mediaInfo['mediaport'] == 4004)
        {
		    if(empty($baiduAcc['mediumid']))
		    {
		        ajaxReturn('', 0, '媒体ID不能为空！');
		    }
		    if(empty($baiduAcc['bannerplacementid']) && empty($baiduAcc['pupopplacementid']))
		    {
		        ajaxReturn('', 0, '请至少填写一项广告位ID');
		    }
        }
        else
        {
            ajaxReturn('', 0, '暂未开通申请！');
        }
        if($subtype == "edit")
        {
        	$num = $this->MediaCutoverAccountModel->edit(array('appid' =>$baiduAcc['appid'], 'mediacutoverid'=>$baiduAcc['mediacutoverid']), $baiduAcc);
        }elseif ($subtype == "add")
        {
        	$num = $this->MediaCutoverAccountModel->add($baiduAcc);
        }
        if($num)
        {
            ajaxReturn(0, 1, 0);
        }
        ajaxReturn('', 0, '申请失败，请刷新重试！');
    }

    /**
     * 添加切量计划
     * Enter description here ...
     */
    function ajaxsetcutoverruleschedule(){
        $data = $this->input->post();
        $data['adminid'] = $this->adminInfo['admin_id'];
        $this->load->model('MediaRuleModel');
        $status = $this->MediaRuleModel->addData($data);
        ajaxReturn('', $status['status'], $status['data']);
    }

    /**
     * 编辑切量计划
     * Enter description here ...
     */
    function ajaxeditcutoverruleschedule(){
        $data = $this->input->post();
        $data['adminid'] = $this->adminInfo['admin_id'];
        $this->load->model('MediaRuleModel');
        $status = $this->MediaRuleModel->eidtData($data);
        if($status['status'] == 1)
        {
            ajaxReturn('', 1, $status['data']);
        }

        ajaxReturn('', 0, $status['data']);
    }


    function appinfoedit(){
        $appId = $this->input->get('appid');
        $this->load->model('AppInfoModel');
        $data = $this->AppInfoModel->getDetailInfo($appId);
        $data['apptype'] = $this->AppInfoModel->getAppType();
        $data['positionList'] = $this->CommonModel->table("app_position")->getList(array('appid'=>$appId, 'status' => 0));
        $this->assign('apptype', $data['apptype']);
        $this->assign('positionList', $data['positionList']);
        $this->assign('info', $data);
        $this->assign('data', $data);
        $this->assign('tab', 'view');
        $this->display('admin/app/appinfoedit.html');
    }

    function ajaxappinfoedit(){
        $postData = $this->input->post();
        $this->load->model('AppInfoModel');
        $appInfo = $this->AppInfoModel->doEdit($postData);
        if($appInfo['status'] == 0)
        {
            ajaxReturn($appInfo['info'], 0, $appInfo['data']);
        }
        ajaxReturn('', 1, array($appInfo['appid'], ''));
    }

    function ajaxappcutoverstatus(){
        $postData = $this->input->post();
        $this->load->model('AppInfoModel');
        $num = $this->AppInfoModel->save(array('appid'=>$postData['id']), array('cutoverstatus'=>$postData['key']));
        if($num <= 0)
        {
            ajaxReturn('未知错误，请刷新重试！', 0, '未知错误，请刷新重试！');
        }
        ajaxReturn('', 1, '');
    }

    function dodelapptag(){
        $where = array();
        $where['appid'] = $this->input->post('appid');
        $where['tagid'] = $this->input->post('tagid');
        if(!$where['appid'] || !$where['tagid']){
            ajaxReturn('参数错误', 0, '');
        }
        $upNum = $this->CommonModel->table("app_tag_relation")->delete($where);
        if($upNum <= 0){
            $appTagInfo = array('status' => 0, 'info' => 'deltagiderr', 'data' => '');
        }else{
            $appTagInfo = array('status' => 1);
        }
        if($appTagInfo['status'] == 0)
        {
            ajaxReturn($appTagInfo['info'], 0, $appTagInfo['data']);
        }
        ajaxReturn('', 1, '');
    }
    /**
     * 渠道开关控制
     */
    function channel(){
        $this->load->model('AdHourAppverModel');
        $appId = $this->input->get('appid');
        $this->getswitchstatus($appId);
        $this->assign('appid', $appId);
        //获取版本渠道
        $appverlist = $this->AdHourAppverModel->getappver($appId);
        $this->assign('appverlist', $appverlist);
        $this->assign('tab', 'channel');
        $this->display('admin/app/channel.html');
    }
    /**
     * 新版积分墙开关控制
     */
    function newwallswitch(){
        $this->load->model('AdHourAppverModel');
        $appId = $this->input->get('appid');
        $this->getswitchstatus($appId);
        $this->assign('appid', $appId);
        //获取版本渠道
        $appverlist = $this->AdHourAppverModel->getappverwall($appId);
        $this->assign('appverlist', $appverlist);
        $this->assign('tab', 'newwallswitch');
        $this->display('admin/app/newwallswitch.html');
    }
    /**
     * 获取开关状态
     */
    function getswitchstatus($appId=''){
        $this->load->model('AppInfoModel');
        $info = $this->AppInfoModel->getRow(array("appid"=>$appId));
        $this->assign('info', $info);
        $this->assign('data', $info);
    }
     
     /**
     * 获取某组边框
     */
    function getborder(){
        $bordergroupid= $this->input->post('bordergroupid');
    	$this->load->model('AdBorderModel');
    	$where=array('bordergroupid'=>$bordergroupid);
    	$BorderList = $this->AdBorderModel->getBorderInfo($where);
    	if($BorderList){
    		ajaxReturn('获取边框成功', 1, $BorderList);
    	}
    	else{
    		ajaxReturn('获取边框失败', 0);
    	}
    	
    }
    /**
     * 编辑开关状态
     */
    function editchannel(){
        $this->load->model('AdHourAppverModel');
        $appId  = $this->input->get('appid');
        $appver = $this->input->get('appver');
        $flg = $this->input->post('flg');
        if($flg == '1'){
            var_dump($this->input->post());
            $channel = $this->input->post('channel');
            $channelarr = implode(",", $channel);
            $where['id'] = array("in",$channelarr);

            $this->AdHourAppverModel->edit($where,array('verswitch'=>'0'));
            //其他未勾选的 更改未 关闭开关
            $this->load->model('AppChannelModel');
            $punchboxids = $this->AppChannelModel->getlist(array('appid'=>$appId),"publisherID");
            foreach ($punchboxids as $key=>$val){
                $pkPublisherIdInfo[] = $val['publisherID'];
            }
            $pkPublisherIdsarr = implode(",", $pkPublisherIdInfo);
            $where_other['pkPublisherId'] = array('in',$pkPublisherIdsarr);
            $where_other['id'] = array("not in",$channelarr);
            $where_other['pkAppVer'] = $appver;
            $this->AdHourAppverModel->edit($where_other,array('verswitch'=>'1'));
            redirect("/admin/app/channel?appid=".$appId);
            exit;
        }
        $this->getswitchstatus($appId);
        $appverlist = $this->AdHourAppverModel->getappverstatus($appId,$appver);
        $this->assign('appverlist', $appverlist);
        $this->assign('tab', 'channel');
        $this->assign('appver', $appver);
        $this->assign('appid', $appId);
        $this->display('admin/app/editchannel.html');
    }
    /**
     * 编辑开关状态
     */
    function editwallswitch(){
        $this->load->model('AdHourAppverModel');
        $appId  = $this->input->get('appid');
        $appver = $this->input->get('appver');
        $flg = $this->input->post('flg');
        if($flg == '1'){
            var_dump($this->input->post());
            $channel = $this->input->post('channel');
            $channelarr = implode(",", $channel);
            $where['id'] = array("in",$channelarr);

            $this->AdHourAppverModel->edit($where,array('verwallswitch'=>'1'));
            //其他未勾选的 更改未 关闭开关
            $this->load->model('AppChannelModel');
            $punchboxids = $this->AppChannelModel->getlist(array('appid'=>$appId),"publisherID");
            foreach ($punchboxids as $key=>$val){
                $pkPublisherIdInfo[] = $val['publisherID'];
            }
            $pkPublisherIdsarr = implode(",", $pkPublisherIdInfo);
            $where_other['pkPublisherId'] = array('in',$pkPublisherIdsarr);
            $where_other['id'] = array("not in",$channelarr);
            $where_other['pkAppVer'] = $appver;
            $this->AdHourAppverModel->edit($where_other,array('verwallswitch'=>'0'));
            redirect("/admin/app/newwallswitch?appid=".$appId);
            exit;
        }
        $this->getswitchstatus($appId);
        $appverlist = $this->AdHourAppverModel->getappverstatus($appId,$appver);
        $this->assign('appverlist', $appverlist);
        $this->assign('tab', 'newwallswitch');
        $this->assign('appver', $appver);
        $this->assign('appid', $appId);
        $this->display('admin/app/editwallswitch.html');
    }

    function saveappori(){
        $appid = $this->input->post('appid');
        $appori = (int)$this->input->post('appori');
        $this->load->model('AppInfoModel');
        $status = $this->AppInfoModel->edit(array("appid"=>$appid), array("appori"=>$appori, "isupdate"=>1, "uptime"=>time()));
        $msg = "应用屏幕方向修改失败";
        if($status){
            $msg = "应用屏幕方向修改成功";
        }
        ajaxReturn($msg, $status);
    }

    /**
     * 于Cocodate 与下载开关设置
     */
    function verlist(){
        $appId = "92B28FD4B2A11EB2CABCB1535C554200";
        $this->load->model('SdkversionLogModel');
        $sdk_list = $this->SdkversionLogModel->getList(array('type'=>'2'),'version',"id desc");
        foreach($sdk_list as $key=>$val){
            $sdk_list[$key]['key_name'] = $appId.'_'.$val['version'];
        }
        $this->assign('appverlist', $sdk_list);
        $this->assign('tab', 'cococonfig');
        $this->display('admin/app/verlist.html');
    }

    /**
     * 媒体点击效果约束
     */
    function mdclickeffect(){
        $appId = $this->input->get('appid');
        $adform = $this->input->get('adform');
        if(empty($appId))
        {
            redirect("/admin/app/applist");
            exit;
        }
        $data = $this->AppInfoModel->getDetailInfo($appId);
        if(empty($adform))
        {
            $adform = 1;
        }
        $this->load->model('AppClickEffectModel');
        $effecttype = $this->AppClickEffectModel->getOne(array('appid'=>$appId, 'adform'=>$adform),'effecttype');
        if(empty($effecttype))
        {
            $effecttype = 0;
        }
        $this->assign('effecttype', $effecttype);
        $this->assign('adform', $adform);
        $this->assign('appid', $appId);
        $this->assign('info', $data);
        $this->assign('data', $data);
        $this->assign('tab', 'mdclickeffect');
        $this->display('admin/app/mdclickeffect.html');
    }

    /**
     * 服务器对接开关
     */
    function stsswitch(){
        $appId = $this->input->get('appid');
        $adform = $this->input->get('adform');
        if(empty($appId))
        {
            redirect("/admin/app/applist");
            exit;
        }
        $data = $this->AppInfoModel->getDetailInfo($appId);
        if(empty($adform))
        {
            $adform = 1;
        }
        $s2sswitches=array();
        $s2sswitches=explode(",",$data['s2sswitches']);
        $this->assign('s2sswitches',$s2sswitches);
        $this->assign('adform', $adform);
        $this->assign('appid', $appId);
        $this->assign('info', $data);
        $this->assign('data', $data);
        $this->assign('tab', 'stsconfig');
        $this->display('admin/app/stsswitch.html');
    }

    /**
     *
     */
    function cococonfig(){
        $sdk_info = array();
        $sdk_info['appId'] = "92B28FD4B2A11EB2CABCB1535C554200";
        $sdk_info['appIdentifier'] = array("com.punchbox.ads","com.punchbox.punchboxservice");
        $sdk_info['ver'] = $this->input->get('ver');
        $sdk_info['key_name'] = $sdk_info['appId']."_".$sdk_info['ver'];
        $coco_url = "http://stats.cocounion.com/adconfig/".$sdk_info['appId']."/".$sdk_info['key_name'];
        $ret = curlGet($coco_url);
        $config_info = json_decode($ret);
        //var_dump($config_info->result->config->adconfig);
        $adconfig = array('banner_moregame'=>'1','button_moregame'=>'1','innerAd'=>'1','upload_applist'=>'false','predownload'=>'false','upload_applist_interval'=>'0','upload_applist_length'=>'0');
        if(!empty($config_info->result)){
            foreach($config_info->result->config->adconfig as $key=>$val){
                $name = $val->name;
                $adconfig[$name] = $val->value;
            }
        }

        $this->assign('tab', 'cococonfig');
        $this->assign('config_info', $config_info);
        $this->assign('sdk_info', $sdk_info);
        $this->assign('adconfig', $adconfig);
        $this->display('admin/app/cococonfig.html');
    }

    /**
     * 渠道开关信息
     */
    function getappinfo(){
        $appId = $this->input->get('appid');
        if(empty($appId))
        {
            redirect("/admin/app/applist");
            exit;
        }
        $data = $this->AppInfoModel->getDetailInfo($appId);
        $this->assign('appid', $appId);
        $this->assign('data', $data);
        $this->assign('info', $data);
        return $data;
    }
    
    /**
     * 打印app_info报表
     */
	function appInfoReport(){
        $appInfoForm = $this->AppInfoModel->getAppReport();
        $this->assign('appInfoForm', $appInfoForm);
        $filename =  "app_info.xls";
        header("Content-type:application/vnd.ms-excel;charset=UTF-8");
        header("Content-Disposition:attachment;filename=$filename");
        $this->display('admin/app/down_appInfoReport.html');
    }
    
    function editmediarulepriority(){
    	$priority = $this->input->post('priority');
    	$id = $this->input->post('id');
    	if(!is_numeric($priority)){
            ajaxReturn('请填写数字', 0);
        }
        if($priority<0)
        {
            ajaxReturn('数值不能小于0', 0);
        }
        $this->load->model("MediaRuleModel");
        $status = $this->MediaRuleModel->save(array('id' => $id), array('priority' => $priority));
    	if($status)
        {
        	ajaxReturn('', 1);
        }
        ajaxReturn('修改0条记录', 0);
    }
    
	/**
	 * wf
	 * 存储媒体dsp兜底切量设置
	 */
    function ajaxsaveappdspcutoverfloors(){
    	$data=$this->input->post();
    	$admin_info= $this->session->userdata('admin');
    	$data['adminid']=$admin_info['admin_id'];
    	$date= time();
    	$data['createtime']= $date;
    	$this->load->model('AppDspCutoverFloorsModel');
    	$flag=$this->AppDspCutoverFloorsModel->addData($data);
	    ajaxReturn($flag['msg'], $flag['status'], "");
    }
    
    /**
	 * wf
	 * 编辑媒体dsp兜底切量设置
	 */
 	function editappdspfloorspriority(){
    	$priority = $this->input->post('priority');
    	$id = $this->input->post('id');
    	if(!is_numeric($priority)){
            ajaxReturn('请填写数字', 0);
        }
        if($priority<0)
        {
            ajaxReturn('数值不能小于0', 0);
        }
        $admin_info= $this->session->userdata('admin');
    	$date= time();
    	$this->load->model("AppDspCutoverFloorsModel");
        $status = $this->AppDspCutoverFloorsModel->save(array('id' => $id), array('priority' => $priority,'edittime'=> $date,'adminid'=>$admin_info['admin_id']));
        if($status)
        {
        	ajaxReturn('', 1);
        }
        ajaxReturn('修改0条记录', 0);
    }
    
    /**
	 * wf
	 * 删除媒体dsp兜底切量设置
	 */
    function deleteappdsppriority()
    {
        $id = $this->input->get('id');
        if(empty($id))
        {
            ajaxReturn('', 0, '');
        }
        $this->load->model("AppDspCutoverFloorsModel");
        $num = $this->AppDspCutoverFloorsModel->save(array('id'=>$id), array('status' => 0));
        if($num == 0)
        {
            ajaxReturn('', 0, '');
        }
        ajaxReturn('', 1, '');
    }
    
    /**
	 * wf
	 * 编辑媒体dsp兜底开关
	 */
    function dspcutoverfloorsswitch(){
    	$appid=$this->input->post('appid');
    	$dspfloorsswitch=(int)$this->input->post('dspfloorsswitch');
    	$num = $this->AppInfoModel->save(array('appid'=>$appid),array('dspfloorsswitch'=>$dspfloorsswitch));
    	if($num == 0)
        {
            ajaxReturn('', 0, '');
        }
        ajaxReturn('', 1, '');
    }
    
    /*
     * wf
     * 保存应用支持广告分类设置
     * 20151109
     */
    function savesupportadtypeid(){
        $appid = $this->input->post('appid');
        $supportadtypeid = (int)$this->input->post('supportadtypeid');
        $this->load->model('AppInfoModel');
        $status = $this->AppInfoModel->edit(array("appid"=>$appid), array("supportadtypeid"=>$supportadtypeid, "isupdate"=>1, "uptime"=>time()));
        $msg = "应用支持广告分类设置失败";
        if($status){
            $msg = "应用支持广告分类设置成功";
        }
        ajaxReturn($msg, $status);
    }
    
    /*
     * wf
     * 激励型配置页面
     */
 	function incentiveconfig(){
       $appId = $this->input->get('appid');
        $this->load->model('AppInfoModel');
        $info = $this->AppInfoModel->getRow(array("appid"=>$appId));
        $this->assign('info', $info);
        $this->assign('data', $info);
        $this->assign('tab', 'incentiveconfig');
        $this->assign('appid', $appId);
        $this->display('admin/app/incentiveconfig.html');
    }
    
    /*
     * wf
     * 激励型配置开关保存
     */
 	function ajaxsaveincentiveswitch(){
    	$appid = $this->input->post('appid');
    	$switchkey=$this->input->post('switchkey');
        $switchval=$this->input->post('switchval');
   		$this->load->model("AppInfoModel");
        $num=$this->AppInfoModel->update(array("appid"=>$appid),array($switchkey=>$switchval));
        if($num)
        {
            ajaxReturn('', 1, '');
        }
        else
        {
            ajaxReturn('', 0, '');
        }
    }
    
    /**
     * 保存插屏关闭按钮误点击率
     */
    function ajaxsavecdisableratio(){
    	$appid = $this->input->post('appid');
    	$cdisableratio=$this->input->post('cdisableratio');
    	if(!preg_match("/^(\d{1,3})(\.\d{0,2})?$/", $cdisableratio)){
            ajaxReturn('插屏关闭按钮误点击率格式错误！保存失败', 0,'插屏关闭按钮误点击率格式错误！保存失败');
        }
        if(floatval($cdisableratio)>10 || floatval($cdisableratio)<0){
        	ajaxReturn('插屏关闭按钮误点击率范围为0-10！保存失败', 0,'插屏关闭按钮误点击率范围为0-10！保存失败');
        	//暂时设为0到10
        }
   		$this->load->model("AppInfoModel");
        $num=$this->AppInfoModel->update(array("appid"=>$appid),array('cdisableratio'=>$cdisableratio));
        if($num)
        {
            ajaxReturn('', 1, '');
        }
        else
        {
            ajaxReturn('', 0, '');
        }
    }


    function syncappinfochance(){
        $postData = $this->input->post();
        $appInfo = $this->AppInfoModel->doSyncappinfochance($postData);
        if($appInfo['status'] == 0)
        {
            ajaxReturn($appInfo['info'], 0, $appInfo['data']);
        }
        ajaxReturn('', 1, '');
    }
}
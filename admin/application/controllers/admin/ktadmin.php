<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Ktadmin extends MY_A_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->model ( "AdminUserModel" );
	}
	function node() {
	}
	/**
	 * 添加kt 渠道管理员
	 */
	function addktadmin($admin_id = '0') {
		$this->load->model ( 'AdminKtUserModel' );
		$info = $this->AdminKtUserModel->getRow ( array (
				"admin_id" => $admin_id 
		) );
		$this->assign ( 'info', $info );
		$this->display ( 'admin/auth/addktadmin.html' );
	}
	function ajaxktsave() {
		$this->load->model ( 'AdminKtUserModel' );
		$data = array ();
		$data ['admin_id'] = ( int ) $this->input->post ( 'admin_id' );
		$data ['admin_name'] = $this->input->post ( 'admin_name' );
		$data ['admin_password'] = $this->input->post ( 'admin_password' );
		$data ['admin_status'] = ( int ) $this->input->post ( 'admin_status' );
		$data ['channelid'] = $this->input->post ( 'channelid' );
		$data ['ioschannelid'] = $this->input->post ( 'ioschannelid' );
		if ($data ['admin_id']) { // 编辑允许修改密码
			if (! empty ( $data ['admin_password'] )) {
				$data ['admin_password'] = md5 ( $data ['admin_password'] );
			} else {
				unset ( $data ['admin_password'] );
			}
			$status = $this->AdminKtUserModel->edit ( array (
					"admin_id" => $data ['admin_id'] 
			), $data );
		} else {
			unset ( $data ['admin_id'] );
			$data ['admin_create_time'] = time ();
			$data ['admin_password'] = md5 ( $data ['admin_password'] );
			$status = $this->AdminKtUserModel->add ( $data );
		}
		if ($status) {
			ajaxReturn ( '保存成功！', 1 );
		}
		ajaxReturn ( '保存失败！', 0, 0 );
	}
	function ktadminlist() {
		$this->load->model ( 'AdminKtUserModel' );
		$list = $this->AdminKtUserModel->getList ( array (), '*', 'admin_id desc ' );
		$this->assign ( 'list', $list );
		$this->display ( 'admin/auth/ktadminlist.html' );
	}
	function ajaxdelktadmin() {
		$this->load->model ( 'AdminKtUserModel' );
		$admin_id = $this->input->post ( 'admin_id' );
		$status = $this->AdminKtUserModel->delete ( array (
				"admin_id" => array (
						"in",
						$admin_id 
				) 
		) );
		if ($status) {
			ajaxReturn ( '删除成功！', 1 );
		}
		ajaxReturn ( '删除失败！', 0 );
	}
	/**
	 * 渠道管理
	 * Enter description here .
	 *
	 * ..
	 */
	function channelmanage() {
		$this->load->model ( 'AppChannelModel' );
		$reason = $this->SystemConfigModel->getSystemConfig ( 'reason_for_apprejection' );
		$this->assign ( 'reason', $reason );
		$ostypeid = $this->input->get_post ( 'ostypeid' ) ? ( int ) $this->input->get_post ( 'ostypeid' ) : 1;
		$data = array (
				'ostypeid' => $ostypeid 
		);
		$data ['channelname'] = trim ( $this->input->get ( 'search' ) );
		$get_appid = $this->input->get ( 'appid' );
		
		if (! empty ( $get_appid )) {
			$data ['appid'] = $get_appid;
		}
		
		// 渠道类别搜索
		$channeltype = trim ( $this->input->get ( 'schanneltype' ) );
		$channeltypearr = $this->AppChannelModel->getChannelType ( $channeltype, $ostypeid );
		$channeltypes = array ();
		if (! empty ( $channeltypearr )) {
			foreach ( $channeltypearr as $key => $value ) {
				$channeltypes [$key] = $value ['channelid'];
			}
		}
		$this->assign ( 'data', $data );
		$this->assign ( 'schanneltype', $channeltype );
		$channellist = $this->AppChannelModel->getChannelSearchList ( $ostypeid, $data ['channelname'], 0, $channeltypes );
		$parentchannellist = $this->AppChannelModel->getParentChannelList ( $ostypeid, 0 );
		$this->assign ( 'channellist', $channellist );
		$this->assign ( 'parentchannellist', $parentchannellist );
		$this->display ( 'admin/service/ktchannelmanage.html' );
	}
	
	/**
	 * CCplay app channel switcher Page
	 *
	 * @author yangsen
	 * @since 20141027
	 */
	function appchannelswitcher() {
		$this->load->model ( 'AppChannelModel' );
		$appid = $this->input->get ( 'appid' );
		
		if (empty ( $appid )) {
			show_error ( "参数错误", 500, "没有指定APPID参数" );
		}
		$where ['appid'] = $appid;
		$this->load->model ( 'AppInfoModel' );
		$appinfo = $this->AppInfoModel->getRow ( $where );
		$this->assign ( 'appinfo', $appinfo );
		$result = $this->AppChannelModel->getChannelByCond ( $where );

		$this->load->model('AppChannelConfigModel');
		$cond= array('appid'=>$appid,'configname'=>'ccplay_autopop','configvalue'=>'1');
		 
		$autopopPids = $this->AppChannelConfigModel->getCol($cond,'publisherID');
		if($autopopPids!=null ) {
		    if(in_array('0', $autopopPids)) {
		        $this->assign('app_autopop', 1);
		    } else {
		        $this->assign('app_autopop', 0);
		    }
		    foreach ($result as &$item) {
    		    
		        if(in_array($item['publisherID'],$autopopPids)) {
		            $item['autopop']=1;
		        } else {
		            $item['autopop']=0;
		        }
		    }
		}else {
		    $this->assign('app_autopop', 0);
		}
		
		$this->assign ( 'appid', $appid );
		$this->assign ( 'result', $result );
		$allchannelids = array ();
		
		foreach ( $result as $v ) {
			array_push ( $allchannelids, $v ['publisherID'] );
		}
		
	    //print_r($allchannelids);
		
		$allchannelids = array_uniq_filter_zero ( $allchannelids );
		$this->assign ( 'appid', $appid );
		$this->assign ( 'allpids', implode ( ',', $allchannelids ) );
		$this->display ( 'admin/ccplay/switcher.html' );
	}
	
	/**
	 * app 多渠道选择设置ccplay相关开关action
	 *
	 * @author yangsen
	 */
	function ccplayConfigureOnChannel() {
		
		// #sel_ccplay,#sel_ccplay_announce,#sel_ccplay_gift,#sel_ccplay_gonglue,#sel_ccplay_kefu
		$appid = $this->input->post ( 'appid' );
		if (empty ( $appid )) {
			return ajaxReturn ( "参数有误", 1 );
		}
		
		$fill_pids = $this->input->post ( 'fill_pids' );
		$fill_pids = explode ( ',', $fill_pids );
		
		$update_ccplay = $this->input->post ( 'ccplay' );
		//$update_ccplay = array_uniq_filter_zero ( $update_ccplay );
		
		$update_ccplay_announce = $this->input->post ( 'ccplay_announce' );
		//$update_ccplay_announce = array_uniq_filter_zero ( $update_ccplay_announce );
		
		$update_ccplay_gift = $this->input->post ( 'ccplay_gift' );
		//$update_ccplay_gift = array_uniq_filter_zero ( $update_ccplay_gift );
		
		$update_ccplay_gonglue = $this->input->post ( 'ccplay_gonglue' );
		//$update_ccplay_gonglue = array_uniq_filter_zero ( $update_ccplay_gonglue );
		
		$update_ccplay_kefu = $this->input->post ( 'ccplay_kefu' );
		//$update_ccplay_kefu = array_uniq_filter_zero ( $update_ccplay_kefu );
		
		//$update_ccplay_xapp = $this->input->post("ccplay_on_xapp");
		//$update_ccplay_xapp = array_uniq_filter_zero ( $update_ccplay_xapp );
		
		$update_ccplay_autopop = $this->input->post('ccplay_autopop');
		//$update_ccplay_autopop = array_uniq_filter_zero ( $update_ccplay_autopop );
		
		$this->load->Model ( 'AppChannelModel' );
		$appid = $this->input->post ( 'appid' );
		$where ['appid'] = $appid;
		
		// echo $this->AppChannelModel->getWhere($where);
		// #ccplay
		$this->multiUpdateChannelValSwitch ( $update_ccplay, $appid, $fill_pids, 'is_ccplay', $where );
		
		// ccplay announce
		$this->multiUpdateChannelValSwitch ( $update_ccplay_announce, $appid, $fill_pids, 'is_ccplay_announce', $where );
		
		// ccplay_xapp
		//$this->multiUpdateChannelValSwitch ( $update_ccplay_xapp, $appid, $fill_pids, 'is_ccplay_xapp', $where );
		
		// ccplay gift
		$this->multiUpdateChannelValSwitch ( $update_ccplay_gift, $appid, $fill_pids, 'is_ccplay_gift', $where );
		
		// ccplay kefu
		$this->multiUpdateChannelValSwitch ( $update_ccplay_kefu, $appid, $fill_pids, 'is_ccplay_kefu', $where );
		
		// ccplay gonglue
		$this->multiUpdateChannelValSwitch ( $update_ccplay_gonglue, $appid, $fill_pids, 'is_ccplay_gonglue', $where );
		$this->load->model('AppChannelConfigModel');
		
		
		if(!empty($update_ccplay_autopop)) {
            //autopop处理
            foreach($update_ccplay_autopop as $_predaterow ) {
                $this->AppChannelConfigModel->add(array('appid'=>$appid,'publisherID'=>$_predaterow,'configname'=>'ccplay_autopop','configvalue'=>1),true);
            }
		} else {
		    $update_ccplay_autopop = array();
		}
        
        
		$unselectedautopop = array_diff($fill_pids, $update_ccplay_autopop);
		foreach($unselectedautopop as $_predaterow) {
    		$this->AppChannelConfigModel->add(array('appid'=>$appid,'publisherID'=>$_predaterow,'configname'=>'ccplay_autopop','configvalue'=>0),true);
		}
		
		$this->load->model('ccplay/CcplayBiz');
		$this->CcplayBiz->flushCCplayPIDFileByAppId($appid);
        
		return ajaxReturn ( "更新成功", 200, array (
				'appid',
				$appid 
		));
		
		// print_r($update_ccplay);
		// print_r($update_ccplay_gift);
		// print_r($update_ccplay_gonglue);
		// print_r($update_ccplay_kefu);
	}
	
	/**
	 * 设置ccplay相关db相关字段多选html控件参数
	 *
	 * @param array $channelids        	
	 * @param integer $appid        	
	 * @param array $fillchannel        	
	 * @param string $field        	
	 * @param array $where
	 *        	其他条件
	 * @param
	 *        	number 开启标示符
	 * @param number $close_val        	
	 */
	private function multiUpdateChannelValSwitch($pids, $appid, $fillchannel, $field, $where = array(), $openval = 1, $close_val = 0) {
		if (! empty ( $pids )) {
			$where_array = array_merge ( $where, array (
					'appid' => $appid 
			), array (
					'publisherID' => array (
							"IN",
							implode ( ',', $pids ) 
					) 
			) );
			// echo $this->AppChannelModel->getWhere($where_array);
			$this->AppChannelModel->edit ( $where_array, array (
					$field => $openval 
			) );
		} else {
		    $pids = array();
		}
		
		$pids = array_diff ( $fillchannel, $pids );
		
		$where_array ['publisherID'] = array (
				"IN",
				implode ( ',', $pids ) 
		);
		if (! empty ( $pids )) {
			$this->AppChannelModel->edit ( $where_array, array (
					$field => $close_val 
			) );
		}
	}
	
}

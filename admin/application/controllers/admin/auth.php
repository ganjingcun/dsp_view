<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends MY_A_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model("AdminUserModel");
	}

	function node(){

	}

	/**
	 * 管理员账号列表
	 */
	function adminlist(){
		$list = array();
		$list = $this->AdminUserModel->getList(array(),'*', 'admin_id desc ');
		$this->assign('list', $list);
		$this->load->model('AdminGroupModel');
		$group = $this->AdminGroupModel->getGroup(array());
		$this->assign('group', $group);
		$this->display('admin/auth/adminlist.html');
	}
	/**
	 * 用户账号列表
	 */
	function userlist(){
		$search = $this->input->get_post('search');
        $this->assign('search', $search);
        $p=trim($this->input->get('per_page'));
        if(!$p){
            $p=1;
        }
        $where = array();
        if($search){
            $where['username'] = array (
                        'LIKE',
                        "%" . $search . "%" 
                );
        }
        $usergroupid = $this->input->get_post('usergroupid');
        $this->assign('usergroupid', $usergroupid);
        if(!empty($usergroupid)){
            $where['usergroupid'] = array (
                        'LIKE',
                        "%" . $usergroupid . "%" 
                );
        }
        $config['per_page'] = 20;
		$this->load->model('UserMemberModel');
		$this->load->model('UserGroupPrivilegeModel');
        $count = $this->UserMemberModel->getCount();
        $list = $this->UserMemberModel->getList($where,"*","userid DESC", "", $p,$config['per_page']);
        $this->load->library('page');
        $current_page =  $p==0 ? 1 : $p;
        $page=$this->page->page_show(base_url().'/admin/auth/userlist?',$count,$config['per_page']);
        $this->assign('list', $list);
        $this->smarty->assign('page', $page);
        $this->smarty->assign('per_page', $config['per_page']);
        $this->smarty->assign('count', $count);
        $group = $this->UserGroupPrivilegeModel->getGroup(array());
		$this->assign('group', $group);
        $this->display('admin/auth/userlist.html');
	}
	
	
	function ajaxDel(){
		$admin_id = $this->input->post('admin_id');
		$status = $this->AdminUserModel->delete(array("admin_id"=>array("in",$admin_id)));
		if($status){
			ajaxReturn('删除成功！', 1);
		}
		ajaxReturn('删除失败！', 0);
	}

	function edit($admin_id = 0){
		if($admin_id < 1){
			exit('链接参数错误');
		}
		$info = $this->AdminUserModel->getRow(array("admin_id"=>$admin_id));
		$this->assign('info', $info);
		$this->load->model('AdminGroupModel');
		$group = $this->AdminGroupModel->getList(array());
		$this->assign('group', $group);
		$this->display("admin/auth/edit.html");
	}

	/**
	 * 用户编辑页面跳转
	 */
	function useredit($userid = 0){
		if($userid < 1){
			exit('链接参数错误');
		}
		$this->load->model('UserMemberModel');
		$this->load->model('UserGroupPrivilegeModel');
		$info = $this->UserMemberModel->getRow(array("userid"=>$userid));
		$this->assign('info', $info);
		$this->load->model('UserGroupPrivilegeModel');
		$group = $this->UserGroupPrivilegeModel->getList(array());
		$this->assign('group', $group);
		$this->display("admin/auth/useredit.html");
	}
	
	function ajaxSave(){
		$data = array();
		$data['admin_id'] = (int)$this->input->post('admin_id');
		$data['admin_name'] = $this->input->post('admin_name');
		$data['admin_password'] = $this->input->post('admin_password');
		$data['group_id'] = (int)$this->input->post('group_id');
		$data['admin_status'] = (int) $this->input->post('admin_status');
		if($data['admin_id']){//编辑允许修改密码
			if(!empty($data['admin_password'])){
				$data['admin_password'] = md5($data['admin_password']);
			}else{
				unset($data['admin_password']);
			}
			$status = $this->AdminUserModel->edit(array("admin_id"=>$data['admin_id']), $data);
		}else{
			unset($data['admin_id']);
			$data['admin_create_time'] = time();
			$data['admin_password'] = md5($data['admin_password']);
			$status = $this->AdminUserModel->add($data);
		}
		if($status){
			ajaxReturn('保存成功！', 1);
		}
		ajaxReturn('保存失败！', 0, 0);
	}
	
	/**
	 * 保存用户组信息
	 */
	function ajaxUserSave(){
		$data = array();
		$data['userid'] = (int)$this->input->post('userid');
		$data['usergroupid'] = (int)$this->input->post('usergroupid');
		$this->load->model('UserMemberModel');
		if($data['userid']){
			$status = $this->UserMemberModel->edit(array("userid"=>$data['userid']), $data);
		}
		if($status){
			ajaxReturn('保存成功！', 1);
		}
		ajaxReturn('保存失败！', 0, 0);
	}
	
	/**
	 * 为所选用户统一分配组
	 */
	function distributegroup(){
		$data = array();
		$useridstring = $this->input->post('userids');
		$data['usergroupid'] = (int)$this->input->post('usergroupid');
		$userid=array();
		$userid=explode(",", $useridstring);
		$this->load->model('UserMemberModel');
		if($userid){
			foreach ($userid as $value) {
				if($value){
					$status = $this->UserMemberModel->edit(array("userid"=>$value), $data);
					if(!$status){
						ajaxReturn('保存失败！', 0, 0);
					}
				}
			}
		}
		ajaxReturn('保存成功！', 1);
	}
	
	function permission(){
	    $this->load->model('AdminNodeModel');
	    $permission = $this->AdminNodeModel->getList(array('pid'=>0));
	    $this->assign('permission', $permission);
	    $group = $this->AdminGroupModel->getList(array());
	    $this->assign('group', $group);
	    
	    $this->load->model('UserPrivilegeModel');
	    $userpermission=$this->UserPrivilegeModel->getList();
	    if(isset($userpermission)){
	    	$this->assign('userpermission',$userpermission);
	    }
	    $this->load->model('UserGroupPrivilegeModel');
	    $usergroup=$this->UserGroupPrivilegeModel->getList();
		if(isset($usergroup)){
			$this->assign('usergroup', $usergroup);
	    }
		$this->display('admin/auth/permission.html');
	}

	function addAdmin(){
		$this->load->model('AdminGroupModel');
		$group = $this->AdminGroupModel->getList(array());
		$this->assign('group', $group);
		$this->display('admin/auth/edit.html');
	}
	
	function ajaxSavePermission(){
	    $data = array();
	    $data['group_id'] = (int)$this->input->post('group_id');
	    $data['group_name'] = strip_tags($this->input->post('group_name'));
	    $data['group_privilege'] = strip_tags(trim($this->input->post('permission'),','));
	    $data['uptime'] = time();
	    if(empty($data['group_name'])){
	        ajaxReturn('权限组名称不能为空', 0, 0);
	    }
	    $group_id = $data['group_id'];
        unset($data['group_id']);
	    if($group_id){
    	    $id = $this->AdminGroupModel->getOne(array("group_name"=>$group_id),'group_id');
            if($id){
                ajaxReturn('已经存在同名的权限组', 0, 0);
            }
	        $status = $this->AdminGroupModel->edit(array("group_id"=>$group_id),$data);
	    }else{
	        $status = $this->AdminGroupModel->add($data);
	    }
	    if($status){
            ajaxReturn('权限组保存成功！', 1);
        }
        ajaxReturn('权限组保存失败！', 0, 0);
	}
	
	function ajaxGetPermission(){
	    $data = array();
	    $data['group_id'] = (int)$this->input->post('id');
	    if(empty($data['group_id'])){
	        ajaxReturn('参数错误！', 0, 0);
	    }
	    $info = $this->AdminGroupModel->getRow($data);
	    if(empty($info)){
	        ajaxReturn('该权限组不存在！', 0, 0);
	    }
	    $list = $this->AdminNodeModel->getList(array('id'=>array("in", trim($info['group_privilege'],','))), 'id');
	    ajaxReturn('ok', 1, $list);
	}
	
	/**
	 * 用户权限组保存
	 */
	function ajaxSaveUserPermission(){
	    $userdata = array();
	    $userdata['groupid'] = (int)$this->input->post('groupid');
	    $userdata['groupname'] = strip_tags($this->input->post('groupname'));
	    $userdata['groupprivilege'] = strip_tags(trim($this->input->post('groupprivilege'),','));
	    $userdata['updatetime'] = time();
	    if(empty($userdata['groupname'])){
	        ajaxReturn('权限组名称不能为空', 0, 0);
	    }
	    $groupid = $userdata['groupid'];
        unset($userdata['groupid']);
        $this->load->model('UserGroupPrivilegeModel');
	    if($groupid){
    	    $status = $this->UserGroupPrivilegeModel->edit(array('groupid'=>$groupid),$userdata);
	    }else{
	    	$id = $this->UserGroupPrivilegeModel->getOne(array('groupname'=>$userdata['groupname']),'groupid');
            if($id){
                ajaxReturn('已经存在同名的权限组', 0, 0);
            }
            else{
	        	$status = $this->UserGroupPrivilegeModel->add($userdata);
            }
	    }
	    if($status){
            ajaxReturn('权限组保存成功！', 1);
        }
        ajaxReturn('权限组保存失败！', 0, 0);
	}
	
	
	/**
	 * 获取用户权限(开发者、广告主)
	 */
	function ajaxGetUserPermission(){
	    $data = array();
	    $data['groupid'] = (int)$this->input->post('id');
	    if(empty($data['groupid'])){
	        ajaxReturn('参数错误！', 0, 0);
	    }
	    $this->load->model('UserGroupPrivilegeModel');
	    $this->load->model('UserPrivilegeModel');
	    $info = $this->UserGroupPrivilegeModel->getRow($data);
	    if(empty($info)){
	        ajaxReturn('该权限组不存在！', 0, 0);
	    }
	    $list = $this->UserPrivilegeModel->getList(array('privilegeid'=>array("in", trim($info['groupprivilege'],','))), 'privilegeid');
	    ajaxReturn('ok', 1, $list);
	}
        /**
         * 添加kt 渠道管理员
         */
        function addktadmin($admin_id='0'){
                $this->load->model('AdminKtUserModel');
                $info = $this->AdminKtUserModel->getRow(array("admin_id"=>$admin_id));
		$this->assign('info', $info);
 		$this->display('admin/auth/addktadmin.html');
	}
        function ajaxktsave(){
                $this->load->model('AdminKtUserModel');
                $data = array();
		$data['admin_id'] = (int)$this->input->post('admin_id');
		$data['admin_name'] = $this->input->post('admin_name');
		$data['admin_password'] = $this->input->post('admin_password');
 		$data['admin_status'] = (int) $this->input->post('admin_status');
                $data['channelid'] = $this->input->post('channelid');
                $data['ioschannelid'] = $this->input->post('ioschannelid');
		if($data['admin_id']){//编辑允许修改密码
			if(!empty($data['admin_password'])){
				$data['admin_password'] = md5($data['admin_password']);
			}else{
				unset($data['admin_password']);
			}
			$status = $this->AdminKtUserModel->edit(array("admin_id"=>$data['admin_id']), $data);
		}else{
			unset($data['admin_id']);
			$data['admin_create_time'] = time();
			$data['admin_password'] = md5($data['admin_password']);
			$status = $this->AdminKtUserModel->add($data);
		}
		if($status){
			ajaxReturn('保存成功！', 1);
		}
		ajaxReturn('保存失败！', 0, 0);
        }
        function ktadminlist(){
                $this->load->model('AdminKtUserModel');
 		$list = $this->AdminKtUserModel->getList(array(),'*', 'admin_id desc ');
		$this->assign('list', $list);
 		$this->display('admin/auth/ktadminlist.html');
        }
        function ajaxdelktadmin(){
                $this->load->model('AdminKtUserModel');
		$admin_id = $this->input->post('admin_id');
		$status = $this->AdminKtUserModel->delete(array("admin_id"=>array("in",$admin_id)));
		if($status){
			ajaxReturn('删除成功！', 1);
		}
		ajaxReturn('删除失败！', 0);
	}
}

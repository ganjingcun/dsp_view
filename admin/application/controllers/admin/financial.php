<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Financial extends MY_A_Controller
{
    private $accountmarkname = array('0'=>"普通",'1'=>"代扣税",'2'=>"触控内部",'3'=>"触控内部");
    private $settletypename = array('0'=>"普通",'1'=>"预付款");
    private $accTypeName = array('0'=>'CPC', '1'=>"CPC",'2'=>"CPM",'3'=>"CPA",'4'=>"未知",'5'=>"未知");
    function __construct(){
        parent::__construct();
        error_reporting(0);
        $this->load->model('UserMemberModel');
        $this->load->model('AdIncomePaidModel');
        $this->load->model('AdIncomeModel');
        $this->load->model('AppInfoModel');
    }

    function charged(){
        $search = $this->input->get_post('search');
        $this->assign('search', $search);
        $this->accounttypearr = array('0'=>"个人/团体",'1'=>"个人/团体",'2'=>"企业",'3'=>"个体工商");
        $this->acctypearr = array('1'=>"CPC",'2'=>"CPM",'3'=>"CPA");
        $this->display('admin/financial/charged.html');
    }

    function ajaxSearchAccount(){
        $username = $this->input->post("search");
        if($username == ''){
            ajaxReturn('搜索用户名不能为空！', 0, 0);
        }
        $username = str_replace("_","\_", $username);
        $username = str_replace("%","\%", $username);
        $list = $this->UserMemberModel->getList(array('username'=>array('like', '%'.$username.'%')));
        if($list){
            foreach($list as $key=>$val){
                $list[$key]['totalcost'] = '￥'.number_format($val['totalcost']/AMOUNT_RATIO, 2, '.', '');
                $list[$key]['totalmoney'] = '￥'.number_format($val['totalmoney']/AMOUNT_RATIO, 2, '.', '');
            }
            ajaxReturn('ok', 1, $list);
        }
        ajaxReturn('找不到该用户', 0, 0);
    }

    function ajaxSaveMoney(){
        $data = array();
        $data['money'] = $this->input->post('money');
        $data['addtime'] = time();
        $user = $this->session->userdata("admin");
        $data['admin_id'] = $user['admin_id'];
        $data['memo'] = $this->input->post('memo');
        $data['userid'] = $this->input->post('userid');
        $password = $this->input->post('password');
        if($password != 'lks#df6Gd'){
            ajaxReturn('授权密码错误！', 0, 0);
        }
        if($data['userid'] < 1){
            ajaxReturn('参数错误', 0, 0);
        }
        $data['money'] = $data['money'] * AMOUNT_RATIO;
        $this->load->model('UserChargedModel');
        $userid = $this->UserMemberModel->getOne(array("userid"=>$data['userid']),"userid");
        if(!$userid) ajaxReturn('无法找到该用户', 0, 0);
        $status = $this->UserChargedModel->add($data);
        if($status){
            $status = $this->UserMemberModel->query("update user_member set totalmoney=totalmoney+{$data['money']}, isupdate=isupdate+1 where userid = {$data['userid']}");
        }
        if($status){
            ajaxReturn('充值成功', 1, 0);
        }
        ajaxReturn('充值失败', 0, 2);
    }

    function ajaxGetChargedList(){
        $userid = $this->input->post('id');
        if($userid<1){
            ajaxReturn('参数错误', 0, 0);
        }
        $this->load->model('UserChargedModel');
        $list = $this->UserChargedModel->getList(array('userid'=>$userid),'*','addtime desc');
        foreach($list as $key=>$val){
            $list[$key]['addtime'] = date('Y-m-d H:i:s', $val['addtime']);
            if ($val['money'] > 0) $list[$key]['money'] = number_format($val['money'] / AMOUNT_RATIO, 2, '.', '');
        }
        ajaxReturn('ok', 1, $list);
    }
    /**
     * 结算列表
     */
    function incomelist(){

        $paidmonth = $this->input->get_post('paidmonth')?$this->input->get_post('paidmonth'):$this->get_last_month('0');
        $username = $this->input->get_post('username');
        $months = $this->get_paid_month();
        $accounttypearr = array('0'=>"个人/团体",'1'=>"个人/团体",'2'=>"企业",'3'=>"个体工商");
        $where['paidmonth'] = $paidmonth?$paidmonth:$this->get_last_month('0');
        $var = "";
        if(!empty($paidmonth)){
            $var .= 'paidmonth='.$paidmonth;
        }
        if(!empty($username)){
            $username = str_replace("_","\_", $username);
            $username = str_replace("%","\%", $username);
            $where['username']  = array('like', '%'.$username.'%');
            $var .= 'username='.$username;
        }

        $p=trim($this->input->get('per_page'));
        if(!$p){
            $p=1;
        }
        $this->AdIncomePaidModel->setStrWhere($where);
        $config['per_page'] = 20;
        $count = $this->AdIncomePaidModel->getCount();
        $list = $this->AdIncomePaidModel->getList($where,"*","id desc", "", $p,$config['per_page']);

        foreach($list as $key=>$val){
            $list[$key]['accounttypename'] = $accounttypearr[$val['accounttype']];
            $list[$key]['userratio_arr'] = json_decode($val['userratio']);

            $userid = $val['userid'];
            $account_info = $this->get_user_account($userid);
            $list[$key]['accounttypename'] = $account_info['accounttypename'];
            $list[$key]['realname'] = $account_info['realname'];
            $list[$key]['idnumber'] = $account_info['idnumber'];
            $list[$key]['bankusername'] = $account_info['bankusername'];
            $list[$key]['bankaccount'] = $account_info['bankaccount'];
            $list[$key]['companyname'] = $account_info['companyname'];
            $list[$key]['bank'] = $account_info['bank'];
            $list[$key]['bankaddress'] = $account_info['bankaddress'];
        }
        $this->load->library('page');

        $page=$this->page->page_show('/admin/financial/incomelist?'.$var,$count,$config['per_page']);

        $this->assign('page', $page);
        $this->assign('per_page', $config['per_page']);
        $this->assign('count', $count);
        // var_dump($months);

        $this->assign('months', $months);
        $this->assign('paidmonth', $paidmonth);
        $this->assign('list', $list);
        $this->assign('username', $username);
        $this->display('admin/financial/incomelist.html');
    }
    /**
     * 用户账号认证信息
     */
    function get_user_account($userid=''){
        $account_where = array('userid'=>$userid,'status'=>1,'auditstatus'=>1);
        $this->load->model('AdAccountinfoModel');
        $accountinfo = $this->AdAccountinfoModel->getRow($account_where,'*','createtime DESC');
        $account_type = $accountinfo['accounttype'];
        $accounttypearr = array('0'=>"个人/团体",'1'=>"个人/团体",'2'=>"企业",'3'=>"个体工商");
        if(!empty($accountinfo))  $accountinfo['accounttypename'] =  $accounttypearr[$account_type];
        return $accountinfo;
    }
    /**
     * 获取月份
     */
    function get_last_month($month_num=1){
        $last_month = date("Y-m",mktime(0, 0 , 0,date("m")-$month_num,1,date("Y")));
        return $last_month;
    }
    /**
     * 获取所有结算的月份
     */
    function get_paid_month(){
        $months = $this->AdIncomePaidModel->getList(array(),"DISTINCT paidmonth","paidmonth DESC");
        return $months;
    }
    /**
     * 财务报表下载
     */
    function down_income(){
        $months = $this->get_paid_month();
        $this->assign('months', $months);
        $this->display('admin/financial/income_report.html');
    }
    function down_paid_report(){
        $month = $this->input->get('month');
        $accounttypearr = array('0'=>"个人/团体",'1'=>"个人/团体",'2'=>"企业",'3'=>"个体工商");
        $list = $this->AdIncomePaidModel->getList(array('paidmonth'=>$month),"*","id DESC");
        foreach($list as $key=>$val){
            $list[$key]['accounttypename'] = $accounttypearr[$val['accounttype']];
            $list[$key]['userratio_arr'] = json_decode($val['userratio']);
            $userid = $val['userid'];
            $account_info = $this->get_user_account($userid);
            $list[$key]['accounttypename'] = $account_info['accounttypename'];
            $users = $this->UserMemberModel->getRow(array('userid'=>$userid));
            $list[$key]['appdetail'] = $this->getAppDetail($userid, $val['cashmonth']);
            $list[$key]['accountmarkname'] = $this->accountmarkname[$users['accountmark']];
            $list[$key]['settletypename'] = $this->settletypename[$users['settletype']];
            $list[$key]['realname'] = $account_info['realname'];
            $list[$key]['idnumber'] = $account_info['idnumber'];
            $list[$key]['bankusername'] = $account_info['bankusername'];
            $list[$key]['bankaccount'] = $account_info['bankaccount'];
            $list[$key]['companyname'] = $account_info['companyname'];
            $list[$key]['bank'] = $account_info['bank'];
            $list[$key]['bankaddress'] = $account_info['bankaddress'];
        }
        $this->assign('list', $list);
        $filename =  $month."提款报表.xls";
        header("Content-type:application/vnd.ms-excel;charset=UTF-8");
        header("Content-Disposition:attachment;filename=$filename");
        $this->display('admin/financial/down_paid_report.html');
    }

    /**
     * 获取用户应用收入详细信息
     */
    function getAppDetail($userid, $month)
    {
        $applist = $this->AppInfoModel->getList(array('userid' => $userid));
        if(empty($applist))
        {
            return array();
        }
        $appids = array();
        $appinfos = array();
        foreach($applist as $v)
        {
            $appids[] = $v['appid'];
            $appinfos[$v['appid']] = $v;
        }
        $monthlist = explode(',', $month);
        $reincomelist = array();
        foreach($monthlist as $mv)
        {
            $incomelist = $this->AdIncomeModel->getList(array('pkCanalId' => array('in', $appids), 'pkMonth' => $mv));
            if(empty($incomelist))
            {
                continue;
            }
            foreach($incomelist as $k => $v)
            {
                $incomelist[$k]['appname'] = isset($appinfos[$v['pkCanalId']]) ? $appinfos[$v['pkCanalId']]['appname'] : '未知';
                $incomelist[$k]['accTypeName'] = $this->accTypeName[$v['accType']];
                $incomelist[$k]['avgincome'] = $v['cincome']/$v['uidClick'];
                $incomelist[$k]['appid'] = $v['pkCanalId'];
                $reincomelist[] = $incomelist[$k];
            }
        }
        return $incomelist;
    }

    /**
     * 确认付款
     */
    function confirm_paid(){
        $id = $this->input->get('id');
        $where['id'] = $id;
        $id = $this->AdIncomePaidModel->edit($where,array('status'=>1));
        if($id){
            $this->message->msg('确认支付成功','/admin/financial/incomelist');
            return;
        }  else {
            $this->message->msg('确认支付失败','/admin/financial/incomelist');
            return;
        }
    }

}
<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

define('ANZHUOSHANGDIAN_IMG_HOST', 'http://imgs.anzhuoshangdian.com/');
class Moregame extends MY_A_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("AdGroupModel");
        $this->load->model("AdCampaignModel");
        $this->load->model("AdStuffModel");
        $this->load->model('AdMgcateModel');
        $this->load->model('AppInfoModel');
        $this->load->model('AdMgcateRelationModel');
        $this->load->model('AdMgApplyModel');
        $this->load->model('AdMgInfoModel');
    }
    /**
     * 一级栏目
     */
    function cate(){
        $page = trim($this->input->get('per_page'));
         
        $type = (int)trim($this->input->get('type'))?(int)trim($this->input->get('type')):'1';
        $ostype = trim($this->input->get('ostype'))?(int)trim($this->input->get('ostype')):'2';
        $parent_cate = '';
        $condition = array();
        $condition['othercondition'] = 'type = '.$type;
        $condition['othercondition'] .= ' and status = 1';
        $condition['othercondition'] .= " and ostype = {$ostype}";
        $this->AdMgcateModel->setStrWhere($condition);
        $count = $this->AdMgcateModel->getCount();
        $this->smarty->assign('count', $count);

        $this->load->library('page');
        $config['per_page'] = 20;

        $current_page = $page == 0 ? 1 : $page;
        $pagelink = $this->page->page_show(base_url() . '/admin/moregame/cate', $count, $config['per_page']);
        $catlist = $this->AdMgcateModel->getList($condition, "*", " appid asc,sortid asc,id desc ", "", $current_page, $config['per_page']);
        foreach($catlist as $key=>$val){
            $catlist[$key]['appname'] = $this->AppInfoModel->getOne(array("appid"=>$val['appid']), "appname");
        }
        $this->pagination->create_links();
        $this->smarty->assign('catlist', $catlist);
        $this->smarty->assign('pagelink', $pagelink);
        $this->smarty->assign('tplname', $this->getTplName());
        $this->smarty->assign('parent_cate', $parent_cate);
        $this->smarty->assign('type', $type);
        $this->smarty->assign('ostype', $ostype);
        $this->smarty->display('admin/moregame/cate.html');
    }
    /**
     * 二级栏目
     * @return type
     */
    function cate_kid($pid = 0){
        $parent_cate = '';
        $condition = array();
        if(empty($pid) ) redirect ("/admin/moregame/cate");
        $parent_cate = $this->AdMgcateModel->getRow(array("id"=>$pid));
        $condition['othercondition'] = ' pid = '. $pid;

        $catlist = $this->AdMgcateRelationModel->getCateKidList($pid);
        $this->smarty->assign('catlist', $catlist);
        $this->smarty->assign('tplname', $this->getTplName());
        $this->smarty->assign('pid', $pid);
        $this->smarty->assign('parent_cate', $parent_cate['catename']);
        $this->smarty->assign('ostype', $parent_cate['ostype']);
        $this->smarty->display('admin/moregame/cate_kid.html');
    }
    private function getTplName(){
        return array(
			'1' => '自定义布局',
			'2' => '排行布局',
			'3' => '自适应分栏',
			'4' => '一栏模板',
			'5' => '二栏模板',
        );
    }

    function addcate(){
        $edit = isset($_GET['id']) ? 'edit' : 'add';
        $ostype = $_GET['ostype'] ? $_GET['ostype'] : 2;
        $this->assign('edit', $edit);
        $cate = array();
        $id = (int)$this->input->get('id');
        if($id){
            $cate = $this->AdMgcateModel->getRow(array('id'=>$id));
        }
        $this->assign('cate', $cate);
        $this->assign('ostype', $ostype);
        $this->display('admin/moregame/addcate.html');
    }

    function save(){
        $id = (int)$this->input->post('id');
        $data = array();
        $data['catename'] = $this->input->post('catename');
        if($data['catename'] == ''){
            $this->ajaxReturn(0, '栏目名称不能为空');
        }
        $data['type'] = (int)$this->input->post('type');
        $data['sortid'] = (int)$this->input->post('sortid');
        $data['tplid'] = (int)$this->input->post('tplid');
        $data['appid'] = (int)$this->input->post('appid');
        $data['ostype'] =  (int)$this->input->post('ostype')?(int)$this->input->post('ostype') : 2;
        if(!$data['tplid']){
            $this->ajaxReturn(0, '模板必须选择一个');
        }
        $data['isindex'] = (int)$this->input->post('isindex');
        $status = 0;
        $data['uptime'] = time();
        $data['num'] = 3;
        switch($data['tplid']){
            case 4://一栏模板每页默认为1条，其它为3条。
                $data['num'] = 1;
                break;
            default:
                $data['num'] = 3;
                break;
        }
        if($data['type'] == 2){
            $data['isindex'] = 0;
        }
        if($id){
            //unset($data['tplid']);
            $status = $this->AdMgcateModel->save(array("id"=>$id), $data);
        }else{
            $data['addtime'] = time();
            $exists = $this->AdMgcateModel->getOne(array("ostype"=>$data['ostype'], "catename"=>$data['catename'], 'type'=>$data['type']), 'id');
            if($exists){
                $this->ajaxReturn(0, '栏目已经存在');
            }
            $status = $this->AdMgcateModel->add($data);
        }
        $msg = 'ok';

        $this->ajaxReturn($status, $msg, $data);
    }

    private function ajaxReturn($status = 0, $info = '' , $data = array()){
        echo json_encode(array('status'=>$status, 'info'=>$info, 'data'=>$data));
        exit;
    }

    function applylist($cateid = 0){
        if($cateid == 0){
            exit('参数错误。');
        }
        $cate = $this->AdMgcateModel->getRow(array("id"=>$cateid));
        $this->assign('cate', $cate);
        $this->assign('isedit', 1);
        $list = $this->AdMgApplyModel->getMoregameListByCateId($cateid, $cate);
        foreach($list as $k => $v)
        {
            if(strrpos($v['icon'], 'http') === false)
            {
                $list[$k]['icon'] = "http://res.cocounion.com/".$v['icon'];
            }
        }
        $this->assign('list', $list);
        $this->assign('count', count($list));
        $this->assign('cateid', $cateid);
        //if(!($cate['tplid'] == 2 && $cate['type'] ==1)){
        $adtmplist = array();
        foreach($list as $val){
            $adtmplist[$val['storeid']] = 1;
        }
        $applist = $this->AdMgApplyModel->getMoregameListByFree($cateid, '', 'am.sortid desc', $cate['ostype']);
        foreach($applist as $k => $v)
        {
            if(strrpos($v['icon'], 'http') === false)
            {
                $applist[$k]['icon'] = "http://res.cocounion.com/".$v['icon'];
            }
        }
        $this->assign('applist', $applist);
        $this->assign('adtmplist', $adtmplist);
        //}
        $this->display('admin/moregame/applylist.html');
    }

    function addapply($cateid = 0, $wyapptype = 0, $ismanual=0){
        if($cateid < 1){
            exit('分类ID不能为空');
        }
        $cate = $this->AdMgcateModel->getRow(array("id"=>$cateid));
        $this->assign('cate', $cate);
        if($cate['ostype']==1)
        {
            $androidTypeJson = curlGet("http://api.anzhuoshangdian.com/Pushapply/applyCategory/");
            if(empty($androidTypeJson))
            {

            }
            $androidTypeList = json_decode($androidTypeJson, true);
            if(empty($androidTypeList))
            {
                $androidTypeList = array();
            }
            else
            {
                if($wyapptype === 0 || $wyapptype == 99)
                {
                    $wyapptype = $androidTypeList[0]['cid'];
                }
            }
        }
        else
        {
            $androidTypeList = array();
        }
        $edit = isset($_GET['edit']) ? 'edit' : 'add';
        $appcatelist = $this->AdMgApplyModel->getList(array('status'=>1), 'storeid,stuffid,cateid', '', 'storeid,stuffid,cateid');
        $appcatearray = array('ad'=>array(),'ap'=>array());
        $catelist = $this->AdMgcateModel->getList();
        $catearray = array();
        foreach($catelist as $k => $v)
        {
            $catearray[$v['id']] = $v['catename'];
        }
        if(!empty($appcatelist))
        {
            foreach($appcatelist as $k => $v)
            {
                if($v['stuffid'])
                {
                    $appcatearray['ad'][$v['stuffid']][] = $catearray[$v['cateid']];
                }
                else
                {
                    $appcatearray['ap'][$v['storeid']][] = $catearray[$v['cateid']];
                }
            }
        }
        
        $applist = $this->AdMgApplyModel->getMoregameListByFree($cateid, $wyapptype, 'ai.updatetime desc', $cate['ostype'], $ismanual);
	    foreach($applist as $k => $v)
	    {
	        if(strrpos($v['icon'], 'http') === false)
	        {
	            $applist[$k]['icon'] = "http://res.cocounion.com/".$v['icon'];
	        }
	        $applist[$k]['catearray'] = '';
	        if($v['stuffid'])
	        {
	            if(isset($appcatearray['ad'][$v['stuffid']]))
	            {
	                $applist[$k]['catearray'] = implode(',', $appcatearray['ad'][$v['stuffid']]);
	            }
	        }
	        else
	        {
	            if(isset($appcatearray['ap'][$v['storeid']]))
	            {
	                $applist[$k]['catearray'] = implode(',', $appcatearray['ap'][$v['storeid']]);
             	}
	        }
	    }
        
        $list = $this->AdMgcateModel->getMoregameList($cateid, $cate['ostype']);
        $adtmplist = array();
        foreach($list as $key => $val){
            $adtmplist[$val['storeid']] = 1;
            $list[$key]['catearray'] = '';
            if($val['adid'])
            {
                if(isset($appcatearray['ad'][$val['adid']]))
                {
                    $list[$key]['catearray'] = implode(',', $appcatearray['ad'][$val['adid']]);
                }
            }
            //            else
            //            {
            //                if(isset($appcatearray['ap'][$val['storeid']]))
            //                {
            //                    $list[$key]['catearray'] = implode(',', $appcatearray['ap'][$val['storeid']]);
            //                }
            //            }
        }
        //        print_r($list);
        //        print_r($appcatearray);


        $this->assign('edit', $edit);
        $this->assign('ostype', $cate['ostype']);
        $this->assign('androidtypelist', $androidTypeList);
        $this->assign('cateid', $cateid);
        $this->assign('wyapptype', $wyapptype);
        $this->assign('parent_cate', $cate);
        $this->assign('edit', $edit);
        $this->assign('list', $list);
        $this->assign('adtmplist', $adtmplist);
        $this->assign('applist', $applist);
        $this->assign('ismanual', $ismanual);
        $this->display('admin/moregame/addapply.html');
    }

    function saveFreeApply(){
        $data = array();
        $data['storeid'] = $this->input->post('storeid');
        $data['sortid'] = $this->input->post('sortid');
        $data['status'] = (int)$this->input->post('status');
        $data['cateid'] = $this->input->post('cateid');
        $data['uptime'] = time();
        $data['isgather'] = 1;
        $where = array();
        $where['cateid'] = $data['cateid'];
        $where['storeid'] = $data['storeid'];
        $exists = $this->AdMgApplyModel->isExists($where);
        if($exists){
            $status = $this->AdMgApplyModel->edit($where, $data);
        }else{
            $data['addtime'] = $data['uptime'];
            $status = $this->AdMgApplyModel->add($data);
        }

        $infodata['othername'] = $this->input->post('othername');
        $infodata['othertype'] = $this->input->post('othertype');
        $this->load->model('AdMgInfoModel');
        $this->AdMgInfoModel->edit(array('storeid'=>$data['storeid']), $infodata);
        $msg = '数据保存失败';
        if($status){
            $msg = '数据保存成功';
        }
        $this->ajaxReturn($status, $msg . $this->AdMgApplyModel->getlastsql().print_r($data, true));
    }

    function saveApply(){
        $data = array();
        if($this->input->post('amaid') > 0){
            $data['id'] = $this->input->post('amaid');
        }
        $data['cateid'] = (int)$this->input->post('cateid');
        $data['stuffid'] = $this->input->post('stuffid');
        $where = array();
        if(isset($data['id'])){//已存在的，只按ID更新
            $where['id'] = $data['id'];
        }else{
            $where = $data;
        }
        $data['storeid'] = $this->input->post('storeid');
        $status = (int)$this->input->post('status');
        if(!($data['cateid'] && $data['storeid'] && $data['stuffid']) ){
            $this->ajaxReturn(0, '参数错误');
        }
        $data['sortid'] = (int)$this->input->post('sortid');
        $data['uptime'] = time();
        $this->load->model('AdMgInfoModel');
        $appinfo = $this->AdMgInfoModel->getRow(array("storeid"=>$data['storeid']));
        print_r($appinfo);
        $othername = $this->input->post('othername');
        $othertype = $this->input->post('othertype');
        $msg = '数据保存失败';
        if($appinfo){
            $this->saveStoreid($data['stuffid'], $data['storeid']);
            if(!empty($appinfo['appname']) && !empty($appinfo['downloadurl']) && !empty($appinfo['icon']))
            {
                $isgather = 1;
            }
            else
            {
                $isgather = 0;
            }
            if($status)
            {
                $data['status'] = 1;
                $data['isgather'] = $isgather;
            }
            else
            {
                $data['status'] = 0;
            }
            $exists = $this->AdMgApplyModel->isExists($where);
            if($exists)
            {
                $status = $this->AdMgApplyModel->save($where, $data);
            }
            else
            {
                $data['addtime'] = $data['uptime'];
                $status = $this->AdMgApplyModel->add($data);
            }
            $this->AdMgInfoModel->save(array('storeid'=>$data['storeid']), array('othername'=>$othername, 'othertype'=>$othertype));
        }else{
            //            $data['status'] = 1;
            //            $data['isgather'] = 0;
            //            $data['addtime'] = $data['uptime'];
            //            $data['adtype'] = 3;
            //            $status = $this->AdMgApplyModel->add($data);
            //            $this->AdMgInfoModel->add(array('storeid'=>$data['storeid'], 'othername'=>$othername, 'othertype'=>$othertype));
            $status = 0;
            $msg = '应用不存在，请先抓取，在设置！';
        }
        if($status){
            $msg = '数据保存成功';
        }
        $this->ajaxReturn($status, $msg);
    }

    function saveSpecial(){
        $this->load->model('AdMgSpecialModel');
        $data = array();
        $where = array();
        if($this->input->post('id') > 0){
            $where['id'] = (int)$this->input->post('id');
        }
        $data['pic'] = $this->input->post('pic');
        $data['spec_type'] = $this->input->post('spec_type');
        $data['status'] = $this->input->post('status');
        $data['ostype'] = (int)$this->input->post('ostype')?(int)$this->input->post('ostype'):2;
        $data['subjectid'] = $this->input->post('subjectid');
        $data['title'] = $this->input->post('title');
        $data['sortid'] = $this->input->post('sortid');
        $data['appid'] = $this->input->post('appid');
        $data['uptime'] = time();
        if($where){
            $status = $this->AdMgSpecialModel->save($where, $data);
        }else{
            $exists = $this->AdMgSpecialModel->getOne(array('subjectid'=>$data['subjectid']), 'id');
            if($exists){
                $this->ajaxReturn(0, '该ID已经添加过了。');
            }
            $data['addtime'] = $data['uptime'];
            $status = $this->AdMgSpecialModel->add($data);
        }
        $msg = '数据保存失败';
        if($status){
            $msg = '数据保存成功';
        }
        $this->ajaxReturn($status, $msg);
    }

    function special(){
        $this->load->model('AdMgSpecialModel');
        $count = $this->AdMgSpecialModel->getCount();
        $this->smarty->assign('count', $count);
        $ostype = (int)$this->input->get('ostype')?(int)$this->input->get('ostype'):2;
        $this->load->library('page');
        $config['per_page'] = 20;
        $page = trim($this->input->get('per_page'));
        $current_page = $page == 0 ? 1 : $page;
        $pagelink = $this->page->page_show(base_url() . '/admin/moregame/cate', $count, $config['per_page']);
        $where = array();
        $where['ostype'] = $ostype;
        if($this->input->get('search')){
            $where['title'] = array('like', $this->input->get('search'));
        }
        $list = $this->AdMgSpecialModel->getList($where, "*", ' status desc,id desc');
        foreach($list as $key=>$val){
            $list[$key]['appname'] = $this->AppInfoModel->getOne(array("appid"=>$val['appid']), "appname");
        }
        $this->assign('ostype', $ostype);
        $this->assign('list', $list);
        $this->pagination->create_links();
        $this->smarty->assign('pagelink', $pagelink);
        $this->display('admin/moregame/special.html');
    }

    private function saveStoreid($stuffid, $storeid){
        $stuffid = (int)$this->input->post('stuffid');
        $storeid = $this->input->post('storeid');
        $this->load->model('AdStuffModel');
        $status = 0;
        if($stuffid && $storeid){
            $status = $this->AdStuffModel->save(array("stuffid"=>$stuffid), array('storeid'=>$storeid));
        }
        return $status;
    }

    function addspecial($specialid = 0){
        $info = array();
        $edit = 'add';
        $ostype = (int)$this->input->get('ostype')?(int)$this->input->get('ostype'):2;
        $this->load->model('AdMgSpecialModel');
        if($specialid>0){
            $info = $this->AdMgSpecialModel->getRow(array("id"=>$specialid));
            $edit= 'edit';
        }
        $cate = $this->AdMgcateModel->getList(array('ostype'=>$ostype));
        $apply = $this->AdMgSpecialModel->getApplyList();
        $this->assign('apply', $apply);
        $this->assign('ostype', $ostype);
        $this->assign('cate', $cate);
        $this->assign('edit', $edit);
        $this->assign('info', $info);
        $this->display('admin/moregame/addspecial.html');
    }

    function setspecialstatus(){
        $id = (int)$this->input->get('id');
        $status = $this->input->get('status');
        $this->load->model('AdMgSpecialModel');
        $this->AdMgSpecialModel->save(array('id'=>$id), array('status'=>$status));
        redirect("/admin/moregame/special");
    }

    public function ajaxDoUploadStuff() {
        $this->load->model('AdMgSpecialModel');
        $returnArray = array('isReceived' => false, 'errorMessage' => '', 'imgInfo' => '', 'imgs_id' => null);
        if ($_FILES['Filedata']['size'] > 120 * 1024) {
            $returnArray['errorMessage'] = '图片大小超过120kb';
            echo json_encode(array($returnArray));
            return;
        }
        $type = $this->input->get_post('type');
        $returnArray = $this->AdMgSpecialModel->upload($type);

        if ($returnArray['isReceived']) {
            $returnArray['imgs_id'] = $type;
        }

        echo json_encode(array($returnArray));
        return;
    }
    
	public function ajaxGetAppInfoAndroid() {
        $storeid = trim($this->input->post('storeid'));
        $status = 0;
        $msg = "抓取失败，请重试！";
        $this->load->model('AdMgInfoModel');
        if(empty($storeid))
        {
            $msg = "请输入StoreID！";
        }
        else
        {
                $info = curlGet("http://api.anzhuoshangdian.com/Cocoapi/getApplyInfo/?appid=".$storeid);
                if(!empty($info))
                {
                    $data = json_decode($info, true);
                    if(!empty($data['result']))
                    {
                    	$msg = "抓取成功！";
                    	$appinfo = array();
		                $appinfo['storeid'] = $data['result']['appid'];
		                $appinfo['appname'] = $data['result']['name'];
		                $appinfo['packagename'] = $data['result']['packname'];
		                $appinfo['icon'] = $data['result']['icon'];
		                $appinfo['size'] = $data['result']['size'];
		                $appinfo['downloadurl'] = $data['result']['url'];
		                $appinfo['apptype'] = $data['result']['cname'];
		                $appinfo['appdesc'] = $data['result']['intro'];
		                $appinfo['versioncode'] = $data['result']['ver'];
               			$appinfo['versionname'] = $data['result']['version_code'];
               			$appinfo['wangyitype'] = $data['result']['cid'];
               			$appinfo['num'] = $data['result']['num'];
               			$appinfo['android_api_level'] = $data['result']['api_level'];
               			$appinfo['level'] = $data['result']['level']; 
	                    if(!empty($data['result']['thumbs']))
		                {
		                    $tmpthumbs = explode(',', $data['result']['thumbs']);
		                    foreach($tmpthumbs as $kt => $vt)
		                    {
		                        $tmpthumbs[$kt] = ANZHUOSHANGDIAN_IMG_HOST.$vt;
		                    }
		                    $appinfo['imgpath'] = json_encode($tmpthumbs);
		                }
		                $appinfo['price'] = 0;
		                $appinfo['ostype'] = 3;
		                $appinfo['oldprice'] = 0;
		                $appinfo['manual'] = 1;
		                $appinfo['updatetime'] = time();
		                
		                $chkInfo = $this->AdMgInfoModel->getRow(array('storeid' => $appinfo['storeid'], 'ostype' => 3));
		                if($chkInfo)
		                {
		                    $this->AdMgInfoModel->edit(array('storeid'=>$appinfo['storeid'], 'ostype' => 3), $appinfo);
		                }else{
		                	$this->AdMgInfoModel->add($appinfo);
		                }
		                $status = 1;
                    }
                }
        }
        $this->ajaxReturn($status, $msg);
    }

    public function ajaxGetAppInfo() {
        $storeid = trim($this->input->post('storeid'));
        $status = 0;
        $msg = "抓取失败，请重试！";
        $this->load->model('AdMgInfoModel');
        if(empty($storeid))
        {
            $msg = "请输入StoreID！";
        }
        else
        {
            $isExists = $this->AdMgInfoModel->isExists(array('storeid' => $storeid));
            if($isExists)
            {
                $msg = "应用已存在！";
            }
            else
            {
                $info = curlGet("http://gather.cocounion.ads/index.php?m=GetAppInfo&a=webGetAppInfo&storeid=".$storeid);
                if(!empty($info))
                {
                    $data = json_decode($info, true);
                    if($data['status'] && $data['info'])
                    {
                        $status = 1;
                        $msg = "抓取成功！";
                        $appinfo['storeid'] = $data['info']['storeid'];
                        $appinfo['appname'] = $data['info']['name'];
                        $appinfo['icon'] = $data['info']['icon'];
                        $appinfo['size'] = $data['info']['size'];
                        $appinfo['apptype'] = $data['info']['apptype'];
                        $appinfo['imgpath'] = isset($data['info']['iphoneimages'])?$data['info']['iphoneimages']:$data['info']['ipadimages'];
                        $appinfo['appdesc'] = $data['info']['descs'];
                        $appinfo['downloadurl'] = $data['info']['url'];
                        $appinfo['versioncode'] = $data['info']['version'];
                        //$appinfo['versionname'] = $info['versionname'];
                        $appinfo['packagename'] = $data['info']['packagename'];
                        $appinfo['price'] = $data['info']['price'];
                        $appinfo['ostype'] = 1;
                        $appinfo['oldprice'] = 0;
                        $appinfo['releasenotes'] = $data['info']['releasenotes'];
                        $appinfo['wangyitype'] = 99;
                        $appinfo['updatetime'] = time();
                        $isExists = $this->AdMgInfoModel->isExists(array('storeid' => $storeid));
                        if($isExists)
                        {
                            $msg = "应用已存在！";
                            $this->ajaxReturn($status, $msg);
                        }
                        $this->AdMgInfoModel->add($appinfo);
                    }
                }
            }
        }
        $this->ajaxReturn($status, $msg);
    }

    //删除抓取信息。
    public function ajaxDelAppInfo() {
        $id = trim($this->input->post('id'));
        $status = 0;
        $this->load->model('AdMgInfoModel');
        if(empty($id))
        {
            $msg = "id错误，请刷新重试！";
        }
        else
        {
            $data = $this->AdMgInfoModel->getRow(array('id' => $id));
            if(empty($data))
            {
                $msg = "应用已删除，请刷新重试！";
            }
            else
            {
                $this->AdMgApplyModel->edit(array("storeid"=>$data['storeid']), array('status'=>0));
                $this->AdMgInfoModel->delete(array('id'=>$id));
            	if(!empty($data['icon']) && is_file(UPLOAD_DIR."/".$data['icon'])){
                	unlink(UPLOAD_DIR."/".$data['icon']);
                }
            	if(!empty($data['imgpath'])){
					$imgpath=explode(',', $data['imgpath']);
		        	foreach($imgpath as $v){
		        		$v_path = trim(stripslashes($v), '[]"');
		        		if(!empty($v_path) && is_file(UPLOAD_DIR."/".$v_path)){
			        		unlink(UPLOAD_DIR."/".$v_path);
			        	}
		        	}
				}
                
                $status = 1;
                $msg = '删除成功！';
            }
        }
        $this->ajaxReturn($status, $msg);
    }

    function delcate($cateid){
        $this->AdMgApplyModel->edit(array("cateid"=>$cateid), array('status'=>0));
        $status = $this->AdMgcateModel->edit(array("id"=>$cateid), array('status'=>0));
        echo json_encode(array('info'=>'删除失败', 'status'=>$status));
    }

    function delApp(){
        $cateid = (int)$this->input->post('cateid');
        $status = (int)$this->input->post('status');
        $stuffid = (int)$this->input->post('stuffid');
        $storeid = (int)$this->input->post('storeid');
        if(!$cateid  && !$storeid){
            $this->ajaxReturn(0, '接收的参数错误');
        }
        $status = $this->AdMgApplyModel->edit(array('cateid'=>$cateid, 'storeid'=>$storeid, 'stuffid'=>$stuffid), array("status"=>$status));
        $msg = "应用修改修改失败" . $this->AdMgApplyModel->getlastsql();
        if($status){
            $msg = "应用数据修改成功";
        }
        $this->ajaxReturn($status, $msg);
    }

    function ajaxEditAppInfo(){
        $othername = $this->input->post('othername');
        $cateid = (int)$this->input->post('cateid');
        $sortid = (int)$this->input->post('sortid');
        $storeid = (int)$this->input->post('storeid');
        $ostype = (int)$this->input->post('ostype');
        if(empty($cateid) || empty($storeid) || (empty($ostype) && $ostype !== 0)){
            $this->ajaxReturn(0, '接收的参数错误');
        }
        $status = $this->AdMgApplyModel->edit(array('cateid'=>$cateid, 'storeid'=>$storeid), array('sortid'=>$sortid));
        $statusInfo = $this->AdMgInfoModel->edit(array('ostype'=>$ostype, 'storeid'=>$storeid), array('othername'=>$othername));
        $msg = "应用数据修改成功";
        $this->ajaxReturn($status, $msg);
    }
    /**
     * 一级栏目添加二级栏目
     */
    function addkitcate(){
        $pid = (int)$this->input->get('id');
        $ostype = (int)$this->input->get('ostype')?(int)$this->input->get('ostype'):2;
        $parent_cate = $this->AdMgcateModel->getRow(array("id"=>$pid), "catename,appid");
        //所有二级栏目
        $condition = array();
        $condition['othercondition'] = 'type = 2';
        $condition['othercondition'] .= ' and status = 1';
        $condition['othercondition'] .= " and ostype = {$ostype}";
        $catlist = $this->AdMgcateModel->getList($condition, "*", " id desc ");
        //一级栏目 现有的 二级栏目
        $has_cate_list = array();
        $has_cate = $this->AdMgcateRelationModel->getRellationList($pid);
        foreach($has_cate as $key=>$val){
            $cateid = $val['cateid'];
            $has_cate_list[$cateid] = $cateid;
        }
        $kid_cates = array();
        $kid_cate = $this->AdMgcateRelationModel->getList(array('pid'=>$pid));
        foreach($kid_cate as $key=>$val){
            $kid_cates[$val['cateid']] = $val;
        }
        foreach($catlist as $key=>$val){
            $cateid = $val['id'];
            if(!empty($has_cate_list[$cateid])){
                unset($catlist[$key]);
            }else{
                if(!empty($kid_cates[$cateid])){
                    $catlist[$key]['is_check'] = '1';
                    $catlist[$key]['cate_alias'] = $kid_cates[$cateid]['cate_alias'];
                    $catlist[$key]['cate_sortid'] = $kid_cates[$cateid]['sortid'];
                }
            }
        }
        $this->assign('catlist', $catlist);
        $this->assign('kid_cate', $kid_cates);
        $this->assign('pid', $pid);
        $this->assign('parent_cate', $parent_cate);
        $this->assign('edit', '');
        $this->assign('ostype', $ostype);
        $this->display('admin/moregame/addkitcate.html');
    }
    /**
     * 保存 二级栏目
     */
    function savekitcate(){
        $permission = $this->input->post('permission');
        $cate_alias = $this->input->post('cate_alias');
        $sortid = $this->input->post('sortid');
        $pid = (int)$this->input->post('pid');
        $ostype = (int)$this->input->post('ostype')?(int)$this->input->post('ostype'):2;
        $condition = array();
        $condition['othercondition'] = 'type = 2';
        $condition['othercondition'] .= ' and status = 1';
        $condition['othercondition'] .= " and ostype = {$ostype}";
        $catlist = $this->AdMgcateModel->getList($condition, "*", " id desc ");
        $now = time();
        foreach ($catlist as $key=>$val){
            $kid_cateid = $val['id'];
            $data = array();
            $data['pid'] = $pid;
            if(!empty($permission[$kid_cateid])) $data['cateid'] = $permission[$kid_cateid];
            $data['cate_alias'] = $cate_alias[$kid_cateid];
            $data['sortid'] = $sortid[$kid_cateid];
            $data['addtime'] = $now;
            $kid_cate = $this->AdMgcateRelationModel->getRow(array('pid'=>$pid,'cateid'=>$kid_cateid));

            if(empty($kid_cate)){
                if(!empty($permission[$kid_cateid])) {
                    $status = $this->AdMgcateRelationModel->add($data);
                }
            }else{
                $id = $kid_cate['id'];
                if(!empty($permission[$kid_cateid])) {
                    $status = $this->AdMgcateRelationModel->save(array("id"=>$id), $data);
                }else{
                    $this->AdMgcateRelationModel->delete(array('id'=>$id));
                }
            }
        }
        redirect("/admin/moregame/cate/?ostype={$ostype}");
    }
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ad extends MY_A_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("AdGroupModel");
        $this->load->model("AdCampaignModel");
        $this->load->model("AdStuffModel");
        $this->load->model("UserMemberModel");
    }

    function advertisers(){
        $where = array();
        $search = $this->input->get('search');
        $usertype = $this->input->get('usertype');
        $this->assign('search', $search);
        $this->assign('usertype', $usertype);
        if(!empty($search)){
            $where['username'] = array('like', "%$search%");
        }
        if(!empty($usertype)){
            $where['usertype'] = $usertype;
        }
        $this->load->model("UserMemberModel");
        $userlist = $this->UserMemberModel->getList($where, '*', 'userid desc ');
        $this->assign('userlist', $userlist);
        $this->display('admin/ad/advertisers.html');
    }
    /**
     * 变更广告组身份
     */
    function ownset(){
        $this->load->model("UserMemberModel");
        $userId = $this->input->get('userid');
        $userType = $this->input->get('usertype');
        $this->UserMemberModel->edit( array('userid' => $userId), array('usertype' => $userType));
        redirect('/admin/ad/advertisers');
    }
    /**
     * 解锁 锁定用户
     */
    function setuserstatus(){
        $userId = $this->input->get('userid');
        $userstatus = $this->input->get('userstatus');
        $this->UserMemberModel->edit( array('userid' => $userId), array('userstatus' => $userstatus));
        redirect('/admin/ad/advertisers');
    }
    function adorder(){
        $this->load->model('UserMemberModel');
        $where = array();
        $data['status'] = (int)$this->input->get('status');
        if($data['status']){
            $where['status'] = $data['status'];
        }
        $data['userid'] = (int)$this->input->get('userid');
        if($data['userid']){
            $where['userid'] = $data['userid'];
        }
        $data['targettype'] = (int)$this->input->get('targettype');
        if($data['targettype']){
            $where['targettype'] = $data['targettype'];
        }
        $data['ostypeid'] = (int)$this->input->get('ostypeid')?(int)$this->input->get('ostypeid'):'2';
        if($data['ostypeid'] ){
            if ( $data['ostypeid'] == '3') $where['ostypeid'] = '0';else $where['ostypeid'] = $data['ostypeid'];
        }
        $data['searchtype'] = (int)$this->input->get('searchtype');
        $data['search'] = $this->input->get('search');
        if($data['searchtype'] == 1 && $data['search']){
            $where['campaignname'] = array('like','%'.$data['search'].'%');
        }elseif($data['searchtype'] == 2 && $data['search']){
            $userwhere['username'] = array('like','%'.$data['search'].'%');
            $userlist = $this->UserMemberModel->getList($userwhere,"userid");
            foreach($userlist as $key=>$val){
                $userlists[] = $val['userid'];
            }
            $userids =  implode(',',$userlists);
            $where['userid'] = array('in',$userids);
        }
         
        $p=trim($this->input->get('per_page'));
        if(!$p){
            $p=1;
        }
        $this->AdCampaignModel->setStrWhere($where);
        $config['per_page'] = 20;
        $count = $this->AdCampaignModel->getCount();
        $adlist = $this->AdCampaignModel->getList($where,"*","campaignid desc", "", $p,$config['per_page']);

        $userid = '';
        $this->load->model('AdGroupModel');
        foreach($adlist as $key=>$val){
            //广告组数
            $groupinfo = $this->AdGroupModel->getRow(array('campaignid'=>$val['campaignid']),"count(adgroupid) as adgroupnum");
            $adlist[$key]['adgroupnum']  = $groupinfo['adgroupnum'];
            $userid[$val['userid']] = $val['userid'];
        }
        $this->load->library('page');
        $current_page =  $p==0 ? 1 : $p;
        $argv = 'status='.$data['status'].'&userid='.$data['userid'].'&targettype='.$data['targettype'].'&ostypeid='.$data['ostypeid'].'&search='.$data['search'];
        $page=$this->page->page_show('/admin/ad/adorder?'.$argv,$count,$config['per_page']);
        $this->assign('data',$data);
        $this->assign('page', $page);
        $this->assign('per_page', $config['per_page']);
        $this->assign('count', $count);
        $this->assign('adlist',$adlist);
        $userlist = $this->UserMemberModel->getList(array("userid"=>array("in", $userid)),"userid,username,realname");
        $users = array();
        foreach($userlist as $val){
            $users[$val['userid']] = $val['realname'].'<br />'.$val['username'];
        }
        $this->assign('users', $users);
        $this->display('admin/ad/adorder.html');
    }

    function adGroup($campaignid=''){
        if(!$campaignid) exit('error');
        $this->getAdCampaign($campaignid);
        $this->assign('tab', 'adgroup');
        $groups = $this->AdGroupModel->getList(array('campaignid'=>$campaignid));
        $this->load->model('AppChannelModel');
        $this->assign('groups', $groups);
        $campaignInfo = $this->AdCampaignModel->getRow(array("campaignid"=>$campaignid));
        $this->assign('ostypeid', $campaignInfo['ostypeid']);
        $list = $this->AdGroupModel->getAdgroup($groups, $campaignid);
        $this->display('admin/ad/adgroup.html');
    }
    
    function getAppIDs(){
    	$adgroupid = $this->input->post('adgroupid');
    	$typearray = $this->AdGroupModel->getById("adtargettype", $adgroupid);
    	$adtargettype = $typearray[0]['adtargettype'];
    	$this->load->model('AppChannelModel');
    	$pidList=$this->AppChannelModel->getAdexchangeAppidList();
    	$exchangePids = array();
    	foreach($pidList as $v){
    		$exchangePids[] = $v['appid'];
    	}
    	$this->load->model('AdRetargetModel');
    	$targetsList = $this->AdRetargetModel->getAdRetarget($adgroupid, 1);
    	$targets = array();
    	foreach($targetsList as $v){
    		$targets[] = $v['appid'];
    	}
    	$appIdList = array();
    	if($adtargettype == 2){
    		foreach($targets as $target){
    			if(in_array($target, $exchangePids)){
    				$appIdList[] = $target;
    			}
    		}
    		ajaxReturn('', 1, $appIdList);
    	}elseif ($adtargettype == 1){
    		foreach($exchangePids as $v){
    			if(!in_array($v, $targets)){
    				$appIdList[] = $v;
    			}
    		}
    		ajaxReturn('', 1, $appIdList);
    	}
    	ajaxReturn('', 1, $exchangePids);
    }

    function adStuff($campaignid=''){
        if(!$campaignid) exit('error');
        $this->getAdCampaign($campaignid);
        $this->assign('tab', 'adstuff');
        $stuff = $this->AdStuffModel->getList(array('campaignid'=>$campaignid));
        $this->assign('stuffs', $stuff);
        $group_tmp = $this->AdGroupModel->getList(array('campaignid'=>$campaignid));
        $groups = array();
        foreach($group_tmp as $val){
            $groups[$val['adgroupid']] = $val;
        }
        $this->assign('groups', $groups);
        $this->display('admin/ad/adstuff.html');
    }

    private function getAdCampaign($campaignid){
        $info = $this->AdCampaignModel->getRow(array("campaignid"=>$campaignid));
        $info['periodset'] = json_decode($info['periodset']);
        $nettypesetarr = explode(",", $info['nettypeset']);

        foreach ($nettypesetarr as $key=>$val){
            $nettypesetstr[] =  getnettypeset($val);
        }
        $info['nettypesetstr'] = implode($nettypesetstr,",");
        $this->assign('info', $info);
    }
    
    function getadgroupretarget(){
        $adgroupid = (int)$this->input->post('adgroupid');
        $type_get = (int)$this->input->post('type');
        $type=($type_get==2)?2:1;
        if($type == 2)
        	$fields = "adsubtargettype retarget_type,adgroupid";
        else 
        	$fields = 'adtargettype retarget_type,adgroupid';
        $info = $this->AdGroupModel->getRow(array('adgroupid'=>$adgroupid), $fields);
        if(empty($info)){
            ajaxReturn('该广告组不存在！', 0, 0);
        }
         
        $this->load->model('AdRetargetModel');
        $list = $this->AdRetargetModel->getAdRetarget($adgroupid, $type);
        $this->load->model('AppTypeModel');
        $os = $this->SystemConfigModel->getSystemConfig('os_type');
        $cate = $this->AppTypeModel->getList(array());
        $cate = getArrayByKey($cate,'apptypeid');
        foreach($list as $key=>$val){
            $list[$key]['os'] = getArrayVal($os,$val['ostypeid']);
            $list[$key]['cate'] = getArrayValByKey($cate,$val['apptypeid'], 'typename');
        }
        if(!empty($list)){
            ajaxReturn($info['retarget_type'], 1, $list);
        }
        ajaxReturn('OK', 0, 0);
    }

    function ajaxgetretargetapp(){
        $appid = $this->input->post('appid');
        $type_get = $this->input->post('type');
        $type=($type_get==2)?2:1;
        if($type == 2){
        	$siteid = trim($this->input->post('siteid'));
        	if(empty($appid)) ajaxReturn('请选择APPID', 0, '');
        	if(empty($siteid)) ajaxReturn('请填写子媒体ID', 0, '');
        }
        $this->load->model('AppInfoModel');
        $appids = explode("\n", $appid);
        $errappids = null;
        $succappids = null;
        if(count($appids) > 0)
        {
	        foreach($appids as $v)
	        {
	            $tmp = trim($v);
	            if(empty($tmp))
	            {
	                continue;
	            }
	            if(!is_numeric($tmp))
	            {
	                $errappids[] = $v;
	                continue;
	            }
	            $info = $this->AppInfoModel->getRow(array('appid'=>$v), 'appid,appname,ostypeid,apptypeid,appchildtypeid');
	            if(empty($info))
	            {
	                $errappids[] = $v;
	                continue;
	            }
	            $this->load->model('AppTypeModel');
	            $os = $this->SystemConfigModel->getSystemConfig('os_type');
	            $info['os'] = getArrayVal($os, $info['ostypeid']);
	            $info['cate'] = $this->AppTypeModel->getOne(array('apptypeid'=>$info['apptypeid']),'typename');
	            $succappids[] = $info;
	        }
        }
        ajaxReturn($errappids, 1, $succappids);
    }

    function ajaxsetretarget(){
        $adgroupid = $this->input->post('adgroupid');
        $retarget = $this->input->post('retarget');
        $retargets = explode(',',trim($retarget,',')); 
        $type_get = $this->input->post('type');
        $type = ($type_get==2)?2:1;
        $user = $this->session->userdata("admin");
        $data = array("adgroupid"=>$adgroupid, 'admin_id'=>$user['admin_id'], 'type'=>$type);
        $batch = array();
        foreach($retargets as $val){
            $tmp = trim($val);
            if(empty($tmp))
            {
                continue;
            }
            if($type==1){
            	$data['appid'] = trim($val);
            }else{
            	$val = trim($val);
            	$data['appid'] = strstr($val,"-",true);
	            $data['siteid'] = trim(strstr($val,"-"),'-');
            }
            $batch[] = $data;
        }
        $adtargettype = (int)$this->input->post('ignoretype');
        $originalIgnoreType = (int)$this->input->post('originalignoretype');
        $this->load->model('AdGroupModel');
        $this->load->model('AdRetargetModel');
    	if($type == 1 && $adtargettype != $originalIgnoreType){
        	$this->AdRetargetModel->delete(array("adgroupid"=>$adgroupid, "type"=>2));
        	$this->AdGroupModel->edit(array("adgroupid"=>$adgroupid), array("adsubtargettype"=>0));
        }
        if(empty($batch)) $adtargettype = 0;
        if($type == 1){
        	$this->AdGroupModel->edit(array("adgroupid"=>$adgroupid), array("adtargettype"=>$adtargettype));
        }
        else {
        	$this->AdGroupModel->edit(array("adgroupid"=>$adgroupid), array("adsubtargettype"=>$adtargettype));
        }
        $count=$this->AdRetargetModel->getCount(array("adgroupid"=>$adgroupid, "type"=>$type));
        $deletescount = $this->AdRetargetModel->delete(array("adgroupid"=>$adgroupid, "type"=>$type));
    	if($deletescount < $count){
        	ajaxReturn('添加定向失败！', 0, 0);
        }
        $this->load->model('AdStuffMoregameModel');
        $this->AdStuffMoregameModel->updateisupdate2($adgroupid,'ad_group');
        if(!empty($batch) && count($batch) > 0)
        {
            $status = $this->AdRetargetModel->addBatch($batch);
        }
        else
        {
            ajaxReturn('媒体定向设置成功！', 1, 0);
        }
        if($status){
            ajaxReturn('媒体定向设置成功！', 1, 0);
        }
        ajaxReturn('添加定向失败！', 0, 0);
    }
    /**
     * 广告组详情
     */
    function groupdetail($adgroupid=''){
        if(!$adgroupid) exit('error');
        $this->assign('tab', 'adgroup');
        $groupinfo = $this->AdGroupModel->getRow(array('adgroupid'=>$adgroupid));
        $this->assign('groupinfo', $groupinfo);
        if($groupinfo['scheduletype'] == 1)
        {
            $this->load->model("AdSchedulesModel");
            $schedule = $this->AdSchedulesModel->getUserData(array('adgroupid'=>$adgroupid));
            $this->assign('schedule', $schedule);
        }
        $campaigninfo = $this->AdCampaignModel->getRow(array('campaignid'=>$groupinfo['campaignid']));
        $this->assign('campaigninfo', $campaigninfo);
        $stuff = $this->AdStuffModel->getList(array('adgroupid'=>$adgroupid));
        $effecttype = $this->SystemConfigModel->getSystemConfig('effect_type');
        $this->assign('effecttype', $effecttype);
        $this->assign('stuff', $stuff);

        $this->display('admin/ad/adgroupdetail.html');
    }

    /**
     * 修改广告创意状态，暂停和启动
     */
    function ajaxsetispause(){
        $id = $this->input->post('id');
        $ispause = (int)$this->input->post('ispause');
        if(empty($id))
        {
            ajaxReturn('请选择要操作的创意！', 0, '');
        }
        $ispause = !$ispause;
        $status = $this->AdStuffModel->edit(array("stuffid"=>$id), array("ispause"=>$ispause));
        $this->load->model('AdStuffMoregameModel');
        $this->AdStuffMoregameModel->updateisupdate2($id,'ad_stuff');
        if($status) ajaxReturn('修改成功！', 1, 0);
        ajaxReturn('修改失败！', 0, $this->AdStuffModel->getlastsql());
    }

    /**
     * 修改优先级
     */
    function ajaxgdadsave(){
        $adgroupid = $this->input->post('adgroupid');
        $gdapriority = $this->input->post('gdapriority');
        $gdadcmpid = $this->input->post('gdadcmpid');
        $gdatag = $this->input->post('gdatag');
        if(empty($adgroupid) || empty($gdadcmpid))
        {
            ajaxReturn('脚本错误，请刷新重试！', 0, '');
        }
        if((!is_numeric($gdapriority) || $gdapriority < 1 || $gdapriority > 100 ) && $gdatag == 1)
        {
            ajaxReturn('优先级不能小于1且不能大于100！', 0, '');
        }
        $where['adgroupid'] = $adgroupid;
        $data['gdatag'] = $gdatag;
        if($gdatag != 0)
        {
            $data['gdapriority'] = $gdapriority;
        }
        $status = $this->AdGroupModel->edit($where, $data);
        $adcmpinfo = $this->AdCampaignModel->getRow(array('campaignid'=>$gdadcmpid));
        if(!empty($adcmpinfo))
        {
            $status = $this->AdCampaignModel->edit(array('campaignid' => $gdadcmpid), array('isupdate' => $adcmpinfo['isupdate']+1,));
        }
        if($status) ajaxReturn('修改成功！', 1, 0);
        ajaxReturn('修改失败！', 0, '');
    }


    /**
     * 绝对优先设置和取消
     */
    function adgroup_ajaxChangeAbsolutePriority($dbpk,$newvalue=0){

        if((!empty($dbpk))){
            if(in_array($newvalue, array('0','1'))){
                $adgroupinfo = $this->AdGroupModel->getRow(array('adgroupid'=>$dbpk));
                if(!empty($adgroupinfo))
                {
                    $adcmpinfo = $this->AdCampaignModel->getRow(array('campaignid'=>$adgroupinfo['campaignid']));
                    $this->AdCampaignModel->edit(array('campaignid' => $adcmpinfo['campaignid']), array('isupdate' => $adcmpinfo['isupdate']+1,));
                }
                else
                {
                    ajaxReturn("广告活动不存在，请刷新重试！", 0);
                }
                $retValue = $this->AdGroupModel->edit(array('adgroupid'=>$dbpk),array('exclusivegda'=>$newvalue));

            }

            ajaxReturn("修改成功", $retValue>0?1:0);
        } else {
            ajaxReturn("缺少参数", 0);
        }

    }
}
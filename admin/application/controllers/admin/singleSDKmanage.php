<?php

if (! defined ( 'BASEPATH' ))
    exit ( 'No direct script access allowed' );
class singleSDKmanage extends MY_A_Controller {
    function __construct() {
        parent::__construct ();
        $this->load->model ( "AppInfoModel" );
        $this->load->model ( 'AdGameListModel' );
        $this->load->model ( 'AdSubChannelModel' );
        $this->load->model ( 'AdChannelPublicModel' );
        $this->load->model ( 'AdStuffModel' );
        $this->load->helper ( 'url' );
        
        $this->assign ( 'login_uname', $this->session->userdata ['admin'] ['admin_name'] );
    }
    
    /**
     * 设置应用客服电话
     */
    public function kefuInfomationList() {
        $this->assign('tabindex', 4);
        $appid = $this->input->get ( 'appid' );
        $appname = $this->AppInfoModel->getOne ( array (
                'appid' => $appid
        ), 'appname' );
        
        $this->load->model ( 'AppChannelConfigModel' );
        
        if (empty ( $appid )) {
            show_error ( '参数错误', '500' );
        }
        $this->_checkSwitchStatus($appid);
        
        $currenttel = $this->AppChannelConfigModel->getOne ( array (
                'appid' => $appid,
                'publisherID'=>0,
                'configname' => 'kefutel'
        ) ,'configvalue');
        if(!isset($currenttel)) {
            $currenttel = '';
        }
        $this->assign('dbval', $currenttel);
        $currenttel = explode('-', $currenttel);
        
        
        $this->assign ( 'appname', $appname );
        $this->assign ( 'appid', $appid );
        $this->assign('val', $currenttel);
        $this->display('admin/singleSDKmanage/gameKefu.html');
    }
    
    
    /**
     * 查看指定APP的客服电话PAGE
     * @deprecated
     * @author yangsen
     */
    private function _kefuInfomationList() {
        $this->assign('tabindex', 4);
        $appid = $this->input->get ( 'appid' );
        $appname = $this->AppInfoModel->getOne ( array (
                'appid' => $appid 
        ), 'appname' );
        
        if (empty ( $appid )) {
            show_error ( '参数错误', '500' );
        }
        
        $this->load->model ( 'AppChannelModel' );
        $this->load->model ( 'AppChannelConfigModel' );
        $channels = $this->AppChannelModel->getChannelByCond ( array (
                'appid' => $appid 
        ) );
        $channels_tels = $this->AppChannelConfigModel->getList ( array (
                'appid' => $appid,
                'configname' => 'kefutel' 
        ) );
        
        if (is_array ( $channels_tels ) && count ( $channels_tels ) > 0) {
            foreach ( $channels_tels as $k => $v ) {
                foreach ( $channels as $ck => &$cv ) {
                    if ($cv ['appid'] == $v ['appid'] && $cv ['channelid'] == $v ['channelid']) {
                        $cv ['configvalue'] = $v ['configvalue'];
                        // $cv['configname']=$v['configname'];
                    }
                }
            }
            // $channels = array_merge_recursive($channels,$channels_tels);
        }
        
        // print_r($channels);
        
        $this->assign ( 'channels', $channels );
        $this->assign ( 'appname', $appname );
        $this->assign ( 'appid', $appid );
        
        $this->display ( 'admin/singleSDKmanage/gameChannelKefuList.html' );
    }
    
    /**
     * ajax 更新 CCPLAY应用某渠道的客服电话
     * 
     * @author yangsen
     */
    public function updateChannelKefuTel() {
        $appid = $this->input->post ( 'appid' );
        $val = $this->input->post ( 'val' );
        $this->load->model ( 'AppChannelConfigModel' );
        
        $currenttel = $this->AppChannelConfigModel->getRow ( array (
                'appid' => $appid,
                'publisherID'=>0,
                'configname' => 'kefutel'
        ) ,'configvalue,campaignid,adid');
        
        if(!empty($val)) {        
            $val = trim($val,'-');
            $val = str_replace('--', '-', $val);
        }
        
        
        
        $campaign_id = null;
        $stuff_id = null;
        if($currenttel!=null) {
            $campaign_id = $currenttel['campaignid'];
            $stuff_id = $currenttel['adid'];
        } else {
            $time = time();
            //广告方式侦测关联值保存
            // 要插入数据的表
            $this->load->model ( 'AdCampaignModel' );
            $this->load->model ( 'AdGroupModel' );
            $this->load->model ( 'AdStuffModel' );
            $this->load->model ( 'AdStuffTextModel' );
            
            $userid = $this->AppInfoModel->getOne ( array (
                    'appid' => $appid
            ), 'userid' );
            $title = '客服电话 APPID:'.$appid."";
            $campaign_data = array (
                    'campaignname' => $title,
                    'targettype' => '2',
                    'periodset' => '0',
                    'ostypeid' => 1,
                    'osversionset' => 0,
                    'aosvset' => '',
                    'createtime' => $time,
                    'status' => 2,//投放中
                    'selfappset' => 1,
                    'userid' => $userid
            );
            $campaign_id = $this->AdCampaignModel->add ( $campaign_data );
            
            $group_data = array (
                    'campaignid' => $campaign_id,
                    'adgroupname' => $title,
                    'adform' => '13',
                    'chargemode' => 1,
                    'adtargettype' => 0,
                    'selfappset' => 1,
                    'is_ccplay_announce' => 1,
                    'starttime' => strtotime ( date ( 'Y-m-d', $time ) ),
                    'endtime' => (strtotime ( date ( 'Y-m-d', $time ) ) + 60 * 60 * 24 * 365*10),
                    'userid' => $userid,
                    'createtime' => $time,
                    'lastupdatetime' => $time,
                    'istracktype' => 2,
                    'status' => 1,
                    'effecttype' => 23
            ); //adform=13 effecttype = 23表示是客服
            
            $group_id = $this->AdGroupModel->add ( $group_data );
            $stuff_data = array (
                    'adgroupid' => $group_id,
                    'campaignid' => $campaign_id,
                    'adstuffname' => $title,
                    'target' => 'tel',
                    'stufftype' => 2,
                    'lastupdatetime' => $time,
                    'status' => 1
            );
            $stuff_id = $this->AdStuffModel->add ($stuff_data);
            //广告方式侦测关联值保存END
        }
        
        
        $data = array (
                'appid' => $appid,
                'publisherID' => 0,
                'configname' => 'kefutel',
                'configvalue' => $val,
                'campaignid'=>$campaign_id,
                'adid'=>$stuff_id
        );
        $efrownum = $this->AppChannelConfigModel->add ( $data, true );
        
        
        ajaxReturn ( '修改成功', 1, array (
                'effectrow' => $efrownum 
        ) );
    }
    
    protected function _checkSwitchStatus($appid) {
        $val = $this->load->Model('AppInfoModel');
        $r =  $this->AppInfoModel->getRow(array('appid'=>$appid));
        $ctws = $this->config->item('ccplay_tab_with_switch');
        $ctflag = $this->config->item('ccplay_tab_flag');
        $this->assign('ctws', $ctws);
        $this->assign('ctflag',$ctflag);
        if($ctws == true && $r['is_ccplay_announce']==0) {
            if($r['is_ccplay_gift']==1) {
                redirect('ccplayGiftList?appid='.$appid);  
            }
            if($r['is_ccplay_kefu']==1) {
                redirect('kefuInfomationList?appid='.$appid);
            }
            if($r['is_ccplay_kefu']==1) {
                redirect('kefuInfomationList?appid='.$appid);
            }
            if('gamePublicListPage' == $this->uri->segment(3)) {
                redirect('putAdListCcplay?appid='.$appid);
            }
        }
        
        $this->assign('tabAppInfo', $r);
        // print_r($r);
        return $r;
    }
    
    /**
     * 根据campaignid查询公告Img
     * 
     * @param unknown $campaignid            
     * @return string
     */
    protected function _getAnnounceImgByCampaignId($campaignid, $bool_baseurl = true) {
        $this->load->model ( 'AdStuffModel' );
        $stuffid_id = null;
        if ($campaignid != 0) {
            $stuffid_id = $this->AdStuffModel->getOne ( array (
                    'campaignid' => $campaignid 
            ), 'stuffid' );
        }
        if (! empty ( $stuffid_id )) {
            $this->load->model ( 'AdStuffTextModel' );
            $db_thumb_location = $this->AdStuffTextModel->getOne ( array (
                    'stuffid' => $stuffid_id 
            ), 'path' );
        }
        // 编辑时候查询上个version的图片地址
        
        if ($bool_baseurl) {
            if ($db_thumb_location != null) {
                return $this->config->item ( 'base_url' ) . 'public/upload' . $db_thumb_location;
            } else {
                return "";
            }
        } else {
            return $db_thumb_location;
        }
    }
    
    /*
     * 媒体游戏管理
     */
    public function appManage() {
        $search = $this->input->get ( 'search' ) ? $this->input->get ( 'search' ) : '';
        $stype = $this->input->get ( 'stype' ) ? intval ( $this->input->get ( 'stype' ) ) : 1;
        
        $is_ccplay = $this->input->get ( 'is_ccplay' );
        
        $p = trim ( $this->input->get ( 'per_page' ) );
        $ostypeid = 1;
        $devicetypeid = 0;
        if (! $p) {
            $p = 1;
        }
        $config ['per_page'] = 20;
        // 获取游戏列表
        $this->load->model ( "AppTypeModel" );
        $this->load->model ( "UserMemberModel" );
        $data = $users = $datas = array ();
        $data ['ostypeid'] = $ostypeid;
        $data ['devicetypeid'] = 0;
        // 获取游戏条数
        $where = " ostypeid=" . $ostypeid . " and devicetypeid=" . $devicetypeid;
        
        if ($stype == 1) {
            if (! empty ( $search )) {
                $data ['appname'] = array (
                        'LIKE',
                        "%" . $search . "%" 
                );
                $where = $where . " and  appname like '%$search%'";
            }
        } else {
            if (! empty ( $search )) {
                $datas ['username'] = array (
                        'LIKE',
                        "%" . $search . "%" 
                );
            }
            $info = $this->UserMemberModel->getList ( $datas, 'userid' );
            
            while ( list ( $k, $v ) = each ( $info ) ) {
                $users [] = $v ['userid'];
            }
        }
        
        if ($is_ccplay != '') {
            $data ['is_ccplay'] = $is_ccplay;
            $where .= ' and is_ccplay=' . $is_ccplay;
        } else {
            $is_ccplay = 0;
        }
        
        if (count ( $users )) {
            $data ['userid'] = array (
                    "in",
                    implode ( ",", $users ) 
            );
            $where .= " and userid in ({$data['userid'][1]}) ";
        }
        $this->AppInfoModel->setStrWhere ( $where );
        $count = $this->AppInfoModel->getCount ();
        //
        $applist = $this->AppInfoModel->getList ( $data, '*', "appid desc", "", $p, $config ['per_page'] );
        $apptype = $this->AppTypeModel->getList ();
        $app = array ();
        foreach ( $apptype as $val ) {
            $app [$val ['apptypeid']] = $val ['typename'];
        }
        if ($count) {
            foreach ( $applist as $k => $v ) {
                $info = $this->UserMemberModel->getRow ( array (
                        "userid" => $v ['userid'] 
                ) );
                $applist [$k] ['realname'] = $info ['realname'];
                $applist [$k] ['username'] = $info ['username'];
                $applist [$k] ['appchildtypeid'] = explode ( ',', trim ( $v ['appchildtypeid'] ) );
            }
        }
        $this->load->library ( 'page' );
        
        $current_page = $p == 0 ? 1 : $p;
        $var = 'ostypeid=' . $ostypeid . '&search=' . $search . '&stype=' . $stype . '&is_ccplay=' . $is_ccplay;
        $page = $this->page->page_show ( 'appManage?' . $var, $count, $config ['per_page'] );
        // print_r($list);
        $this->smarty->assign ( 'applist', $applist );
        $this->smarty->assign ( 'app', $app );
        $this->smarty->assign ( 'search', $search );
        $this->smarty->assign ( 'page', $page );
        $this->smarty->assign ( 'per_page', $config ['per_page'] );
        $this->smarty->assign ( 'count', $count );
        $this->smarty->assign ( 'stype', $stype );
        $this->smarty->assign ( 'is_ccplay', $is_ccplay );
        $this->load->display ( 'admin/singleSDKmanage/appManage.html' );
    }
    /**
     * ccplay公告开关
     */
    public function changeCCplayAnnounce() {
        $data = array ();
        $is_ccplay_announce = ( int ) $this->input->get ( 'is_ccplay_announce' );
        $data ['is_ccplay_announce'] = $is_ccplay_announce;
        $this->appPropertyChange ( $this->input->get ( 'appid' ), $data );
    }
    
    /**
     * 修改ccplay是否开启礼包
     */
    public function changeGiftInCCPlay() {
        $is_ccplay_gift = ( int ) $this->input->get ( 'is_ccplay_gift' );
        $data ['is_ccplay_gift'] = $is_ccplay_gift;
        $this->appPropertyChange ( $this->input->get ( 'appid' ), $data );
    }
    
    /**
     * app属性修改方法
     * 
     * @param unknown $appid            
     * @param array $fields            
     * @author yangsen
     */
    protected function appPropertyChange($appid, array $fields, $resp=true) {
        $where = array (
                "appid" => ( int ) $appid 
        );
        $this->load->model ( 'AppInfoModel' );
        $status = $this->AppInfoModel->edit ( $where, $fields );
        if ((! empty ( $fields )) && $status) {
             $this->AppInfoModel->setDec ( $where, 'isupdate' );
        }
        
        if($resp) {
            if($status!=-1) {
                ajaxReturn ( '修改成功', 1, '' );
            } else {
                ajaxReturn ( '修改失败', 0, '' );
            }
        } else {
            return $status;
        }
    }
    
    /**
     * ccplay 总开关
     */
    public function switchCCplay() {
        $is_ccplay = ( int ) $this->input->get ( 'is_ccplay' );
        $data ['is_ccplay'] = $is_ccplay;
        
        $appid = $this->input->get ( 'appid' );
        
        $status = $this->appPropertyChange ( $appid, $data,false );
        
        if($status!=-1) {
            $this->load->model('ccplay/CcplayBiz');
            $this->CcplayBiz->flushCCplayPIDFileByAppId($appid);
            ajaxReturn ( '修改成功', 1, '' );
        } else {
            ajaxReturn ( '修改失败', 0, '' );
        }
    }
    
    /**
     * ccplay 换量开关
     */
    public function switchCCplayXAPP() {
        $is_ccplay_xapp = $this->input->get ( 'is_ccplay_xapp' );
        $data ['is_ccplay_xapp'] = $is_ccplay_xapp;
        $this->appPropertyChange ( $this->input->get ( 'appid' ), $data );
    }
    
    /**
     * ccplay 客服开关
     */
    public function switchCCplayKefu() {
        $is_ccplay_xapp = $this->input->get ( 'is_ccplay_kefu' );
        $data ['is_ccplay_kefu'] = $is_ccplay_xapp;
        $this->appPropertyChange ( $this->input->get ( 'appid' ), $data );
    }
    
    /**
     * ccplay 攻略开关
     */
    public function switchCCplayGonglue() {
        $is_ccplay_xapp = $this->input->get ( 'is_ccplay_gonglue' );
        $data ['is_ccplay_gonglue'] = $is_ccplay_xapp;
        $this->appPropertyChange ( $this->input->get ( 'appid' ), $data );
    }
    
    /**
     * ccplay强制开关
     */
    function switchCCPlayAutopop() {
        $ccplay_conf_autopop = $this->input->get('is_ccplay_autopop');
        $appid = $this->input->get('appid');
        $this->load->model('AppChannelConfigModel');
        $retCode = $this->AppChannelConfigModel->add(array('appid'=>$appid,'configname'=>'ccplay_autopop','publisherID'=>0,'configvalue'=>$ccplay_conf_autopop),true);
        
        if($retCode!=-1) {
            $this->load->model('ccplay/CcplayBiz');
            $this->CcplayBiz->flushCCplayPIDFileByAppId($appid);
              ajaxReturn ( '修改成功', 1, '' );
        } else {
            ajaxReturn ( '修改失败', -1, '' );
        }
    }
    
    
    /*
     * 分渠道游戏公告
     */
    public function gamePublicListPage($appid = 0) {
        $this->load->model ( 'AdChannelPublicModel' );
        $this->smarty->assign ( 'today', date ( 'Y-m-d' ) );
        // form field
        if ($appid == 0) {
            $appid = $this->input->get ( 'appid' ) ? $this->input->get ( 'appid' ) : 0;
        }
        if (empty ( $appid )) {
            show_error ( "请在\"媒体游戏管理\"界面下跳转渠道公告管理页面", 301, "操作流程调整" );
        }
        $this->assign ( 'appid', $appid );
        $keyword = $this->input->get ( 'keyword' );
        $search_title = $this->input->get ( 'search_title' ) ? $this->input->get ( 'search_title' ) : '';
        
        $publish_id = $this->input->get ( 'publisherID' ) ? $this->input->get ( 'publisherID' ) : '';
        
        $dateCond = $this->getDateRangeByControl ();
        
        // $start_date = $this->input->get('startDate')?$this->input->get('startDate'):'';
        // $end_date = $this->input->get('endDate')?$this->input->get('endDate'):'';
        
        // order by
        $order_field = $this->input->get ( 'order_field' ) ? $this->input->get ( 'order_field' ) : 'publish_time';
        $order_direct = $this->input->get ( 'order_direct' ) ? $this->input->get ( 'order_direct' ) : 'desc';
        
        $this->smarty->assign ( 'orderField', $order_field );
        $this->smarty->assign ( 'orderDirect', $order_direct );
        
        $appname = $this->AppInfoModel->getOne ( array (
                'appid' => $appid 
        ), 'appname' );
        $this->assign ( 'appname', $appname );
        // $gamelist = $this->AppInfoModel->getList($where_app, 'appid, appname');
        $p = trim ( $this->input->get ( 'per_page' ) );
        if (! $p) {
            $p = 1;
        }
        $where = 'status != 0 and status !=6';
        $data ['status'] = array (
                'NOT IN',
                array (
                        '0',
                        '6' 
                ) 
        );
        if (! empty ( $appid )) {
            $where .= " and appid=$appid";
            $data ['appid'] = $appid;
        }
        
        if (! empty ( $dateCond )) {
            
            $data ['publish_time'] = array (
                    array (
                            'EGT',
                            strtotime ( $dateCond ['sDate'] ) 
                    ) 
            );
            
            $where .= " and `publish_time`>=" . strtotime ( $dateCond ['sDate'] ) . "  ";
        }
        
        if (! empty ( $dateCond )) {
            if (isset ( $data ['publish_time'] ) && is_array ( $data ['publish_time'] )) {
                $data ['publish_time'] [] = array (
                        'LT',
                        strtotime ( $dateCond ['eDate'] ) 
                );
            } else {
                $data ['publish_time'] = array (
                        'LT',
                        strtotime ( $dateCond ['eDate'] ) 
                );
            }
            
            $where .= " and `publish_time`<" . strtotime ( $dateCond ['eDate'] ) . " ";
        }
        
        //test
        
        if ($search_title != null && $search_title != '') {
            $data ['title'] = array (
                    'LIKE',
                    '%' . $search_title . '%' 
            );
            $where .= " and (title like '%$search_title%') ";
        }
        
        if (! empty ( $keyword )) {
            $data ['announcement'] = array (
                    'LIKE',
                    '%' . $keyword . '%' 
            );
            $where .= " and (announcement like '%$keyword%')";
        }
        
        $config ['per_page'] = 20;
        // 获取表数据
        $this->load->model ( 'AppChannelModel' );
        $public_data = $this->AdChannelPublicModel->getList ( $data, '*', "dingorder asc, {$order_field} {$order_direct}", "", $p, $config ['per_page'] );
        $this->AdChannelPublicModel->setStrWhere ( $where );
        $count = $this->AdChannelPublicModel->getCount ();
        $this->load->library ( 'page' );
        
        $current_page = $p == 0 ? 1 : $p;
        $var = 'appid=' . $appid;
        $page = $this->page->page_show ( '/admin/singleSDKmanage/gamePublicListPage?' . $var, $count, $config ['per_page'] );
        // 处理显示数据
        $appid_arr = $channel_id_arr = array ();
        foreach ( $public_data as $key => $item ) {
            $appid_arr [] = $item ['appid'];
            $channel_tmp = explode ( ',', $item ['publisherID'] );
            foreach ( $channel_tmp as $key => $channelid ) {
                $channel_id_arr [] = $channelid;
            }
        }
        array_unique ( $appid_arr );
        array_unique ( $channel_id_arr );
        
        // channel相关信息转换
        $channel_where ['publisherID'] = array (
                'IN',
                $channel_id_arr 
        );
        $channellist = $this->AppChannelModel->getList ( $channel_where, 'publisherID, channelname' );
        $channel_tmp = array ();
        foreach ( $channellist as $key => $item ) {
            $channel_tmp [$item ['publisherID']] = $item;
        }
        $channellist = $channel_tmp;
        foreach ( $public_data as $key => &$item ) {
            $publisherID_arr = explode ( ',', $item ['publisherID'] );
            $publisherID_tmp = array ();
            foreach ( $publisherID_arr as $key => $value ) {
                $publisherID_tmp [] = $channellist [$value] ['channelname'];
            }
            $publisherID_str = implode ( ',', $publisherID_tmp );
            $item ['channelname'] = $publisherID_str;
            $item ['thumb'] = $this->_getAnnounceImgByCampaignId ( $item ['campaignid'] );
            $callTypeAndUrlResult = $this->_getCallTypeAndCallURIByCampaignId ( $item ['campaignid'] );
            $item = array_merge ( $item, $callTypeAndUrlResult );
            
            $item ['publishtime'] = date ( "Y-m-d H:i:s", $item ['publish_time'] );
            if ($item ['publish_time'] < time () && $item ['status'] == 1) {
                $this->AdChannelPublicModel->edit ( array (
                        'id' => $item ['id'] 
                ), array (
                        'status' => 4 
                ) );
                $item ['status'] = 4;
            } else if ($item ['status'] == 4 && $item ['publish_time'] > time ()) {
                $this->AdChannelPublicModel->edit ( array (
                        'id' => $item ['id'] 
                ), array (
                        'status' => 1 
                ) );
                $item ['status'] = 1;
            }
            switch ($item ['status']) {
                case '1' :
                    $status = '<span class="label">待审核</span>';
                    $operate = '<a class="btn btn-primary" href="showPublicContent?id=' . $item ['id'] . '">查看</a>';
                    break;
                case '2' :
                    $status = '<span class="label label-inverse">审核未通过</span>';
                    $operate = '<a class="btn btn-primary" href="editPublicContentPage?id=' . $item ['id'] . '"><i class="icon-edit icon-white"></i>编辑</a>&nbsp;|&nbsp;<a class="btn btn-danger" href="delChannelPublic?id=' . $item ['id'] . '&campaignid=' . $item ['campaignid'] . '" onclick="return confirm(\'是否要删除\');"><i class="icon-white  icon-minus-sign"></i>删除</a>';
                    break;
                case '3' :
                    $status = '<span class="label label-success">已发布</span>';
                    $operate = '<a class="btn btn-primary" href="showPublicContent?id=' . $item ['id'] . '">查看</a>&nbsp;|&nbsp;<a class="btn" href="stopChannelPublic?id=' . $item ['id'] . '&campaignid=' . $item ['campaignid'] . '">停用</a>';
                    break;
                case '4' :
                    $status = '<span class="label label-warning">过期自动驳回</span>';
                    $operate = '<a class="btn btn-primary" href="editPublicContentPage?id=' . $item ['id'] . '"><i class="icon-edit icon-white"></i>编辑</a>&nbsp;|&nbsp;<a class="btn btn-danger" href="delChannelPublic?id=' . $item ['id'] . '&campaignid=' . $item ['campaignid'] . '" onclick="return confirm(\'是否要删除\');"><i class="icon-white  icon-minus-sign"></i>删除</a>';
                    break;
                case '5' :
                    $status = '<span class="label label-important">已停用</span>';
                    $operate = '<a class="btn btn-primary" href="showPublicContent?id=' . $item ['id'] . '">查看</a>';
                    break;
                default :
                    $status = '<span class="label">未知</span>';
                    $operate = '<a href="#">无</a>';
                    break;
            }
            $item ['status'] = $status;
            $item ['operate'] = $operate;
        }
        
        // print_r($channellist);
        $this->smarty->assign ( 'page', $page );
        $this->smarty->assign ( 'per_page', $config ['per_page'] );
        $this->smarty->assign ( 'count', $count );
        $this->smarty->assign ( 'appid', $appid );
        $this->smarty->assign ( 'appname', $appname );
        // $this->smarty->assign('gamelist', $gamelist);
        $this->smarty->assign ( 'publicdata', $public_data );
        
        $this->load->display ( 'admin/singleSDKmanage/gamePublicList.html' );
    }
    
    /**
     * 添加分渠道游戏公告 VIEW
     */
    public function addChannelPublicPage() {
        $cur_appid = $this->input->get ( 'appid' );
        if (empty ( $cur_appid )) {
            show_error ( "参数错误，请选择从游戏管理进入管理公告" );
        } else {
            $this->assign ( 'appid', $cur_appid );
            $appname = $this->AppInfoModel->getOne ( array (
                    'appid' => $cur_appid 
            ), 'appname' );
            $this->assign ( 'appname', $appname );
        }
        $login_uname = $this->session->userdata ['admin'] ['admin_name'];
        // 媒体游戏数据
        $where_app = array (
                'is_ccplay' => 1 
        );
         
        // 获取渠道列表
        $this->load->model ( 'AppChannelModel' );
        $channelinfo = $this->AppChannelModel->getList ( array (
                'appid' => $cur_appid
        ), 'publisherID, channelname' );
        
        /** edit by yangsen start **/ 
        $this->smarty->assign ('allchannel',$channelinfo);
        /** edit by yangsen end **/ 
        
        $this->load->display ( 'admin/singleSDKmanage/addChannelPublic.html' );
    }

    private function pictypecheck() {
        
        if(!empty($_FILES['thumb']['name'])) {
            if($_FILES['thumb']['size']==0 || $_FILES['thumb']['size']/1024/1024>2) {
                $this->respToClient('只允许上传2mb以下的PNG、JPG图片');
            } else if($_FILES["thumb"]["type"]=='') {
                        $this->respToClient('无法获取文件类型，这张图片有问题，请使用专业制图工具或windows截图工具创作图片。');
             } 
            else if (!in_array($_FILES["thumb"]["type"],array("image/jpeg","image/jpg","image/pjpeg","image/x-png",'image/png',"image/x-png",'image/x-ms-bmp')))
            {
                $this->respToClient('只允许传图片PNG、JPG类型,这个文件的类型是获取到的是'.$_FILES["thumb"]["type"]);
            } 
        }
    }
    
    /**
     * 增加渠道公告
     */
    public function doAddChannelPublic($status = 1) {
        $appid = $this->input->post('appid');
        $title = trim($this->input->post('title', true));
        $content1 = rtrim(strip_tags($this->input->post('content1', true)));
        $public_time = trim($this->input->post('time'));
        $showstyle = $this->input->post('showstyle');
        $calltype = $this->input->post('calltype');
        $callurl = $this->input->post('callurl');
        //$callurl = htmlspecialchars($callurl);
        $isajax = $this->input->is_ajax_request();
        
        
        if (is_array ( $callurl )) {
            $callurl = $callurl [0];
        }
        
        if (empty ( $title )) {
            $this->respToClient('标题不能为空');
        }
        
        if( empty ( $public_time )) {
            $public_time = time();
        }
        
        if(WeiboLength($title)>18) {
            $this->respToClient('标题不能多于18个字符');
        }
        
        if($content1!=null && WeiboLength($content1)>250) {
            $this->respToClient('内容不能多于250个字符');
        }
        
        $r = $this->AdChannelPublicModel->getRow(array('appid'=>$appid,'title'=>$title,'status'=>array('IN',array(3,5))));
        if(!empty($r)) {
            $this->respToClient('公告标题已经存在，请修改');
        }
        
        $publisher = $this->input->post('publisher');
        
        if(empty($publisher)) {
            $this->respToClient('渠道至少要选择一项');
        }
        
        $publisher_str = implode ( ',', $publisher );
        
        
        
        
        $htmlData = '';
        if (! empty ( $_POST ['content1'] )) {
            if (get_magic_quotes_gpc ()) {
                $htmlData = stripslashes ( $_POST ['content1'] );
            } else {
                $htmlData = $_POST ['content1'];
            }
        }
        $content = htmlspecialchars ( $htmlData );
        $time = time ();
        $public_time = strtotime ( $public_time );
        
        
        $this->pictypecheck();
        
        // 设置缩略图
        $gonggao_config = $this->config->item ( 'gonggao' );
        $uploadResp = array ();
        
        if (isset($_FILES ['thumb']) && $_FILES ['thumb'] != null && $_FILES ['thumb'] ['name'] != null) {
            $uploadResp = $this->_uploadAnnouncementThumb ( 'thumb', $gonggao_config );
            if(!$uploadResp['isok']) {
                $msg = is_array($uploadResp['msg'])?$uploadResp['msg'][0]:$uploadResp['msg'];
                $this->respToClient($msg);
            }
        }
        
        // 要插入数据的表
        $this->load->model ( 'AdCampaignModel' );
        $this->load->model ( 'AdGroupModel' );
        $this->load->model ( 'AdStuffModel' );
        $this->load->model ( 'AdStuffTextModel' );
        
        $userid = $this->AppInfoModel->getOne ( array (
                'appid' => $appid 
        ), 'userid' );
        $campaign_data = array (
                'campaignname' => $title,
                'targettype' => '2',
                'periodset' => '0',
                'ostypeid' => 1,
                'osversionset' => 0,
                'aosvset' => '',
                'createtime' => $time,
                'status' => 2,
                'selfappset' => 1,
                'userid' => $userid 
        );
        $campaign_id = $this->AdCampaignModel->add ( $campaign_data );
        
        $group_data = array (
                'campaignid' => $campaign_id,
                'adgroupname' => $title,
                'adform' => '9',
                'chargemode' => 1,
                'adtargettype' => 0,
                'selfappset' => 1,
                'is_ccplay_announce' => 1,
                'starttime' => strtotime ( date ( 'Y-m-d', $public_time ) ),
                'endtime' => (strtotime ( date ( 'Y-m-d', $public_time ) ) + 60 * 60 * 24 * 365),
                'userid' => $userid,
                'createtime' => $time,
                'lastupdatetime' => $time,
                'istracktype' => 2,
                'status' => 1,
                'effecttype' => $calltype 
        ) // 游戏内模块20 HTML 21
;
        $group_id = $this->AdGroupModel->add ( $group_data );
        $stuff_data = array (
                'adgroupid' => $group_id,
                'campaignid' => $campaign_id,
                'adstuffname' => $title,
                'target' => $callurl,
                'stufftype' => 2,
                'lastupdatetime' => $time,
                'status' => 1 
        );
        $stuff_id = $this->AdStuffModel->add ( $stuff_data );
        
        // 设置缩略图
        $gonggao_config = $this->config->item ( 'gonggao' );
        
        $stuff_text_data = array (
                'stuffid' => $stuff_id,
                'adtitle' => $title,
                'adcontent' => $content1,
                'path' => isset ( $uploadResp ['url'] ) ? $uploadResp ['url'] : null 
        );
        $this->AdStuffTextModel->add ( $stuff_text_data );
        
        $author = $this->input->post ( 'author' );
        $ding = $this->input->post ( 'dingorder' );
        if ($ding == 0) {
            $ding = 99;
        }
        
        if($showstyle=='2') {
            if($stuff_text_data['path']==null) {
                $showstyle ='3';
            }
        }
        
        $channel_public_data = array (
                'appid' => $appid,
                'campaignid' => $campaign_id,
                'publisherID' => $publisher_str,
                'title' => $title,
                'announcement' => $content1,
                'publish_time' => $public_time,
                'time' => $time,
                'author' => $author,
                'dingorder' => $ding,
                'status' => $status,
                'showstyle' => $showstyle,
                'picsmallsize' =>isset($uploadResp['height'])?$uploadResp['width'].'x'.$uploadResp['height']:null
        );
        $this->AdChannelPublicModel->add ( $channel_public_data );
        if($isajax) {
            ajaxReturn('添加成功', 1,array('gotourl'=>'gamePublicListPage?appid='.$appid));
        } else {
            redirect('gamePublicListPage?appid='.$appid);
        }
        //$this->gamePublicListPage ( $appid );
    }
    // 修改
    public function editPublicContentPage() {
        $id = trim ( $this->input->get ( 'id' ) );
        $channelpublic = $this->AdChannelPublicModel->getRow ( array (
                'id' => $id 
        ) );
        $channelpublic ['publish_time'] = date ( "Y-m-d H:i:s", $channelpublic ['publish_time'] );
        // 媒体游戏数据
        $where_app = array (
                'is_ccplay' => 1 
        );
        $applist = $this->AppInfoModel->getList ( $where_app, 'appid, appname' );
        $appid_arr = array ();
        foreach ( $applist as $key => $value ) {
            array_push ( $appid_arr, $value ['appid'] );
        }
        // 获取渠道列表
        $this->load->model ( 'AppChannelModel' );
        $gamechannel = array ();
        if (! empty ( $appid_arr )) {
            foreach ( $appid_arr as $key => $appid ) {
                $channelinfo = $this->AppChannelModel->getList ( array (
                        'appid' => $appid 
                ), 'publisherID, channelname' );
                $gamechannel [$appid] = $channelinfo;
            }
        }
        
        $thumb = $this->_getAnnounceImgByCampaignId ( $channelpublic ['campaignid'] );
        // calltype callurl FIXME calltype 和callurl
        $calltypeResult = $this->_getCallTypeAndCallURIByCampaignId ( $channelpublic ['campaignid'] );
        $channelpublic = array_merge ( $channelpublic, $calltypeResult );
        $channelpublic['callurl'] = htmlspecialchars($channelpublic['callurl']);
        $selectedChannels = $channelpublic['publisherID'];
        $selectedChannels = explode(',', $selectedChannels);
        
        $this->smarty->assign ( 'applist', $applist );
        $this->smarty->assign ('allchannel',$gamechannel[$channelpublic ['appid']]);
        $this->assign('selectedchannel', $selectedChannels);
        
        
        $gamechannel = json_encode ( $gamechannel ); //history used
        $this->smarty->assign ( 'channel', (array)$gamechannel );//history used.
        $channelpublic ['thumb'] = $thumb;
        $this->smarty->assign ( 'firstappid', $channelpublic ['appid'] );//may not useful.
        $this->smarty->assign ( 'channelpublic', $channelpublic );
        
        $this->load->display ( 'admin/singleSDKmanage/editGamePublic.html' );
    }
    
    /**
     * 根据campaignid返回calltype
     * 
     * @param unknown $campaignid            
     * @return NULL
     */
    protected function _getCallTypeAndCallURIByCampaignId($campaignid) {
        $this->load->model ( 'AdGroupModel' );
        $this->load->model ( 'AdStuffModel' );
        $ret = array ();
        if ($campaignid != 0) {
            $ret ['calltype'] = $this->AdGroupModel->getOne ( array (
                    'campaignid' => $campaignid 
            ), 'effecttype' );
            $ret ['callurl'] = $this->AdStuffModel->getOne ( array (
                    'campaignid' => $campaignid 
            ), 'target' );
            return $ret;
        }
        
        return null;
    }
    
    /**
     * 编辑
     */
    public function doEditPublicContent($status = 1) {
        $appid = $this->input->post ( 'appid' );
        $title = trim ( $this->input->post ( 'title' ,true) );
        $content1 = trim ( strip_tags($this->input->post ( 'content1',true )));
        $public_time = trim ( $this->input->post ( 'time' ) );
        $id = trim ( $this->input->post ( 'chaid' ) );
        $campaign_id = trim ( $this->input->post ( 'campaignid' ) );
        $calltype = $this->input->post ( 'calltype' );
        $callurl = $this->input->post ( 'callurl' );
        //$callurl = htmlspecialchars($callurl);
        if (is_array ( $callurl )) {
            $callurl = $callurl [0];
        }
        $showstyle = $this->input->post ( 'showstyle' );
        $delpic = $this->input->post ( 'delpic' );
        $publisher = $this->input->post('publisher');
        $old_campaign_id = $campaign_id;
        
        $isajax = $this->input->is_ajax_request();
        if (empty ( $title ) ) {
            $this->respToClient('标题不能为空');
        }
        
        if(empty ( $public_time )) {
            $public_time = time();
        }
        
        if(WeiboLength($title)>18) {
            $this->respToClient('标题不能多于18个字符');
        }
        
        
        if($content1!=null && WeiboLength($content1)>250) {
            $this->respToClient('内容不能多于250个字符');
        }
        
        $r = $this->AdChannelPublicModel->getList(array('appid'=>$appid,'title'=>$title,'status'=>array('IN',array(3,5))));
        
        foreach($r as $check) {
            if($check['id']!=$id && $check['title']==$title) {
                $this->respToClient('公告标题已经存在，请修改');
            }
        }
        
        if(empty ( $publisher )) {
            $this->respToClient('发布渠道 参数不能为空');
        }
        
        $old_picheight = $this->AdChannelPublicModel->getOne(array('id'=>$id),'picsmallsize');
        
        
        
        
        
        $publisher_str = implode ( ',', $publisher );
        $htmlData = '';
        if (! empty ( $_POST ['content1'] )) {
            if (get_magic_quotes_gpc ()) {
                $htmlData = stripslashes ( $_POST ['content1'] );
            } else {
                $htmlData = $_POST ['content1'];
            }
        }
        $content = htmlspecialchars ( $htmlData );
        $time = time ();
        $public_time = strtotime ( $public_time );
        
        
        
        if (empty ( $_FILES ) || $_FILES ['thumb'] ['name'] == null) {
            $thumb = $this->_getAnnounceImgByCampaignId ( $old_campaign_id, false );
            $thumb_url = $thumb;
            $thumb_s_size = $old_picheight;
        } else {
            $this->pictypecheck();
            $thumb = $this->_uploadAnnouncementThumb ( 'thumb', $this->config->item ( 'gonggao' ) );
            if(!$thumb['isok']) {
                $msg = is_array($thumb['msg'])?$thumb['msg'][0]:$thumb['msg'];
                $this->respToClient($msg);
            } else {
                $thumb_url = $thumb ['url'];
                $thumb_s_size = isset($thumb['height'])?$thumb['width'].'x'.$thumb['height']:null;
            }
        }
        
        
        
        
        
        
        // 要修改数据的表
        $this->load->model ( 'AdCampaignModel' );
        $this->load->model ( 'AdGroupModel' );
        $this->load->model ( 'AdStuffModel' );
        $this->load->model ( 'AdStuffTextModel' );
        
        $userid = $this->AppInfoModel->getOne ( array (
                'appid' => $appid 
        ), 'userid' );
        $campaign_data_old = array (
                'status' => 3,
                'lastupdatetime' => $time 
        );
        $this->AdCampaignModel->edit ( array (
                'campaignid' => $campaign_id 
        ), $campaign_data_old );
       
        $campaign_data = array (
                'campaignname' => $title,
                'targettype' => '2',
                'periodset' => '0',
                'ostypeid' => 1,
                'osversionset' => 0,
                'aosvset' => '',
                'createtime' => $time,
                'status' => 1,
                'selfappset' => 1,
                'userid' => $userid 
        );
        $campaign_id = $this->AdCampaignModel->add ( $campaign_data );
        
        $group_data = array (
                'campaignid' => $campaign_id,
                'adgroupname' => $title,
                'adform' => '9',
                'chargemode' => 1,
                'adtargettype' => 0,
                'selfappset' => 1,
                'is_ccplay_announce' => 1,
                'starttime' => strtotime ( date ( 'Y-m-d', $public_time ) ),
                'endtime' => (strtotime ( date ( 'Y-m-d', $public_time ) ) + 60 * 60 * 24 * 365),
                'userid' => $userid,
                'createtime' => $time,
                'lastupdatetime' => $time,
                'istracktype' => 2,
                'status' => 1,
                'effecttype' => $calltype 
        ) // 游戏内模块20 HTML 21
;
        $group_id = $this->AdGroupModel->add ( $group_data );
        $stuff_data = array (
                'adgroupid' => $group_id,
                'campaignid' => $campaign_id,
                'adstuffname' => $title,
                'target' => $callurl,
                'stufftype' => 2,
                'lastupdatetime' => $time,
                'status' => 1 
        );
        $stuff_id = $this->AdStuffModel->add ( $stuff_data );
        
        if ('1' == $delpic) {
            $thumb_url = '';
            $thumb_s_size= null;
        }
        
        $stuff_text_data = array (
                'stuffid' => $stuff_id,
                'adtitle' => $title,
                'adcontent' => $content1,
                'path' => $thumb_url 
        );
        $this->AdStuffTextModel->add ( $stuff_text_data );
        $channel_public_data_old = array (
                'status' => 6,
                'lastupdatetime' => $time 
        );
        $this->AdChannelPublicModel->edit ( array (
                'id' => $id 
        ), $channel_public_data_old );
        
        $author = $this->input->post ( 'author' );
        $ding = $this->input->post ( 'dingorder' );
        
        if ($ding == 0) {
            $ding = 99;
        }
        
        if($showstyle=='2') {
            if($stuff_text_data['path']=='') {
                $showstyle ='3';
            }
        }
        
        $channel_public_data = array (
                'appid' => $appid,
                'campaignid' => $campaign_id,
                'publisherID' => $publisher_str,
                'title' => $title,
                'announcement' => $content1,
                'status' => $status,
                'publish_time' => $public_time,
                'time' => $time,
                'author' => $author,
                'dingorder' => $ding,
                'lastupdatetime' => $time,
                'showstyle' => $showstyle,
                'picsmallsize'=> $thumb_s_size
        );
        $lastInsertId = $this->AdChannelPublicModel->add ( $channel_public_data );
        // 编辑代码
        /*
         * $campaign_data = array(
         * 'campaignname' => $title,
         * 'lastupdatetime' => $time,
         * 'status' => 1,
         * 'userid' => $userid
         * );
         * $this->AdCampaignModel->edit(array('campaignid' => $campaign_id), $campaign_data);
         *
         * $group_data = array(
         * 'adgroupname' => $title,
         * 'starttime' => strtotime(date('Y-m-d', $public_time)),
         * 'endtime' => (strtotime(date('Y-m-d', $public_time)) + 60 * 60 * 24 * 365),
         * 'userid' => $userid,
         * 'lastupdatetime' => $time
         * );
         * $this->AdGroupModel->edit(array('campaignid' => $campaign_id), $group_data);
         * $stuff_data = array(
         * 'adstuffname' => $title,
         * 'lastupdatetime' => $time
         * );
         * $this->AdStuffModel->edit(array('campaignid', $campaign_id), $stuff_data);
         * $stuff_id = $this->AdStuffModel->getOne(array('campaignid' => $campaign_id), 'stuffid');
         * $stuff_text_data = array(
         * 'adtitle' => $title,
         * 'adcontent' => $content1
         * );
         * $this->AdStuffTextModel->edit(array('stuffid' => $stuff_id), $stuff_text_data);
         *
         * $channel_public_data = array(
         * 'appid' => $appid,
         * 'publisherID' => $publisher_str,
         * 'title' => $title,
         * 'announcement' => $content1,
         * 'status' => 1,
         * 'publish_time' => $public_time,
         * 'lastupdatetime' => $time
         * );
         * $this->AdChannelPublicModel->edit(array('id' => $id), $channel_public_data);
         */
        //redirect ( 'showPublicContent?id=' . $lastInsertId );
        if($isajax) {
            ajaxReturn('修改成功', 1,array('gotourl'=>'gamePublicListPage?appid='.$appid));
        } else {
            redirect('gamePublicListPage?appid='.$appid);
        }
    }
    // 详情页展示
    public function showPublicContent() {
        $statusmap = array (
                0 => '已删除',
                1 => '待审核',
                2 => '审核未通过 ',
                3 => '已发布',
                4 => '过期自动驳回',
                5 => '已停用',
                6 => '编辑' 
        );
        
        $id = trim ( $this->input->get ( 'id' ) );
        if (empty ( $id ))
            exit ( '缺少参数' );
        $channeldata = $this->AdChannelPublicModel->getRow ( array (
                'id' => $id 
        ) );
        
        // 读取缩略图
        $this->load->model ( 'AppChannelModel' );
        $appname = $this->AppInfoModel->getOne ( array (
                'appid' => $channeldata ['appid'] 
        ), 'appname' );
        $channeldata ['appname'] = $appname;
        $channel = explode ( ',', $channeldata ['publisherID'] );
        $channel_name_arr = $this->AppChannelModel->getList ( array (
                'publisherID' => array (
                        'IN',
                        $channel 
                ) 
        ), 'channelname' );
        $channel_name = array ();
        foreach ( $channel_name_arr as $key => $value ) {
            $channel_name [] = $value ['channelname'];
        }
        $channel_name = implode ( '、', $channel_name );
        $channeldata ['channelname'] = $channel_name;
        $channeldata ['publish_time'] = date ( 'Y-m-d H:i:s', $channeldata ['publish_time'] );
        $channeldata ['verify'] = false;
        $channeldata ['thumb'] = $this->_getAnnounceImgByCampaignId ( $channeldata ['campaignid'] );
        $channeldata ['statusname'] = $statusmap [$channeldata ['status']];
        $channeldata ['announcement'] = $channeldata['announcement'];
        
        $retCallTypeAndURI = $this->_getCallTypeAndCallURIByCampaignId($channeldata ['campaignid']);
        $channeldata = array_merge($channeldata,$retCallTypeAndURI);
        $channeldata [ 'callurl'] = htmlspecialchars(($channeldata['callurl']));
        
        if (empty ( $channeldata )) {
            exit ( '参数错误' );
        }
        $this->smarty->assign ( 'position', '分渠道游戏公告详情' );
        $this->smarty->assign ( 'channeldata', $channeldata );
        $this->load->display ( 'admin/singleSDKmanage/showPublicContent.html' );
    }
    
    // 启用
    public function enableChannelPublic() {
        $pageid = $this->input->get('pageid');
        if(!isset($pageid)) {
            $pageid = 1;
        }
        $id = trim ( $this->input->get ( 'id' ) );
        $this->load->model ( 'AdCampaignModel' );
        $this->load->model('AdGroupModel');
        $campaignid = trim ( $this->input->get ( 'campaignid' ) );
        $time = time ();
        $campaign_data = array (
                'status' => 2,
                'lastupdatetime' => $time 
        );
        $this->AdCampaignModel->edit ( array (
                'campaignid' => $campaignid 
        ), $campaign_data );
        
        $this->AdGroupModel->edit ( array (
                'campaignid' => $campaignid
        ), array('status'=>1,'lastupdatetime' => $time) );
        
        
        $channel_public_data = array (
                'status' => 3,
                'lastupdatetime' => $time 
        );
        $result = $this->AdChannelPublicModel->edit ( array (
                'id' => $id 
        ), $channel_public_data );
        
        if($this->input->is_ajax_request()) {
            ajaxReturn('修改成功', $result);
        } else {
            redirect('gamePublicListPage?appid='.$this->input->get ( 'appid' ).'&per_page='.$pageid);
        }
    }
    
    // 停用
    public function stopChannelPublic() {
        $pageid = $this->input->get('pageid');
        if(!isset($pageid)) {
            $pageid = 1;
        }
        $id = trim ( $this->input->get ( 'id' ) );
        $this->load->model ( 'AdCampaignModel' );
        $this->load->model('AdGroupModel');
        $campaignid = trim ( $this->input->get ( 'campaignid' ) );
        $time = time ();
        $campaign_data = array (
                'status' => 3,
                'lastupdatetime' => $time 
        );
        $this->AdCampaignModel->edit ( array (
                'campaignid' => $campaignid 
        ), $campaign_data );
        
        $this->AdGroupModel->edit ( array (
                'campaignid' => $campaignid
        ), array (
                'status' => 2,
                'lastupdatetime' => $time 
        ));
        
        $channel_public_data = array (
                'status' => 5,
                'lastupdatetime' => $time 
        );
        $result = $this->AdChannelPublicModel->edit ( array (
                'id' => $id 
        ), $channel_public_data );
        
        if($this->input->is_ajax_request()) {
            ajaxReturn('修改成功', $result);
        } else {
            redirect('gamePublicListPage?appid='.$this->input->get ( 'appid' ).'&per_page='.$pageid);
        }
    }
    // 删除
    public function delChannelPublic() {
        $id = trim ( $this->input->get ( 'id' ) );
        $this->load->model ( 'AdCampaignModel' );
        $campaignid = trim ( $this->input->get ( 'campaignid' ) );
        $time = time ();
        $campaign_data = array (
                'status' => 3,
                'lastupdatetime' => $time 
        );
        $this->AdCampaignModel->edit ( array (
                'campaignid' => $campaignid 
        ), $campaign_data );
        $channel_public_data = array (
                'status' => 0,
                'lastupdatetime' => $time 
        );
        $this->AdChannelPublicModel->edit ( array (
                'id' => $id 
        ), $channel_public_data );
        redirect('gamePublicListPage?appid='.$this->input->get ( 'appid' ));
    }
    /*
     * 广告游戏列表
     */
    public function adGameList() {
        // 添加逛游戏的链接地址
        $add_ad_url = site_url ( 'admin/singleSDKmanage/addAdGamePage' );
        //
        $p = trim ( $this->input->get ( 'per_page' ) );
        if (! $p) {
            $p = 1;
        }
        $this->load->library ( 'page' );
        $config ['per_page'] = 20;
        $current_page = $p == 0 ? 1 : $p;
        // $this->AdGameListModel->setStrWhere($where);
        $count = $this->AdGameListModel->getCount ();
        //
        $adList = $this->AdGameListModel->getList ( array (), '*', 'time desc', '', $p, $config ['per_page'] );
        $page = $this->page->page_show ( 'adGameList?', $count, $config ['per_page'] );
        //
        if (! empty ( $adList )) {
            foreach ( $adList as $key => &$value ) {
                $value ['path'] = site_url ( 'public/upload' . $value ['path'] );
                $value ['time'] = date ( "Y-m-d H:i:s", $value ['time'] );
            }
        }
        $this->smarty->assign ( 'page', $page );
        $this->smarty->assign ( 'per_page', $config ['per_page'] );
        $this->smarty->assign ( 'count', $count );
        $this->smarty->assign ( 'adList', $adList );
        $this->smarty->assign ( 'add_url', $add_ad_url );
        $this->load->display ( 'admin/singleSDKmanage/adGameList.html' );
    }
    public function addAdGamePage() {
        $this->load->display ( 'admin/singleSDKmanage/addAdGame.html' );
    }
    
    /**
     * 添加游戏
     */
    public function doAddGame() {
        
        $gameid = $this->input->post('id');
        $gamename = $this->input->post ( 'gamename' );
        $pkglist = $this->input->post('pkglist',true);
        $path_l = $this->input->post ( 'img_162' );
        $path = $this->input->post ( 'img_86' );
        $gamename = trim($gamename);
        $checkexists = $this->AdGameListModel->getOne(array('name'=>$gamename,'status'=>1),'id');
        
        if (empty ( $gamename )) { // || empty($adcontent)
            ajaxReturn( '游戏名称不能为空',-1 );
        }
        if (empty ( $path )) {
            ajaxReturn( 'APP图标不能为空',-1 );
        }
        
        
        if(empty($gameid)) {
            if($checkexists!=null) {
                ajaxReturn('游戏名称已经存在，不能重复', -1);
            }
            
            $adcontent = ''; // $this->input->post('adcontent');
            $time = time ();
            
            $gameList_data = array (
                    'path_l' => $path_l,
                    'path' => $path,
                    'name' => $gamename,
                    'adcontent' => $adcontent,
                    'time' => $time,
                    'pkglist'=>$pkglist
            );
           $retCode =  $this->AdGameListModel->add ( $gameList_data );
        } else {
            if(!empty($checkexists) && $checkexists!=$gameid) {
                ajaxReturn('游戏名称已经存在，不能重复', -1);
            }
            
            $gameList_data = array (
                    'name'=>$this->input->post ( 'gamename' ),
                    'pkglist'=>$pkglist,
                    'path_l' => $path_l,
                    'path' => $path
            );
            $retCode = $this->AdGameListModel->edit (array('id'=>$gameid), $gameList_data );
        }
        if($retCode>-1) {
            ajaxReturn('处理成功', 1);
        } else {
            ajaxReturn('处理失败', -1);
        }
        //redirect('adGameList');
    }
    
    
    public function changeStatus() {
        $adgameid = $this->input->post('adgameid');
    }
    
    
    
    /**
     * 修改游戏基本数据
     */
    public function editGamePage() {
    	$gameid =  $this->input->get('id');
        
        $time = time ();
        if (empty ( $gameid )) { // || empty($adcontent)
            exit ( '参数不能为空' );
        } else {
        	$detail = $this->AdGameListModel->getRow(array('id'=>$gameid));
        	$detail['viewpath'] = site_url('public/upload'.$detail['path']);
        	$detail['viewpath_l'] = site_url('public/upload'.$detail['path_l']);
        	$this->assign('id', $gameid);
        	$this->assign('detail',$detail);
        	$this->addAdGamePage();
        }
    }
    
    /**
     * 游戏icon上传
     */
    public function ajaxDoUploadStuff() {
        $returnArray = array (
                'isReceived' => false,
                'errorMessage' => '',
                'imgInfo' => '',
                'imgs_id' => null 
        );
        if ($_FILES ['Filedata'] ['size'] > 30 * 1024) {
            $returnArray ['errorMessage'] = '图片大小超过30kb';
            echo json_encode ( array (
                    $returnArray 
            ) );
            return;
        }
        $type = $this->input->get_post ( 'type' );
        $returnArray = $this->AdGameListModel->upload ( $type );
        
        if ($returnArray ['isReceived']) {
            $returnArray ['imgs_id'] = $type;
        }
        
        echo json_encode ( array (
                $returnArray 
        ) );
        return;
    }
    
    /*
     * 分渠道投放广告
     * duplicate for ccplay
     */
    public function putAdListCcplay() {
        $this->assign('tabindex', 3);
        $appid = $this->input->get ( 'appid' ) ? $this->input->get ( 'appid' ) : 0;
        $this->assign ( 'appid', $appid );
        $appname = $this->AppInfoModel->getOne ( array (
                'appid' => $appid
        ), 'appname' );
        $this->_checkSwitchStatus($appid);
        $this->assign('appname', $appname);
        
        $p = trim ( $this->input->get ( 'per_page' ) );
        if (! $p) {
            $p = 1;
        }
        $config ['per_page'] = $this->config->item('pagesize');

        // 渠道名称
        $publisher = $this->AdSubChannelModel->getpublisher ();
        $publisher_tmp = array ();
        foreach ( $publisher as $key => $value ) {
            $publisher_tmp [$value ['publisherID']] = $value;
        }
        $publisher = $publisher_tmp;
        
        $this->load->model('AppChannelModel');
        $q_channelcond = array('appid'=>$appid,'is_ccplay'=>1,/* 'is_ccplay_xapp'=>1 */);
        $show_data = $this->AppChannelModel->getList($q_channelcond);
        $count = $this->AppChannelModel->getCount($q_channelcond);
        $this->load->library ( 'page' );
    
        $current_page = $p == 0 ? 1 : $p;
        $var = 'appid=' . $appid;
        $page = $this->page->page_show ( '/admin/singleSDKmanage/putAdListCcplay?' . $var, $count, $config ['per_page'] );
        // 媒体游戏数据
        $where_app = array (
                'is_ccplay' => 1
        );
        //$gamelist = $this->AppInfoModel->getList ( $where_app, 'appid, appname' );
        
        // print_r($gamelist);
        $this->smarty->assign ( 'page', $page );
        $this->smarty->assign ( 'per_page', $config ['per_page'] );
        $this->smarty->assign ( 'count', $count );
        //$this->smarty->assign ( 'gamelist', $gamelist );
        $this->smarty->assign ( 'putList', $show_data );
        $this->load->display ( 'admin/singleSDKmanage/putAdListCcplay.html' );
    }
    
    
    
    
    
    
    /*
     * 分渠道投放广告
     * CCPLAY看putAdListCcplay方法
     */
    public function putAdList() {
        $this->assign('tabindex', 3);
        $opt_game = $this->input->get ( 'search' ) ? $this->input->get ( 'search' ) : 0;
        $this->assign ( 'appid', $opt_game );
        $data = array (
                'status' => 1 
        );
        $where = 'status = 1';
        if (! empty ( $opt_game )) {
            $data ['appid'] = $opt_game;
            $where .= " and appid = $opt_game";
        }
        $p = trim ( $this->input->get ( 'per_page' ) );
        if (! $p) {
            $p = 1;
        }
        $config ['per_page'] = 20 * 7;
        $this->AdSubChannelModel->setStrWhere ( $where );
        $count = $this->AdSubChannelModel->getCount ();
        $putList = $this->AdSubChannelModel->getList ( $data, '*', "id desc", "", $p, $config ['per_page'] );
        // 媒体名称
        $appName = $this->AdSubChannelModel->getAppName ();
        $appName_tmp = array ();
        foreach ( $appName as $key => $value ) {
            $appName_tmp [$value ['appid']] = $value;
        }
        $appName = $appName_tmp;
        // 渠道名称
        $publisher = $this->AdSubChannelModel->getpublisher ();
        $publisher_tmp = array ();
        foreach ( $publisher as $key => $value ) {
            $publisher_tmp [$value ['publisherID']] = $value;
        }
        $publisher = $publisher_tmp;
        // 游戏名称
        $gameId = $this->AdSubChannelModel->getgameId ();
        $gameid_arr = array ();
        foreach ( $gameId as $key => $Item ) {
            foreach ( $Item as $key => $value ) {
                array_push ( $gameid_arr, $value );
            }
        }
        $gameid_arr = array_unique ( $gameid_arr );
        
        $show_data = array ();
        if (! empty ( $gameid_arr )) {
            $gameName = $this->AdSubChannelModel->getgameName ( $gameid_arr );
            $gameName_tmp = array ();
            foreach ( $gameName as $key => $value ) {
                $gameName_tmp [$value ['id']] = $value;
            }
            $gameName = $gameName_tmp;
            // 重组列表
            $putList_tmp = array();
            foreach ( $putList as $key => $value ) {
                if (! isset ( $putList_tmp [$value ['publisherID']] )) {
                    $putList_tmp [$value ['publisherID']] = array ();
                }
                array_push ( $putList_tmp [$value ['publisherID']], $value );
            }
            $index = 0;
            foreach ( $putList_tmp as $key_item => $item ) {
                $show_data [$index] ['channelname'] = $publisher [$key_item] ['channelname'];
                $show_data [$index] ['publisherID'] = $key_item;
                foreach ( $item as $key => $value ) {
                    
                    $show_data [$index] ['appname'] = $appName [$value ['appid']] ['appname'];
                    if(isset($gameName [$value ['adgameid']] ['name'])) {
                        $show_data [$index] ['adgamename_' . (6 - $key)] =$gameName [$value ['adgameid']] ['name'];
                    } else {
                        $show_data [$index] ['adgamename_' . (6 - $key)] = '';
                    }
                    $item_time = $value ['time'];
                }
                $show_data [$index] ['time'] = date ( "Y-m-d H:i:s", $item_time );
                $index ++;
            }
        }
        $this->load->library ( 'page' );
        
        $current_page = $p == 0 ? 1 : $p;
        $var = 'search=' . $opt_game;
        $page = $this->page->page_show ( '/admin/singleSDKmanage/putAdList?' . $var, $count, $config ['per_page'] );
        // 媒体游戏数据
        $where_app = array (
                'is_ccplay' => 1 
        );
        $gamelist = $this->AppInfoModel->getList ( $where_app, 'appid, appname' );
        
        // print_r($gamelist);
        $this->smarty->assign ( 'page', $page );
        $this->smarty->assign ( 'per_page', $config ['per_page'] / 7 );
        $this->smarty->assign ( 'count', $count / 7 );
        $this->smarty->assign ( 'optgame', $opt_game );
        $this->smarty->assign ( 'gamelist', $gamelist );
        $this->smarty->assign ( 'putList', $show_data );
        $this->load->display ( 'admin/singleSDKmanage/putAdList.html' );
    }
    
    /**
     * 旧的增加交叉推广
     * @deprecated
     */
    public function addPushAdListPage() {
        // 媒体游戏数据
        $where_app = array (
                'is_ccplay' => 1 
        );
        $applist = $this->AppInfoModel->getList ( $where_app, 'appid, appname' );
        $appid_arr = array ();
        foreach ( $applist as $key => $value ) {
            array_push ( $appid_arr, $value ['appid'] );
        }
        // 获取渠道列表
        $this->load->model ( 'AppChannelModel' );
        $gamechannel = array ();
        if (! empty ( $appid_arr )) {
            foreach ( $appid_arr as $key => $appid ) {
                $channelinfo = $this->AppChannelModel->getList ( array (
                        'appid' => $appid 
                ), 'publisherID, channelname' );
                $gamechannel [$appid] = $channelinfo;
            }
        }
        // 渠道排查添加过的
        $publisher = $this->AdSubChannelModel->getpublisher ();
        if (! empty ( $publisher )) {
            foreach ( $publisher as $key => $publish ) {
                $pid = $publish ['publisherID'];
                foreach ( $gamechannel as $key_c => &$channel ) {
                    foreach ( $channel as $key => $value ) {
                        if ($pid == $value ['publisherID']) {
                            unset ( $channel [$key] );
                            if (count ( $gamechannel [$key_c] ) == 0) {
                                foreach ( $applist as $key_app => &$app ) {
                                    if ($app ['appid'] == $key_c) {
                                        unset ( $applist [$key_app] );
                                    }
                                }
                                unset ( $gamechannel [$key_c] );
                            }
                            $channel = array_values ( $channel );
                        }
                    }
                }
            }
        }
        // 投放的广告
        $gameList = $this->AdGameListModel->getList ();
        $this->smarty->assign ( 'applist', $applist );
        $gamechannel = json_encode ( $gamechannel );
        $this->smarty->assign ( 'channel', $gamechannel );
        $this->smarty->assign ( 'firstappid', $appid_arr [0] );
        $this->smarty->assign ( 'gameList', $gameList );
        $this->load->display ( 'admin/singleSDKmanage/addPushAdList.html' );
    }
    public function ajaxgetappinfo() {
        $postData = $this->input->post ();
        try {
            $appInfo = $this->getAppInfo ( $postData );
        
            if ($appInfo ['status'] == 0) {
                ajaxReturn ( $appInfo ['info'], 0, $appInfo ['data'] );
            }
            ajaxReturn ( '', 1, $appInfo ['data'] );
        } catch (Exception $e) {
            ajaxReturn ( '包经过加密无法解析', -1 );
        }
    }
    private function getAppInfo($data) {
        $reData ['data'] = array (
                'targetmsg',
                '请填写正确的apk包地址。' 
        );
        $reData ['info'] = 'targeterr';
        $reData ['status'] = 0;
        if (strpos ( $data ['target'], '.apk' ) > 0) {
            $pkgHeader = get_header ( $data ['target'], 1 );
            if (! $pkgHeader) {
                return $reData;
            }
            if (! isset ( $pkgHeader ['Content-Length'] ) || empty ( $pkgHeader ['Content-Length'] )) {
                return $reData;
            }
            $this->load->library ( 'getapkinfo' );
            $packData = GetApkInfo::getApkInfo ( $data ['target'] );
            $packData = json_decode ( $packData, true );
            if (! is_array ( $packData ) || ! isset ( $packData ['package'] ) || ! isset ( $packData ['ver'] ) || ! isset ( $packData ['code'] )) {
                return $reData;
            }
            $appInfo ['packagename'] = $packData ['package'];
            $appInfo ['appversionname'] = $packData ['ver'];
            $appInfo ['appversioncode'] = $packData ['code'];
            // 单位到K
            $appInfo ['appsize'] = $pkgHeader ['Content-Length'] / 1024;
            $reData ['status'] = 1;
        } else {
            return $reData;
        }
        if ($reData ['status']) {
            $reData ['data'] = $appInfo;
        }
        return $reData;
    }
    
    /**
     * 旧界面的6+1
     * @deprecated
     */
    public function doAddPushAd() {
        if ($_SERVER ['REQUEST_METHOD'] == "POST") {
            $app = trim ( $this->input->post ( 'app' ) );
            $publisher = trim ( $this->input->post ( 'publisher' ) );
            // $campaignid_0 = trim($this->input->post('campaignid_0'));
            $adcontent = trim ( $this->input->post ( 'adcontent' ) );
            $gametype = trim ( $this->input->post ( 'gametype' ) );
            for($i = 0; $i < 7; $i ++) {
                //
                $id_name = 'ad_' . $i;
                ${$id_name} = trim ( $this->input->post ( 'ad_' . $i ) );
                //
                $down_name = 'downads_' . $i;
                ${$down_name} = trim ( $this->input->post ( 'downads_' . $i ) );
                //
                $packagename_name = 'packagename_' . $i;
                ${$packagename_name} = trim ( $this->input->post ( 'packagename_' . $i ) );
                //
                $appversionname_name = 'appversionname_' . $i;
                ${$appversionname_name} = trim ( $this->input->post ( 'appversionname_' . $i ) );
                //
                $appversioncode_name = 'appversioncode_' . $i;
                ${$appversioncode_name} = trim ( $this->input->post ( 'appversioncode_' . $i ) );
            }
            $time = time ();
            if (empty ( $gametype ) || empty ( $adcontent )) {
                exit ( '投放广告内容为空' );
            }
            if (empty ( $downads_1 ) || empty ( $downads_2 ) || empty ( $downads_3 ) || empty ( $downads_4 ) || empty ( $downads_5 ) || empty ( $downads_6 )) {
                exit ( '下载地址不能为空' );
            }
            if (mb_strlen ( $adcontent, 'utf8' ) > 30) {
                exit ( '宣传文案最多30字' );
            }
            if (empty ( $appversionname_0 ) || empty ( $appversionname_1 ) || empty ( $appversionname_2 ) || empty ( $appversionname_3 ) || empty ( $appversionname_4 ) || empty ( $appversionname_5 ) || empty ( $appversionname_6 ) || empty ( $packagename_0 ) || empty ( $packagename_1 ) || empty ( $packagename_2 ) || empty ( $packagename_3 ) || empty ( $packagename_4 ) || empty ( $packagename_5 ) || empty ( $packagename_6 ) || empty ( $appversioncode_0 ) || empty ( $appversioncode_6 ) || empty ( $appversioncode_1 ) || empty ( $appversioncode_2 ) || empty ( $appversioncode_3 ) || empty ( $appversioncode_4 ) || empty ( $appversioncode_5 )) {
                exit ( '包名、版本信息不能为空' );
            }
            //
            $gameList = $this->AdGameListModel->getList ();
            $gameList_tmp = array ();
            foreach ( $gameList as $key => $value ) {
                $gameList_tmp [$value ['id']] = $value;
            }
            $gameList = $gameList_tmp;
            // 要插入数据的表
            $this->load->model ( 'AdCampaignModel' );
            $this->load->model ( 'AdGroupModel' );
            $this->load->model ( 'AdStuffModel' );
            $this->load->model ( 'AdStuffMoregameModel' );
            $this->load->model ( 'AdSubChannelModel' );
            $userid = $this->AppInfoModel->getOne ( array (
                    'appid' => $app 
            ), 'userid' );
            // 不可添加相同渠道的数据，暂时屏蔽
            /*
             * $sameSubChannel = $this->AdSubChannelModel->getList(array('appid' => $app, 'publisherID' => $publisher, 'status' => 1), 'id, campaignid');
             * if( !empty($sameSubChannel) ){
             * for ($i=0; $i < 7 ; $i++) {
             * $id = $sameSubChannel[$i]['id'];
             * $name = 'ad_' . $i;
             * $downads_name = 'downads_' . $i;
             * $downads = ${$downads_name};
             * $gameid = ${$name};
             * $gamename = $gameList[$gameid]['name'];
             * $campaignid = $sameSubChannel[$i]['campaignid'];
             * if($i == 0){
             * $path = $gameList[$gameid]['path_l'];
             * }
             * else{
             * $path = $gameList[$gameid]['path'];
             * }
             * $packagename_name = 'packagename_'.$i;
             * $packagename = ${$packagename_name};
             * $appversionname_name = 'appversionname_'.$i;
             * $appversionname = ${$appversionname_name};
             * $appversioncode_name = 'appversioncode_'.$i;
             * $appversioncode = ${$appversioncode_name};
             * //
             * $campaign_data = array(
             * 'campaignname' => $gamename
             * );
             * $this->AdCampaignModel->edit(array('campaignid' => $campaignid), $campaign_data);
             * //
             * $group_data = array(
             * 'adgroupname' => $gamename,
             * 'starttime' => strtotime(date('Y-m-d')),
             * 'endtime' => (strtotime(date('Y-m-d')) + 60 * 60 * 24 * 365),
             * 'lastupdatetime' => $time
             * );
             * $this->AdGroupModel->edit(array('campaignid' => $campaignid), $group_data);
             * $group_id = $this->AdGroupModel->getOne(array('campaignid' => $campaignid), 'adgroupid');
             * $adgroupid = $group_id;
             * $stuff_data = array(
             * 'campaignid' => $campaignid,
             * 'adstuffname' => $gamename,
             * 'target' => $downads,
             * 'packagename' => $packagename,
             * 'appversionname' => $appversionname,
             * 'appversioncode' => $appversioncode,
             * 'lastupdatetime' => $time
             * );
             * if($i == 0){
             * $stuff_data['appversionname'] = $appversionname;
             * }
             * $stuffid = $this->AdStuffModel->getOne(array('adgroupid' => $adgroupid), 'stuffid');
             * $this->AdStuffModel->edit(array('stuffid' => $stuffid), $stuff_data);
             * $stuff_moregame_data = array(
             * 'appname' => $gamename,
             * 'path' => $path,
             * 'appcontent' => ''
             * );
             * if($i == 0){
             * $stuff_moregame_data['appcontent'] = $adcontent;
             * }
             * $this->AdStuffMoregameModel->edit(array('stuffid' => $stuffid), $stuff_moregame_data);
             * $subchannel_data = array(
             * 'appid' => $app,
             * 'publisherID' => $publisher,
             * 'adgameid' => $gameid,
             * 'campaignid' => $campaignid,
             * 'download' => $downads,
             * 'adcontent' => '',
             * 'gametype' => '',
             * 'packagename' => $packagename,
             * 'appversionname' => $appversionname,
             * 'appversioncode' => $appversioncode,
             * 'time' => $time
             * );
             * if($i == 0){
             * $subchannel_data['adcontent'] = $adcontent;
             * $subchannel_data['gametype'] = $gametype;
             * }
             * $this->AdSubChannelModel->edit(array('id' => $id), $subchannel_data);
             * }
             *
             * }
             * else{
             */
            for($i = 0; $i < 7; $i ++) {
                $name = 'ad_' . $i;
                $downads_name = 'downads_' . $i;
                $downads = ${$downads_name};
                $gameid = ${$name};
                $gamename = $gameList [$gameid] ['name'];
                if ($i == 0) {
                    $path = $gameList [$gameid] ['path_l'];
                } else {
                    $path = $gameList [$gameid] ['path'];
                }
                $packagename_name = 'packagename_' . $i;
                $packagename = ${$packagename_name};
                $appversionname_name = 'appversionname_' . $i;
                $appversionname = ${$appversionname_name};
                $appversioncode_name = 'appversioncode_' . $i;
                $appversioncode = ${$appversioncode_name};
                //
                $campaign_data = array (
                        'campaignname' => $gamename,
                        'targettype' => '1',
                        'periodset' => '0',
                        'ostypeid' => 1,
                        'osversionset' => 0,
                        'aosvset' => '',
                        'createtime' => $time,
                        'status' => 2,
                        'selfappset' => 1,
                        'userid' => $userid 
                );
                $campaign_id = $this->AdCampaignModel->add ( $campaign_data );
                $group_data = array (
                        'campaignid' => $campaign_id,
                        'adgroupname' => $gamename,
                        'adform' => '10',
                        'chargemode' => 1,
                        'adtargettype' => 0,
                        'selfappset' => 1,
                        'is_ccplay_announce' => 1,
                        'starttime' => strtotime ( date ( 'Y-m-d' ) ),
                        'endtime' => (strtotime ( date ( 'Y-m-d' ) ) + 60 * 60 * 24 * 365),
                        'userid' => $userid,
                        'createtime' => $time,
                        'lastupdatetime' => $time,
                        'istracktype' => 2,
                        'status' => 1 
                );
                $group_id = $this->AdGroupModel->add ( $group_data );
                $stuff_data = array (
                        'adgroupid' => $group_id,
                        'campaignid' => $campaign_id,
                        'adstuffname' => $gamename,
                        'target' => $downads,
                        'stufftype' => 5,
                        'packagename' => $packagename,
                        'appversionname' => $appversionname,
                        'appversioncode' => $appversioncode,
                        'lastupdatetime' => $time,
                        'status' => 1 
                );
                if ($i == 0) {
                    $stuff_data ['appversionname'] = $appversionname;
                }
                $stuff_id = $this->AdStuffModel->add ( $stuff_data );
                $stuff_moregame_data = array (
                        'stuffid' => $stuff_id,
                        'appname' => $gamename,
                        'path' => $path,
                        'appcontent' => '' 
                );
                if ($i == 0) {
                    $stuff_moregame_data ['appcontent'] = $adcontent;
                }
                $this->AdStuffMoregameModel->add ( $stuff_moregame_data );
                
                $subchannel_data = array (
                        'appid' => $app,
                        'publisherID' => $publisher,
                        'adgameid' => $gameid,
                        'campaignid' => $campaign_id,
                        'download' => $downads,
                        'adcontent' => '',
                        'gametype' => '',
                        'packagename' => $packagename,
                        'appversionname' => $appversionname,
                        'appversioncode' => $appversioncode,
                        'position' => $i,
                        'time' => $time 
                );
                if ($i == 0) {
                    $subchannel_data ['adcontent'] = $adcontent;
                    $subchannel_data ['gametype'] = $gametype;
                }
                $this->AdSubChannelModel->add ( $subchannel_data );
            }
            // }
            $this->putAdList ();
        }
    }
    public function doDeletePutAd() {
        $publisherID = trim ( $this->input->get ( 'publisherID' ) );
        $subxappid = trim($this->input->get("subxappid"));
        $this->load->model ( 'AdCampaignModel' );
        if(!empty($publisherID)) {
        
            $data = $this->AdSubChannelModel->getList ( array (
                    'publisherID' => $publisherID,
                    'status' => 1 
            ), 'id, campaignid' );
            
            if (! empty ( $data )) {
                foreach ( $data as $key => $value ) {
                    $this->AdCampaignModel->edit ( array (
                            'campaignid' => $value ['campaignid'] 
                    ), array (
                            'status' => 3 
                    ) );
                    $this->AdSubChannelModel->edit ( array (
                            'id' => $value ['id'] 
                    ), array (
                            'status' => 0 
                    ) );
                }
            }
        } else if(!empty($subxappid)) {
            
            $campid = $this->AdSubChannelModel->getOne(array('id'=>$subxappid),'campaignid');
            $retCode = $this->AdCampaignModel->edit( array (
                    'campaignid' => $campid
            ), array (
                    'status' => 3
            ) );
            $this->AdSubChannelModel->edit ( array (
                    'id' => $subxappid
            ), array (
                    'status' => 0
            ) );
        }
        if($this->input->is_ajax_request()) {
            ajaxReturn(array("处理成功"),1);
        } else {
            $this->putAdListCcplay ();
        }
    }
    
    
    
    /**
     * 游戏推广应用管理
     * @deprecated
     */
    public function editPushAdPage() {
        // 媒体游戏数据
        $publisherID = $this->input->get ( 'publisherID' );
        // 要编辑的数据
        $channeldata = $this->AdSubChannelModel->getList ( array (
                'publisherID' => $publisherID,
                'status' => 1 
        ), '*', 'id' );
        if (! empty ( $channeldata )) {
            $where_app = array (
                    'is_ccplay_announce' => 1,
                    'is_ccplay' => 1 
            );
            $applist = $this->AppInfoModel->getList ( $where_app, 'appid, appname' );
            $appid_arr = array ();
            foreach ( $applist as $key => $value ) {
                array_push ( $appid_arr, $value ['appid'] );
            }
            // 获取渠道列表
            $this->load->model ( 'AppChannelModel' );
            $gamechannel = array ();
            if (! empty ( $appid_arr )) {
                foreach ( $appid_arr as $key => $appid ) {
                    $channelinfo = $this->AppChannelModel->getList ( array (
                            'appid' => $appid 
                    ), 'publisherID, channelname' );
                    $gamechannel [$appid] = $channelinfo;
                }
            }
            // 投放的广告
            $gameList = $this->AdGameListModel->getList ();
            $this->smarty->assign ( 'applist', $applist );
            $gamechannel = json_encode ( $gamechannel );
            //
            
            foreach ( $channeldata as $key => $value ) {
                $this->smarty->assign ( 'id_' . $key, $value ['id'] );
                $this->smarty->assign ( 'adgameid_' . $key, $value ['adgameid'] );
                $this->smarty->assign ( 'campaignid_' . $key, $value ['campaignid'] );
                $this->smarty->assign ( 'downads_' . $key, $value ['download'] );
                $this->smarty->assign ( 'packagename_' . $key, $value ['packagename'] );
                $this->smarty->assign ( 'appversionname_' . $key, $value ['appversionname'] );
                $this->smarty->assign ( 'appversioncode_' . $key, $value ['appversioncode'] );
            }
            // 第一条数据
            $this->smarty->assign ( 'appid', $channeldata [0] ['appid'] );
            $this->smarty->assign ( 'gametype', $channeldata [0] ['gametype'] );
            $this->smarty->assign ( 'adcontent', $channeldata [0] ['adcontent'] );
            
            $this->smarty->assign ( 'publisherID', $publisherID );
            $this->smarty->assign ( 'channel', $gamechannel );
            $this->smarty->assign ( 'firstappid', $channeldata [0] ['appid'] );
            $this->smarty->assign ( 'gameList', $gameList );
            $this->load->display ( 'admin/singleSDKmanage/editPushAd.html' );
        }
    }
    public function doEditPushAd() {
        if ($_SERVER ['REQUEST_METHOD'] == 'POST') {
            $app = trim ( $this->input->post ( 'appid' ) );
            $publisherID = trim ( $this->input->post ( 'publisherID' ) );
            $adcontent = trim ( $this->input->post ( 'adcontent' ) );
            $gametype = trim ( $this->input->post ( 'gametype' ) );
            for($i = 0; $i < 7; $i ++) {
                $id_name = 'id_' . $i;
                ${$id_name} = trim ( $this->input->post ( 'id_' . $i ) );
                //
                $ad_name = 'ad_' . $i;
                ${$ad_name} = trim ( $this->input->post ( 'ad_' . $i ) );
                //
                $down_name = 'downads_' . $i;
                ${$down_name} = trim ( $this->input->post ( 'downads_' . $i ) );
                //
                $packagename_name = 'packagename_' . $i;
                ${$packagename_name} = trim ( $this->input->post ( 'packagename_' . $i ) );
                //
                $appversionname_name = 'appversionname_' . $i;
                ${$appversionname_name} = trim ( $this->input->post ( 'appversionname_' . $i ) );
                //
                $appversioncode_name = 'appversioncode_' . $i;
                ${$appversioncode_name} = trim ( $this->input->post ( 'appversioncode_' . $i ) );
                //
                $campaignid_name = 'campaignid_' . $i;
                ${$campaignid_name} = trim ( $this->input->post ( 'campaignid_' . $i ) );
            }
            $time = time ();
            if (empty ( $gametype ) || empty ( $adcontent )) {
                exit ( '投放广告内容为空' );
            }
            if (empty ( $downads_1 ) || empty ( $downads_2 ) || empty ( $downads_3 ) || empty ( $downads_4 ) || empty ( $downads_5 ) || empty ( $downads_6 )) {
                exit ( '下载地址不能为空' );
            }
            if (mb_strlen ( $adcontent, 'utf8' ) > 30) {
                exit ( '宣传文案最多30字' );
            }
            if (empty ( $appversionname_0 ) || empty ( $appversionname_1 ) || empty ( $appversionname_2 ) || empty ( $appversionname_3 ) || empty ( $appversionname_4 ) || empty ( $appversionname_5 ) || empty ( $appversionname_6 ) || empty ( $packagename_0 ) || empty ( $packagename_1 ) || empty ( $packagename_2 ) || empty ( $packagename_3 ) || empty ( $packagename_4 ) || empty ( $packagename_5 ) || empty ( $packagename_6 ) || empty ( $appversioncode_0 ) || empty ( $appversioncode_6 ) || empty ( $appversioncode_1 ) || empty ( $appversioncode_2 ) || empty ( $appversioncode_3 ) || empty ( $appversioncode_4 ) || empty ( $appversioncode_5 )) {
                exit ( '包名、版本信息不能为空' );
            }
            if (empty ( $publisherID ))
                exit ( '不能修改，参数错误' );
                //
            $gameList = $this->AdGameListModel->getList ();
            $gameList_tmp = array ();
            foreach ( $gameList as $key => $value ) {
                $gameList_tmp [$value ['id']] = $value;
            }
            $gameList = $gameList_tmp;
            // 要编辑数据的表
            $this->load->model ( 'AdCampaignModel' );
            $this->load->model ( 'AdGroupModel' );
            $this->load->model ( 'AdStuffModel' );
            $this->load->model ( 'AdStuffMoregameModel' );
            $this->load->model ( 'AdSubChannelModel' );
            $userid = $this->AppInfoModel->getOne ( array (
                    'appid' => $app 
            ), 'userid' );
            for($i = 0; $i < 7; $i ++) {
                $id = ${'id_' . $i};
                $gameid = ${'ad_' . $i};
                $downads = ${'downads_' . $i};
                $gamename = $gameList [$gameid] ['name'];
                $campaignid = ${'campaignid_' . $i};
                if ($i == 0) {
                    $path = $gameList [$gameid] ['path_l'];
                } else {
                    $path = $gameList [$gameid] ['path'];
                }
                $packagename_name = 'packagename_' . $i;
                $packagename = ${$packagename_name};
                $appversionname_name = 'appversionname_' . $i;
                $appversionname = ${$appversionname_name};
                $appversioncode_name = 'appversioncode_' . $i;
                $appversioncode = ${$appversioncode_name};
                //
                $old_adgameid = $this->AdSubChannelModel->getOne ( array (
                        'id' => $id,
                        'status' => 1 
                ), 'adgameid' );
                $campaignid_new = $campaignid;
                // 游戏没有改变
                if ($old_adgameid == $gameid) {
                    $adgroupid = $this->AdGroupModel->getOne ( array (
                            'campaignid' => $campaignid 
                    ), 'adgroupid' );
                    $stuff_data = array (
                            'campaignid' => $campaignid,
                            'adstuffname' => $gamename,
                            'target' => $downads,
                            'packagename' => $packagename,
                            'appversionname' => $appversionname,
                            'appversioncode' => $appversioncode,
                            'lastupdatetime' => $time 
                    );
                    
                    $stuffid = $this->AdStuffModel->getOne ( array (
                            'adgroupid' => $adgroupid 
                    ), 'stuffid' );
                    $this->AdStuffModel->edit ( array (
                            'stuffid' => $stuffid 
                    ), $stuff_data );
                    $stuff_moregame_data = array (
                            'appname' => $gamename,
                            'path' => $path,
                            'appcontent' => '' 
                    );
                    if ($i == 0) {
                        $stuff_moregame_data ['appcontent'] = $adcontent;
                    }
                    $this->AdStuffMoregameModel->edit ( array (
                            'stuffid' => $stuffid 
                    ), $stuff_moregame_data );
                } else // 游戏改变
{
                    $campaign_data_old = array (
                            'status' => 3 
                    );
                    $this->AdCampaignModel->edit ( array (
                            'campaignid' => $campaignid 
                    ), $campaign_data_old );
                    // 插入新的数据
                    $campaign_data = array (
                            'campaignname' => $gamename,
                            'targettype' => '1',
                            'periodset' => '0',
                            'ostypeid' => 1,
                            'osversionset' => 0,
                            'aosvset' => '',
                            'createtime' => $time,
                            'status' => 2,
                            'selfappset' => 1,
                            'userid' => $userid 
                    );
                    $campaign_id = $this->AdCampaignModel->add ( $campaign_data );
                    $campaignid_new = $campaign_id;
                    $group_data = array (
                            'campaignid' => $campaign_id,
                            'adgroupname' => $gamename,
                            'adform' => '10',
                            'chargemode' => 1,
                            'adtargettype' => 0,
                            'selfappset' => 1,
                            'is_ccplay_announce' => 1,
                            'starttime' => strtotime ( date ( 'Y-m-d' ) ),
                            'endtime' => (strtotime ( date ( 'Y-m-d' ) ) + 60 * 60 * 24 * 365),
                            'userid' => $userid,
                            'createtime' => $time,
                            'lastupdatetime' => $time,
                            'istracktype' => 2,
                            'status' => 1 
                    );
                    $group_id = $this->AdGroupModel->add ( $group_data );
                    $stuff_data = array (
                            'adgroupid' => $group_id,
                            'campaignid' => $campaign_id,
                            'adstuffname' => $gamename,
                            'target' => $downads,
                            'stufftype' => 5,
                            'packagename' => $packagename,
                            'appversionname' => $appversionname,
                            'appversioncode' => $appversioncode,
                            'lastupdatetime' => $time,
                            'status' => 1 
                    );
                    $stuff_id = $this->AdStuffModel->add ( $stuff_data );
                    $stuff_moregame_data = array (
                            'stuffid' => $stuff_id,
                            'appname' => $gamename,
                            'path' => $path,
                            'appcontent' => '' 
                    );
                    if ($i == 0) {
                        $stuff_moregame_data ['appcontent'] = $adcontent;
                    }
                    $this->AdStuffMoregameModel->add ( $stuff_moregame_data );
                }
                $subchannel_data_old = array (
                        'lastupdatetime' => $time,
                        'status' => 2 
                );
                $this->AdSubChannelModel->edit ( array (
                        'id' => $id 
                ), $subchannel_data_old );
                $subchannel_data = array (
                        'appid' => $app,
                        'publisherID' => $publisherID,
                        'adgameid' => $gameid,
                        'campaignid' => $campaignid_new,
                        'download' => $downads,
                        'adcontent' => '',
                        'gametype' => '',
                        'packagename' => $packagename,
                        'appversionname' => $appversionname,
                        'appversioncode' => $appversioncode,
                        'position' => $i,
                        'time' => $time 
                );
                if ($i == 0) {
                    $subchannel_data ['adcontent'] = $adcontent;
                    $subchannel_data ['gametype'] = $gametype;
                }
                $this->AdSubChannelModel->add ( $subchannel_data );
            }
            $this->putAdList ();
        }
    }
    
    /**
     * 上传公告缩略图内部方法
     * 
     * @param string $name            
     * @param
     *            array array os size
     * @param string $path            
     */
    function _uploadAnnouncementThumb($name, $upload_config) {
        // print_r($upload_config);
        $this->load->library ( 'upload', $upload_config );
        if (! file_exists ( $upload_config ['upload_path'] )) {
            mkdir ( $upload_config ['upload_path'] . '/480/', 0777, true );
        }
        if (empty ( $_FILES [$name] ['name'] ) || ! $this->upload->do_upload ( $name )) {
            log_message('error',json_encode($this->upload->data(),true));
            return array (
                    'isok' => false,
                    'msg' => $this->upload->error_msg 
            );
        } else {
            $data ['upload_data'] = $this->upload->data (); // 文件的一些信息
            $thumb_name = $data ['upload_data'] ['file_name']; // 取得文件名
            
            $this->load->helper ( 'picture' );
            $originfilename = $upload_config ['upload_path'] . $thumb_name;
            $newfilename = $upload_config ['upload_path'] . '480/' . $thumb_name;
            // print_r(array($this->upload->upload_path.$thumb_name, $this->upload->upload_path.'lite_'.$thumb_name,$upload_config['size']));
            
            // echo $originfilename.'<br/>';
            // echo $newfilename.'<br/>';
            // echo "convert -scale {$upload_config['size']['width']}x{$upload_config['size']['height']}! {$originfilename} {$newfilename}";
            // exec("convert -scale {$upload_config['size']['width']}x{$upload_config['size']['height']}! {$originfilename} {$newfilename}",$out);
            $dualed_img = img ( $originfilename, $newfilename, array (
                    'width' => '480',
                    'autocalcWithWidth' => true 
            ) );
            return array (
                    'isok' => true,
                    'realpath' => $newfilename,
                    'url' => $upload_config ['imgurl'] . '480/' . $thumb_name,
                    'width'=>$dualed_img['width'],
                    'height'=>$dualed_img['height']
            );
        }
    }
    
    /**
     * FAQ编辑页面
     */
    public function faqEdit() {
        $this->assign('tabindex', 2);
        $faqid = $this->input->get ( 'id' );
        $appid = $this->input->get ( 'appid' );
        $appname = $this->AppInfoModel->getOne ( array (
                'appid' => $appid
        ), 'appname' );
        $this->assign('appname', $appname);
        
        if (empty ( $appid )) {
            show_error ( 'FAQ参数缺少' );
        }
        $this->_checkSwitchStatus($appid);
        if (! empty ( $faqid )) {
            $this->load->model ( 'CCPlayFAQModel' );
            $row = $this->CCPlayFAQModel->getRow ( array (
                    'id' => $faqid 
            ) );
            $this->assign ( 'row', $row );
        }
        
        $this->assign ( 'appid', $appid );
        
        $this->display ( 'admin/singleSDKmanage/editFAQItem.html' );
    }
    
    /**
     * FAQ提交处理
     */
    public function doEditFAQItem() {
    	$time = time();
    	
    	$this->load->model ( 'CCPlayFAQModel' );
    	$faqid = $this->input->post('id');
    	$appid = $this->input->post ( 'appid' );
    	if(empty($appid)) {
    	    $this->pageMsgBack('非法操作，请从应用界面进入');
    	}
    	
    	
    	$input_question = $this->input->post('q');
    	$input_anwser = $this->input->post('a');
    	
    	if(empty($input_question)) {
    	    ajaxReturn('FAQ标题不能为空', -1);
    	}
    	
    	if(empty($input_anwser)) {
    	    ajaxReturn('FAQ答案不能为空', -1);
    	}
    	
    	if(WeiboLength($input_question)>100) {
    	    ajaxReturn("标题不能超过100个汉字或200个英文字母长度", -1);
    	}
    	
    	if(WeiboLength($input_anwser)>100) {
    	    ajaxReturn("内容不能超过100个汉字或200个英文字母长度", -1);
    	}
    	
    	$campaignid = $this->CCPlayFAQModel->getOne(array('appid'=>$appid,'campaignid'=>array('NEQ',NULL),'id'=>$faqid),'campaignid','campaignid asc');
    	if(empty($campaignid)) {
    	    $cnt = $this->CCPlayFAQModel->getCount('1=1');
        	//增加广告相关START
        	$this->load->model ( 'AdCampaignModel' );
        	$this->load->model ( 'AdGroupModel' );
        	$this->load->model ( 'AdStuffModel' );
        	$this->load->model ( 'AdStuffTextModel' );
        	
        	
        	
        	$userid = $this->AppInfoModel->getOne ( array (
        	        'appid' => $appid
        	), 'userid' );
        	$title = 'FAQ_IN_APPID_'.$appid."";
        	$campaign_data = array (
        	        'campaignname' => $title,
        	        'targettype' => '2',
        	        'periodset' => '0',
        	        'ostypeid' => 1,
        	        'osversionset' => 0,
        	        'aosvset' => '',
        	        'createtime' => $time,
        	        'status' => 1,
        	        'selfappset' => 1,
        	        'userid' => $userid
        	);
        	$campaign_id = $this->AdCampaignModel->add ( $campaign_data );
        	
        	
        	$group_data = array (
        	        'campaignid' => $campaign_id,
        	        'adgroupname' => $title,
        	        'adform' => '14',//14 ccplay FAQ
        	        'chargemode' => 1,
        	        'adtargettype' => 0,
        	        'selfappset' => 1,
        	        'is_ccplay_announce' => 1,
        	        'starttime' => strtotime ( date ( 'Y-m-d', $time ) ),
        	        'endtime' => (strtotime ( date ( 'Y-m-d', $time ) ) + 60 * 60 * 24 * 365*10),
        	        'userid' => $userid,
        	        'createtime' => $time,
        	        'lastupdatetime' => $time,
        	        'istracktype' => 2,
        	        'status' => 1,
        	        'effecttype' => 21
        	); // effecttype = 21表示 HTML页面
        	
        	$group_id = $this->AdGroupModel->add ( $group_data );
        	$stuff_data = array (
        	        'adgroupid' => $group_id,
        	        'campaignid' => $campaign_id,
        	        'adstuffname' => $title,
        	        'target' => 'tel',
        	        'stufftype' => 2,
        	        'lastupdatetime' => $time,
        	        'status' => 1
        	);
        	$stuff_id = $this->AdStuffModel->add ($stuff_data);
        	
        	//广告方式侦测关联值保存END
        	$data = array(
        			'id'=>$this->input->post('id'),
        	        'a' => $input_anwser,
        	        'q' => $input_question,
        	        'appid' => $this->input->post('appid'),
        	        'ctime' => date('Y-m-d H:i:s', $time),
        	        'campaignid' => $campaign_id,
        	        'orderpriority'=>$cnt+1
        	);
        	
        	$retCode = $this->CCPlayFAQModel->add($data);
    	} else {
        	$data = array(
        			'campaignid'=>$campaignid,
        	        'a' => $input_anwser,
        	        'q' => $input_question
        	);
        	
        	$retCode = $this->CCPlayFAQModel->save(array('id'=>$this->input->post('id')), $data);
    	}
        
        
        if ($retCode > -1) {
            $msg = '处理成功';
        } else {
            $msg = '处理失败';
        }
        
        if($this->input->is_ajax_request()) {
            ajaxReturn($msg, $retCode);    
         } else {
            $this->pageMsgBack($msg, 'faqList?appid=' . $this->input->post('appid'));
        }
    }
    
    /**
     * FAQ列表
     * (non-PHPdoc)
     * 
     * @see singleSDKmanage::faqList()
     */
    public function faqList() {
        $this->assign('tabindex', 2);
        $appid = $this->input->get ( 'appid' );
        $appname = $this->AppInfoModel->getOne ( array (
                'appid' => $appid 
        ), 'appname' );
        $this->_checkSwitchStatus($appid);
        $this->load->model ( 'CCPlayFAQModel' );
        $now = time();
        $rows = $this->CCPlayFAQModel->getList (array (
                'appid' => $appid 
        ),'*' ,'orderpriority asc,ctime asc');
        
        foreach($rows as &$v) {
            $timediff = $now-strtotime($v['ctime']);
            if($timediff<3600) {
                $v['timediff'] = intval($timediff/60).'分钟前';
            } else if($timediff<86400) {
                $v['timediff'] = intval($timediff/3600).'小时前';
            }
        }
        $this->assign ( 'appid', $appid );
        $this->assign ( 'faqlist', $rows );
        $this->assign ( 'appname', $appname );
        
        $this->load->display ( 'admin/singleSDKmanage/faqList.html' );
    }
    
    /**
     * 删除FAQ项
     */
    public function faqDel() {
        $id = $this->input->post ( 'id' );
        $appid = $this->input->post ( 'appid' );
        $this->load->model ( 'CCPlayFAQModel' );
        $retCode = $this->CCPlayFAQModel->delete ( array (
                'id' => $id 
        ) );
        if ($retCode == 1) {
            $msg = '处理成功';
        } else {
            $msg = '处理失败';
        }
        ajaxReturn ( $msg, $retCode );
    }
    
    /**
     * FAQ位置修改
     */
    public function faqMv() {
        $this->load->model ( 'CCPlayFAQModel' );
        $faqid = $this->input->get('id');
        
        $faqappid = $this->input->get('appid');
        $direct = $this->input->get('direct');
        
        $clickOrder = $this->CCPlayFAQModel->getOne(array('id'=>$faqid),'orderpriority');
        $queryPrevID =null;
        $queryNextID = null;
        $retCode = 1;
        if('u'==$direct) {
            $data['orderpriority']=-1;
            $queryPrevID = $this->CCPlayFAQModel->getOne(array('appid'=>$faqappid,'orderpriority'=>array('LT',$clickOrder)),'id','orderpriority desc');
            if($queryPrevID!=null) {
                $retCode = $this->CCPlayFAQModel->edit ( array('id'=>array('EQ',$faqid)),
                        $data,
                        array('orderpriority')
                );
                
                $this->CCPlayFAQModel->edit ( array('id'=>$queryPrevID),array('orderpriority'=>1),
                    array('orderpriority'));
            }
            
        } else {
        	$data['orderpriority']=1;
        	$queryNextID = $this->CCPlayFAQModel->getOne(array('appid'=>$faqappid,'orderpriority'=>array('GT',$clickOrder)),'id','orderpriority asc');
        	if($queryNextID!=null) {
            	$retCode = $this->CCPlayFAQModel->edit ( array('id'=>array('EQ',$faqid)),
            	        $data,
            	        array('orderpriority')
            	);
            	
            	$this->CCPlayFAQModel->edit ( array('id'=>$queryNextID),array('orderpriority'=>-1),
        	        array('orderpriority'));
        	}
        }
        
        if($retCode>0) {
        	ajaxReturn("设置成功", 1);
        } else {
        	ajaxReturn("设置失败", -1);
        }
    }
    
    
    
    /**
     * 交叉换量分渠道管理页面
     * mgrXapplist?publisherID={$item['publisherID']}&appid={$item['appid']}
     */
    public function mgrXapplist() {
    	$this->assign('tabindex', 3);
    	
        $publisherID = $this->input->get('publisherID');
        $appid = $this->input->get('appid');
        $this->load->Model('AdSubChannelModel');
        $this->load->Model('AdGameListModel');
        $statusMap = array('0'=>'无效','1'=>'有效');
        
        $appname = $this->AppInfoModel->getOne ( array (
                'appid' => $appid
        ), 'appname' );
        $this->_checkSwitchStatus($appid);
        $this->assign('appname', $appname);
        $this->assign('appid', $appid);
        $this->assign('publisherID',$publisherID);
        
        $data = array (
                'status' => 1,
        		'publisherID'=>$publisherID,
        		'appid' => $appid
        );
        $where = 'status = 1 and publisherID='.$publisherID;
        $where .= " and appid = $appid";
       
        $p = trim ( $this->input->get ( 'per_page' ) );
        if (!$p) {
            $p = 1;
        }
        $config ['per_page'] = 20 * 7;
        $this->AdSubChannelModel->setStrWhere ( $where );
        //echo $where;
        
        $allGameList = $this->AdGameListModel->getList();
        $gamemap = array();
        foreach ($allGameList as $gameone) {
            $gamemap[$gameone['id']]=$gameone;
        }
        
        //$count = $this->AdSubChannelModel->getCount ();
        $putList = $this->AdSubChannelModel->getList ( $data, '*', "id desc", "", $p, $config ['per_page'] );
        foreach($putList as &$row) {
        	$row['statusname'] = $statusMap[$row['status']];
        	$row['gamename'] = $gamemap[$row['adgameid']]['name'];
        }
        $this->assign("xappList",$putList);
        
        
        
        $this->assign("gameList",  $gamemap);
        $this->display('admin/singleSDKmanage/xappList.html');
    }
    
    /**
     * 交叉换量APP记录增加Action与编辑Action
     * @author yangsen
     */
    public function addXappAction() {
    	
        $appid = $this->input->post('appid');
        $adcontent = trim ( $this->input->post ( 'adcontent' ) );   
        $gametype = trim ( $this->input->post ( 'gametype' ) );
        $time=time();
        
        $itemrowid = $this->input->post('id');
        
        $gameid = $this->input->post('adgameid');
        $this->load->model ( 'AdCampaignModel' );
        $this->load->model ( 'AdGroupModel' );
        $this->load->model ( 'AdStuffModel' );
        $this->load->model ( 'AdStuffMoregameModel' );
        $this->load->model ( 'AdSubChannelModel' );
        	
        	
        $packagename = $this->input->post('packagename');
        $appversionname = $this->input->post('appversionname');
        $appversioncode = $this->input->post('appversioncode');
        $downads = $this->input->post('download_url');
        $publisher  = $this->input->post('publisherID');
        
        
        //检查是否已经添加过了此APP
        $checkDuplicateGameid = $this->AdSubChannelModel->getRow(
               array('appid' => $appid,
                        'publisherID' => $publisher,'adgameid'=>$gameid,'status'=>1,'id'=>array('NEQ',$itemrowid))
        );
        
        if($checkDuplicateGameid!=null) {
            ajaxReturn('该渠道已存在相同推广游戏', -1);
            return ;
        }
        
        
        $q_position = $this->AdSubChannelModel->getOne(
                array('appid' => $appid,
                        'publisherID' => $publisher),'max(position)');
        if($q_position==null) {
            $q_position = 1;
        }
        $q_position +=1;
        
        $userid = $this->AppInfoModel->getOne ( array (
                'appid' => $appid
        ), 'userid' );
        
        
        $gameRow = $this->AdGameListModel->getRow(array('id'=>$gameid));
        $path = $gameRow['path'];
        $gamename = $gameRow['name'];
        
        if(empty($itemrowid)) {
        
            $campaign_data = array (
                    'campaignname' => $gamename,
                    'targettype' => '1',
                    'periodset' => '0',
                    'ostypeid' => 1,
                    'osversionset' => 0,
                    'aosvset' => '',
                    'createtime' => $time,
                    'status' => 2,//投放中
                    'selfappset' => 1,
                    'userid' => $userid
            );
            $campaign_id = $this->AdCampaignModel->add ( $campaign_data );
            $group_data = array (
                    'campaignid' => $campaign_id,
                    'adgroupname' => $gamename,
                    'adform' => '10',
                    'chargemode' => 1,
                    'adtargettype' => 0,
                    'selfappset' => 1,
                    'is_ccplay_announce' => 1,
                    'starttime' => strtotime ( date ( 'Y-m-d' ) ),
                    'endtime' => (strtotime ( date ( 'Y-m-d' ) ) + 60 * 60 * 24 * 365),
                    'userid' => $userid,
                    'createtime' => $time,
                    'lastupdatetime' => $time,
                    'istracktype' => 2,
                    'status' => 1 //1是有效
            );//kt应用推广
            $group_id = $this->AdGroupModel->add ( $group_data );
            $stuff_data = array (
                    'adgroupid' => $group_id,
                    'campaignid' => $campaign_id,
                    'adstuffname' => $gamename,
                    'target' => $downads,
                    'stufftype' => 5,
                    'packagename' => $packagename,
                    'appversionname' => $appversionname,
                    'appversioncode' => $appversioncode,
                    'lastupdatetime' => $time,
                    'status' => 1 //1是审核通过
            );
            
            $stuff_id = $this->AdStuffModel->add ( $stuff_data );
            $stuff_moregame_data = array (
                    'stuffid' => $stuff_id,
                    'appname' => $gamename,
                    'path' => $path,
                    'appcontent' => $adcontent
            );
            
            $this->AdStuffMoregameModel->add ( $stuff_moregame_data );
            //查询数据库中的position数据
            $subchannel_data = array (
                    'appid' => $appid,
                    'publisherID' => $publisher,
                    'adgameid' => $gameid,
                    'campaignid' => $campaign_id,
                    'download' => $downads,
                    'adcontent' => $adcontent,
                    'gametype' => '是',
                    'packagename' => $packagename,
                    'appversionname' => $appversionname,
                    'appversioncode' => $appversioncode,
                    'position' => $q_position,
                    'time' => $time
            );
            
            $retCode=$this->AdSubChannelModel->add ( $subchannel_data );
            $_rowid = $retCode;
        } else {
            $campaignid = $this->AdSubChannelModel->getOne(array('id'=>$itemrowid),'campaignid');
            $this->AdCampaignModel->edit (array('campaignid'=>$campaignid),array(
                    'campaignname' => $gamename
            ));
            
            
            $stuffid = $this->AdStuffModel->getOne(array('campaignid'=>$campaignid),'stuffid');
            //modify adstuff data
            $stuff_data = array (
                    
                    'target' => $downads,
                    'packagename' => $packagename,
                    'appversionname' => $appversionname,
                    'appversioncode' => $appversioncode,
                    'lastupdatetime' => $time,
                    'adstuffname' => $gamename
            );
            
            $this->AdStuffModel->edit (array('stuffid'=>$stuffid), $stuff_data);
            
            
            $stuff_moregame_data = array (
                    'path' => $path,
                    'appcontent' => $adcontent,
                    'appname' => $gamename
            );
            
            $this->AdStuffMoregameModel->edit (array('stuffid'=>$stuffid), $stuff_moregame_data );
            
            $subchannel_data = array (
                    'adgameid' => $gameid,
                    'download' => $downads,
                    'packagename' => $packagename,
                    'appversionname' => $appversionname,
                    'appversioncode' => $appversioncode,
                    'adcontent' => $adcontent,
                    'lastupdatetime'=>$time
            );
            $retCode=$this->AdSubChannelModel->edit (array('id'=>$itemrowid), $subchannel_data );
            $_rowid = $itemrowid;
            
        }
            
        
        if($retCode>-1) {
        $subchannel_data['id'] = $_rowid;
        $subchannel_data['time'] = date('Y-m-d H:i:s',$time);
        	$info = '处理成功';
        } else {
        	$info = '处理失败';
        }
        ajaxReturn($info, $retCode,$subchannel_data);
        
    }
    
    function ccplayGonglueList() {
        $this->assign('tabindex', 5);
        $appid = $this->input->get ( 'appid' );
        $appname = $this->AppInfoModel->getOne ( array (
                'appid' => $appid
        ), 'appname' );
        
        $this->load->model ( 'AppChannelConfigModel' );
        
        if (empty ( $appid )) {
            show_error ( '参数错误', '500' );
        }
        
        
        $gonglueurl = $this->AppChannelConfigModel->getOne ( array (
                'appid' => $appid,
                'publisherID'=>0,
                'configname' => 'gonglueurl'
        ) ,'configvalue');
        
        
        
        $this->assign ( 'appname', $appname );
        $this->assign ( 'appid', $appid );
        $this->_checkSwitchStatus($appid);
        $this->assign('val', $gonglueurl);
        $this->display('admin/singleSDKmanage/gameGonglue.html');
    }
    
    /**
     * 更新攻略
     */
    function updateChannelGonglue() {

        $appid = $this->input->post ( 'appid' );
        $pid = $this->input->post ( 'pid' );
        $val = $this->input->post ( 'val' );
        if(!empty($val)) {
            $val = str_replace('http://', '', $val);
        }
        $this->load->model ( 'AppChannelConfigModel' );
        
        $currenttel = $this->AppChannelConfigModel->getRow ( array (
                'appid' => $appid,
                'publisherID'=>0,
                'configname' => 'gonglueurl'
        ) ,'configvalue,campaignid,adid');
        
        
        $campaign_id = null;
        $stuff_id = null;
        if($currenttel!=null) {
            $campaign_id = $currenttel['campaignid'];
            $stuff_id = $currenttel['adid'];
        } else {
            $time = time();
            //广告方式侦测关联值保存
            // 要插入数据的表
            $this->load->model ( 'AdCampaignModel' );
            $this->load->model ( 'AdGroupModel' );
            $this->load->model ( 'AdStuffModel' );
            $this->load->model ( 'AdStuffTextModel' );
        
            $userid = $this->AppInfoModel->getOne ( array (
                    'appid' => $appid
            ), 'userid' );
            $title = '攻略URL APPID:'.$appid."";
            $campaign_data = array (
                    'campaignname' => $title,
                    'targettype' => '2',
                    'periodset' => '0',
                    'ostypeid' => 1,
                    'osversionset' => 0,
                    'aosvset' => '',
                    'createtime' => $time,
                    'status' => 1,
                    'selfappset' => 1,
                    'userid' => $userid
            );
            $campaign_id = $this->AdCampaignModel->add ( $campaign_data );
        
            $group_data = array (
                    'campaignid' => $campaign_id,
                    'adgroupname' => $title,
                    'adform' => '12',
                    'chargemode' => 1,
                    'adtargettype' => 0,
                    'selfappset' => 1,
                    'is_ccplay_announce' => 1,
                    'starttime' => strtotime ( date ( 'Y-m-d', $time ) ),
                    'endtime' => (strtotime ( date ( 'Y-m-d', $time ) ) + 60 * 60 * 24 * 365*10),
                    'userid' => $userid,
                    'createtime' => $time,
                    'lastupdatetime' => $time,
                    'istracktype' => 2,
                    'status' => 1,
                    'effecttype' => 21
            );  
        
            $group_id = $this->AdGroupModel->add ( $group_data );
            $stuff_data = array (
                    'adgroupid' => $group_id,
                    'campaignid' => $campaign_id,
                    'adstuffname' => $title,
                    'target' => 'tel',
                    'stufftype' => 2,
                    'lastupdatetime' => $time,
                    'status' => 1
            );
            $stuff_id = $this->AdStuffModel->add ($stuff_data);
            //广告方式侦测关联值保存END
        }
        
        
        $data = array (
                'appid' => $appid,
                'publisherID' => 0,
                'configname' => 'gonglueurl',
                'configvalue' => $val,
                'campaignid'=>$campaign_id,
                'adid'=>$stuff_id
        );
        $efrownum = $this->AppChannelConfigModel->add ( $data, true );
        
        
        ajaxReturn ( '修改成功', 1, array (
        'effectrow' => $efrownum
        ) );
    }
}

?>
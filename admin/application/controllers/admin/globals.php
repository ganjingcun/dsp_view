	<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
define('UPLOAD_DOC_PATH',UPLOAD_DIR.'/doc');
define('UPLOAD_SDK_DYNAMIC_PACKAGE',UPLOAD_DIR.'/dynamicsdk/package/'.date('Ym').'/'.rand(1000,9999));
define('UPLOAD_SDK_DYNAMIC_CONFIG',UPLOAD_DIR.'/dynamicsdk/sdkinfo');
class Globals extends MY_A_Controller
{
	private $sdkostype = array(1=>'ios', 2=>'android');
    function __construct()
    {
        parent::__construct();
        $this->load->model("AdEcpcModel");
        $configlist = $this->SystemConfigModel->getAllConfig();
        $this->assign('configlist', $configlist);
    }

    function ecpcset(){
        $tab = (int)$this->input->get('tab');
        $rate = array('ratio'=>0);
        $this->assign('moregame', $rate);
        $this->assign('banner', $rate);
        $this->assign('pop', $rate);
        $this->assign('tab', $tab);
        $this->assign('search', '');
        $ecpc = $this->AdEcpcModel->getDefaultRatio();
        $this->assign('ecpc', $ecpc);
        $list = $this->AdEcpcModel->getList(array(), "*", "ostype asc,addtime desc, adtype asc");
        $ecpclist = array();
        foreach($list as $val){
            $ecpclist[$val['ostype']][$val['addtime']][$val['adtype']] = $val;
        }
        $this->load->model('AdminUserModel');
        $this->assign('ecpclist', $ecpclist);
        $this->assign('admins', $this->AdminUserModel->getAdminName());
        $this->display('admin/globals/ecpcset.html');
    }

    /**
     * 轮播广告数量设置
     * Enter description here ...
     */
    function gdadset(){
        $this->load->model("AdRotatorConfigModel");
        $list = $this->AdRotatorConfigModel->getList(array('status'=>0));
        $data = array();
        foreach($list as $val){
            $data[$val['adtype']] = $val['rotatorlength'];
        }
        $this->assign('data', $data);
        $this->display('admin/globals/gdadset.html');
    }

    /**
     * 轮播广告数量设置历史记录
     * Enter description here ...
     */
    function gdadhistory(){
        $adtype = $this->input->get('adtype');
        $adtypename = $adtype;
        switch ($adtype)
        {
            case 'androidbanner':
                $adtypename = 'Android Banner轮播数量';
                break;
            case 'iosbanner':
                $adtypename = 'iOS Banner轮播数量';
                break;
            case 'androidpopup':
                $adtypename = 'Android 插屏轮播数量';
                break;
            case 'iospopup':
                $adtypename = 'iOS 插屏轮播数量';
                break;
        }
        $this->load->model("AdRotatorConfigModel");
        $data = $this->AdRotatorConfigModel->getList(array('adtype'=>$adtype), '*', ' addtime desc, id desc ');
        $this->assign('data', $data);
        $this->assign('adtypename', $adtypename);
        $this->display('admin/globals/gdadhistory.html');
    }

    /**
     * 设置轮播广告数量设置
     * Enter description here ...
     */
    function ajaxgdadsave(){
        $adtype = $this->input->post('adtype');
        $rotatorlength = $this->input->post('rotatorlength');
        if(!is_numeric($rotatorlength)){
        	ajaxReturn('数量必须为数字', 0);
        }
        elseif($adtype !='iosdsp' && $adtype!='androiddsp' && $rotatorlength < 2){
        	ajaxReturn('数量不能小于2个', 0);
        }
        elseif(($adtype == 'iosdsp' || $adtype == 'androiddsp') && $rotatorlength < 0 ){
        	 ajaxReturn('数量不能小于0个', 0);
        }
        $this->load->model("AdRotatorConfigModel");
        $user = $this->session->userdata("admin");
        $this->AdRotatorConfigModel->edit(
        array('adtype' => $adtype, 'status'=>0),
        array('status'=>1, 'edittime'=>time(), 'adminid' => $user['admin_id'])
        );
        $status = $this->AdRotatorConfigModel->add(
        array('adtype' => $adtype, 'rotatorlength' =>$rotatorlength, 'addtime'=>time(), 'adminid' => $user['admin_id'])
        );
        if($status){
            ajaxReturn('修改成功', 1);
        }
        ajaxReturn('修改失败', 0);
    }

    /**
     * 批量设置积分墙分成比例
     * Enter description here ...
     */
    function ajaxbatchsetsave(){
        $valueset = $this->input->post('valueset');
        $os = $this->input->post('os');

        if(!preg_match("/^(\d{1,3})(\.\d{0,2})?$/", $valueset))
        {
            ajaxReturn('请输入数字！', 0, '请输入数字！');
        }
        $this->load->model('AdEcpcAppModel');
        $data = $this->AdEcpcAppModel->getOSAppList($os);
        if(empty($data))
        {
            ajaxReturn('请输入数字！', 0, '未找到要修改的数据！');
        }
        foreach ($data as $k => $v)
        {
            $update = array();
            $update['appid'] = $v['appid'];
            $update['banner'] = $v['banner'];
            $update['bannerratio'] = $v['bannerratio'];
            $update['bannertype'] = $v['bannertype'];
            $update['popup'] = $v['popup'];
            $update['popupratio'] = $v['popupratio'];
            $update['popuptype'] = $v['popuptype'];
            $update['moregame'] = $v['moregame'];
            $update['moregameratio'] = $v['moregameratio'];
            $update['moregametype'] = $v['moregametype'];
            $update['launch'] = $v['launch'];
            $update['launchratio'] = $v['launchratio'];
            $update['launchtype'] = 2;
            $update['wall'] = $valueset;
            $update['wallratio'] = $v['wallratio'];
            $update['walltype'] = 3;
            $update['uptime'] = 0;
            $update['status'] = 1;
            $update['addtime'] = time();
            $this->AdEcpcAppModel->edit(array('id'=>$v['id']), array('status'=>0, 'uptime'=>time()));
            $this->AdEcpcAppModel->add($update);
        }
        ajaxReturn('请输入数字！', 1, '批量修改成功！共修改'.count($data).'条记录！');
    }

    function cutoverset()
    {
        $partnerdata = array('maobo'=>'茂博');
        $adtypedata = array('1'=>'Banner');

        $this->load->model('CutoverSetModel');
        $search = $this->input->post('search');
        if(empty($search))
        {
            $where = array();
        }
        else
        {
            $where = array('pid'=>array('LIKE','%'.$search.'%'));
        }
        $list = $this->CutoverSetModel->getList($where, '*', 'isdel asc, edittime desc');
        $pids = array();
        foreach ($list as $k=>$v)
        {
            $pids[] = $v['pid'];
        }
        $this->load->model('AppChannelModel');
        $this->load->model('AppInfoModel');
        $pidlist = $this->AppChannelModel->getList(array('publisherID'=>array('in',$pids)), '*');
        $aids = array();
        $pinfo = array();
        $ainfo = array();
        foreach ($pidlist as $k=>$v)
        {
            $pinfo[$v['publisherID']] = $v['channelname'];
            $ainfo[$v['publisherID']] = $v['appid'];
            $aids[] = $v['appid'];
        }
        $aidlist = $this->AppInfoModel->getList(array('appid'=>array('in',$aids)), '*');
        foreach ($aidlist as $k=>$v)
        {
            $key = array_search($v['appid'], $ainfo);
            if( $key > 0)
            {
                $ainfo[$key] = $v['appname'];
            }
        }
        foreach($list as $k => $v)
        {
            if(isset($pinfo[$v['pid']]))
            {
                $list[$k]['pname'] = $pinfo[$v['pid']];
            }
            else
            {
                $list[$k]['pname'] = '未知渠道';
            }
            if(isset($ainfo[$v['pid']]))
            {
                $list[$k]['aname'] = $ainfo[$v['pid']];
            }
            else
            {
                $list[$k]['aname'] = '未知应用';
            }
        }
        $this->assign('partnerdata', $partnerdata);
        $this->assign('adtypedata', $adtypedata);
        $this->assign('list', $list);
        $this->assign('search', $search);
        $this->display('admin/globals/cutoverset.html');

    }

    /**
     *
     * 添加切量PID
     */
    function addcutover(){
        $this->load->model('CutoverSetModel');
        $pid = trim($this->input->post('pid'));
        $partner = trim($this->input->post('partner'));
        $adtype = trim($this->input->post('adtype'));
        if(empty($pid))
        {
            ajaxReturn('请填写PID!', 0);
        }
        $this->load->model("AppChannelModel");
        $pinfo = $this->AppChannelModel->getList(array('publisherID'=>$pid), '*');
        if(empty($pinfo))
        {
            ajaxReturn('PID不存在!', 0);
        }
        $data['pid'] = strtoupper($pid);
        $data['partner'] = $partner;
        $data['os'] = 'Android';
        $data['partner'] = $partner;
        $data['adtype'] = $adtype;
        $data['createtime'] = time();
        $data['edittime'] = time();

        $id = $this->CutoverSetModel->add($data);
        if($id>0)
        {
            ajaxReturn('添加成功!', 1);
        }
        else
        {
            ajaxReturn('添加失败!', 0);
        }
    }

    /**
     *
     * 刷新时间
     */
    function doEditCutover(){
        $this->load->model('CutoverSetModel');
        $id = trim($this->input->post('id'));
        $key = trim($this->input->post('key'));
        if(empty($id))
        {
            ajaxReturn('操作失败!', 0);
        }
        $num = $this->CutoverSetModel->edit(array('id'=>$id), array('edittime'=>time(), 'isdel'=>$key));
        if($num>0)
        {
            ajaxReturn('操作成功!', 1);
        }
        else
        {
            ajaxReturn('操作失败!', 0);
        }
    }

    function appset(){
        $tab = (int)$this->input->get('tab');
        $this->assign('tab', $tab);
        $search_app = $this->input->get('search_app');
        $appid = $this->input->get('appid');
        $this->assign('search_app', $search_app);
        $where = '';
        $os = (int)$this->input->get('search_os');
        $this->assign('search_os', $os);
        if($os){
            $where .= ' and app_info.ostypeid = '.$os;
        }
        $ratio = array();
        if($search_app){
            $this->load->model('AdEcpcModel');
            $ratio = $this->AdEcpcModel->getDefaultRatio();
            $where .= ' and app_info.appname = "'.$search_app.'"';
            if(!empty($appid))
            {
                $where .= ' and app_info.appid = "'.$appid.'"';
            }
        }

        $this->load->model('ClickRatioModel');
        $chargeratio = $this->ClickRatioModel->getDefaultRatio();
        $this->load->model('AppInfoModel');
        $this->load->library('page');
        $p=trim($this->input->get('per_page'));
        if(!$p){
            $p=1;
        }
        $current_page =  $p==0 ? 1 : $p;
        $config['per_page'] = 20;
        $count = $this->AppInfoModel->getAppCount($where);
        $page=$this->page->page_show('/admin/globals/appset?tab='.$tab.'&search_os='.$os,$count,$config['per_page']);
        $fields = 'app_info.appname,app_info.appid,app_info.ostypeid,ifnull(ad_ecpc_app.banner,"") banner,ifnull(ad_ecpc_app.moregame,"") moregame,ifnull(ad_ecpc_app.popup, "") popup,ad_ecpc_app.launch,ad_ecpc_app.wall,ifnull(ad_ecpc_app.feeds, "") feeds,ifnull(ad_ecpc_app.video, "") video,ifnull(ad_ecpc_app.bannerratio,"") bannerratio,ifnull(ad_ecpc_app.moregameratio,"") moregameratio,ifnull(ad_ecpc_app.popupratio, "") popupratio,ad_ecpc_app.launchratio,ad_ecpc_app.wallratio,ifnull(ad_ecpc_app.feedsratio, "") feedsratio,ifnull(ad_ecpc_app.videoratio, "") videoratio,ad_ecpc_app.uptime,ad_ecpc_app.bannertype,ad_ecpc_app.moregametype,ad_ecpc_app.popuptype,ad_ecpc_app.launchtype,ad_ecpc_app.walltype,ad_ecpc_app.feedstype,ifnull(ad_ecpc_app.videotype,"") videotype,ad_ecpc_app.addtime';
        $list = $this->AppInfoModel->getAppCpc($where, $fields,$p,$config['per_page']);
        $this->assign('list', $list);
        $this->assign('page', $page);
        $this->assign('count', $count);
        $this->assign('ratio', $ratio);
        $this->assign('appid', $appid);
        $this->assign('chargeratio', $chargeratio);
        $this->assign('per_page', $config['per_page']);
        $this->display('admin/globals/appset.html');
    }

    /**
     * 积分墙，精品推荐预加载时间设置。
     * Enter description here ...
     */
    function preloadset(){
        $this->load->model('SystemConfigModel');
        $data['mgignoredevice'] = $this->SystemConfigModel->getOne(array('skey'=>'mgignoredevice'), 'sval');
        $data['taskignoredevice'] = $this->SystemConfigModel->getOne(array('skey'=>'taskignoredevice'), 'sval');
        $data['antifraudswitch'] = $this->SystemConfigModel->getOne(array('skey'=>'antifraudswitch'), 'sval');
        $data['popupignoredevice'] = $this->SystemConfigModel->getOne(array('skey'=>'popupignoredevice'), 'sval');
        $data['dspcutovercount'] = $this->SystemConfigModel->getOne(array('skey'=>'dspcutovercount'), 'sval');//单次dsp切量次数
//        $data['sdkaccumulatepointsdelay'] = $this->SystemConfigModel->getOne(array('skey'=>'sdkaccumulatepointsdelay'), 'sval');
//        $data['serviceaccumulatepointsdelay'] = $this->SystemConfigModel->getOne(array('skey'=>'serviceaccumulatepointsdelay'), 'sval');
		$data['trackeventtype'] = $this->SystemConfigModel->getOne(array('skey'=>'trackeventtype'), 'sval');
		$data['trackeventtype1'] = $this->SystemConfigModel->getOne(array('skey'=>'trackeventtype1'), 'sval');//安装
		$data['trackeventtype2'] = $this->SystemConfigModel->getOne(array('skey'=>'trackeventtype2'), 'sval');//激活
		$data['trackeventtype3'] = $this->SystemConfigModel->getOne(array('skey'=>'trackeventtype3'), 'sval');//注册
		$data['trackeventtype4'] = $this->SystemConfigModel->getOne(array('skey'=>'trackeventtype4'), 'sval');//点击
		$data['trackeventtype5'] = $this->SystemConfigModel->getOne(array('skey'=>'trackeventtype5'), 'sval');//签到
		$data['trackeventtype6'] = $this->SystemConfigModel->getOne(array('skey'=>'trackeventtype6'), 'sval');//好友分享
		$data['trackeventtype7'] = $this->SystemConfigModel->getOne(array('skey'=>'trackeventtype7'), 'sval');//关注公众账号
		$data['trackeventtype8'] = $this->SystemConfigModel->getOne(array('skey'=>'trackeventtype8'), 'sval');//搜索
		$data['trackeventtype9'] = $this->SystemConfigModel->getOne(array('skey'=>'trackeventtype9'), 'sval');//观看视频
		$data['trackeventtype99'] = $this->SystemConfigModel->getOne(array('skey'=>'trackeventtype99'), 'sval');//自定义
        if($data['antifraudswitch'] === null)
        {
            $this->SystemConfigModel->add(array('skey'=>'antifraudswitch', 'sval'=>1));
        }
        if($data['mgignoredevice'] === null)
        {
            $this->SystemConfigModel->add(array('skey'=>'mgignoredevice', 'sval'=>0));
        }
        if($data['taskignoredevice'] === null)
        {
            $this->SystemConfigModel->add(array('skey'=>'taskignoredevice', 'sval'=>0));
        }
        if($data['popupignoredevice'] === null)
        {
            $this->SystemConfigModel->add(array('skey'=>'popupignoredevice', 'sval'=>0));
        }
//   		if($data['sdkaccumulatepointsdelay'] === null)
//        {
//            $this->SystemConfigModel->add(array('skey'=>'sdkaccumulatepointsdelay', 'sval'=>10));
//        }
//   		if($data['serviceaccumulatepointsdelay'] === null)
//        {
//            $this->SystemConfigModel->add(array('skey'=>'serviceaccumulatepointsdelay', 'sval'=>20));
//        }
    	if($data['trackeventtype'] === null)
        {
            $this->SystemConfigModel->add(array('skey'=>'trackeventtype', 'sval'=>0));
        }
     	if($data['dspcutovercount'] === null)
        {
            $this->SystemConfigModel->add(array('skey'=>'dspcutovercount', 'sval'=>2));
        }
        $data['trackeventtypeid'] = $this->SystemConfigModel->getOne(array('skey'=>'trackeventtype'), 'id');
   		if($data['trackeventtype1'] === null)
        {
            $this->SystemConfigModel->add(array('skey'=>'trackeventtype1', 'sval'=>10, 'parentid'=>$data['trackeventtypeid']));
        }
   		if($data['trackeventtype2'] === null)
        {
            $this->SystemConfigModel->add(array('skey'=>'trackeventtype2', 'sval'=>10, 'parentid'=>$data['trackeventtypeid']));
        }
    	if($data['trackeventtype3'] === null)
        {
            $this->SystemConfigModel->add(array('skey'=>'trackeventtype3', 'sval'=>10, 'parentid'=>$data['trackeventtypeid']));
        }
   		if($data['trackeventtype4'] === null)
        {
            $this->SystemConfigModel->add(array('skey'=>'trackeventtype4', 'sval'=>10, 'parentid'=>$data['trackeventtypeid'] ));
        }
    	if($data['trackeventtype5'] === null)
        {
            $this->SystemConfigModel->add(array('skey'=>'trackeventtype5', 'sval'=>10, 'parentid'=>$data['trackeventtypeid'] ));
        }
   		if($data['trackeventtype6'] === null)
        {
            $this->SystemConfigModel->add(array('skey'=>'trackeventtype6', 'sval'=>10, 'parentid'=>$data['trackeventtypeid'] ));
        }
    	if($data['trackeventtype7'] === null)
        {
            $this->SystemConfigModel->add(array('skey'=>'trackeventtype7', 'sval'=>10, 'parentid'=>$data['trackeventtypeid'] ));
        }
   		if($data['trackeventtype8'] === null)
        {
            $this->SystemConfigModel->add(array('skey'=>'trackeventtype8', 'sval'=>10, 'parentid'=>$data['trackeventtypeid'] ));
        }
   		if($data['trackeventtype9'] === null)
        {
            $this->SystemConfigModel->add(array('skey'=>'trackeventtype9', 'sval'=>10, 'parentid'=>$data['trackeventtypeid'] ));
        }
   		if($data['trackeventtype99'] === null)
        {
            $this->SystemConfigModel->add(array('skey'=>'trackeventtype99', 'sval'=>10, 'parentid'=>$data['trackeventtypeid'] ));
        }
        $data['mgignoredevice'] = $this->SystemConfigModel->getOne(array('skey'=>'mgignoredevice'), 'sval');
        $data['taskignoredevice'] = $this->SystemConfigModel->getOne(array('skey'=>'taskignoredevice'), 'sval');
        $data['antifraudswitch'] = $this->SystemConfigModel->getOne(array('skey'=>'antifraudswitch'), 'sval');
        $data['popupignoredevice'] = $this->SystemConfigModel->getOne(array('skey'=>'popupignoredevice'), 'sval');
        $data['dspcutovercount'] = $this->SystemConfigModel->getOne(array('skey'=>'dspcutovercount'), 'sval');
//        $data['sdkaccumulatepointsdelay'] = $this->SystemConfigModel->getOne(array('skey'=>'sdkaccumulatepointsdelay'), 'sval');
//        $data['serviceaccumulatepointsdelay'] = $this->SystemConfigModel->getOne(array('skey'=>'serviceaccumulatepointsdelay'), 'sval');
		$data['trackeventtype1'] = $this->SystemConfigModel->getOne(array('skey'=>'trackeventtype1'), 'sval');
		$data['trackeventtype2'] = $this->SystemConfigModel->getOne(array('skey'=>'trackeventtype2'), 'sval');
		$data['trackeventtype3'] = $this->SystemConfigModel->getOne(array('skey'=>'trackeventtype3'), 'sval');
		$data['trackeventtype4'] = $this->SystemConfigModel->getOne(array('skey'=>'trackeventtype4'), 'sval');
		$data['trackeventtype5'] = $this->SystemConfigModel->getOne(array('skey'=>'trackeventtype5'), 'sval');
		$data['trackeventtype6'] = $this->SystemConfigModel->getOne(array('skey'=>'trackeventtype6'), 'sval');
		$data['trackeventtype7'] = $this->SystemConfigModel->getOne(array('skey'=>'trackeventtype7'), 'sval');
		$data['trackeventtype8'] = $this->SystemConfigModel->getOne(array('skey'=>'trackeventtype8'), 'sval');
		$data['trackeventtype9'] = $this->SystemConfigModel->getOne(array('skey'=>'trackeventtype9'), 'sval');
		$data['trackeventtype99'] = $this->SystemConfigModel->getOne(array('skey'=>'trackeventtype99'), 'sval');
        $this->assign('mgignoredevice', $data['mgignoredevice']);
        $this->assign('taskignoredevice', $data['taskignoredevice']);
        $this->assign('antifraudswitch', $data['antifraudswitch']);
        $this->assign('popupignoredevice', $data['popupignoredevice']);
        $this->assign('dspcutovercount', $data['dspcutovercount']);
//        $this->assign('sdkaccumulatepointsdelay', $data['sdkaccumulatepointsdelay']);
//        $this->assign('serviceaccumulatepointsdelay', $data['serviceaccumulatepointsdelay']);
		$this->assign('trackeventtype1', $data['trackeventtype1']);
		$this->assign('trackeventtype2', $data['trackeventtype2']);
		$this->assign('trackeventtype3', $data['trackeventtype3']);
		$this->assign('trackeventtype4', $data['trackeventtype4']);
		$this->assign('trackeventtype5', $data['trackeventtype5']);
		$this->assign('trackeventtype6', $data['trackeventtype6']);
		$this->assign('trackeventtype7', $data['trackeventtype7']);
		$this->assign('trackeventtype8', $data['trackeventtype8']);
		$this->assign('trackeventtype9', $data['trackeventtype9']);
		$this->assign('trackeventtype99', $data['trackeventtype99']);
        $this->display('admin/globals/preloadset.html');
    }

    /**
     * web积分墙应用地址
     * Enter description here ...
     */
    function webintegralurl(){
        $this->load->model('SystemConfigModel');
        $data['webintegralurl'] = $this->SystemConfigModel->getOne(array('skey'=>'webintegralurl'), 'sval');
        if($data['webintegralurl'] === null)
        {
            $data['webintegralurl'] = 'http://wbw.cocounion.com';
            $this->SystemConfigModel->add(array('skey'=>'webintegralurl', 'sval'=>$data['webintegralurl']));
        }
        $data['webintegralerrurl'] = $this->SystemConfigModel->getOne(array('skey'=>'webintegralerrurl'), 'sval');
        if($data['webintegralerrurl'] === null)
        {
            $data['webintegralerrurl'] = 'http://wbw.cocounion.com/pointout.html';
            $this->SystemConfigModel->add(array('skey'=>'webintegralerrurl', 'sval'=>$data['webintegralerrurl']));
        }
        $this->assign('webintegralurl', $data['webintegralurl']);
        $this->assign('webintegralerrurl', $data['webintegralerrurl']);
        $this->display('admin/globals/webintegralurl.html');
    }

    function ajaxwebintegralurlset(){
        $settype = $this->input->post('settype');
        $val = trim($this->input->post('val'));
        if ( ! filter_var($val, FILTER_VALIDATE_URL)) {
            ajaxReturn('请输入正常URL！保存失败', 0);
        }
        if($settype != 'webintegralurl' && $settype != 'webintegralerrurl' )
        {
            ajaxReturn('参数错误，请刷新重试！保存失败', 0);
        }

        $user = $this->session->userdata("admin");
        $this->load->model('SystemConfigModel');
        $where['skey'] = $settype;
        $data['sval'] = $val;
        $status = $this->SystemConfigModel->edit($where, $data);
        if($status){
            ajaxReturn('修改成功', 1);
        }
        ajaxReturn('修改失败', 0);
    }

    function ajaxPreloadSet(){
        $settype = $this->input->post('settype');
        $val = (int)$this->input->post('val');
        if(!preg_match("/^\d/", $val)){
            ajaxReturn('时间格式设置错误！保存失败', 0);
        }
        if($val<0)
        {
            ajaxReturn('时间不能小于一小时！保存失败', 0);
        }
        $user = $this->session->userdata("admin");
        $this->load->model('SystemConfigModel');
        $where['skey'] = $settype;
        $data['sval'] = $val;
        $status = $this->SystemConfigModel->edit($where, $data);
        if($status){
            ajaxReturn('修改成功', 1);
        }
        ajaxReturn('修改失败', 0);
    }

    function ajaxEcpcSave(){
        $data = array();
        $data['ostype'] = (int)$this->input->post('ostype');
        $data['ratio'] = $this->input->post('ratio');
        if(!preg_match("/^(\d{1,3})(\.\d{0,2})?$/", $data['ratio'])){
            ajaxReturn('单价或分成比例格式错误！保存失败', 0);
        }
        $data['status'] = 1;
        $data['addtime'] = time();
        $user = $this->session->userdata("admin");
        $data['admin_id'] = $user['admin_id'];
        $data['adtype'] = (int)$this->input->post('adtype');
        if($data['adtype']<1 || $data['ostype']<1 || $data['ratio']<=0) ajaxReturn('数据格式错误！保存失败'.print_r($data, true), 0);
        $status = $this->AdEcpcModel->add($data);
        if($status){
            $this->AdEcpcModel->edit(array("status"=>1, "addtime" => array("lt", $data['addtime']), "ostype"=>$data['ostype'], "adtype"=>$data['adtype']),array("uptime"=>$data['addtime'],"status"=>0));
            ajaxReturn('eCPC修改成功', 1);
        }
        ajaxReturn('eCPC修改失败', 0);
    }

    function ajaxGetAppCpc(){
        $where = '';
        if($this->input->post('search_os')){
            $where .= ' and app_info.ostypeid = '.(int)$this->input->post('search_os');
        }
        if($this->input->post('seach_app')){
            //$where .= ' and app_info.appname like "%'.$this->input->post('seach_app').'%"';
            $where .= ' and app_info.appname = "'.$this->input->post('seach_app').'"';
        }
        $where .= " and ad_ecpc_app.status = 1";
        $this->load->model('AppInfoModel');
        $fields = 'app_info.appname,app_info.appid,app_info.ostypeid,ifnull(ad_ecpc_app.banner,"") banner,ifnull(ad_ecpc_app.moregame,"") moregame,ifnull(ad_ecpc_app.popup, "") popup';
        $list = $this->AppInfoModel->getAppCpc($where, $fields);
        if($list){
            ajaxReturn('',1,$list);
        }
        ajaxReturn('查询失败', 0, '');
    }

    function ajaxappcpcset(){
        $data = array();
        $where = array();
        $data['appid'] = $this->input->post('appid');
        if(empty($data['appid'])){
            ajaxReturn('参数错误', 0, '');
        }
        if($data['appid']){
            $where['appid'] = $data['appid'];
        }
        $data['addtime'] = time();
        $data['banner'] = $this->input->post('banner');
        $data['popup'] = $this->input->post('popup');
        $data['moregame'] = $this->input->post('moregame');
        if($data['banner'] <= 0 || $data['popup'] <= 0 || $data['moregame'] <= 0){
            ajaxReturn('eCPC设置项必须都大于0', 0, $data);
        }
        $data['status'] = 1;
        $user = $this->session->userdata("admin");
        $data['admin_id'] = $user['admin_id'];
        $this->load->model('AdEcpcAppModel');
        $this->AdEcpcAppModel->edit(array('appid'=>$where['appid'],'status'=>1), array('uptime'=>$data['addtime']));
        $status = $this->AdEcpcAppModel->add($data);
        if($status){
            $this->AdEcpcAppModel->edit(array('appid'=>$where['appid'],'status'=>1, 'uptime'=>$data['addtime']), array('status'=>0));
            ajaxReturn('修改成功！', 1, 0);
        }
        ajaxReturn('修改失败！', 0, 0);
    }

    function clickratio(){
        $tab = (int)$this->input->get('tab');
        $rate = array('ratio'=>0);
        $this->assign('moregame', $rate);
        $this->assign('banner', $rate);
        $this->assign('pop', $rate);
        $this->assign('tab', $tab);
        $this->assign('search', '');
        $this->load->model('ClickRatioModel');
        $ecpc = $this->ClickRatioModel->getDefaultRatio();
        $this->assign('ecpc', $ecpc);
        $list = $this->ClickRatioModel->getList(array(), "*", "ostype asc,addtime desc, adtype asc");
        $ecpclist = array();
        foreach($list as $val){
            $ecpclist[$val['ostype']][$val['addtime']][$val['adtype']] = $val;
        }
        $this->load->model('AdminUserModel');
        $this->assign('ecpclist', $ecpclist);
        $this->assign('admins', $this->AdminUserModel->getAdminName());
        $this->display('admin/globals/clickratio.html');
    }

    function ajaxratiosave(){
        $data = array();
        $data['ostype'] = (int)$this->input->post('ostype');
        $data['ratio'] = $this->input->post('ratio');
        $data['chargemode'] = $this->input->post('chargemode');
        if(!preg_match("/^(\d{1,3})(\.\d{0,2})?$/", $data['ratio'])){
            ajaxReturn('系数格式错误！保存失败', 0);
        }
        $data['status'] = 1;
        $data['addtime'] = time();
        $user = $this->session->userdata("admin");
        $data['admin_id'] = $user['admin_id'];
        $data['adtype'] = (int)$this->input->post('adtype');
        //print_r($data);exit;

        $this->load->model('ClickRatioModel');
        if($data['adtype']<1 || $data['ostype']<1 || $data['chargemode']<1 || $data['ratio']<=0) ajaxReturn('数据格式错误！保存失败'.print_r($data, true), 0);
        $status = $this->ClickRatioModel->add($data);
        if($status){
            $this->ClickRatioModel->edit(array("status"=>1, "addtime" => array("lt", $data['addtime']), "ostype"=>$data['ostype'], "adtype"=>$data['adtype'], "chargemode"=>$data['chargemode']),array("uptime"=>$data['addtime'],"status"=>0));
            ajaxReturn('修改成功！', 1);
        }
        ajaxReturn('修改失败！', 0);
    }

    function sdkversion(){
        $this->load->library('MyUpload');
        $this->load->helper('tools');
        $this->load->model('SdkversionLogModel');
        $this->load->model('ApiversionLogModel');
        if($this->input->get_post('name')){  
        	$data['name'] =  $this->input->get_post('name');
            $data['version'] =  $this->input->get_post('version');
            $data['size'] =  $this->input->get_post('size');
            $data['type'] =  $this->input->get_post('type');
            $data['sdktype'] =  $this->input->get_post('sdktype');
            $data['content'] =  $this->input->get_post('content');
            $data['updatetime'] = time();
            if(empty($_FILES['icon']['name'])){
                $this->message->msg('请上传SDK文件',"/admin/globals/sdkversion");
            }else if(!empty($_FILES['icon']['name'])){
                $sdk_upload =$this->upload($_FILES['icon']);
                if($sdk_upload['error']){
                    $this->message->msg("SDK文件上传失败:".$sdk_upload['error'],"/admin/globals/sdkversion");
                }
                $data['packpath']=base_url().'public/upload'.$sdk_upload['path'];
                $data['md5sum'] = md5_file(UPLOAD_DIR.$sdk_upload['path']);
            }
            if( $this->SdkversionLogModel->add($data)){
                $this->message->msg('上传成功','/admin/globals/sdkversion');
                return;
            }else{
                $this->message->msg('上传失败','/admin/globals/sdkversion');
                return;
            }
        }
        $sdk_list = $this->SdkversionLogModel->getList(array(),'*',"id desc");
        $this->smarty->assign('sdk_list',$sdk_list);
        $this->load->display("admin/globals/sdkversion.html");
    }

     function apiversion(){
        $this->load->library('MyUpload');
        $this->load->helper('tools');
        $this->load->model('ApiversionLogModel');
        if($this->input->get_post('apiname')){
        	$dataapi=array();
        	$dataapi['name'] =  $this->input->get_post('apiname');
            $dataapi['version'] =  $this->input->get_post('apiversion');
            $dataapi['size'] =  $this->input->get_post('apisize');
            $dataapi['content'] =  $this->input->get_post('apicontent');
            $dataapi['updatetime'] = time();             
            if(empty($_FILES['icon']['name'])){
                $this->message->msg('请上传API文件',"/admin/globals/apiversion");
            }else if(!empty($_FILES['icon']['name'])){
                $api_upload =$this->upload($_FILES['icon']);
                if($api_upload['error']){
                    $this->message->msg("API文件上传失败:".$api_upload['error'],"/admin/globals/apiversion");
                }
                $dataapi['packpath']=base_url().'public/upload'.$api_upload['path'];
                $dataapi['md5sum'] = md5_file(UPLOAD_DIR.$api_upload['path']);
            }
            if( $this->ApiversionLogModel->add($dataapi)){
                $this->message->msg('上传成功','/admin/globals/apiversion');
                return;
            }else{
                $this->message->msg('上传失败','/admin/globals/apiversion');
                return;
            }
        }
        $api_list = $this->ApiversionLogModel->getList(array(),'*',"id desc");
        $this->smarty->assign('api_list',$api_list);
        $this->load->display("admin/globals/apiversion.html");
    }

    function dynasdkoperation(){
    	$postdata = $this->input->post();
    	$this->load->model('SdkDynamicModel');
    	$data['name'] = trim($postdata['name']);
    	$data['content'] = trim($postdata['content']);
    	$data['size'] = trim($postdata['size']);
    	
    	$baseRow = $this->SdkDynamicModel->getRow(array('id'=>$postdata['baseid']));
    	if(!$baseRow) ajaxReturn('未知错误，请刷新重试', 0, '');
    	$maxversion = $this->SdkDynamicModel->getmaxversion(2, $postdata['baseid']);
    	
    	if($postdata['operation_type']=='add'){
    		$data['version'] = trim($postdata['version']);
	    	if(strnatcasecmp($data['version'], $maxversion) <= 0){
	    		ajaxReturn('版本号应大于已有版本号', 0);
	    	}
    		if(!isset($postdata['md5sum']) || empty($postdata['md5sum'])){
    			ajaxReturn('请上传SDK文件', 0, '');
    		}
    		$data['baseid'] = $postdata['baseid'];
    		$data['ostype'] = $postdata['ostype'];
    		$data['md5sum'] = $postdata['md5sum'];
    		$data['packpath'] = $postdata['packpath'];
    		$data['sdktype'] = 2;
    		$data['createtime'] = time();
    		$data['updatetime'] = time();
    		$insertid = $this->SdkDynamicModel->add($data);
    		if($insertid){
    			$json_data=array();
    			$json_data['result'] = true;
    			$json_data['id'] = $insertid;
    			$json_data['os'] = $data['ostype'];
    			$components['version'] = $data['version'];
    			$components['componentname'] = $data['name'];
    			$components['url'] = $data['packpath'];
    			$components['checksum'] = $data['size'];
    			$components['date'] = $data['createtime'];
    			$components['description'] = $data['content'];
    			$json_data['sdkinfo'] = array('baseversion'=>$baseRow['version'],'basename'=>$baseRow['name'],'description'=>$baseRow['content'],'components'=>$components);
	    		$file = UPLOAD_SDK_DYNAMIC_CONFIG.'/'.$this->sdkostype[$data['ostype']].'/'.$baseRow['version'].'/index.html';
			    if (is_file($file)) {
			        unlink($file);
			    }
    			file_put_contents(UPLOAD_SDK_DYNAMIC_CONFIG.'/'.$this->sdkostype[$data['ostype']].'/'.$baseRow['version'].'/index.html',json_encode($json_data));
    			ajaxReturn('', 1, '');
    		}
    	}
    	if($postdata['operation_type']=='edit'){
    		if(isset($postdata['md5sum']) && !empty($postdata['md5sum'])){
    			$data['md5sum'] = $postdata['md5sum'];
    			$data['packpath'] = $postdata['packpath'];
    		}
    		$data['updatetime'] = time();
    		$editInfo =  $this->SdkDynamicModel->edit(array('id'=>$postdata['id']),$data);
    		$rowData = $this->SdkDynamicModel->getRow(array('id'=>$postdata['id']));
    		if( $editInfo !== null){
    			$json_data=array();
    			$json_data['result'] = true;
    			$json_data['id'] = $rowData['id'];
    			$json_data['os'] = $rowData['ostype'];
    			$components['version'] = $rowData['version'];
    			$components['componentname'] = $rowData['name'];
    			$components['url'] = $rowData['packpath'];
    			$components['checksum'] = $rowData['size'];
    			$components['date'] = $rowData['createtime'];
    			$components['description'] = $rowData['content'];
    			$json_data['sdkinfo'] = array('baseversion'=>$baseRow['version'],'basename'=>$baseRow['name'],'description'=>$baseRow['content'],'components'=>$components);
	    		if($rowData['version'] == $maxversion)
	    		{
	    			$file = UPLOAD_SDK_DYNAMIC_CONFIG.'/'.$this->sdkostype[$rowData['ostype']].'/'.$baseRow['version'].'/index.html';
				    if (is_file($file)) {
				        unlink($file);
				    }
	    			file_put_contents(UPLOAD_SDK_DYNAMIC_CONFIG.'/'.$this->sdkostype[$rowData['ostype']].'/'.$baseRow['version'].'/index.html',json_encode($json_data));
	    		}
    			ajaxReturn('', 1, '');
    		}
    	}
    }

    function getdynamicsdkrow(){
    	$id = $this->input->get('id');
    	$baseid = $this->input->get('baseid');
    	$this->load->model('SdkDynamicModel');
    	$info = $this->SdkDynamicModel->getRow(array('id'=>$id));
    	$maxversion = $this->SdkDynamicModel->getmaxversion(2, $baseid);
		$info['maxversion'] = $maxversion;    	
    	ajaxReturn('', 1, $info);
    }

    function dynasdkupload(){
        $this->load->library('MyUpload');
        $this->load->helper('tools');
        $returnArray = array('isReceived' => false);
    	if(empty($_FILES['dynasdkfile']['name'])){
    		$returnArray['errorMessage'] = '未知错误';
    		echo json_encode(array($returnArray));
    		return;
        }else{
            $sdk_upload =$this->upload_keepname($_FILES['dynasdkfile'],UPLOAD_SDK_DYNAMIC_PACKAGE);
            if($sdk_upload['error']){
	    		$returnArray['errorMessage'] = "SDK文件上传失败:".$sdk_upload['error'];
	    		echo json_encode(array($returnArray));
	    		return;
            }
            $returnArray['isReceived']=true;
            $returnArray['packpath']=base_url().'public/upload'.$sdk_upload['path'];
            $returnArray['md5sum'] = md5_file(UPLOAD_DIR.$sdk_upload['path']);
            $returnArray['filename'] = $_FILES['dynasdkfile']['name'];
            echo json_encode(array($returnArray));
            return;
        }
    }

    //SDK动态更新-基础框架版本
    function sdkdynamicbase(){
    	$this->load->model('SdkDynamicModel');
    	$sdkList = $this->SdkDynamicModel->getList(array('sdktype'=>1, 'status'=>1),'*','id desc');
    	$this->smarty->assign('sdkList', $sdkList);
    	$this->load->display("admin/globals/sdkdynamicbase.html");
    }

    //添加基础框架版本
    function addbasesdk(){
    	$postdata = $this->input->post();
    	$data['ostype'] = trim($postdata['ostype']);
    	$data['name'] = trim($postdata['name']);
    	$data['version'] = trim($postdata['version']);
    	$data['content'] = trim($postdata['content']);
    	$this->load->model('SdkDynamicModel');
    	$maxversion = $this->SdkDynamicModel->getmaxversion(1);
    	if(strnatcasecmp($data['version'], $maxversion) <= 0){
    		ajaxReturn('版本号应大于已有版本号', 0);
    	}
    	$data['createtime'] = time();
    	$data['updatetime'] = time();
    	if($this->SdkDynamicModel->add($data)){
    		createDir(UPLOAD_SDK_DYNAMIC_CONFIG.'/'.$this->sdkostype[$data['ostype']].'/'.$data['version']);
    		ajaxReturn('添加成功', 1);
    	}else{
    		ajaxReturn('添加失败', 0);
        }
    }
    
    //修改基础框架版本
    function editbasesdk($id=''){
    	$this->load->model("SdkDynamicModel");
    	$sdkInfo = $this->SdkDynamicModel->getRow(array('id'=>$id));
    	$this->smarty->assign('applyinfo',$sdkInfo);
    	if($this->input->get_post('name')){
    		$id = $this->input->get_post('id');
    		$data['name'] = $this->input->get_post('name');
    		$data['content'] = $this->input->get_post('content');
    		$data['updatetime'] = time();
    		$editInfo = $this->SdkDynamicModel->edit(array('id'=>$id),$data);
            if( $editInfo !== null){
	            $file = UPLOAD_SDK_DYNAMIC_CONFIG.'/'.$this->sdkostype[$this->input->get_post('baseostype')].'/'.$this->input->get_post('baseversion').'/index.html';
			    if (is_file($file)) {
			        $json_string = file_get_contents($file);
			        $json_array = json_decode($json_string,true);
			        $json_array['sdkinfo']['basename']=$data['name'];
			        $json_array['sdkinfo']['description']=$data['content'];
			        file_put_contents($file, json_encode($json_array));
			    }
                $this->message->msg('编辑成功','/admin/globals/sdkdynamicbase');
                return;
            }else{
                $this->message->msg('编辑失败','/admin/globals/sdkdynamicbase');
                return;
            }
    	}
    	$this->load->display("admin/globals/sdkbaseedit.html");
    }

    //删除基础框架版本SDK
    function delete_base_sdk(){
    	$this->load->model('SdkDynamicModel');
    	$id = $this->input->post('id');
    	$ostype = $this->input->post('ostype');
    	$row = $this->SdkDynamicModel->getRow(array('id'=>$id));
    	if($this->SdkDynamicModel->edit(array('id'=>$id), array('status'=>0))){
    		$this->SdkDynamicModel->edit(array('baseid'=>$id), array('status'=>0));
    		$files = glob(UPLOAD_SDK_DYNAMIC_CONFIG.'/'.$this->sdkostype[$ostype].'/'.$row['version'].'/*');
			foreach ($files as $file) {
			    if (is_file($file)) {
			        unlink($file);
			    }
			}
			rmdir(UPLOAD_SDK_DYNAMIC_CONFIG.'/'.$this->sdkostype[$ostype].'/'.$row['version']);
            ajaxReturn('', 1);
        }else{
            ajaxReturn('删除失败！', 0);
        }
    }

    //SDK动态更新版本
    function sdkdynamic($id=''){
    	$this->load->model('SdkDynamicModel');
    	if(empty($id)){
    		$where = array('sdktype'=>1,'status'=>1);
    	}else{
    		$where = array('id'=>$id);
    	}
    	$sdkList = array();
    	$baseSdkList = $this->SdkDynamicModel->getList($where,'*','id desc');
    	foreach($baseSdkList as $k=>$baseSdk){
    		$dynaSdkList = $this->SdkDynamicModel->getList(array('sdktype'=>2, 'baseid'=>$baseSdk['id'], 'status'=>1),'*','id desc');
	    	foreach($dynaSdkList as $k=>$v){
	    		$v['basename'] = $baseSdk['name'];
	    		$sdkList[]=$v;
	    	}
    	}
    	$this->smarty->assign('sdkList',$sdkList);
    	$this->load->display("admin/globals/sdkdynamic.html");
    }

    function sdkdynamicmodal(){
    	$this->load->display("admin/globals/sdkdynamicmodal.html");
    }
    
    function delete_dynamic_sdk(){
    	$this->load->model('SdkDynamicModel');
    	$id = $this->input->post('id');
    	$baseid = $this->input->post('baseid');
    	$sdkRow = $this->SdkDynamicModel->getRow(array('id'=>$id));
    	$baseRow = $this->SdkDynamicModel->getRow(array('id'=>$baseid));
    	$maxversion = $this->SdkDynamicModel->getmaxversion(2, $baseid);
    	if($this->SdkDynamicModel->edit(array('id'=>$id), array('status'=>0))){
    		if($sdkRow['version'] == $maxversion){
    			$file = UPLOAD_SDK_DYNAMIC_CONFIG.'/'.$this->sdkostype[$sdkRow['ostype']].'/'.$baseRow['version'].'/index.html';
			    if (is_file($file)) {
			        unlink($file);
			    }
    			$newRow = $this->SdkDynamicModel->getLatestRow(2,$baseid);
    			if($newRow){
	    			$json_data=array();
	    			$json_data['result'] = true;
	    			$json_data['id'] = $newRow['id'];
	    			$json_data['os'] = $newRow['ostype'];
	    			$components['version'] = $newRow['version'];
	    			$components['componentname'] = $newRow['name'];
	    			$components['url'] = $newRow['packpath'];
	    			$components['checksum'] = $newRow['size'];
	    			$components['date'] = $newRow['createtime'];
	    			$components['description'] = $newRow['content'];
	    			$json_data['sdkinfo'] = array('baseversion'=>$baseRow['version'],'basename'=>$baseRow['name'],'description'=>$baseRow['content'],'components'=>$components);
		    		file_put_contents(UPLOAD_SDK_DYNAMIC_CONFIG.'/'.$this->sdkostype[$newRow['ostype']].'/'.$baseRow['version'].'/index.html',json_encode($json_data));
    			}
    		}
            ajaxReturn('删除成功！', 1);
        }else{
            ajaxReturn('删除失败！', 0);
        }
    }
    
    function sdkedit($id=''){
        $this->load->model('SdkversionLogModel');
        $sdk_info = $this->SdkversionLogModel->getRow(array('id'=>$id));
        $this->smarty->assign('applyinfo',$sdk_info);
        if( $this->input->get_post('name')){
            $id =  $this->input->get_post('id');
            $data['name'] =  $this->input->get_post('name');
            $data['version'] =  $this->input->get_post('version');
            $data['size'] =  $this->input->get_post('size');
            $data['type'] =  $this->input->get_post('type');
            $data['content'] =  $this->input->get_post('content');
            $data['sdktype']=$this->input->get_post('sdktype');
            $res = $this->SdkversionLogModel->edit(array('id'=>$id),$data);
             
            if( $res !== null){
                $this->message->msg('编辑成功','/admin/globals/sdkversion');
                return;
            }else{
                $this->message->msg('编辑失败','/admin/globals/sdkversion');
                return;
            }
        }
        $sdk_list = $this->SdkversionLogModel->getList(array(),'*',"id desc");
        $this->smarty->assign('sdk_list',$sdk_list);
        $this->load->display("admin/globals/sdkedit.html");
    }

    
    /**
     * 编辑API信息
     * @param $id
     */
	function apiedit($id=''){
        $this->load->model('ApiversionLogModel');
        $api_info = $this->ApiversionLogModel->getRow(array('id'=>$id));
        $this->smarty->assign('applyinfo',$api_info);
        if( $this->input->get_post('name')){
            $id =  $this->input->get_post('id');
            $data['name'] =  $this->input->get_post('name');
            $data['version'] =  $this->input->get_post('version');
            $data['size'] =  $this->input->get_post('size');
            $data['content'] =  $this->input->get_post('content');
            $res = $this->ApiversionLogModel->edit(array('id'=>$id),$data);
            if( $res !== null){
                $this->message->msg('编辑成功','/admin/globals/apiversion');
                return;
            }else{
                $this->message->msg('编辑失败','/admin/globals/apiversion');
                return;
            }
        }
        $api_list = $this->ApiversionLogModel->getList(array(),'*',"id desc");
        $this->smarty->assign('api_list',$api_list);
        $this->load->display("admin/globals/apiedit.html");
    }
    
    
    /**
     * 上传用户文件
     * @param $name   $_FILES['img'] 页面file名字
     * @return string 返回被保存的文件的数据库格式
     */

    private function upload($name,$path=UPLOAD_DOC_PATH,$size=10240)
    {
        $return = array('path'=>'','error'=>array());
        $MyUpload = new MyUpload($name);
        $MyUpload->setUploadPath($path);
        if($MyUpload->upload())
        {
            $return['path'] = $MyUpload->getUplodedFilePath();
        }else{
            $return['error'] = '未知错误，上传失败';
        }

        return $return;
    }
    
    //上传文件，保存文件时不更改文件的名字
	private function upload_keepname($name,$path=UPLOAD_DOC_PATH,$size=10240)
    {
        $return = array('path'=>'','error'=>array());
        $MyUpload = new MyUpload($name);
        $MyUpload->setUploadPath($path);
        if($MyUpload->upload(true,false))
        {
            $return['path'] = $MyUpload->getUplodedFilePath();
        }else{
            $return['error'] = '未知错误，上传失败';
        }

        return $return;
    }

    function delete_sdk(){
        $this->load->model('SdkversionLogModel');
        if($this->SdkversionLogModel->delete(array('id'=>$this->input->post('id'))))
        {
            ajaxReturn('删除成功！', 1);
        }else{
            ajaxReturn('删除失败！', 0);
        }
    }

    /**
     * 删除api
     */
    function delete_api(){
        $this->load->model('ApiversionLogModel');
        if($this->ApiversionLogModel->delete(array('id'=>$this->input->post('id')))){
        	ajaxReturn('删除成功！', 1);
        }
        else{
            ajaxReturn('删除失败！', 0);
        }
    }
    
    function ajaxappsetsave(){
        $where = array();
        $where['appid'] = $this->input->post('appid');
        $data = array();
        $data['appid'] = $this->input->post('appid');
        $data['banner'] = $this->input->post('banner');
        $data['bannerratio'] = $this->input->post('bannerratio');
        $data['bannertype'] = $this->input->post('bannertype');
        $data['popup'] = $this->input->post('popup');
        $data['popupratio'] = $this->input->post('popupratio');
        $data['popuptype'] = $this->input->post('popuptype');
        $data['moregame'] = $this->input->post('moregame');
        $data['moregameratio'] = $this->input->post('moregameratio');
        $data['moregametype'] = $this->input->post('moregametype');
        $data['launch'] = $this->input->post('launch');
        $data['launchratio'] = $this->input->post('launchratio');
        $data['launchtype'] = 2;
        $data['wall'] = $this->input->post('wall');
        $data['wallratio'] = $this->input->post('wallratio');
        $data['walltype'] = 3;
        $data['feeds'] = $this->input->post('feeds');
        $data['feedsratio'] = $this->input->post('feedsratio');
        $data['feedstype'] = $this->input->post('feedstype');
        $data['video'] = $this->input->post('video');
        $data['videoratio'] = $this->input->post('videoratio');
        $data['videotype'] = $this->input->post('videotype');
        if(!preg_match("/^(\d{1,3})(\.\d{0,2})?$/", $data['banner'])){
            ajaxReturn('banner分成比例格式错误！保存失败', 0);
        }
        if(!preg_match("/^(\d{1,3})(\.\d{0,2})?$/", $data['moregame'])){
            ajaxReturn('精品推荐分成比例格式错误！保存失败', 0);
        }
        if(!preg_match("/^(\d{1,3})(\.\d{0,2})?$/", $data['popup'])){
            ajaxReturn('弹出广告分成比例格式错误！保存失败', 0);
        }
        if(!preg_match("/^(\d{1,3})(\.\d{0,2})?$/", $data['launch'])){
            ajaxReturn('开屏广告分成比例格式错误！保存失败', 0);
        }
        if(!preg_match("/^(\d{1,3})(\.\d{0,2})?$/", $data['wall'])){
            ajaxReturn('积分墙广告分成比例格式错误！保存失败', 0);
        }
        if(!preg_match("/^(\d{1,3})(\.\d{0,2})?$/", $data['feeds'])){
            ajaxReturn('信息流广告单价格式错误！保存失败', 0);
        }
        if(!preg_match("/^(\d{1,3})(\.\d{0,2})?$/", $data['bannerratio'])){
            ajaxReturn('banner分成比例格式错误！保存失败', 0);
        }
        if(!preg_match("/^(\d{1,3})(\.\d{0,2})?$/", $data['moregameratio'])){
            ajaxReturn('精品推荐分成比例格式错误！保存失败', 0);
        }
        if(!preg_match("/^(\d{1,3})(\.\d{0,2})?$/", $data['popupratio'])){
            ajaxReturn('弹出广告分成比例格式错误！保存失败', 0);
        }
        if(!preg_match("/^(\d{1,3})(\.\d{0,2})?$/", $data['launchratio'])){
            ajaxReturn('开屏广告分成比例格式错误！保存失败', 0);
        }
        if(!preg_match("/^(\d{1,3})(\.\d{0,2})?$/", $data['wallratio'])){
            ajaxReturn('积分墙广告分成比例格式错误！保存失败', 0);
        }
        if(!preg_match("/^(\d{1,3})(\.\d{0,2})?$/", $data['feedsratio'])){
            ajaxReturn('信息流广告分成比例格式错误！保存失败', 0);
        }
     	if(!preg_match("/^(\d{1,3})(\.\d{0,2})?$/", $data['videoratio'])){
            ajaxReturn('视频广告分成比例格式错误！保存失败', 0);
        }
        $this->load->model('AdEcpcAppModel');
        $data['uptime'] = 0;
        $data['status'] = 1;
        $data['addtime'] = time();
        $user = $this->session->userdata("admin");
        $data['admin_id'] = $user['admin_id'];
        $this->AdEcpcAppModel->edit(array('appid'=>$where['appid'],'status'=>1), array('uptime'=>$data['addtime']));
        $status = $this->AdEcpcAppModel->add($data);
        if($status){
            $where['uptime'] = $data['addtime'];
            $this->AdEcpcAppModel->edit($where, array('status'=>0));
            ajaxReturn('设置成功！', 1, 0);
        }
        ajaxReturn('保存到数据库时发生错误', 0);
    }
    /**
     * 积分墙作弊返还控制
     */
    function antifraud(){
        $os = $this->input->get('os');
        $this->load->model('SystemConfigModel');
        $data['antifraudios'] = $this->SystemConfigModel->getOne(array('skey'=>'antifraudios'), 'sval');
        $data['antifraudandroid'] = $this->SystemConfigModel->getOne(array('skey'=>'antifraudandroid'), 'sval');
        $data['iostime'] = $this->SystemConfigModel->getOne(array('skey'=>'antifraudios'), 'addtime');
        $data['androidtime'] = $this->SystemConfigModel->getOne(array('skey'=>'antifraudandroid'), 'addtime');
        $data['antifraudswitch'] = $this->SystemConfigModel->getOne(array('skey'=>'antifraudswitch'), 'sval');
         
        $this->assign('data', $data);
        $this->assign('tab', $os);
        $this->display('admin/globals/antifraud.html');
    }
    /**
     * 作弊设置 日志
     */
    function antifraudlog(){
        $this->load->model('AdAntifraudLogModel');
        $antifraud_ios = $this->AdAntifraudLogModel->getList(array('type'=>'1'));
        $antifraud_android = $this->AdAntifraudLogModel->getList(array('type'=>'2'));
        $this->load->model('AdminUserModel');
        foreach($antifraud_ios as $key=>$val){
            $admin_id = $val['adminid'];
            $antifraud_ios[$key]['username'] = $this->AdminUserModel->getOne(array('admin_id'=>$admin_id),'admin_name');
        }
        foreach($antifraud_android as $key=>$val){
            $admin_id = $val['adminid'];
            $antifraud_android[$key]['username'] = $this->AdminUserModel->getOne(array('admin_id'=>$admin_id),'admin_name');
        }
        $data['iostime'] = $this->SystemConfigModel->getOne(array('skey'=>'antifraudios'), 'addtime');
        $data['androidtime'] = $this->SystemConfigModel->getOne(array('skey'=>'antifraudandroid'), 'addtime');
        $data['antifraudswitch'] = $this->SystemConfigModel->getOne(array('skey'=>'antifraudswitch'), 'sval');
        $this->assign('data', $data);
        $this->assign('antifraud_ios', $antifraud_ios);
        $this->assign('antifraud_android', $antifraud_android);
        $this->assign('tab', 0);
        $this->display('admin/globals/antifraudlog.html');

    }
    /**
     * 作弊返还设置保存
     */
    function ajaxAntifraud(){
        $settype = $this->input->post('settype');
        $val = (int)$this->input->post('val');
        if(!preg_match("/^\d/", $val)){
            ajaxReturn('数字格式错误！保存失败', 0);
        }
        if($val<0)
        {
            ajaxReturn('不能小于0！保存失败', 0);
        }
        $this->load->model('SystemConfigModel');
        $where['skey'] = $settype;
        $data['sval'] = $val;
        $data['addtime'] = time();
        $status = $this->SystemConfigModel->edit($where, $data);
        if($status){
            //保存日志
            $this->load->model('AdAntifraudLogModel');
            $user = $this->session->userdata("admin");
            $adddata['ratio'] = $val;
            $adddata['adminid'] = $user['admin_id'];
            if($settype == 'antifraudios') $adddata['type'] = 1;
            if($settype == 'antifraudandroid') $adddata['type'] = 2;
            $adddata['addtime'] = time();
            $this->AdAntifraudLogModel->add($adddata);
            ajaxReturn('修改成功', 1);
        }
        ajaxReturn('修改失败', 0);
    }
    /**
     * 给组设置 作弊返还
     */
    function integral(){
        $os = (int)$this->input->get('os');
        if(!$os){
            $os = 2;
        }
        $this->assign('os', $os);
        $this->load->model('AdStuffIntegralModel');
        $search = $this->input->post('search');
        $list = $this->AdStuffIntegralModel->getMoregameTop($os,'',$search, '');
        $stuffids = '';
        $tlist = array();
        foreach($list as $key=>$val){
            $taskinfo = $this->CommonModel->table("ad_task")->getRow(array("stuffid"=>$val['stuffid']));
            $list[$key]['tasktype'] = $taskinfo['tasktype'];
            $list[$key]['taskname'] = $taskinfo['taskname'];
            $stuffids .= $val['stuffid'].',';
            $tlist[$val['stuffid']] = array();
        }
        if($stuffids){
            $stuffids = rtrim($stuffids, ',');
            $tasklist = $this->CommonModel->table("ad_task")->getList(array("stuffid"=>array("in", $stuffids)));
            foreach($tasklist as $val){
                $tlist[$val['stuffid']][] = $val;
            }
        }
        $this->assign('tlist', $tlist);
        $this->assign('list', $list);
        $this->assign('tab', $os);
        $this->display('admin/globals/integral.html');
    }
    /**
     * 修改组的 作弊返还比例
     */
    function  editintegral(){
        $this->load->model('AdStuffIntegralModel');
        $adgroupid = (int)$this->input->get('adgroupid');
        $os = (int)$this->input->get('os');
        $edit_flg = $this->input->post('edit_flg');
        $antifraud = $this->input->post('antifraud');
        if($edit_flg){
            $this->load->model("AdGroupModel");
            $num = $this->AdGroupModel->edit(array('adgroupid'=>$adgroupid), array('antifraud'=>$antifraud));
            redirect('/admin/globals/integral?os='.$os);
        }
         
        $groupinfo = $this->AdStuffIntegralModel->getMoregamebyid($adgroupid);
        $taskinfo = $this->CommonModel->table("ad_task")->getRow(array("stuffid"=>$groupinfo['stuffid']));
        $groupinfo['tasktype'] = $taskinfo['tasktype'];
        $groupinfo['taskname'] = $taskinfo['taskname'];
        $this->assign('tab', $os);
        $this->assign('groupinfo', $groupinfo);
        $this->display('admin/globals/editintegral.html');
    }
    
    /**
     * 查询公告标题是否重复
     * @return string 1是有记录，0 是无记录
     */
    function gamePublishUniqueCheck() {
    	$this->load->model('AdChannelPublicModel');
    	$title = $this->input->get('title');
    	$appid = $this->input->get('appid');
    	
    	
    	$r = $this->AdChannelPublicModel->getRow(array('appid'=>$appid,'title'=>$title,'status'=>3));
    	if(!empty($r)) {
    		echo '1';
    	} else {
    		echo '0';
    	}
    }
    
	/**
     * 边框设置
     */
    function  border(){
    	$this->load->model('AdBorderModel');
    	$sysBorderList = $this->AdBorderModel->getSysBorderList();
    	$list=array();
    	if($sysBorderList)
    	{
    		foreach($sysBorderList as $v)
    		{
    			$list[$v['bordergroupid']][]=$v;
    		}
    	}
        $this->assign('list', $list);
        $this->assign('sysBorderCount', count($list));
        $this->display('admin/globals/border.html');
    }
    
	/**
     * 上传边框
     */
	public function ajaxDoUploadStuff() {
		
        $this->load->model('AdBorderModel');
        $returnArray = array('isReceived' => false, 'errorMessage' => '', 'imgInfo' => '', 'imgs_id' => null);
        if ($_FILES['Filedata']['size'] > 120 * 1024) {
            $returnArray['errorMessage'] = '图片大小超过120kb';
            echo json_encode(array($returnArray));
            return;
        }
        $type = $this->input->get_post('type');
        
        $returnArray = $this->AdBorderModel->upload($type);

        if ($returnArray['isReceived']) {
            $returnArray['imgs_id'] = $type;
        }

        echo json_encode(array($returnArray));
        return;
    }
    
    public function setPositionBorder(){
    	$postdata = $this->input->post();
    	$this->load->model('AppPositionModel');
    	$status = $this->AppPositionModel->editPositionBorder($postdata);
        if($status['status'] == 0)
        {
            ajaxReturn($status['info'], 0);
        }
        ajaxReturn('编辑成功', 1);
    }
    
    public function getBorderById(){
    	$postdata = $this->input->post();
    	$bordergroupid=$postdata['bordergroupid'];
    	$this->load->model('AdBorderModel');
    	$where=array('bordergroupid' =>$bordergroupid, 'type' => 2);
    	$cusBorderinfo = $this->AdBorderModel->getBorderInfo($where);
    	$detail=array();
   		if($cusBorderinfo)
    	{
    		foreach ($cusBorderinfo as $value) {
    			if(!empty($value['sizeid'])){
    				$detail[$value['sizeid']]=$value;
    			}
    			else{
    				$detail['icon']=$value;
    			}
    		}
    	}
    	ajaxReturn('', 1 ,$detail);
    }
    
    public function saveBorder() {
        $this->load->model('AdBorderModel');
		$data['img_border1'] = $this->input->post('img_border1');
		$data['img_border2'] = $this->input->post('img_border2');
		$data['img_border3'] = $this->input->post('img_border3');
		$data['img_border4'] = $this->input->post('img_border4');
		$data['img_border5'] = $this->input->post('img_border5');
		$data['img_border6'] = $this->input->post('img_border6');
		$data['img_border7'] = $this->input->post('img_border7');
	    $status=$this->AdBorderModel->addBorder($data);
	    if($status){
	        ajaxReturn('添加成功', 1);
	    }
	    ajaxReturn('添加失败', 0);
    }
    
    /**
     * 删除一组边框
     */
    function delBorder(){
    	$bordergroupid=$this->input->post('bordergroupid');
    	$this->load->model('AdBorderModel');
        $status = $this->AdBorderModel->delBorder($bordergroupid);
        if($status)
        {
            ajaxReturn('删除成功', 1);
        }
        ajaxReturn('删除失败', 0);
    }
    
    /**
     * 显示编辑边框
     * wf
     */
    function showeditborder(){
    	$bordergroupid=$this->input->get('bordergroupid');
    	$this->load->model('AdBorderModel');
    	$where=array('bordergroupid' =>$bordergroupid, 'type' => 1);
    	$sysBorderinfo = $this->AdBorderModel->getBorderInfo($where);
    	$detail=array();
   		if($sysBorderinfo)
    	{
    		foreach ($sysBorderinfo as $value) {
    			$detail['defaultborder'] = $value['defaultborder'];
    			if($value['sizeid']!=""){
    				$detail[$value['sizeid']]=$value;
    			}
    			else{
    				$detail['icon']=$value;
    			}
    		}
    	}
    	$detail['bordergroupid'] = $bordergroupid;
        $this->assign('detail', $detail);
        $this->display('admin/globals/showeditborder.html');
    }
    
    /**
     * 编辑边框
     * wf
     */
    function editborder(){
    	$postData = $this->input->post();
    	$this->load->model('AdBorderModel');
        $status = $this->AdBorderModel->doEdit($postData);
        if($status['status'] == 0)
        {
            ajaxReturn('编辑边框失败', 0);
        }
        ajaxReturn('编辑边框成功', 1);
    }
    
    /**
     * 修改系统边框状态
     * wf
     */
    function setborderstatus(){
    	$postData=$this->input->post();
    	$this->load->model('AdBorderModel');
    	$status = $this->AdBorderModel->dosetborderstatus($postData);
        if($status)
        {
            ajaxReturn('修改状态成功', 1);
        }
        ajaxReturn('修改状态失败', 0);
    }
    
    /**
     * 设置默认边框
     */
    function setdefaultborder(){
    	$postdata = $this->input->post();
    	$this->load->model('AdBorderModel');
    	$status = $this->AdBorderModel->doSetDefaultBorder($postdata);
    	if($status) ajaxReturn('设置成功', 1);
    	ajaxReturn('设置失败', 0);
    }
    
	/**
     * 公告
     */
    function  noticeboard(){
    	$this->load->model('NoticeBoardModel');
    	$whereable=array('status'=>1, 'type'=>1);
    	$orderby="createtime desc";
    	$ablelist = $this->NoticeBoardModel->getNoticeByWhere($whereable,$orderby);
    	$wheredis=array('status'=>0, 'type'=>1);
    	$orderby="edittime desc";
    	$dislist = $this->NoticeBoardModel->getNoticeByWhere($wheredis,$orderby);
    	$newsInUse = $this->NoticeBoardModel->getNoticeByWhere(array('status'=>1, 'type'=>2), "priority desc");
    	$newsNotInUse = $this->NoticeBoardModel->getNoticeByWhere(array('status'=>0, 'type'=>2), "priority desc");
    	$this->assign('newsInUse', $newsInUse);
    	$this->assign('newsNotInUse', $newsNotInUse);
        $this->assign('ablelist', $ablelist);
        $this->assign('dislist', $dislist);
        $this->display('admin/globals/noticeboard.html');
    }
    
    /*
     * 添加合作公司
     */
    function addpartner(){
    	$postdata = $this->input->post();
    	$data['title'] = trim($postdata['title']);
    	$data['target'] = trim($postdata['target']);
    	$data['priority'] = trim($postdata['priority']);
    	$data['logo'] = isset($postdata['imgpath'])?$postdata['imgpath']:'';
    	$data['status']=1;
        $user = $this->session->userdata("admin");
        $data['adminid'] = $user['admin_id'];
    	$this->load->model('CoPartnerModel');
    	$adInfo = $this->CoPartnerModel->doAdd($data);
	    if($adInfo['status']==0){
	    	ajaxReturn($adInfo['info'], 0, $adInfo['data']);
	    }
	    ajaxReturn('', 1,'');
    }
    /*
     * 编辑合作公司
     */
    function editPartner(){
    	$postdata = $this->input->post();
    	$id = $postdata['id'];
    	$originalImg = $postdata['originalImg'];
    	$data['title'] = trim($postdata['title']);
    	$data['target'] = trim($postdata['target']);
    	$data['priority'] = trim($postdata['priority']);
    	if(isset($postdata['imgpath'])){
    		$data['logo'] = $postdata['imgpath'];	
    	}
        $user = $this->session->userdata("admin");
        $data['adminid'] = $user['admin_id'];
        $this->load->model('CoPartnerModel');
        $editInfo = $this->CoPartnerModel->doEdit($data,$id);
	    if($editInfo['status']==0){
	    	ajaxReturn($editInfo['info'], 0, $editInfo['data']);
	    }
    	if(isset($postdata['imgpath'])){
    		@unlink(UPLOAD_DIR.$originalImg);	
    	}
	    ajaxReturn('', 1,'');
    }
    
    function editNews(){
    	$this->load->model('NoticeBoardModel');
    	$postdata = $this->input->post();
    	$id = $postdata['id'];
    	$originalImg = $postdata['originalImg'];
		$data['title'] = trim($postdata['title']);
		$data['abstract'] = trim($postdata['newsabstract']);
		$data['priority'] = trim($postdata['priority']);
    	if(isset($postdata['imgpath'])){
    		$data['imgpath'] = $postdata['imgpath'];	
    	}
        $user = $this->session->userdata("admin");
        $data['adminid'] = $user['admin_id'];
	    $adInfo=$this->NoticeBoardModel->doEdit($data,$id);
	    if($adInfo['status']==0){
	    	ajaxReturn($adInfo['info'], 0, $adInfo['data']);
	    }
    	if(isset($postdata['imgpath'])){
    		@unlink(UPLOAD_DIR.$originalImg);	
    	}
	    ajaxReturn('', 1,'');
    }
    
    function addContactUs(){
    	$postdata = $this->input->post();
    	$data['title'] = trim($postdata['title']);
    	$data['email'] = trim($postdata['email']);
    	$data['qq'] = trim($postdata['qq']);
    	$data['qqgroup'] = trim($postdata['qqgroup']);
    	$data['phonenum'] = trim($postdata['phonenum']);
    	$data['priority'] = trim($postdata['priority']);
        $user = $this->session->userdata("admin");
        $data['adminid'] = $user['admin_id'];
        $this->load->model('ContactUsModel');
        $adInfo = $this->ContactUsModel->doAdd($data);
	    if($adInfo['status']==0){
	    	ajaxReturn($adInfo['info'], 0, $adInfo['data']);
	    }
	    ajaxReturn('', 1,'');
    }
    
    function editContacts(){
    	$postdata = $this->input->post();
    	$id = $postdata['id'];
    	$data['title'] = trim($postdata['title']);
    	$data['email'] = trim($postdata['email']);
    	$data['qq'] = trim($postdata['qq']);
    	$data['qqgroup'] = trim($postdata['qqgroup']);
    	$data['phonenum'] = trim($postdata['phonenum']);
    	$data['priority'] = trim($postdata['priority']);
        $user = $this->session->userdata("admin");
        $data['adminid'] = $user['admin_id'];
        $this->load->model('ContactUsModel');
        $editInfo = $this->ContactUsModel->doEdit($data,$id);
	    if($editInfo['status']==0){
	    	ajaxReturn($editInfo['info'], 0, $editInfo['data']);
	    }
	    ajaxReturn('', 1,'');
    }
    /**
     * 新增功告
     * Enter description here ...
     */
    function addnotice() {
    	$this->load->model('NoticeBoardModel');
    	$postdata = $this->input->post();
		$data['type'] = $postdata['newstype'];
		$data['title'] = trim($postdata['title']);
		$data['abstract'] = trim($postdata['newsabstract']);
		$data['priority'] = trim($postdata['priority']);
		$data['imgpath'] = isset($postdata['imgpath'])?$postdata['imgpath']:'';
		$data['status']=1;
        $user = $this->session->userdata("admin");
        $data['adminid'] = $user['admin_id'];
	    $adInfo=$this->NoticeBoardModel->doAdd($data);
	    if($adInfo['status']==0){
	    	ajaxReturn($adInfo['info'], 0, $adInfo['data']);
	    }elseif($adInfo['status'] == 3){
	    	ajaxReturn($adInfo['info'], 3, $adInfo['data']);
	    }
	    ajaxReturn('', 1,'');
    }
    
    /**
     * 删除公告
     */
    function deletenotice(){
    	$id=$this->input->post('id');
    	$this->load->model('NoticeBoardModel');
        $status = $this->NoticeBoardModel->doDelete($id);
        if($status['status']==1)
        {
            ajaxReturn('删除成功', 1, '');
        }
        else{
        	ajaxReturn('删除失败', 0, '');
        }
        ajaxReturn('删除成功', 1, '');
    }
    
    /**
     *修改公告状态
     * 
     */
    function editnoticestatus(){
        $this->load->model('NoticeBoardModel');
        $id = trim($this->input->post('id'));
        $status = trim($this->input->post('status'));
        if(empty($id))
        {
            ajaxReturn('操作失败!', 0);
        }
        $num = $this->NoticeBoardModel->edit(array('id'=>$id), array('edittime'=>time(), 'status'=>$status));
        if($num>0)
        {
            ajaxReturn('操作成功!', 1);
        }
        else
        {
            ajaxReturn('操作失败!', 0);
        }
    }
    

    /**
     * DSP切量兜底系数管理
     */
    function dspcutoverfloors()
    {
        $this->load->model('SystemConfigModel');
    	$dspcutoverfloorsswitch = $this->SystemConfigModel->getOne(array('skey'=>'dspcutoverfloorsswitch'), 'sval');//dsp切量兜底设置开关
    	if($dspcutoverfloorsswitch === null){
            $this->SystemConfigModel->add(array('skey'=>'dspcutoverfloorsswitch', 'sval'=>0));//默认为关
        }
        $dspcutoverfloorsswitch = $this->SystemConfigModel->getOne(array('skey'=>'dspcutoverfloorsswitch'), 'sval');//dsp切量兜底设置开关
        
        $p=trim($this->input->get('per_page'));
        if(!$p){
            $p=1;
        }
        $this->load->model("MediaCutoverManageModel");
        $where = array();
        $count = $this->MediaCutoverManageModel->getCount();
        $config['per_page'] = 20;
        $medialist = $this->MediaCutoverManageModel->getList($where,"*","id DESC","", $p,$config['per_page']);
        $this->load->library('page');
        $current_page =  $p==0 ? 1 : $p;
        $page=$this->page->page_show(base_url().'/admin/globals/dspcutoverfloors?',$count,$config['per_page']);
        $this->pagination->create_links();
        $this->smarty->assign('page', $page);
        $this->smarty->assign('per_page', $config['per_page']);
        $this->smarty->assign('count', $count);
        $this->assign('medialist', $medialist);
        $this->assign('dspcutoverfloorsswitch', $dspcutoverfloorsswitch);
        $this->display('admin/globals/dspcutoverfloors.html');
    }
    /**
     * 修改DSP切量兜底系数管理
     */
	function ajaxdspcutoverfloors(){
        $id = $this->input->post('id');
        $val = $this->input->post('val');
        if(!preg_match("/^\d/", $val)){
           ajaxReturn('请输入数字', 0);
        }
        if(((int) $val)<1){
            ajaxReturn('系数不能小于1，请重新输入', 0);
        }
        $this->load->model("MediaCutoverManageModel");
        $where['id'] = $id;
        $data['dspcutoverfloors'] = $val;
        $status = $this->MediaCutoverManageModel->edit($where, $data);
        if($status){
            ajaxReturn('修改成功', 1);
        }
        ajaxReturn('修改失败', 0);
    }
    
    /**
     * DSP切量兜底系数开关
     */
    function dspcutoverfloorsswitch(){
        $sval = $this->input->post('sval');
        
        $this->load->model("SystemConfigModel");
        $where['skey'] = "dspcutoverfloorsswitch";
        $data['sval'] = $sval;
        $status = $this->SystemConfigModel->edit($where, $data);
        if($status){
            ajaxReturn('修改成功', 1);
        }
        ajaxReturn('修改失败', 0);
    }
    
/*
     * 合作伙伴设置
     */
    function coPartner()
    {
    	$this->load->model('CoPartnerModel');
    	$partnerList = $this->CoPartnerModel->getList(array('status'=>1), '*', 'priority desc');
    	$nonPartnerList = $this->CoPartnerModel->getList(array('status'=>0), '*', 'priority desc');
    	$this->assign('partnerList', $partnerList);
    	$this->assign('nonpartnerList', $nonPartnerList);
    	$this->display('admin/globals/copartner.html');
    }
    
    function contactus()
    {
    	$this->load->model('ContactUsModel');
    	$contactInUse = $this->ContactUsModel->getList(array('status'=>1), '*', 'priority desc');
    	$contactNotInUse = $this->ContactUsModel->getList(array('status'=>0), '*', 'priority desc');
    	$this->assign('contactInUse', $contactInUse);
    	$this->assign('contactNotInUse', $contactNotInUse);
    	$this->display('admin/globals/contactus.html');
    }
    
    /*
     * 改变partner的状态
     */
    function editpartnerstatus()
    {
    	$this->load->model('CoPartnerModel');
        $id = trim($this->input->post('id'));
        $status = trim($this->input->post('status'));
        if(empty($id))
        {
            ajaxReturn('操作失败!', 0);
        }
        $num = $this->CoPartnerModel->edit(array('id'=>$id), array('lastedittime'=>time(), 'status'=>$status));
        if($num>0)
        {
            ajaxReturn('操作成功!', 1);
        }
        else
        {
            ajaxReturn('操作失败!', 0);
        }
    }
    
    function editContactStatus()
    {
    	$postdata=$this->input->post();
    	$this->load->model('ContactUsModel');
		$id = trim($postdata['id']);
		$status=trim($postdata['status']);
		if(empty($id)){
			ajaxReturn('操作失败!', 0);
		}   
		$num = $this->ContactUsModel->edit(array('id'=>$id), array('edittime'=>time(), 'status'=>$status));
		if($num>0)
        {
            ajaxReturn('操作成功!', 1);
        }
        else
        {
            ajaxReturn('操作失败!', 0);
        }
    }
    
    function editSetType(){
        $settype = $this->input->post('settype');
        $val = $this->input->post('val');
        $id = $this->input->post('id');
        $model = $this->input->post('model');
        if(!preg_match("/^\d/", $val)){
            ajaxReturn('数字格式错误！保存失败', 0);
        }
        if($val<0)
        {
            ajaxReturn('不能小于0！保存失败', 0);
        }
        $this->load->model($model);
        $user = $this->session->userdata("admin");
        $num = $this->$model->edit(array('id'=>$id), array($settype=>$val, 'edittime'=>time()));
 		if($num>0)
        {
            ajaxReturn('修改成功', 1);
        }
        else
        {
            ajaxReturn('修改失败', 0);
        }
    }
    
	/**
     * 汇率设置
     */
    function exchangerate(){
    	$this->load->model('ExchangeRateModel');
    	$rateInUse = $this->ExchangeRateModel->getRates(1);
    	$rateNotInUse = $this->ExchangeRateModel->getRates(0);
    	$this->assign('rateInUse', $rateInUse);
    	$this->assign('rateNotInUse', $rateNotInUse);
    	$this->display('admin/globals/exchangerate.html');
    }
    
	function addRate(){
    	$postdata = $this->input->post();
    	$data['rate'] = trim($postdata['rate']);
        $data['currFrom'] = $postdata['from'];
        $data['currTo'] = $postdata['to'];
        $data['createtime'] = time();
        $user = $this->session->userdata("admin");
        $data['adminid'] = $user['admin_id'];
        $data['status'] = 1;
       
        $this->load->model('ExchangeRateModel');
        if(!$data['currFrom'] || !$data['currTo']){
			ajaxReturn('rateerr', 0, array('ratemsg','请选择货币'));
        }
    	if($data['currFrom'] == $data['currTo']){
			ajaxReturn('rateerr', 0, array('ratemsg','请选择不同的币种'));
        }
    	if(!is_numeric($data['rate'])){
			ajaxReturn('rateerr', 0, array('ratemsg','请填写数字'));
		}
    	if($data['rate']<=0){
			ajaxReturn('rateerr', 0, array('ratemsg','汇率不可小于或等于0'));
		}
        $rateid = $this->ExchangeRateModel->add($data);
        if($rateid){
    		$this->load->model('ExchangeRateModel');
			$this->ExchangeRateModel->disableRate($rateid, $data['currFrom'], $data['currTo']);
            ajaxReturn('更改成功', 1, 0);
        }
        ajaxReturn('更改失败', 0, array('ratemsg','添加失败，请刷新重试'));
    }
  	function editRateStatus()
    {
    	$postdata=$this->input->post();
    	$this->load->model('ExchangeRateModel');
		$id = trim($postdata['id']);
		$status=trim($postdata['status']);
		if(empty($id)){
			ajaxReturn('操作失败!', 0);
		}   
		if($status == 1){
			$exitnum = $this->ExchangeRateModel->getList(array('currFrom'=>$postdata['cfrom'], 'currTo'=>$postdata['cto'], 'status'=>1),'id');
			$result = '请先停用使用中的'.$postdata['cfrom'].'到'.$postdata['cto'].'的转换';
			if(count($exitnum)>0) ajaxReturn($result, 0);
		}
		$num = $this->ExchangeRateModel->edit(array('id'=>$id), array('edittime'=>time(), 'status'=>$status));
		if($num>0)
        {
            ajaxReturn('操作成功!', 1);
        }
        else
        {
            ajaxReturn('操作失败!', 0);
        }
    }
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class singleSDKdata extends MY_A_Controller {

	function __construct()
	{
                error_reporting(0);
		parent::__construct();
		$this->load->model('DailyAdStatsModel');
	}
	 /**
    * kt 公司内部后台 - 时间分析
    */
    function timelist(){
          $where = $this->getDateRangeByControl();
          $appinfo = $this->DailyAdStatsModel->getListByTime($where);
          $this->assign('appinfo', $appinfo); 
          $this->load->display('admin/singleSDKmanage/datatimelist.html');
    }
    /**
    * 媒体游戏分析
    * @return type
    */
    function medialist(){
          $where = $this->getDateRangeByControl();
          $appinfo = $this->DailyAdStatsModel->getListByMedia($where);
          $this->assign('appinfo', $appinfo); 

          $this->load->display('admin/singleSDKmanage/datamedialist.html');
    }
    /**
    * 渠道分析
    * @return type
    */
    function channellist(){
          $where = $this->getDateRangeByControl();
          $appinfo = $this->DailyAdStatsModel->getListByChannel($where);
          $this->assign('appinfo', $appinfo);               
          $this->load->display('admin/singleSDKmanage/datachannellist.html');
    }
    /**
    * 网络分析
    * @return type
    */
    function netlist(){
          $where = $this->getDateRangeByControl();
          $appinfo = $this->DailyAdStatsModel->getListByNet($where);
          $this->assign('appinfo', $appinfo);   
           $this->load->display('admin/singleSDKmanage/datanetlist.html');
    }
}


?>
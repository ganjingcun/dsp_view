<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SpecialService extends MY_A_Controller
{
    function __construct(){
        parent::__construct();
        $this->load->model('UserMemberModel');
        $this->load->model('AppInfoModel');
        $configlist = $this->SystemConfigModel->getAllConfig();
        $this->assign('configlist', $configlist);
    }

    function ajaxSetSignapisort()
    {
        $id = $this->input->post('adid');
        $apisort = (int)$this->input->post('val');
        $os = $this->input->post('os');
        if($apisort<=0)
        {
            ajaxReturn('请输入大于0的整数！', 0, '');
        }
        $this->load->model('AdStuffMoregameModel');
        if(empty($id))
        {
            ajaxReturn('未知错误！', 0, '');
        }
        $status = $this->AdStuffMoregameModel->edit(array("textid"=>array("in", $id)), array('apisort'=>$apisort));
        if($status) ajaxReturn('修改成功！', 1, 0);
        ajaxReturn('修改失败！', 0, $this->AdStuffMoregameModel->getlastsql());
        
    }
    function ajaxSetSignIntegral(){
        $id = $this->input->post('adid');
        $integralsort = (int)$this->input->post('val');
        $os = $this->input->post('os');
        if($integralsort<=0)
        {
            ajaxReturn('请输入大于0的整数！', 0, '');
        }
        $this->load->model('AdStuffIntegralModel');
        if(empty($id))
        {
            ajaxReturn('未知错误！', 0, '');
        }

        $currentsort = $this->AdStuffIntegralModel->getRow(array('textid' => $id));
        if($currentsort['integralsort'] < $integralsort)
        {
            $integralsort++;
        }
        $status = $this->AdStuffIntegralModel->edit(array("textid"=>array("in", $id)), array("integraltime"=>time(), 'integralsort'=>$integralsort));
        $list = $this->AdStuffIntegralModel->getMoregameTop($os,1);
        $allList = $this->AdStuffIntegralModel->getIsIntegralMoregame($os);
        $allTextIds = array();
        foreach($allList as $v)
        {
            $allTextIds[$v['textid']] = '';
        }
        if(!empty($list))
        {
            foreach ($list as $k=>$v)
            {
                $this->AdStuffIntegralModel->edit(array("textid"=>$v['textid']), array('integralsort'=>$k+1));
                if(isset($allTextIds[$v['textid']]))
                {
                    unset($allTextIds[$v['textid']]);
                }
            }
            if(!empty($allTextIds))
            {
                foreach ($allTextIds as $k=>$v)
                {
                    $this->AdStuffIntegralModel->edit(array("textid"=>$k), array('integralsort'=>0));
                }
            }
        }
        $this->load->model('AdStuffMoregameModel');
        $this->AdStuffMoregameModel->updateisupdate($id,"ad_stuff_integral");
        if($status) ajaxReturn('修改成功！', 1, 0);
        ajaxReturn('修改失败！', 0, $this->AdStuffIntegralModel->getlastsql());
    }

    function hulustore(){
        $os = (int)$this->input->get('os');
        if(!$os){
            $os = 2;
        }
        $ishulu = (int)$this->input->get('ishulu');
        $confirm = !$ishulu ? '推送到葫芦商城' :'从葫芦商城释放';
        $this->assign('confirm', $confirm);
        $this->assign('os', $os);
        $this->assign('ishulu', $ishulu);
        $this->load->model('AdStuffIntegralModel');
        $search = $this->input->post('search');
        $list = $this->AdStuffIntegralModel->getHuLuStore($os,$ishulu,$search);
        $stuffids = '';
        $tlist = array();
        foreach($list as $val){
            $stuffids .= $val['stuffid'].',';
            $tlist[$val['stuffid']] = array();
        }
        if($stuffids){
            $stuffids = rtrim($stuffids, ',');
            $tasklist = $this->CommonModel->table("ad_task")->getList(array("stuffid"=>array("in", $stuffids)));
            foreach($tasklist as $val){
                $tlist[$val['stuffid']][] = $val;
            }
        }
        $this->assign('tlist', $tlist);
        $this->assign('list', $list);
        $this->display('admin/specialservice/hulustore.html');
    }

    function ajaxsetishulu(){
        $id = $this->input->post('id');
        $ishulu = (int)$this->input->post('ishulu');
        $os = $this->input->post('os');
        $this->load->model('AdStuffIntegralModel');

        $max = $this->AdStuffIntegralModel->getMaxOrder($os);

        if(empty($id))
        {
            ajaxReturn('请选择要推送的应用！', 0, '');
        }
        $ids = explode(',', $id);
        $status = $this->AdStuffIntegralModel->edit(array("textid"=>array("in", $ids)), array('ishulu'=>$ishulu, 'hulutime'=>time()));

        $this->load->model('AdStuffMoregameModel');
        $this->AdStuffMoregameModel->updateisupdate($id,"ad_stuff_integral");
        if($status) ajaxReturn('修改成功！', 1, 0);
        ajaxReturn('修改失败！', 0, $this->AdStuffIntegralModel->getlastsql());
    }

    function ajaxsetisapi(){
        $id = $this->input->post('id');
        $isapi = (int)$this->input->post('isapi');
        $os = $this->input->post('os');
        $this->load->model('AdStuffMoregameModel');
        $max = $this->AdStuffMoregameModel->getMaxOrder($os);
        if(empty($id))
        {
            ajaxReturn('请选择要推送的应用！', 0, '');
        }
        $ids = explode(',', $id);
        $status = $this->AdStuffMoregameModel->edit(array("textid"=>array("in", $ids)), array('isapi'=>$isapi, 'apitime'=>time()));
        $this->load->model('AdStuffMoregameModel');
        $this->AdStuffMoregameModel->updateisupdate($id,"ad_stuff_moregame");
        if($status) ajaxReturn('修改成功！', 1, 0);
        ajaxReturn('修改失败！', 0, $this->AdStuffIntegralModel->getlastsql());
    }

    function apirecommend(){
        $os = (int)$this->input->get('os');
        if(!$os){
            $os = 2;
        }
        $isapi = (int)$this->input->get('isapi');
        $confirm = !$isapi ? '推送到API投放' :'从API释放';
        $this->assign('confirm', $confirm);
        $this->assign('os', $os);
        $this->assign('isapi', $isapi);
        $this->load->model('AdStuffMoregameModel');
        $search = $this->input->post('search');
        $list = $this->AdStuffMoregameModel->getAPIList($os,$isapi,$search);
        $this->assign('list', $list);
        $this->display('admin/specialservice/apirecommend.html');
    }

    function ajaxsetpopupisapi(){
        $id = $this->input->post('id');
        $isapi = (int)$this->input->post('isapi');
        $os = $this->input->post('os');
        $this->load->model('AdStuffModel');
        if(empty($id))
        {
            ajaxReturn('请选择要推送的应用！', 0, '');
        }
        $ids = explode(',', $id);
        $status = $this->AdStuffModel->edit(array("stuffid"=>array("in", $ids)), array('isapi'=>$isapi, 'apitime'=>time()));
        $this->load->model('AdStuffMoregameModel');
        $this->AdStuffMoregameModel->updateisupdate2($id,'ad_stuff');
        if($status) ajaxReturn('修改成功！', 1, 0);
        ajaxReturn('修改失败！', 0, $this->AdStuffMoregameModel->getlastsql());
    }

    function apiinterstitial(){
        $os = (int)$this->input->get('os');
        if(!$os){
            $os = 2;
        }
        $isapi = (int)$this->input->get('isapi');
        $confirm = !$isapi ? '推送到API投放' :'从API释放';
        $this->assign('confirm', $confirm);
        $this->assign('os', $os);
        $this->assign('isapi', $isapi);
        $this->load->model('AdStuffModel');
        $search = $this->input->post('search');
        $list = $this->AdStuffModel->getAPIPopupList($os,$isapi,$search);
        $this->assign('list', $list);
        $this->display('admin/specialservice/apiinterstitial.html');
    }
}
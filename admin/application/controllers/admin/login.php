<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->init();
	}
	
	function init()
	{
		$this->load->library(array('admin/position','admin/message'));
	}

	function index()
	{
		$data['validate_admin_login'] = $this->get_config('ciccms/admin','sys','validate_admin_login');
		$this->smarty->assign('data',$data);
		$this->load->display('admin/login/index.html');
		//$this->load->view('admin/login/index',$data);
	}

	function login_in()
	{
		//判断验证码是否开启
		if($this->input->post('validate_code') !== FALSE)
		{
			//判断用户输入的验证码与session中存在的验证码是否对应
			if(strtoupper($this->input->post('validate_code')) != $this->session->userdata('validate_code'))
			{
				$this->message->msg('验证码不正确',('/admin/login'));
			}
		}
		//验证登录表单输入是否为空
		$result = $this->validate('admin_login',array('<li>','</li>'));
		if($result !== TRUE)
			$this->message->msg($result,('/admin/login'));
		$this->load->model('AdminUserModel');
		$this->load->model('AdminGroupModel');
		$user_admin = $this->AdminUserModel->getRow(array('admin_name'=>$this->input->post('admin_name')));
		if(empty($user_admin))
		{
			$this->message->msg('没有此管理员！',('/admin/login'));
		}
		if($user_admin['admin_password'] == md5($this->input->post('admin_password')))
		{
			if($user_admin['admin_status'] == '1')
			{
				$REMOTE_ADDR = $this->input->server('REMOTE_ADDR');
				$this->AdminUserModel->edit(array('admin_id'=>$user_admin['admin_id']),array('admin_last_login_ip'=>$REMOTE_ADDR,'admin_last_login_time'=>time()));
				$admin_group = $this->AdminGroupModel->getRow(array('group_id'=>$user_admin['group_id']));
				$data['is_login']	= TRUE;
				$data['admin'] 		= array(	
						'admin_id'				=> $user_admin['admin_id'],
						'admin_name'			=> $user_admin['admin_name'],
						'admin_last_login_ip'	=> $user_admin['admin_last_login_ip'],
						'admin_last_login_time'	=> $user_admin['admin_last_login_time'],
						'REMOTE_ADDR'			=> $REMOTE_ADDR,
						'group_name'			=> $admin_group['group_name'],
						'group_privilege'		=> unserialize($admin_group['group_privilege']),
				);
				$this->session->set_userdata($data);
				redirect('/admin/admin/index');
			}else{
				$this->message->msg('管理员帐号已禁止！！',('/admin/login/login_in'));
			}	
		}else{
			$this->message->msg('密码错误！',('/admin/login'));
		}
	}
	
	function login_out()
	{
		$data = array('is_login'=>'','admin'=>'');
		$this->session->unset_userdata($data);
		$this->message->msg('成功退出！',('/admin/login'),0);
	}
	function empty_login(){
		$this->load->display('admin/login/empty.html');
	}
}
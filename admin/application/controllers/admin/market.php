<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Market extends MY_A_Controller
{
    function __construct(){
        parent::__construct();
        $this->load->model('MarketModel');
        $this->load->library('pwdsecurity');
        $configlist = $this->SystemConfigModel->getAllConfig();
        $this->assign('configlist', $configlist);
    }

    /**
     * 应用商店列表页
     */
    function marketlist(){
    	$key = trim($this->input->get('key'));
    	$where = array();
    	if(!empty($key))
        {
            $where['marketname'] = array('LIKE', '%'.$key.'%');
        }
        $where['status'] = 0;
        $p=trim($this->input->get('per_page'));
        if(!$p){
            $p=1;
        }
        $config['per_page'] = 20;
        $count = $this->MarketModel->getCount($where);
        $this->load->library('page');
        $current_page =  $p==0 ? 1 : $p;
        $var = '&key='.$key;
        $page=$this->page->page_show('/admin/market/marketlist?'.$var,$count,$config['per_page']);
        $this->pagination->create_links();
        $this->assign('key', $key);
        $this->smarty->assign('page', $page);
        $this->smarty->assign('per_page', $config['per_page']);
        $this->smarty->assign('count', $count);
        $list = array();
        $list = $this->MarketModel->getList($where, '*',"marketid", '', $p, $config['per_page']);
        $this->assign('list', $list);
        $this->display('admin/market/marketlist.html');
    }

   
    /**
     * 新增应用商店跳转页面
     */
    function addmarket(){
        $this->assign('mark', '新增');
        $this->display('admin/market/addmarket.html');
    }


    /**
     * 修改应用商店跳转页面
     */
    function editmarket(){
        $marketid = $this->input->get('marketid');
        $key = $this->input->get('key');
        $data = $this->MarketModel->getRow(array('marketid'=>$marketid));
        if(empty($data) || !isset($data['marketid']))
        {
            return ;
        }
        $this->assign('mark', '修改');
        $this->assign('data', $data);
        $this->assign('key', $key);
        $this->display('admin/market/addmarket.html');
    }

    /**
     * 保存market
     */
    function ajaxmarketsave(){
        if($this->input->post('savetype') == 'add')
        {
            $this->marketadd();
        }
        else if($this->input->post('savetype') == 'edit' && $this->input->post('marketid') > 0)
        {
            $this->marketedit();
        }
    }

    /**
     * 添加market
     */
    private function marketadd()
    {    				
    	$data['marketname'] = $this->input->post('marketname')?trim($this->input->post('marketname')):"";
    	$data['marketratio'] = $this->input->post('marketratio')?trim($this->input->post('marketratio')):"";
    	$data['devratio'] = $this->input->post('devratio')?trim($this->input->post('devratio')):""; 
    	$data['platformratio'] = $this->input->post('platformratio')?trim($this->input->post('platformratio')):"";
        $data['description'] = $this->input->post('description')?trim($this->input->post('description')):"";
        $status = $this->MarketModel->doAdd($data);
        if($status['status'])
        {
            ajaxReturn('添加成功！', 1, '添加成功！');
        }
        ajaxReturn($status['info'], $status['status'], $status['data']);
    }

    /**
     * 编辑market
     */
    private function marketedit()
    {
        $where['marketid'] = $this->input->post('marketid')?trim($this->input->post('marketid')):"";
        
        $data['marketname'] = $this->input->post('marketname')?trim($this->input->post('marketname')):"";
        
    	$data['marketratio'] = $this->input->post('marketratio')?trim($this->input->post('marketratio')):"";
    	
    	$data['devratio'] = $this->input->post('devratio')?trim($this->input->post('devratio')):"";
    	
    	$data['platformratio'] = $this->input->post('platformratio')?trim($this->input->post('platformratio')):"";
    	
    	$data['description'] = $this->input->post('description')?trim($this->input->post('description')):"";
    	
        $status = $this->MarketModel->doEdit($where, $data);
        if($status['status'])
        {
        	 ajaxReturn('修改成功1！', 1, $status['data']);
        }
        ajaxReturn($status['info'], $status['status'], $status['data']);
    }

    
    /**
     * 删除market
     */
    function ajaxdel(){
       
        $where['marketid'] = trim($this->input->post('marketid'));
        if(empty($where['marketid']))
        {
            ajaxReturn('1删除失败，请刷新后重试！', 0, '1删除失败，请刷新后重试！');
        }
        $marketinfo = $this->MarketModel->getRow($where);
        if(empty($marketinfo))
        {
            ajaxReturn('2该记录不存在，请刷新后重试！', 0, '2该记录不存在，请刷新后重试！');
        }
        $status = $this->MarketModel->edit($where,array('status'=>1,'edittime'=>time()));
        if($status)
        {
            ajaxReturn('成功删除！', 1, '成功删除！');
        }
        ajaxReturn('3删除失败，请刷新后重试！', 0, '1删除失败，请刷新后重试！');
    }

    
 	/**
     * 查看商店渠道列表
     */
	function channellist(){
        $key = trim($this->input->get('key'));
        $marketid = trim($this->input->get('marketid'));
        $marketinfo = $this->MarketModel->getRow(array('marketid'=>$marketid,'status'=>0));
        $this->smarty->assign('marketinfo', $marketinfo);
    	$where1 = array();
		$where2 = '1';
    	if(!empty($key))
        {
            $where1['channelname'] = array('LIKE', '%'.$key.'%');
            $where2.=" and ac.`channelname` like '%".$key."%' ";
        }
    	if(!empty($marketid)){
    		$where1['marketid'] = $marketid;
    		$where2.=" and ac.`marketid`= ".intval($marketid)." ";
    	}
        $this->load->model("AppChannelModel");
        $count = $this->AppChannelModel->getchanneluserlistcount($where1);
        $p=trim($this->input->get('per_page'));
        if(!$p){
            $p=1;
        }
        $config['per_page'] = 20;
        $this->load->library('page');
        $current_page =  $p==0 ? 1 : $p;
        $var = '&key='.$key."&marketid=".$marketid;
        $page=$this->page->page_show('/admin/market/channellist?'.$var,$count,$config['per_page']);
        $this->pagination->create_links();
        $this->smarty->assign('key', $key);
        $this->smarty->assign('page', $page);
        $this->smarty->assign('per_page', $config['per_page']);
        $this->smarty->assign('count', $count);
        $list = array();
        $list = $this->AppChannelModel->getchanneluserlist($where1, '*',"id", '', $p, $config['per_page']);
        $this->smarty->assign('list', $list);
        $this->display('admin/market/channellist.html');
    }
    
	 /**
     * 修改渠道信息跳转
     */
    function editchannel(){
        $id = trim($this->input->get('id'));
        $marketid = trim($this->input->get('marketid'));
        $where = array();
        $where['id'] = $id;
    	$data=array();
    	$this->load->model("AppChannelModel");
        $data = $this->AppChannelModel->getRow($where);
        $this->assign('data', $data);
        $this->assign('marketid', $marketid);
        $this->display('admin/market/editchannel.html');
    }
    
 	function ajaxchannelsave()
    {
        $where['id'] = $this->input->post('id')?trim($this->input->post('id')):"";
    	$data['marketratio'] = $this->input->post('marketratio')?trim($this->input->post('marketratio')):"";
    	$data['devratio'] = $this->input->post('devratio')?trim($this->input->post('devratio')):"";
    	$data['platformratio'] = $this->input->post('platformratio')?trim($this->input->post('platformratio')):"";
    	$this->load->model("AppChannelModel");
        $status = $this->AppChannelModel->doEdit($where,$data);
        if($status['status'])
        {
        	 ajaxReturn('修改成功1！', 1, $status['data']);
        }
        ajaxReturn($status['info'], $status['status'], $status['data']);
    }


	/**
     *渠道列表页
     */
    function appchannellist(){
    	$search = $this->input->get_post('search');
        $where = " and 1 ";
        if($search){
            $search = str_replace("_","\_", $search);
            $search = str_replace("%","\%", $search);
            $where .= " and publisherID like '%".$search."%'";
        }
		$config['per_page'] = 20;
        $p=trim($this->input->get('per_page'));
		if(!$p){
			$p=1;
		}
		$offset = ($p-1)*$config['per_page'];
		$this->load->model("AppChannelModel");
        $incomeListQuery = $this->AppChannelModel->getchannelusermarketlist($offset, $config['per_page'],$where);
        $list = $incomeListQuery['list'];
        $count = $incomeListQuery['count'];
       	$this->load->library('page');
		$current_page = $p==0 ? 1 : $p;
		$var = '&search='.$this->input->get_post('search');
		$page=$this->page->page_show('/admin/market/appchannellist?'.$var,$count,$config['per_page']);
		$this->smarty->assign('search', $search);
        $this->smarty->assign ( 'page', $page );
		$this->smarty->assign ( 'per_page', $config ['per_page'] );
		$this->smarty->assign ( 'count', $count );
        $this->smarty->assign('list', $list);
        $this->display('admin/market/appchannellist.html');
    }
    
    
     /**
     * 修改渠道对应应用商店页面跳转
     */
    function editappchannel(){
        $id = trim($this->input->get('id'));
        $where = array();
        $where['id'] = $id;
    	$data=array();
    	$this->load->model("AppChannelModel");
        $data = $this->AppChannelModel->getRow($where);
        $list = $this->MarketModel->getList(array('status'=>0), '*',"marketid desc");
        $this->assign('list', $list);
        $this->assign('data', $data);
        $this->display('admin/market/editappchannel.html');
    }
    
    
    /**
     * 渠道修改渠道名称和关联应用商店保存
     */
 	function ajaxappchannelsave()
    {
        $where['id'] = $this->input->post('id')?trim($this->input->post('id')):"";
    	$data['channelname'] = $this->input->post('channelname')?trim($this->input->post('channelname')):"";
    	if($this->input->post('marketid') == 0){
    		$data['marketid']="";
    	}else{
    		$data['marketid'] = trim($this->input->post('marketid'));
    	}
    	$this->load->model("AppChannelModel");
    	$oldappchannelinfo = $this->AppChannelModel->getRow($where);
    	if($data['marketid'] == $oldappchannelinfo['marketid'] && $data['channelname'] == $oldappchannelinfo['channelname']){
    		 ajaxReturn("您未做任何修改", 0, "您未做任何修改");
    	}
    	else{
    		$num1 = $this->AppChannelModel->edit($where,$data);
    		$this->load->model("ChannelTypeModel");
    		$num2 = $this->ChannelTypeModel->edit(array('channelid'=>$oldappchannelinfo['channelid']),$data);
    		if($num1>0){
    			 ajaxReturn('修改成功！', 1, '修改成功！');
    		}
    		else{
    			 ajaxReturn('修改失败！', 0, '修改失败！');
    		}
    	}
    }
    
    

	/**
	 * 应用商店用户账号列表
	 */
	function marketuserlist(){
		$marketlisttmp = $this->MarketModel->getList(array('status'=>0),'marketid,marketname');
		$marketlist = array();
		if($marketlisttmp){
			foreach ( $marketlisttmp as $market){
				$marketlist[$market['marketid']] = $market;
			}
		}
		$this->assign('marketlist', $marketlist);
		$this->load->model('MarketUserMemberModel');
        if($this->input->get_post('username')){  
        	$data['username'] =  $this->input->get_post('username');
        	$data['password'] = PwdSecurity::cryptPassword($this->input->get_post('password'));
            $data['marketid'] =  $this->input->get_post('marketid');
            $data['regtime'] = time();
            $data['regip'] = $this->input->ip_address();
            $data['userstatus'] = 1;
            $pattern = "/^([0-9A-Za-z\\-_\\.]+)@([0-9a-z]+\\.[a-z]{2,3}(\\.[a-z]{2})?)$/i";
            if(preg_match($pattern,$data['username'])){
            	$this->message->msg('用户名输入格式不正确','/admin/market/marketuserlist');
                return;
            }
            if($this->MarketUserMemberModel->add($data)){
                $this->message->msg('创建成功','/admin/market/marketuserlist');
                return;
            }else{
                $this->message->msg('创建失败','/admin/market/marketuserlist');
                return;
            }
        }
        $marketuserlist = $this->MarketUserMemberModel->getList(array('userstatus'=>1),'*',"marketuserid desc");
        $this->smarty->assign('marketuserlist',$marketuserlist);
        $this->load->display("admin/market/marketuserlist.html");
    }
    
    function editmarketuser($marketuserid=""){
    	$this->load->model('MarketUserMemberModel');
        $marketuserinfo = $this->MarketUserMemberModel->getRow(array('marketuserid'=>$marketuserid));
        $this->smarty->assign('marketuserinfo',$marketuserinfo);
        
        $marketlist = $this->MarketModel->getList(array('status'=>0),'marketid,marketname');
		$this->assign('marketlist', $marketlist);
        if( $this->input->get_post('username')){
            $marketuserid =  $this->input->get_post('marketuserid');
            $data = array();
            $data['marketid'] =  $this->input->get_post('marketid');
            $res = $this->MarketUserMemberModel->edit(array('marketuserid'=>$marketuserid),$data);
            if( $res !== null){
                $this->message->msg('编辑成功','/admin/market/marketuserlist');
                return;
            }else{
                $this->message->msg('编辑失败','/admin/market/marketuserlist');
                return;
            }
        }
        $this->load->display("admin/market/marketuseredit.html");
    }
    
 	function editmarketuserpass($marketuserid=""){
    	$this->load->model('MarketUserMemberModel');
        $marketuserinfo = $this->MarketUserMemberModel->getRow(array('marketuserid'=>$marketuserid));
        $this->smarty->assign('marketuserinfo',$marketuserinfo);
        if( $this->input->get_post('password')){
            $marketuserid =  $this->input->get_post('marketuserid');
            $data['password'] = PwdSecurity::cryptPassword($this->input->get_post('password'));
            $res = $this->MarketUserMemberModel->edit(array('marketuserid'=>$marketuserid),$data);
            if( $res !== null){
                $this->message->msg('改密成功','/admin/market/marketuserlist');
                return;
            }else{
                $this->message->msg('改密失败','/admin/market/marketuserlist');
                return;
            }
        }
        $this->load->display("admin/market/editmarketuserpass.html");
    }
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Business extends MY_A_Controller
{
    private $accountmarkname = array('0'=>"普通",'1'=>"代扣税",'2'=>"触控内部",'3'=>"触控内部");
    function __construct(){
        error_reporting(0);
        parent::__construct();
        $this->load->model('AdIncomeModel');
        $this->load->model('UserMemberModel');
        $this->load->model('AppInfoModel');
        $this->load->model('AdAccountinfoModel');
        $this->load->model('TeamMembersModel');
        $this->load->model('AdIncomePaidModel');
        $this->load->model('AdMonthIncomeModel');
        $this->accounttypearr = array('0'=>"个人/团体",'1'=>"个人/团体",'2'=>"企业",'3'=>"个体工商");
        $this->acctypearr = array('1'=>"CPC",'2'=>"CPM",'3'=>"CPA");
    }

    function incomelist(){
        $search = $this->input->get_post('search');
        $this->assign('search', $search);
        $where = "1";
        if($search){
            $search = str_replace("_","\_", $search);
            $search = str_replace("%","\%", $search);
            $where .= " and username like '%".$search."%'";
        }
        
		$config['per_page'] = 20;
        if(!$search){
        	$p=trim($this->input->get('per_page'));
			if(!$p){
				$p=1;
			}
			$offset = ($p-1)*$config['per_page'];
        	$incomeListQuery = $this->AdIncomeModel->getIncomeList($offset, $config['per_page']);
        	$confirmList = $incomeListQuery['list'];
        	$count = $incomeListQuery['count'];
        	if($confirmList){
        		foreach($confirmList as $k=>$v){
        			$confirmList[$k]['accounttype'] = empty($confirmList[$k]['accounttype'])?"0":$confirmList[$k]['accounttype'];
        			$confirmList[$k]['accounttypename'] = $this->accounttypearr[$confirmList[$k]['accounttype']];
        			switch ($confirmList[$k]['accountmark'])
                    {
                        case 0:
                            $confirmList[$k]['accountmarkname'] = '普通';
                            break;
                        case 1:
                            $confirmList[$k]['accountmarkname'] = '代扣税';
                            break;
                        case 2:
                            $confirmList[$k]['accountmarkname'] = '触控内部';
                            break;
                    }
        		}
        	}
        	$showlist = $confirmList;
        }
        
        if($search){
	        $users = $this->UserMemberModel->getList($where, '*','userid');
	        foreach($users as $key=>$val){
	            //身份类型  1个人/团体，2 企业，3 个体工商
	            $userid = $val['userid'];
	            $accountinfo = $this->get_user_account($userid);
	            if(!empty($accountinfo)){
	                $sum_cincome = $this->get_sum_income($userid,'',1,'1');
	                if($sum_cincome > 0){
	                    $users_info[$key]['sum_cincome'] = $sum_cincome;
	                    $users_info[$key]['sum_income'] = $this->get_sum_income($userid,'','','1');
	                    $account_type = empty($accountinfo['accounttype'])?"0":$accountinfo['accounttype'];
	                    $users_info[$key]['accounttype'] = $account_type;
	                    $users_info[$key]['accounttypename'] = $this->accounttypearr[$account_type] ;
	                    $users_info[$key]['username'] = $val['username'];
	                    $users_info[$key]['contact'] = $accountinfo['contact'];
	                    $users_info[$key]['userid'] = $val['userid'];
	                    switch ($val['accountmark'])
	                    {
	                        case 0:
	                            $users_info[$key]['accountmarkname'] = '普通';
	                            break;
	                        case 1:
	                            $users_info[$key]['accountmarkname'] = '代扣税';
	                            break;
	                        case 2:
	                            $users_info[$key]['accountmarkname'] = '触控内部';
	                            break;
	                    }
	                }
	            }
	        }
        	$count = count($users_info);
        	$showlist = $users_info;
        }
       	$this->load->library('page');
		$current_page = $p==0 ? 1 : $p;
		$var = '&search='.$this->input->get_post('search');
		$page=$this->page->page_show('/admin/business/incomelist?'.$var,$count,$config['per_page']);
        $this->smarty->assign ( 'page', $page );
		$this->smarty->assign ( 'per_page', $config ['per_page'] );
		$this->smarty->assign ( 'count', $count );
		$this->assign('search', $search);
        $this->assign('users_info', $showlist);
        $this->display('admin/business/incomelist.html');
    }
    /**
     * 开发者 应用结算历史记录
     */
    function income_history(){
        $userid = $this->input->get('userid');
        $search = $this->input->get_post('search');
        $this->assign('search', $search);
        $income_info = $this->AdMonthIncomeModel->getList(array('userid'=>$userid),"*","id desc");
        $userinfo = $this->UserMemberModel->getRow(array('userid'=>$userid));
        //用户信息
        $accountinfo = $this->get_user_account($userid);
        if(!empty($accountinfo)){
            $account_type = empty($accountinfo['accounttype'])?"0":$accountinfo['accounttype'];
            $userinfo['accounttype'] = $account_type;
            $userinfo['accounttypename'] = $this->accounttypearr[$account_type] ;
        }
        $this->smarty->assign('income_info', $income_info);
        $this->smarty->assign('userinfo',$userinfo);
        $this->display('admin/business/income_history.html');
    }
    /**
     * 查看当月收入详情
     */
    function income_detail(){
        $userid = $this->input->get('userid');
        $month = $this->input->get('month');
        $last_month = $month?$month:$this->get_last_month(1);
        $sum_income = $this->get_sum_income($userid,$month,1);
        $app_infos = $this->get_user_apps($userid,$month);
        foreach($app_infos as $key=>$val){
            $appid = $val['pkCanalId'];
            $appinfo = $this->AppInfoModel->getRow(array('appid'=>$appid),"appname","appid desc");
            $app_infos[$key]['appname'] = $appinfo['appname'];
            $acctype = $val['accType'];
            $app_infos[$key]['acctypename'] = $this->acctypearr[$acctype];
            $app_infos[$key]['appid'] = $val['appid'];
            $app_infos[$key]['avgincome'] = $val['cincome']/$val['uidClick'];
        }
        $userinfo = $this->UserMemberModel->getRow(array('userid'=>$userid));
        $this->smarty->assign('sum_income', $sum_income);
        $this->smarty->assign('userinfo', $userinfo);
        $this->smarty->assign('app_infos', $app_infos);
        $this->smarty->assign('last_month', $last_month);
        $this->display('admin/business/income_detail.html');
    }
    /**
     * 修改结算单
     */
    function edit_income_detail(){
        $userid = $this->input->get('userid');
        $month = $this->input->get('month');
        $last_month = $month?$month:$this->get_last_month(1);
        $sum_income = $this->get_sum_income($userid,'',1);
        $app_infos = $this->get_user_apps($userid);

        foreach($app_infos as $key=>$val){
            $appid = $val['pkCanalId'];
            $appinfo = $this->AppInfoModel->getRow(array('appid'=>$appid),"appname","appid desc");
            $app_infos[$key]['appname'] = $appinfo['appname'];
            $acctype = $val['accType'];
            $app_infos[$key]['acctypename'] = $this->acctypearr[$acctype];
            $app_infos[$key]['appid'] = $val['appid'];
            $app_infos[$key]['avgincome'] =$val['cincome']/$val['uidClick'];
        }
        if($this->input->get_post('edit_flg') == '1'){
            $cincome_arr = $this->input->get_post('cincome');
            foreach($cincome_arr as $k=>$v){
                $cincome = formatmoney($v,'set');
                $this->AdIncomeModel->edit(array('id'=>$k),array('cincome'=>$cincome));
            }
            $this->message->msg('编辑成功','/admin/business/incomelist');
            return;
        }
        $userinfo = $this->UserMemberModel->getRow(array('userid'=>$userid));
        $this->smarty->assign('sum_income', $sum_income);
        $this->smarty->assign('userinfo', $userinfo);
        $this->smarty->assign('app_infos', $app_infos);
        $this->smarty->assign('last_month', $last_month);
        $this->display('admin/business/edit_income_detail.html');
    }
    /**
     * 修改结算单
     */
    function edit_income_info(){
        $incomeid = $this->input->get_post('incomeid');
        $type = $this->input->get('type');
        $income_info = $this->AdMonthIncomeModel->getRow(array('id'=>$incomeid));
        $last_month = $income_info['month'];
        $where['id']=array("in",$income_info['incomeid']);
        $app_infos = $this->AdIncomeModel->getList($where);
        foreach($app_infos as $key=>$val){
            $appid = $val['pkCanalId'];
            $appinfo = $this->AppInfoModel->getRow(array('appid'=>$appid),"appname","appid desc");
            $app_infos[$key]['appname'] = $appinfo['appname'];
            $acctype = $val['accType'];
            $app_infos[$key]['acctypename'] = $this->acctypearr[$acctype];
            $app_infos[$key]['appid'] = $val['appid'];
            $app_infos[$key]['avgincome'] = $val['cincome']/$val['uidClick'];
        }
        if($this->input->get_post('edit_flg') == '1'){
            $cincome_arr = $this->input->get_post('cincome');
            foreach($cincome_arr as $k=>$v){
                $cincome = formatmoney($v,'set');
                $this->AdIncomeModel->edit(array('id'=>$k),array('cincome'=>$cincome));
            }
            $userid = $income_info['userid'];
            $data['cincome'] = $this->get_sum_income($userid,$last_month,1);
            //判断是个人还是企业，企业无扣税，个人扣税
            $accountinfo = $this->get_user_account($userid);
            if($accountinfo['accounttype'] == '1'){
                //结算对应的month 和id
                $info = $this->get_user_userratio($userid, $data['cincome']);
                $data['userratio']  = json_encode($info['teaminfo']);
                $data['taxmoney'] = $info['taxmoney'];
                $data['aftertaxmoney'] = $info['aftertaxmoney'];
            }else{
                $data['aftertaxmoney'] = $data['cincome'];
            }
            $this->AdMonthIncomeModel->edit(array('id'=>$incomeid),$data);
            $this->message->msg('编辑成功','/admin/business/income_list');
            return;
        }

        $sum_income = $income_info['cincome'];
        $userinfo = $this->UserMemberModel->getRow(array('userid'=>$income_info['userid']));
        $this->smarty->assign('sum_income',  $sum_income);
        $this->smarty->assign('userinfo', $userinfo);
        $this->smarty->assign('last_month', $last_month);
        $this->smarty->assign('app_infos', $app_infos);
        $this->smarty->assign('incomeid',$incomeid);
        $this->smarty->assign('type', $type);
        $this->display('admin/business/edit_income_info.html');
    }
    
    function add_account_list(){
    	 $userid = $this->input->get('userid');
    	 $confirm_result = $this->income_confirmation($userid);
         $this->message->msg($confirm_result['info'],$confirm_result['path']);
    }
    
    function income_confirmation($userid){
    	$result = array('status'=>1,'info'=>'','path'=>'');
    	$this->load->model('AdMonthIncomeModel');
    	$data['month'] =  $this->get_last_month();
        $data['userid'] = $userid;
        $count_info = $this->AdMonthIncomeModel->getRow($data,"id");
   		if(!empty($count_info)){
   			$result['status'] = 0;
   			$result['info'] = '此月份结算已经生成，请勿重复';
   			$result['path'] = '/admin/business/incomelist';
            return $result;
        }
        $data['cincome'] = $this->get_sum_income($userid,'',1,'1');
        $data['income'] = $this->get_sum_income($userid,'','','1');
        
    	//结算对应的 收入报表id
        $income_info = $this->get_income_month($userid);

        $data['incomeid'] =  $income_info['incomeid'];
        $user = $this->session->userdata("admin");
        $data['adminuid'] =  $user['admin_id'];
        $data['addtime'] = time();
        //判断是个人还是企业，企业无扣税，个人扣税
         
        $accountinfo = $this->get_user_account($userid);
        if($accountinfo['accounttype'] == '1'){
            $info = $this->get_user_userratio($userid, $data['cincome']);
            $data['userratio']  = json_encode($info['teaminfo']);
            $data['taxmoney'] = $info['taxmoney'];
            $data['aftertaxmoney'] = $info['aftertaxmoney'];
        }else{
            $data['aftertaxmoney'] = $data['cincome'];
        }
   		$id = $this->AdMonthIncomeModel->add($data);
        if($id){
            $appinfo = $this->get_user_appid($userid);
            $appdata['pkCanalId']=array("in",implode(",",$appinfo));
            $appdata['incomeid'] = '0';
            $appdata['pkMonth'] = $this->get_last_month();
            $this->AdIncomeModel->edit($appdata,array('incomeid'=>$id));

            $result['info'] = '确定成功';
            $result['path'] = '/admin/business/incomelist';
            return $result;
        }else{
        	$result['status'] = 2;
        	$result['info'] = '确定失败';
        	$result['path'] = '/admin/business/incomelist';
            return $result;
        }
    }
    
    /**
     * 批量确定结算记录
     */
    function add_account_list_multiple(){
    	$postdata = $this->input->post();
    	$confirmIdList = $postdata['confirmid'];
    	$existlist = array();
    	$faillist = array();
    	$info = array();
    	foreach($confirmIdList as $k=>$v){
    		$re = $this->income_confirmation($v);
    		if($re['status'] == 0) $existlist[] = $v;
    		elseif($re['status'] == 2) $faillist[] = $v;
    	}
    	if(!empty($existlist)){
    		$existids = implode(',', $existlist);
    		$where = "userid in (".$existids.")";
    		$usernamelist = $this->UserMemberModel->getList($where,'username');
    		if($usernamelist){
    			$usernames=array();
    			foreach($usernamelist as $v){
    				$usernames[]=$v['username'];
    			}
    			$info=array_merge($info,$usernames);
    			$info[]=' 此月份结算已经生成，请勿重复';
    		}
    	}
    	if(!empty($faillist)){
    		$failids = implode(',', $faillist);
    		$where = "userid in (".$failids.")";
    		$usernamelist = $this->UserMemberModel->getList($where,'username');
    		if($usernamelist){
    			$usernames=array();
    			foreach($usernamelist as $v){
    				$usernames[]=$v['username'];
    			}
    			$info=array_merge($info,$usernames);
    			$info[]=' 确认失败';
    		}
    	}
    
    	if(!empty($faillist) || !empty($existlist)){
    		ajaxReturn($info, 0, '');	
    	}
    	ajaxReturn('', 1, '');
    }
    /**
     * 获取开发者应用id
     */
    function get_user_appid($userid=''){
        $applist = $this->AppInfoModel->getList(array('userid'=>$userid),"appid","appid desc");
        $appinfo = array();
        foreach ($applist as $key=>$v) {
            $appinfo[]=$v['appid'];
        }
        return $appinfo;
    }
    /**
     * 获取团队分成比例情况
     */
    function get_user_userratio($userid='',$sumcincome='0'){
        //团队成员分成比例
        $tax_num = 1000000;
        $sumcincome = $sumcincome/$tax_num;
         
        $accountinfo = $this->get_user_account($userid);
        $team_info = $this->TeamMembersModel->getList(array( 'userid'=>$userid,'status'=>1,'auditstatus' =>1,'isdel'=>0),"id,realname,ratio,idnumber");
        $sum_taxmoney =0;
        foreach($team_info as $key=>$val){
            $befor_tax_money = $sumcincome*$val['ratio']/100;
            $teaminfo[$key]['cincome'] = $tax_num*$befor_tax_money;
            $teaminfo[$key]['taxmoney'] = $tax_num*$this->get_tax_money($befor_tax_money);
            $teaminfo[$key]['aftertaxincome'] = $teaminfo[$key]['cincome'] - $teaminfo[$key]['taxmoney'];
            $teaminfo[$key]['idnumber'] = $val['idnumber'];
            $teaminfo[$key]['realname'] = $val['realname'];
            $teaminfo[$key]['userid'] = $val['id'];
            $sum_taxmoney += $teaminfo[$key]['taxmoney'];
        }
        //开发自己的分成比例
        $key = $key+1;
        $befor_tax_money = $sumcincome*$accountinfo['ratio']/100;
        $teaminfo[$key]['cincome'] = $tax_num*$befor_tax_money;
        $teaminfo[$key]['taxmoney'] = $tax_num*$this->get_tax_money($befor_tax_money);
        $teaminfo[$key]['aftertaxincome'] = $teaminfo[$key]['cincome'] - $teaminfo[$key]['taxmoney'];
        $teaminfo[$key]['idnumber'] = $accountinfo['idnumber'];
        $teaminfo[$key]['realname'] = $accountinfo['realname'];
        $teaminfo[$key]['userid'] = $userid;
        $sum_taxmoney += $teaminfo[$key]['taxmoney'];
        $info['teaminfo'] = $teaminfo;
        $info['taxmoney'] = $sum_taxmoney;
        $info['aftertaxmoney'] = $tax_num*$sumcincome - $sum_taxmoney;
        return $info;
    }

    /**
     * 取得结算记录的 对应的 未结算 收入报表的id
     */
    function get_income_month($userid='',$last_month=''){
        //取得结算的月份
        $applist = $this->AppInfoModel->getList(array('userid'=>$userid),"appid","appid desc");
        $appinfo = array();
        foreach ($applist as $key=>$v) {
            $appinfo[]=$v['appid'];
        }
        $appdata['pkCanalId']=array("in",implode(",",$appinfo));
        $appdata['incomeid'] = '0';
        $last_month = $last_month?$last_month:$this->get_last_month();
        $appdata['pkMonth'] = $last_month;
        //结算的 报表id
        $info = $this->AdIncomeModel->getList($appdata,"id");
        foreach ($info as $key=>$val) {
            $incomeid_arr[] = $val['id'];
        }
        $data['incomeid'] = implode(",",$incomeid_arr);
        return $data;
    }
    /**
     * 获取开发者税前总收入
     */
    function get_sum_income($userid='',$last_month='',$type='',$ifcreate=''){
        $data = array();
        $appdata = array();
        $data['userid'] = $userid;

        $applist = $this->AppInfoModel->getList($data,"appid","appid desc");

        $appinfo = array();
        foreach ($applist as $key=>$v) {
            $appinfo[]=$v['appid'];
        }
        $appdata['pkCanalId']=array("in",implode(",",$appinfo));
        $last_month = $last_month?$last_month:$this->get_last_month();
        $appdata['pkMonth'] = $last_month;
        if($ifcreate == '1') $appdata['incomeid'] = '0';
        if($type == '1') $income_sql = "sum(cincome) as sum_income";
        else $income_sql = "sum(income) as sum_income";
        $info = $this->AdIncomeModel->getRow($appdata,$income_sql);
        return $info['sum_income'];
    }
    /**
     * 获取开发者相应月份的应用收入报表列表
     */
    function get_user_apps($userid='',$last_month=''){
        $data = array();
        $appdata = array();
        $data['userid'] = $userid;

        $applist = $this->AppInfoModel->getList($data,"appid","appid desc");
        $appinfo = array();
        foreach ($applist as $key=>$v) {
            $appinfo[]=$v['appid'];
        }
        $appdata['pkCanalId']=array("in",implode(",",$appinfo));
        $last_month = $last_month?$last_month:$this->get_last_month();
        $appdata['pkMonth'] = $last_month;
        $info = $this->AdIncomeModel->getList($appdata,"*");
        return $info;
    }
    /**
     * 获取月份
     */
    function get_last_month($month_num=1){
        $last_month = date("Y-m",mktime(0, 0 , 0,date("m")-$month_num,1,date("Y")));
        return $last_month;
    }
    /**
     * 个人团体开发者扣税方式
     * 1、个税计算算法：
     *       0-800之间，不收个税
     *        800-4000之间，个税=(x-800)*20%
     *       4000-25000之间，个税=16%x
     *       25000.001-62500,个税=24%x-2000
     *       62500~∞，个税=32%x-7000
     *      (x为收入)
     */
    function get_tax_money($income=''){
        $tax_money = 0;
        if($income <= 800){
            $tax_money = 0;
        }elseif (800 < $income && $income <=4000) {
            $tax_money = ($income - 800) * 0.2;
        }elseif(4000 < $income && $income <=25000){
            $tax_money = $income * 0.16 ;
        }elseif(25000 < $income && $income <=62500){
            $tax_money = $income * 0.24 - 2000;
        }elseif($income > 62500){
            $tax_money = $income * 0.32 - 7000;
        }
        //$after_money = $income - $tax_money;
        return  $tax_money ;
    }
    /**
     * 运营结算列表
     */
    function income_list(){
        $searchmonth = $this->input->get_post('searchmonth') ? $this->input->get_post('searchmonth') : $this->get_last_month();
        $accounttype = $this->input->get_post('accounttype') ? $this->input->get_post('accounttype') : 0;
        $accountmark = $this->input->get_post('accountmark');
        if($accountmark === '' || $accountmark == 9 || $accountmark === false)
        {
            $accountmark = 9;
        }
        $search = trim($this->input->get_post('search'));
        $where['month'] = $searchmonth?$searchmonth:$this->get_last_month();
        $var = "";
        $userwhere = array();
        if(!empty($where['month'] )){
            $var .= 'searchmonth='.$where['month'] ;
        }
        if(!empty($accounttype))
        {
            $userwhere['accounttype'] = $accounttype;
            $var .= '&accounttype='.$accounttype ;
        }
        if($accountmark != 9)
        {
            $userwhere['accountmark'] = $accountmark;
            $var .= '&accountmark='.$accountmark ;
        }
        if(!empty($search))
        {
            $search = str_replace("_","\_",$search);
            $search = str_replace("%","\%",$search);
            $userwhere['username'] = array('like', '%'.$search.'%');
            $var .= '&search='.$search ;
        }
        $userinfolist = array();
        if(!empty($userwhere))
        {
            $userlist = $this->UserMemberModel->getList($userwhere);
            //echo $this->UserMemberModel->getLastSQL();
            if(!empty($userlist))
            {
                $useridarray = array();
                foreach($userlist as $v)
                {
                    $useridarray[] = $v['userid'];
                    $userinfolist[$v['userid']] = $v;
                }
                $where['userid'] = array('in', $useridarray);
            }
            else
            {
                $where['userid'] = 0;
            }
        }
        $p=trim($this->input->get('per_page'));
        if(!$p){
            $p=1;
        }
        $months = $this->AdMonthIncomeModel->getList(array(),"DISTINCT month","month DESC");
        $this->AdMonthIncomeModel->setStrWhere($where);
        $config['per_page'] = 20;
        $count = $this->AdMonthIncomeModel->getCount();
        $list = $this->AdMonthIncomeModel->getList($where,"*","id desc", "", $p,$config['per_page']);

        foreach($list as $key=>$val){
            $userid = $val['userid'];
            $list[$key]['userratio_arr'] = json_decode($val['userratio']);
            $users = isset($userinfolist[$userid]) ? $userinfolist[$userid] : $this->UserMemberModel->getRow(array('userid'=>$userid));
            $accountinfo = $this->get_user_account($userid);
            $list[$key]['username'] = $users['username'];
            $account_type =$accountinfo['accounttype'];
            $list[$key]['accounttypename'] = $this->accounttypearr[$account_type] ;
            $list[$key]['accountmarkname'] = $this->accountmarkname[$users['accountmark']] ;
            $list[$key]['contact'] = $accountinfo['contact'];
        }
        $this->load->library('page');
        $page=$this->page->page_show('/admin/business/income_list?'.$var,$count,$config['per_page']);

        $this->assign('page', $page);
        $this->assign('per_page', $config['per_page']);
        $this->assign('count', $count);
        $this->assign('months', $months);
        $this->assign('searchmonth', $searchmonth);
        $this->assign('search', $search);
        $this->assign('accounttype', $accounttype);
        $this->assign('accountmark', $accountmark);
        $this->assign('list', $list);
        $this->display('admin/business/income_list.html');
    }
    /**
     * 获取所有结算的月份
     */
    function get_paid_month(){
        $months = $this->AdIncomePaidModel->getList(array(),"DISTINCT paidmonth","paidmonth DESC");
        return $months;
    }
    /**
     * 提款单列表
     */
    function invoicelist($type){
        $paidmonth = $this->input->get_post('paidmonth')?$this->input->get_post('paidmonth'):$this->get_last_month(0);
        $username = $this->input->get_post('username');
        $months = $this->get_paid_month();
        $where['paidmonth'] = $paidmonth?$paidmonth:$this->get_last_month('0');
        $var = "";
        if(!empty($paidmonth)){
            $var .= 'paidmonth='.$paidmonth;
        }
        if(!empty($username)){
            $username = str_replace("_","\_", $username);
            $username = str_replace("%","\%", $username);
            $where['username']  = array('like', '%'.$username.'%');
            $var .= 'username='.$username;
        }

        $p=trim($this->input->get('per_page'));
        if(!$p){
            $p=1;
        }
        //$where['othercondition'] =  " and accounttype !=1";
        $this->AdIncomePaidModel->setStrWhere($where);
        $config['per_page'] = 20;
        $count = $this->AdIncomePaidModel->getCount();
        $list = $this->AdIncomePaidModel->getList($where,"*","id desc", "", $p,$config['per_page']);

        foreach($list as $key=>$val){
            $list[$key]['userratio_arr'] = json_decode($val['userratio']);
            $userid = $val['userid'];
            $account_info = $this->get_user_account($userid);
            $list[$key]['accounttypename'] = $account_info['accounttypename'];
            $list[$key]['realname'] = $account_info['realname'];
            $list[$key]['idnumber'] = $account_info['idnumber'];
            $list[$key]['bankusername'] = $account_info['bankusername'];
            $list[$key]['bank'] = $account_info['bank'];
            $list[$key]['bankaddress'] = $account_info['bankaddress'];
            $list[$key]['bankaccount'] = $account_info['bankaccount'];
            $list[$key]['companyname'] = $account_info['companyname'];
        }
        $this->load->library('page');
        if($type == 'view'){
            $page=$this->page->page_show('/admin/business/paidlist?'.$var,$count,$config['per_page']);
        }else{
            $page=$this->page->page_show('/admin/business/invoicelist?'.$var,$count,$config['per_page']);
        }
        $this->assign('page', $page);
        $this->assign('per_page', $config['per_page']);
        $this->assign('count', $count);
        $this->assign('months', $months);
        $this->assign('paidmonth', $paidmonth);
        $this->assign('list', $list);

        $this->assign('username', $username);
        $this->display('admin/business/invoicelist.html');
    }
    /**
     * 财务打款单
     */
    function paidlist(){
        $type = 'view';
        $this->assign('type', $type);
        $this->invoicelist($type);
    }
    /**
     * 确认收到发票
     */
    function confirm_invoice(){
        $id = $this->input->get('id');
        $where['id'] = $id;
        $id = $this->AdIncomePaidModel->edit($where,array('status'=>2));
        if($id){
            $this->message->msg('确认收到发票成功','/admin/business/invoicelist');
            return;
        }  else {
            $this->message->msg('确认收到发票失败','/admin/business/invoicelist');
            return;
        }
    }
    /**
     * 财务报表下载
     */
    function down_income(){
        $months = $this->get_paid_month();
        $this->assign('months', $months);
        $this->display('admin/business/income_report.html');
    }
    function down_paid_report(){
        $month = $this->input->get('month');
        $accounttypearr = array('0'=>"个人/团体",'1'=>"个人/团体",'2'=>"企业",'3'=>"个体工商");
        $list = $this->AdIncomePaidModel->getList(array('paidmonth'=>$month),"*","id DESC");
        foreach($list as $key=>$val){
            $list[$key]['userratio_arr'] = json_decode($val['userratio']);
            $userid = $val['userid'];
            $account_info = $this->get_user_account($userid);
            $list[$key]['accounttypename'] = $account_info['accounttypename'];
            $users = $this->UserMemberModel->getRow(array('userid'=>$userid));
            $list[$key]['accountmarkname'] = $this->accountmarkname[$users['accountmark']];
            $list[$key]['realname'] = $account_info['realname'];
            $list[$key]['idnumber'] = "'".$account_info['idnumber'];
            $list[$key]['bankusername'] = $account_info['bankusername'];
            $list[$key]['bankaccount'] = $account_info['bankaccount'];
            $list[$key]['companyname'] = $account_info['companyname'];
            $list[$key]['bank'] = $account_info['bank'];
            $list[$key]['bankaddress'] = $account_info['bankaddress'];
        }
        $this->assign('list', $list);

        print_r($list);exit;

        $filename =  $month."提款报表.xls";
        header("Content-type:application/vnd.ms-excel;charset=UTF-8");
        header("Content-Disposition:attachment;filename=$filename");
        $this->display('admin/business/down_paid_report.html');
    }
    /**
     * 用户账号认证信息
     */
    function get_user_account($userid=''){
        $account_where = array('userid'=>$userid,'status'=>1,'auditstatus'=>1);
        $accountinfo = $this->AdAccountinfoModel->getRow($account_where,'*','createtime DESC');
        $account_type = $accountinfo['accounttype'];
        if(!empty($accountinfo)) $accountinfo['accounttypename'] = $this->accounttypearr[$account_type] ;
        return $accountinfo;
    }

    /**
     * 用户详细信息
     */
    function account_detail()
    {
        $userid = $this->input->get("userid");
        if(empty($userid))
        {
            echo "用户ID错误！";
            exit;
        }
        $accountInfo = $this->get_user_account($userid);
        if(empty($accountInfo))
        {
            echo "账号不存在！";
            exit;
        }
        $account_type = empty($accountInfo['accounttype'])?"0":$accountInfo['accounttype'];
        $accountInfo['accounttypename'] = $this->accounttypearr[$account_type] ;
        $this->assign('accountInfo', $accountInfo);
        if($account_type == 1)
        {
            $accountGroupInfo = $team_info = $this->TeamMembersModel->getList(array( 'userid'=>$userid,'status'=>1,'auditstatus' =>1,'isdel'=>0),"id,realname,ratio,idnumber");
            //加入主账号分成比例
            $accounttmp[] = array('id'=>$userid, 'realname'=>$accountInfo['bankusername'], 'ratio'=>$accountInfo['ratio'], 'idnumber'=>$accountInfo['idnumber']);
            if(empty($accountGroupInfo))
            {
                $accountGroupInfo = $accounttmp;
            }
            else
            {
                $accountGroupInfo = array_merge_recursive($accounttmp, $accountGroupInfo);
            }
            $this->assign('accountGroupInfo', $accountGroupInfo);
        }
        $this->display('admin/business/account_detail.html');
    }

    /**
     * 保存用户标识
     */
    function saveaccountmark()
    {
        $accountmark = $this->input->post('accountmark');
        $userid = $this->input->post('userid');
        $userinfo = $this->UserMemberModel->getRow(array('userid'=>$userid));
        if(empty($userinfo))
        {
            ajaxReturn('', 0, '该用户不存在，请刷新重试！');
        }
        if($userinfo['accountmark'] == $accountmark)
        {
            ajaxReturn('', 0, '账户标识未改变。');
        }
        $this->UserMemberModel->edit(array('userid'=>$userid), array('accountmark'=>$accountmark));
        ajaxReturn('', 1, '');
    }

    /**
     * 保存用户标识
     */
    function savesettletype()
    {
        $settletype = $this->input->post('settletype');
        $userid = $this->input->post('userid');
        $userinfo = $this->UserMemberModel->getRow(array('userid'=>$userid));
        if(empty($userinfo))
        {
            ajaxReturn('', 0, '该用户不存在，请刷新重试！');
        }
        if($userinfo['settletype'] == $settletype)
        {
            ajaxReturn('', 0, '结算方式未更改。');
        }
        $this->UserMemberModel->edit(array('userid'=>$userid), array('settletype'=>$settletype));
        ajaxReturn('', 1, '');
    }
}
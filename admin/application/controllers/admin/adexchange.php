<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adexchange extends MY_A_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("AdexchangeModel");
        $this->load->model("DspAdEcpcAppModel");
    }
    
    function ajaxgetpidlist(){
    	$publisherID = $this->input->post('publisherID');
    	$ostypeid = (int)$this->input->post('ostypeid');
    	$this->load->model('AppChannelModel');
    	$where=array('publisherID'=>$publisherID,'ostypeid'=>$ostypeid);
    	$pidList=$this->AppChannelModel->getPidList($where);
    	$data=$pidList;
		if(isset($pidList)&& count($pidList)>0){
			ajaxReturn('查找成功!', 1, $data);
        }
        else{
        	ajaxReturn('该渠道ID不存在!', 0, $data);
        }
    }
    
    /**
     * 新增一条adx系统
     */
    function addadx(){
    	$post = array();
    	$post = $this->input->post();
    	if($this->AdexchangeModel->doAdd($post)){
    		ajaxReturn('保存成功!', 1);
    	}
     	else{
        	ajaxReturn('保存失败!', 0);
        }
    }
	/**
     * adx系统列表
     */
    function adxlist(){
    	$adxlist=$this->AdexchangeModel->getList(array('status'=>1),'*','id');
    	$this->assign('adxlist', $adxlist);
    	$this->display('admin/adexchange/adxlist.html');
    }

    
    function ajaxgetadxinfo(){
    	$id = $this->input->get('id');
    	$adxinfo=$this->AdexchangeModel->getRow(array('id'=>$id));
    	if(isset($adxinfo)&& count($adxinfo)>0){
			ajaxReturn('查找成功！', 1, $adxinfo);
		}
		ajaxReturn('查找失败！', 0, $adxinfo);
    }

	/**
     * 编辑adx info
     */
	function editadxinfo(){
    	$id = $this->input->post('id');
    	$adxname = $this->input->post('adxname');
    	$adxid = $this->input->post('adxid');
    	$iospid = $this->input->post('iospid');
    	$androidpid = $this->input->post('androidpid');
    	$user = $this->session->userdata("admin");
		$admin_id = $user['admin_id'];
    	$where=array('id'=>$id);
    	$editdata=array('adxname'=>$adxname,'adxid'=>$adxid,'iospid'=>$iospid,'androidpid'=>$androidpid,'edittime'=>time(),'admin_id'=>$admin_id);
    	$count=$this->AdexchangeModel->edit($where,$editdata);
    	if($count>0){
			ajaxReturn('编辑成功！', 1);
		}
		ajaxReturn('编辑失败！', 0);
    }
    
    
 	/**
     * 查看dsp_ad_ecpc_app设置
     */
    function dspadecpcapp(){
        $publisherid = trim($this->input->get('publisherid'));
    	$this->load->model('AppChannelModel');
    	$where=array('publisherID'=>$publisherid);
    	$pidinfo=$this->AppChannelModel->getRow($where);
		if(isset($pidinfo) && !empty($pidinfo)){
			$this->assign('appid', $pidinfo['appid']);
			$this->load->model('AppInfoModel');
        	$appInfo = $this->AppInfoModel->getRow(array('appid'=>$pidinfo['appid']));
			$this->assign('ostypeid', $appInfo['ostypeid']);
	        $data = $this->DspAdEcpcAppModel->getList(array('appid'=>$pidinfo['appid'],'status'=>0));
	        $this->assign('data', $data);
	        $this->assign('publisherid', $publisherid);
	        $this->display('admin/adexchange/dspadecpcapp.html');
        }
        else{
        	echo "<script> {window.alert('该渠道不存在');} </script>";
        	$this->adxlist();
        }
    }
    
  /**
     * 新增一条adx系统
     */
    function adddspadecpcapp(){
    	$data = array();
    	$data['appid'] = $this->input->post("appid");
    	$data['siteid'] = $this->input->post('siteid');
    	$data['dsp'] = $this->input->post('dsp');
    	$data['dsptype'] = $this->input->post('dsptype');
    	$data['dspratio'] = $this->input->post('dspratio');
    	if(!preg_match("/^(\d{1,3})(\.\d{0,2})?$/", $data['dsp'])){
             ajaxReturn('DSP广告单价格式错误！保存失败', 0,'DSP广告单价格式错误！保存失败');
        }
    	if(!preg_match("/^(\d{1,3})(\.\d{0,2})?$/", $data['dspratio'])){
            ajaxReturn('DSP广告扣量系数格式错误！保存失败', 0,'DSP广告扣量系数格式错误！保存失败');
        }
    	if($this->DspAdEcpcAppModel->doAdd($data)){
    		ajaxReturn('保存成功!', 1,'保存成功!');
    	}
     	else{
        	ajaxReturn('保存失败!', 0,'保存成功!');
        }
    }
    

    
	/**
     * 编辑adx info
     */
    
	function editdspadecpcapp(){
		
		$data = array();
		$id = $this->input->post("id");
		$where = array('id'=>$id);
    	$data['appid'] = $this->input->post("appid");
    	$data['siteid'] = $this->input->post('siteid');
    	$data['dsp'] = $this->input->post('dsp');
    	$data['dsptype'] = $this->input->post('dsptype');
    	$data['dspratio'] = $this->input->post('dspratio');
    	if(!preg_match("/^(\d{1,3})(\.\d{0,2})?$/", $data['dsp'])){
            ajaxReturn('DSP广告单价格式错误！保存失败', 0,'DSP广告单价格式错误！保存失败');
        }
    	if(!preg_match("/^(\d{1,3})(\.\d{0,2})?$/", $data['dspratio'])){
            ajaxReturn('DSP广告扣量系数格式错误！保存失败', 0,'DSP广告扣量系数格式错误！保存失败');
        }
    	$user = $this->session->userdata("admin");
		$data['admin_id'] = $user['admin_id'];
    	$data['uptime'] = time();
    	$count=$this->DspAdEcpcAppModel->edit($where,$data);
    	if($count>0){
			ajaxReturn('编辑成功！', 1,"编辑成功！");
		}
		ajaxReturn('编辑失败！', 0,'编辑失败！');
    }
    
    /**
     * 跳转到新增dsp_ad_ecpc_app页面
     */
 	function adddspinfo(){
 		$publisherid = $this->input->get("publisherid");
 		$appid = $this->input->get("appid");
 		$this->load->model("AppInfoModel");
 		$appInfo = $this->AppInfoModel->getRow(array('appid'=>$appid));
		$this->assign('ostypeid', $appInfo['ostypeid']);
        $this->assign('mark', '新增');
        $this->assign('appid', $appid);
        $this->assign('publisherid', $publisherid);
        $this->display('admin/adexchange/adddspadecpcapp.html');
    }
    
    /**
     * 跳转到编辑dsp_ad_ecpc_app页面
     */
 	function editdspinfo(){
 		$publisherid = $this->input->get("publisherid");
 		$appid = $this->input->get("appid");
 		$id = $this->input->get("id");
 		$where = array();
 		$where['id']=$id;
    	$data=$this->DspAdEcpcAppModel->getRow($where);
 		if(empty($data) || !isset($data['id']))
        {
            return ;
        }
        $this->assign('mark', '编辑');
        $this->assign('appid', $appid);
        $this->assign('publisherid', $publisherid);
        $this->assign('data', $data);
        $this->display('admin/adexchange/adddspadecpcapp.html');
    }
    
     /**
     * 新增或者编辑dsp_ad_ecpc_app信息
     */
	function ajaxdspadecpcappsave(){
        if($this->input->post('savetype') == 'add')
        {
            $this->adddspadecpcapp();
        }
        else if($this->input->post('savetype') == 'edit' && $this->input->post('id') > 0)
        {
            $this->editdspadecpcapp();
        }
    }
    
    function ajaxdel(){
       
        $where['id'] = trim($this->input->post('id'));
        if(empty($where['id']))
        {
            ajaxReturn('1删除记录有误，请刷新后重试！', 0, '删除记录有误，请刷新后重试！');
        }
        $dspinfo = $this->DspAdEcpcAppModel->getRow($where);
        if(empty($dspinfo))
        {
            ajaxReturn('2删除记录有误，请刷新后重试！', 0, '删除记录有误，，请刷新后重试！');
        }
        $user = $this->session->userdata("admin");
        $status = $this->DspAdEcpcAppModel->edit($where,array('status'=>1,'uptime'=>time(),'admin_id'=>$user['admin_id']));
        if($status)
        {
            ajaxReturn('成功删除！', 1, '成功删除！');
        }
        ajaxReturn('3删除记录有误，请刷新后重试！', 0, '删除记录有误，请刷新后重试！');
    }
}
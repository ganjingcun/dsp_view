<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adproject extends MY_A_Controller
{
    function __construct(){
        parent::__construct();
        $this->load->model('UserMemberModel');
        $this->load->model('AdprojectModel');
        $this->load->model('AdprojectCampaignModel');
        $this->load->model('AdprojectDaylevelModel');
        $this->load->model('AdProjectLogModel');
        $configlist = $this->SystemConfigModel->getAllConfig();
        $this->assign('configlist', $configlist);
    }

    /**
     * 内投订单列表页
     * Enter description here ...
     */
    function selfcase(){
        $adstatus = $this->input->get('ad_status')?(int)$this->input->get('ad_status'):2;
        $projectid = (int)$this->input->get('projectid');
        $adtime = $this->input->get('ad_time')?(int) $this->input->get('ad_time'):2;
        $key = trim($this->input->get('key'));
    	if($projectid){
        	$adstatus = 4;
        	$key = '';
        	$adtime = 1;
        }
        $sortfield = trim($this->input->get('sortfield'));
        $orderData['projectid'] = trim($this->input->get('projectid')) ? trim($this->input->get('projectid')) : 'desc';
        $orderData['projectname'] = trim($this->input->get('projectname')) ? trim($this->input->get('projectname')) : 'desc';
        $orderData['operations'] = trim($this->input->get('operations')) ? trim($this->input->get('operations')) : 'desc';
        $orderData['platform'] = trim($this->input->get('platform')) ? trim($this->input->get('platform')) : 'desc';
        $orderData['ostype'] = trim($this->input->get('ostype')) ? trim($this->input->get('ostype')) : 'desc';
        $orderData['starttime'] = trim($this->input->get('starttime')) ? trim($this->input->get('starttime')) : 'desc';
        $orderData['endtime'] = trim($this->input->get('endtime')) ? trim($this->input->get('endtime')) : 'desc';
        $orderData['company'] = trim($this->input->get('company')) ? trim($this->input->get('company')) : 'desc';
        $orderData['price'] = trim($this->input->get('price')) ? trim($this->input->get('price')) : 'desc';
        $orderData['daylevel'] = trim($this->input->get('daylevel')) ? trim($this->input->get('daylevel')) : 'desc';
        $orderData['sales'] = trim($this->input->get('sales')) ? trim($this->input->get('sales')) : 'desc';
        $orderData['adstatus'] = trim($this->input->get('adstatus')) ? trim($this->input->get('adstatus')) : 'desc';
        $defaultsort = '';
        foreach ($orderData as $k => $v)
        {
            if($sortfield == $k)
            {
                $order[]= $k . ' ' . $v;
                if($v == 'desc')
                {
                    $defaultsort .= '&'.$k.'='.'asc';
                    $orderData[$k] = 'asc';
                }
                else
                {
                    $orderData[$k] = 'desc';
                    $defaultsort .= '&'.$k.'='.'desc';

                }
            }
            else
            {
                $defaultsort .= '&'.$k.'='.$v;
            }
        }
        $defaultsort .= "&ad_status=".$adstatus;
        $where = array('casetype' => 1);
     	if(!empty($projectid))
        {
        	if(stripos($projectid,',') != false){
        		$projectids = array_filter(explode(',',$projectid));
        		$where['projectid'] = array('IN',$projectids);
        	} 
        	else{
        		$where['projectid'] = array('LIKE', '%'.$projectid.'%');
        	}
        }
        if(!empty($key))
        {
            $where['projectname'] = array('LIKE', '%'.$key.'%');
            $defaultsort .= "&key=".$key;
        }
    	switch ($adstatus)
        {
            case 0 :
            	//中+待
                $where['adstatus'] = 0;
                $where['endtime'] = array('EGT',strtotime(date("Y-m-d").' ' . '00:00:00'));
                break;
            case 1 :
            	//暂停
                $where['adstatus'] = 1;
                $where['endtime'] = array('EGT',strtotime(date("Y-m-d").' ' . '00:00:00'));
                break;
            case 2 :
            	//中
                $where['adstatus'] = 0;
                $where['starttime'] = array('ELT',time());
                $where['endtime'] = array('EGT',strtotime(date("Y-m-d").' ' . '00:00:00'));
                break;
            case 3 :
            	//待
                $where['adstatus'] = 0;
                $where['starttime'] = array('GT',strtotime(date("Y-m-d").' ' . '00:00:00'));
                break;
            case 4 :
            	//全
            	$where['othercondition'] = " and adstatus != '2'";
                break;
            case 5 :
            	//结束
//				$where['othercondition'] = " and adstatus != '2'";
				$where['adstatus'] = array('NEQ','2');
                $where['endtime'] = array('LT',strtotime(date("Y-m-d").' ' . '00:00:00'));
                break;
        }
        $firstdayoflastmonth = strtotime(date('Y-m-01', strtotime('-1 month')). ' ' . '00:00:00'); 
        $lastdayoflastmonth = strtotime(date('Y-m-t', strtotime('-1 month')). ' ' . '00:00:00'); 
        $firstday = date('Y-m-01', strtotime(date("Y-m-d")));
        $firstdayofthismonth = strtotime($firstday. ' ' . '00:00:00'); 
        $lastdayofthismonth = strtotime(date('Y-m-d', strtotime("$firstday +1 month -1 day")). ' ' . '00:00:00'); 
    	switch ($adtime)
        {
            case 1 :
                break;
            case 2 :
            	if($adstatus != '0' && $adstatus != '2' && $adstatus!='5'){
            		$where['endtime'] = array('EGT',$firstdayofthismonth);
            	}
            	if($adstatus == '5'){
            		unset($where['endtime']);
            		$between = " and endtime BETWEEN ".trim($firstdayofthismonth)." AND ".trim(strtotime(date("Y-m-d").' 00:00:00'))." ";
            		$where['othercondition'] = $between;
            	}
                break;
            case 3 :
                $where['starttime'] = array('ELT',$lastdayoflastmonth);
                $where['endtime'] = array('EGT',$firstdayoflastmonth);
                break;
        }
        $this->assign('adtime', $adtime);
        $this->assign('adstatus', $adstatus);
        $this->assign('key', $key);
        $p=trim($this->input->get('per_page'));
        if(!$p){
            $p=1;
        }
        $config['per_page'] = 50;
        $count = $this->AdprojectModel->getCount($where);
        $this->load->library('page');
        $current_page =  $p==0 ? 1 : $p;
        $page=$this->page->page_show('/admin/adproject/selfcase?adstatus='.$adstatus.'&key='.$key.$defaultsort,$count,$config['per_page']);
        $this->pagination->create_links();
        $this->smarty->assign('page', $page);
        $this->smarty->assign('per_page', $config['per_page']);
        $this->smarty->assign('count', $count);
        $this->smarty->assign('defaultsort', $defaultsort);
        $this->smarty->assign('sortfield', $sortfield);
        $this->smarty->assign('orderData', $orderData);
         if(empty($order))
        {
            $orderby = ' createtime desc,projectid desc ';
        }
        else
        {
            $orderby = implode(',', $order);
        }
        $list = array();
        $list = $this->AdprojectModel->getList($where, '*',$orderby, '', $p, $config['per_page']);
        if(isset($list) && count($list)>0){
	        foreach($list as $k=>$v)
	        {
	            $list[$k]['adstatustext'] = $this->getAdStatus($v);
	        }
         }
        $this->assign('list', $list);
        $this->display('admin/adproject/selfcase.html');
    }

    /**
     * 内投订单列表页
     * Enter description here ...
     */
    function outcase(){
        $adstatus = $this->input->get('ad_status')?(int)$this->input->get('ad_status'):2;
        $key = trim($this->input->get('key'));
        $projectid = $this->input->get('projectid');
        $adtime = $this->input->get('ad_time')?(int) $this->input->get('ad_time'):4;
    	if($projectid){
        	$adstatus = 4;
        	$key = '';
        	$adtime = 1;
        }
        $sortfield = trim($this->input->get('sortfield'));
        $orderData['projectid'] = trim($this->input->get('projectid')) ? trim($this->input->get('projectid')) : 'desc';
        $orderData['projectname'] = trim($this->input->get('projectname')) ? trim($this->input->get('projectname')) : 'desc';
        $orderData['operations'] = trim($this->input->get('operations')) ? trim($this->input->get('operations')) : 'desc';
        $orderData['computation'] = trim($this->input->get('computation')) ? trim($this->input->get('computation')) : 'desc';
        $orderData['ostype'] = trim($this->input->get('ostype')) ? trim($this->input->get('ostype')) : 'desc';
        $orderData['starttime'] = trim($this->input->get('starttime')) ? trim($this->input->get('starttime')) : 'desc';
        $orderData['endtime'] = trim($this->input->get('endtime')) ? trim($this->input->get('endtime')) : 'desc';
        $orderData['shortname'] = trim($this->input->get('shortname')) ? trim($this->input->get('shortname')) : 'desc';
        $orderData['price'] = trim($this->input->get('price')) ? trim($this->input->get('price')) : 'desc';
        $orderData['outers'] = trim($this->input->get('outers')) ? trim($this->input->get('outers')) : 'desc';
        $orderData['adstatus'] = trim($this->input->get('adstatus')) ? trim($this->input->get('adstatus')) : 'desc';
        $defaultsort = '';
        foreach ($orderData as $k => $v)
        {
            if($sortfield == $k)
            {
                $order[]= $k . ' ' . $v;
                if($v == 'desc')
                {
                    $defaultsort .= '&'.$k.'='.'asc';
                    $orderData[$k] = 'asc';
                }
                else
                {
                    $orderData[$k] = 'desc';
                    $defaultsort .= '&'.$k.'='.'desc';

                }
            }
            else
            {
                $defaultsort .= '&'.$k.'='.$v;
            }
        }
        $defaultsort .= "&ad_status=".$adstatus;
        $where = array('casetype' => 2);
     	if(!empty($projectid))
        {
        	if(stripos($projectid,',') != false){
        		$projectids = array_filter(explode(',',$projectid));
        		$where['projectid'] = array('IN',$projectids);
        	} 
        	else{
        		$where['projectid'] = array('LIKE', '%'.$projectid.'%');
        	}
        }
        if(!empty($key))
        {
            $where['projectname'] = array('LIKE', '%'.$key.'%');
            $defaultsort .= "&key=".$key;
        }
        switch ($adstatus)
        {
            case 0 :
            	//中+待
                $where['adstatus'] = 0;
                $where['endtime'] = array('EGT',strtotime(date("Y-m-d").' ' . '00:00:00'));
                break;
            case 1 :
            	//暂停
                $where['adstatus'] = 1;
                $where['endtime'] = array('EGT',strtotime(date("Y-m-d").' ' . '00:00:00'));
                break;
            case 2 :
            	//中
                $where['adstatus'] = 0;
                $where['starttime'] = array('ELT',time());
                $where['endtime'] = array('EGT',strtotime(date("Y-m-d").' ' . '00:00:00'));
                break;
            case 3 :
            	//待
                $where['adstatus'] = 0;
                $where['starttime'] = array('GT',strtotime(date("Y-m-d").' ' . '00:00:00'));
                break;
            case 4 :
            	//全
            	$where['othercondition'] = " and adstatus != '2'";
                break;
            case 5 :
            	//结束
				$where['adstatus'] = array('NEQ','2');
                $where['endtime'] = array('ELT',strtotime(date("Y-m-d").' ' . '00:00:00'));
                break;
        }
        $firstdayoflastmonth = strtotime(date('Y-m-01', strtotime('-1 month')). ' ' . '00:00:00'); 
        $lastdayoflastmonth = strtotime(date('Y-m-t', strtotime('-1 month')). ' ' . '00:00:00'); 
        $firstday = date('Y-m-01', strtotime(date("Y-m-d")));
        $firstdayofthismonth = strtotime($firstday. ' ' . '00:00:00'); 
        $lastdayofthismonth = strtotime(date('Y-m-d', strtotime("$firstday +1 month -1 day")). ' ' . '00:00:00'); 
   		switch ($adtime)
        {
            case 1 :
                break;
            case 2 :
        		if($adstatus != '0' && $adstatus != '2' && $adstatus!='5'){
            		$where['endtime'] = array('EGT',$firstdayofthismonth);
            	}
            	if($adstatus == '5'){
            		unset($where['endtime']);
            		$between = " and endtime BETWEEN ".trim($firstdayofthismonth)." AND ".trim(strtotime(date("Y-m-d").' 00:00:00'))." ";
            		$where['othercondition'] = $between;
            	}
                break;
            case 3 :
                $where['starttime'] = array('ELT',$lastdayoflastmonth);
                $where['endtime'] = array('EGT',$firstdayoflastmonth);
                break;
        }
        $this->assign('adtime', $adtime);
        $this->assign('adstatus', $adstatus);
        $this->assign('key', $key);
        $p=trim($this->input->get('per_page'));
        if(!$p){
            $p=1;
        }
        $config['per_page'] = 50;
        
        $count = $this->AdprojectModel->getCount($where);
        $this->load->library('page');
        $current_page =  $p==0 ? 1 : $p;
        $page=$this->page->page_show('/admin/adproject/outcase?ad_status='.$adstatus.'&key='.$key.$defaultsort,$count,$config['per_page']);
        $this->pagination->create_links();
        $this->smarty->assign('page', $page);
        $this->smarty->assign('per_page', $config['per_page']);
        $this->smarty->assign('count', $count); 
        $this->smarty->assign('defaultsort', $defaultsort);
        $this->smarty->assign('sortfield', $sortfield);
        $this->smarty->assign('orderData', $orderData);
        if(empty($order))
        {
            $orderby = ' createtime desc,projectid desc ';
        }
        else
        {
            $orderby = implode(',', $order);
        }
        $list = array();
        $list = $this->AdprojectModel->getList($where, '*', $orderby, '', $p, $config['per_page']);
        if(isset($list) && count($list)>0){
	        foreach($list as $k=>$v)
	        {
	        	unset($list[$k]['daylevel']);
	        	$daylevels =  $this->AdprojectDaylevelModel->getList(array('projectid'=>$v['projectid'],'status'=>0),'casedate,daylevel','id asc');
	        	if (count($daylevels)>0){
	        		$list[$k]['day_level'] = $daylevels;
	        	}
	        	else{
	        		$list[$k]['day_level'] = "";
	        	}
	            $list[$k]['adstatustext'] = $this->getAdStatus($v);
	        }
        }
        $this->assign('list', $list);
        $this->display('admin/adproject/outcase.html');
    }
    /**
     * 判断订单状态
     */
    private function getAdStatus($data)
    {
        if($data['adstatus'] == 1 && $data['endtime']>=strtotime(date("Y-m-d").' ' . '00:00:00'))
        {
            return '已暂停';
        }
        if($data['starttime'] <= time() && $data['endtime'] >= strtotime(date("Y-m-d").' ' . '00:00:00'))
        {
            return '投放中';
        }
        if($data['starttime'] > strtotime(date("Y-m-d").' ' . '00:00:00'))
        {
            return '待投放';
        }
        if($data['endtime'] < strtotime(date("Y-m-d").' ' . '00:00:00') && $data['adstatus'] != 2)
        {
            return '投放结束';
        }
    	if($data['adstatus'] == 2)
        {
            return '已删除';
        }
    }

    /**
     * 新增内投订单
     * Enter description here ...
     */
    function addselfcase(){
        $this->assign('mark', '新增');
        $adstatus = $this->input->get('adstatus');
        $relateid = $this->input->get('relateid')?$this->input->get('relateid'):'0';
        $key = $this->input->get('key');
        $this->assign('adstatus', $adstatus);
        $this->assign('key', $key);
        $this->assign('relateid', $relateid);
        $this->display('admin/adproject/addselfcase.html');
    }

    /**
     * 新增外方订单
     * Enter description here ...
     */
    function addoutcase(){
        $this->assign('mark', '新增');
        $adstatus = $this->input->get('adstatus');
        $relateid = $this->input->get('relateid');
        if($relateid){
        	$relateinfo = $this->AdprojectModel->getRow(array('projectid'=>$relateid));
        	$this->assign('relateinfo', $relateinfo);
        }
        $key = $this->input->get('key');
        $this->assign('adstatus', $adstatus);
        $this->assign('key', $key);
        $this->display('admin/adproject/addoutcase.html');
    }

    /**
     * 修改内投订单
     * Enter description here ...
     */
    function editselfcase(){
        $projectid = $this->input->get('projectid');
        $adstatus = $this->input->get('adstatus');
        $key = $this->input->get('key');
        $data = $this->AdprojectModel->getRow(array('projectid'=>$projectid));
        if(empty($data) || !isset($data['projectid']))
        {
            return ;
        }
        $starttime = $data['starttime'];
        $endtime = $data['endtime'];
        $data['starttime'] = date('Y-m-d', $starttime);
        $data['endtime'] = date('Y-m-d', $endtime);
        $data['price'] = formatmoney($data['price'],'get',2,'.');
		$this->load->model('AdprojectCampaignModel');
		$data['adcampaignlist']= $this->AdprojectCampaignModel->getList(array('projectid'=>$projectid,'status'=>0));
        $this->assign('mark', '修改');
        $this->assign('data', $data);
        $this->assign('adstatus', $adstatus);
        $this->assign('key', $key);
        $this->display('admin/adproject/addselfcase.html');
    }

    /**
     * 修改外放订单
     * Enter description here ...
     */
    function editoutcase(){
        $projectid = $this->input->get('projectid');
        $adstatus = $this->input->get('adstatus');
        $key = $this->input->get('key');
        $data = $this->AdprojectModel->getRow(array('projectid'=>$projectid));
        if(empty($data) || !isset($data['projectid']))
        {
            return ;
        }
        $selfcasecount = $this->AdprojectModel->getCount(array('othercondition' => " adstatus != '2' and casetype='1' "),'*','projectid desc');
        $selfcaselist = $this->AdprojectModel->getList(array('othercondition' => " adstatus != '2'  and casetype='1' "),'*','projectid desc');
        $this->assign('selfcaselist', $selfcaselist);
        $this->assign('selfcasecount', $selfcasecount);
        $starttime = $data['starttime'];
        $endtime = $data['endtime'];
        $data['starttime'] = date('Y-m-d', $starttime);
        $data['endtime'] = date('Y-m-d', $endtime);
        $data['price'] = formatmoney($data['price'],'get',2,'.');
        unset($data['daylevel']);
		$data['daylevel'] = $this->AdprojectDaylevelModel->getList(array('projectid'=>$projectid,'status'=>0),'*','id asc');
        $this->assign('mark', '修改');
        $this->assign('data', $data);
        $this->assign('adstatus', $adstatus);
        $this->assign('key', $key);
        $this->display('admin/adproject/addoutcase.html');
    }

    /**
     * 保存adproject
     */
    function ajaxadprojectsave(){
        if($this->input->post('savetype') == 'add')
        {
            $this->adprojectadd();
        }
        else if($this->input->post('savetype') == 'edit' && $this->input->post('projectid') > 0)
        {
            $this->adprojectedit();
        }
    }

    /**
     * 添加case
     * Enter description here ...
     */
    private function adprojectadd()
    {    				
    	$data['projectname'] = $this->input->post('projectname')?trim($this->input->post('projectname')):"";
    	$data['operations'] = $this->input->post('operations')?trim($this->input->post('operations')):"";
    	$data['outers'] = $this->input->post('outers')?trim($this->input->post('outers')):""; //casetype=2
    	$data['computation'] = $this->input->post('computation')?trim($this->input->post('computation')):"";//casetype=2
    	$data['platform'] = $this->input->post('platform')?trim($this->input->post('platform')):"";
    	$data['ostype'] = $this->input->post('ostype')?trim($this->input->post('ostype')):"";
    	$data['adcampaignlist'] = $this->input->post('adcampaignlist')?$this->input->post('adcampaignlist'):array();
    	$data['starttime'] = $this->input->post('starttime')?trim($this->input->post('starttime')):"";
    	$data['endtime'] = $this->input->post('endtime')?trim($this->input->post('endtime')):"";
        $data['company'] = $this->input->post('company')?trim($this->input->post('company')):"";
        $data['shortname'] = $this->input->post('shortname')?trim($this->input->post('shortname')):"";//casetype=2
        $data['price'] = $this->input->post('price')?trim($this->input->post('price')):"";
        $data['casetype'] = $this->input->post('casetype')?trim($this->input->post('casetype')):"1";
        $data['daylevel'] = $this->input->post('daylevel')?trim($this->input->post('daylevel')):"";        	
        $data['sales'] = $this->input->post('sales')?trim($this->input->post('sales')):"";//casetype=1
        $data['description'] = $this->input->post('description')?trim($this->input->post('description')):"";
        $data['relateid'] = $this->input->post('relateid')?trim($this->input->post('relateid')):0;
        
        $status = $this->AdprojectModel->doAdd($data);
        if($status['status'])
        {
            ajaxReturn('添加成功！', 1, '添加成功！');
        }
        ajaxReturn($status['info'], $status['status'], $status['data']);
    }

    /**
     * 编辑case
     * Enter description here ...
     */
    private function adprojectedit()
    {
        $where['projectid'] = $this->input->post('projectid')?trim($this->input->post('projectid')):"";
        
        $data['projectname'] = $this->input->post('projectname')?trim($this->input->post('projectname')):"";
        
    	$data['operations'] = $this->input->post('operations')?trim($this->input->post('operations')):"";
    	
    	$data['outers'] = $this->input->post('outers')?trim($this->input->post('outers')):null;
    	
    	$data['computation'] = $this->input->post('computation')?trim($this->input->post('computation')):"";
    	
    	$data['platform'] = $this->input->post('platform')?trim($this->input->post('platform')):"";
    	
    	$data['ostype'] = $this->input->post('ostype')?trim($this->input->post('ostype')):"";
    	
    	$data['adcampaignlist'] = $this->input->post('adcampaignlist')?$this->input->post('adcampaignlist'):array();
    	
    	$data['starttime'] = $this->input->post('starttime')?trim($this->input->post('starttime')):"";
    	
    	$data['endtime'] = $this->input->post('endtime')?trim($this->input->post('endtime')):"";
    	
        $data['company'] = $this->input->post('company')?trim($this->input->post('company')):"";
        
        $data['shortname'] = $this->input->post('shortname')?trim($this->input->post('shortname')):"";
        
        $data['price'] = $this->input->post('price')?trim($this->input->post('price')):"";
        
        $data['daylevel'] = $this->input->post('daylevel')?trim($this->input->post('daylevel')):"";
        
        $data['sales'] = $this->input->post('sales')?trim($this->input->post('sales')):"";
        
        $data['description'] = $this->input->post('description')?trim($this->input->post('description')):"";
        
        $data['casetype'] = $this->input->post('casetype')?trim($this->input->post('casetype')):"";
        
        $data['relateid'] = $this->input->post('relateid')?trim($this->input->post('relateid')):0;
    	
        $status = $this->AdprojectModel->doEdit($where, $data);
        if($status['status'])
        {
        	 ajaxReturn('修改成功1！', 1, $status['data']);
        }
        ajaxReturn($status['info'], $status['status'], $status['data']);
    }

    /**
     * 暂停case
     * Enter description here ...
     */
    function ajaxeditstatus(){
        $where['projectid'] = trim($this->input->post('projectid'));
        $data['adstatus'] = trim($this->input->post('adstatus'));
        if(empty($where['projectid']))
        {
            ajaxReturn('订单错误，请刷新后重试！', 0, '订单错误，请刷新后重试！');
        }
        if($data['adstatus'] != 1 && $data['adstatus'] != 0 )
        {
            ajaxReturn('订单错误，请刷新后重试！', 0, '订单错误，请刷新后重试！');
        }
        $status = $this->AdprojectModel->save($where, $data);
        if($status)
        {
            ajaxReturn('设置成功！', 1, '设置成功！');
        }
        ajaxReturn('订单错误，请刷新后重试！', 0, '订单错误，请刷新后重试！');
    }

    /**
     * 复制case
     * Enter description here ...
     */
    function ajaxcopycase(){
        $where['id'] = trim($this->input->post('id'));
        if(empty($where['id']))
        {
            ajaxReturn('订单错误，请刷新后重试！', 0, '订单错误，请刷新后重试！');
        }
        $data = $this->AdprojectModel->getRow($where);
        $admin = $this->session->userdata("admin");
        $data['adminid'] = $admin['admin_id'];
        $data['createtime'] = time();
        unset($data['id']);
        $status = $this->AdprojectModel->add($data);
        if($status)
        {
            ajaxReturn('克隆成功！', 1, '克隆成功！');
        }
        ajaxReturn('3订单错误，请刷新后重试！', 0, '订单错误，请刷新后重试！');
    }

    /**
     * 删除case
     * Enter description here ...
     */
    function ajaxdel(){
       
        $where['projectid'] = trim($this->input->post('projectid'));
        if(empty($where['projectid']))
        {
            ajaxReturn('1订单错误，请刷新后重试！', 0, '订单错误，请刷新后重试！');
        }
        $caseinfo = $this->AdprojectModel->getRow($where);
        if(empty($caseinfo))
        {
            ajaxReturn('2订单错误，请刷新后重试！', 0, '订单错误，请刷新后重试！');
        }
        $relateid = $caseinfo['relateid'];
        $count=0;
        $success = 0;
        if($caseinfo['relateid'] != '0'){
	        if($caseinfo['casetype'] == 1){
	        	$count = $this->AdprojectCampaignModel->getCount(array('projectid'=>$where['projectid'],'status'=>0));
	        	if($count>0){
	        		$list = $this->AdprojectCampaignModel->getList(array('projectid'=>$where['projectid'],'status'=>0));
	        		$admin = $this->session->userdata("admin");
        			$this->AdProjectLogModel->add(array('data'=>json_encode($list), 'adminid'=>$admin['admin_id'], 'createtime'=>time()));
	        		$success = $this->AdprojectCampaignModel->edit($where,array('status'=>1));	
		        	if($success <= 0){
	        			return array('status' => 0, 'data'=>array('', '删除相关活动失败！'), 'info' => '删除相关活动失败！');
	        		}
	        	}
		    	if(stripos($relateid,',') != false){
		    		$relateids = array_filter(explode(',',$caseinfo['relateid']));
		        	$relatewhere['projectid'] = array('IN',$relateids);
		        } 
		        else{
		        	$relatewhere['projectid'] = $relateid;
		        }
		        $list = $this->AdprojectModel->getList($relatewhere);
        		$admin = $this->session->userdata("admin");
        		$this->AdProjectLogModel->add(array('data'=>json_encode($list), 'adminid'=>$admin['admin_id'], 'createtime'=>time()));
		        $success = $this->AdprojectModel->edit($relatewhere,array('adstatus'=>'2','edittime'=>time()));
	        	if($success <= 0){
	        		return array('status' => 0, 'data'=>array('', '删除相关活动失败！'), 'info' => '删除相关活动失败！');
	        	}
	        }
	        elseif($caseinfo['casetype'] == 2) {
	        	$count = $this->AdprojectDaylevelModel->getCount(array('projectid'=>$caseinfo['projectid'],'status'=>0));
	        	if($count>0){
	        		$list = $this->AdprojectDaylevelModel->getList(array('projectid'=>$caseinfo['projectid'],'status'=>0));
	        		$admin = $this->session->userdata("admin");
	        		$this->AdProjectLogModel->add(array('data'=>json_encode($list), 'adminid'=>$admin['admin_id'], 'createtime'=>time()));
	        		$success = $this->AdprojectDaylevelModel->edit(array('projectid'=>$caseinfo['projectid']),array('status'=>'1','edittime'=>time()));
	        		if($success <= 0){
	        			return array('status' => 0, 'data'=>array('', '删除相关活动失败！'), 'info' => '删除相关活动失败！');
	        		}
	        	}
	        	$relatewhere['projectid'] = array('LIKE', '%'.$relateid.'%');
	        	$relateinfo = $this->AdprojectModel->getRow($relatewhere);
	        	if(!empty($relateinfo) && stripos($relateinfo['relateid'],',') != false){
		    		$relateids = array_filter(explode(',',$relateinfo['relateid']));
		    		array_splice($relateids,array_search($caseinfo['projectid'],$relateids),1);
		    		$relateidstring = implode(',',$relateids);
		    		$list = $this->AdprojectModel->getList($relatewhere);
	        		$admin = $this->session->userdata("admin");
	        		$this->AdProjectLogModel->add(array('data'=>json_encode($list), 'adminid'=>$admin['admin_id'], 'createtime'=>time()));
		    		$success = $this->AdprojectModel->edit($relatewhere,array('relateid'=>$relateidstring,'edittime'=>time()));
		        	if($success <= 0){
		        		return array('status' => 0, 'data'=>array('', '删除相关活动失败！'), 'info' => '删除相关活动失败！');
		        	}
		        } 
		        elseif(!empty($relateinfo) && stripos($relateinfo['relateid'],',') == false){
		        	$relateidstring = '0';
		        	$list = $this->AdprojectModel->getList($relatewhere);
	        		$admin = $this->session->userdata("admin");
	        		$this->AdProjectLogModel->add(array('data'=>json_encode($list), 'adminid'=>$admin['admin_id'], 'createtime'=>time()));
		        	$success = $this->AdprojectModel->edit($relatewhere,array('relateid'=>$relateidstring,'edittime'=>time()));
			        if($success <= 0){
		        		return array('status' => 0, 'data'=>array('', '删除相关活动失败！'), 'info' => '删除相关活动失败！');
		        	}
		        }
	        }
        }
        $list = $caseinfo;
        $admin = $this->session->userdata("admin");
        $this->AdProjectLogModel->add(array('data'=>json_encode($list), 'adminid'=>$admin['admin_id'], 'createtime'=>time()));
        $status = $this->AdprojectModel->edit($where,array('adstatus'=>2,'edittime'=>time()));
        if($status)
        {
            ajaxReturn('成功删除！', 1, '成功删除！');
        }
        ajaxReturn('3订单错误，请刷新后重试！', 0, '订单错误，请刷新后重试！');
    }

    /**
     * 获取广告组名称
     * Enter description here ...
     */
    function ajaxgetadgroupinfo(){
        $where['adgroupid'] = trim($this->input->post('adgroupid'));

        if(empty($where['adgroupid']))
        {
            ajaxReturn('请填写广告组ID！', 0, '请填写正确的广告组ID！');
        }
        $this->load->model('AdGroupModel');
        $adgroupname = $this->AdGroupModel->getOne($where, 'adgroupname');
        if($adgroupname)
        {
            ajaxReturn('设置成功！', 1, $adgroupname);
        }
        ajaxReturn('请填写广告组ID！', 0, '请填写正确的广告组ID！');
    }
    
    function getadcampaignList(){
     	$search = $this->input->post("search");
     	$platform = $this->input->post("platform")?$this->input->post("platform"):'3';
     	$ostype = $this->input->post("ostype") == '3'?'0':$this->input->post("ostype");
     	$where['campaignid']=$search;
     	$this->load->model("AdCampaignModel");
     	$adcampaignlist = $this->AdCampaignModel->getRow($where,"campaignid,campaignname,ostypeid,userid","campaignid desc");//"campaignid,campaignname"
     	if($adcampaignlist){
     		if($ostype != 0 && $adcampaignlist['ostypeid']!=$ostype){
     			ajaxReturn('该活动与平台系统类型不符', 0, "");
     		}
     		$this->load->model("UserMemberModel");
     		$searchplatform = $this->UserMemberModel->getRow(array('userid'=>$adcampaignlist['userid']));
     		if($platform == 1 && (!empty($searchplatform['usergroupid']) && '1' != $searchplatform['usergroupid'] )){
     			ajaxReturn('该活动属于DSP平台，不是平台，与设定归属不符', 0, "");
     		}
     		elseif($platform == 2 && (empty($searchplatform['usergroupid']) || '2' != $searchplatform['usergroupid'])){
     			ajaxReturn('该活动属于平台，不是DSP平台，与设定归属不符', 0, "");
     		}
     		ajaxReturn('', 1, $adcampaignlist);
     	}else{
     		ajaxReturn("该活动不存在", 0, "");
     	}
    }
    
    
	function prDates() { 
		$starttime = $this->input->post("starttime");
		$endtime = $this->input->post("endtime");
	    $dt_start = strtotime($starttime. ' ' . '00:00:00'); 
	    $dt_end   = strtotime($endtime. ' ' . '00:00:00'); 
	    if($dt_start > $dt_end){
	    	  ajaxReturn("开始日期不能小于结束日期", 0, "");
	    }
	    $data=array('starttime'=>$dt_start,'endtime'=>$dt_end);
	    ajaxReturn("", 1, $data);
	}
}
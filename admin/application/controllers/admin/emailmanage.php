<?php
class EmailManage extends MY_A_Controller {
    public function __construct() {
      parent::__construct();  
    }  
    
    /**
     * 发送电子邮件的编辑页面
     */
    public function batchSendEmail() {
            $this->load->library('email');
           $this->assign('sender',$this->email->smtp_user);
           $this->display('admin/emaileditor.html');
    }
    
    /**
     * 发送电子邮件的事件处理调用
     */
    public function doSendmailAction() {
        $subject = $this->input->post('subject');
        $content = $this->input->post('email_content');
        $to = $this->input->post('to');
        
        $this->load->library('upload');
        $files = $_FILES['attachfiles'];
        //var_dump($files);
        
        if(empty($subject)) {
            $this->respToClient('邮件标题为空');
        }
        
        if(empty($content)) {
            $this->respToClient('邮件内容为空');
        }
        
        if(empty($to)) {
            $this->respToClient('至少要有一个收件人');
        }
        
        $data['upload_data']=$this->upload->data();
        $this->load->library('email');
        $to = explode (';', $to);
        $this->email->to($to);
        $this->email->from('accoftest@163.com');
        $this->email->subject($subject);
        $this->email->message($content);
        
        foreach ($_FILES['attachfiles']['tmp_name'] as $k=>$tmpfname) {
            
            $uploaddir = '/var/www/uploads/';
            $uploadfile = $uploaddir . $_FILES['attachfiles']['name'][$k];
           // echo $uploadfile;
            move_uploaded_file($_FILES['attachfiles']['tmp_name'][$k], $uploadfile);
            
            if($_FILES['attachfiles']['size'][$k]>0 && $_FILES['attachfiles']['error'][$k]==0) {
                $this->email->attach($uploadfile);
            }
        }
        
        $ret = $this->email->send();
        //echo $this->email->print_debugger();
        $this->email->clear();
        if($ret) {
            $this->respToClient('发送成功');
        } else {
            $this->respToClient('发送失败');
        }
        
    }
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Casemanage extends MY_A_Controller
{
    function __construct(){
        parent::__construct();
        $this->load->model('UserMemberModel');
        $this->load->model('CaseManageModel');
        $configlist = $this->SystemConfigModel->getAllConfig();
        $this->assign('configlist', $configlist);
    }

    /**
     * 内投订单列表页
     * Enter description here ...
     */
    function selfcase(){
        $adstatus = (int)$this->input->get('ad_status');
         $key = trim($this->input->get('key'));
        $sortfield = trim($this->input->get('sortfield'));
        $orderData['id'] = trim($this->input->get('id')) ? trim($this->input->get('id')) : 'desc';
        $orderData['starttime'] = trim($this->input->get('starttime')) ? trim($this->input->get('starttime')) : 'desc';
        $orderData['endtime'] = trim($this->input->get('endtime')) ? trim($this->input->get('endtime')) : 'desc';
        $orderData['campaignid'] = trim($this->input->get('campaignid')) ? trim($this->input->get('campaignid')) : 'desc';
        $orderData['adgroupname'] = trim($this->input->get('adgroupname')) ? trim($this->input->get('adgroupname')) : 'desc';
        $orderData['adgroupid'] = trim($this->input->get('adgroupid')) ? trim($this->input->get('adgroupid')) : 'desc';
        $orderData['channelname'] = trim($this->input->get('channelname')) ? trim($this->input->get('channelname')) : 'desc';
        $orderData['companyname'] = trim($this->input->get('companyname')) ? trim($this->input->get('companyname')) : 'desc';
        $orderData['contact'] = trim($this->input->get('contact')) ? trim($this->input->get('contact')) : 'desc';
        $orderData['adstatus'] = trim($this->input->get('adstatus')) ? trim($this->input->get('adstatus')) : 'desc';
        $orderData['price'] = trim($this->input->get('price')) ? trim($this->input->get('price')) : 'desc';
        $orderData['daylevel'] = trim($this->input->get('daylevel')) ? trim($this->input->get('daylevel')) : 'desc';
        $defaultsort = '';
        foreach ($orderData as $k => $v)
        {
            if($sortfield == $k)
            {
                $order[]= $k . ' ' . $v;
                if($v == 'desc')
                {
                    $defaultsort .= '&'.$k.'='.'asc';
                    $orderData[$k] = 'asc';
                }
                else
                {
                    $orderData[$k] = 'desc';
                    $defaultsort .= '&'.$k.'='.'desc';

                }
            }
            else
            {
                $defaultsort .= '&'.$k.'='.$v;
            }
        }
        $defaultsort .= "&ad_status=".$adstatus;
        $where = array('casetype' => 1);
        if(!empty($key))
        {
            $where['adgroupname'] = array('LIKE', '%'.$key.'%');
            $defaultsort .= "&key=".$key;
        }
        switch ($adstatus)
        {
            case 0 :
                $where['adstatus'] = 0;
                $where['endtime'] = array('GT',time()-3599);
                break;
            case 1 :
                $where['adstatus'] = 1;
                break;
            case 2 :
                $where['adstatus'] = 0;
                $where['starttime'] = array('ELT',time());
                $where['endtime'] = array('EGT',time()-3599);
                break;
            case 3 :
                $where['adstatus'] = 0;
                $where['starttime'] = array('GT',time());
                break;
            case 4 :
                break;
            case 5 :
                $where['adstatus'] = 0;
                $where['endtime'] = array('LT',time()-3599);
                break;
        }
        $this->assign('adstatus', $adstatus);
        $this->assign('key', $key);
        $p=trim($this->input->get('per_page'));
        if(!$p){
            $p=1;
        }
        $config['per_page'] = 50;
        $count = $this->CaseManageModel->getCount($where);
        $this->load->library('page');
        $current_page =  $p==0 ? 1 : $p;
        $page=$this->page->page_show('/admin/casemanage/selfcase?adstatus='.$adstatus.'&key='.$key.$defaultsort,$count,$config['per_page']);
        $this->pagination->create_links();
        $this->smarty->assign('page', $page);
        $this->smarty->assign('per_page', $config['per_page']);
        $this->smarty->assign('count', $count);
        $this->smarty->assign('defaultsort', $defaultsort);
        $this->smarty->assign('sortfield', $sortfield);
        $this->smarty->assign('orderData', $orderData);
         if(empty($order))
        {
            $orderby = ' createtime desc,channelname desc, campaignid desc ';
        }
        else
        {
            $orderby = implode(',', $order);
        }
        $list = $this->CaseManageModel->getList($where, '*',$orderby, '', $p, $config['per_page']);
        foreach($list as $k=>$v)
        {
            $list[$k]['adstatustext'] = $this->getAdStatus($v);
        }
        //echo $this->CaseManageModel->getLastSql();
        $this->assign('list', $list);
        $this->display('admin/casemanage/selfcase.html');
    }



    /**
     * 内投订单列表页
     * Enter description here ...
     */
    function outcase(){
        $adstatus = (int)$this->input->get('ad_status');
        $key = trim($this->input->get('key'));
        $sortfield = trim($this->input->get('sortfield'));
        $orderData['id'] = trim($this->input->get('id')) ? trim($this->input->get('id')) : 'desc';
        $orderData['starttime'] = trim($this->input->get('starttime')) ? trim($this->input->get('starttime')) : 'desc';
        $orderData['endtime'] = trim($this->input->get('endtime')) ? trim($this->input->get('endtime')) : 'desc';
        $orderData['campaignid'] = trim($this->input->get('campaignid')) ? trim($this->input->get('campaignid')) : 'desc';
        $orderData['adgroupname'] = trim($this->input->get('adgroupname')) ? trim($this->input->get('adgroupname')) : 'desc';
        $orderData['channelname'] = trim($this->input->get('channelname')) ? trim($this->input->get('channelname')) : 'desc';
        $orderData['companyname'] = trim($this->input->get('companyname')) ? trim($this->input->get('companyname')) : 'desc';
        $orderData['contact'] = trim($this->input->get('contact')) ? trim($this->input->get('contact')) : 'desc';
        $orderData['adstatus'] = trim($this->input->get('adstatus')) ? trim($this->input->get('adstatus')) : 'desc';
         $orderData['price'] = trim($this->input->get('price')) ? trim($this->input->get('price')) : 'desc';
        $orderData['daylevel'] = trim($this->input->get('daylevel')) ? trim($this->input->get('daylevel')) : 'desc';

        $defaultsort = '';
        foreach ($orderData as $k => $v)
        {
            if($sortfield == $k)
            {
                $order[]= $k . ' ' . $v;
                if($v == 'desc')
                {
                    $defaultsort .= '&'.$k.'='.'asc';
                    $orderData[$k] = 'asc';
                }
                else
                {
                    $orderData[$k] = 'desc';
                    $defaultsort .= '&'.$k.'='.'desc';

                }
            }
            else
            {
                $defaultsort .= '&'.$k.'='.$v;
            }
        }
        $defaultsort .= "&ad_status=".$adstatus;
 
        $where = array('casetype' => 2);
        if(!empty($key))
        {
            $where['adgroupname'] = array('LIKE', '%'.$key.'%');
            $defaultsort .= "&key=".$key;
        }
        switch ($adstatus)
        {
            case 0 :
                $where['adstatus'] = 0;
                $where['endtime'] = array('GT',time()-3599);
                break;
            case 1 :
                $where['adstatus'] = 1;
                break;
            case 2 :
                $where['adstatus'] = 0;
                $where['starttime'] = array('ELT',time());
                $where['endtime'] = array('EGT',time()-3599);
                break;
            case 3 :
                $where['adstatus'] = 0;
                $where['starttime'] = array('GT',time());
                break;
            case 4 :
                break;
            case 5 :
                $where['adstatus'] = 0;
                $where['endtime'] = array('LT',time()-3599);
                break;
        }
        $this->assign('adstatus', $adstatus);
        $this->assign('key', $key);
        $p=trim($this->input->get('per_page'));
        if(!$p){
            $p=1;
        }
        $config['per_page'] = 50;
        $count = $this->CaseManageModel->getCount($where);
        $this->load->library('page');
        $current_page =  $p==0 ? 1 : $p;
        $page=$this->page->page_show('/admin/casemanage/outcase?ad_status='.$adstatus.'&key='.$key.$defaultsort,$count,$config['per_page']);
        $this->pagination->create_links();
        $this->smarty->assign('page', $page);
        $this->smarty->assign('per_page', $config['per_page']);
        $this->smarty->assign('count', $count); 
        $this->smarty->assign('defaultsort', $defaultsort);
        $this->smarty->assign('sortfield', $sortfield);
        $this->smarty->assign('orderData', $orderData);
        if(empty($order))
        {
            $orderby = ' createtime desc,channelname desc, campaignid desc ';
        }
        else
        {
            $orderby = implode(',', $order);
        }
        $list = $this->CaseManageModel->getList($where, '*', $orderby, '', $p, $config['per_page']);
//        echo $this->CaseManageModel->getLastSql();
        foreach($list as $k=>$v)
        {
            $list[$k]['adstatustext'] = $this->getAdStatus($v);
        }
        //echo $this->CaseManageModel->getLastSql();
        $this->assign('list', $list);
        $this->display('admin/casemanage/outcase.html');
    }
    /**
     * 判断订单状态
     */
    private function getAdStatus($data)
    {
        if($data['adstatus'] == 1)
        {
            return '已暂停';
        }
        if($data['starttime'] <= time() && $data['endtime'] >= time()-3599 )
        {
            return '投放中';
        }
        if($data['starttime'] > time())
        {
            return '待投放';
        }
        if($data['endtime']< time()-3599)
        {
            return '投放结束';
        }
    }

    /**
     * 新增内投订单
     * Enter description here ...
     */
    function addselfcase(){
        $this->assign('mark', '新增');
        $adstatus = $this->input->get('adstatus');
        $key = $this->input->get('key');
        $this->assign('adstatus', $adstatus);
        $this->assign('key', $key);
        $this->display('admin/casemanage/addselfcase.html');
    }

    /**
     * 新增内投订单
     * Enter description here ...
     */
    function addoutcase(){
        $this->assign('mark', '新增');
        $adstatus = $this->input->get('adstatus');
        $key = $this->input->get('key');
        $this->assign('adstatus', $adstatus);
        $this->assign('key', $key);
        $this->display('admin/casemanage/addoutcase.html');
    }

    /**
     * 修改内投订单
     * Enter description here ...
     */
    function editselfcase(){
        $id = $this->input->get('id');
        $casecopy = $this->input->get('casecopy');
        $adstatus = $this->input->get('adstatus');
        $key = $this->input->get('key');
        $data = $this->CaseManageModel->getRow(array('id'=>$id));
        if(empty($data) || !isset($data['id']))
        {
            return ;
        }
        $starttime = $data['starttime'];
        $endtime = $data['endtime'];
        $data['starttime'] = date('Y-m-d', $starttime);
        $data['endtime'] = date('Y-m-d', $endtime);
        $data['starthour'] = date('H', $starttime);
        $data['endhour'] = date('H', $endtime);
        $data['price'] = formatmoney($data['price'],'get',2,'.');

        $this->assign('mark', '修改');
        $this->assign('data', $data);
        $this->assign('casecopy', $casecopy);
        $this->assign('adstatus', $adstatus);
        $this->assign('key', $key);
        $this->display('admin/casemanage/addselfcase.html');
    }

    /**
     * 修改外放订单
     * Enter description here ...
     */
    function editoutcase(){
        $id = $this->input->get('id');
        $casecopy = $this->input->get('casecopy');
        $adstatus = $this->input->get('adstatus');
        $key = $this->input->get('key');
        $data = $this->CaseManageModel->getRow(array('id'=>$id));
        if(empty($data) || !isset($data['id']))
        {
            return ;
        }
        $starttime = $data['starttime'];
        $endtime = $data['endtime'];
        $data['starttime'] = date('Y-m-d', $starttime);
        $data['endtime'] = date('Y-m-d', $endtime);
        $data['starthour'] = date('H', $starttime);
        $data['endhour'] = date('H', $endtime);
        $data['price'] = formatmoney($data['price'],'get',2,'.');

        $this->assign('mark', '修改');
        $this->assign('data', $data);
        $this->assign('casecopy', $casecopy);
        $this->assign('adstatus', $adstatus);
        $this->assign('key', $key);
        $this->display('admin/casemanage/addoutcase.html');
    }

    /**
     * 保存case
     * Enter description here ...
     */
    function ajaxcasesave(){
        if($this->input->post('savetype') == 'add')
        {
            $this->caseadd();
        }
        else if($this->input->post('savetype') == 'edit' && $this->input->post('id') > 0)
        {
            $this->caseedit();
        }
    }

    /**
     * 添加case
     * Enter description here ...
     */
    private function caseadd()
    {
        $data['starttime'] = trim($this->input->post('starttime'));
        $data['endtime'] = trim($this->input->post('endtime'));
        $data['campaignid'] = trim($this->input->post('campaignid'));
        $data['adgroupid'] = trim($this->input->post('adgroupid'));
        $data['adgroupname'] = trim($this->input->post('adgroupname'));
        $data['channelname'] = trim($this->input->post('channelname'));
        $data['companyname'] = trim($this->input->post('companyname'));
        $data['price'] = trim($this->input->post('price'));
        $data['daylevel'] = trim($this->input->post('daylevel'));
        $data['contact'] = trim($this->input->post('contact'));
        $data['description'] = trim($this->input->post('description'));
        $data['starthour'] = trim($this->input->post('starthour'));
        $data['endhour'] = trim($this->input->post('endhour'));
        $data['casetype'] = trim($this->input->post('casetype'));

        $status = $this->CaseManageModel->doAdd($data);
        if($status['status'])
        {
            ajaxReturn('添加成功！', 1, '添加成功！');
        }
        ajaxReturn($status['info'], $status['status'], $status['data']);
    }

    /**
     * 编辑case
     * Enter description here ...
     */
    private function caseedit()
    {
        $where['id'] = trim($this->input->post('id'));
        $data['starttime'] = trim($this->input->post('starttime'));
        $data['endtime'] = trim($this->input->post('endtime'));
        $data['campaignid'] = trim($this->input->post('campaignid'));
        $data['adgroupid'] = trim($this->input->post('adgroupid'));
        $data['adgroupname'] = trim($this->input->post('adgroupname'));
        $data['channelname'] = trim($this->input->post('channelname'));
        $data['companyname'] = trim($this->input->post('companyname'));
        $data['price'] = trim($this->input->post('price'));
        $data['daylevel'] = trim($this->input->post('daylevel'));
        $data['contact'] = trim($this->input->post('contact'));
        $data['description'] = trim($this->input->post('description'));
        $data['starthour'] = trim($this->input->post('starthour'));
        $data['endhour'] = trim($this->input->post('endhour'));
        $data['casetype'] = trim($this->input->post('casetype'));
        $status = $this->CaseManageModel->doEdit($where, $data);
        if($status['status'])
        {
            ajaxReturn('修改成功！', 1, $status['data']);
        }
        ajaxReturn($status['info'], $status['status'], $status['data']);
    }

    /**
     * 暂停case
     * Enter description here ...
     */
    function ajaxeditstatus(){
        $where['id'] = trim($this->input->post('id'));
        $data['adstatus'] = trim($this->input->post('adstatus'));
        if(empty($where['id']))
        {
            ajaxReturn('1订单错误，请刷新后重试！', 0, '订单错误，请刷新后重试！');
        }
        if($data['adstatus'] != 1 && $data['adstatus'] != 0 )
        {
            ajaxReturn('2订单错误，请刷新后重试！', 0, '订单错误，请刷新后重试！');
        }
        $status = $this->CaseManageModel->save($where, $data);
        if($status)
        {
            ajaxReturn('设置成功！', 1, '设置成功！');
        }
        ajaxReturn('3订单错误，请刷新后重试！', 0, '订单错误，请刷新后重试！');
    }

    /**
     * 复制case
     * Enter description here ...
     */
    function ajaxcopycase(){
        $where['id'] = trim($this->input->post('id'));
        if(empty($where['id']))
        {
            ajaxReturn('1订单错误，请刷新后重试！', 0, '订单错误，请刷新后重试！');
        }
        $data = $this->CaseManageModel->getRow($where);
        $admin = $this->session->userdata("admin");
        $data['adminid'] = $admin['admin_id'];
        $data['createtime'] = time();
        unset($data['id']);
        $status = $this->CaseManageModel->add($data);
        if($status)
        {
            ajaxReturn('克隆成功！', 1, '克隆成功！');
        }
        ajaxReturn('3订单错误，请刷新后重试！', 0, '订单错误，请刷新后重试！');
    }

    /**
     * 删除case
     * Enter description here ...
     */
    function ajaxdel(){
        $where['id'] = trim($this->input->post('id'));
        if(empty($where['id']))
        {
            ajaxReturn('1订单错误，请刷新后重试！', 0, '订单错误，请刷新后重试！');
        }
        $caseinfo = $this->CaseManageModel->getRow($where);
        if(empty($caseinfo))
        {
            ajaxReturn('2订单错误，请刷新后重试！', 0, '订单错误，请刷新后重试！');
        }
        $this->load->model('CaseManageLogModel');
        $admin = $this->session->userdata("admin");
        $this->CaseManageLogModel->add(array('data'=>json_encode($caseinfo), 'adminid'=>$admin['admin_id'], 'createtime'=>time()));
        $status = $this->CaseManageModel->delete($where);
        if($status)
        {
            ajaxReturn('成功删除！', 1, '成功删除！');
        }
        ajaxReturn('3订单错误，请刷新后重试！', 0, '订单错误，请刷新后重试！');
    }

    /**
     * 获取广告组名称
     * Enter description here ...
     */
    function ajaxgetadgroupinfo(){
        $where['adgroupid'] = trim($this->input->post('adgroupid'));

        if(empty($where['adgroupid']))
        {
            ajaxReturn('请填写广告组ID！', 0, '请填写正确的广告组ID！');
        }
        $this->load->model('AdGroupModel');
        $adgroupname = $this->AdGroupModel->getOne($where, 'adgroupname');
        if($adgroupname)
        {
            ajaxReturn('设置成功！', 1, $adgroupname);
        }
        ajaxReturn('请填写广告组ID！', 0, '请填写正确的广告组ID！');
    }
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MY_A_Controller
{
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->display('admin/admin/admin.html');
	}

	function main()
	{
		$this->smarty->assign('web',$this->get_web_info());
		$this->smarty->assign('admin',$this->session->userdata('admin'));
		$this->load->display('admin/admin/main.html');
	}

	private function get_web_info()
	{
		return array(
			'SERVER_NAME'		=> $this->input->server('SERVER_NAME'),	
			'SERVER_ADDR'		=> $this->input->server('SERVER_ADDR'),
			'REMOTE_ADDR'		=> $this->input->server('REMOTE_ADDR'),
			'CHARSET'			=> $this->config->item('charset'),
		);
	}

	private function get_server_info()
	{
		return array(
			'SERVER_SOFTWARE'	=> $this->input->server('SERVER_SOFTWARE'),
			'PHP_OS'			=> PHP_OS,
			'PHP_VERSION'		=> PHP_VERSION,
			'MYSQL_VERSION'		=> $this->db->version(),
			'GD_VERSION'		=> $this->get_gd_version(),
			'FILE_UPLOAD'		=> $this->get_file_upload(),
		);
	}
	private function get_gd_version()
	{
		if(function_exists('gd_info'))
		{
			$gd_info	= gd_info();
			$gd_version = $gd_info['GD Version'];
		}else{
			$gd_version = '未知';
		}
		return $gd_version;
	}

	private function get_file_upload()
	{
		if(@ini_get('file_uploads')){
			$umfs = ini_get('upload_max_filesize');
			$pms = ini_get('post_max_size');
   			return '允许 | 文件:'.$umfs.' | 表单：'.$pms;
		}else{
			return '<span class="red_font">禁止</span>';
		}
	}

	function import(){
		$fun1	= get_declared_classes();
		$handle = opendir('./application/controllers/admin/');
		if ($handle) {
			while (false !== ($file = readdir($handle))) {
				if ($file != "." && $file != ".." && $file != '.svn' && $file !='login.php') {
					require_once './application/controllers/admin/'.$file;
				}
			}
			closedir($handle);
		}
		$fun2	= get_declared_classes();
		$fun	= array_diff($fun2,$fun1);
		$data	= array();$i = 0;
		foreach($fun as $val){
			$class	= null;
			$class	= new ReflectionClass($val);
			$model	= array();
			$model	= $class->getMethods();
			$data[$val]['name']	= $val;
			
			foreach ($model as $k=>$v) {
				$m = new ReflectionMethod($val, $v->name);
				if(!($m->isPrivate() || $m->isProtected() || $m->name == "__construct" || $m->name == "get_instance") && !in_array($m->name, array('top','center','left','bottom','pageMsgBack','is_null','is_unique','get_unique_category') )){
					$data[$val]['child'][]	= $v->name;
					$i++;
				}
			}
		}
		echo $i.'<hr>';
		print_r($data);
		echo "Import End";
	}
}
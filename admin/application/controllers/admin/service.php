<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Service extends MY_A_Controller
{
    private $urlmatch='/^(https?:\/\/).?([\w\d-]+\.)+[\w-]+(\/[\d\w-.\/\!\+?%&=]*)?$/Ui';
    function __construct()
    {
        parent::__construct();
        $this->load->model("UserMemberModel");
        $configlist = $this->SystemConfigModel->getAllConfig();
        $this->assign('configlist', $configlist);
    }

    function userList(){
        $where = array();
        $where['userstatus'] = 0;
        $userstatus = 0;
        $userlist = $this->UserMemberModel->getList($where, '*', 'userid desc ');
        $p=trim($this->input->get('per_page'));
        if(!$p){
            $p=1;
        }
        $config ['per_page'] = 20;
        $count = $this->UserMemberModel->getCount ();
        $userlist = $this->UserMemberModel->getList($where, '*', 'userid desc', "", $p, $config ['per_page']);
        
        $this->load->library ( 'page' );
        $current_page = $p == 0 ? 1 : $p;
        $var = '&userstatus=' . $userstatus;
        $page = $this->page->page_show ( 'UserMemberModel?' . $var, $count, $config ['per_page'] );
        $this->smarty->assign('page', $page);
        $this->smarty->assign('per_page', $config['per_page']);
        $this->smarty->assign('count', $count);
        $this->assign('userlist', $userlist);
        $this->display('admin/users/userlist.html');
    }

    function setuseraudit($id){
        $info = $this->UserMemberModel->getRow(array("userid"=>(int)$id), 'userstatus');
        if($info['userstatus'] == 0){
            $status = $this->UserMemberModel->edit(array("userid"=>(int)$id), array("userstatus"=>1));
            if($status){
                ajaxReturn('账号激活成功！', 1);
            }
        }else{
            ajaxReturn('该状态不允许再激活!', 0);
        }
        ajaxReturn('账号激活失败！', 0);
    }

    /**
     *
     * 大客户管理
     */
    function bigCustomer(){
        $search = $this->input->get_post('search');
        $this->assign('search', $search);
        $p=trim($this->input->get('per_page'));
        if(!$p){
            $p=1;
        }
        $where = array();
        if($search){
            $where['username'] = array (
                        'LIKE',
                        "%" . $search . "%" 
                );
        }
        $usertype = $this->input->get('usertype');
        $this->assign('usertype', $usertype);         
        if(!empty($usertype)){
                $where['usertype'] = array (
                        'LIKE',
                        "%" . $usertype . "%" 
                );
        }
        $config['per_page'] = 20;
        $this->load->model("AppInfoModel");
        $this->UserMemberModel->setStrWhere($where);
        $count = $this->UserMemberModel->getCount();
        $users = $this->UserMemberModel->getList($where,"*","userid DESC", "", $p, $config ['per_page']);
        $this->load->library('page');
        $current_page =  $p==0 ? 1 : $p;
        $page=$this->page->page_show(base_url().'/admin/service/bigcustomer?',$count,$config['per_page']);
        $this->assign('users', $users);
        $this->smarty->assign('page', $page);
        $this->smarty->assign('per_page', $config['per_page']);
        $this->smarty->assign('count', $count);
        $this->display('admin/service/bigcustomer.html');
    }
/**
 * 解锁 锁定用户
 */
    function setuserstatus(){
        $userId = $this->input->get('userid');
        $userstatus = $this->input->get('userstatus');
        $this->UserMemberModel->edit( array('userid' => $userId), array('userstatus' => $userstatus));
        redirect('/admin/service/bigCustomer');
    }
    /**
     *
     * 忽略广告定向设备列表
     */
    function ignoredevice(){
        $this->load->model('IgnoreDeviceModel');
        $search = $this->input->post('search');
        if(empty($search))
        {
            $where = array();
        }
        else
        {
            $where = array('ignoreid'=>array('LIKE','%'.$search.'%'));
        }
    	$p=trim($this->input->get('per_page'));
        if(!$p){
            $p=1;
        }
        $config ['per_page'] = 20;
        $this->IgnoreDeviceModel->setStrWhere($where);
        $count = $this->IgnoreDeviceModel->getCount ();
        $list = $this->IgnoreDeviceModel->getList($where, '*', 'edittime desc', "", $p, $config ['per_page']);
        
        $this->load->library ( 'page' );
        $current_page = $p == 0 ? 1 : $p;
        $var = '&search=' . $search;
        $page = $this->page->page_show ( 'ignoredevice?' . $var, $count, $config ['per_page'] );
        $this->assign('list', $list);
        $this->assign('search', $search);
        $this->smarty->assign ( 'page', $page );
        $this->smarty->assign ( 'per_page', $config ['per_page'] );
        $this->smarty->assign ( 'count', $count );
        $this->display('admin/service/ignoredevice.html');
    }
    
    /**
     *
     * 添加忽略广告定向设备
     */
	function addignore(){
	    $this->load->model('IgnoreDeviceModel');
		$ignoreid = trim($this->input->post('ignoreid'));
	    $ignorename = trim($this->input->post('ignorename'));
	    if(empty($ignoreid))
	    {
	        ajaxReturn('请填写MAC或IDFA!', 0);
	    }
	    if(empty($ignorename))
	    {
	        ajaxReturn('请填写公司名称!', 0);
	    }
	    $id = $this->IgnoreDeviceModel->add(array('ignoreid'=>strtoupper($ignoreid), 'ignorename'=>$ignorename, 'createtime'=>time(), 'edittime'=>time()));
	    if($id>0)
	    {
	        ajaxReturn('添加成功!', 1);
	    }
	    else
	    {
	        ajaxReturn('添加失败!', 0);
	    }
	}
	
    /**
     *
     * 刷新时间
     */
    function doEditIgnore(){
        $this->load->model('IgnoreDeviceModel');
        $id = trim($this->input->post('id'));
        if(empty($id))
        {
            ajaxReturn('刷新失败!', 0);
        }
        $num = $this->IgnoreDeviceModel->edit(array('id'=>$id), array('edittime'=>time()));
        if($num>0)
        {
            ajaxReturn('刷新成功!', 1);
        }
        else
        {
            ajaxReturn('刷新失败!', 0);
        }
    }

    /**
     *
     * 刷新时间
     */
    function doDelIgnore(){
        $this->load->model('IgnoreDeviceModel');
        $id = trim($this->input->post('id'));
        if(empty($id))
        {
            ajaxReturn('删除失败!', 0);
        }
        $num = $this->IgnoreDeviceModel->delete(array('id'=>$id));
        if($num>0)
        {
            ajaxReturn('删除成功!', 1);
        }
        else
        {
            ajaxReturn('删除失败!', 0);
        }
    }
    function examine(){
        $reason = $this->SystemConfigModel->getSystemConfig('reason_for_rejection');
        $this->assign('reason', $reason);
        $appstatus = $this->SystemConfigModel->getSystemConfig('app_audit_status');
        $this->assign('appstatus', $appstatus);
        $status = (int)$this->input->get('status');
        $this->assign('status', $status);
        $p=trim($this->input->get('per_page'));
        if(!$p){
            $p=1;
        }
        $where = '';
        $data = array();
        switch($status){
            case 0:
                $where .= " and ad_stuff.status=0";
                $data['status'] = $status;
                break;
            case 1:
                $where .= " and ad_stuff.status=1";
                $data['status'] = $status;
                break;
            case 2:
                $where .= " and ad_stuff.status=2";
                $data['status'] = $status;
                break;
            default:
                break;
        }
        $where .= " and ad_group.adform not in (9,10) ";

        $config['per_page'] = 20;
        $this->load->model('AdStuffModel');
        $this->AdStuffModel->setStrWhere($data);
        $count = $this->AdStuffModel->getCount();
        $stufflist = $this->AdStuffModel->getStuffList($where, $p,$config['per_page']);
        $arr = array();
        $tmp = array();
        /*foreach($stufflist as $val){
         $arr[$val['stufftype']] = isset($arr[$val['stufftype']]) ? $arr[$val['stufftype']] .','.$val['stuffid'] : $val['stuffid'];
         }*/
        $this->load->library('page');
        $current_page =  $p==0 ? 1 : $p;
        $page=$this->page->page_show('/admin/service/examine?status='.$status,$count,$config['per_page']);
        $this->pagination->create_links();
        $this->smarty->assign('page', $page);
        $this->smarty->assign('per_page', $config['per_page']);
        $this->smarty->assign('count', $count);
        $this->assign('stufflist', $stufflist);
        $effecttype = $this->SystemConfigModel->getSystemConfig('effect_type');
        $stuff_type = $this->SystemConfigModel->getSystemConfig('stuff_type');
        $this->assign('effecttype', $effecttype);
        $this->assign('stufftype', $stuff_type);
        $this->assign('AMOUNT_RATIO', AMOUNT_RATIO);
        $this->display('admin/service/examine.html');
    }

    function ajaxsetaudit(){
        $stuffid = $this->input->post('stuffid');
        $reason = trim($this->input->post('audit'), ',');
        $status = $this->input->post('status');
        $callbackurl = $this->input->post('callbackurl');
        $otherid = $this->input->post('otherid');
        $lowerlimit=trim($this->input->post('lowerlimit'));
        $upperlimit=trim($this->input->post('upperlimit'));
        $promotionratio=trim($this->input->post('promotionratio'));
        $isspeedup = trim($this->input->post('isspeedup'));
        $speedupscale = trim($this->input->post('speedupscale'));
        $adform=$this->input->post('adform');
        if($status == 2){
            if(!($status && $stuffid) || empty($reason)){
                ajaxReturn('参数错误', 0, 0);
            }
        }else{
            if(!($status && $stuffid)){
                ajaxReturn('参数错误', 0, 0);
            }
        }
        if(!empty($callbackurl))
        {
            if(!preg_match($this->urlmatch, $callbackurl))
            {
                ajaxReturn('请输入正确的URL地址。', 0, 0);
            }
        }
        
        if($adform == 30 && $status == 1){
	        if(empty($lowerlimit) || empty($upperlimit))
	        {
	            ajaxReturn('请填写DSP竞价区间比例', 0, 0);
	        }
	    	if(intval($lowerlimit)!=$lowerlimit || intval($upperlimit)!=$upperlimit)
	        {
	            ajaxReturn('DSP竞价区间比例, 请填写整数', 0, 0);
	        }
	        if(empty($promotionratio))
	        {
	            ajaxReturn('请填写DSP底价提升比例', 0, 0);
	        }
	   	 	if(intval($promotionratio)!=$promotionratio)
	        {
	            ajaxReturn('DSP底价提升比例, 请填写整数', 0, 0);
	        }
        }
        if($isspeedup == 1){
	    	if(intval($speedupscale)!=$speedupscale)
	        {
	            ajaxReturn('加速点击比例, 请填写整数', 0, 0);
	        }
	        $this->load->model('AdStuffModel');
	        $adstuffinfo = $this->AdStuffModel->getRow(array('stuffid'=>$stuffid));
	        $this->load->model('AdGroupModel');
	        $adgroupinfo = $this->AdGroupModel->getRow(array('adgroupid'=>$adstuffinfo['adgroupid']));
	        $data = array('campaignId'=>$adgroupinfo['otherid'],'scale'=>round($speedupscale/100, 2));
	        $url = "http://stats.cocounion.com/click/scale";
	        $result = curlSendJson(json_encode($data),$url);
	        if(empty($result) || $result !="OK")
	        {
	             ajaxReturn('传送加速点击比例失败', 0, 0);
	        }
	        else{
	        	 $speed_status = $this->AdStuffModel->edit(array('stuffid'=>$stuffid), array('speedupscale'=>$speedupscale));
	        	 if(!$speed_status && $speedupscale!=$adstuffinfo['speedupscale']){
	        	 	ajaxReturn('保存加速点击比例失败', 0, 0);
	        	 }
	        }
        }
        $this->load->model('AdStuffModel');
        $status = $this->AdStuffModel->edit(array('stuffid'=>$stuffid), array("status"=>$status, "callbackurl"=>$callbackurl, "otherid"=>$otherid,"lowerlimit"=>$lowerlimit, "upperlimit"=>$upperlimit, 'promotionratio'=>$promotionratio, 'reason'=>$reason, 'audittime'=>time()));
        if($status){
            $this->load->model('AdStuffMoregameModel');
            $this->AdStuffMoregameModel->updateisupdate2($stuffid,'ad_stuff');
            ajaxReturn('审核结果保存成功', 1, print_r($reason,true));
        }
        ajaxReturn('审核结果保存失败', 0, 0);
    }

    function ajaxdelreason(){
        $key = $this->input->post('key');
        if(empty($key)){
            ajaxReturn('参数错误！', 0, 0);
        }
        $num = $this->SystemConfigModel->delete(array('skey'=>$key, 'parentid' => 41));
        if($num>0)
        {
            ajaxReturn('删除成功！', 1, 0);
        }
        ajaxReturn('未知错误！', 0, 0);

    }

    function ajaxdelchannel(){
        $channelid = $this->input->post('channelid');
        $ostypeid = $this->input->post('ostypeid');
        if(empty($channelid) || empty($ostypeid)){
            ajaxReturn('参数错误！', 0, 0);
        }
        $this->load->model('AppChannelModel');
        $info = $this->AppChannelModel->del($channelid, $ostypeid);
        if($info['status']>0)
        {
            ajaxReturn('删除成功！', 1, 0);
        }
        ajaxReturn('未知错误！', 0, 0);
    }

    function ajaxgetchannelinfo(){
        $channelid = $this->input->get('channelid');
        $ostypeid = $this->input->get('ostypeid');
        if(empty($channelid) || empty($ostypeid)){
            ajaxReturn('参数错误！', 0, 0);
        }
        $this->load->model('AppChannelModel');
        $info = $this->AppChannelModel->getChannelInfo($channelid, $ostypeid);
        if($info['status']>0)
        {
            ajaxReturn($info['info'], 1, 0);
        }
        ajaxReturn('未知错误！', 0, 0);
    }

    function ajaxsavechannel(){
        $params['ostypeid'] = $this->input->post('ostypeid');
        $params['channelname'] = trim($this->input->post('channelname'));
        $params['channelid'] = $this->input->post('channelid');
        $params['inchlid'] = $this->input->post('inchlid');
        $params['channeltype'] = $this->input->post('channeltype');
        $params['subtype'] = $this->input->post('subtype');
        if(empty($params['ostypeid'])){
            ajaxReturn('参数错误！', 0, 0);
        }
        if(empty($params['channelname']))
        {
            ajaxReturn('渠道名称不能为空！', 0, 0);
        }
        if($params['subtype'] != 'edit' && $params['subtype'] != 'add')
        {
            ajaxReturn('参数错误！', 0, 0);
        }
        if($params['subtype'] == 'edit' && $params['channelid'] < 0)
        {
            ajaxReturn('参数错误！', 0, 0);
        }


        $this->load->model('AppChannelModel');

        $num = $this->AppChannelModel->save($params);

        if($num>0)
        {
            ajaxReturn($num, 1, 0);
        }
        elseif($num == -1)
        {
            ajaxReturn('渠道号或渠道名称不能重复！', 0, 0);
        }
        ajaxReturn('没有更新！', 0, 0);
    }

    function viewstuff(){
        $stuffid = $this->input->get('id');
        $this->load->model('AdStuffModel');
        $info = $this->AdStuffModel->getRow(array("stuffid"=>$stuffid));
        if(empty($info)){
            exit('广告创意不存在。');
        }
        $this->assign('info', $info);
        switch($info['stufftype']){
        	case 9:
            case 1:
                $this->viewBanner($info);
                break;
            case 2:
                $this->viewBannerText($info);
                break;
            case 3:
                $this->viewPopup($info);
                break;
            case 4:
                break;
            case 5:
                $this->viewMoregame($info);
                break;
            case 6:
                $this->viewTask($info);
                break;
            case 8:
            	$this->viewFeeds($info);
            	break;
            case 10:
            	$this->viewPopupTextImg($info);
            	break;
            case 11:
            	$this->viewPopMultiImg($info);
            	break;
            default:
                break;
        }
    }
    
    private function viewPopMultiImg($info){
    	$this->load->model('AdStuffTextModel');
    	$this->load->model('AdStuffImgModel');
        $text = $this->AdStuffTextModel->getRow(array('stuffid'=>$info['stuffid']));
        $this->assign('text', $text);
        $stuff = $this->AdStuffImgModel->getList(array('stuffid'=>$info['stuffid'],'status'=>1));
        $this->assign('stuff', $stuff);
        $this->display('admin/service/viewbanner.html');
    }
    
    private function viewFeeds($info){
    	$this->load->model('AdStuffImgModel');
        $this->load->model('AdStuffTextModel');
    	$stuff = $this->AdStuffImgModel->getList(array('stuffid'=>$info['stuffid'],'status'=>1));
    	$text = $this->AdStuffTextModel->getRow(array('stuffid'=>$info['stuffid']), 'adtitle, adcontent');
    	$this->assign('stuff', $stuff);
    	$this->assign('text', $text);
    	$this->display('admin/service/viewfeeds.html');
    }
    
    private function viewTask($info){
        $stuff = $this->CommonModel->table("ad_stuff_integral")->getList(array('stuffid'=>$info['stuffid']));
        $this->assign('stuff', $stuff);
        $this->display('admin/service/viewtask.html');
    }

    private function viewBanner($info){
        $this->load->model('AdStuffImgModel');
        $stuff = $this->AdStuffImgModel->getList(array('stuffid'=>$info['stuffid'],'status'=>1));
        $this->assign('stuff', $stuff);
        $this->display('admin/service/viewbanner.html');
    }

    private function viewBannerText($info){
        $this->load->model('AdStuffTextModel');
        $stuff = $this->AdStuffTextModel->getList(array('stuffid'=>$info['stuffid']));
        $this->assign('stuff', $stuff);
        $this->display('admin/service/viewbannertext.html');
    }

    private function viewPopup($info){
        $this->load->model('AdStuffImgModel');
        $stuff = $this->AdStuffImgModel->getList(array('stuffid'=>$info['stuffid']));
        $this->assign('stuff', $stuff);
        $this->display('admin/service/viewpopup.html');
    }

    private function viewPopupTextImg($info){
    	$this->load->model('AdStuffTextModel');
    	$this->load->model('AdStuffImgModel');
        $text = $this->AdStuffTextModel->getRow(array('stuffid'=>$info['stuffid']));
        $img = $this->AdStuffImgModel->getList(array('stuffid'=>$info['stuffid']));
        $this->assign('text', $text);
        $this->assign('img', $img);
        $this->display('admin/service/viewpopuptext.html');
    }
    
    private function viewMoregame($info){
        $this->load->model('AdStuffMoregameModel');
        $stuff = $this->AdStuffMoregameModel->getList(array('stuffid'=>$info['stuffid']));
        $this->assign('stuff', $stuff);
        $this->display('admin/service/viewmoregame.html');
    }

    function ajaxsetreason(){
        $data = array();
        $data['skey'] = (int)$this->input->post('id');
        $data['sval'] = strip_tags(trim($this->input->post('reason')));
        $data['parentid'] = 1;
        $keyname = strip_tags(trim($this->input->post('keyname')));
        if($keyname){
            $data['parentid'] = $this->SystemConfigModel->getOne(array("skey"=>$keyname),"id");
            if($data['parentid'] < 1){
                ajaxReturn('找不到该参数配置！', 0, 0);
            }
        }
        $data['addtime'] = time();
        if(!$data['skey'] || $data['sval'] == ''){
            ajaxReturn('参数错误！', 0, 0);
        }
        $isexists = $this->SystemConfigModel->getOne(array('skey'=>$data['skey'],'parentid'=>$data['parentid']),'id');
        if($isexists){
            ajaxReturn('该ID已存在', 0, 0);
        }
        $status = $this->SystemConfigModel->add($data);
        if($status){
            ajaxReturn('添加成功！', 1, 1);
        }
        ajaxReturn('添加失败！', 0, 0);
    }
    
    function paging(){
    	
    }
    
    function appaudit(){
        $reason = $this->SystemConfigModel->getSystemConfig('reason_for_apprejection');
        $this->assign('reason', $reason);
        $auditstatus = $this->input->get('status') ? (int)$this->input->get('status') : 1;
        $data = array('auditstatus' => $auditstatus);
        $data['search'] = $this->input->get('search');
        $data['ostypeid'] = $this->input->get('os')?$this->input->get('os'):'2';
        $this->assign('data', $data);
        $this->load->model('AppInfoModel');
        $applist = $this->AppInfoModel->getAppSearchList($data);
        $p=trim($this->input->get('per_page'));
        if(!$p){
            $p=1;
        }
        $config['per_page'] = 20;
        $count = count($applist);
        $offset = ($p-1)*$config['per_page'];
        $showlist = array();
        $showlist=array_slice($applist, $offset, $config['per_page']);
        $this->load->library('page');
        $current_page =  $p==0 ? 1 : $p;
        $var = '&status='.$auditstatus.'&search='.$data['search'].'&os='.$data['ostypeid'];
        $page=$this->page->page_show('/admin/service/appaudit?'.$var,$count,$config['per_page']);
        $this->smarty->assign('page', $page);
        $this->smarty->assign('per_page', $config['per_page']);
        $this->smarty->assign('count', $count);
        $this->assign('applist', $showlist);
        $this->display('admin/service/appaudit.html');
    }

    function membersaudit(){
        $this->load->model('TeamMembersModel');
        $this->load->model('AccountInfoModel');
        $auditstatus = $this->input->get('auditstatus') ? (int)$this->input->get('auditstatus') : 0;
        $data = array('auditstatus' => $auditstatus);// 0-待审核 1-审核通过 2-审核驳回
        $data['search'] = $this->input->get('search');
        $this->assign('data', $data);
        $accountlist = $this->AccountInfoModel->getAccountSearchList($data);
        $memberlist = $this->TeamMembersModel->getMemberSearchList($data);
        $datalist = array();
        if($auditstatus == 0)
        {
            foreach ($accountlist as $k => $v)
            {
                $datalist[$v['createtime']][] = $v;
            }
            foreach($memberlist as $k => $v)
            {
                $datalist[$v['createtime']][] = $v;
            }
        }
        else
        {
            foreach ($accountlist as $k => $v)
            {
                $datalist[$v['createtime']][] = $v;
            }
            foreach($memberlist as $k => $v)
            {
                $datalist[$v['createtime']][] = $v;
            }
        }
        krsort($datalist);

    	$p=trim($this->input->get('per_page'));
        if(!$p){
            $p=1;
        }
        $config['per_page'] = 20;
        $count = count($datalist);
        $offset = ($p-1)*$config['per_page'];
        $showlist = array();
        $showlist=array_slice($datalist, $offset, $config['per_page']);
        $this->load->library('page');
        $current_page =  $p==0 ? 1 : $p;
        $var = '&search='.$data['search'].'&auditstatus='.$auditstatus;
        $page=$this->page->page_show('/admin/service/membersaudit?'.$var,$count,$config['per_page']);
        $this->assign('showlist', $showlist);
        $this->assign('accountlist', $accountlist);
        $this->assign('memberlist', $memberlist);
        $this->smarty->assign('page', $page);
        $this->smarty->assign('per_page', $config['per_page']);
        $this->smarty->assign('count', $count);
        $this->display('admin/service/membersaudit.html');
    }

    function grouphistory(){
        $this->load->model('TeamMembersModel');
        $this->load->model('AccountInfoModel');
        $userid = $this->input->get('userid');
        $accountlist = $this->AccountInfoModel->getAccountHistory($userid);
        $memberlist = $this->TeamMembersModel->getMemberHistory($userid);
        $info = array();
        foreach ($accountlist as $v)
        {
            $info[$v['createtime']] = $v;
        }
        foreach ($memberlist as $v)
        {
            $info[$v['createtime']] = $v;
        }
        krsort($info);
        $this->assign('info', $info);
        $userinfo = $this->UserMemberModel->getRow(array("userid"=>$userid));
        $this->assign('info', $info);
        $this->assign('userinfo', $userinfo);
        //$this->assign('memberlist', $memberlist);
        $this->display('admin/service/grouphistory.html');
    }



    function groupinfo(){
        $this->load->model('TeamMembersModel');
        $this->load->model('AccountInfoModel');
        $id = $this->input->get('id') ? (int)$this->input->get('id') : 0;
        if(empty($id) || $id < 0)
        {
            return;
        }
        $accountinfo = $this->AccountInfoModel->getAccountInfo($id);
        if(empty($accountinfo['data']))
        {
            return;
        }
        $userinfo = $this->UserMemberModel->getRow(array("userid"=>(int)$accountinfo['data']['userid']));
        if(empty($userinfo))
        {
            return;
        }
        if($accountinfo['status'] == 0)
        {
            return ;
        }
        $this->assign('accountinfo', $accountinfo['data']);
        $this->assign('userinfo', $userinfo);
        $this->display('admin/service/groupinfo.html');
    }

    function memberinfo(){
        $this->load->model('TeamMembersModel');
        $this->load->model('AccountInfoModel');
        $id = $this->input->get('id') ? (int)$this->input->get('id') : 0;
        if(empty($id) || $id < 0)
        {
            return;
        }
        $memberinfo = $this->TeamMembersModel->getMemberInfo($id);
        if(empty($memberinfo['data']))
        {
            return;
        }
        $userinfo = $this->UserMemberModel->getRow(array("userid"=>(int)$memberinfo['data']['userid']));
        if(empty($userinfo))
        {
            return;
        }
        if($memberinfo['status'] == 0)
        {
            return ;
        }
        $this->assign('memberinfo', $memberinfo['data']);
        $this->assign('userinfo', $userinfo);
        $this->display('admin/service/memberinfo.html');
    }


    function memberview(){
        $this->load->model('TeamMembersModel');
        $this->load->model('AccountInfoModel');
        $id = $this->input->get('id') ? (int)$this->input->get('id') : 0;
        if(empty($id) || $id < 0)
        {
            return;
        }
        $memberinfo = $this->TeamMembersModel->getMemberInfo($id);
        if(empty($memberinfo['data']))
        {
            return;
        }
        $userinfo = $this->UserMemberModel->getRow(array("userid"=>(int)$memberinfo['data']['userid']));
        if(empty($userinfo))
        {
            return;
        }
        if($memberinfo['status'] == 0)
        {
            return ;
        }
        $this->assign('memberinfo', $memberinfo['data']);
        $this->assign('userinfo', $userinfo);
        $this->display('admin/service/memberview.html');
    }

    function groupview(){
        $this->load->model('TeamMembersModel');
        $this->load->model('AccountInfoModel');
        $id = $this->input->get('id') ? (int)$this->input->get('id') : 0;
        if(empty($id) || $id < 0)
        {
            return;
        }
        $info = $this->AccountInfoModel->getAccountInfo($id);
        if(empty($info['data']))
        {
            return;
        }
        $userinfo = $this->UserMemberModel->getRow(array("userid"=>(int)$info['data']['userid']));
        if(empty($userinfo))
        {
            return;
        }
        if($info['status'] == 0)
        {
            return ;
        }
        $this->assign('accountinfo', $info['data']);
        $this->assign('userinfo', $userinfo);
        $this->display('admin/service/groupview.html');
    }

    function auditinfo(){
        $id = $this->input->get('id');
        if($id < 0)
        {

        }
        $this->load->model("AppAuditInfoModel");
        $info = $this->AppAuditInfoModel->getRow(array('id'=>$id));
        if(empty($info))
        {

        }
        $data = json_decode($info['auditinfo'], true);
        $this->assign('data', $data);
        $this->display('admin/service/auditinfo.html');
    }

    /**
     * 渠道管理
     * Enter description here ...
     */
    function channelmanage(){
         $this->load->model('AppChannelModel');
        $reason = $this->SystemConfigModel->getSystemConfig('reason_for_apprejection');
        $this->assign('reason', $reason);
        $ostypeid = $this->input->get_post('ostypeid') ? (int)$this->input->get_post('ostypeid') : 1;
        $data = array('ostypeid' => $ostypeid);
        $data['search'] = trim($this->input->get('search'));
        //渠道类别搜索
        $channeltype = trim($this->input->get('schanneltype'));
        $channeltypearr = $this->AppChannelModel->getChannelType($channeltype,$ostypeid);
        $channeltypes = array();
        if(!empty($channeltypearr)){
                foreach ($channeltypearr as $key=>$value) {
                        $channeltypes[$key] = $value['channelid'];
                }
        }
        $this->assign('data', $data);
        $this->assign('schanneltype', $channeltype);
        $channellist = $this->AppChannelModel->getChannelSearchList($ostypeid, $data['search'], 0,$channeltypes);
        $parentchannellist = $this->AppChannelModel->getParentChannelList($ostypeid, 0);
        $this->assign('channellist', $channellist);
        $this->assign('parentchannellist', $parentchannellist);
         $this->display('admin/service/channelmanage.html');
    }

    function ajaxsetappaudit(){
        $appid = $this->input->post('appid');
        $reason = trim($this->input->post('audit'), ',');
        $otherreason = trim($this->input->post('otherreason'));
        $status = $this->input->post('status');
        $reasoncontent = '';
        if($status == 3){
            if(!($status && $appid) || (empty($reason) && empty($otherreason))){
                ajaxReturn('参数错误.', 0, 0);
            }
            else{
	            $reasontext = $this->SystemConfigModel->getSystemConfig('reason_for_apprejection');
		        $tmpreason = explode(',', $reason);
		        if(is_array($tmpreason))
		        {
		            foreach ($tmpreason as $v)
		            {
		                foreach ($reasontext as $kt=>$vt)
		                {
		                    if($v == $kt)
		                    {
		                        $reasoncontent .= $vt."<br>";
		                    }
		                }
		            }
		        }
		        else
		        {
		            foreach ($reasontext as $kt=>$vt)
		            {
		                if($tmpreason == $kt)
		                {
		                    $reasoncontent = $vt."<br>";
		                    break;
		                }
		            }
		        }
		        if(!empty($otherreason))
		        {
		            $reasoncontent .= $otherreason."<br>";
		        }
            }
        }else{
            if(!($status && $appid)){
                ajaxReturn('参数错误', 0, 0);
            }
            else{
            	$reasoncontent=$reason;
            }
        }
        $this->load->model('AppInfoModel');
        $data = array("auditstatus"=>$status, 'auditmemo'=>$reasoncontent, 'auditime'=>time());
        if($data['auditstatus'] == 2){
            $data['auditing'] = 1;
        }else{
            $data['auditing'] = 0;
        }
        $status = $this->AppInfoModel->edit(array('appid'=>$appid), $data);
        if($status){
            ajaxReturn('审核结果保存成功', 1, print_r($reason,true));
        }
        ajaxReturn('审核结果保存失败', 0, 0);
    }

    function ajaxsetaccountaudit(){
        $id = $this->input->post('id');
        $reason = trim($this->input->post('audit'), ',');
        $otherreason = trim($this->input->post('otherreason'));
        $status = $this->input->post('status');
        if($status == 2){
            if(!($status && $id) || (empty($reason) && empty($otherreason))){
                ajaxReturn('参数错误.', 0, 0);
            }
        }else{
            if(!($status && $id)){
                ajaxReturn('参数错误', 0, 0);
            }
        }
        $reasoncontent = "";
        if(!empty($reason))
        {
            $reasoncontent = $reason;
        }
        if(!empty($otherreason))
        {
            $reasoncontent .= $otherreason;
        }
        $data = array("auditstatus"=>$status, 'reason'=>$reasoncontent, 'audittime'=>time());
        $this->load->model('AccountInfoModel');
        $accountInfo = $this->AccountInfoModel->getAccountInfo($id);
        if($accountInfo['status'] != 1 || $accountInfo['data']['tag'] == 3 ||  $accountInfo['data']['tag'] >= 5 )
        {
            ajaxReturn('审核结果保存失败1', 0, $accountInfo);
        }
        if($accountInfo['data']['tag'] == 2 && $status == 1 && $accountInfo['data']['auditstatus'] == 1)
        {
            $where['status'] = 1;
            $where['accounttype'] = 1;
            $where['auditstatus'] = 1;
            $where['userid'] = $accountInfo['data']['userid'];
            $currentAccInfo = $this->AccountInfoModel->getRow($where);
            if(empty($currentAccInfo))
            {
                ajaxReturn('审核结果保存失败2', 0, '未知错误');
            }
            $num = $this->AccountInfoModel->edit(array('id'=>$currentAccInfo['id']), array('status'=>2));
            if(empty($num))
            {
                ajaxReturn('审核结果保存失败3', 0, '未知错误');
            }
        }
        $num = $this->AccountInfoModel->edit(array('id'=>$id), $data);
        if($num && $accountInfo['data']['tag'] == '1'){
            $this->UserMemberModel->edit(array('userid'=>$accountInfo['data']['userid']),array('accountstatus'=>$status));
            ajaxReturn('审核结果保存成功', 1, print_r($reason,true));
        }
        elseif($num && ($accountInfo['data']['tag'] == '2'|| $accountInfo['data']['tag']==4))
        {
            if($status == 1)
            {
                $this->UserMemberModel->edit(array('userid'=>$accountInfo['data']['userid'],),array('accounttype'=>$accountInfo['data']['accounttype']));
            }
            ajaxReturn('审核结果保存成功', 1, print_r($reason,true));
        }
        ajaxReturn('审核结果保存失败4', 0, 0);
    }

    function ajaxsetmemberaudit(){
        $id = $this->input->post('id');
        $reason = trim($this->input->post('audit'), ',');
        $otherreason = trim($this->input->post('otherreason'));
        $status = $this->input->post('status');
        if($status == 2){
            if(!($status && $id) || (empty($reason) && empty($otherreason))){
                ajaxReturn('参数错误.', 0, 0);
            }
        }else{
            if(!($status && $id)){
                ajaxReturn('参数错误', 0, 0);
            }
        }
        $reasoncontent = "";
        if(!empty($reason))
        {
            $reasoncontent = $reason;
        }
        if(!empty($otherreason))
        {
            $reasoncontent .= $otherreason;
        }
        $data = array("auditstatus"=>$status, 'reason'=>$reasoncontent, 'audittime'=>time());
        $this->load->model('TeamMembersModel');
        $status = $this->TeamMembersModel->edit(array('id'=>$id), $data);
        if($status){
            ajaxReturn('审核结果保存成功', 1, print_r($reason,true));
        }
        ajaxReturn('审核结果保存失败', 0, 0);
    }
    
    function ajaxsetauditmemo(){
    	$appid = $this->input->post('appid');
        $auditmemo = trim($this->input->post('auditmemo'))?trim($this->input->post('auditmemo')):"";
        if($auditmemo == "" || $appid == ""){
             ajaxReturn('备注内容为空', 0, "");
        }else{
            $this->load->model('AppInfoModel');
	        $data = array('auditmemo'=>$auditmemo);
	        $status = $this->AppInfoModel->edit(array('appid'=>$appid), $data);
	        if($status){
	            ajaxReturn('备注保存成功', 1, 0);
	        }
	        ajaxReturn('备注保存失败', 0, 0);
        }
    }
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Coc extends MY_A_Controller
{
	private $genderlist = array('1'=>'女', '2'=>'男');
    function __construct()
    {
        parent::__construct();
        $this->load->model("coc/CocUserModel");
    }
    
    function userlist(){
    	$p=trim($this->input->get('per_page'));
        if(!$p){
            $p=1;
        }
    	$pagesize = 20;
    	$where = "reserve1 is null or reserve1 != 3";
    	$this->CocUserModel->setStrWhere($where);
        $count = $this->CocUserModel->getCount();
        $users = $this->CocUserModel->getList($where,"*","", "", $p,$pagesize);
        foreach($users as $k=>$v){
        	$users[$k]['createtime'] = date('Y-m-d',$users[$k]['createtime']);
        	$users[$k]['gender'] = $this->genderlist[$users[$k]['gender']];
        }
        $this->load->library('page');
        $current_page =  $p==0 ? 1 : $p;
        $page=$this->page->page_show(base_url().'/admin/coc/userlist?',$count,$pagesize);
        $this->assign('users', $users);
        $this->smarty->assign('page', $page);
        $this->smarty->assign('per_page', $pagesize);
        $this->smarty->assign('count', $count);
    	$this->display('admin/coc/userlist.html');
    }
    
	function changeStatus(){
        $where['userid'] = $this->input->post('userid');
        $set['reserve1'] = $this->input->post('status');
        $this->load->model('coc/CocUserModel');
        if(empty($where['userid']))
        {
            ajaxReturn('修改失败!', 0);
        }
        $num = $this->CocUserModel->edit($where, $set);
        if($num)
        {
            ajaxReturn('修改成功!', 1);
        }
        ajaxReturn('修改失败!', 0);
    }
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Operation extends MY_A_Controller
{
    function __construct(){
        parent::__construct();
        $this->load->model('UserMemberModel');
        $this->load->model('AppInfoModel');
        $configlist = $this->SystemConfigModel->getAllConfig();
        $this->assign('configlist', $configlist);
    }


   	function moregametop(){
        $os = (int)$this->input->get('os');
        if(!$os){
            $os = 2;
        }
        $istop = (int)$this->input->get('istop');
        $confirm = !$istop ? '推送到TOP3' :'从TOP3释放';
        $this->assign('confirm', $confirm);
        $this->assign('os', $os);
        $this->assign('istop', $istop);
        $this->load->model('AdStuffMoregameModel');
        $search = $this->input->post('search');
        $list = $this->AdStuffMoregameModel->getMoregameTop($os,$istop,$search);
        $p=trim($this->input->get('per_page'));
        if(!$p){
            $p=1;
        }
        $config['per_page'] = 20;
        $count = count($list);
        $offset = ($p-1)*$config['per_page'];
        $showlist = array();
        $showlist=array_slice($list, $offset, $config['per_page']);
        $this->load->library('page');
        $current_page =  $p==0 ? 1 : $p;
        $var = '&search='.$search.'&confirm='.$confirm.'&os='.$os.'&istop='.$istop;
        $page=$this->page->page_show('/admin/operation/moregametop?'.$var,$count,$config['per_page']);
        $this->assign('showlist', $showlist);
        $this->smarty->assign('page', $page);
        $this->smarty->assign('per_page', $config['per_page']);
        $this->smarty->assign('count', $count);
        $this->assign('search',$search);
        $this->display('admin/operation/moregametop.html');
    }

    function appendmg(){
        $os = (int)$this->input->get('os');
        if(!$os){
            $os = 2;
        }
        $istop = (int)$this->input->get('istop');
        $confirm = !$istop ? '推送' :'删除';
        $this->assign('confirm', $confirm);
        $this->assign('os', $os);
        $this->assign('istop', $istop);
        $this->load->model('AdStuffMoregameModel');
        $search = $this->input->post('search');
        $list = $this->AdStuffMoregameModel->getAppendMoregame($os,$istop,$search);
        $this->assign('list', $list);
        $this->assign('search', $search);
        $this->display('admin/operation/appendmg.html');
    }

    function ajaxSetMoregameTop(){
        $id = $this->input->post('id');
        $istop = (int)$this->input->post('istop');
        $this->load->model('AdStuffMoregameModel');
        $status = $this->AdStuffMoregameModel->edit(array("textid"=>array("in", $id)), array("istop"=>$istop));
        $this->AdStuffMoregameModel->updateisupdate($id,"ad_stuff_moregame");
        if($status) ajaxReturn('修改成功！', 1, 0);
        ajaxReturn('修改失败！', 0, $this->AdStuffMoregameModel->getlastsql());
    }

    function ajaxSetAppendmg(){
        $id = $this->input->post('id');
        $istop = (int)$this->input->post('istop');
        $os = $this->input->post('os');
        $this->load->model('AdStuffMoregameModel');
        $max = $this->AdStuffMoregameModel->getMaxOrder($os);
        if(empty($id))
        {
            ajaxReturn('请选择要推送的应用！', 0, '');
        }
        $ida = explode(',', $id);

        foreach ($ida as $id)
        {
            if($istop == 0)
            {
                $max = 0;
            }
            else
            {
                $max++;
            }
            $status = $this->AdStuffMoregameModel->edit(array("textid"=>array("in", $id)), array("integraltime"=>time(), 'isintegral'=>$istop, 'integralsort'=>$max));
            $this->AdStuffMoregameModel->updateisupdate($id,"ad_stuff_moregame");
        }

        if($istop == 0)
        {
            $list = $this->AdStuffMoregameModel->getAppendMoregame($os,1);
            $allList = $this->AdStuffMoregameModel->getIsIntegralMoregame($os);
            $allTextIds = array();
            foreach($allList as $v)
            {
                $allTextIds[$v['textid']] = '';
            }
            if(!empty($list))
            {
                foreach ($list as $k=>$v)
                {
                    $this->AdStuffMoregameModel->edit(array("textid"=>$v['textid']), array('integralsort'=>$k+1));
                    if(isset($allTextIds[$v['textid']]))
                    {
                        unset($allTextIds[$v['textid']]);
                    }
                }
                if(!empty($allTextIds))
                {
                    foreach ($allTextIds as $k=>$v)
                    {
                        $this->AdStuffMoregameModel->edit(array("textid"=>$k), array('integralsort'=>0, 'isintegral'=>0));
                    }
                }
            }
        }
        if($status) ajaxReturn('修改成功！', 1, 0);
        ajaxReturn('修改失败！', 0, $this->AdStuffMoregameModel->getlastsql());
    }

    function ajaxSetSignAppendmg(){
        $id = $this->input->post('adid');
        $integralsort = (int)$this->input->post('val');
        $os = $this->input->post('os');
        if($integralsort<=0)
        {
            ajaxReturn('请输入大于0的整数！', 0, '');
        }
        $this->load->model('AdStuffMoregameModel');
        if(empty($id))
        {
            ajaxReturn('未知错误！', 0, '');
        }
        $currentsort = $this->AdStuffMoregameModel->getRow(array('textid' => $id));
        if($currentsort['integralsort'] < $integralsort)
        {
            $integralsort++;
        }
        $status = $this->AdStuffMoregameModel->edit(array("textid"=>array("in", $id)), array("integraltime"=>time(), 'integralsort'=>$integralsort));
        $this->AdStuffMoregameModel->updateisupdate($id,"ad_stuff_moregame");
        $list = $this->AdStuffMoregameModel->getAppendMoregame($os,1);
        $allList = $this->AdStuffMoregameModel->getIsIntegralMoregame($os);
        $allTextIds = array();
        foreach($allList as $v)
        {
            $allTextIds[$v['textid']] = '';
        }
        if(!empty($list))
        {
            foreach ($list as $k=>$v)
            {
                $this->AdStuffMoregameModel->edit(array("textid"=>$v['textid']), array('integralsort'=>$k+1));
                if(isset($allTextIds[$v['textid']]))
                {
                    unset($allTextIds[$v['textid']]);
                }
            }
            if(!empty($allTextIds))
            {
                foreach ($allTextIds as $k=>$v)
                {
                    $this->AdStuffMoregameModel->edit(array("textid"=>$k), array('integralsort'=>0, 'isintegral'=>0));
                }
            }
        }

        if($status) ajaxReturn('修改成功！', 1, 0);
        ajaxReturn('修改失败！', 0, $this->AdStuffMoregameModel->getlastsql());
    }

    function ajaxSetSignIntegral(){
        $id = $this->input->post('adid');
        $integralsort = (int)$this->input->post('val');
        $os = $this->input->post('os');
        if($integralsort<=0)
        {
            ajaxReturn('请输入大于0的整数！', 0, '');
        }
        $this->load->model('AdStuffIntegralModel');
        if(empty($id))
        {
            ajaxReturn('未知错误！', 0, '');
        }

        $currentsort = $this->AdStuffIntegralModel->getRow(array('textid' => $id));
        if($currentsort['integralsort'] < $integralsort)
        {
            $integralsort++;
        }
        $status = $this->AdStuffIntegralModel->edit(array("textid"=>array("in", $id)), array("integraltime"=>time(), 'integralsort'=>$integralsort));
        $this->load->model('AdStuffMoregameModel');
        $this->AdStuffMoregameModel->updateisupdate($id,"ad_stuff_integral");
        $list = $this->AdStuffIntegralModel->getMoregameTop($os,1);
        $allList = $this->AdStuffIntegralModel->getIsIntegralMoregame($os);
        $allTextIds = array();
        foreach($allList as $v)
        {
            $allTextIds[$v['textid']] = '';
        }
        if(!empty($list))
        {
            foreach ($list as $k=>$v)
            {
                $this->AdStuffIntegralModel->edit(array("textid"=>$v['textid']), array('integralsort'=>$k+1));
                if(isset($allTextIds[$v['textid']]))
                {
                    unset($allTextIds[$v['textid']]);
                }
            }
            if(!empty($allTextIds))
            {
                foreach ($allTextIds as $k=>$v)
                {
                    $this->AdStuffIntegralModel->edit(array("textid"=>$k), array('integralsort'=>0));
                }
            }
        }
        if($status) ajaxReturn('修改成功！', 1, 0);
        ajaxReturn('修改失败！', 0, $this->AdStuffIntegralModel->getlastsql());
    }

    function integral(){
        $os = (int)$this->input->get('os');
        if(!$os){
            $os = 2;
        }
        $istop = (int)$this->input->get('istop');
        $confirm = !$istop ? '推送到TOP3' :'从TOP3释放';
        $this->assign('confirm', $confirm);
        $this->assign('os', $os);
        $this->assign('istop', $istop);
        $this->load->model('AdStuffIntegralModel');
        $search = $this->input->post('search');
        $list = $this->AdStuffIntegralModel->getMoregameTop($os,$istop,$search, '');
        $stuffids = '';
        $tlist = array();
        $taskadgroup = null;
        foreach($list as $key=>$val){
            $taskinfo = $this->CommonModel->table("ad_task")->getRow(array("stuffid"=>$val['stuffid']));
            $list[$key]['tasktype'] = $taskinfo['tasktype'];
            $list[$key]['taskname'] = $taskinfo['taskname'];
            $stuffids .= $val['stuffid'].',';
            $taskadgroup[$val['stuffid']] = array('starttime' => $val['starttime'], 'endtime' => $val['endtime']);
            $tlist[$val['stuffid']] = array();
            $tpstring = json_decode($taskinfo['taskprice'], true);
            $retpstring = '';
            if(isset($tpstring[0]['time']))
            {
                foreach($tpstring as $kt=>$vt)
                {
                    $retpstring .= "{$vt['time'][0]}~{$vt['time'][1]}"."<br/>";
                    $tmptimemk = 0;
                    foreach($vt['data'] as $dv)
                    {
                        $retpstring .= "&nbsp;&nbsp;&nbsp;&nbsp;{$dv[0]}至{$dv[1]}时&nbsp;&nbsp;￥".formatmoney($dv[2])."<br/>";
                    }
                }
            }
            else
            {
                $deftaskprice = array(array('time'=>array(date('Y-m-d', $val['starttime']), date('Y-m-d', $val['endtime']))));
                $retpstring .= date('Y-m-d', $val['starttime'])."~".date('Y-m-d', $val['endtime'])."<br/>";
                if(!is_array($tpstring))
                {
                    $retpstring .= "&nbsp;&nbsp;&nbsp;&nbsp;0至24时&nbsp;&nbsp;￥".formatmoney($tpstring);
                }
                else
                {
                    foreach($tpstring as $ktp=>$vtp)
                    {
                        $tpkey = explode(',', $ktp);
                        $retpstring .= "&nbsp;&nbsp;&nbsp;&nbsp;{$tpkey[0]}至{$tpkey[1]}时&nbsp;&nbsp;￥".formatmoney($vtp)."<br>";
                    }
                }
            }
            $list[$key]['taskprice'] = $retpstring;
        }
        if($stuffids){
            $stuffids = rtrim($stuffids, ',');
            $tasklist = $this->CommonModel->table("ad_task")->getList(array("stuffid"=>array("in", $stuffids)));
            foreach($tasklist as $val){
                $tpstring = json_decode($val['taskprice'], true);
                $retpstring = '';
                if(isset($tpstring[0]['time']))
                {
                    foreach($tpstring as $kt=>$vt)
                    {
                        $retpstring .= "{$vt['time'][0]}~{$vt['time'][1]}"."<br/>";
                        $tmptimemk = 0;
                        foreach($vt['data'] as $dv)
                        {
                            $retpstring .= "&nbsp;&nbsp;&nbsp;&nbsp;{$dv[0]}至{$dv[1]}时&nbsp;&nbsp;￥".formatmoney($dv[2])."<br/>";
                        }
                    }
                }
                else
                {
                    $deftaskprice = array(array('time'=>array(date('Y-m-d', $taskadgroup[$val['stuffid']]['starttime']), date('Y-m-d',  $taskadgroup[$val['stuffid']]['endtime']))));
                    $retpstring .= date('Y-m-d',  $taskadgroup[$val['stuffid']]['starttime'])."~".date('Y-m-d',  $taskadgroup[$val['stuffid']]['endtime'])."<br/>";
                    if(!is_array($tpstring))
                    {
                        $retpstring .= "&nbsp;&nbsp;&nbsp;&nbsp;0至24时&nbsp;&nbsp;￥".formatmoney($tpstring);
                    }
                    else
                    {
                        foreach($tpstring as $ktp=>$vtp)
                        {
                            $tpkey = explode(',', $ktp);
                            $retpstring .= "&nbsp;&nbsp;&nbsp;&nbsp;{$tpkey[0]}至{$tpkey[1]}时&nbsp;&nbsp;￥".formatmoney($vtp)."<br>";
                        }
                    }
                }
                $val['taskprice'] = $retpstring;
                $tlist[$val['stuffid']][] = $val;
            }
        }
        $this->assign('tlist', $tlist);
        $this->assign('list', $list);
        $this->display('admin/operation/integral.html');
    }

    function ajaxsetintegraltop(){
        $id = $this->input->post('id');
        $istop = (int)$this->input->post('istop');
        $os = $this->input->post('os');
        $this->load->model('AdStuffIntegralModel');

        $max = $this->AdStuffIntegralModel->getMaxOrder($os);

        if(empty($id))
        {
            ajaxReturn('请选择要推送的应用！', 0, '');
        }
        $ida = explode(',', $id);

        foreach ($ida as $id)
        {
            if($istop == 0)
            {
                $max = 0;
            }
            else
            {
                $max++;
            }
            $status = $this->AdStuffIntegralModel->edit(array("textid"=>array("in", $id)), array("integraltime"=>time(), 'istop'=>$istop, 'integralsort'=>$max));
            $this->load->model('AdStuffMoregameModel');
            $this->AdStuffMoregameModel->updateisupdate($id,"ad_stuff_integral");
        }
        $list = $this->AdStuffIntegralModel->getAppendMoregame($os,1);
        $allList = $this->AdStuffIntegralModel->getIsIntegralMoregame($os);
        $allTextIds = array();
        foreach($allList as $v)
        {
            $allTextIds[$v['textid']] = '';
        }
        if(!empty($list))
        {
            foreach ($list as $k=>$v)
            {
                $this->AdStuffIntegralModel->edit(array("textid"=>$v['textid']), array('integralsort'=>$k+1));
                if(isset($allTextIds[$v['textid']]))
                {
                    unset($allTextIds[$v['textid']]);
                }
            }
            if(!empty($allTextIds))
            {
                foreach ($allTextIds as $k=>$v)
                {
                    $this->AdStuffIntegralModel->edit(array("textid"=>$k), array('integralsort'=>0, 'isintegral'=>0));
                }
            }
        }
        if($status) ajaxReturn('修改成功！', 1, 0);
        ajaxReturn('修改失败！', 0, $this->AdStuffIntegralModel->getlastsql());
    }

    /**
     * 预下载列表
     */
    function predownload(){
        $predown = (int)$this->input->get('predown');
        $this->assign('predown', $predown);
        $confirm = '推送到预下载';
        if($predown == 1){
            $confirm = '释放广告组';
        }
        $where = " and ad_stuff.ispredown = $predown and ad_campaign.ostypeid = 1 and ad_group.adform = 2";
        $search = $this->input->post('search');
        $this->assign('search', $search);
        if($search){
            $where .= " and ( ad_group.adgroupname like '%{$search}%' or ad_group.adgroupid like '%{$search}%')";
        }
        $this->assign('confirm', $confirm);
        $list = array();
        $p=trim($this->input->get('per_page'));
        if(!$p){
            $p=1;
        }
        $config['per_page'] = 20;
        $this->load->model('AdStuffModel');
        $this->AdStuffModel->setStrWhere($where);
        $count = $this->AdStuffModel->getPreCount($where);
        $list = $this->AdStuffModel->getPredown($where, $p,$config['per_page']);
        $this->assign('list', $list);
        $this->load->library('page');
        $current_page =  $p==0 ? 1 : $p;
        $page=$this->page->page_show('/admin/operation/predownload?predown='.$predown,$count,$config['per_page']);
        $this->pagination->create_links();
        $adformname = array('1'=>"Banner广告",'2'=>"弹出广告",'3'=>"精品推荐",'4'=>"广告墙");
        $this->smarty->assign('page', $page);
        $this->smarty->assign('adformname', $adformname);
        $this->smarty->assign('per_page', $config['per_page']);
        $this->smarty->assign('count', $count);
        $this->display('admin/operation/predownload.html');
    }

    function ajaxsetpredownload(){
        $ids = trim($this->input->post('id'), ',');
        $predown = (int)$this->input->post('predown');
        $this->load->model('AdStuffModel');
        $msg = '释放广告组';
        if($predown == 1){
            $msg = '预下载推送';
            $ids1 = explode(',', $ids);
            $newpre = count($ids1);
            $nowpre = $this->AdStuffModel->getCount(array('ispredown'=>1),"stuffid");
            if($nowpre + $newpre > 5){
                return ajaxReturn('预下载列表已到达5个上限,请您先释放,再添加！', 0, 0);
            }
        }
        $status = $this->AdStuffModel->edit(array("stuffid"=>array("in", $ids)), array('ispredown'=>$predown));
        $this->load->model('AdStuffMoregameModel');
        $this->AdStuffMoregameModel->updateisupdate2($ids,'ad_stuff');
        if($status) ajaxReturn($msg. '成功！', 1, 0);
        ajaxReturn($msg.'失败！', 0, $this->AdStuffModel->getlastsql());
    }

    function viewstuff(){
        $stuffid = $this->input->get('id');
        $this->load->model('AdStuffModel');
        $info = $this->AdStuffModel->getRow(array("stuffid"=>$stuffid));
        if(empty($info)){
            exit('广告创意不存在。');
        }
        $this->assign('info', $info);
        switch($info['stufftype']){
            case 1:
                $this->viewBanner($info);
                break;
            case 2:
                $this->viewBannerText($info);
                break;
            case 3:
                $this->viewPopup($info);
                break;
            case 4:
                break;
            case 5:
                $this->viewMoregame($info);
                break;
            case 6:
                $this->viewTask($info);
                break;
            default:
                break;
        }
    }

    private function viewTask($info){
        $stuff = $this->CommonModel->table("ad_stuff_integral")->getList(array('stuffid'=>$info['stuffid']));
        $this->assign('stuff', $stuff);
        $this->display('admin/service/viewtask.html');
    }

    private function viewBanner($info){
        $this->load->model('AdStuffImgModel');
        $stuff = $this->AdStuffImgModel->getList(array('stuffid'=>$info['stuffid'],'status'=>1));
        $this->assign('stuff', $stuff);
        $this->display('admin/service/viewbanner.html');
    }

    private function viewBannerText($info){
        $this->load->model('AdStuffTextModel');
        $stuff = $this->AdStuffTextModel->getList(array('stuffid'=>$info['stuffid']));
        $this->assign('stuff', $stuff);
        $this->display('admin/service/viewbannertext.html');
    }

    private function viewPopup($info){
        $this->load->model('AdStuffImgModel');
        $stuff = $this->AdStuffImgModel->getList(array('stuffid'=>$info['stuffid']));
        $this->assign('stuff', $stuff);
        $this->display('admin/service/viewpopup.html');
    }

    private function viewMoregame($info){
        $this->load->model('AdStuffMoregameModel');
        $stuff = $this->AdStuffMoregameModel->getList(array('stuffid'=>$info['stuffid']));
        $this->assign('stuff', $stuff);
        $this->display('admin/service/viewmoregame.html');
    }
    function about(){
        echo '此栏目部分功能已经转移到广告管理菜单下，如有问题，请联系技术人员！';exit;
    }
}
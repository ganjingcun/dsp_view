<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

define('CONTROLLERS', str_replace("\\", "/", dirname(__FILE__).'/singleSDKmanage.php'));
include_once(CONTROLLERS);

class singleSDKadmin extends singleSDKmanage {

	function __construct()
	{
		parent::__construct();
	}
    
    public function doAddChannelPublic() {
        
        parent::doAddChannelPublic(3);
    }
    
    
    public function doEditPublicContent() {
        $status = $this->input->post('status');
        if($status!=null) {
            parent::doEditPublicContent($status);
        } else {
            parent::doEditPublicContent();
        }
    }
    
	/*
	 * 分渠道游戏公告
	 **/
    public function gamePublicListPage($appid = 0){
        
        
        $this->assign('tabindex', 0);
        
        
        $this->smarty->assign('today',date('Y-m-d'));
        
        //form field
        if($appid==0) {
            $appid = $this->input->get('appid') ? $this->input->get('appid') : 0;
        }
        $this->_checkSwitchStatus($appid);
        if(empty($appid)) {
            show_error("请在\"媒体游戏管理\"界面下跳转渠道公告管理页面",301,"操作流程调整");
        }
        $this->assign('appid', $appid);
        $keyword  = $this->input->get('keyword');
        $keyword = trim($keyword);
        $search_title = $this->input->get('search_title')?$this->input->get('search_title'):'';
        $search_title = trim($search_title);
        $publish_id = $this->input->get('publisherID')?$this->input->get('publisherID'):'';
        $dateCond = $this->getDateRangeByControl();
        
        //$start_date = $this->input->get('startDate')?$this->input->get('startDate'):'';
        //$end_date = $this->input->get('endDate')?$this->input->get('endDate'):'';
        
        //order by
        $order_field = $this->input->get('order_field')?$this->input->get('order_field'):'dingorder';
        $order_direct = $this->input->get('order_direct')?$this->input->get('order_direct'):'asc';
        
        
        $this->smarty->assign('keyword',$keyword);
        $this->smarty->assign('search_title',$search_title);
        $this->smarty->assign('orderField',$order_field);
        $this->smarty->assign('orderDirect',$order_direct);
        $appname = $this->AppInfoModel->getOne(array('appid'=>$appid),'appname');
        $p=trim($this->input->get('per_page'));
        if(!$p){
            $p=1;
        }
        $data['status'] = array('NOT IN', array('0', '6'));
        if(!empty($appid)){
            $data['appid'] = $appid;
        }
        
        
    if(!empty($dateCond)) {
             
            $data['publish_time']=array(array('EGT',strtotime($dateCond['sDate'])));
        }
        
        if(!empty($dateCond)) {
            if(isset($data['publish_time']) && is_array($data['publish_time'])) {
                $data['publish_time'][]=array('LT',strtotime($dateCond['eDate'])+86400);
            } else {
                $data['publish_time']=array('LT',strtotime($dateCond['eDate'])+86400);
            }
        }
        
        if($search_title!=null && $search_title!='') {
            $titles = preg_split('/\s+/', $search_title);
            if(sizeof($titles)>1) {
                foreach ($titles as $_t) {
                    $__tmp_title_array[] = array('LIKE','%'.$_t.'%');
                }
               $__tmp_title_array[] = 'OR';
                $data['title'] = $__tmp_title_array;
            } else {
                $data['title']=array('LIKE','%'.$search_title.'%');
            }
        }
        
        if(!empty($keyword)) {
           $kwds = preg_split('/\s+/', $keyword);
            //$kwds = explode(' ',$keyword);
            if(sizeof($kwds)>1) {
                foreach ($kwds as $_t) {
                    $__tmp_desc_array[] = array('LIKE','%'.$_t.'%');
                }
                $__tmp_desc_array[] = 'OR';
                $data['announcement'] = $__tmp_desc_array;
            } else {
                $data['announcement']=array('LIKE','%'.$keyword.'%');
            }
        }
        
        
        $config['per_page'] = $this->config->item('pagesize');
        //获取表数据
        $this->load->model('AdChannelPublicModel');
        $this->load->model('AppChannelModel');
        
        $morenOrder = "";
        //if($order_field=='dingorder') {
            $morenOrder = ',publish_time desc';
        //}
        $public_data = $this->AdChannelPublicModel->getList($data, '*', "{$order_field} {$order_direct} $morenOrder ", "", $p, $config['per_page']);
        //echo $this->AdChannelPublicModel->getLastSql(); 
       
        $this->AdChannelPublicModel->setStrWhere($this->AdChannelPublicModel->getWhere($data));
		$count = $this->AdChannelPublicModel->getCount();
        $this->load->library('page');
         
        $var = 'appid='.$appid;
        foreach($_GET as $_paramk =>$paramv) {
            if($_paramk!='per_page') {
                $var.='&'.$_paramk.'='.$paramv;
            }
        }
        
        $pageind = $this->input->get('per_page')?$this->input->get('per_page'):1;
        
        
        $config['per_page'];
        $this->assign('tableIndStart', ($pageind-1)*$config['per_page']+1);
        $page=$this->page->page_show('/admin/singleSDKadmin/gamePublicListPage?'.$var,$count,$config['per_page']);
        //处理显示数据
        $appid_arr = $channel_id_arr = array();
        foreach ($public_data as $key => $item) {
        	$appid_arr[] = $item['appid'];
        	$channel_tmp = explode(',', $item['publisherID']);
        	foreach ($channel_tmp as $key => $channelid) {
        		$channel_id_arr[] = $channelid;
        	}
        }
        array_unique($appid_arr);
        array_unique($channel_id_arr);
       
        //channel相关信息转换
        $channel_where['publisherID'] = array('IN', $channel_id_arr);
        $channellist = $this->AppChannelModel->getList($channel_where, 'publisherID, channelname');
        $channel_tmp = array();
        foreach ($channellist as $key => $item) {
        	$channel_tmp[$item['publisherID']] = $item;
        }
        $channellist = $channel_tmp;
        $now = time();
        foreach ($public_data as $key => &$item) {
        	$publisherID_arr = explode(',', $item['publisherID']);
        	$publisherID_tmp = array();
        	foreach ($publisherID_arr as $key => $value) {
        		$publisherID_tmp[] = $channellist[$value]['channelname'];
        	}
        	$publisherID_str = implode(',', $publisherID_tmp);
        	$item['channelname'] = $publisherID_str;
            $item['thumb']=$this->_getAnnounceImgByCampaignId($item['campaignid']);
            $callTypeAndUrlResult = $this->_getCallTypeAndCallURIByCampaignId($item['campaignid']);
            $item = array_merge($item,$callTypeAndUrlResult);
        	$item['publishtime'] = date("Y-m-d H:i:s", $item['publish_time']);
        	if( $item['publish_time'] < time() && $item['status'] == 1 ){
        		$this->AdChannelPublicModel->edit(array('id' => $item['id']), array('status' => 4));
        		$item['status'] = 4;
        	}
        	else if( $item['status'] == 4 && $item['publish_time'] > time()){
        		$this->AdChannelPublicModel->edit(array('id' => $item['id']), array('status' => 1));
        		$item['status'] = 1;
        	}
        	
        	$this->assign('role_operator', 'admin');
        	switch ($item['status']) {
        		case '1':
        			$status = '<span class="label">待审核</span>';
                    $operate = '<a href="verifyPublicContentPage?id='.$item['id'].'">审核</a>&nbsp;|&nbsp;<a href="editPublicContentPage?id='.$item['id'].'">编辑</a>&nbsp;|&nbsp;<a href="delChannelPublic?id='.$item['id'].'&campaignid='.$item['campaignid'].'&appid='.$appid.'" onclick="return confirm(\'是否要删除\');">删除</a>';
        			break;
        		case '2':
        			$status = '<span class="label label-inverse">审核未通过</span>';
                    $operate = '<a class="btn btn-primary" href="editPublicContentPage?id='.$item['id'].'"><i class="icon-edit icon-white"></i>编辑</a>&nbsp;|&nbsp;<a class="btn btn-danger" href="delChannelPublic?id='.$item['id'].'&campaignid='.$item['campaignid'].'&appid='.$appid.'" onclick="return confirm(\'是否要删除\');"><i class="icon-white  icon-minus-sign"></i>删除</a>';
        			break;
        		case '3':
        			$status = '<span class="label label-success">已发布</span>';
                    $operate = '<a class="btn btn-primary" href="showPublicContent?id='.$item['id'].'&from='. urlencode($_SERVER['REQUEST_URI']).'">查看</a>&nbsp;|&nbsp;<button class="btn" onclick="if(confirm(\'确认操作？\')) location.href=\'stopChannelPublic?id='.$item['id'].'&campaignid='.$item['campaignid'].'&appid='.$appid.'&pageid='.$pageind.'\'">停用</a>';
        			break;
        		case '4':
        			$status = '<span class="label label-warning">过期自动驳回</span>';
                    $operate = '<a class="btn btn-primary" href="editPublicContentPage?id='.$item['id'].'"><i class="icon-edit icon-white"></i>编辑</a>&nbsp;|&nbsp;<a class="btn btn-danger" href="delChannelPublic?id='.$item['id'].'&campaignid='.$item['campaignid'].'&appid='.$appid.'" onclick="return confirm(\'是否要删除\');"><i class="icon-white  icon-minus-sign"></i>删除</a>';
        			break;
        		case '5':
        			$status = '<span class="label label-important">已停用</span>';
                    $operate = '<a class="btn btn-primary evt_disable_announce" href="showPublicContent?id='.$item['id'].'&appid='.$appid.'">查看</a>&nbsp;|&nbsp;<a class="btn" onclick="if(confirm(\'确认操作？\')) location.href=\'enableChannelPublic?id='.$item['id'].'&campaignid='.$item['campaignid'].'&appid='.$appid.'&pageid='.$pageind.'\'">启用</a>';
        			break;
        		default:
        			$status = '<span class="label">未知</span>';
        			$operate = '<a href="#">无</a>';
        			break;
        	}
        	$item['status'] = $status;
        	$item['operate'] = $operate;
            $timediff = $now-strtotime($item['publishtime']);
             
            if($timediff<3600) {
                $item['timediff'] = intval($timediff/60).'分钟前';
            } else if($timediff<86400) {
                $item['timediff'] = intval($timediff/3600).'小时前';
            }
             
        }
		//print_r($channellist);
		$this->smarty->assign('page', $page);
        $this->smarty->assign('per_page', $config['per_page']);
        $this->smarty->assign('count', $count);
        $this->smarty->assign('appid', $appid);
        $this->smarty->assign('appname',$appname);
        //$this->smarty->assign('gamelist', $gamelist);
		$this->smarty->assign('publicdata', $public_data);
		$this->load->display('admin/singleSDKmanage/gamePublicList.html');
	}
	//审核
	public function verifyPublicContentPage(){
		$id = trim($this->input->get('id'));
		if(empty($id))
			exit('缺少参数');
		$channeldata = $this->AdChannelPublicModel->getRow(array('id' => $id));

		$channeldata['thumb'] = _getAnnounceImgByCampaignId($channeldata['campaignid']);
		
		$appname = $this->AppInfoModel->getOne(array('appid' => $channeldata['appid']), 'appname');
		$channeldata['appname'] = $appname;
		$channel = explode(',', $channeldata['publisherID']);
		$channel_name_arr = $this->AppChannelModel->getList(array('publisherID' => array('IN', $channel)), 'channelname');
		$channel_name = array();
		foreach ($channel_name_arr as $key => $value) {
			$channel_name[] = $value['channelname'];
		}
		$channel_name = implode('、', $channel_name);
		$channeldata['channelname'] = $channel_name;
		$channeldata['publish_time'] = date('Y-m-d H:i:s', $channeldata['publish_time']);
		$channeldata['verify'] = true;
		//
		if( empty($channeldata) ){
			exit('参数错误');
		}
		$this->smarty->assign('position', '分渠道游戏公告详情');
		$this->smarty->assign('channeldata', $channeldata);
		$this->load->display('admin/singleSDKmanage/showPublicContent.html');
	}
	public function doVerifyPublicContent(){
		$id = trim($this->input->get('id'));
		$this->load->model('AdCampaignModel');
		$campaignid = trim($this->input->get('campaignid'));
		$agree = trim($this->input->get('agree'));
		$time = time();
		$campaign_data = $channel_public_data = array();
		if($agree == 'y')
		{
			$campaign_data = array(
				'status' => 2,
				'lastupdatetime' => $time
			);
			$channel_public_data = array(
				'status' => 3,
				'lastupdatetime' => $time
			);
		}
		else if($agree == 'n')
		{
			$channel_public_data = array(
				'status' => 2,
				'lastupdatetime' => $time
			);
		}
		$this->AdCampaignModel->edit(array('campaignid' => $campaignid), $campaign_data);
		$this->AdChannelPublicModel->edit(array('id' => $id), $channel_public_data);
		$this->gamePublicListPage();
	}

    
    /**
     * ccplay 礼包列表
     */
    public function ccplayGiftList() {
        
        $pgsize =$this->config->item('pagesize');
        
        $appid = trim($this->input->get('appid'));
        
        $this->assign('tabindex',1);
        
        if(empty($appid)) {
            show_error("缺少参数，请从应用管理界面进入",'301');
        }
        $this->assign('appid', $appid);
        $this->_checkSwitchStatus($appid);
        $statusMap = array(1=>'启用',2=>'停用');
                
        $appname = $this->AppInfoModel->getOne(array('appid'=>$appid),'appname');
        $this->assign('appname',$appname);
        $this->assign('appid',$appid);
        
        $this->load->model('CcplayGiftDefine');
        $condition = array('appid'=>$appid);
        $pgnum = $this->input->get('per_page')?$this->input->get('per_page'):1;
        
        $orderField = $this->input->get('orderField');
        $orderDirect = $this->input->get('orderDirect');
        
        if(empty($orderField)) {
            $orderField = 'id';
        }
        if(empty($orderDirect)) {
            $orderDirect = 'desc';
        }
        
        $this->assign('orderDirect', $orderDirect);
        $this->assign('orderField', $orderField);
        $morenOrder = ',ctime desc';
        //$condition=array(),$fileds = '*',$orderBy='',$groupBy='', $pageId = 1, $pageSize = 0
        $result = $this->CcplayGiftDefine->getList($condition,$fileds = '*',$orderBy=" $orderField $orderDirect $morenOrder", '', $pgnum, $pgsize);
        $count = $this->CcplayGiftDefine->getCount($condition);
        $now = time();
        
        $dailyGiftList = array();
        foreach($result as $key => &$v) {
            $result[$key]['statusname'] = $statusMap[$v['status']];
            $giftcontent = json_decode($result[$key]['content'],true);
            $giftString = "";
            if(!empty($giftcontent)) {
                foreach($giftcontent as $item) {
                    if(isset($item['gameitemkey']) && isset($item['itemcnt'])) {
                        $giftString .= $item['giftitemname'].' / ';
                    }
                }
            }
            //echo $giftString.'</br>';
            $result[$key]['content'] = $giftString;
            
            $timediff = $now-strtotime($v['ctime']);
            if($timediff<3600) {
                $v['timediff'] = intval($timediff/60).'分钟前';
            } else if($timediff<86400) {
                $v['timediff'] = intval($timediff/3600).'小时前';
            }
            
            if($v['isdailytask']>0) {
                $dailyGiftList[$v['isdailytask']] = $v;
            }
        }
        
        $this->load->library('page');
        
        $var = '?appid='.$appid.'&orderField='.$orderField.'&orderDirect='.$orderDirect;
        //$base_url,$total_rows,$per_page,$uri_segment=4
        $page = $this->page->page_show ( '/admin/singleSDKadmin/ccplayGiftList' . $var, $count, $pgsize);
        
        $this->assign('pgsize',$pgsize);
        $this->assign('count', $count);
        $this->assign('page',$page);
        $this->assign('dailygift',$dailyGiftList);
        $this->assign('rows',$result);
        $this->display('admin/singleSDKmanage/ccplayGiftList.html');
    }
    
    /**
     * 停止ccplay礼包（删除）
     */
    public function ccplayGiftDisable() {
        $this->assign('tabindex',1);
        $appid = trim($this->input->get('appid'));
        $giftid = trim($this->input->get('giftid'));
        if(empty($appid) || empty($giftid)) {
            show_error("缺少参数，请从应用管理界面进入",'301');
        } else {
            echo 1111;
        }
        $this->load->model('CcplayGiftDefine');
        $condition = array('appid'=>$appid,'id'=>$giftid);
        $campaignid = $this->CcplayGiftDefine->getOne($condition,'campaignid');
        $this->load->model('AdCampaignModel');
        if(!empty($campaignid)) {
            $this->AdCampaignModel->edit(array('campaignid'=>$campaignid),array('status'=>3));
        }
        
        $result = $this->CcplayGiftDefine->edit($condition,array('status'=>2));
        redirect('ccplayGiftList?appid='.$appid);
    }
    /**
     * 启用ccplay礼包
     */
    public function ccplayGiftEnable() {
        $this->assign('tabindex',1);
        $appid = trim($this->input->get('appid'));
        $giftid = trim($this->input->get('giftid'));
        
        if(empty($appid) || empty($giftid)) {
            show_error("缺少参数，请从应用管理界面进入",'301');
        } else {
            echo 222;
        }
        $this->load->model('CcplayGiftDefine');
        $condition = array('appid'=>$appid,'id'=>$giftid);
        
        
        $result = $this->CcplayGiftDefine->edit($condition,array('status'=>1));
        
        $this->load->model('AdCampaignModel');
        $campaignid = $this->CcplayGiftDefine->getOne($condition,'campaignid');
        if(!empty($campaignid)) {
            $this->AdCampaignModel->edit(array('campaignid'=>$campaignid),array('status'=>2));
        }
        
        redirect('ccplayGiftList?appid='.$appid);
    }
    
    /**
     * 编辑礼包页面
     */
    public function ccplayGiftEdit() {
        $appid = trim($this->input->get('appid'));
        $giftid = trim($this->input->get('giftid'));
        $appname = $this->AppInfoModel->getOne(array('appid'=>$appid),'appname');
        $this->assign('appname',$appname);
        $this->assign('appid',$appid);
        $this->assign('tabindex',1);    
        $this->_checkSwitchStatus($appid);
        if(empty($appid)) {
            show_error("缺少参数，请从应用管理界面进入",'301');
        } else if(empty($giftid)) {
            $this->assign("pagetitle", '新增加礼包');
            $this->display('admin/singleSDKmanage/ccplayGiftNew.html');
        } else {
            $this->assign('pagetitle','编辑礼包');
            $this->load->model('CcplayGiftDefine');
            $condition = array('id'=>$giftid,'appid'=>$appid);
            $result = $this->CcplayGiftDefine->getRow($condition);
            
            $this->assign('result',$result);
            $this->display('admin/singleSDKmanage/ccplayGiftEdit.html');
        }
    }
        
    
    
    /**
     * 修改礼包内容
     */
    public function ccplayGiftEditSubmit() {
        $appid = trim($this->input->post('appid'));
        $giftid = $this->input->post('giftid');
        $giftpackagename = $this->input->post('giftpackagename',true);
        
        $pkgitemname = $this->input->post('pkgitemname');
        
        
        
        if(empty($giftpackagename)) {
            $retMsg = "礼包名称不能为空";
        if($this->input->is_ajax_request()) {
                ajaxReturn($retMsg, -1);
            } else {
                $this->pageMsgBack($retMsg);
            }
        }
        
        if(WeiboLength($giftpackagename)>12) {
            $retMsg = "礼包名称长度最长12个汉字、或24个字符";
            if($this->input->is_ajax_request()) {
                ajaxReturn($retMsg, -1);
            } else {
                $this->pageMsgBack($retMsg);
            }
            
        }
        $this->load->model('CcplayGiftDefine');
        
        if(empty($appid) || empty($giftid)) {
            $retMsg = "缺少参数，请从应用管理界面进入";
            if($this->input->is_ajax_request()) {
                ajaxReturn($retMsg, -1);
            } else {
                $this->pageMsgBack($retMsg);
            }
        }
        
        //$checkExists['content']
        $checkExists = $this->CcplayGiftDefine->getList(array('appid'=>$appid,'giftpackagename'=>$giftpackagename));
        $currentData = $this->CcplayGiftDefine->getRow(array('appid'=>$appid,'id'=>$giftid));
        
        $thisRow = null;
        foreach($checkExists as $row) {
            if($row['id']!=$giftid) {
                
                $retMsg = "礼包名称已经存在了，换个名称吧";
                if($this->input->is_ajax_request()) {
                    ajaxReturn($retMsg, -1);
                } else {
                    $this->pageMsgBack($retMsg);
                }
            }
        }
        
        $oldcontentVal = $currentData['content'];
        $old_content = json_decode($oldcontentVal,true);
        if($old_content!=null) {
            for($i=0;$i<count($old_content);$i++) {
            	if(!empty($pkgitemname)) {
            	        if(WeiboLength($pkgitemname[$i])>18) {
            	            ajaxReturn("礼包描述最大18个中文字符、36个英文字符", -1);
            	        }
            		$old_content[$i]['giftitemname'] = $pkgitemname[$i];
            	}
            }
        } else {
            log_message('error','礼包编辑解析编辑前的礼包值失败,原值为:'.$old_content);
        }
        
        
        //print_r($old_content);
        
        //解析旧数据（修改过名称后）
        $giftpackageval = json_encode($old_content);
        if($giftpackageval== null || 'null'==$giftpackageval ||''==$giftpackageval) { //格式报错使用旧值
            $giftpackageval = $oldcontentVal;
        } else {
            log_message('error','礼包编辑名称后转换为JSON字符失败'.print_r($old_content,true));
        }
        
        
        $this->load->model('CcplayGiftDefine');
        $condition = array('id'=>$giftid,'appid'=>$appid);
        
        if(mb_strlen($giftpackageval,"UTF-8")>8000) {
            if($this->input->is_ajax_request()) {
                ajaxReturn('存储的数据超过了数据库限制，每个礼包最多允许8000个字符，请联系管理员升级', -1);
            }
        }
        
        $newdata = array(
                'giftpackagename'=>$giftpackagename,
                'content'=>$giftpackageval
        );
        
        
        
        
        $result = $this->CcplayGiftDefine->edit($condition,$newdata);
        
        if($result!=-1) {
            $retMsg = "修改成功";
            $retcode = 1;
        } else {
            $retMsg = "修改失败";
            $retcode = -1;
        }
        if($this->input->is_ajax_request()) {
            ajaxReturn($retMsg, $retcode);
        } else {
            $this->pageMsgBack($retMsg);
        }
    }

        
    /**
     * 增加礼包提交
     */
    public function ccplayGiftAddSubmit() {
        $giftpackageval = trim($this->input->post('submitvalue'));
        $appid = trim($this->input->post('appid'));
        
        $giftpackagename = trim($this->input->post('giftpackagename',true));
        
        if(empty($giftpackagename)) {
            $retMsg = "礼包名称不能为空";
            if($this->input->is_ajax_request()) {
                ajaxReturn($retMsg, -1);
            } else {
                $this->pageMsgBack($retMsg);
            }
        } 
        
        if(WeiboLength($giftpackagename)>12) {
            $retMsg = "礼包名称长度最长12个汉字、或24个字符";
            if($this->input->is_ajax_request()) {
                ajaxReturn($retMsg, -1);
            } else {
                $this->pageMsgBack($retMsg);
            }
        }
        

        $this->load->model('CcplayGiftDefine');
        $checkExists = $this->CcplayGiftDefine->getRow(array('appid'=>$appid,'giftpackagename'=>$giftpackagename));
        
        if(!empty($checkExists)) {
            
            $retMsg = "礼包名称已经存在了，换个名称吧";
            if($this->input->is_ajax_request()) {
                ajaxReturn($retMsg, -1);
            } else {
                $this->pageMsgBack($retMsg);
            }
        }
        
        if(empty($appid)) {
            show_error("请从应用管理界面进入管理，参数中没找到应用标识",'301');
        }
        
        if(empty($giftpackageval) || $giftpackageval=='[]') {
            $retMsg = "不能增加不含礼品的礼包，至少应该有一项礼品";
            if($this->input->is_ajax_request()) {
                ajaxReturn($retMsg, -1);
            } else {
                $this->pageMsgBack($retMsg);
            }
        }
        
        $giftpackageval = trim($giftpackageval);
        if(mb_strlen($giftpackageval)>8000) {
            $retMsg = "存储的数据超过了数据库限制，每个礼包最多允许8000个字符，请联系管理员升级";
            if($this->input->is_ajax_request()) {
                ajaxReturn($retMsg, -1);
            } else {
                $this->pageMsgBack($retMsg);
            }
        }
        
        $giftpackgeArr = json_decode($giftpackageval,true);
        foreach ($giftpackgeArr as $k=>$v) {
                if(WeiboLength($v['giftitemname'])>18) {
                    ajaxReturn("礼包描述最大18个中文字符、36个英文字符", -1);
                }
        }
        
        $time =time();
        
        
        //TODO $campaignid..
        // 要插入数据的表
        $this->load->model ( 'AdCampaignModel' );
        $this->load->model ( 'AdGroupModel' );
        $this->load->model ( 'AdStuffModel' );
        $this->load->model ( 'AdStuffTextModel' );
        
        $userid = $this->AppInfoModel->getOne ( array (
                'appid' => $appid
        ), 'userid' );
        
        $campaign_data = array (
                'campaignname' => '礼包:'.$giftpackagename,
                'targettype' => '2',
                'periodset' => '0',
                'ostypeid' => 1,
                'osversionset' => 0,
                'aosvset' => '',
                'createtime' => $time,
                'status' => 2,//1 待投放，2 投放中，3 已暂停，4 已中止 5 已结束 6 已删除.
                'selfappset' => 1, //正常广告
                'userid' => $userid
        );
        $campaign_id = $this->AdCampaignModel->add ( $campaign_data );
        
        $group_data = array (
                'campaignid' => $campaign_id,
                'adgroupname' => $giftpackagename,
                'adform' => '11',
                'chargemode' => 1,
                'adtargettype' => 2,
                'selfappset' => 1,
                'is_ccplay_announce' => 1,
                'starttime' => $time,
                'endtime' => ($time + 60 * 60 * 24 * 365),
                'userid' => $userid,
                'createtime' => $time,
                'lastupdatetime' => $time,
                'istracktype' => 2,
                'status' => 1,
                'effecttype' => 20
        ) // 游戏内模块20 HTML 21
        ;
        $group_id = $this->AdGroupModel->add ( $group_data );
        $stuff_data = array (
                'adgroupid' => $group_id,
                'campaignid' => $campaign_id,
                'adstuffname' => $giftpackagename,
                'stufftype' => 2,
                'lastupdatetime' => $time,
                'status' => 1
        );
        $stuff_id = $this->AdStuffModel->add ( $stuff_data );
        
        $stuff_text_data = array (
                'stuffid' => $stuff_id,
                'adtitle' => $giftpackagename,
                'adcontent' => '',
                'path' => null
        );
        $this->AdStuffTextModel->add ( $stuff_text_data );
        
        
        
        $this->load->model('CcplayGiftDefine');
        $data = array(
                'appid'=>$appid,
                'campaignid'=>$campaign_id,
                'giftpackagename'=>$giftpackagename,
                'content'=>trim($giftpackageval),
                'status'=>2,
                'isdailytask'=>0,
                'ctime'=>date('Y-m-d H:i:s',$time)
        );
        $result = $this->CcplayGiftDefine->add($data);
        $retMsg = "";
        if($result!=-1) {
            $retMsg = "处理成功";
        } else {
            $retMsg = "处理失败";
        }
        
        if($this->input->is_ajax_request()) {
            ajaxReturn($retMsg, $result);
        } else {
            $this->pageMsgBack($retMsg,'/admin/singleSDKadmin/ccplayGiftList?appid='.$appid);
        }
    }
    
    
    
    /**
     * 每日礼包设置页面
     */
    public function ccplayGiftDailyTask() {
        
        $this->assign('tabindex',1);
        
        
        $appid = $this->input->get('appid');
        if(empty($appid)) {
            show_error("缺少参数，请从应用管理界面进入",'301');
        }
        
        
        $this->load->model ( 'AppChannelModel' );
        
        $appname = $this->AppInfoModel->getOne ( array (
                'appid' => $appid
        ), 'appname' );
        $this->_checkSwitchStatus($appid);
        $this->assign('appname', $appname);
        $this->load->model('CcplayGiftDefine');
        $condition = array('appid'=>$appid);
        $result = $this->CcplayGiftDefine->getList($condition,$fileds = '*',$orderBy='id desc', $pageSize = 0);
        
        $selected_gift = array();
        foreach($result as $key => &$v) {
            
            $giftcontent = json_decode($result[$key]['content'],true);
            $giftString = "";
            if(!empty($giftcontent)) {
                foreach($giftcontent as $item) {
                   // echo $item['giftitemname'];
                    if(isset($item['giftitemname'])) {
                        $giftString .= $item['giftitemname'].' / ';
                    }
                }
            }
            //$giftString = mb_substr($giftString, 0,$giftString-2);
            $result[$key]['content'] = htmlspecialchars($giftString);
                
                
            if($v['isdailytask']>0) {
                $selected_gift[$v['isdailytask']] = $v;
            }
        }
        
        ksort($selected_gift);
        
        $this->assign('selected_gift', $selected_gift);
        $this->assign('rows',$result);
        $this->assign('appid', $appid);
        $this->display('admin/singleSDKmanage/ccplayGiftDailySetPage.html');
    }
    
    /**
     * 每日礼包设置ACTION
     */
    public function giftDailyConfig() {
        $appid = $this->input->post('appid',true);
        $result = array();
        
        $giftids = $this->input->post('giftids');
        $this->load->model('AdCampaignModel');
        if(empty($appid)) {
            $result['info']='参数错误，保存失败'.$appid;
        } else {
            //remove old 礼包flag
            
            $this->load->model('CcplayGiftDefine');
            $oldcampaignids = $this->CcplayGiftDefine->getCol(array('appid'=>$appid),'campaignid');
            
            $this->AdCampaignModel->edit(array('campaignid'=>array('IN',$oldcampaignids)),array('status'=>3));
            $this->CcplayGiftDefine->edit(array('appid'=>$appid),array('isdailytask'=>0,'status'=>'2'));
            
            
            if(!empty($giftids)) {
                $campaignids = $this->CcplayGiftDefine->getCol(array('id'=>array('IN',$giftids)),'campaignid');
                if(!empty($campaignids)) {
                    $this->AdCampaignModel->edit(array('campaignid'=>array('IN',$campaignids)),array('status'=>2));
                }
           
                $ord = 1;
                foreach($giftids as $updateitem) {
                    $condition = array('appid'=>$appid,'id'=>$updateitem);
                    $this->CcplayGiftDefine->edit($condition,array('isdailytask'=>$ord++,'status'=>'1'));
                }
            }
            
            $result['info']='保存成功';
        }
        
        echo json_encode($result);
    }
    
    
    /**
     * 客户端相关配置（强制开启）
     */
    public function appClientConfig() {
        
        $this->assign('tabindex', 6);
        $this->load->model('AppChannelModel');
        $appid = $this->input->get('appid');
        if(empty($appid)) {
            $this->pageMsgBack('参数有误，请从应用界面进入');
        }
        $channelids = $this->AppChannelModel->getList(array('appid'=>$appid,'is_ccplay'=>array('GT',0)),'id,channelid,channelname,publisherID');
        
        $this->load->model('AppChannelConfigModel');
        $appname = $this->AppInfoModel->getOne(array('appid'=>$appid),'appname');
        $this->assign('appname', $appname);
        $cond= array('appid'=>$appid,'configname'=>'ccplay_autopop','configvalue'=>'1','channelid'=>array('NEQ',0));
        
        $this->assign('appid', $appid);
        
        $selectedchannels =  $this->AppChannelConfigModel->getList($cond);
        
        $selectedchannelsArr = array();
        foreach($selectedchannels as &$item) {
            $selectedchannelsArr[$item['channelid']] = $item['channelid'];
        }
        
        $selectChannelids= array_keys($selectedchannelsArr);
        foreach ($channelids as &$item)
        {
            if(isset($item['channelid']) && in_array($item['channelid'],$selectChannelids)) {
                $item['configvalue'] = 1;
            } else {
                $item['configvalue'] = 0;
            }
        }
        $this->assign('openedchannels', $channelids);
        
        $this->display('admin/singleSDKmanage/ccplayClientConfig.html');
    }
    
    
    
    /**
     * 刷新ccplay与ccdata的开关文件更新
     */
    public function flushCcplayDataFileBridge() {
        $this->load->model('ccplay/CcplayBiz');
        $this->CcplayBiz->flushCCplayPIDFileByAppId();
        exit('刷新完成');
    }
    
    /**
     * 更新ccplay 客户端配置Action
     */
    public function updateCcplayClientHardOpen() {
        $appid = $this->input->post('appid');
        $channelid = $this->input->post('channelid');
        $configname = $this->input->post('configname');
        
        if(empty($configname) || empty($appid)) {
            $msg = "参数错误";    
        } else {
            $newval = $this->input->post('configvalue');
            $this->load->model('AppChannelConfigModel');
            $data = array('appid'=>$appid,'channelid'=>$channelid,'configname'=>'ccplay_autopop','configvalue'=>$newval);
            $retCode = $this->AppChannelConfigModel->add($data,true);
        }
        if($retCode>-1) {
            $filepath = $this->config->item('ccplay_data_briage_path');
            if(!file_exists($filepath)) {
                createDir($filepath);
            }
            
            $this->load->model('AppChannelModel');
            $pid = $this->AppChannelModel->getOne(array('channelid'=>$channelid),'publisherID');
            $pidfilename = $filepath.$pid.'.html';
            if('1'==$newval) {
                file_put_contents($pidfilename,'{"ccplay": "1", "fceopen": "1"}');
            } else {
                file_put_contents($pidfilename,'{"ccplay": "1", "fceopen": "0"}');
            }
                        
            
            ajaxReturn("更新成功", $retCode);
        } else {
            ajaxReturn("更新失败", $retCode);
        }
    }
    
   
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: chukong
 * Date: 2/26/16
 * Time: 2:16 PM
 *
 * 用于与畅思DSP进行一些数据和日志同步
 */

class DataApi extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('DataApiModel');
        $this->load->model('SystemConfigModel');
    }

    /**
     * 将畅思DSP中的应用日志导入到本地数据库
     */
    public function appGather()
    {
        $result = $this->DataApiModel->appGather();
        echo $result['error'];
    }

    /**
     * 畅思
     */
    public function doAuditAPI()
    {
        if (isset($GLOBALS["HTTP_RAW_POST_DATA"]))
        {
            $postdata = json_decode($GLOBALS["HTTP_RAW_POST_DATA"]);
        }
        if (!isset($postdata) || !isset($postdata->userinfo) || !isset($postdata->params))
        {
            ajaxReturn('参数错误.', 0, 0);
        }
        $apiauth = $this->config->item('dsp_api_userinfo');
        if ($postdata->userinfo->username != $apiauth['username'] || $postdata->userinfo->key != $apiauth['key'])
        {
            ajaxReturn('身份验证失败.', 0, 0);
        }


        $appid = $postdata->params->appid;
        $reason = trim($postdata->params->audit, ',');
        $otherreason = trim($postdata->params->otherreason);
        $status = $postdata->params->status;
        $reasoncontent = '';

        if($status == 3){
            if(!($status && $appid) || (empty($reason) && empty($otherreason))){
                ajaxReturn('参数错误.', 0, 0);
            }
            else{
                $reasontext = $this->SystemConfigModel->getSystemConfig('reason_for_apprejection');
                $tmpreason = explode(',', $reason);
                if(is_array($tmpreason))
                {
                    foreach ($tmpreason as $v)
                    {
                        foreach ($reasontext as $kt=>$vt)
                        {
                            if($v == $kt)
                            {
                                $reasoncontent .= $vt."<br>";
                            }
                        }
                    }
                }
                else
                {
                    foreach ($reasontext as $kt=>$vt)
                    {
                        if($tmpreason == $kt)
                        {
                            $reasoncontent = $vt."<br>";
                            break;
                        }
                    }
                }
                if(!empty($otherreason))
                {
                    $reasoncontent .= $otherreason."<br>";
                }
            }
        }else{
            if(!($status && $appid)){
                ajaxReturn('参数错误', 0, 0);
            }
            else{
                $reasoncontent=$reason;
            }
        }
        $this->load->model('AppInfoModel');
        $data = array("auditstatus"=>$status, 'auditmemo'=>$reasoncontent, 'auditime'=>time());
        if($data['auditstatus'] == 2){
            $data['auditing'] = 1;
        }else{
            $data['auditing'] = 0;
        }
        $status = $this->AppInfoModel->edit(array('appid'=>$appid), $data);
        if($status){
            ajaxReturn('审核结果保存成功', 1, print_r($reason,true));
        }
        ajaxReturn('审核结果保存失败', 0, 0);
    }
}
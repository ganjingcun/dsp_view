<?php

class Captcha extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->cache();
	}
	
	function index($width,$height,$code_num)
	{
		$this->load->library('validate_code',array('width'=>$width,'height'=>$height,'code_num'=>$code_num));
		$yanzhengma = $this->validate_code->get_validate_code();
		$this->session->set_userdata(array('validate_code'=>strtoupper($yanzhengma)));
		
		$this->validate_code->out_img();
	}
	
	private function cache()
	{
		if($this->get_config('ciccms/site','web_cache'))
		{
			$this->output->cache($this->get_config('ciccms/site','web_cache_time'));
		}
	}
}
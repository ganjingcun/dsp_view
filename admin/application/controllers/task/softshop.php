<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
define('ANZHUOSHANGDIAN_IMG_HOST', 'http://imgs.anzhuoshangdian.com/');
class SoftShop extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('AdMgInfoModel');
    }

    /**
     * 初始页列表
     */
    function index(){
        $appTypeJson = curlGet("http://api.anzhuoshangdian.com/Pushapply/applyCategory/");
        if(empty($appTypeList))
        {

        }
        $appTypeData = json_decode($appTypeJson, true);
        if(empty($appTypeData))
        {
             
        }
        $appTypeList = array();
        $str = null;
        foreach($appTypeData as $v)
        {
            //$appTypeList[$v['cid']] = $v['cname'];
            $appInfoJson = curlGet("http://api.anzhuoshangdian.com/Pushapply/getUpdateApply/?cateid={$v['cid']}&uptime={time()-7200}&limit=100");
            if(empty($appInfoJson))
            {
                continue;
            }
            $appInfoData = json_decode($appInfoJson, true);
            if(empty($appInfoData))
            {
                continue;
            }
            $num = 0;
            $numup = 0;
            foreach($appInfoData as $data)
            {
                //暂时屏蔽cocosplay项目
                if($data['isplay'])
                {
                    continue;
                }
                $appinfo = array();
                $appinfo['storeid'] = $data['appid'];
                $appinfo['appname'] = $data['name'];
                $appinfo['icon'] = ANZHUOSHANGDIAN_IMG_HOST.$data['icon'];
                $appinfo['size'] = $data['size'];
                $appinfo['apptype'] = $v['cname'];
                if(!empty($data['thumbs']))
                {
                    $tmpthumbs = explode(',', $data['thumbs']);
                    foreach($tmpthumbs as $kt => $vt)
                    {
                        $tmpthumbs[$kt] = ANZHUOSHANGDIAN_IMG_HOST.$vt;
                    }
                    $appinfo['imgpath'] = json_encode($tmpthumbs);
                }
                $appinfo['appdesc'] = $data['intro'];
                $appinfo['downloadurl'] = $data['url'];
                $appinfo['versioncode'] = $data['ver'];
                $appinfo['versionname'] = $data['version_code'];
                $appinfo['packagename'] = $data['packname'];
                $appinfo['num'] = $data['num'];
                $appinfo['price'] = 0;
                $appinfo['ostype'] = 3;
                $appinfo['oldprice'] = 0;
                //$appinfo['releasenotes'] = $data['info']['releasenotes'];
                $appinfo['wangyitype'] = $v['cid'];
                $appinfo['updatetime'] = $data['update_time'];
                $chkInfo = $this->AdMgInfoModel->getRow(array('storeid' => $appinfo['storeid'], 'ostype' => 3));
                if($chkInfo)
                {
                    if($chkInfo['updatetime'] != $data['update_time'] || $chkInfo['icon'] != ANZHUOSHANGDIAN_IMG_HOST.$data['icon'])
                    {
                        $this->AdMgInfoModel->edit(array('storeid'=>$data['appid'], 'ostype' => 3), $appinfo);
                        $numup++;
                    }
                    continue;
                }
                $this->AdMgInfoModel->add($appinfo);
                $num++;
            }
            $str .= "<br> {$v['cname']}:新增 {$num} 条数据，更新 {$numup} 条数据。";
        }
        echo $str;
        echo "<br>更新完成。";
    }

/**
	 * 更新media_rule的字段domainfiltertype,仅执行一次，若字段areaset为空，则将domainfiltertype字段置为0
	 */
	function updateDomainfiltertype(){
		$this->load->model("MediaRuleModel");
		$mediaRuleList=$this->MediaRuleModel->getList(array(),'*');
		if(count($mediaRuleList)>0){
			$i=0;
			foreach ($mediaRuleList as $mediarule){
				if ($mediarule['areaset']== null || $mediarule['areaset']=="") {
					$data=array('domainfiltertype'=>0,'id'=>$mediarule['id']);
					$result=$this->MediaRuleModel->editDomainFilterType($data);
					if($result['status']==0){
						echo $mediarule['id']." 失败<br/>";
					}else{
						echo $mediarule['id']."成功<br/>";
						$i++;
					}
				}
				else{
					continue;
				}
			}
			echo "<br/><br/><br/>共".$i."条成功";
		}
		else{
			echo "无数据";
		}
	}
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ClickRate extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('report/ClickRateClass');
        $this->load->model('AppStuffRatioModel');
        $this->load->model('AppInfoModel');
        $this->load->model('AppChannelModel');
        $this->load->model('SystemConfigModel');
        $this->load->model('AdIncomePaidModel');
        $this->load->model('AdMonthIncomeModel');
        $this->load->model('UserMemberModel');
        $this->load->model('AdAccountinfoModel');
    }

    /**
     * 初始页列表
     */
    function index(){
        $this->updateRate();
        echo '更新完成。';
    }

    /**
     * banner
     */
    private function updateRate()
    {
        $this->AppStuffRatioModel->delete(array('updatetime'=>array('LT',time()-86400)));
        $bReport = new ClickRateClass();
        $report = $bReport->getDataAdReport();
        foreach($report as $k=>$v)
        {
            foreach($v as $kt=>$vt)
            {
                if($kt == 0)
                {
                    $g = '';
                    if(isset($vt['ctr']) && ($vt['ctr'] < 100 && $vt['ctr'] > 0.01))
                    {
                        $g['ctr'] = $vt['ctr'];
                    }
                    if(isset($vt['atr']) && ($vt['atr'] < 100 && $vt['atr'] > 0.01))
                    {
                        $g['atr'] = $vt['atr'];
                    }
                    if(empty($g))
                    {
                        continue;
                    }
                    $this->AppInfoModel->edit(array('appid'=>$k), $g);
                    $this->AppChannelModel->edit(array('appid'=>$k), $g);
                }
                else
                {
                    $s = '';
                    if(isset($vt['ctr']) && ($vt['ctr'] < 100 && $vt['ctr'] > 0.01))
                    {
                        $s['ctr'] = $vt['ctr'];
                    }
                    else
                    {
                        $s['ctr'] = 0;
                    }
                    if(isset($vt['atr']) && ($vt['atr'] < 100 && $vt['atr'] > 0.01))
                    {
                        $s['atr'] = $vt['atr'];
                    }
                    else
                    {
                        $s['atr'] = 0;
                    }
                    $this->AppStuffRatioModel->update(array('appid'=>$k, 'stuffid'=>$kt, 'ctr'=>$s['ctr'], 'atr'=>$s['atr']));
                }
            }
        }
        //$this->SystemConfigModel->update(array('skey'=>'banner_click_rate'), array('sval' => $bannerSumReport['clickrate']));
    }
    /**
     * 这里获取广告素材内容 微商店  不是定时任务 是接口
     * @return string
     */
    function getWSDData()
    {
        $stuffid = $this->input->get('stuffid');
        if(empty($stuffid))
        {
            return '';
        }
        $this->load->model('AdStuffModel');
        $data = $this->AdStuffModel->getSendData($stuffid);
        if(empty($data))
        {
            return '';
        }
        echo json_encode($data);
    }
    /**
     * 个人开发者 16 生成 提款单
     */
    function create_paid(){
        $today = date("d");
        if($today != 16) exit('no permission');
        $last_month = date("Y-m",mktime(0, 0 , 0,date("m")-1,1,date("Y")));
        $where['month'] =  $last_month;
        $where ['paidid'] = 0;
        $income_list = $this->AdMonthIncomeModel->getlist($where);
        foreach($income_list as $key=>$val){
            $userid = $val['userid'];
            $userinfo = $this->UserMemberModel->getRow(array('userid'=>$userid));
            $accounttype = $userinfo['accounttype'];
            if($accounttype ==1){
                $data['userid'] = $userid;
                $data['username'] = $userinfo['username'];     //账号
                $data['incomekey'] =  substr(md5($userinfo['username']),0,2).time().rand(10000,99999);
                $data['sumcincome'] = $val['cincome'];
                $data['userratio']  = $val['userratio'];
                $data['taxmoney'] = $val['taxmoney'];
                $data['aftertaxmoney'] = $val['aftertaxmoney'];
                //结算对应的 收入报表id
                $data['addtime'] = time();
                $data['cashmonth'] = $last_month;
                $data['paidmonth'] = date("Y-m");
                $paidid = $this->AdIncomePaidModel->add($data);
                if($paidid){
                    $income_id = $val['id'];
                    $this->AdMonthIncomeModel->edit(array('id'=>$income_id),array('paidid'=>$paidid,'status'=>1));
                }
            }
        }
        exit('ok');
    }
}

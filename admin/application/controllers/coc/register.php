<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->init();
	}
	
	function init()
	{
		$this->load->library(array('admin/position','admin/message'));
	}

	function index(){
		$this->load->display('caogen_pro/index.html');
	}
	
	function cocSignUp()
	{
		$postdata = $this->input->post();
		if(empty($postdata)) error_log('empty');
		$chk = $this->chkRegister($postdata);
		if($chk['status'] == 0){
			return $chk;
		}
		$data['name'] = trim($postdata['name']);
		$data['gender'] = $postdata['gender'];
		$data['age'] = trim($postdata['age']);
		$data['address'] = trim($postdata['address']);
		$data['contact'] = trim($postdata['contact']);
		$data['homepage1'] = isset($postdata['homepage1'])?trim($postdata['homepage1']):"";
		$data['homepage2'] = isset($postdata['homepage2'])?trim($postdata['homepage2']):"";
		$data['homepage3'] = isset($postdata['homepage3'])?trim($postdata['homepage3']):"";
		$data['homepage4'] = isset($postdata['homepage4'])?trim($postdata['homepage4']):"";
		$data['homepage5'] = isset($postdata['homepage5'])?trim($postdata['homepage5']):"";
		$data['createtime'] = time();
		$this->load->model('coc/CocUserModel');
		$userid = $this->CocUserModel->add($data);
        if($userid)
        {
        	return array('status' => 1, 'userid' => $userid);
        }
        return array('status' => 0, 'data' => '', 'info'=>'');
	}
	
	function chkRegister($params){
		$error = array('status' => 1, 'data' => '', 'info'=>'');
		if(empty($params['name']))
        {
        	$error['status'] = 0;
            $error['data'] = array('namemsg', '请填写姓名');
            $error['info'] = 'nameerr';
            return $error;
        }
		if(empty($params['age']))
        {
        	$error['status'] = 0;
            $error['data'] = array('agemsg', '请填写年龄');
            $error['info'] = 'ageerr';
            return $error;
        }
        return $error;
	}
}
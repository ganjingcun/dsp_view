<?php
class MyHooks
{
	public function afterAction()
	{
		if(defined('SQL_DEBUG') and SQL_DEBUG==true)
		{
			$log = SQLLog::read();
			$len = count($log);
			echo "<p>总共执行了 ".Timer::getQqueryCount()." 条查询，总耗时：".sprintf('%.8f',Timer::getTotalTime()).'秒</p>';
			for($i=0;$i<$len;$i++)
			{
				$timer = $log[$i];
				$timer->getStartTime();
				echo "<p>".$timer->getSql()." ------ ".sprintf('%.8f',$timer->getTimeSpent())."秒</p>";
			}
		}
	}
}
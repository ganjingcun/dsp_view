/*
 * 	Character Count Plugin - jQuery plugin
 * 	Dynamic character count for text areas and input fields
 *	written by Alen Grakalic	
 *	http://cssglobe.com/post/7161/jquery-plugin-simplest-twitterlike-dynamic-character-count-for-textareas
 *
 *	Copyright (c) 2009 Alen Grakalic (http://cssglobe.com)
 *	Dual licensed under the MIT (MIT-LICENSE.txt)
 *	and GPL (GPL-LICENSE.txt) licenses.
 *
 *	Built for jQuery library
 *	http://jquery.com
 *
 *  REEDIT BY YANGSEN
 *
 */
 
(function($) {
	
	function getStrleng(str,maxstrlen) {
		if(maxstrlen==undefined) {
			maxstrlen = 140;
		}
        myLen = 0; 
        i = 0; 
        for (; (i < str.length) && (myLen <= maxstrlen * 2); i++) { 
            if (str.charCodeAt(i) > 0 && str.charCodeAt(i) < 128) 
                myLen+=0.5; 
            else 
                myLen += 1; 
        } 
        return myLen; 
    }
	
	$.fn.charCount = function(options){
	  
		// default configuration properties
		var defaults = {	
			allowed: 250,		
			warning: 25,
			css: 'counter',
			counterElement: 'span',
			cssWarning: 'warning',
			cssExceeded: 'exceeded',
			counterText: ''
		}; 
			
		var options = $.extend(defaults, options); 
		
		function calculate(obj){
			var __tmptxt = $(obj).val();
			var count = getStrleng(__tmptxt,__tmptxt.length);
			count = parseInt(count);
			var available = options.allowed - count;
			return {'current':count,'sx':available,'allowed':options.allowed}
		};
		
		function calcIt(obj,tishiEle) {
			var result = calculate(obj);
			//console&& console.log(result);
			
			if(result.sx <= options.warning && result.sx >= 0){
				$(tishiEle).addClass(options.cssWarning);
			} else {
				$(tishiEle).removeClass(options.cssWarning);
			}
			if(result.sx < 0){
				$(tishiEle).addClass(options.cssExceeded);
			} else {
				$(tishiEle).removeClass(options.cssExceeded);
			}
			//console && console.log($(tishiEle));
			
			$(tishiEle).html(result.current);
		}
		
		
				
		this.each(function() {  			
			if($(options.counterElement).size()>0) {
				$(options.counterElement).val(options.counterText);
			} else {
				$(this).after('<'+ options.counterElement +' class="' + options.css + '">'+ options.counterText +'</'+ options.counterElement +'>');
			}
			calcIt(this,options.counterElement);
			$(this).keyup(function(){calcIt(this,options.counterElement)});
			$(this).change(function(){calcIt(this,options.counterElement)});
		});
	  
	};

})(jQuery);

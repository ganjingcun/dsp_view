/*本js文件用于页面交互效果的初始化
  放置于页面底部
  依赖：jquery.js
*/
/*subject/index*/
/*<div id="toolTip" style="position:absolute"><img src="/public/common/images/no_pic.gif"/></div>
<script type="text/javascript" src="/public/js/page.js"></script>*/
$(".image_link a").hover(
	function(e){
		var _src = $(this).attr("name"),_pos = $(this).position(),_size = $(this).attr("data-size")||'auto';
		if(_src)$("#toolTip").find("img").width(_size).attr("src",_src);
		$("#toolTip").css({"top":_pos.top+20,"left":_pos.left-250}).show();
	},
	function(e){
		$("#toolTip").hide();
	}
);
$("#toolTip img").error(function(){
	$(this).attr("src","/public/common/images/no_pic.gif");
});
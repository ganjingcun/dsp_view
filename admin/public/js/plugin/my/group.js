function Group() {
	this.userid = 0;
	this.obj_group_pop = $('#fc-changegroup-id');
	this.obj_group_box = this.obj_group_pop.find('#groupchoice');
	this.source_group_list = {};
	this.source_group2_list = {};
	this.data_list = this.source_group_list;

	this.current_user_arrGroupid = new Array();
}

Group.prototype.fill = function() {

	var str = '<ul id="group_list" class="group_list" style=" margin-left:20px; width:450px;">';
	if(this.data_list.length==1){
		this.obj_group_box.parents('form:eq(0)').find('input[name=grouptype]:eq(1)').attr('disabled',true);
	}
	else
	{
		for ( var i in this.data_list) {
			if (this.data_list[i]['grouptype'] != 1) {
				str += '<li><input id="" type="checkbox" name="groupid[]" value="' + this.data_list[i]['groupid'] + '" ';
	
				if ($.inArray(this.data_list[i]['groupid'],
						this.current_user_arrGroupid) != -1) {
					str += " checked ";
				}
				str += '/> <label for="c1">' + this.data_list[i]['groupname'] + '</label></li>';
			}
		}
	}
	str += '</ul>';
	this.obj_group_box.html(str);
}

Group.prototype.getUserGroupId = function() {
	var g = this.source_group2_list[this.userid];
	this.current_user_arrGroupid = new Array();
	for ( var i in g) {
		this.current_user_arrGroupid.push(g[i]['groupid']);
	}
	//alert(this.current_user_arrGroupid);
}

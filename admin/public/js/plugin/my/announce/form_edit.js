//文本编辑器
/*KindEditor.ready(function(K) {
	var editor1 = K.create('textarea[name="content1"]', {
		cssPath : '/public/admin/css/prettify.css',
		uploadJson : '/singleSDKmanage/upload_json.php',
		fileManagerJson : '/singleSDKmanage/file_manager_json.php',
		allowFileManager : true,
		items:['textcolor', 'bgcolor', 'bold',
		       'italic', 'underline', 'strikethrough'],
		afterCreate : function() {
		},
		afterChange:function() {
			this.sync();
			var c = this.count();
			if(c>250) {
				c='<font color="red">'+c+'</font>';
			}
			K('#editor_counter').html(c);
		},htmlTags:{}
	});
	//prettyPrint();
});*/

$(function(){
	$("#textarea_content").charCount({"allowed":250,"counterElement":$("#editor_counter")});
	
    //$('#public_time').datetimepicker({});
	 $(".calltype").each(function(i,e) {
	    	var _this = $(e);
	    	_this.bind('change',function() {
	    		 $(".callurl").attr('disabled',true);
	    		 $(_this).next(".callurl").removeAttr('disabled');
	    	});
	    	
	 });
    //渠道全选
    $("#chk_channelall").change(function() {
    	var v = $(this).is(":checked");
    	$("#myUl").find(":checkbox").each(function() {
    		$(this).prop("checked",v);
    	});
    });
    
    /**
     * 渠道channelcheck检测
     */
    $(".cls_publishes").change(function() {
    	var _optlist = $(".cls_publishes");
    	var result = true;
    	_optlist.each(function() {
    		result = $(this).is(":checked")&result;
    	}); 
    	
    	if(result ==1 ) {
    		$("#chk_channelall").prop("checked",true);
    	} else {
    		$("#chk_channelall").prop("checked",false);
    	}
    	
    });
    
    /**
     * 点击删除按钮的事件处理
     */
    function fn_delpic_btn() {
    	$(this).remove();
    	$('#delpic').val(1);$('#yulantu').attr('src','');
    	$("#thumb").val('');
    	$('#span_showstyle').hide();
    	$('#hid_showstyle').val(3);
    }
    
    
    $("#thumb").change(function() {
    	var _this = $(this);
    	var thisval = $(this).val();
    	if(thisval=='') {
    		$('#span_showstyle').hide();
    		$('#hid_showstyle').val(3);
    	} else {
    		$('#span_showstyle').show();
    		$('#hid_showstyle').val(2);
    	}
    	
    	
    	extension = thisval.substr(thisval.lastIndexOf("\."))||'';
    	if(thisval!='' && -1==$.inArray(extension.toLowerCase(),['.png','.jpg','.jpeg'])){
    		alert("只允许传jpg、png类型图片");
    		$("#thumb").val('');
    		return false;
    	}
    	
    	
    	
    	if(typeof(FileReader)!=='undefined') {
    		var reader = new FileReader();
    		console && console.log(_this.get(0).files[0]);
    		if(thumb.files[0]!=undefined) {
	            reader.readAsDataURL(_this.get(0).files[0]); 
	            reader.onload = function(e){ 
	                $("#yulantu").attr("src",this.result); 
	            } 
    		}
    	}
    	
		if($('#evt_delpic').size()==0) {
			$('#delpic').after('<a href="javascript:void(0)" id="evt_delpic" style="cursor:pointer;">删除</a>');
			$('#evt_delpic').unbind('click');
			$('#evt_delpic').bind('click',fn_delpic_btn);
		}
    	
    	if($("#thumb").val()=='') {
    		$("#evt_delpic").remove();
        	$('#delpic').val(1);$('#yulantu').attr('src',$('#yulantu').attr('data-origin-src'));
        	$("#thumb").val('');
        	$('#span_showstyle').hide();
        	$('#hid_showstyle').val(3);
    	}
    	$("#delpic").val(0);
    });
    
    //document load event
    $('#evt_delpic').unbind('click');
	$('#evt_delpic').bind('click',fn_delpic_btn);
    
    //仅显示图片处理
    $("#chk_showstyle").change(function() {
     	 var _this = $(this);
     	 if(_this.is(":checked")) {
     		 $("#hid_showstyle").val(1);
     	 } else {
     		 $("#hid_showstyle").val(2);
     	 }
    });
    
    
    $("#form_publisher").ajaxForm({
    	"beforeSubmit" : function(event) {
    		e = event;
    		if($(":checkbox[name='publisher[]']")==null || $(":checkbox[name='publisher[]']:checked").size()==0){
            	if($(":checkbox[name='publisher']").val()==undefined) {
            	 alert("必须选择发布公告的渠道");
            	 return false;
            	}
        	}
        	$("#btn_submit").prop("disabled",true);
    	},
    	"success":function(data) {
    		data = $.parseJSON(data);
    		alert(data['info']);
    		if(data['status']==1) {
    			var gotourl = data['data']['gotourl'];
    			if(gotourl.lastIndexOf('?')>0) {
    				gotourl+='&';
    			} else {
    				gotourl+='?';
    			}
    			gotourl+='r='+Math.random();
    			location.href=gotourl;
    		} else {
    			$("#btn_submit").prop("disabled",false);
    		}
    	},
    	"timeout": 30000, //30 seconds
    	"error":function(xhr, status, error) {
    		$("#btn_submit").prop("disabled",false);
    		if(status=='timeout') {
    			alert('网速过慢，请稍后再试吧');
    		}
		}
    });
    
});
/**
 * APP Manage Page Js .
 * @author yangsen
 */

    $(function(){
        $(".on_off").on('click',function(){
            var appid = $(this).attr('data-log');
            var onandoff = $(this).hasClass("off") ? 1 : 0;
            var obj = $(this);
            var announceUrl = '/admin/singleSDKmanage/changeCCplayAnnounce';
            var giftSwitchUrl = '/admin/singleSDKmanage/changeGiftInCCPlay';
            var ccplaySwitchUrl = '/admin/singleSDKmanage/switchCCplay';
            var kefuSwitchUrl = '/admin/singleSDKmanage/switchCCplayKefu';
            var gonglueSwitchUrl = '/admin/singleSDKmanage/switchCCplayGonglue';
            var  autopopSwitchUrl = '/admin/singleSDKmanage/switchCCPlayAutopop';
            
            
            var rqUrl = '';
            var dat = { "appid": appid};
            if(obj.hasClass('evt_gift_switcher')) {
            	rqUrl = giftSwitchUrl;
            	dat['is_ccplay_gift']=onandoff;
            } else if(obj.hasClass('evt_announce_switcher')) {
            	rqUrl = announceUrl;
            	dat['is_ccplay_announce']=onandoff;
            } else if(obj.hasClass('evt_ccplay_switcher')) {
            	rqUrl = ccplaySwitchUrl;
            	dat['is_ccplay']=onandoff;
            } else if(obj.hasClass('evt_kefu_switcher')) {
            	rqUrl = kefuSwitchUrl;
            	dat['is_ccplay_kefu'] =onandoff;
            }else if(obj.hasClass('evt_gonglue_switcher')) {
            	rqUrl = gonglueSwitchUrl;
            	dat['is_ccplay_gonglue'] =onandoff;
            }else if(obj.hasClass('evt_autopop_switcher')) {
            	rqUrl = autopopSwitchUrl;
            	dat['is_ccplay_autopop']=onandoff;
            }
            
            if(rqUrl==='') {
            	return false;
            }
            $.get(rqUrl, dat, function(data){
                if(data && data['status'] == 1){
                    if(onandoff){
                        obj.removeClass("off");
                        obj.addClass("on");
                    }else{
                        obj.removeClass("on");
                        obj.addClass("off");
                    }
                }else{
                    if(data && data['info']){
                        alert(data['info']);
                    }else{
                        alert('开关修改失败');
                    }
                }
            }, 'json');
            return false;
        });
    });

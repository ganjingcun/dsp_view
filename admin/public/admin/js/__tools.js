/**
 * 微博字数计算
 */
function getStrleng(str,maxstrlen) {
		if(maxstrlen==undefined) {
			maxstrlen = 140;
		}
        myLen = 0; 
        i = 0; 
        for (; (i < str.length) && (myLen <= maxstrlen * 2); i++) { 
            if (str.charCodeAt(i) > 0 && str.charCodeAt(i) < 128) 
                myLen+=0.5; 
            else 
                myLen += 1; 
        } 
        return myLen; 
}
weiboLen = getStrleng;
	/*开关定向：area以及way特殊对象
	  如果
	*/
	var conditionObj = function(db,sda){
		var DB = this.DB= db;
		var SDA = this.SDA = sda;
		var curV = this.curV = "area";
		function conditionSwitch(elem){
			curV = $("option",elem).eq(elem.selectedIndex).attr("data-v");
			// $("#condition_con .form_input").hide();
			renderSel(curV);
			// $("#form_"+curV).show();
		}
		function isInSel(val,arr){
			return $.inArray(val,arr);
		}
		function valInDB(name,db,key,label){
			var res;
			if(db){
				switch(label){
					case 'area':
					res =valInArrDB(name,db,key);break;
					case 'way':
					res =valInObjDB(name,db,key);break;
					case 'ver':
					res =valInArrDB(name,db,key);break;
				}
			}
			return res;
		}
		/*验证指定name是否存在数据中,数据结构为object*/
		function valInObjDB(name,obj,key){
			var res = false;
			for(var j in obj){
				for(var i=0;i<obj[j].length;i++){
					if(obj[j][i][key] == name){
						res = obj[j][i];
						break;
					}
				}
				if (res)break;	
			}
			return res;
		}
		/*验证指定name是否存在数据中，数据结构为Array*/
		function valInArrDB(name,arr,key){
			var res = false;
			for(var i=0;i<arr.length;i++){
				if(arr[i][key] == name){
					res = arr[i];
					break;
				}
			}
			return res;
		}
		function renderSel(key){
			var _db = DB[key];
			if(_db){
				switch(key){
					case 'area':
						renderSelArr(key);
					break;
					case 'way':
						renderSelObj(key);
					break;
					case 'ver':
						renderSelArr(key);
					break;
				}
			}
		}
		//可选择区域的模板函数,数据结构为object
		function renderSelObj(key){
			var str="";
			var _db = DB[key];
			function filter(str){
				switch(str){
					case 'all':str="";break;
					case 'num':str="";break;
					default:;
				}
				return str;
			}
			if(_db){
				for(var j in _db){
					if(filter(j)!="") str+="<span class='cat'>"+filter(j)+" </span>" ;
					for(var i=0;i<_db[j].length;i++){
						//如果选中了0，表示全部选择
						if(isInSel(_db[j][i].val,SDA[key]) != -1){
							str+='<label class="selected" data-v="'+_db[j][i].val+'"><input type="checkbox" checked="checked"/>'+_db[j][i].name+'</label> ';
						}else{
							str+='<label data-v="'+_db[j][i].val+'"><input type="checkbox"/>'+_db[j][i].name+'</label> ';
						}
						
					}
				}
			}
			$("#form_"+key).html(str);
			//return str;
		}
		//可选择区域的模板函数,数据结构为Array
		function renderSelArr(key){
			var str="";
			var _db = DB[key];
			if(_db){
				// console.log(_db.length);
				for(var i=0;i<_db.length;i++){
					//如果选中了0，表示全部选择
					if(isInSel(_db[i].val,SDA[key]) != -1){
						str+='<label class="selected" data-v="'+_db[i].val+'"><input type="checkbox" checked="checked"/>'+_db[i].name+'</label> ';
					}else{
						str+='<label data-v="'+_db[i].val+'"><input type="checkbox"/>'+_db[i].name+'</label> ';
					}
					
				}
			}
			$("#form_"+key).html(str);
			//return str;
		}
		/*结果区域的模板函数*/
		function renderShowL(v){
			var str="";
			var realV=v||curV;
			var _db = SDA[realV];
			if(_db){
				for(var i=0;i<_db.length;i++){
					var iobj = valInDB(_db[i],DB[realV],'val',realV);
					str+='<span class="item" data-v="'+iobj.val+'">'+iobj.name+'<em class="close">x</em></span> ';
				}
			}
			//$("#condition_"+realV+"_list .item").remove();
			$("#condition_"+realV+"_list .list").html(str);
			if(realV=="ver"){
				$("#input_hidden_"+realV).val(SDA[realV].join("||"));
			}else{
				$("#input_hidden_"+realV).val(SDA[realV].join(","));
			};
		}

		/*取消 选择*/
		function cancelShowL(v){
			var str="",str2="";
			var realV=v||curV;
			var _db = tmpFDA[realV];
			if(_db){
				for(var i=0;i<_db.length;i++){
					var iobj = valInDB(_db[i],DB[realV],'val',realV);
					str+='<span class="item" data-v="'+iobj.val+'">'+iobj.name+'<em class="close">x</em></span> ';
					str2+='<span class="item" data-v="'+iobj.val+'">'+iobj.name+'</span> ';
				}
			}
			$("#condition_"+realV+"_list .list").html(str);
			$("#cannoteidt_"+realV+"_list .list").html(str2);
			if(realV=="ver"){
				$("#input_hidden_"+realV).val(tmpFDA[realV].join("||"));
			}else{
				$("#input_hidden_"+realV).val(tmpFDA[realV].join(","));
			};
		}
		
		/*不可编辑区域的模板函数*/
		function contEditRenderShowL(v){
			var str="";
			var realV=v||curV;
			var _db = SDA[realV];
			if(_db){
				for(var i=0;i<_db.length;i++){
					var iobj = valInDB(_db[i],DB[realV],'val',realV);
					str+='<span class="item" data-v="'+iobj.val+'">'+iobj.name+'</span> ';
				}
			}
			$("#cannoteidt_"+realV+"_list .list").html(str);
		};
		
		
		/**/
		function removeCon(dir,val){
			var _arr = SDA[dir];
			//var _val = valInDB(val,DB[dir],'name');
			if($.inArray(val,SDA)){
				SDA[dir] = $.grep(_arr,function(value){return value != val;})
			}
			renderShowL(dir);
			contEditRenderShowL(dir)
			renderSel(dir);
		}

		function addCon(dir,val){
			if(!SDA[dir])SDA[dir]=[];
			//$.inArray(val,SDA[dir]) 判断是否存在
			//$.inArray("0",SDA[dir]) "全部"是否选中
			if($.inArray(val,SDA[dir])!=-1 || $.inArray("0",SDA[dir])!=-1){
				return false;
			}else{
				var allselect = $("#form_"+dir);
				if(val == "0"){ 
					SDA[dir]=["0"]
				}else{ 
					SDA[dir].push(val); 
					var l = getlength(DB[dir]);
					if(l <= SDA[dir].length+1){
						SDA[dir]=["0"];
					};
				}
			};
			// console.log(tmpFDA[dir]);
			renderShowL(dir);
			contEditRenderShowL(dir);
			renderSel(dir);
			return true;
		}

		function getlength(arr){
			var l=0;
			if(arr.length && typeof arr =="object"){
				l += arr.length
			}else{
				for (var i in arr) {
					l += getlength(arr[i]);
				};
			};
			return l;
		};

		$("#switchSel").change(function(e){
			conditionSwitch(this);
		});

		//选择区域 点击事件
		$("#form_area label").live("click",function(){
			var _val = $(this).attr("data-v");
			if(addCon("area",_val)){ 
				$(this).addClass("selected") 
			}else{
				removeCon("area",_val);
				$(this).removeClass("selected");
			}
			return false;
		});
		$("#form_way label").live("click",function(){
			var _val = $(this).attr("data-v");
			if(addCon("way",_val)){ 
				$(this).addClass("selected") 
			}else{
				removeCon("way",_val);
				$(this).removeClass("selected");
			}
			return false;
		});

		$("#form_ver label").live("click",function(){
			var _val = $(this).attr("data-v");
			if(addCon("ver",_val)){ 
				$(this).addClass("selected");
			}else{
				removeCon("ver",_val);
				$(this).removeClass("selected");
			}
			return false;
		});

		//结果区域 点击事件
		$("#condition_way_list .item").live("click",function(){
			removeCon('way',$(this).attr("data-v"));
			renderSel('way');
		});
		$("#condition_area_list .item").live("click",function(){
			removeCon('area',$(this).attr("data-v"));
			renderSel('area');
		});
		$("#condition_ver_list .item").live("click",function(){
			removeCon('ver',$(this).attr("data-v"));
			renderSel('ver');
		});
		

		//保存按钮
		$("#button_save").click(function(){
			$("#uneidt_con").show();
			$("#eidt_con").hide();
		});

		//取消按钮
		$("#directional_reset").click(function(){
			$("#uneidt_con").show();
			$("#eidt_con").hide();
			cancel();
		});

		//编辑按钮
		$("#button_edit").click(function(){

			$("#uneidt_con").hide();
			$("#eidt_con").show();
		});
		
		//初始化
		var init = this.init = function(){
			for(var i in SDA){
				renderShowL(i);
				renderSel(i)
				contEditRenderShowL(i);
			}
		};
		//重置
		var reset = this.reset = function(){
			for(var i in SDA){
				SDA[i] = [];
				renderShowL(i);
				renderSel(i);
				contEditRenderShowL(i);
			}
		}
		//取消
		var cancel = this.cancel = function(){
			for(var i in tmpFDA){
				var arr = [].concat(tmpFDA[i]);
				SDA[i] = arr;
				cancelShowL(i);
				renderSel(i);
			}
		}
	};
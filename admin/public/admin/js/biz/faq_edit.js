/**
 * FAQ 编辑页面的带代码
 */
  $(function() {
	  $('#form_editFAQ').ajaxForm({
		  'beforeSubmit':function() {
			  var q_len = $("#editor_counter_q").text();
			  var a_len = $("#editor_counter_a").text();
			  
			  if(q_len==0) {
				  alert('FAQ标题不能为空');
				  return false;
			  }
             if(a_len==0) {
                  alert('FAQ答案不能为空');
                  return false;
              }
			  
			  
			  if(q_len>100) {
				  alert('问题字数长度最大100个汉字');
				  $("#editor_counter_q").focus();
				  return false;
			  }
			  if(a_len>100) {
				  alert('答案长度最大100个汉字');
				  $("#editor_counter_a").focus();
				  return false;
			  }
		  },
		  'success':function(data) {
			  data = $.parseJSON(data);
			  alert(data.info);
			  if(data['status']>-1) {
				  if(document.referrer!=null) {
					  location.href = document.referrer;
				  } else {
					  history.back();
				  } 
			  }
		  }
	  });
	  $("#q").charCount({ "allowed":100,"counterElement":$("#editor_counter_q") });
	  $("#a").charCount({ "allowed":100,"counterElement":$("#editor_counter_a") });
  });
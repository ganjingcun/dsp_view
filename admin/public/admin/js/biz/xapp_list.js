
function getpackinfo(thi){
	var target = $("#download_url").val();
	
	var _form =$("#xapp_editform"); 
	var packagename = $('#help_packagename');
	var appversionname = $('#help_appversionname');
	var appversioncode = $('#help_appversioncode');
	packagename.attr("readonly", false).val('');
	appversionname.attr("readonly", false).val('');
	appversioncode.attr("readonly", false).val('');
    $(thi).button('loading');
    $("#box_tishi").text('');
    $.ajax({
		"url":"/admin/singleSDKmanage/ajaxgetappinfo",
		"type":"post",
		//"contentType": "application/json", 
		"data":{ "target": target, "r": Math.random() },
		 "timeout": 30000, //30 seconds
		 "dataType": "json",
		 "success":function(data) {
			 $(thi).button('reset');
		        $(thi).next().remove();
				if(data && data['status'] == 0){
					
					$("#box_tishi").text(data.data[1]);
				}else if(data['status']==-1) {
					$("#box_tishi").text(data['info']);
				} else {
					packagename.val(data.data.packagename).attr("readonly", true);
		    		appversionname.val(data.data.appversionname).attr("readonly", true);
		    		appversioncode.val(data.data.appversioncode).attr("readonly", true);
				} 
		 },
		 "error": function(xhr, status, errorThrown){
			     if(status=='timeout') {
			    	 $("#box_tishi").text('获取app包信息超时，或包解析错误了');
			     } else {
			    	 $("#box_tishi").text('包经过加密无法解析');
			     }

			     $(thi).button('reset');
		 }
	});
}

//insert intp table 
/*
 * 'appid' => $appid,
    'publisherID' => $publisher,
    'adgameid' => $gameid,
    'campaignid' => $campaign_id,
    'download' => $downads,
    'adcontent' => $adcontent,
    'gametype' => '是',
    'packagename' => $packagename,
    'appversionname' => $appversionname,
    'appversioncode' => $appversioncode,
    'position' => 99,
    'time' => $time
 */
$(function() {
	$("#form_appmod").ajaxForm({
	     "beforeSubmit":function() {
	         if($("#sel_adgameid").val()=='0') {
	                alert('请选择正确的游戏');
	                return false;
	          }
	         if($.trim($("#download_url").val())=='') {
	        	 alert('下载地址必须填啊');
	        	 return false;
	         }
	     },
	     "success":function(data) {
	         
	    	 data = $.parseJSON(data);
	    	 //console&&console.log(data['status']);
	    	 if(data['status']=='-1') {
	            	alert(data['info']);
	            	return ;
	         }
	         data = data['data'];
	         var hid_xapp_appid = $("#hid_xapp_appid").val();
	         	 var gamename = $("#sel_adgameid option:selected").text();
		         if(hid_xapp_appid!='') {
		        	 var oldline = $("#xappTable tr[data-id='"+hid_xapp_appid+"']");
		        	 oldline.html('<td class="center">'+data['id']+'</td><td>'+data['adgameid']+'</td><td>'+gamename+'</td><td>'+data['packagename']+'</td><td>'+data['download']+'</td><td title="'+data["appversioncode"]+'">'+data['appversionname']+'</td><td>'+data['time']+'</td><td><span class=" label   label-success ">有效</span></td><td>'+data['adcontent']+'</td><td><button class="btn btn-primary evt_modify_xappitem">修改</button> <button class="btn btn-danger evt_del_xappitem">删除</button></td>');
		        	 $($(".evt_modify_xappitem"),oldline).bind('click',btn_mod_handler);
		        	 $($(".evt_del_xappitem"),oldline).unbind('click').bind('click',btn_del_handler);
		        	 $("#evt_xappaddformreset").trigger('click');
		        	 $("#nodata").hide();
		        	 alert('修改成功');
		        	 
		         } else {
		        	 $("#xappTable").prepend('<tr data-appid="'+data['appid']+'" data-id="'+data['id']+'"><td class="center">'+data['id']+'</td><td>'+data['adgameid']+'</td><td>'+gamename+'</td><td>'+data['packagename']+'</td><td>'+data['download']+'</td><td title="'+data["appversioncode"]+'">'+data['appversionname']+'</td><td>'+data['time']+'</td><td><span class=" label   label-success ">有效</span></td><td>'+data["adcontent"]+'</td><td><button class="btn btn-primary evt_modify_xappitem">修改</button> <button class="btn btn-danger evt_del_xappitem">删除</button></td></tr>');
		        	 $(".evt_modify_xappitem:first").bind('click',btn_mod_handler);
		        	 $(".evt_del_xappitem:first").unbind('click').bind('click',btn_del_handler);
		        	 $("#evt_xappaddformreset").trigger('click');
		        	 $("#nodata").hide();
		        	 alert('增加成功');
		         }
	     }
	 });
	
	/**
	 * 编辑按钮处理函数
	 */
	function btn_mod_handler(e) {
		var _this = $(e.target);
		var _p = _this.closest("tr");
		$("#form_appmod").show('fast');
		var packagename = $('#help_packagename');
		var appversionname = $('#help_appversionname');
		var appversioncode = $('#help_appversioncode');
		var appadcontent = $('#help_adcontent');
		packagename.attr("readonly", false).val('');
		appversionname.attr("readonly", false).val('');
		appversioncode.attr("readonly", false).val('');
		
		$("#sel_adgameid").val($("td:eq(1)",_p).text());
		$("#hid_xapp_appid").val($("td:eq(0)",_p).text());
		$("#download_url").val($("td:eq(4)",_p).text()||'');
		packagename.val($("td:eq(3)",_p).text()||'').attr("readonly", true);
		appversionname.val($("td:eq(5)",_p).text()||'').attr("readonly", true);
		appversioncode.val($("td:eq(5)",_p).attr('title')||'').attr("readonly", true);
		appadcontent.val($("td:eq(8)",_p).text()||'');
	}
	
	
	$(".evt_modify_xappitem").bind('click',btn_mod_handler);
	

	/**
	 * 删除按钮处理函数
	 */
	function btn_del_handler(e) {
		//console.log(e);
		 
		var _this = $(e.target);
		if(window.confirm('确认删除？')) {
			var _p = _this.closest("tr");
			_xappid = _p.attr('data-id');
			if(_xappid=='') {return ;}
			$.getJSON('doDeletePutAd',{"subxappid":_xappid},function(data) {
				if(data.status==1) {
					_p.remove();
					if($("#xappTable tr").not($("#nodata")).size()==0) { 
						$("#nodata").show();
					}
					
					if($("#form_appmod").is(":visible"))  {
						$("#evt_xappaddformreset").trigger('click');
					}
				}
				
			});
			
			
		}
	}
	$(".evt_del_xappitem").unbind('click').bind('click',btn_del_handler);
	
	$("#evt_xappaddformreset").click(function() {
		$('#form_appmod').get(0).reset();$('#form_appmod').toggle('fast');
		var packagename = $('#help_packagename');
		var appversionname = $('#help_appversionname');
		var appversioncode = $('#help_appversioncode');
		var appadcontent = $('#help_adcontent');
		packagename.attr("readonly", true).val('');
		appversionname.attr("readonly", true).val('');
		appversioncode.attr("readonly", true).val('');
		appadcontent.val('');
		$("#hid_xapp_appid").val('');
	});
	
});

 
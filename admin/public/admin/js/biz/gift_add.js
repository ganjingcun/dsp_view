 

function typeOf(value) {
	  var t = typeof value;
	  if (value === null)
	    t = 'null'; //null表示什么都没有,只有null===null才返回true
	  if (t === 'object' && value.constructor === Array)
	    t = 'array'; //如果type确实为'object'，后面才会执行
	  return t;
	}

function isFalse(value) {
	  var i, s = true, t = typeof value;
	  if (value !== null && t === 'object')
	    for(i in value) return false; //如果对象是可枚举的
	  else if (t === 'function')
	    return isFalse(value()); //依据函数的返回值判定
	  else
	    s = value ? false : true;
	  return s;
	}
var json2list = function _parse(o,grp) {
	if(grp==undefined) {grp='';}
	 var __i = 0;
	 __i++;
	  if (isFalse(o))  // 这个实际上只检查 [...], {...}, null
	    return 'null'; // 如果是[],{}和null其中一个，就返回'null'字符
	  else {
	    var i, s = '', t = typeOf(o);
	    for (i in o) {
	      var s1 = '', t1 = typeof o[i];
	      if (t1 === 'object')
	        s1 = _parse(o[i]);
	      else // 对于[...]及{...}以外的，直接字符化即可
	        s1 = '<span>' + String(o[i]) + '</span>';
	 
	      if (t === 'array')
	        s += '<li>' + s1 + '</li>';
	      else //'object'需要加上键值
	        s = s+ '<li><span>' + i + '</span> : ' + s1 + '</li>';
	    }
	  }
	  
	  //custom replace
	  s = s.replace('giftitemname','描述名称');
	  s = s.replace('itemcnt','礼包数量');
	  s = s.replace('gameitemkey','游戏对应key');
	  
	  return '<ul class="'+grp+'_'+__i+'">' + s + '</ul>';
	}

var json2giftitemblock = function(o) {
	var _out = '<table class="table table-bordered">';
	
	for(var i in o) {
		_out +='<tr class="evt_rm_item_line"><td>'+o[i]['giftitemname']+'</td><td rowspan="2" width="100px" style="vertical-align:middle"><button class="evt_rm_item cur_shou btn btn-danger btn-small">删除</button></td></tr>';
		_out+='<tr><td>'+o[i]['gameitemkey'] +'*'+o[i]['itemcnt']+'</td></tr>';
	}
	var _out = _out+'</table>';
	return _out;
}


$(function() {
	
	
	$("#form_giftadd").ajaxForm({
		"success":function(data) {
			data = $.parseJSON(data);
				alert(data['info']);
			if(data.status>0) {
				//history.back();
				 if(document.referrer!=null) {
					 location.href=document.referrer;
				 } else {
					 history.back();
				 }
			} else {
				$("#btn_giftadd").prop("disabled", false);
			}
		},
		"beforeSubmit":function() {
			 
			var hidval = $("#giftitemtotal").val();
			if($.trim($('#txt_pkgname').val())=='') {
				alert('礼包名称不能为空');
				$("#btn_giftadd").prop("disabled", false);
				return false;
			}
			
			if(hidval==''||hidval=='[]') {
				alert('至少填写一项礼包项目');
				$("#btn_giftadd").prop("disabled", false);
				return false;
			} else {
				$("#btn_giftadd").prop("disabled", true);
			}	
			
			
		},
		"error":function(data) {
			$("#btn_giftadd").prop("disabled", false);
			data = $.parseJSON(data);
			alert(data.info);
		}
	});
	
	
	
	$("#form_giftEdit").ajaxForm({
		"success":function(data) {
			data = $.parseJSON(data);
			alert(data['info']);
		},
		"beforeSubmit":function() {
			
			if($.trim($('#txt_pkgname').val())=='') {
				alert('礼包名称不能为空');
				$("#btn_giftadd").prop("disabled", false);
				return false;
			}
			stop = false;
			$(".input_biz_desc").each(function() {
				if($.trim($(this).val())=='') {
					alert('描述名称是必填项，只能输入普通的英文字母数字下划线汉语');
					stop = true;
					return ;
				}
			});
			if(stop) {
				return false;
			}
		}
	});
	
	$(".evt_itemadd").click(function() {
		if(/\D+/.test($("#itemnum").val()) || 0==$("#itemnum").val()) {
			alert('道具数量只能输入大于0的整数');
			return false;
		}
		if($(".evt_rm_item_line").size()>9) {
			alert("最多能添加10条礼包项");
			return false;
		}
		
		var _newobj = {
			     "giftitemname":$.trim($("#itemname").val()),
			     "itemcnt":$.trim($("#itemnum").val()),
			     "gameitemkey":$.trim($("#gamegoodkey").val())
		}
		
		stopflag = 0 ;
		for(var i in _newobj) {
			if(_newobj[i]=='') {
				stopflag =1;
				alert("资料没填全啊");
				return false;
			}
		}
		
		if(weiboLen(_newobj['giftitemname'])>18) {
			alert("描述文字不可超过18个字符");
			$("#itemname").focus();
			return false;
		}
				
		var _historyval = $("#giftitemtotal").val();
		  var _h = new Array();
		if(_historyval!='') {
			 var tmp = $.parseJSON(_historyval);
			 if(_historyval[0]=='{'){
				 _h.push(tmp);
			 } else {
				 _h = tmp;
			 }
		}
		_h.push(_newobj);
		
		//$("#giftitemscontainer").html(json2list(_h,'gift_item_'));
		$("#giftitemscontainer").html(json2giftitemblock(_h,'gift_item_'));
		$("#giftitemtotal").val(JSON.stringify(_h));
		$("#itemname").val('');
		$("#itemnum").val('');
		$("#gamegoodkey").val('');
		$('.evt_rm_item').bind('click',giftitem_del_btn_event_handler); 
	});
	
	function giftitem_del_btn_event_handler() {
		  if(!window.confirm('确认删除？')){return false;}
		  var _thisline = $(this).closest('tr');
		  var _thistable = $(this).closest('table');
		  var json_ind = $('.evt_rm_item_line').index(_thisline);//获取点击的删除属于第几行元素
		  
		  var json_string = $('#giftitemtotal').val();
		  var json_obj = $.parseJSON(json_string);
		  if(json_string[0]=='{') {
			  json_obj = [json_obj];
		  }
			  delete(json_obj[json_ind]);
			 var newvl = '';
			 var flitered_array = [];
			 
			 for(var i in json_obj) {
				   if(json_obj[i]!=null) {
					   flitered_array.push(json_obj[i]);
				   }	 
			 }
			  newvl = JSON.stringify(flitered_array);
			 
			  $('#giftitemtotal').val(newvl);
			//  console && console.log($('#giftitemtotal').val());
		  _thisline.next().remove();
		  _thisline.remove();
	  }
	  $('.evt_rm_item').bind('click',giftitem_del_btn_event_handler); 
	  //$("#itemnum").numeral();//数字输入框限制
});
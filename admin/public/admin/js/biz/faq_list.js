$(function() {
       $(".evt_faq_up").add(".evt_faq_down").click(function() {
           var _this = $(this);
           var faqid = _this.attr('data-id');
           var p = _this.closest('tr');
           $(".evt_faq_up").add(".evt_faq_down").removeClass('lastActive');
           _this.addClass('lastActive');
           var appid = p.attr('data-appid');
           var rqUrl = 'faqMv';
           var direct = _this.hasClass('evt_faq_up')?'u':'d';
           /*if(p.attr('data-ordernum')==0 && direct=='u') {
        	   return false;
           } else {
        	   //最后一行就别下移了， 
        	   if(direct=='d' && p.next().length==0) {
        		   return false;
        	   }
           }*/
           
           $.get(rqUrl,{ 'appid':appid,'id':faqid,'direct':direct},function(data) {
        	   try {
        		   data = $.parseJSON(data);
        		   // alert(data.info);
                   if(data.status==1) {
                	   
                	   //location.reload();
                	   if('u'==direct) {
                		   var prevP = p.prev('.candrag');
	                	   p.after(prevP);
	                	} else if('d'==direct) {
	                	   var nextP = p.next('.candrag');
	                	   nextP.after(p);
	                	}
                   }
        	   }catch(e) {
        		   alert('程序遇到bug，请提供截图给系统管理员'+data);
        	   }
             
           });
       });
       
       $(".evt_del_faqitem").click(function() {
    	   var _this = $(this);
    	   var itemid = $(this).attr('data-itemid');
    	   if(confirm('确认删除?')) {
    		   $.post('/admin/singleSDKadmin/faqDel',{ 'id':itemid },function(data) {
    			   data = $.parseJSON(data);
    			   alert(data.info);
    			   if(data.status=='1') {
    				   _this.closest('tr').remove();
    				   if($(".candrag").size()==0) {
    					   $("#nodata").show();
    				   }
    			   }
    		   });
    	   }
       });
       
  });
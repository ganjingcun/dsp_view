
/**
 * author: yangsen
 */
$(document).ready(function() {

                  var cb = function(start, end, label) {
                    $('#inputDate').val(start.format('YYYY-MM-DD') + ' ~ ' + end.format('YYYY-MM-DD'));
                    //alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");
                  }

                  var optionSet2 = {
                    startDate: moment().subtract(7, 'days'),
                    endDate: moment(),
                    opens: 'left',
                    showDropdowns: true,
                    format: 'YYYY-MM-DD',
                    ranges: {
                       '今天': [moment(), moment()],
                       '昨天': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                       '最近7天': [moment().subtract(6, 'days'), moment()],
                       '最近30天': [moment().subtract(29, 'days'), moment()],
                       '本月': [moment().startOf('month'), moment().endOf('month')],
                       '上月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    locale: {
                        applyLabel: '确定',
                        cancelLabel: '重置',
                        fromLabel: '开始',
                        toLabel: '结束',
                        customRangeLabel: '自定义',
                        daysOfWeek: ['日', '一', '二', '三', '四', '五','六'],
                        monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
                        firstDay: 1
                    }
                  };

                  $('#inputDate span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' ~ ' + moment().format('MMMM D, YYYY'));
                  $('#inputDate').daterangepicker(optionSet2, cb);
                  $('#inputDate').on('show.daterangepicker', function() { console.log("show event fired"); });
                  $('#inputDate').on('hide.daterangepicker', function() { console.log("hide event fired"); });
                  $('#inputDate').on('apply.daterangepicker', function(ev, picker) { 
                    console.log("apply event fired, start/end dates are " 
                      + picker.startDate.format('MMMM D, YYYY') 
                      + " to " 
                      + picker.endDate.format('MMMM D, YYYY')
                    ); 
                  });
                  $('#inputDate').on('cancel.daterangepicker', function(ev, picker) { console.log("cancel event fired"); });

                  //列排序
                  $("th[data-orderfield]").click(function() {
                  	var _this = $(this);
                  	var orderfield = _this.attr('data-orderfield');
                  	$("#order_field").val(orderfield);
                  	if(_this.attr('data-orderby')==null || _this.attr('data-orderby')=='desc') {
                  		$("#order_direct").val('asc');
                  	} else {
                  		$("#order_direct").val('desc');
                  	}
                  	$("#search_form").submit();
                  });
                  
});
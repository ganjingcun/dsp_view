/**
 * 礼包相关配置JS
 */
$(function() {
	
	    var modflag = false;
	    //page loaded init.
		function checknodata() {
			if($("#tbody_selected_gift tr").not('.nodata').size()>0) {
				$(".nodata").hide();
			} else{
				$(".nodata").show();
			}
		}
	
	    checknodata();
	    
	    
	    document.body.onbeforeunload=function() {
	    	if(modflag) {
	    			return '您还没有点击保存按钮，直接跳转相当于没保存';
	    	}
	    };
	    
	    
	    
	    /**
	     * 移动按钮处理函数
	     */
	    function mvBtnEventHdl(event) {
	    	modflag = true;	    	
    			$(".mv_siblings").removeClass('lastActive');
    			var _this = $(event.target);
        		_this.addClass('lastActive');
            	var forward = _this.attr('data-arrow');
            	var  p = _this.closest('tr');
            	var p_ind = p.index(".candrag");
            	
            	var prevP = p.prev('.candrag');
            	var nextP = p.next('.candrag');
            	var _siblings = document.getElementsByClassName('candrag');
            	if(p_ind==0) {
            		nextP =$('.candrag:eq(1)');
            	} else if(p_ind==_siblings.length) {
            		prevP = $('.candrag:eq('+(_siblings.length-2)+')');
            	} else {
            	
            		 prevP = p.prev('.candrag');
                	 nextP = p.next('.candrag');
            
            	
                	console.log(_siblings);
                	var prevI = null;var nextI = null;
                	for(var i=0;i<_siblings.length;i++) {
                		if(i==p_ind-1) {
                			prevI = i;
                		}
                		
                		if(i==p_ind+1) {
                			nextI = i;
                		}
                	}
                	
                	prevP = $('.candrag:eq('+prevI+')');
                	nextP = $('.candrag:eq('+nextI+')');
            	}
            	//console.log(p.index('.candrag'));
            	if('up'==forward) {
            	   p.after($(prevP));
            	} else if('down'==forward) {
            	   $(nextP).after(p);
            	}
	    }
	    
	    
	
        $("#tbody_selected_gift").draggable();
        $(".mv_siblings").on('click',mvBtnEventHdl);
        
        
        
        $("#btn_submit").click(function() {
        	var giftids = [];
        	var appid = $("#hid_appid").val();
        	
        	$('#tbody_selected_gift .candrag').each(function() {
        		giftids.push($(this).attr("data-giftid"));
        	});
        	
        	//console&&console.log(giftids);
        	
        	$.post('giftDailyConfig',{"giftids":giftids,"appid":appid},function(data) {
    			data=$.parseJSON(data);
    			alert(data.info);
    			modflag=false;
    		});
        	 
        });
         
        
        function  __rmtr(event){
        	if(window.confirm('确认删除？')) {
        		//移除tr
        		$(event.target).closest('.candrag').remove();
        		$("#tbody_selected_gift tr:empty").remove();//jquery bugfix .
        		checknodata();
        	}
        	
        }
        
        $(".evt_rmgiftitem").unbind('click').bind('click',__rmtr);
        $("#sel_pkg").change(function(){
        	$("#showarea_of_sel").text($(":selected",$(this)).attr('data-detail'));
        });
        
        $("#btn_sel_pkg").click(function(e) {
        	modflag = true;
        	var maxind = $('#tbody_selected_gift .candrag').size();
        	if(maxind>4) {
        		alert("最多能设置5个礼包。");
        		return false;
        	}
        	maxind = (maxind+1);
        	
        	var _giftid = $("#sel_pkg").val();
        	if(_giftid=="0") {return false};
        	//check unique
        	var tableitems = $(".candrag");
        	
        	var exists = false;
        	tableitems.each(function(){
        		if(exists==true) return ;
        		if(_giftid==$(this).attr("data-giftid")) {
        			alert("已经在列表中了,无需再次添加");
        			exists = true;
        		}
        	});
        	
        	if(exists) {return false;}
        	
        	var _option = $(":selected",$("#sel_pkg"));
        	var _giftcont = _option.attr('data-detail');
        	var _giftname = _option.attr('data-name');
        	
        	//console.log(maxind);
        	var newtr = '<tr data-giftid="'+_giftid+'" class="candrag"><td>'+_giftid+'</td><td>'+_giftname+'</td><td>'+_giftcont+'</td>';
        	newtr +='<td><button class="btn btn-danger btn-middle evt_rmgiftitem">删除</button> <button class="btn btn-middle mv_siblings" data-arrow="up">↑</button> <button class="btn btn-middle mv_siblings" data-arrow="down">↓</button></td><tr>';
        	$("#tbody_selected_gift").append(newtr);
        	
        	
        	$(".mv_siblings").unbind('click');
        	
        	$(".mv_siblings").each(function() {
        		$(this).on('click',mvBtnEventHdl);
        	});
        	
        	
        	$(".evt_rmgiftitem").unbind('click').bind('click',__rmtr);
        	$(".nodata").hide();
        });
        
    });
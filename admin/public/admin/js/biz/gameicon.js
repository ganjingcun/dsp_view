/**
 * addAdGame.html 's js
 */
function ajaxUploder(fileObj, imgtype,tpl) {

	var obj 			= $(fileObj);
	var fileId 			= obj.attr('id');
	var id 				= obj.parents('.item:eq(0)').attr('data1');
	var image_show_box 	= obj.parents('div:eq(0)').find('.image_show_box');
	var adgroupid 		= obj.parents('.item:eq(0)').attr('data2');
	var maindiv 		= obj.parents('.controls');

	//var stufftype = $('input:radio[name="stufftype"]:checked').val();

	var loading = obj.siblings('.onload').show();
	$.ajaxFileUpload( {
				url : '/admin/singleSDKmanage/ajaxDoUploadStuff/?type=' + imgtype,
				secureuri : true,
				fileElementId : fileId,
				dataType : 'json',// 服务器返回的格式，可以是json
				success : function(data, status) { 
        			$("#iconimgerr").hide();
					var msgObj = data[0]
					if (msgObj.isReceived == true) {
						var str = '<div class="'+tpl+'image_show_box">';
						for ( var i in msgObj.imgInfo) {
							if( msgObj.imgInfo[i]['width'] == 162 ) {
								str+='<img src="/public/upload'+msgObj.imgInfo[i]['newImg']+'" style="width:162px;height:162px; vertical-align:bottom" />' +
                                        '<a class="btn btn-info" href="/public/upload'+msgObj.imgInfo[i]['newImg']+'" target="_blank">查看实际效果</a>' +
                                        '<span>('+msgObj.imgInfo[i]['width']+'*'+msgObj.imgInfo[i]['height']+')</span>';
							}
							
							str+='<input type="hidden" id="img_'+msgObj.imgInfo[i]['width']+'" name="img_'+msgObj.imgInfo[i]['width']+'" value="'+msgObj.imgInfo[i]['newImg']+'"/>';
						}
						str += '&nbsp;&nbsp;</div>';
						maindiv.find('.'+tpl+'image_show_box').remove();// 将刚才上传的干掉
						maindiv.append(str);
						$('[name=isupdate]').val('1');
						maindiv.find('.'+tpl+'_image_show_box').fadeIn("slow");
						

					} else {
						var msg = msgObj.errorMessage;
						alert('    ' + msg);
					}
					loading.hide();
					return true;
				},
				error : function(data, status, e) {
					loading.hide();
				}
			})
}
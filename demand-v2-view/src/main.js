import App from './App'
import Vue from 'vue'
import VueX from 'vuex'
import store from './store'
import VueRouter from 'vue-router'
import Element from 'element-ui'
import 'element-ui/lib/theme-default/index.css'
/*eslint-disable */
Vue.use(Element)
Vue.use(VueRouter)
Vue.use(VueX)

export const router = new VueRouter({
  hashbang: false,
  routes: [
    // { path: '/',
    //   component: require('./containers/login')
    // },
    { path: '/',
      component: require('./components/home')
    },

    { path: '/home',
      component: require('./components/home'),
      children: [
        // an empty path will be treated as the default, e.g.
        // components rendered at /parent: Root -> Parent -> Default
          
        { path: 'dashboard', component: require('./components/home/dashboard') },
          
        { path: 'plans', component: require('./components/home/plans') },
        { path: 'plan', component: require('./components/home/plan') },
        { path: 'ads', component: require('./components/home/ads') },
        { path: 'adsdetail', component: require('./components/home/adsdetail') },
        { path: 'newads', component: require('./components/home/newads') },
        { path: 'stuff', component: require('./components/home/stuff') },
        { path: 'groups', component: require('./components/home/groups') },

        { path: 'report', component: require('./components/home/report') },

        { path: 'balance', component: require('./components/home/balance') },
        { path: 'financing', component: require('./components/home/financing') },
        { path: 'invoice', component: require('./components/home/invoice') },
          
        { path: 'setting', component: require('./components/home/setting') },

      ]
    }
  ]
})

new Vue({el: '#app', store, router, render: h => h(App)})

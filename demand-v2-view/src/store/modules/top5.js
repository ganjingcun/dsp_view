import * as types from '../mutation-types'

// init state
const state = {
  imp: [],
  click: [],
  clickrate: [],
}

// mutations
const mutations = {
  [types.GET_TOP5] (state, {top5data}) {
    state.imp = top5data.imp
    state.click = top5data.click
    state.clickrate = top5data.clickrate
  }
}

export default {
  state,
  mutations
}

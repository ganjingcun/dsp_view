import * as types from '../mutation-types'

// init state
const state = {
  account: 0
}

// mutations
const mutations = {
  [types.ADD_USER_ACCOUNT] (state, {account}) {
    state.account = account
  }
}

export default {
  state,
  mutations
}

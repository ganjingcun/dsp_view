import * as types from '../mutation-types'

// init state
const state = {
  account: [],
  ads: [],
}

// mutations
const mutations = {
  [types.GET_MONEY_AND_ADS] (state, {data}) {

    console.log('stats/get_money_and_adsstats/get_money_an',data)

    state.account = data.account
    state.ads = data.ads
  }
}

export default {
  state,
  mutations
}

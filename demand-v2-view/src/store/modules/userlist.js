import * as types from '../mutation-types'

// init state
const state = {
  listData: [],
  pageTotal: 1
}

// mutations
const mutations = {
  [types.GET_USER_LIST] (state, {listData, pageTotal}) {
    console.log(listData)
    console.log(pageTotal)
    state.listData = listData
    state.pageTotal = pageTotal
   
  }
}

export default {
  state,
  mutations
}

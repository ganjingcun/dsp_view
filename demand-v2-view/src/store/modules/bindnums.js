import * as types from '../mutation-types'

// init state
const state = {
  selectlist: [],
  lastownernum: 0,
  loading: false,
  btntext: '绑定'
}

// mutations
const mutations = {
  [types.GET_SHOP_USER_LIST] (state, {data}) {
    state.selectlist = data
  },
  [types.GET_LAST_OWNER_NUM] (state, {num}) {
    state.lastownernum = num
  },
  [types.BIND_USER_NUMS] (state) {
    state.loading = !state.loading
    state.btntext = '绑定'
  }
}

export default {
  state,
  mutations
}

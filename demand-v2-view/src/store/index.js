import Vue from 'vue'
import Vuex from 'vuex'
import * as actions from './actions'
import * as getters from './getters'
import userinfo from './modules/userinfo'
import bindnums from './modules/bindnums'
import userlist from './modules/userlist'
import top5 from './modules/top5'
import dashboarddata from './modules/dashboarddata'

Vue.use(Vuex)

export default new Vuex.Store({
  actions,
  getters,
  modules: {
    userinfo,
    bindnums,
    userlist,
    top5,
    dashboarddata
  }
})

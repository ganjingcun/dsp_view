import Vue from 'vue'
import VueResource from 'vue-resource'
import * as types from './mutation-types'

Vue.use(VueResource)
const ip = 'http://nbw.dsp-api.cocos-ads.com'

// 定义全局变量
export const addUserAccount = ({commit}, account) => {
  commit(types.ADD_USER_ACCOUNT, {account: account})
}

// 用户列表--首先屏幕数据
export const getuserlist = ({commit}, context) => {
    // export const userlist = (adminAccount, page, pageSize, tableData, pageTotal, notify) => {
    let adminAccount = context.adminAccount
    let page = context.currpage
    let pageSize = context.pageSize
    let notify = context.$notify
    // actoins for userlist todo
    Vue.http.post(ip + '/campaign/get_list', {'admin_account': adminAccount, 'page': page, 'page_size': pageSize}).then((res) => {

        console.log(res.data)
    if (res.data.status !== 0) {
        notify({
            title: '错误',
            message: res.data.msg,
            type: 'error'
        })
    } else {
        let tableData = {}
        tableData.listData = res.data.data.items

        tableData.pageTotal = res.data.data.count

        commit(types.GET_USER_LIST, {listData: tableData.listData, pageTotal: tableData.pageTotal})
        // context.$store.dispatch('updatepage', tableData)
    }
}, (response) => {
        // error callback
        notify({
            title: '服务器开小差～',
            message: '服务器开小差，请联系管理员～',
            type: 'error'
        })
    })
}



// 用户列表--首先屏幕数据
export const dashboarddata = ({commit}, context) => {
    // export const userlist = (adminAccount, page, pageSize, tableData, pageTotal, notify) => {
    let adminAccount = context.adminAccount
    let page = context.currpage
    let pageSize = context.pageSize
    let notify = context.$notify
    // actoins for userlist todo
    Vue.http.post(ip + '/stats/get_money_and_ads', {'admin_account': adminAccount, 'page': page, 'page_size': pageSize}).then((res) => {

        console.log(res.data)
    if (res.data.status !== 0) {
        notify({
            title: '错误',
            message: res.data.msg,
            type: 'error'
        })
    } else {
        commit(types.GET_MONEY_AND_ADS, {data: res.data.data})
        // context.$store.dispatch('updatepage', tableData)
    }
}, (response) => {
        // error callback
        notify({
            title: '服务器开小差～',
            message: '服务器开小差，请联系管理员～',
            type: 'error'
        })
    })
}


// 用户列表--首先屏幕数据
export const gettopfive = ({commit}, context) => {
    // export const userlist = (adminAccount, page, pageSize, tableData, pageTotal, notify) => {
    let adminAccount = context.adminAccount
    let page = context.currpage
    let pageSize = context.pageSize
    let notify = context.$notify
    // actoins for userlist todo
    Vue.http.post(ip + '/report/get_top5', {'admin_account': adminAccount, 'page': page, 'page_size': pageSize}).then((res) => {

        console.log(res.data)
    if (res.data.status !== 0) {
        notify({
            title: '错误',
            message: res.data.msg,
            type: 'error'
        })
    } else {
        commit(types.GET_TOP5, {top5data: res.data.data})
        // context.$store.dispatch('updatepage', tableData)
    }
}, (response) => {
        // error callback
        notify({
            title: '服务器开小差～',
            message: '服务器开小差，请联系管理员～',
            type: 'error'
        })
    })
}

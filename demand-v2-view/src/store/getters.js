// 拉取管理信息
export const getacount = state => state.userinfo.account

// 拉取绑定信息
export const getbindnums = state => state.bindnums

// 拉去用户列表
export const getuserlist = state => state.userlist

export const gettopfive = state => state.top5

export const dashboarddata = state => state.dashboarddata

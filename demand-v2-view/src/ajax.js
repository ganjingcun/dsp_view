import Vue from 'vue'
import VueResource from 'vue-resource'
import md5 from 'md5'

Vue.use(VueResource)

const ip = 'http://nbw.demand-api.cocos-ads.com/'



// 管理员登录
// {"code":"200","data":{"name":"管理员昵称"},"success":true}
export const login = (adminAcount, password, router, notify) => {

  Vue.http.post(ip + '/admin/admin/login', {'admin_account': adminAcount, 'pass_word': password}).then((res) => {
    console.log(res)
    if (!res.data.success) {
      notify({
        title: '登陆错误',
        message: res.data.message ,
        type: 'error'
      })
    } else {

      var newTh = (new Date().getTime() / 1000 / 60 / 60) + "#" + adminAcount;
      localStorage.setItem('chejianzhurenadminloginkey', newTh);

      router.push('home/add')
    }
  }, (response) => {
    // error callback
    notify({
      title: '服务器开小差～',
      message: '服务器开小差，请联系管理员～',
      type: 'error'
    })
  })
}


<?php
/**
 * 自定义Model继承类，扩展CI_Model
 *
 * @author wangdong
 * @date 2013.10.15
 * @note 详细说明及修改日志
 */
class MY_Model extends CI_Model{
	/**
	 * MY_Model初始化
	 *
	 * @author wangdong
	 * @date 2013.10.15
	 * @note 详细说明及修改日志
	 */
	function __construct()
	{
		parent::__construct();
	}
	
}
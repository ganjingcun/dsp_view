<?php 

class AdBorderClass extends MY_Model{

    /**
     * 初始化
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * 获取系统边框
     */
    public function getSysBorderList()
    {
    	$sql = "select * from ad_border where type=1 and status=1 group by bordergroupid";
    	$result = $this->db->query($sql);
    	$data=array();
		$data = $result->result_array();
        return $data;
    }
    
	/**
     * 获取border列表
     */
    public function getBorderList($where)
    {
        $query = $this->db->get_where('ad_border', $where);
        $data = $query->result_array();
        return $data;
    }
    
    function delBorderGroup($bordergroupid)
    {
    	$where['bordergroupid']=$bordergroupid;
    	$query = $this->db->get_where('ad_border', $where, 1);
    	if ($query->num_rows() > 0)
        {
            $data = $query->result_array();
        }
    	if(empty($data) || $data[0]['type'] == 1)
        {
            return;
        }
        $set['status']=2;
        $this->db->update('ad_border',$set, $where);
    	return;
    }

}

<?php /**
* @filename	: apptypeclass.php
* @encoding	: UTF-8
* @author		: niejianhui
* @datetime	: 2012-12-17 17:19:24
* @Description  : 应用类型model
*/
class AppChannelClass extends MY_Model{

    /**
     * 初始化
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model("manage/MarketClass");
    }

    /**
     * 新建渠道
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    public function doAdd($params, $userInfo)
    {
        $params['channelname'] = trim($params['channelname']);
        $chk = $this->chkAddInfo($params, $userInfo);
        if($chk !== true)
        {
            $err['status'] = 0;
            $err = array_merge($err, $chk);
            return $err;
        }
        $data['channelname'] = $params['channelname'];
        $data['userid'] = $userInfo['userid'];
        $data['createtime'] = time();
        
        $this->load->model('manage/AppInfoClass');
        $appInfo = $this->AppInfoClass->getAppInfoRow($params['appid'], $userInfo['userid']);
        if($appInfo['ostypeid'] == 1)
        {
        	$data['marketid'] = $params['marketid'];
            $table = "channel_type";
        }
        else
        {
            $table = "channel_ios_type";
        }
        $this->db->insert($table, $data);
        $id = $this->db->insert_id();
        if($id <= 0) return array('status' => 0, 'info' => 'positionnameerr', 'data' => array('positionnamemsg', '未知错误。'));
        return array('status' => 1, 'id' => $id);
    }

    /**
     * 申请渠道PunchboxId
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    public function doAddChannel($params, $userInfo)
    {
        $chk = $this->chkAddChannelInfo($params, $userInfo);
        if($chk !== true)
        {
            $err['status'] = 0;
            $err = array_merge($err, $chk);
            return $err;
        }
        $channelList = explode(',', $params['channelid']);
        $this->load->model('manage/AppInfoClass');
        $appInfo = $this->AppInfoClass->getAppInfoRow($params['appid'], $userInfo['userid']);
        $channelTyepList = $this->getChannelTypeList($appInfo['ostypeid'], $userInfo['userid'], $userInfo['usertype']);
        foreach ($channelTyepList as $value) {
            $channelIdsArray[$value['channelid']]['channelname'] = $value['channelname'];
            if(1 == $appInfo['ostypeid']){
            	$channelIdsArray[$value['channelid']]['marketid'] = $value['marketid'];
            }
        }


        $this->load->library('UUID');
        foreach ($channelList as $v)
        {
            if(!empty($v))
            {
                $data['userid'] = $userInfo['userid'];
                $data['appid'] = $params['appid'];
                $data['channelid'] = $v;
                if(!isset($channelIdsArray[$v]))
                {
                    continue;
                }
                if(1 == $appInfo['ostypeid']){
                	$data['marketid'] = $channelIdsArray[$v]['marketid'];
                }
                $data['channelname'] = $channelIdsArray[$v]['channelname'];
                $data['publisherID'] = $params['appid'].'-'.UUID::getUUID();;
                $data['createtime'] = time();
            }
            else
            {
                continue;
            }
            $dataBatch[] = $data;
        }
        if(empty($dataBatch))
        {
            return array('status' => 0, 'info' => 'channeliderr', 'data' => array('channelidmsg', '未知错误。'));
        }
        $this->db->insert_batch('app_channel', $dataBatch);
        $id = $this->db->insert_id();
        if($id <= 0) return array('status' => 0, 'info' => 'channeliderr', 'data' => array('channelidmsg', '未知错误。'));
        return array('status' => 1, 'id' => $id);
    }

    /**
     * 检测渠道信息
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    private function chkAddInfo($params, $userInfo)
    {
        if(empty($params['appid']))
        {
            $error['data'] = array('channelnamemsg', '未知错误！');
            $error['info'] = 'channelnameerr';
            return $error;
        }
        if(empty($params['channelname']))
        {
            $error['data'] = array('channelnamemsg', '渠道名称不能为空。');
            $error['info'] = 'channelnameerr';
            return $error;
        }
        if(mb_strlen($params['channelname']) > 50)
        {
            $error['data'] = array('channelnamemsg', '渠道名称不能超过50个字符。');
            $error['info'] = 'channelnameerr';
            return $error;
        }
        if($params['marketid'] != 0){
        	$marketinfo = $this->MarketClass->getRow(array('marketid'=>$params['marketid'],'status'=>0));
        	if(empty($marketinfo)){
        		$error['data'] = array('marketidmsg', '该应用商店不存在。');
	            $error['info'] = 'marketiderr';
	            return $error;
        	}
        }
        $appInfo = $this->AppInfoClass->getAppInfoRow($params['appid'], $userInfo['userid']);
        $channelInfo = $this->getRow(array('channelname' => $params['channelname'], 'userid' =>  $userInfo['userid']), $appInfo['ostypeid']);
        if($channelInfo !== false)
        {
            $error['data'] = array('channelnamemsg', '渠道名称不能重复。');
            $error['info'] = 'channelnameerr';
            return $error;
        }
        return true;
    }

    /**
     * 获取渠道列表
     */
    public function getList($appId, $userId)
    {
        if(empty($userId) || $userId <=0)
        {
            return false;
        }
        $where['userid'] = $userId;
        if(!empty($appId))
        {
            $where['appid'] = $appId;
        }
        $this->db->order_by('createtime', "desc");
        $query = $this->db->get_where('app_channel', $where);
        if ($query->num_rows() > 0)
        {
            $data = $query->result_array();
        }
        if(empty($data))
        {
            return false;
        }
        return $data;
    }
    
    /**
     * 获取转化率最高的10个渠道列表
     */
    public function getRatioTopTenList($appId, $userId)
    {
        if(empty($userId) || $userId <=0)
        {
            return false;
        }
        $where['userid'] = $userId;
        if(!empty($appId))
        {
            $where['appid'] = $appId;
        }
        $this->db->order_by('ctr desc,atr desc,createtime desc');
        $this->db->limit(10);
        $query = $this->db->get_where('app_channel', $where);
        if ($query->num_rows() > 0)
        {
            $data = $query->result_array();
        }
        if(empty($data))
        {
            return false;
        }
        return $data;
    }
    
    /**
     * 搜索渠道列表
     */
    public function getSearchList($keys, $appId, $userId)
    {
        if(empty($userId) || $userId <=0 || empty($keys))
        {
            return false;
        }
        $where['userid'] = $userId;
        if(!empty($appId))
        {
            $where['appid'] = $appId;
        }$this->db->like('channelname', $keys);
        $this->db->order_by('ctr desc,atr desc,createtime desc');
        $this->db->limit(10);
        $query = $this->db->get_where('app_channel', $where);
        if ($query->num_rows() > 0)
        {
            $data = $query->result_array();
        }
        if(empty($data))
        {
            return false;
        }
        return $data;
    }

    /**
     * 检测渠道信息
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    private function chkAddChannelInfo($params, $userInfo)
    {
        if(empty($params['channelid']) )
        {
            $error['data'] = '';
            $error['info'] = 'channeliderr';
            return $error;
        }
        if( empty($params['appid']) )
        {
            $error['data'] = array('channelidmsg', '未知错误！');
            $error['info'] = 'channeliderr';
            return $error;
        }
        return true;
    }


    /**
     * 获取广告位基本信息
     * Enter description here ...
     * @param $appId
     * @param $userId
     */
    public function getRow($where, $ostypeId)
    {

        if($ostypeId == 1)
        {
            $table = 'channel_type';
        }
        else
        {
            $table = 'channel_ios_type';
        }
        $query = $this->db->get_where($table, $where);
        if ($query->num_rows() > 0)
        {
            $data = $query->row_array();
        }
        if(empty($data))
        {
            return false;
        }
        return $data;
    }

    /**
     * 获取渠道基本信息
     * Enter description here ...
     * @param $appId
     * @param $userId
     */
    public function getChannelTypeList($ostypeId, $userId, $userType = 0)
    {
        if($ostypeId == 1)
        {
            $table = 'channel_type';
        }
        else
        {
            $table = 'channel_ios_type';
        }
        $this->db->order_by("sort", "DESC");
        if($userType == 3)
        {
            $query = $this->db->get_where($table, array('userid' => 0));
        }
        else
        {
            $query = $this->db->get_where($table, array('userid' => $userId));
        }
        if ($query->num_rows() > 0)
        {
            $data = $query->result_array();
        }
        if(empty($data))
        {
            return false;
        }
        return $data;
    }

}
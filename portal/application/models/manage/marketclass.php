<?php 

class MarketClass extends MY_Model{

    /**
     * 初始化
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    
	/**
     * 获取列表
     */
    public function getList($where)
    {
        $query = $this->db->get_where('market', $where);
        $data = $query->result_array();
        return $data;
    }
    
 	/**
     * 获取一行
     */
    public function getRow($where)
    {
        $this->db->order_by('marketid', "desc");
        $query = $this->db->get_where('market', $where);
        if ($query->num_rows() > 0)
        {
            $data = $query->row_array();
        }
        if(empty($data))
        {
            return false;
        }
        return $data;
    }

}

<?php /**
* @filename	: apptypeclass.php
* @encoding	: UTF-8
* @author		: niejianhui
* @datetime	: 2012-12-17 17:19:24
* @Description  : 应用类型model
*/
class SdkVersionLogClass extends MY_Model{

    /**
     * 初始化
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * 获取标签列表
     */
    public function getRow($osType = 1,$sdktype=1)
    {
        $where['type'] = $osType;
        $where['sdktype'] = $sdktype;
        $this->db->order_by('id', "desc");
        $query = $this->db->get_where('sdkversion_log', $where);
        if ($query->num_rows() > 0)
        {
            $data = $query->row_array();
        }
        if(empty($data))
        {
            return false;
        }
        return $data;
    }

    /**
     * 获取标签列表
     */
    public function getList($osType = 1,$sdktype=1)
    {
        $where['type'] = $osType;
        $where['sdktype'] = $sdktype;
        $this->db->order_by('id', "desc");
        $query = $this->db->get_where('sdkversion_log', $where);
        if ($query->num_rows() > 0)
        {
            $data = $query->result_array();
        }
        if(empty($data))
        {
            return false;
        }
        return $data;
    }
}
<?php
/**
 * 用户信息操作
 *
 * @brief M层
 * @author 王栋
 * @date 2013.10.15
 * @note 详细说明及修改日志
 */
class UserInfoClass extends MY_Model{

    /**
     * 初始化
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('Security');
        $this->load->database();
    }

    /**
     * 注册用户接口
     *
     * @author wangdong
     * @date 2013.10.15
     * @note 详细说明及修改日志
     */
    public function doRegister($params)
    {
        $error = array('status' => false);
        $this->load->helper("email");
        $params['username'] = trim($params['username']);
        if(empty($params['username']) || $params['username'] == '邮箱')
        {
            $error['status'] = true;
            $error['usernamemsg'] = '邮箱不能为空！';
            return $error;
        }
        if(!valid_email($params['username']))
        {
            $error['status'] = true;
            $error['usernamemsg'] = '请输入正确的邮箱地址！';
            return $error;
        }
        if(empty($params['password']))
        {
            $error['status'] = true;
            $error['passwordmsg'] = '密码不能为空！';
            return $error;
        }
        if(strlen($params['password']) < 6 || strlen($params['password']) > 16)
        {
            $error['status'] = true;
            $error['passwordmsg'] =  '新密码不能少于6位或大于16位！';
            return $error;
        }
        if($params['password'] != $params['passconf'])
        {
            $error['status'] = true;
            $error['passwordmsg'] = '请注意密码一致！';
            return $error;
        }
        if(strtoupper($params['validatecode']) != strtoupper($params['captcha']))
        {
            $error['status'] = true;
            $error['captchamsg'] = '验证码错误！';
            return $error;
        }
        $data = $this->getUserInfo($params['username']);
        if(!empty($data))
        {
            $error['status'] = true;
            $error['usernamemsg'] = '邮箱已经被注册！';
            return $error;
        }
        //        $cocodata['user_name'] = $params['username'];
        //        $cocodata['password'] = $params['password'];
        //        $cocodata['repassword'] = $params['passconf'];
        //        $cocodata['email'] = $params['username'];
        //        $cocoUrl = "http://test.www.cocoachina.com/bbs/register_new.php";
        //        $postdata = "&regname=" . $cocodata['user_name'] . "&regemail=" . $cocodata['email'] . "&regpwd=" . $cocodata['password'] . "&from=cocounion&redirect_url=$redirect_url";
        //        $reg_status = curlPost($cocoUrl, $postdata);
        //        if(!empty($reg_status))
        //        {
        //            preg_match_all("|\<span class=\"f14\"\>(.*)\<\/span\>|U", $reg_status, $needcontents);
        //            $msg = mb_convert_encoding($needcontents[1][0], 'utf-8', 'gbk');
        //            $error['status'] = true;
        //            $error['usernamemsg'] = '邮箱已经被注册！';
        //            return $error;
        //        }
        $data['username'] = $params['username'];
        $data['email'] = $params['username'];
        //        $data['password'] = md5($params['password']);
        $data['password'] = Security::cryptPassword($params['password']);
        $data['regip']= $params['ip'];
        $data['regtime'] = time();
        $data['secretKey'] = getAppSecret();
        $this->db->insert('user_member', $data);
        $userId = $this->db->insert_id();
        if($userId > 0)
        {
            $this->sendMail($userId,$data['username'],$data['password'],1);
        }
        return $error;
    }

    /**
     * 注册用户接口
     *
     * @author wangdong
     * @date 2013.10.15
     * @note 详细说明及修改日志
     */
    public function oauthRegister($params)
    {
        $error = array('status' => false);
        $this->load->helper("email");

        if(empty($params['username']))
        {
            $error['status'] = true;
            $error['usernamemsg'] = '用户名不能为空！';
            return $error;
        }
        if(empty($params['password']))
        {
            $error['status'] = true;
            $error['passwordmsg'] = '密码不能为空！';
            return $error;
        }
        $data = $this->getUserInfo($params['username']);
        if(!empty($data))
        {
            $error['status'] = true;
            $error['usernamemsg'] = '用户名已经被注册！';
            return $error;
        }
        $data['username'] = $params['username'];
        $data['email'] = $params['email'];
        //        $data['password'] = md5($params['password']);
        $data['password'] = Security::cryptPassword($params['password']);
        $data['regip']= $params['ip'];
        $data['regtime'] = time();
        $data['usertype'] = 4;
        $data['userstatus'] = 1;
        $this->db->insert('user_member', $data);
        return $error;
    }

    public function reportAPIAuthentication($params){
    	$error = array('status' => false, 'data'=>'');
    	$this->load->helper("email");
        if(!valid_email($params['username']))
        {
            $error['data'] = '请输入正确的邮箱地址';
            return $error;
        }
    	$data = $this->getUserInfo($params['username']);
    	if(empty($data))
        {
            $error['data'] = '用户名错误';
            return $error;
        }
        if(md5($data['secretkey']) != $params['key']){
        	$error['data'] = '密钥错误';
        	return $error;
        }
    	if($data['userstatus'] != 1)
        {
            $error['data'] = '用户未激活或被锁定！';
            return $error;
        }
    	
    	$error['status'] = true;
    	return $error;
    }
    /**
     * 登陆验证类
     * @author wangdong
     */
    public function doLogin($params)
    {
        $error = array('status' => false, 'info'=>'', 'data'=>'');
        $this->load->helper("email");

        if(empty($params['username']) || $params['username'] == '邮箱')
        {
            $error['data'] = '邮箱不能为空！';
            return $error;
        }
        if(!valid_email($params['username']))
        {
            $error['data'] = '请输入正确的邮箱地址！';
            return $error;
        }
        if(empty($params['password']))
        {
            $error['data'] = '密码不能为空！';
            return $error;
        }
        if(strtoupper($params['validatecode']) != strtoupper($params['captcha']))
        {
            $error['data'] = '验证码错误！';
            return $error;
        }
        $data = $this->getUserInfo($params['username']);
        if(empty($data))
        {
            $error['data'] = '用户名错误！';
            return $error;
        }
        //        if($data['passwordtype'] == 'md5')
        //        {
        //            if($data['password'] != md5($params['password']))
        //            {
        //                $error['data'] = '密码错误！';
        //                return $error;
        //            }
        //        }
        //        else
        //        {
        //            if($data['password'] != Security::cryptPassword($params['password']))
        //            {
        //                $error['data'] = '密码错误！';
        //                return $error;
        //            }
        //        }
        if($params['autologin'])
        {
            $autologin = get_cookie('autologin');
            $autopwd = get_cookie('autopwd');
            $autousername = get_cookie('autousername');
            if($params['username'] != $autousername)
            {
                $autologin = false;
            }
        }
        else
        {
            $autologin = false;
        }
        if(!$autologin)
        {
            if(empty($data) || $data['password'] != Security::cryptPassword($params['password']))
            {
                $error['data'] = '用户名或密码错误！';
                return $error;
            }
        }
        else
        {
            if($autopwd != md5($data['username'].$data['password']))
            {
                $error['data'] = '用户名或密码错误！';
                return $error;
            }
        }
        if($data['userstatus'] != 1)
        {
            $error['data'] = '用户未激活或被锁定！';
            return $error;
        }
        $this->setSession($data);
        $this->loginUpdate($data ,$params['ip']);
        if($params['autologin'])
        {
            set_cookie('autologin', 1, 86400*15);
            set_cookie('autopwd', md5($data['username'].$data['password']), 86400*15);
            set_cookie('autousername', $data['username'], 86400*15);
        }
        else
        {
            set_cookie('autologin', 0, -1);
            set_cookie('autopwd', '', -1);
            set_cookie('autousername', '', -1);
        }
        $error['status'] = true;
        $error['data'] = '';
        $error['info'] = $data['username'];
        return $error;
    }

    /**
     * 登陆验证类
     * @author wangdong
     */
    public function oauthLogin($params)
    {
        $error = array('status' => false);
        if(empty($params['username']))
        {
            $error['status'] = 4;
            $error['usernamemsg'] = '用户名不能为空！';
            return $error;
        }
        if(empty($params['password']))
        {
            $error['status'] = 3;
            $error['passwordmsg'] = '密码不能为空！';
            return $error;
        }
        $data = $this->getUserInfo($params['username']);
        if(empty($data))
        {
            $error['status'] = 2;
            return $error;
        }
        if(empty($data) || $data['password'] != Security::cryptPassword($params['password']))
        {
            $error['status'] = true;
            $error['usernamemsg'] = '用户名或密码错误！';
            return $error;
        }
        if($data['userstatus'] != 1)
        {
            $error['status'] = true;
            $error['usernamemsg'] = '用户未激活或被锁定！';
            return $error;
        }
        $data['email'] = $params['email'];
        $this->setSession($data);
        $this->loginUpdate($data ,$params['ip']);
        $error['status'] = false;
        return $error;
    }

    /**
     * 更新用户登录信息
     * @author wangdong
     */
    private function loginUpdate($data ,$ip)
    {
        $where['userid'] = $data['userid'];
        $upData['logintime'] = time();
        $upData['loginip'] = $ip;
        $this->db->where($where);
        $this->db->update('user_member', $upData);
        return true;
    }

    /**
     * 更新用户登录信息
     * @author wangdong
     */
    private function setSession($data)
    {
        $this->load->library('session');

        $userInfo['isremind'] = 1;
        if($data['accounttype'] == 1 && $data['isremind'] == 0)
        {
            $this->load->model('user/IncomeClass');
            if($this->IncomeClass->getSumIncome($data['userid'], 1) > 10000*1000000)
            {
                $userInfo['isremind'] = 0;
                $this->db->where(array('userid'=>$data['userid']));
                $this->db->update('user_member', array('isremind'=>1));
            }
        }
        $userInfo['userid'] = $data['userid'];
        $userInfo['username'] = $data['username'];
        $userInfo['usertype'] = $data['usertype'];
        $userInfo['realname'] = $data['realname'];
        $userInfo['telephone'] = $data['telephone'];
        $userInfo['isauth'] = $data['accountstatus'];
        $userInfo['istype'] = $data['accounttype'];
        $this->session->set_userdata($userInfo);
        return true;
    }

    /**
     * 用户激活
     * @author wangdong
     */
    public function activate($userId ,$ip)
    {
        $upData['logintime'] = time();
        $upData['userstatus'] = 1;
        $this->db->where('userid',$userId);
        $this->db->update('user_member', $upData);
        return true;
    }

    /**
     * 修改密码
     * @author wangdong
     */
    public function doEditPWD($params ,$userInfo)
    {
        $params['oldpwd'] = trim($params['oldpwd']);
        $params['newpwd'] = trim($params['newpwd']);
        $params['renewpwd'] = trim($params['renewpwd']);
        $error = array('status' => false);
        if(empty($params['oldpwd']))
        {
            $error['data'] = array('oldpwdmsg', '原密码不能为空！');
            $error['info'] = 'oldpwderr';
            return $error;
        }
        if(empty($params['newpwd']))
        {
            $error['data'] = array('newpwdmsg', '新密码不能为空！');
            $error['info'] = 'newpwderr';
            return $error;
        }
        if(strlen($params['newpwd']) < 6 || strlen($params['newpwd']) > 16)
        {
            $error['data'] = array('newpwdmsg', '新密码不能少于6位或大于16位！');
            $error['info'] = 'newpwderr';
            return $error;
        }
        if($params['newpwd'] != $params['renewpwd'])
        {
            $error['data'] = array('renewpwdmsg', '两次输入密码不一致，请确认新密码！');
            $error['info'] = 'renewpwderr';
            return $error;
        }

        $user = $this->getRow($userInfo['userid']);
        if($user['password'] != Security::cryptPassword($params['oldpwd']))
        {
            $error['data'] = array('oldpwdmsg', '原密码错误，请确认！');
            $error['info'] = 'oldpwderr';
            return $error;
        }
        $upData['password'] = Security::cryptPassword($params['newpwd']);
        $this->db->where('userid',$userInfo['userid']);
        $this->db->update('user_member', $upData);
        //清除记住密码的内容。
        set_cookie('autologin', 0, -1);
        set_cookie('autopwd', '', -1);
        set_cookie('autousername', '', -1);
        $error['status'] = 1;
        return $error;
    }
    /**
     * 修改密码
     * @author wangdong
     */
    public function cocoEditPassword($params)
    {
        $params['oldpwd'] = trim($params['password']);
        $params['newpwd'] = trim($params['newpassword']);
        //1001		没有接收数据
        //1002		用户名不能为空
        //1004		密码不能为空
        //1007		用户名和密码不匹配
        if(empty($params['oldpwd']))
        {
            $error['errorcode'] = 1004;
            $error['status'] = 'error';
            return $error;
        }
        if(empty($params['newpwd']))
        {
            $error['errorcode'] = 1004;
            $error['status'] = 'error';
            return $error;
        }
        //        if(strlen($params['newpwd']) < 6 || strlen($params['newpwd']) > 16)
        //        {
        //            $error['data'] = array('newpwdmsg', '新密码不能少于6位或大于16位！');
        //            $error['info'] = 'newpwderr';
        //            return $error;
        //        }

        $user =  $this->getUserInfo($params['username']);

        if(empty($user))
        {
            $error['errorcode'] = 1001;
            $error['status'] = 'error';
            return $error;
        }
        //        if($user['passwordtype']=='md5')
        //        {
        //            if($user['password'] != $params['oldpwd'])
        //            {
        //                $error['errorcode'] = 1007;
        //                $error['status'] = 'error';
        //                return $error;
        //            }
        //        }
        //        else
        //        {
        //            if($user['password'] != Security::cryptPassword($params['oldpwd']))
        //            {
        //                $error['errorcode'] = 1007;
        //                $error['status'] = 'error';
        //                return $error;
        //            }
        //        }
        $upData['password'] = $params['newpwd'];
        $this->db->where('username',$params['username']);
        $this->db->update('user_member', $upData);
        $error['status'] = 'success';
        return $error;
    }

    /**
     * 获取单个用户信息
     *
     * @brief M层
     * @author 王栋
     * @date 2013.10.15
     * @note 详细说明及修改日志
     */
    public function getUserInfo($username)
    {
        $query = $this->db->get_where('user_member' ,array('username' => $username));
        if ($query->num_rows() > 0)
        {
            $data = $query->row_array();
        }
        if(empty($data))
        {
            return false;
        }
        return $data;
    }

    /**
     * 获取单个创意信息
     *
     * @brief M层
     * @author 王栋
     * @date 2013.10.15
     * @note 详细说明及修改日志
     */
    public function getRow($userId = 0)
    {
        $query = $this->db->get_where('user_member' ,array('userid' => $userId));
        if ($query->num_rows() > 0)
        {
            $data = $query->row_array();
        }
        if(empty($data))
        {
            return false;
        }
        return $data;
    }
    /**
     * 发送用户激活邮件
     * @param  $userid
     * @param  $username
     * @param  $pwd 未加密的密码
     * @return null
     */
    public function sendMail($userid,$username,$pwd,$type=1)
    {
        $this->load->library('sendemail');
        if($type==1)
        {
            //发送注册邮件
            $this->sendemail->sendRegEmail($userid,$username,Security::cryptPassword($pwd,$username));
        }
        else
        {
            //发送密码重置邮件
            $this->sendemail->sendResetEmail($userid,$username,Security::cryptPassword($pwd,$username));
        }
    }

    /**
     * 重置用户密码
     *
     * @access public
     * @param $id 用户id
     * @param $parentid 父id
     * @param $newPassWord 新密码
     * @return bool
     */
    function resetUserPassword($newPassWord, $user)
    {
        $upData['password'] = Security::cryptPassword($newPassWord);
        $this->db->where('userid',$user['userid']);
        $this->db->update('user_member', $upData);
        $error['status'] = 1;
        return $error;
    }
	
    function generateSecKeyToUsers(){
    	$sql = "select userid, secretKey from user_member";
    	$query=$this->db->query($sql);
    	$users = $query->result_array();
    	if($users){
    		foreach($users as $user){
    			if(empty($user['secretKey'])){
    				$secretKey = getAppSecret();
    				$this->db->update('user_member', array('secretKey'=>$secretKey), array('userid'=>$user['userid']));
    			}
    		}
    	}
    	return;
    }
    
    function getSecretKey($userid){
    	$sql = "select secretKey from user_member where userid=".$userid;
    	$query = $this->db->query($sql);
    	$result = $query->result_array();
    	if($result)
    		return $result[0]['secretKey'];
    	else return '';
    }
}
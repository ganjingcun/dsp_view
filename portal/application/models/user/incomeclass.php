<?php
/**
 * 用户信息操作
 *
 * @brief M层
 * @author 王栋
 * @date 2013.10.15
 * @note 详细说明及修改日志
 */
class IncomeClass extends MY_Model{

    /**
     * 初始化
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('Security');
        $this->load->database();
    }

     
    /**
     * 获取单个用户信息
     *
     * @brief M层
     * @author 王栋
     * @date 2013.10.15
     * @note 详细说明及修改日志
     */
    public function getIncomeInfo($where=array())
    {
        $query = $this->db->get_where('ad_month_income' ,$where);
        if ($query->num_rows() > 0)
        {
            $data = $query->result_array();
        } 
        if(empty($data))
        {
            return false;
        }
        return $data;
    }
        /**
         * 取得开发者可结算金额 
         * 个人就是上个月的，公司则是所有未结算月份的综合
         */
        public  function getSumIncome($userid='',$accounttype=''){
                $where['userid'] = $userid;
                if($accounttype == '1'){
                        $where['month'] = $this->get_last_month();
                }  
                $where['status'] = '0';                
                $query = $this->db->get_where('ad_month_income' ,$where);
                if ($query->num_rows() > 0)
                {
                    $data = $query->result_array();
                } 
                if(empty($data))
                {
                    return false;
                }
                $sum_income = $this->get_sum($data);
                return $sum_income;

        }
        /**
         * 获取月份
         */
        function get_last_month($month_num=1){
                $last_month = date("Y-m",mktime(0, 0 , 0,date("m")-$month_num,1,date("Y")));
                return $last_month;
        }
        /**
         * 计算总和
         */
        function get_sum($data=array()){
                $sumIncome = 0;
                foreach($data as $key=>$val){
                        $sumIncome += $val['cincome'];
                }
                return $sumIncome;
        }
}
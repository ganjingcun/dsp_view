<?php
/* *用户信息操作
 * @filename: noticeboardclass.php
 * @brief M层
 * @author 王飞
 * @date 2015-04-03
 * @note 公告
 */
class NoticeBoardClass extends MY_Model{

    /**
     * 初始化
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('Security');
        $this->load->database();
    }
    
	/**
     * 获取所有当前可用的公告
	 * @author 王飞
	 * @date 2015-04-03
     */
    public function getAvailableNotices()
    {
//    	$where=array('status'=>1);
//        $query = $this->db->get_where('notice_board' ,$where);
//   		$data = $query->result_array();
//		if(empty($data))
//		{
//			return false;
//		}
//        return $data;
        
        $sql = "select * from notice_board where status=1 order by createtime desc";
    	$result = $this->db->query($sql);
    	$data=array();
		$data = $result->result_array();
   		if(empty($data))
		{
			return false;
		}
		return $data;
    }
}
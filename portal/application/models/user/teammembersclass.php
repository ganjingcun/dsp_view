<?php
/**
 * 用户信息操作
 *
 * @brief M层
 * @author 王栋
 * @date 2013.10.15
 * @note 详细说明及修改日志
 */
class TeamMembersClass extends MY_Model{

    /**
     * 初始化
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('Security');
        $this->load->database();
    }

    /**
     * 添加组成员
     *
     * @author wangdong
     * @date 2013.10.15
     * @note 详细说明及修改日志
     */
    public function doAddMember($params, $userInfo)
    {
        $error = array('status' => false);
        $error = $this->chkAddInfo($params);
        if($error['status'] == false)
        {
            return $error;
        }
        $memberCount = $this->getMemberList($userInfo['userid']);
        if(count($memberCount) >= 19)
        {
            $error['status'] = 0;
            $error['data'] = array('certtypemsg', '团队成员不能超过20位！');
            $error['info'] = 'certtypeerr';
            return $error;
        }
        $chkMember = $this->getMemberWhere(array('idnumber'=>$params['idnumber'], 'certtype'=>$params['certtype'], 'status'=>1, 'isdel'=>0, 'userid'=>$userInfo['userid']));
        if(!empty($chkMember))
        {
            $error['status'] = 0;
            $error['data'] = array('certtypemsg', '此证件已被添加！');
            $error['info'] = 'certtypeerr';
            return $error;
        }
        $chkAccount = $this->getAccountStatus($params, $userInfo);
        if(!$chkAccount)
        {
            $error['status'] = 0;
            $error['data'] = array('certtypemsg', '此证件不能与主账号证件相同！');
            $error['info'] = 'certtypeerr';
            return $error;
        }
        $data['certtype'] = $params['certtype'];
        $data['realname'] = $params['realname'];
        $data['idnumber'] = $params['idnumber'];
        $data['uploadify100'] = isset($params['uploadify100'])?$params['uploadify100']:'';
        $data['uploadify200'] = isset($params['uploadify200'])?$params['uploadify200']:'';
        $data['userid'] = $userInfo['userid'];
        $data['status'] = 1;
        $data['createtime'] = time();
        $id = $this->db->insert('team_members', $data);
        if($id > 0)
        {
            $error['status'] == 1;
            return $error;
        }
        else
        {
            $error['status'] = 0;
            $error['data'] = array('certtypemsg', '未知错误！');
            $error['info'] = 'certtypeerr';
            return $error;
        }
    }

    /**
     *
     */
    function getAccountStatus($params, $userInfo)
    {
        $where['status'] = 1;
        $where['accounttype'] = $userInfo['istype'];
        $where['userid'] = $userInfo['userid'];
        $accountInfo = $this->AccountInfoClass->getAccountByStatus($where);
        if($params['idnumber'] == $accountInfo['idnumber'] && $params['certtype'] == $accountInfo['certtype'])
        {
            return false;
        }
        else
        {
            return true;
        }
    }




    /**
     * 修改组成员
     *
     * @author wangdong
     * @date 2013.10.15
     * @note 详细说明及修改日志
     */
    public function doEditMember($params, $userInfo)
    {
        $error = array('status' => 0);
        $id = $params['id'];
        if(empty($id))
        {
            $error['data'] = array('certtypemsg', '未知错误！');
            $error['info'] = 'certtypeerr';
            return $error;
        }
        $error = $this->chkAddInfo($params);
        $memberInfo = $this->getMemberInfo($id, $userInfo['userid']);
        if(empty($memberInfo) || $memberInfo['auditstatus'] !=2)
        {
            $error['data'] = array('certtypemsg', '该用户不允许被编辑！');
            $error['info'] = 'certtypeerr';
            $error['status'] = 0;
            return $error;
        }
        if(!$error['status'])
        {
            return $error;
        }

        $chkMember = $this->getMemberWhere(array('idnumber'=>$params['idnumber'], 'certtype'=>$params['certtype'], 'status'=>1, 'isdel'=>0, 'id !='=>$params['id'], 'userid'=>$userInfo['userid']));
        if(!empty($chkMember))
        {
            $error['status'] = 0;
            $error['data'] = array('certtypemsg', '此证件已被添加！');
            $error['info'] = 'certtypeerr';
            return $error;
        }
        $chkAccount = $this->getAccountStatus($params, $userInfo);
        if(!$chkAccount)
        {
            $error['status'] = 0;
            $error['data'] = array('certtypemsg', '此证件不能与主账号证件相同！');
            $error['info'] = 'certtypeerr';
            return $error;
        }
        $data = $memberInfo;
        unset($data['id']);
        $data['certtype'] = $params['certtype'];
        $data['realname'] = $params['realname'];
        $data['idnumber'] = $params['idnumber'];
        if(!empty($params['uploadify100']))
        {
            $data['uploadify100'] = $params['uploadify100'];
        }
        if(!empty($params['uploadify200']))
        {
            $data['uploadify200'] = $params['uploadify200'];
        }
        $data['userid'] = $userInfo['userid'];
        $data['status'] = 1;
        $data['auditstatus'] = 0;
        $data['createtime'] = time();
        $data['reason'] = '';
        $id = $this->db->insert('team_members', $data);
        if($id > 0)
        {
            $this->db->where('id',$params['id']);
            $id = $this->db->update('team_members', array('status'=>2));
            $error['status'] == 1;
            return $error;
        }
        else
        {
            $error['status'] = 0;
            $error['data'] = array('certtypemsg', '未知错误！');
            $error['info'] = 'certtypeerr';
            return $error;
        }
    }

    /**
     * 修改组成员
     *
     * @author wangdong
     * @date 2013.10.15
     * @note 详细说明及修改日志
     */
    public function doEditRatio($params, $userInfo)
    {
        $error = array('status' => 0);
        if(array_sum($params) != 100)
        {
            $error['data'] = array('certtypemsg', '分配比例之和必须为100%！');
            $error['info'] = 'certtypeerr';
            return $error;
        }
        $this->load->model('user/UserInfoClass');
        $userinfo = $this->UserInfoClass->getRow($userInfo['userid']);
        $where['status'] = 1;
        $where['accounttype'] = $userinfo['accounttype'];
        $where['userid'] = $userInfo['userid'];
        $accountInfo = $this->AccountInfoClass->getAccountByStatus($where);
        if(empty($accountInfo))
        {
            $error['data'] = array('certtypemsg', '您更改的用户不存在！');
            $error['info'] = 'certtypeerr';
            return $error;
        }
        $memberList = $this->getMemberList($userInfo['userid']);
        $memberIds = null;
        $sumRatio = $params['g_ratio'];
        foreach ($memberList as $v)
        {
            if(isset($params['m_ratio_'.$v['id']]))
            {
                $memberIds[$v['id']] = $params['m_ratio_'.$v['id']];
                $sumRatio += $params['m_ratio_'.$v['id']];
            }
        }
        if($sumRatio != 100)
        {
            $error['data'] = array('certtypemsg', '分配比例之和必须为100%！');
            $error['info'] = 'certtypeerr';
            return $error;
        }
        $this->db->where('id',$accountInfo['id']);
        $id = $this->db->update('account_info', array('ratio'=>$params['g_ratio']));
        if($id > 0)
        {
            if(!empty($memberIds))
            {
                foreach ($memberIds as $k=>$v)
                {
                    $this->db->where('id', $k);
                    $this->db->update('team_members', array('ratio'=>$v));
                }
            }
            $error['status'] = 1;
            return $error;
        }
        else
        {
            $error['status'] = 0;
            $error['data'] = array('certtypemsg', '未知错误！');
            $error['info'] = 'certtypeerr';
            return $error;
        }
    }

    /**
     * 删除组成员
     *
     * @author wangdong
     * @date 2013.10.15
     * @note 详细说明及修改日志
     */
    public function doDelete($params, $userInfo)
    {
        $error = array('status' => 0);
        $id = $params['id'];
        if(empty($id))
        {
            $error['data'] = array('certtypemsg', '未知错误！');
            $error['info'] = 'certtypeerr';
            return $error;
        }
        $memberInfo = $this->getMemberInfo($id, $userInfo['userid']);
        if(empty($memberInfo))
        {
            $error['data'] = array('certtypemsg', '未知用户！');
            $error['info'] = 'certtypeerr';
            return $error;
        }
        if($memberInfo['auditstatus'] == 0)
        {
            $error['data'] = array('certtypemsg', '审核中的用户不允许被删除！');
            $error['info'] = 'certtypeerr';
            return $error;
        }
        if($memberInfo['ratio'] > 0)
        {
            $error['data'] = array('certtypemsg', '此成员的分配比例大于零，请先调整为零在删除！');
            $error['info'] = 'certtypeerr';
            return $error;
        }
        $data['isdel'] = 1;
        $this->db->where('id',$id);
        $id = $this->db->update('team_members', $data);
        if($id > 0)
        {
            $error['status'] = 1;
            return $error;
        }
        else
        {
            $error['status'] = 0;
            $error['data'] = array('certtypemsg', '未知错误！');
            $error['info'] = 'certtypeerr';
            return $error;
        }
    }

    /**
     * 检测广告组信息
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    private function chkAddInfo($params)
    {
        $error = array('status' => 0);
        if(empty($params['certtype']))
        {
            $error['data'] = array('certtypemsg', '请选择证件类型！');
            $error['info'] = 'certtypeerr';
            return $error;
        }
        if(empty($params['realname']))
        {
            $error['data'] = array('realnamemsg', '请填写证件姓名。');
            $error['info'] = 'realnameerr';
            return $error;
        }
        if(empty($params['idnumber']))
        {
            $error['data'] = array('idnumbermsg', '请填写证件号码。');
            $error['info'] = 'idnumbererr';
            return $error;
        }
        if(empty($params['uploadify100']))
        {
            $error['data'] = array('uploadify100msg', '请上传证件正面图片。');
            $error['info'] = 'uploadify100err';
            return $error;
        }
        if(empty($params['uploadify200']))
        {
            $error['data'] = array('uploadify200msg', '请上传证件背面图片。');
            $error['info'] = 'uploadify200err';
            return $error;
        }
        $error['status'] = 1;
        return $error;
    }

    /**
     * 获取账号基本信息
     * Enter description here ...
     * @param $appId
     * @param $userId
     */
    public function getMemberInfo($id,$userId)
    {
        $query = $this->db->get_where('team_members', array('id' => $id, 'userid' => $userId));
        if ($query->num_rows() > 0)
        {
            $data = $query->row_array();
        }
        if(empty($data))
        {
            return false;
        }
        return $data;
    }

    /**
     * 获取账号基本信息
     * Enter description here ...
     * @param $appId
     * @param $userId
     */
    public function getMemberWhere($params)
    {
        if(empty($params))
        {
            return false;
        }
        $query = $this->db->get_where('team_members', $params);
        if ($query->num_rows() > 0)
        {
            $data = $query->row_array();
        }
        if(empty($data))
        {
            return false;
        }
        return $data;
    }

    /**
     * 检测联系人信息
     *
     * @author wangdong
     * @date 2013.10.19
     * @note 详细说明及修改日志
     */
    private function chkContactInfo($params)
    {
        $error = array('status' => false);
        if(empty($params['contact']))
        {
            $error['data'] = array('contactmsg', '请填写联系人姓名。');
            $error['info'] = 'contacterr';
            return $error;
        }
        if(empty($params['mobile']))
        {
            $error['data'] = array('mobilemsg', '请填写手机号码。');
            $error['info'] = 'mobileerr';
            return $error;
        }
        if(empty($params['address']))
        {
            $error['data'] = array('addressmsg', '请填写联系地址。');
            $error['info'] = 'addresserr';
            return $error;
        }
        if(empty($params['qqnumber']))
        {
            $error['data'] = array('qqnumbermsg', '请填写您的QQ。');
            $error['info'] = 'qqnumbererr';
            return $error;
        }
        $error['status'] = true;
        return $error;
    }

    /**
     * 获取成员信息列表
     */
    public function getMemberList($userId)
    {
        $where['userid'] = $userId;
        $where['status'] = 1;
        $where['isdel'] = 0;
        $this->db->order_by("status asc, createtime desc");
        $query = $this->db->get_where('team_members', $where);
        if ($query->num_rows() > 0)
        {
            $data = $query->result_array();
        }
        if(empty($data))
        {
            return false;
        }
        return $data;
    }
}
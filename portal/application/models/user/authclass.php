<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 登陆用户信息获取
 *
 * @brief M层
 * @author 王栋
 * @date 2012.03.20
 * @note 详细说明及修改日志
 */
class AuthClass extends MY_Model
{
	private $username = '';
	private $password = '';

	public function __construct($username='',$password='')
	{
		$this->username = $username;
		$this->password = $password;
	}

	public function authentication()
	{
		return true;
	}

	public static function getCurrentUser()
	{
		AuthClass::instance()->load->library('session');
		$session= new CI_Session();
		return $session->userdata;
	}

	/**
	 * 创建一个对象
	 * @return AuthClass
	 */
	public static function instance()
	{
		return new AuthClass;
	}

	/**
	 * 获取登陆用户Id
	 */
	public static function getUserId()
	{
		$userInfo= self::getCurrentUser();
		return $userInfo['userid'];
	}

	/**
	 * 获取登陆用户账号
	 */
	public static function getUserName()
	{
		$userInfo= self::getCurrentUser();
		return $userInfo['username'];
	}

	/**
	 * 获取登陆用户类型 1 广告组,2 开发者
	 */
	public static function getUserType()
	{
		$userInfo= self::getCurrentUser();
		return $userInfo['usertype'];
	}

	/**
	 * 获取登陆用户姓名
	 */
	public static function getRealName()
	{
		$userInfo= self::getCurrentUser();
		return $userInfo['realname'];
	}

	/**
	 * 获取登陆用户电话
	 */
	public static function getTelephone()
	{
		$userInfo= self::getCurrentUser();
		return $userInfo['telephone'];
	}

	/**
	 * 设置登陆用户电话
	 */
	public static function setCookies($data)
	{
		AuthClass::instance()->load->library('session');
		$session= new CI_Session();
		$session->set_userdata($data);
	}
}
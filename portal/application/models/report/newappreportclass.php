<?php
include dirname(__FILE__).'/reportclass.php';
class NewAppReportClass extends ReportBase{
    /**
     * 初始化
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * 获取近两天收入
     */
    function getDayAppIncomeReport($ids)
    {
        if(empty($ids))
        {
            return false;
        }
        $sDate = date('Y-m-d', time()-86400);
        $eDate = date('Y-m-d', time());
        $condition = array();
        $condition['server']       = "server=ads_union";
        $condition['t']            = "t=d_all_cost";
        $condition['pkCanalId'] = "pkCanalId=in|".$ids;
        $condition['spkDay'] = 'sd=ge|'.strtotime($sDate);
        $condition['epkDay'] = 'ed=le|'.strtotime($eDate);
        $condition['s']  = "s=".urlencode("pkDay,sum(deductIncome) income");
        $condition['g']  = "g=pkDay";
        $list = $this->getDataSourceOnce($condition);
        $tmp = array('yesterday'=>'0.00', 'day'=>'0.00');
        if(!empty($list))
        {
            foreach($list as $val){
                if($val['pkDay'] == $sDate)
                {
                    $tmp['yesterday'] = formatmoney($val['income']);
                }
                if($val['pkDay'] == $eDate)
                {
                    $tmp['day'] = formatmoney($val['income']);
                }
            }
        }
        return $tmp;
    }

    /**
     * 获取图行数据
     * Enter description here ...
     * @param $ids
     * @param $sDate
     * @param $eDate
     * @param $adform
     * @param $pkAdId
     * @param $gpkAdId
     */
    function getAllGraphReport($ids, $sDate, $eDate)
    {
        if(empty($ids))
        {
            return false;
        }
        $t = 'd_all_cost';
        $condition = array();
        $condition['server']       = "server=ads_union";
        $condition['t']            = "t={$t}";
        $condition['pkCanalId'] = "pkCanalId=in|".$ids;
        $condition['spkDay'] = 'sd=ge|'.strtotime($sDate);
        $condition['epkDay'] = 'ed=le|'.strtotime($eDate);
        $condition['s']  = "s=".urlencode("pkDay,pkOsType,sum(deductIncome) income");
        $condition['g']  = "g=pkDay,pkOsType";
        $list = $this->getDataSourceOnce($condition);
        $tmp = array();
        foreach($list as $val)
        {
            $tmp[$val['pkOsType']][$val['pkDay']] = $val['income'];
        }
        $formatDate = mktimes($sDate,$eDate,true,true);
        foreach ($formatDate as $v)
        {
            if(!isset($tmp['iOS'][$v]))
            {
                $tmp['iOS'][$v] = 0;
            }
            if(!isset($tmp['Android'][$v]))
            {
                $tmp['Android'][$v] = 0;
            }
        }
        uksort($tmp['iOS'], 'dateKeySort');
        uksort($tmp['Android'], 'dateKeySort');
        return $tmp;
    }

    /**
     * 获取图行数据
     * Enter description here ...
     * @param $ids
     * @param $sDate
     * @param $eDate
     * @param $adform
     * @param $pkAdId
     * @param $gpkAdId
     */
    function getAllDeviceGraphReport($ids, $sDate, $eDate)
    {
        if(empty($ids))
        {
            return false;
        }
        $t = 'd_all_cost';
        $condition = array();
        $condition['server']       = "server=ads_union";
        $condition['t']            = "t={$t}";
        $condition['pkCanalId'] = "pkCanalId=in|".$ids;
        $condition['spkDay'] = 'sd=ge|'.strtotime($sDate);
        $condition['epkDay'] = 'ed=le|'.strtotime($eDate);
        $condition['s']  = "s=".urlencode("pkDeviceType,sum(deductIncome) income");
        $condition['g']  = "g=pkDeviceType";
        $list = $this->getDataSourceOnce($condition);
        $tmp = array();
        foreach($list as $val)
        {
            $tmp[$val['pkDeviceType']] = $val['income'];
        }
        return $tmp;
    }

    /**
     * 获取图行数据
     * Enter description here ...
     * @param $ids
     * @param $sDate
     * @param $eDate
     * @param $adform
     * @param $pkAdId
     * @param $gpkAdId
     */
    function getAllAdTypeGraphReport($ids, $sDate, $eDate)
    {
        if(empty($ids))
        {
            return false;
        }
        $t = 'd_all_cost';
        $condition = array();
        $condition['server']       = "server=ads_union";
        $condition['t']            = "t={$t}";
        $condition['pkCanalId'] = "pkCanalId=in|".$ids;
        $condition['spkDay'] = 'sd=ge|'.strtotime($sDate);
        $condition['epkDay'] = 'ed=le|'.strtotime($eDate);
        $condition['s']  = "s=".urlencode("pkAdType,sum(deductIncome) income");
        $condition['g']  = "g=pkAdType";
        $list = $this->getDataSourceOnce($condition);
        $tmp = array();
        foreach($list as $val)
        {
            $k = getAdType($val['pkAdType']);
            if(empty($k))
            {
                continue;
            }
            if(isset($tmp[$k]))
            {
                $tmp[$k] += $val['income'];
            }
            else
            {
                $tmp[$k] = $val['income'];
            }
        }
        return $tmp;
    }

    /**
     * 获取标格数据
     * Enter description here ...
     * @param $ids
     * @param $sDate
     * @param $eDate
     * @param $adform
     * @param $pkAdId
     * @param $gpkAdId
     */
    function getAllTableReport($ids, $sDate, $eDate)
    {
        if(empty($ids))
        {
            return false;
        }
        $t = 'd_all_cost';
        $condition = array();
        $condition['server']       = "server=ads_union";
        $condition['t']            = "t={$t}";
        $condition['pkCanalId'] = "pkCanalId=in|".$ids;
        $condition['spkDay'] = 'sd=ge|'.strtotime($sDate);
        $condition['epkDay'] = 'ed=le|'.strtotime($eDate);
        $condition['s']  = "s=".urlencode("pkAdType,acctype,pkOsType,sum(deductIncome) income");
        $condition['g']  = "g=pkAdType,acctype,pkOsType";
        $list = $this->getDataSourceOnce($condition);
        $tmp = array();
        foreach($list as $val)
        {
            //精品推荐为3，原生精品推荐为8.
            if($val['pkAdType'] == 8)
            {
                $val['pkAdType'] = 3;
            }
            if(isset($tmp[$val['pkOsType']][$val['acctype']][$val['pkAdType']]))
            {
                $tmp[$val['pkOsType']][$val['acctype']][$val['pkAdType']] += $val['income'];
            }
            else
            {
                $tmp[$val['pkOsType']][$val['acctype']][$val['pkAdType']] = $val['income'];
            }
        }
        return $tmp;
    }

    function getAllTableReport2($userid, $sDate, $eDate)
    {
        $sql = "select b.ostypeid,
                sum(if(a.adform=1, income, 0)) as income1,
                sum(if(a.adform=2, income, 0)) as income2,
                sum(if(a.adform=3, income, 0)) as income3
                from app_gatherlog as a left join app_info as b on a.appid=b.appid
                where b.userid=$userid and a.logdate between '$sDate' and '$eDate'
                group by b.ostypeid";
        $data = $this->db->query($sql)->result_array();
        if(empty($data))
        {
            return false;
        }

        $result = array_fill(0, 3, array_fill(0, 3, 0));
        foreach ($data as $row)
        {
            $result[$row['ostypeid']][0] = $row['income1'] * 1000000;
            $result[$row['ostypeid']][1] = $row['income2'] * 1000000;
            $result[$row['ostypeid']][2] = $row['income3'] * 1000000;
        }
        return $result;
    }

    function getAllGraphReport2($userid, $sDate, $eDate)
    {
        $sql = "select b.ostypeid, sum(income) as income, a.logdate
                from app_gatherlog as a left join app_info as b on a.appid=b.appid
                where b.userid=$userid and a.logdate between '$sDate' and '$eDate'
                group by b.ostypeid, a.logdate";
        $data = $this->db->query($sql)->result_array();
        if(empty($data))
        {
            return array('iOS'=>array_fill(0, 0, 0), 'Android'=>array_fill(0, 0, 0));
        }

        $daytotal = $this->diffBetweenTwoDays($eDate, $sDate) + 1;
        $result = array('iOS'=>array_fill(0, $daytotal, 0), 'Android'=>array_fill(0, $daytotal, 0));
        foreach ($data as $row)
        {
            $ostype = ($row['ostypeid'] == 1) ? 'Android' : 'iOS';
            $dayindex = $this->diffBetweenTwoDays($row['logdate'], $sDate);
            $result[$ostype][$dayindex] = $row['income'] * 1000000;
        }
        return $result;
    }

    function getDayAppIncomeReport2($userid)
    {
        $sDate = date('Y-m-d', time()-86400);
        $eDate = date('Y-m-d', time());

        $sql = "select sum(income) as income, a.logdate
                from app_gatherlog as a left join app_info as b on a.appid=b.appid
                where b.userid=$userid and a.logdate between '$sDate' and '$eDate'
                group by a.logdate";
        $data = $this->db->query($sql)->result_array();
        $result = array('day'=>0, 'yesterday'=>0);
        foreach ($data as $row)
        {
            if ($row['logdate'] == $sDate)
            {
                $result['yesterday'] = formatmoney($row['income'] * 1000000);
            }
            else if ($row['logdate'] == $eDate)
            {
                $result['day'] = formatmoney($row['income'] * 1000000);
            }
        }
        return $result;
    }

    private function diffBetweenTwoDays ($day1, $day2)
    {
        $second1 = strtotime($day1);
        $second2 = strtotime($day2);

        if ($second1 < $second2) {
            $tmp = $second2;
            $second2 = $second1;
            $second1 = $tmp;
        }
        return ($second1 - $second2) / 86400;
    }

    /**
     * 获取平台标格数据
     * Enter description here ...
     * @param $ids
     * @param $sDate
     * @param $eDate
     * @param $adform
     * @param $pkAdId
     * @param $gpkAdId
     */
    function getAllDeviceTableReport($ids, $sDate, $eDate)
    {
        if(empty($ids))
        {
            return false;
        }
        $t = 'd_all_cost';
        $condition = array();
        $condition['server']       = "server=ads_union";
        $condition['t']            = "t={$t}";
        $condition['pkCanalId'] = "pkCanalId=in|".$ids;
        $condition['spkDay'] = 'sd=ge|'.strtotime($sDate);
        $condition['epkDay'] = 'ed=le|'.strtotime($eDate);
        $condition['s']  = "s=".urlencode("acctype,pkDeviceType,sum(deductIncome) income");
        $condition['g']  = "g=acctype,pkDeviceType";
        $list = $this->getDataSourceOnce($condition);
        $tmp = array();
        $sum = 0;
        foreach($list as $val)
        {
            $tmp[$val['pkDeviceType']][$val['acctype']] = $val['income'];
            $sum += $val['income'];
        }

        foreach ($tmp as $k=>$v) {
            $tmp[$k]['sum'] = array_sum($v);
            if(isset($sum) && $sum > 0){
            	$tmp[$k]['ratio'] = $tmp[$k]['sum']*100/$sum;
            }
            else{
            	$tmp[$k]['ratio'] = 0.00;
            }
        }
        return $tmp;
    }

    /**
     * 获取平台标格数据
     * Enter description here ...
     * @param $ids
     * @param $sDate
     * @param $eDate
     * @param $adform
     * @param $pkAdId
     * @param $gpkAdId
     */
    function getAllAdTypeTableReport($ids, $sDate, $eDate)
    {
        if(empty($ids))
        {
            return false;
        }
        $t = 'd_all_cost';
        $condition = array();
        $condition['server']       = "server=ads_union";
        $condition['t']            = "t={$t}";
        $condition['pkCanalId'] = "pkCanalId=in|".$ids;
        $condition['spkDay'] = 'sd=ge|'.strtotime($sDate);
        $condition['epkDay'] = 'ed=le|'.strtotime($eDate);
        $condition['s']  = "s=".urlencode("acctype,pkAdType,sum(deductIncome) income");
        $condition['g']  = "g=acctype,pkAdType";
        $list = $this->getDataSourceOnce($condition);
        $tmp = null;
        $sum = 0;
        foreach($list as $val)
        {
            $k = getAdType($val['pkAdType']);
            if(empty($k))
            {
                continue;
            }
            if(isset($tmp[$k][$val['acctype']]))
            {
                $tmp[$k][$val['acctype']] += $val['income'];
            }
            else
            {
                $tmp[$k][$val['acctype']] = $val['income'];
            }
            $sum += $val['income'];
        }
        if(!empty($tmp))
        foreach ($tmp as $k=>$v) {
            $tmp[$k]['sum'] = array_sum($v);
        	if(isset($sum) && $sum > 0)
        	{
        		$tmp[$k]['ratio'] = $tmp[$k]['sum']*100/$sum;
        	}
        	else
        	{
        		$tmp[$k]['ratio'] = 0.00;
        	}
        }
        return $tmp;
    }

    /**
     * 获取平台标格数据
     * Enter description here ...
     * @param $ids
     * @param $sDate
     * @param $eDate
     * @param $adform
     * @param $pkAdId
     * @param $gpkAdId
     */
    function getAllAppTableReport($ids, $sDate, $eDate)
    {
        if(empty($ids))
        {
            return false;
        }
        $t = 'd_all_cost';
        $condition = array();
        $condition['server']       = "server=ads_union";
        $condition['t']            = "t={$t}";
        $condition['pkCanalId'] = "pkCanalId=in|".$ids;
        $condition['spkDay'] = 'sd=ge|'.strtotime($sDate);
        $condition['epkDay'] = 'ed=le|'.strtotime($eDate);
        $condition['s']  = "s=".urlencode("pkOsType,pkCanalId,pkAdType,sum(deductIncome) income");
        $condition['g']  = "g=pkOsType,pkCanalId,pkAdType";
        $list = $this->getDataSourceOnce($condition);
        $tmp = array();
        $sum = array('iOS'=>0,'Android'=>0);
        foreach($list as $val)
        {
            //精品推荐为3，原生精品推荐为8.
            if($val['pkAdType'] == 8)
            {
                $val['pkAdType'] = 3;
            }
            if(isset($tmp[$val['pkOsType']][$val['pkCanalId']][$val['pkAdType']]))
            {
                $tmp[$val['pkOsType']][$val['pkCanalId']][$val['pkAdType']] += $val['income'];
            }
            else
            {
                $tmp[$val['pkOsType']][$val['pkCanalId']][$val['pkAdType']] = $val['income'];
            }
            if(isset($sum[$val['pkOsType']]))
            {
                $sum[$val['pkOsType']] += $val['income'];
            }
        }

        foreach ($tmp as $k=>$v) {
            foreach ($v as $kk=>$vv)
            {
                $tmp[$k][$kk]['sum'] = array_sum($vv);
                if(isset($sum[$k]) && $sum[$k] > 0)
                {
                    $tmp[$k][$kk]['ratio'] = $tmp[$k][$kk]['sum']*100/$sum[$k];
                }
                else
                {
                    $tmp[$k][$kk]['ratio'] = 0.00;
                }
            }
        }
        return $tmp;
    }

    function getDataSourceOnce($condition){
        $data = getDataApi($condition,true);
        //        $data = getDataApi($condition,true,true);print_r($data);
        if(!empty($data['data']))
        return $data['data'];
        else
        return array();
    }
}
<?php /**
* @filename	: adgroupclass.php
* @encoding	: UTF-8
* @author		: funbox
* @datetime	: 2016-10-31 17:19:24
* @Description  : 广告model
*/

class AdStuffImgClass extends MY_Model{
    /**
     * 初始化
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('tools');
		$this->load->helper('db_exchange');
    }

	/**
	 * 获取创意图片列表
	 */
	public function getList($params, $userInfo)
	{
		if (isset($params['stuffid'])) {
			$this->db->where('stuffid', $params['stuffid']);
		}
		if (isset($params['stuffid_in'])) {
			$this->db->where_in('stuffid', $params['stuffid_in']);
		}
		$this->db->order_by('imgid', "desc");

		$this->db->from('ad_stuff_img');
		$this->db->join('ad_stuff_img_size', 'ad_stuff_img.sizeid = ad_stuff_img_size.sizeid');

		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0)
		{
			$data = $query->result_array();
		}
		if(empty($data))
		{
			return false;
		}
		return $data;
	}

	public function add($data) {

		$this->db->insert('ad_stuff_img', $data);
		$id = $this->db->insert_id();
		return $id;
	}
	public function addBatch($data) {

		$id = $this->db->insert_batch('ad_stuff_img', $data);
		//$id = $this->db->insert_id();
		return $id;
	}
}
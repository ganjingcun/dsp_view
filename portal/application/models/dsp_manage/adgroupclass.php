<?php /**
* @filename	: adgroupclass.php
* @encoding	: UTF-8
* @author		: funbox
* @datetime	: 2016-10-31 17:19:24
* @Description  : 广告model
*/

class AdGroupClass extends MY_Model{
    /**
     * 初始化
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('tools');
		$this->load->helper('db_exchange');
    }

	/**
	 * 获取广告列表
	 */
	public function getAdsList($params, $userInfo)
	{
		$this->db->where('userid', $userInfo['userid']);
		if(isset($params['status_in']))
		{
			$this->db->where_in('status', $params['status_in']);
		}
		$this->db->order_by('adgroupid', "desc");
		$query = $this->db->get('ad_group');
		if ($query->num_rows() > 0)
		{
			$data = $query->result_array();
		}
		if(empty($data))
		{
			return false;
		}
		return $data;
	}

	/**
	 * 获取广告列表
	 */
	public function getAdsInfo($adgroupid, $userInfo)
	{
		$this->db->where('userid', $userInfo['userid']);
		$this->db->where('adgroupid', $adgroupid);

		$query = $this->db->get('ad_group');
		if ($query->num_rows() > 0)
		{
			$data = $query->row_array();
		}
		if(empty($data))
		{
			return false;
		}
		return $data;
	}

	public function add($data) {

		$this->db->insert('ad_group', $data);
		$id = $this->db->insert_id();
		return $id;
	}

	public function save($data,$where) {

		$this->db->where('adgroupid', $where['adgroupid']);
		$this->db->where('userid', $where['userid']);
		$res = $this->db->update('ad_group', $data);

		return $res;
	}
}
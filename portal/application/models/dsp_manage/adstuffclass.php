<?php /**
* @filename	: adgroupclass.php
* @encoding	: UTF-8
* @author		: funbox
* @datetime	: 2016-10-31 17:19:24
* @Description  : 广告model
*/

class AdStuffClass extends MY_Model{
    /**
     * 初始化
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('tools');
		$this->load->helper('db_exchange');
    }

	/**
	 * 获取创意列表
	 */
	public function getList($params, $userInfo)
	{
		$this->db->where('userid', $userInfo['userid']);
		$this->db->where('adgroupid', $params['adgroupid']);
		if(isset($params['status_in']))
		{
			$this->db->where_in('status', $params['status_in']);
		}
		$this->db->order_by('stuffid', "desc");
		$query = $this->db->get('ad_stuff');
		if ($query->num_rows() > 0)
		{
			$data = $query->result_array();
		}
		if(empty($data))
		{
			return false;
		}
		return $data;
	}

	/**
	 * 获取创意详情
	 */
	public function getInfo($stuffid, $userInfo)
	{
		$this->db->where('userid', $userInfo['userid']);
		$this->db->where('stuffid', $stuffid);

		$query = $this->db->get('ad_stuff');
		if ($query->num_rows() > 0)
		{
			$row = $query->row_array();
		}
		if(empty($data))
		{
			return false;
		}
		return $data;
	}

	public function add($data) {

		$this->db->insert('ad_stuff', $data);
		$id = $this->db->insert_id();
		return $id;
	}
}
<!--  主体部分 [ -->
<style>
	.add_new_app {
		position: relative;
		margin: 10px 0;
		width: 80%;
		font-size: 16px;
		font-weight: 800;
		background: #fff0cb;
	}

	.add_new_app .cue {
		line-height: 30px;
		padding: 0px 30px 0px 20px;
		color: #e58527;
	}

	.add_new_app .cue .close {
		position: absolute;
		z-index: 1;
		right: 2px;
		top: 2px;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
		line-height: 12px;
		background: none;
		color: #666;
	}

	.add_new_app .cue .close:hover {
		color: #e58527;
	}

	.close {
		background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
		color: #666;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 16px;
		line-height: 12px;
		font-weight: 800;
		position: absolute;
		right: 2px;
		top: 2px;
		z-index: 1;
	}

	.center {
		text-align: center;
	}
</style>
<div class="inner_container" id="innerContainer">
	<section>
		<div id="content" class="content">
			<div class="right_content" id="rightContent">
				<header class="title">
					<strong class="title_strong"><i class="icon top_title_icon"></i>所有推广计划</strong>
				</header>
				<div>
					<table class="table table-bordered ">
						<thead>
						<tr>
							<th>推广计划名称</th>
							<th>推广目标类型</th>
							<th>系统类型</th>
							<th>日预算</th>
							<th>状态</th>
							<th></th>
						</tr>
						</thead>
						<tbody>
						<?php if (!empty($data)) :?>
							<?php foreach ($data as $value) :?>
								<tr key="<?php echo $value['campaignid'] ?>">
									<td style="width:30%"><?php echo $value['campaignname'];?></td>
									<td style="width:20%"><?php echo get_campaign_targettype($value['targettype']);?></td>
									<td style="width:10%"><?php echo get_campaign_ostypeid($value['ostypeid']);?></td>
									<td style="width:15%"><?php echo $value['daybudget']?formatmoney($value['daybudget']):'-'; ?></td>
									<td style="width:10%"><?php echo get_ads_status($value['status']);?></td>
									<td style="width:15%; position: static;">
										<a href="/dsp_manage/campaign/showinfo?campaignid=<?php echo $value['campaignid'];?>">查看详情</a>
										<a href="/dsp_manage/campaign/showedit?campaignid=<?php echo $value['campaignid'];?>">修改</a>
									</td>
								</tr>
							<?php endforeach;?>
						<?php else:?>
							<tr>
								<td colspan="8">
									<p class="ad_errer_info">
										<b class="red">*</b>您好，目前无任何广告信息，
										<a href="/dsp_manage/ads/showadd">去创建广告？</a>
									</p>
								</td>
							</tr>
						<?php endif;?>
						</tbody>
					</table>
					<br>
				</div>
			</div>
		</div>
	</section>
</div>
<?php $this->load->view("manage/inc/footer"); ?>
<!--  主体部分 [ -->
<div class="inner_container" id="innerContainer">
    <?php $this->load->view('manage/inc/left')?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <div class="jumbotron jumbotron_no_shadow add_app_box">
                    <header class="title">
                        <h3><strong  class="square_box"></strong><span>添加推广计划</span></h3>
                    </header>
                    <form id="frm" name="frm" method="post">
                        <div class="wrap">
							<div class="form-group">
								<span class="fl form-label"><i class="require_item">*</i>推广计划名称：</span>
								<span class="fl"><input class="form-control form-control-small" type="text" name="campaignname" id="campaignname" maxlength="50"></span>
								<span class="fl alert alert-small alert-warning" id="campaignnameerr" style="display: none"><i class="icon error_icon"></i><span id="campaignnamemsg"></span></span>
							</div>
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>投放类型：</span>
								<div class="platform fl">
									<label class="radio_style"><input type="radio" name="targettype" value="1">App下载</label>
									<label class="radio_style"><input type="radio" name="targettype" value="2">打开Website</label>
								</div>
								<span class="fl alert alert-small alert-warning" id="targettypeerr" style="display: none;"><i class="icon error_icon"></i><span id="targettypemsg">请选择投放类型！</span></span>
							</div>
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>系统类型：</span>
								<div class="platform fl">
									<label class="radio_style"><input type="radio" name="ostypeid" value="0">不限</label>
									<label class="radio_style"><input type="radio" name="ostypeid" value="1">Android</label>
									<label class="radio_style"><input type="radio" name="ostypeid" value="2">iOS</label>
								</div>
								<span class="fl alert alert-small alert-warning" id="ostypeiderr" style="display: none;"><i class="icon error_icon"></i><span id="ostypeidmsg">请选择系统类型！</span></span>
							</div>
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>投放币种：</span>
								<div class="platform fl">
									<label class="radio_style"><input type="radio" name="currency" value="1">人民币</label>
									<label class="radio_style"><input type="radio" name="currency" value="2">美元</label>
								</div>
								<span class="fl alert alert-small alert-warning" id="currencyerr" style="display: none;"><i class="icon error_icon"></i><span id="currencymsg">请选择投放币种！</span></span>
							</div>
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>广告活动类别：</span>
								<div class="platform fl">
									<label class="radio_style"><input type="radio" name="adtypeid" value="1">应用</label>
									<label class="radio_style"><input type="radio" name="adtypeid" value="27">游戏</label>
								</div>
								<span class="fl alert alert-small alert-warning" id="adtypeiderr" style="display: none;"><i class="icon error_icon"></i><span id="adtypeidmsg">请选择系统类型！</span></span>
							</div>
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>广告活动子类别：</span>
								<div class="platform fl">
									<label class="radio_style"><input type="radio" name="adchildtypeid" value="2">报刊杂志</label>
								</div>
								<span class="fl alert alert-small alert-warning" id="adchildtypeiderr" style="display: none;"><i class="icon error_icon"></i><span id="adchildtypeidmsg">请选择广告活动子类别！</span></span>
							</div>
						</div>
						<header class="title">
							<h4><strong  class="square_box"></strong><span>定向设置</span></h4>
						</header>
						<div class="wrap">
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>系统版本定向：</span>
								<div class="platform fl">
									<label class="radio_style"><input type="radio" name="osversionset" value="0">所有系统版本</label>
									<label class="radio_style"><input type="radio" name="osversionset" value="1">自行选择</label>
								</div>
								<span class="fl alert alert-small alert-warning" id="osversionseterr" style="display: none;"><i class="icon error_icon"></i><span id="osversionsetmsg">请选择系统版本定向！</span></span>
							</div>
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>设备类型定向：</span>
								<div class="platform fl">
									<label class="radio_style"><input type="radio" name="devicetypeset" value="0">所有设备类型</label>
									<label class="radio_style"><input type="radio" name="devicetypeset" value="1">自行选择</label>
								</div>
								<span class="fl alert alert-small alert-warning" id="devicetypeseterr" style="display: none;"><i class="icon error_icon"></i><span id="devicetypesetmsg">请选择设备类型定向！</span></span>
							</div>
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>网络类型定向：</span>
								<div class="platform fl">
									<label class="radio_style"><input type="radio" name="nettypeset" value="0">所有网络类型</label>
									<label class="radio_style"><input type="radio" name="nettypeset" value="1">自行选择</label>
								</div>
								<span class="fl alert alert-small alert-warning" id="nettypeseterr" style="display: none;"><i class="icon error_icon"></i><span id="nettypesetmsg">请选择网络类型定向！</span></span>
							</div>
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>运营商定向：</span>
								<div class="platform fl">
									<label class="radio_style"><input type="radio" name="operatorsset" value="0">所有运营商网络</label>
									<label class="radio_style"><input type="radio" name="operatorsset" value="1">自行选择</label>
								</div>
								<span class="fl alert alert-small alert-warning" id="operatorsseterr" style="display: none;"><i class="icon error_icon"></i><span id="operatorssetmsg">请选择运营商定向！</span></span>
							</div>
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>媒体类别定向：</span>
								<div class="platform fl">
									<label class="radio_style"><input type="radio" name="apptypeset" value="0">所有媒体类别</label>
									<label class="radio_style"><input type="radio" name="apptypeset" value="1">自行选择</label>
								</div>
								<span class="fl alert alert-small alert-warning" id="apptypeseterr" style="display: none;"><i class="icon error_icon"></i><span id="apptypesetmsg">请选择媒体类别定向！</span></span>
							</div>
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>地域定向：</span>
								<div class="platform fl">
									<label class="radio_style"><input type="radio" name="areaset" value="">所有国家和地区</label>
									<label class="radio_style"><input type="radio" name="areaset" value="CN">自定义中国大陆及港澳台地区</label>
									<label class="radio_style"><input type="radio" name="areaset" value="OVERSEA">自定义海外地域</label>
								</div>
								<span class="fl alert alert-small alert-warning" id="areaseterr" style="display: none;"><i class="icon error_icon"></i><span id="areasetmsg">请选择地域定向！</span></span>
							</div>
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>Dmp定向：</span>
								<div class="platform fl">
									<label class="radio_style"><input type="radio" name="dmpset" value="0">所有用户</label>
									<label class="radio_style"><input type="radio" name="dmpset" value="1">自行选择</label>
								</div>
								<span class="fl alert alert-small alert-warning" id="dmpseterr" style="display: none;"><i class="icon error_icon"></i><span id="dmpsetmsg">请选择Dmp定向！</span></span>
							</div>
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>平台新设备定向：</span>
								<div class="platform fl">
									<label class="radio_style"><input type="radio" name="newdeviceset" value="0">所有设备</label>
									<label class="radio_style"><input type="radio" name="newdeviceset" value="1">自行选择</label>
								</div>
								<span class="fl alert alert-small alert-warning" id="newdeviceseterr" style="display: none;"><i class="icon error_icon"></i><span id="newdevicesetmsg">请选择平台新设备定向！</span></span>
							</div>
							<!--
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>限制广告追踪定向：</span>
								<div class="platform fl">
									<label class="radio_style"><input type="radio" name="devicetypeset" value="0">所有设备</label>
									<label class="radio_style"><input type="radio" name="devicetypeset" value="1">关闭限制广告追踪设备</label>
								</div>
								<span class="fl alert alert-small alert-warning" id="devicetypeseterr" style="display: none;"><i class="icon error_icon"></i><span id="devicetypesetmsg">请选择设备类型定向！</span></span>
							</div>
							-->
						</div>
						<header class="title">
							<h4><strong  class="square_box"></strong><span>预算及效果</span></h4>
						</header>
						<div class="wrap">
                            <div class="form-group">
                                <span class="fl form-label"><i class="require_item">*</i>日预算：</span>
                                <span class="fl"><input class="form-control form-control-small" type="text" name="daybudget" id="daybudget" maxlength="50">元/天,请输入大于等于100的整数。</span>
                                <span class="fl alert alert-small alert-warning" id="daybudgeterr" style="display: none"><i class="icon error_icon"></i><span id="daybudgetmsg"></span></span>
                            </div>
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>关注效果：</span>
								<div class="platform fl">
									<label class="radio_style"><input type="radio" name="attention" value="0">关注点击</label>
									<label class="radio_style"><input type="radio" name="attention" value="1">关注曝光</label>
									<label class="radio_style"><input type="radio" name="attention" value="2">关注转化</label>
								</div>
								<span class="fl alert alert-small alert-warning" id="attentionerr" style="display: none;"><i class="icon error_icon"></i><span id="attentionmsg">请选择关注效果！</span></span>
							</div>
						</div>
						<header class="title">
							<h4><strong  class="square_box"></strong><span>预算及效果</span></h4>
						</header>
						<div class="wrap">
							<div class="form-group">
								<span class="fl form-label"><i class="require_item">*</i>投放周期：</span>
								<span class="fl">
									<input class="form-control form-control-small" type="text" name="starttime" id="starttime" maxlength="20" value="2016-11-01">至
									<input class="form-control form-control-small" type="text" name="endtime" id="endtime" maxlength="20" value="2016-11-01">
								</span>

							</div>
							<div class="form-group tab_box add_ios">
								<span class="fl form-label"><i class="require_item">*</i>投放时段：</span>
								<div class="platform fl">
									<label class="radio_style"><input type="radio" name="periodset" value="0">全天投放</label>
									<label class="radio_style"><input type="radio" name="periodset" value="">分时段投放</label>
								</div>
								<span class="fl alert alert-small alert-warning" id="periodseterr" style="display: none;"><i class="icon error_icon"></i><span id="periodsetmsg">请选择投放时段！</span></span>
							</div>
						</div>
                        <div class="function_btn">
                            <button type="submit" id="njh_add_app" class="btn btn-primary btn-primary-noboder">提交</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
<?php $this->load->view("manage/inc/footer");?>
<script>
$("#frm").submit(function(e){
	e.preventDefault();
	$("#njh_add_app").attr("disabled", 'disabled');
	$.post("/dsp_manage/campaign/doadd/", $('#frm').serialize(), function(data){
		if(data && data['status'] == 0){
			if(data['data'] != '')
			{
				$("#"+data['data'][0]).html(data['data'][1]);
			}
			$("#"+data['info']).show();
			$("#njh_add_app").removeAttr("disabled");
			return false;
		}else{
			alert('保存成功！');
			location.href="/dsp_manage/campaign/showlist";
		}
	}, 'json');
});
</script>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>控制面板 - 开发者 - Cocos Ads</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="<?php echo  base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo  base_url('assets/bootstrap/css/font-awesome.min.css') ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo  base_url('assets/dist/css/AdminLTE.min.css') ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo  base_url('assets/dist/css/skins/skin-blue-light.min.css') ?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <base target="main-frame">
</head>


<!-- ADD THE CLASS fixed TO GET A FIXED HEADER AND SIDEBAR LAYOUT -->
<!-- the fixed layout is not compatible with sidebar-mini -->
<body class="hold-transition skin-blue-light sidebar-mini" style="overflow-x:hidden;overflow-y:hidden;">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="/" target="_top" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>ds</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Cocos</b>Ads</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo  base_url('/public/version1.0/images/df.png') ?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo stripos($userinfo['username'], '@')>0?substr($userinfo['username'], 0, stripos($userinfo['username'], '@')):$userinfo['username'];?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo  base_url('/public/version1.0/images/df.png') ?>" class="img-circle" alt="User Image">

                <p>
                  <?php echo stripos($userinfo['username'], '@')>0?substr($userinfo['username'], 0, stripos($userinfo['username'], '@')):$userinfo['username'];?> - 开发者
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="/manage/userinfo/index" class="btn btn-default btn-flat">个人资料</a>
                </div>
                <div class="pull-right">
                  <a href="/webindex/logout" class="btn btn-default btn-flat" target="_top">退出</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active">
          <a href="/manage/appreport/gatherreport">
            <i class="fa fa-dashboard"></i> <span>概览</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-money"></i> <span>推广计划管理</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="/dsp_manage/campaign/showlist"><i class="fa fa-circle-o"></i> 推广计划列表</a></li>
            <li><a href="/dsp_manage/campaign/showadd"><i class="fa fa-circle-o"></i> 添加推广计划</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-th"></i> <span>广告管理</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="/dsp_manage/ads/showlist"><i class="fa fa-circle-o"></i> 广告列表</a></li>
            <li><a href="/dsp_manage/ads/showadd"><i class="fa fa-circle-o"></i> 添加广告</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <iframe id="main-frame" name="main-frame" src="/manage/appreport/gatherreport" frameborder="0" style="width:100%;"></iframe>

<!--     <div class="embed-responsive embed-responsive-16by9">
      <iframe name="main-frame" src="/manage/appreport/gatherreport"></iframe>
    </div> -->
  </div>
  <!-- /.content-wrapper -->

  <!-- =============================================== -->


<!-- ./wrapper -->

<!-- jQuery 2.2.0 -->
<script src="<?php echo  base_url('assets/plugins/jQuery/jQuery-2.2.0.min.js') ?>"></script>
<!-- Bootstrap 3.3.5 -->
<script src="<?php echo  base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo  base_url('assets/dist/js/app.js') ?>"></script>

</body>
</html>

<!--  主体部分 [ -->
<style>
	.add_new_app {
		position: relative;
		margin: 10px 0;
		width: 80%;
		font-size: 16px;
		font-weight: 800;
		background: #fff0cb;
	}

	.add_new_app .cue {
		line-height: 30px;
		padding: 0px 30px 0px 20px;
		color: #e58527;
	}

	.add_new_app .cue .close {
		position: absolute;
		z-index: 1;
		right: 2px;
		top: 2px;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
		line-height: 12px;
		background: none;
		color: #666;
	}

	.add_new_app .cue .close:hover {
		color: #e58527;
	}

	.close {
		background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
		color: #666;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 16px;
		line-height: 12px;
		font-weight: 800;
		position: absolute;
		right: 2px;
		top: 2px;
		z-index: 1;
	}

	.center {
		text-align: center;
	}
</style>
<div class="inner_container" id="innerContainer">
	<section>
		<div id="content" class="content">
			<div class="right_content" id="rightContent">
				<header class="title">
					<strong class="title_strong"><i class="icon top_title_icon"></i><?php echo $ads['adgroupname'].'(ID:'.$ads['adgroupid'].')';?>的广告创意</strong>
				</header>
				<div class="form-group">
					<div class="fl">
						<a class="btn btn-primary btn-primary-noboder" type="button" href="/dsp_manage/ads/addstuff?adgroupid=<?php echo $ads['adgroupid'];?>"><i class="icon look_table_icon"></i>添加创意</a>
					</div>
				</div>
				<div>
					<table class="table table-bordered ">
						<thead>
						<tr>
							<th>创意名称</th>
							<th>目标地址</th>
							<th>创意类型</th>
							<th>创意</th>
							<th>启用</th>
							<th>状态</th>
							<th></th>
						</tr>
						</thead>
						<tbody>
						<?php if (!empty($stuff)) :?>
							<?php foreach ($stuff as $value) :?>
								<tr key="<?php echo $value['stuffid'] ?>">
									<td style="width:25%"><?php echo $value['adstuffname'];?></td>
									<td style="width:20%"><a href="<?php echo $value['target'];?>" target="_blank"><?php echo $value['target'];?></a></td>
									<td style="width:10%"><?php echo get_stufftype($value['stufftype']);?></td>
									<td style="width:10%">
										<?php if (!empty($stuff_imgs[$value['stuffid']])) :?>
											<?php foreach ($stuff_imgs[$value['stuffid']] as $item) :?>
												<a href="<?php echo $this->config->item('pic_url').$item['path'];?>" target="_blank"><?php echo $item['width'].'×'.$item['height'];?></a><br>
											<?php endforeach;?>
										<?php else: ?>
											-
										<?php endif;?>
									</td>
									<td style="width:5%"><?php echo ($value['ispause']==0)?'是':'否'; ?></td>
									<td style="width:10%"><?php echo get_stuffstatus($value['status']);?></td>
									<td style="width:20%; position: static;">
										<a href="/dsp_manage/ads/showinfo?adgroupid=<?php echo $value['adgroupid'];?>">查看详情</a>
										<!--<a href="/dsp_manage/ads/showedit?adgroupid=<?php echo $value['adgroupid'];?>">修改</a>-->
									</td>
								</tr>
							<?php endforeach;?>
						<?php endif;?>
						</tbody>
					</table>
					<br>
				</div>
			</div>
		</div>
	</section>
</div>
<?php $this->load->view("manage/inc/footer"); ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Cocos Ads-移动开发者快速变现</title>
<link rel="icon" href="../../favicon.ico">
<script type="text/javascript"> if(top!=this){top.location.href = location.href;}</script>
<meta name="keywords" content="SSP,Ads,Cocos,Unity,流量变现,移动广告,炒股软件" />
<meta name="description" content="Cocos Ads是全球著名的移动游戏引擎cocos2d-x的生态产品之一，专注于游戏的移动广告网络。Cocos Ads通过与创新的编辑器Cocos Creator深度耦合来让开发者无比轻松地通过轻点几次鼠标即可为APP嵌入广告，并通过聚合广告网络来为App开发者最大化地通过现有的用户群赚取更多的钱。" />
<link href="http://cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
<script src="http://cdn.bootcss.com/jquery/1.11.3/jquery.min.js"></script>
<script src="http://cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="/public/cocos/js/jquery.cookie.js"></script>
<link href="/public/cocos/css/index.css" rel="stylesheet">
<script src="/public/cocos/js/index.js"></script>
<script src="/public/cocos/js/banner.js"></script>
</head>

<body class="body">
<!-- nav -->
<div class="header1">
  <div class="w1150 mg0 clearfix">
    <div class="header_left">
      <div class="header_logo clearfix" id="logo_min"><img src="/public/cocos/images/logo.png" class="logo_min"></div>
    </div>
    <div class="header_left" style="padding-top: 50px; color: #ff0000;">
      <b>beta</b>
    </div>
    <div class="header_right">
      <div class="nav">
        <ul class="clearfix">
          <li class="nav_li"><a href=""><span class="nav_span ft16 now">首 页</span></a></li>
          <!--<li class="nav_li"><a href=""><span class="nav_span ft16">广告样式</span></a></li>-->
          <li class="nav_li"><a href="<?php if ($username) {
            echo '/manage/appinfo/index/1';
          } else {
            echo 'https://passport.cocos.com/sso/signin?client_id='.$this->config->item('sso_client_id').'&url='.$this->config->item('base_url').'/manage/appinfo/index/1';
          }
          ?>"><span class="nav_span ft16">SDK下载</span></a></li>
          <li class="nav_li"><a href="/manual/index.html"><span class="nav_span ft16">帮助中心</span></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>

<!-- banner -->
<div class="section bg_fff">
  <div id="banner" class="banner">
    <ul class="clearfix banner_ul">
      <li class="ban_li ban_0"></li>
      <li class="ban_li ban_1"><a href="/" target="_blank" class="link_a"></a></li>
    </ul>
    <div class="ban_bt"><i class="bt_lf"></i><i class="bt_ct"><span class="now"></span><span></span></i><i class="bt_rg"></i></div>
    <div class="ban_icon_main">
      <div class="ban_icon ban_lf"></div>
      <div class="ban_icon ban_rg"></div>
    </div>
    
    <!--登录前 -->
    <div class="login_box">
      <div class="login_main"> 
        <!-- 登录背景-->
        <div class="login_bg"></div>

        <div class="login_cont" id="login_cont">
          <div class="login_cont_list clrfff">
            <div class="lg_cont_tit">
              <div class="lg_tit_right flt_rg ft12">
                <div id="lg_tit_reg">
                  <a href="https://passport.cocos.com/auth/signup?client_id=<?php echo $this->config->item('sso_client_id')?>&url=<?php echo $this->config->item('base_url')?>" class="clrfff"><span class="lg_tit_rg_sp">立即注册</span></a>
                  <span class="lg_tit_rg_sp clre8 pd_lr10">|</span>
                  <a href="https://passport.cocos.com/auth/forget_pass" class="clrfff"><span class="lg_tit_rg_sp">忘记密码</span></a>
                </div>
                <div id="lg_tit_info"><span id="lg_tit_acc"></span> （<a href="/webindex/logout" class="clrfff">退出</a>）</div>
              </div>
              <span class="lg_tit_sp ft16" id="lg_tit_sp">帐户概览</span> </div>

              <!-- 输入框 -->
              <!--
              <ul class="lg_ul">
                <li class="lg_ip_li bg_tel ">
                  <input type="text" class="ip_text clr999" value="邮箱" id="username" name="account" />
                </li>
                <li class="lg_ip_li bg_pwd">
                  <input type="text" class="ip_text clr999" value="密码" id="lg_pwd" name="password" />
                  <input type="password" class="ip_text bg_pwd" id="password" />
                  <input type="hidden" value="" id="tourl" class="txt" />
                </li>
                <li>
                  <label><input type="checkbox" name="auto_sign"> 记住登录</label>
                </li>
              </ul>
              -->

              <div style="height:36px"></div>

              <!-- 帐户概览 -->
              <div class="lg_bt_list ft12">
                <span class="lg_bt_span wd0">今日收入</span>
                  <span class="lg_bt_span wd1" id="jsr_value">（尚未登录）</span>           
              </div>
              <div class="lg_bt_list ft12">
                <span class="lg_bt_span wd0">昨日收入</span>
                  <span class="lg_bt_span wd1" id="zsr_value">（尚未登录）</span>
              </div>
              <div class="lg_bt_list ft12">
                <span class="lg_bt_span wd0">账户余额</span>
                  <span class="lg_bt_span" id="cnt_value">（尚未登录）</span>
              </div>


            <div class="lg_ip_button">
              <input type="button" class="ip_button" id="btn_login" value="登 录" />
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
<br>
<br>
<div class="container w1150"> 
  <!-- Big Links -->
  <div class="row row-md-flex row-md-flex-wrap">
    <div class="col-sm-6 col-md-3 col-lg-3">
      <div class="well text-center">
        <div class="stat-icon"> <span class="glyphicon glyphicon-download"></span> </div>
        <div class="stat-number"> <a href="<?php if ($username) {
            echo '/manage/appinfo/index/1';
          } else {
            echo 'https://passport.cocos.com/sso/signin?client_id='.$this->config->item('sso_client_id').'&url='.$this->config->item('base_url').'/manage/appinfo/index/1';
          }
          ?>">下载 SDK</a> </div>
        <div>最新版本：<b>0.1.0</b></div>
      </div>
    </div>
    <div class="col-sm-6 col-md-3 col-lg-3">
      <div class="well text-center">
        <div class="stat-icon"> <span class="glyphicon glyphicon-book"></span> </div>
        <div class="stat-number"> <a href="/manual/index.html">阅读用户手册</a> </div>
        <div>清晰完善的文档</div>
      </div>
    </div>
    <div class="col-sm-6 col-md-3 col-lg-3">
      <div class="well text-center">
        <div class="stat-icon"> <span class="glyphicon glyphicon-user"></span> </div>
        <div class="stat-number"> <a href="">到论坛讨论</a> </div>
        <div>加入我们的社区</div>
      </div>
    </div>
    <div class="col-sm-6 col-md-3 col-lg-3">
      <div class="well text-center">
        <div class="stat-icon"> <span class="glyphicon glyphicon-random"></span> </div>
        <div class="stat-number"> <a href="">联系我们的商务</a> </div>
        <div>商务QQ: 5226880</div>
      </div>
    </div>
  </div>
  
  <!-- Features -->
  <hr/>
  <div class="row">
    <h2 class="ft36">为什么使用 Cocos Ads？</h2>
    <br>
    <div class="col-sm-12 col-md-6 features">
      <div class="feature">
        <div class="feature-icon"> <span class="glyphicon glyphicon-star-empty"></span> </div>
        <h5>小巧的SDK</h5>
        <p>Cocos Ads 包括用户手册的安装包仅有 5MB 大小。</p>
      </div>
      <div class="feature">
        <div class="feature-icon"> <span class="glyphicon glyphicon-book"></span> </div>
        <h5>清晰与完善的文档</h5>
        <p> Cocos Ads 安装包中包含《用户手册》，手册囊括了入门介绍、教程、DEMO，“手把手”指导，还包括了SDK的API文档。<br>
          &nbsp; </p>
      </div>
      <div class="feature">
        <div class="feature-icon"> <span class="glyphicon glyphicon-hdd"></span> </div>
        <h5>广泛兼容各种平台</h5>
        <p> Cocos Ads 除支持用Cocos引擎开发的游戏外，更支持原生Android、iOS应用。 </p>
      </div>
      <div class="feature">
        <div class="feature-icon"> <span class="glyphicon glyphicon-send"></span> </div>
        <h5>不喜欢复杂，热爱简约</h5>
        <p>Cocos Ads 的API优雅而简约，最小情况下您只需要2行代码即可显示您的广告。通过Cocos Creator，您甚至可以只须轻点几次鼠标即可完成SDK的集成工作。</p>
      </div>
      <div class="feature">
        <div class="feature-icon"> <span class="glyphicon glyphicon-road"></span> </div>
        <h5>简约而不简单</h5>
        <p> 除提供简约的快速API外，我们还为高级用户预留了更多接口，满足各种定制功能。Cocos Ads 是来帮你的，而不是来添乱的。 </p>
      </div>
    </div>
    <div class="col-sm-12 col-md-6 features">
      <div class="feature">
        <div class="feature-icon"> <span class="glyphicon glyphicon-dashboard"></span> </div>
        <h5>出色的性能</h5>
        <p>Cocos Ads 的性能始终优于大多数同类产品，我们极大限度地进行优化，以减少您用户的宝贵网络流量，来提升您APP的留在率。</p>
      </div>
      <div class="feature">
        <div class="feature-icon"> <span class="glyphicon glyphicon-ok"></span> </div>
        <h5>业界领先的ECPM</h5>
        <p>对于来自Cocos Ads的合格玩家，广告商支付更高的费用，给您更高的eCPM</p>
      </div>
      <div class="feature">
        <div class="feature-icon"> <span class="glyphicon glyphicon-list-alt"></span> </div>
        <h5>极高的填充率</h5>
        <p>Cocos Ads 高达99%超高填充率，业界领先</p>
      </div>
      <div class="feature">
        <div class="feature-icon"> <span class="glyphicon glyphicon-leaf"></span> </div>
        <h5>付款速度快</h5>
        <p>这难道不是我们所希望的吗？Cocos Ads 结算单价高，付款速度快</p>
      </div>
      <div class="feature">
        <div class="feature-icon"> <span class="glyphicon glyphicon-thumbs-up"></span> </div>
        <h5>广告展现形式丰富</h5>
        <p>Cocos Ads 拥有Banner、插屏广告、推荐墙、视频广告等丰富的展现形式</p>
      </div>
    </div>
  </div>
  <br>
  <br>
  <hr/>
  <h2>友情链接</h2>
  <div> <a href="http://www.cocoachina.com" target="_blank">CocoaChina</a><span>|</span><a href="http://cn.cocos2d-x.org/" target="_blank">Cocos引擎中文官网</a><span>|</span> </div>
  <div class="text-center"> 2015 @ Cocos.com  沪ICP备14008742号-5 <a style="color:#989da4;width:80px;background:url();margin-right:-1px;" href="http://www.cocos.com/about_us/" target="_blank">关于我们</a> </div>
  <br>
</div>
</body>
</html>
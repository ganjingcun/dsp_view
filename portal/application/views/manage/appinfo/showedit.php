<!--  主体部分 [ -->
<div class="inner_container" id="innerContainer">
    <?php $this->load->view('manage/inc/left')?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <?php $this->load->view('manage/inc/righttop')?>
                <div class="inner_tab">
                    <ul class="tab_item">
                        <li><a href="/manage/appposition/showinfo/?appid=<?php echo $appid?>">广告位管理</a></li>
                        <li><a href="/manage/appchannel/showinfo/?appid=<?php echo $appid?>">渠道管理</a></li>
                        <li><a href="/manage/apptag/showinfo/?appid=<?php echo $appid?>">标签管理</a></li>
                        <li class="active"><a href="javascript:;">基本信息</a></li>
                         <?php if($isintegral == 1){  ?><li ><a href="/manage/appinfo/backconfig/?appid=<?php echo $appid?>">积分回调方式</a></li><?php } ?>
                    </ul>
                    <div class="jumbotron jumbotron_no_shadow tab_box base_info" >
                        <div id="editInfo" class="edit_info" style="padding-left: 10px;">
                            <div class="form-group">
                                <span class="fl form-label">系统类型：</span>
                                <span class="fl values">
                                    <?php
                                    if($ostypeid == 1)
                                    {
                                        echo 'Android';
                                    }
                                    elseif($ostypeid == 2)
                                    {
                                        echo 'iOS';
                                    }?>
                                    <input type="text" hidden id="appid" name="appid" value="<?php echo $appid?>" >
                                </span>
                            </div>
                            <div class="form-group">
                                <span class="fl form-label">平台类型：</span>
                                <span class="fl values">
                                    <?php if($devicetypeid == 1) echo '无'?>
                                    <?php if($devicetypeid == 2) echo 'iPhone'?>
                                    <?php if($devicetypeid == 3) echo 'iPad'?>
                                    <?php if($devicetypeid == 4) echo 'Universal'?>
                                </span>
                            </div>
                            <div class="form-group">
                                <span class="fl form-label"><i class="require_item">*</i>应用名称：</span>
                                <span class="fl values">
                                    <input type="text" class="fl form-control form-control-small" name="appname" id="appname" value="<?php echo $appname?>" placeholder="应用名称">
                                    <span class="fl alert alert-small alert-warning" style="margin-left: 10px;line-height:18px; display: none" id="appnameerr"><i class="icon error_icon"></i><span id="appnamemsg"></span></span>
                                </span>
                            </div>
                            <div class="form-group">
                                <span class="fl form-label"><i class="require_item">*</i>应用包名称：</span>
                                <span class="fl values">
                                    <input type="text" class="fl form-control form-control-small" name="packagename" id="packagename" value="<?php echo $packagename?>" placeholder="应用包名称">
                                    <span class="fl alert alert-small alert-warning" style="margin-left: 10px;line-height:18px;display: none" id="packagenameerr"><i class="icon error_icon"></i><span id="packagenamemsg"></span></span>
                                </span>
                            </div>


                            <div class="form-group">
                                <span class="fl form-label">积分墙：</span>
                                <span class="fl values"><?php if($isintegral == 1){echo '已开启';}else{echo '未开启';}?><span style="color:red; margin-left:15px;">*开启积分墙后，才能够给应用添加积分墙广告位。</span></span>
                            </div>
                            <div class="form-group" style="display: <?php if($isintegral == 0){echo 'none';}?>;">
                                <span class="fl form-label"><span class="red">*</span>兑换比例：</span>
                                <span class="fl values">
                                    <span style="font-family: 'arial'">&yen;</span> 1  = <?php echo $integration?>&nbsp;&nbsp;<?php echo empty($unit)? '金币':$unit;?>
                                    <span class="fl alert alert-small alert-warning" style="margin-left: 10px;line-height:18px;display: none" id="integrationerr"><i class="icon error_icon"></i><span id="integrationmsg">请填写正确对兑换比例。</span></span>
                                </span>
                            </div>

                            <div class="form-group">
                                <span class="fl form-label">下载地址：</span>
                                <span class="fl">
                                    <input type="text" name="appurl" id="appurl"  class="fl form-control form-control-small"  value="<?php echo $appurl?>" placeholder="请填写正确的下载地址">
                                    <span class="fl alert alert-small alert-warning" style="margin-left: 10px;line-height:18px;display: none" id="appurlerr"><i class="icon error_icon"></i><span id="appurlmsg"></span>请填写正确的下载地址。</span>
                                </span>
                            </div>

                            <div class="form-group">
                                <span class="fl form-label"><i class="require_item">*</i>应用分类：</span>
                                <span class="fl app_classify_fixed" style="width:750px;">
                                    <span>
                                        <?php
                                        foreach ($apptype as $k=>$v)
                                        {
                                            ?>
                                            <?php if ($apptypeid == $k){ echo $v['typename'];}?></option>
                                        <?php
                                        }
                                        ?>
                                    </span>

                                    <?php
                                    foreach ($apptype as $k=>$v)
                                    {
                                        ?>
                                        <div id="form_way_<?php echo $k?>" class="check_labels" style="display: <?php if ($apptypeid != $k) echo 'none'?>;">
                                            <?php
                                            if(isset($v['childtype']))
                                            {
                                                foreach ($v['childtype'] as $cv)
                                                {
                                                    ?>
                                                    <span class="track
                                                        <?php
                                                    if(isset($childtypename) && !empty($childtypename))
                                                    {
                                                        foreach ($childtypename as $value) {
                                                            if($value == $cv['typename'])
                                                            {
                                                                echo "checked";
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                    " name="appchildtypeid" data-apptypeid="<?php echo $cv['apptypeid']?>"><i class="icon icon_track"></i><?php echo $cv['typename']?></span>

                                                <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                    <?php
                                    }
                                    ?>

                                </span>
                                <br style="clear:left;">
                                <span class="alert alert-small alert-warning" id="appchildtypeiderr" style="margin-left: 220px; display: none;"><i class="icon error_icon"></i><span id="appchildtypeidmsg">请选择应用分类。</span></span>
                            </div>
                            <div class="form-group">
                                <span class="fl form-label">应用介绍：</span>
                                <span class="fl">
                                    <textarea class="form-control" maxlength="500" name="appdescription" id="appdescription"  placeholder="写点什么吧......"><?php echo $appdescription?></textarea>
                                </span>
                                <span class="fl alert alert-small alert-warning" style="margin-left: 10px;display: none; line-height:18px;" id="appdescriptionerr"><i class="icon error_icon"></i><span id="appdescriptionmsg">请选择应用分类。</span></span>
                            </div>

                            <div class="btn-group base_info_btn_group" style="margin-left: 220px;">
                                <a href="javascript:;" id="btnEditDone" class="btn btn-success btn-success-noboder">保&nbsp;&nbsp;&nbsp;&nbsp;存</a>
                                <a href="/manage/appinfo/showinfo/?appid=<?php echo $appid; ?>" id="btnEditCancel" class="btn btn-default btn-default-noboder">取&nbsp;&nbsp;&nbsp;&nbsp;消</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
</div>
<!-- ] 主体部分 -->

<?php $this->load->view("manage/inc/footer");?>

<script>
    var r = /^\+?[0-9][0-9]*$/;

    $(document).ready(function(){
        $('.check_labels').on('click','.track',function(){
            $(this).toggleClass('checked');
        })
        $("#btnEditDone").click(function(){
            var appname = $("#appname").val();
            var packagename = $("#packagename").val();
            var appurl = $("#appurl").val();
            var appid = $("#appid").val();
            var appdescription = $("#appdescription").val();
            var appchildtypeid = '';
            var isintegral = $('input:checkbox[name="isintegral"]:checked').val();

            var integration = $("#integration").val();
            var unit = $("#unit").val();
            $(".app_classify_fixed .track").each(function(){
                if($(this).hasClass("checked") == true)
                {
                    appchildtypeid+=$(this).data('apptypeid')+",";
                }
            })
            $("#ostypeiderr").hide();
            $("#devicetypeiderr").hide();
            $("#appnameerr").hide();
            $("#apptypeiderr").hide();
            $("#packagenameerr").hide();
            $("#integrationerr").hide();
            $("#uniterr").hide();

            $.post("/manage/appinfo/doedit",
                {
                    appname:appname,
                    packagename:packagename,
                    appurl:appurl,
                    appid:appid,
                    appdescription:appdescription,
                    appchildtypeid:appchildtypeid,
                    integration:integration,
                    unit:unit,
                    isintegral:isintegral,
                    r:Math.random()
                },
                function(data)
                {
                    if(data && data['status'] == 0){
                        if(data['data'] != '')
                        {
                            $("#"+data['data'][0]).html(data['data'][1]);
                        }
                        $("#"+data['info']).show();
                        return false;
                    }else{
                        location.href = '/manage/appinfo/showinfo/?appid='+data['data'][0];
                    }
                },
                'json');
            return false;
        });
    });
    $("#isintegral").click(function(){
        //$(this).attr("checked");
        //$(this).is(":checked");
        if($(this).is(":checked"))
        {
            $("#isintegraldiv").show();
        }
        else
        {
            $("#isintegraldiv").hide();
        }
    })
</script>
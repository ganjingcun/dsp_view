<!--  主体部分 [ -->
<div class="inner_container" id="innerContainer">
    <?php $this->load->view('manage/appinfo/apileft')?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <header class="title">
                    <strong class="title_strong"><i class="icon top_title_icon"></i>API下载</strong>
                </header>
                <div class="sdk_slides">
                    <div class="slides_container">
                         <div class="inner">
                            <div class="box">
                                <div class="fl"></div>
                                <div class="fl down_content">
                                    <h3>CocosAds 广告API</h3>
                                    <a href="javascript:;" id="iosdown" style="margin-top:30px;" class="small-btn small-btn-primary"><i class="icon download_icon"></i>API&nbsp;下载</a>
                                    <div class="detail">
                                    	<?php if($apiinfo == null || $apiinfo == ""){?>
                                    		<p><label><h1>暂无版本信息</h1></label></p>
                                    	<?php }
                                    	else{?>
	                                        <p><label>版本：</label><?php echo $apiinfo['version']?></p>
	                                        <p><label>大小：</label><?php echo $apiinfo['size']?> </p>
	                                        <p><label>更新时间：</label><?php echo date("Y-m-d",$apiinfo['updatetime'])?></p>
	                                        <p><label>md5sum：</label><?php echo $apiinfo['md5sum']?></p>
                                        <?php }?>
                                    </div>
                                    <div class="news">
                                        <h4>更新动态</h4>
                                        <p><?php echo $apiinfo['content']?></p>
                                    </div>
                                    <div><a href="/manage/appinfo/showapiinfolist">历史版本</a></div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
</div>
<?php $this->load->view("manage/inc/footer");?>
<script>
$(function(){
	$("#iosdown").click(function(){
		window.open("<?php echo $apiinfo['packpath']?>", 'down');
	});
});
</script>
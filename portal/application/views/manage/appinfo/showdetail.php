<div class="inner_container" id="innerContainer">
    <?php $this->load->view('manage/inc/left')?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <div class="jumbotron jumbotron_no_shadow add_app_box">
                    <header class="title">
                        <h3><strong  class="square_box"></strong><span>添加应用</span></h3>
                    </header>
                    <div class="add_app_step add_app_step_03"></div>

                    <div class="wrap">
                        <div class="info">
                            <h3>您已经成功添加APP,接下来您可以：</h3>
                            <div class="function_btn">
                                <a href="/manage/appinfo/showsdkinfo" class="btn btn-success btn-success-noboder" target="_blank"><i class="icon download_icon"></i>下载并嵌入SDK</a>
                                <a href="/manage/appchannel/showinfo/?appid=<?php echo $appid?>" class="btn btn-primary btn-primary-noboder"><i class="icon right_icon"></i>前往详情页提交审核</a>
                            </div>
                        </div>
                        <table class="table table-bordered table-bordered-rlborder">
                            <thead>
                                <tr>
                                    <th colspan="2">应用信息</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    <label>系统类型：</label>
                                    <?php
                                    if($ostypeid == 1)
                                    {
                                        echo 'Android';
                                    }
                                    elseif($ostypeid == 2)
                                    {
                                        echo 'iOS';
                                    }?>
                                </td>
                                <td>
                                    <label>平台类型：</label>
                                    <?php
                                    if($ostypeid == 2)
                                    {
                                        if($devicetypeid == 2) echo 'iPhone';
                                        if($devicetypeid == 3) echo 'iPad';
                                        if($devicetypeid == 4) echo 'Universal';
                                    }else if($ostypeid == 1){
                                        echo '无';
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"><label>应用名称：</label><?php echo $appname?></td>
                            </tr>
                            <tr>
                                <td colspan="2"><label>下载地址：</label><?php echo $appurl?></td>
                            </tr>
                            <tr>
                                <td colspan="2"><label>积分墙：</label><?php if($isintegral == 1){echo '已开启';}else{echo '未开启';}?> *开启积分墙后，才能够给应用添加积分墙广告位。</td>
                            </tr>
                            <tr style="display:<?php if($isintegral == 0){echo 'none';}?>">
                                <td colspan="2"><label>兑换比例：</label><span><span style="font-family: 'arial'">&yen;</span> 1  =<?php echo $integration?>&nbsp;&nbsp;<?php echo empty($unit)? '金币':$unit;?></span></td>
                            </tr>
                            <tr>
                                <td colspan="2" class="app_classify">
                                    <label>应用分类：</label>
                                    <div class="list">
                                        <strong><?php echo $typename?></strong>

                                        <?php
                                        if(isset($childtypename) && !empty($childtypename))
                                        {?>
                                            <div class="form_input subform ">
                                                <?php
                                                foreach ($childtypename as $value) {
                                                    ?>
                                                    <span data-v="" class="selected"><?php echo $value?></span>
                                                <?php
                                                }
                                                ?>
                                            </div>
                                        <?php }?>

                                    </div>

                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"><label>应用介绍：</label><?php echo $appdescription?></td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="table table-bordered table-bordered-rlborder">
                            <thead>
                            <tr>
                                <th colspan="4">广告位信息</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th>广告位ID</th>
                                <th>广告位名称</th>
                                <th>广告位形式</th>
                                <th>广告位描述</th>
                            </tr>
                            <?php
                            //标识判断广告位ID是否有不为空的广告位
                            $tmp = 0;

                            if(!empty($positionList))
                            {
                                foreach ($positionList as $v)
                                {
                                    if(empty($v['positionid']))
                                    {
                                        continue;
                                    }
                                    ?>
                                    <tr class="ar" style="word-wrap: break-word;word-break: normal;">
                                        <td><?php echo $v['positionid']?></td>
                                        <td><?php echo $v['positionname']?></td>
                                        <td><?php
                                            $str = 'Banner广告';
                                            switch ($v['adform'])
                                            {
                                                case 1:
                                                    $str = 'Banner广告';
                                                    break;
                                                case 2:
                                                    $str = '插屏广告';
                                                    break;
                                                case 3:
                                                    $str = '推荐墙广告';
                                                    break;
                                                case 4:
                                                    $str = '积分墙广告';
                                                    break;
                                            }
                                            echo $str;
                                            ?></td>
                                        <td  style="word-wrap:break-word;word-break: break-all;"><?php echo $v['positiondesc']?></td>
                                    </tr>
                                    <?php
                                    $tmp++;
                                }
                            }
                            if($tmp == 0)
                            {
                                echo '<tr class="ac"><td colspan="5">默认广告位不展示在广告位栏，如需灵活控制广告触发时机，建议您创建新广告位</td></tr>';
                            }
                            ?>
                            </tbody>
                        </table>
                        <table class="table table-bordered table-bordered-rlborder">
                            <thead>
                            <tr>
                                <th colspan="2">渠道信息</th>
                            </tr>

                            </thead>
                            <tbody>
                            <tr>
                                <th>渠道名称</th>
                                <th>PublisherId</th>
                            </tr>
                            <?php
                            if(!empty($channellist))
                            {
                                foreach ($channellist as $v)
                                {
                                    ?>
                                    <tr class="">
                                        <td><?php echo $v['channelname']?></td>
                                        <td><?php echo $v['publisherID']?></td>
                                    </tr>
                                <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <table class="table table-bordered table-bordered-rlborder">
                            <thead>
                            <tr>
                                <th>标签信息</th>
                            </tr>
                            </thead>
                            <tbody>

                            <tr>
                                <td>
                                    <div class="info">
                                        <h3>暂未添加标签！</h3>
                                        <p >*建议您给应用添加标签，以获得更好的广告效果。</p>
                                    </div>

                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="info">
                        <h3>您已经成功添加APP,接下来您可以：</h3>
                        <div class="function_btn">
                            <a href="/manage/appinfo/showsdkinfo" class="btn btn-success btn-success-noboder" target="_blank"><i class="icon download_icon"></i>下载并嵌入SDK</a>
                            <a href="/manage/appchannel/showinfo/?appid=<?php echo $appid?>" class="btn btn-primary btn-primary-noboder"><i class="icon right_icon"></i>前往详情页提交审核</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php $this->load->view("manage/inc/footer");?>
<script>
    $(function(){
        $('.black_mask').show();
    })
</script>
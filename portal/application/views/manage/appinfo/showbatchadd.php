<!--  主体部分 [ -->
<div class="inner_container" id="innerContainer">
    <?php $this->load->view('manage/inc/left')?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <div class="jumbotron jumbotron_no_shadow add_app_box">
                    <header class="title">
                        <h3><strong  class="square_box"></strong><span>批量添加App</span></h3>
                    </header>
                    <form method="post" enctype="multipart/form-data">
                        <div class="wrap">
	                        <div class="form-group">
	                        	<strong  class="square_box"></strong><span>请参照说明与模板，上传大小不超过300KB、后缀为CSV、格式为UTF-8的文件，批量添加App</span>
                        	</div>
                       		<div class="form-group">
                                <span class="fl" style="line-height: 40px;"><i class="require_item"></i>CSV模板下载：</span>
                                <span class="fl">
                                	<a href="javascript:void(0);" id="csvdown" class="btn btn-primary  btn-primary-noboder"><i class="icon download_icon"></i>下载</a>
                                </span>
                                <span class="fl" style="line-height: 40px; margin-left:30px;"><i class="require_item"></i>CSV说明下载：</span>
                                <span class="fl">
                                	<a href="javascript:void(0);" id="instructiondown" class="btn btn-primary  btn-primary-noboder"><i class="icon download_icon"></i>下载</a>
                                </span>
                            </div>
							<div class="form-group">
                                <span class="fl" style="line-height: 40px;"><i class="require_item">*</i>请选择要导入的CSV文件：</span>
                                <span class="fl">
                                	<input type="file" style="margin-top:10px" id="file" name="file" onchange='ajaxFileUpload(this)'>
                                	<input type="text" id="filepath" name="filepath" style="display: none">
                                </span>
                                <span class="fl alert alert-small alert-warning" id="fileerr" style="display: none"><i class="icon"></i><span id="filemsg"></span></span>
                            </div>
                            <div class="form-group">
                            	<span class="alert alert-small alert-warning" id="adderr" style="display: none"><i class="icon loading"></i><span id="addmsg"></span></span>
                            	<span class="alert alert-small alert-warning" style="line-height: 40px;color:#FB4C20;font-weight:bold; display:none" id="result"></span>
                            </div>
                        </div>

                        <div class="function_btn">
                            <button type="submit" class="btn btn-primary btn-primary-noboder" id="doBatchAdd" onclick="return false;">批量添加</button>
                            <button type="reset" onclick="confirmCancel();" class="btn btn-default btn-default-noboder">取 消</button> 
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="/public/js/ajaxfileupload.js"></script>
<?php $this->load->view("manage/inc/footer");?>

<script type="text/javascript">
    function confirmCancel(){
        art.dialog({
            title:'<i class="icon icon_node"></i>提示',
            lock: true,
            fixed:true,
            ok:function(){
                location.href='/manage/appinfo/showlist';
                return true;
            },
            cancel:true,
            background: '#fff', // 背景色
            opacity: 0.6,	// 透明度
            content: '<div style="text-align: center; font-size: 14px;">您的操作将不会被保存，确定要继续吗？</div>',
            id: 'deleteLightBox',
            okVal:'确认',
            cancelVal:'取消'
        });
    }
    
    function ajaxFileUpload(fileObj){
        var obj       = $(fileObj);
        var fileId 	  = obj.attr('id');
        var str       = '';
        $.ajaxFileUpload({
            url:'/manage/appinfo/doupload',
            secureuri:false,
            fileElementId:fileId,
            dataType: 'json',
            success: function(data){
                $('#fileerr').hide();
                if(data.status==1)
                {
                    $("input[name='filepath']").val(data.data[0]);
                    $("#fileerr").find("i").removeClass("icon_lose").addClass("icon_success");
                	$("#filemsg").html(data.info);     
                }
                else{
                	$("#fileerr").find("i").removeClass("icon_success").addClass("icon_lose");
                    $("#filemsg").html(data.info);
                }
                $('#fileerr').show();
            }
        });
    }


    $(function(){
	    $("#csvdown").click(function(){
			window.open("http://dev.cocounion.com/public/upload/csvdoc/app_batch_upload_demo.csv", 'down');
		});

	    $("#instructiondown").click(function(){
			window.open("http://dev.cocounion.com/public/upload/csvdoc/Batch_Upload_Application_Instraction.txt", 'down');
		});

		$("#doBatchAdd").click(function(){
	        var file=$("input[name='filepath']").val()?$("input[name='filepath']").val():"";
	        $("#filemsg").html();
	        $('#fileerr').hide();
	        $("#result").empty();
	        if(file == ""){
	            $("#result").append("您还未上传文件");
	            $("#result").show();
	        }
	        if(file!=""){
	            $("#addmsg").html("正在批量添加应用，请稍候...");
	            $("#adderr").show();
	           	$.ajax({  
	                url : "/manage/appinfo/dobatchadd",  
	                async : false, 
	                data:{file:file},
	                type : "POST",  
	                dataType : "json",  
	                success : function(data){
		                $("#file").val("");
		                $("#addmsg").html();
		                $("#adderr").hide();
	                    $("#result").append(data.info);
	                    $("#result").show();
		           }
	            });
	           	return false;
	        }
	    });
	});
</script>

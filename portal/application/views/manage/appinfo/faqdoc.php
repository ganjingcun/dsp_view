<!--  主体部分 [ -->
<div class="inner_container" id="innerContainer">
    <?php if($from == 2){
    			$this->load->view('manage/appinfo/apileft');
    		}
    		else{
    			$this->load->view('manage/appinfo/left');
    		}?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <header class="title">
                    <strong class="title_strong"><i class="icon top_title_icon"></i>常见问题及文档</strong>
                </header>
                <ul class="common_question">
                    <li>
                        <a href="javascript:;" title="" name="1"><i class="icon dot"></i>1、CocosAds开发者如何盈利？</a>
                        <div class="answer_box jumbotron">
                            <i class="icon answer_triger_icon"></i>
                            <p>开发者只要将广告SDK嵌入应用，然后提交应用审核，审核通过就会有正式广告。用户下载安装应用，根据广告模式的不同，用户或点击广告或下载安装广告上的应用，开发者就会获得收益。</p>
                            <p>广告的有效点击规则为同一部手机同一个广告1小时之内点击次数>=1次，都计算为1次有效点击（去重复）。</p>
                        </div>
                    </li>
                    <li>
                        <a href="javascript:;" title="" name="2"><i class="icon dot"></i>2、CocosAds如何结算提现收入？</a>
                        <div class="answer_box jumbotron">
                            <i class="icon answer_triger_icon"></i>
                            <p>CocosAds每月1-10日为上月收入的结算审核期，审核通过后开发者将可以在收入管理页查看上月结算信息，每月25-20日为上月收入(或企业提现)打款期。</p>
                            <p>对于企业开发者和个体工商户开发者，每月20日前需根据当月结算详情或提现详情，提供等额的有效发票和请款订单，CocosAds将依据款单和发票支付开发者收入。</p>
                            <p>对于个人和团体开发者，每月结算时上海拓畅信息技术有限公司将代扣相关税务，并定期自动结算打款。</p>
                            <p>如果遇到异常情况，请联系CocosAds客服人员。</p>
                        </div>
                    </li>
                    <li>
                        <a href="javascript:;" title="" name="3"><i class="icon dot"></i>3、开发者平台使用的流程有哪些？</a>
                        <div class="answer_box jumbotron">
                            <i class="icon answer_triger_icon"></i>
                            <p>登陆CocosAds网站（http://ads.cocos.com）注册开发者账户，注册成功后添加应用，提交应用信息（应用名称，类型，详细介绍），添加广告位信息(添加后会生成广告位id，稍后嵌入广告时用到)，然后将生成一个官方渠道的Publisher id(之后嵌入广告时用到)， 然后下载sdk压缩文件，里面包括sdk文件以及开发说明文档，您可以根据说明文档在您的应用程序中嵌入广告。嵌入成功后可以回到CocosAds上提交应用程序安装包等待审核，审核通过后您就可以开始推广应用获取广告收益了。</p>
                        </div>
                    </li>
                    <li>
                        <a href="javascript:;" title="" name="4"><i class="icon dot"></i>4、为什么广告上附带“测试”图标？</a>
                        <div class="answer_box jumbotron">
                            <i class="icon answer_triger_icon"></i>
                            <p>应用程序没有通过审核，广告将投放为测试广告；模拟器中执行广告程序，广告也将投放为测试广告。仅当程序通过审核且代码嵌入无误时，且在真实的设备上运行时，才会显示正常广告。</p>
                            <p>此外，测试广告不计费，且没有收入。</p>
                        </div>
                    </li>
                    <li>
                        <a href="javascript:;" title="" name="5"><i class="icon dot"></i>5、嵌入积分墙的应用，下载完成指示的任务，为何仍获取不到积分？</a>
                        <div class="answer_box jumbotron">
                            <i class="icon answer_triger_icon"></i>
                            <p>获取不到积分可能有以下几种情况：</p>
                            <p>  1) 设备无效，可以换个手机试试；</p>
                            <p>  2) 用户已经安装过积分墙推荐的应用；</p>
                            <p>  3) 用户网络不稳定，或用户操作时存在任务卡断的问题等。</p>

                        </div>
                    </li>
                    <li>
                        <a href="javascript:;" title="" name="6"><i class="icon dot"></i>6、为何积分墙上的应用积分都是0？</a>
                        <div class="answer_box jumbotron">
                            <i class="icon answer_triger_icon"></i>
                            <p>建议您看下广告平台上是否开启积分墙，并设定积分兑换比例。</p>
                        </div>
                    </li>
                    <li>
                        <a href="javascript:;" title="" name="7"><i class="icon dot"></i>7、什么是placement ID广告位ID(非Publisher ID)？</a>
                        <div class="answer_box jumbotron">
                            <i class="icon answer_triger_icon"></i>
                            <p>广告位是用来区分在同一个应用中，不同位置接入同一种广告类型的标识。如果开发者不同位置广告传入了不同广告位id，最后能在平台上看到不同展示位置对应分开和合计的广告数据报表，帮助开发者更多了解用户的行为。如果开发者不希望区分，传空或者XML中不设置即可。</p>
                            <p>广告位ID是有别于Publisher ID的。广告位ID需要开发先在平台进行创建，然后才能在SDK中传入，如果没有先创建，传入将无法识别。</p>
                            <p>如果没有，为空也行。在 Android 布局创建AdView后，这段代码就会立即尝试加载广告。</p>
                        </div>
                    </li>
                    <li>
                        <a href="javascript:;" title="" name="8"><i class="icon dot"></i>8、广告叠加</a>
                        <div class="answer_box jumbotron">
                            <i class="icon answer_triger_icon"></i>
                            <p>在同一个页面同一时间不要同时展示多种广告，如展示固定广告的同时，又展示全屏广告，
                                导致全屏广告遮盖固定广告，造成一些无用的数据。
                            </p>
                        </div>
                    </li>
                    <li>
                        <a href="javascript:;" title="" name="9"><i class="icon dot"></i>9、如何提高我的广告收益？</a>
                        <div class="answer_box jumbotron">
                            <i class="icon answer_triger_icon"></i>
                            <p>创建应用程序时，正确的选择所属类别；在应用程序界面中合理选择嵌入广告的位置（比如界面顶部），将有助于提高你的广告收益；当然不断的改进你的应用程序产品质量，提高用户量和用户粘性，才是你的制胜之道。    </p>
                        </div>
                    </li>
                    <li>
                        <a href="javascript:;" title="" name="10"><i class="icon dot"></i>10、如何查看收益详情？</a>
                        <div class="answer_box jumbotron">
                            <i class="icon answer_triger_icon"></i>
                            <p>开发者在登录平台后，可以通过点击【报表】菜单，查看您的应用程序在不同时间周期内统计的展示次数、点击次数、点击率和收入金额。</p>
                        </div>
                    </li>
                    <li>
                        <a href="javascript:;" title="" name="11"><i class="icon dot"></i>11、畅思代扣个人劳务报酬个人所得税税负是多少？</a>
                        <div class="answer_box jumbotron">
                            <i class="icon answer_triger_icon"></i>
                            <p>上海拓畅信息技术有限公司按照中国个人所得税关于劳务报酬的相关法案、根据团队各个成员的收入来代扣代缴个税，具体规则如下：</p>
                            <p>• 月收入低于800，应缴税额为：0；</p>
                            <p>• 月收入800-4000，应缴税额为：（收入-800）*20%；</p>
                            <p>• 月收入4000-25000，应缴税额为：（收入-收入*20%）*20%；</p>
                            <p>• 月收入25000-62500，应缴税额为：（收入-收入*20%）*30%-2000；</p>
                            <p>• 月收入高于62500，应缴税额为：（收入-收入*20%）*40%-7000；</p>
                        </div>
                    </li>
                    <li>
                        <a href="javascript:;" title="" name="12"><i class="icon dot"></i>12、什么是个人/团体开发者？</a>
                        <div class="answer_box jumbotron">
                            <i class="icon answer_triger_icon"></i>
                            <p>Publisher ID是CocosAds上，开发者的应用程序根据不同渠道生成的ID。默认生成的Publisher id定义为官方渠道。</p>
                        </div>
                    </li>
                    <li>
                        <a href="javascript:;" title="" name="13"><i class="icon dot"></i>13、什么是个体工商户开发者？</a>
                        <div class="answer_box jumbotron">
                            <i class="icon answer_triger_icon"></i>
                            <p>个体工商户开发者是指有经营能力并依照《个体工商户条例》的规定经工商行政管理部门登记，从事工商业经营的开发者。</p>
                            <p>个体工商户开发者收入结算提现时需要提供具有法律效力的收入发票。不同地区个体工商户收入所得税核定征收比率存在差异，核定征收比率最低约为5%左右。</p>
                        </div>
                    </li>
                    <li>
                        <a href="javascript:;" title="" name="14"><i class="icon dot"></i>14、什么是企业开发者？</a>
                        <div class="answer_box jumbotron">
                            <i class="icon answer_triger_icon"></i>
                            <p>企业开发者是指具有法人资格的产品开发者。企业开发者收入结算及提现时需要提供具有法律效力的收入发票。</p>
                        </div>
                    </li>
                    <li>
                        <a href="javascript:;" title="" name="15"><i class="icon dot"></i>15、个体工商户办理流程？</a>
                        <div class="answer_box jumbotron">
                            <i class="icon answer_triger_icon"></i>
                            <p>1.	工商营业执照：一般个体工商户应到经营场所所在地的工商行政管理所申请办理营业执照。</p>
                            <p>(1) 申请资料准备。个体工商户可以不申请名称。如需申请名称，应在登记注册前到工商部门申请个体工商户名称登记。如不需申请名称，可直接登录当地工商行政管理局的网站下载办理指南和申请表格，或到附近工商行政管理机关领取申请表格，参照指南说明准备文件，经营项目涉及前置许可的，应先取得相应批准文件；</p>
                            <p>(2) 申请资料提交。个体工商户可以现场提交申请资料。除此，还可以选择通过网上审计的方式提交资料，请直接登录当地工商行政管理局的网站办理；</p>
                            <p>(3) 个体工商户取得工商行政管理局颁发的营业执照。</p>

                            <p>2.	税务登记证：一般个体工商户应到经营场所所在地的地方税务所申请办理营业执照。</p>
                            <p>(1)时限：个体工商户自领取营业执照或自有关部门批准设立之日起三十日内，持有关证件，向税务机关申报办理税务登记；</p>
                            <p>(2)申请资料准备。个体工商户应准备以下资料，申领税务登记证：</p>
                            <p style="padding-left:20px ">1) 营业执照副本或其他核准执业证件原件及复印件；</p>
                            <p style="padding-left:20px ">2) 负责人（业主）居民身份证、护照或其他证明身份的合法证件原件及复印件；</p>
                            <p style="padding-left:20px ">3) 房产证明（产权证、租赁协议）原件及复印件；如为自有房产，请提供产权证或买卖契约等合法的产权证明原件及其复印件；如为租赁的场所，请提供租赁协议原件及复印件，出租人为自然人的还须提供产权证明的复印件；以上资料均应贴花；</p>
                            <p style="padding-left:20px ">4)《税务登记表》（纳税人填写完整，个体工商户无须加盖公章）</p>
                            <p>(3)申请资料提交。个体工商户现场提交申请资料；</p>
                            <p>(4)个体工商户取得地方税务局颁发的营业执照。</p>
                            </p>
                        </div>
                    </li>
                </ul>
                <br>
                <br>
            </div>
    </section>
</div>
<?php $this->load->view("manage/inc/footer");?>
<script>
    $('.common_question li').bind('click',function(){
        $('.common_question li .answer_box').slideUp(0);
        $(this).find('.answer_box').stop(true,false).slideDown(0);
    })
</script>

<!--  主体部分 [ -->
<style>
.add_new_app{ position:relative; margin:10px 0;  width:80%;
    font-size: 16px;
    font-weight: 800;
    background:#fff0cb;
    }
.add_new_app .cue{ line-height:30px;  padding:0px 30px 0px 20px; color:#e58527; }
.add_new_app .cue .close{ position:absolute; z-index:1; right:2px; top:2px; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:12px; background:none; color:#666; }
.add_new_app .cue .close:hover{ color:#e58527; }
.close {
    background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
    color: #666;
    font-family: Arial,Helvetica,sans-serif;
    font-size: 16px;
    line-height: 12px;
    font-weight: 800;
    position: absolute;
    right: 2px;
    top: 2px;
    z-index: 1;
}
.center{text-align:center;}
</style>
<div class="inner_container" id="innerContainer">
    <?php $this->load->view('manage/inc/left')?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <form action="" method="post" name="search" id="search">
                    <header class="title">
                        <strong class="title_strong"><i class="icon top_title_icon"></i>所有应用</strong>
                        <div class="search">
                            <input type="text" class="search_box form-control" value="<?php echo $keys;?>"  id="keys" size="50" name="keys"  placeholder="请输入应用名称">
                            <input type="submit" class="icon search_btn" value=" " name="sub">
                        </div>
                    </header>
                    <div>
                        <table class="table table-bordered ">
                            <thead>
                            <tr>
                                <th>应用名称</th>
                                <th>
                                    <select id="ostypeid" name="ostypeid">
                                        <option value="0" <?php if ($ostypeid == 0) {echo 'selected';}?>>所有系统类型</option>
                                        <option value="2" <?php if ($ostypeid == 2) {echo 'selected';}?>>iOS</option>
                                        <option value="1" <?php if ($ostypeid == 1) {echo 'selected';}?>>Android</option>
                                    </select>
                                </th>
                                <th>应用类型</th>
                                <th>开关</th>
                                <th>渠道信息</th>
                                <th>
                                    <select id="auditstatus" name="auditstatus">
                                        <option value="9" <?php if ($auditstatus == 9) {echo 'selected';}?>>所有审核状态</option>
                                        <option value="2" <?php if ($auditstatus == 2) {echo 'selected';}?>>审核通过</option>
                                        <option value="3" <?php if ($auditstatus == 3) {echo 'selected';}?>>审核未通过</option>
                                        <option value="1" <?php if ($auditstatus == 1) {echo 'selected';}?>>审核中</option>
                                        <option value="0" <?php if ($auditstatus == 0) {echo 'selected';}?>>未提交审核</option>
                                    </select>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if(!empty($data))
                            {
                                ?>
                                <?php
                                foreach ($data as $value)
                                {
                                    ?>
                                    <tr key="<?php echo $value['appid']?>">
                                        <td style="width:30%"><a href="/manage/appchannel/showinfo/?appid=<?php echo $value['appid']?>"><?php echo $value['appname']?></a></td>
                                        <td style="width:20%"><?php if($value['ostypeid'] == 1) echo 'Android'; else echo 'iOS';?></td>
                                        <td style="width:10%"><?php echo $value['typename']?></td>
                                        <td style="width:15%"><?php if($value['switch'] == 0){?>
                                            <div class="icon on_off off" name="switch" appid="<?php echo $value['appid']?>"><i class="icon icon_switch"></i></div>
                                            <?php }else{ ?>
                                            <div class="icon on_off on" name="switch" appid="<?php echo $value['appid']?>"><i class="icon icon_switch"></i></div>
                                            <?php }?>
                                        </td>
                                        <td style="width:10%"><a href="/manage/appchannel/showinfo/?appid=<?php echo $value['appid']?>">查看</a></td>

                                        <td style="width:15%; position: static;">
                                            <?php
                                            switch ($value['auditstatus'])
                                            {
                                                case 0:
                                                    $str = '<a href="/manage/appposition/showinfo/?appid='.$value['appid'].'" style="text-decoration:none;"><span class="stadus"><span class="label label-default">未提交审核</span></span></a>';
                                                    break;
                                                case 1:
                                                    $str = '<span class="stadus"><span class="label label-warning">审核中</span></span>';
                                                    break;
                                                case 2:
                                                    $str = '<span class="stadus"><span class="label label-success">审核通过</span></span>';
                                                    break;
                                                case 3:
                                                    $str = '<span class="stadus"><a href="/manage/appposition/showinfo/?appid='.$value['appid'].'" style="text-decoration:none;"><span class="label label-danger">审核驳回</span></a><i  data-toggle="tooltip" data-placement="left"   data-original-title="'.$value['auditmemo'].'" class="icon icon_help"></i></span>';
                                                    break;
                                            }
                                            echo $str;
                                            ?>
                                        </td>
                                    </tr>

                                <?php
                                }
                            }
                            else
                            {
                                ?>
                                <tr><td colspan="8"><p class="ad_errer_info"><b class="red">*</b>您好，目前无任何应用信息，<a href="/manage/appinfo/showadd">去创建应用？</a></p></td></tr>
                            <?php
                            }
                            ?>

                            </tbody>
                        </table>
                        <br>
                    </div>
                </form>
            </div>

        </div>
    </section>
</div>

<?php $this->load->view("manage/inc/footer");?>

<script>
	function closeCue(){
		$(".add_new_app").hide("fast");
	}
	
    $(".on_off").click(function(e){
        var appid = $("#appid").val();
        var obj = this;
        var appid = $(obj).attr("appid");
        if($(obj).hasClass("off")){
            $.post("/manage/appinfo/ajaxSwitch/", {appid:appid, switch:1}, function(data){
                $("#my_switch").html("关闭");
                if(data && data['status']==1){
                    $(obj).addClass("on");
                    $(obj).removeClass("off");
                }else{
                    if(data && data['info']){
                        alert(data['info']);
                    }
                }
            }, 'json');
        }else{
            $.post("/manage/appinfo/ajaxSwitch/", { appid:appid, switch:0}, function(data){
                $("#my_switch").html("开启");
                if(data && data['status']==1){
                    $(obj).addClass("off");
                    $(obj).removeClass("on");
                }else{
                    if(data && data['info']){
                        alert(data['info']);
                    }
                }
            }, 'json');
        }
    });

    //问号 icon 弹出提示框
    $('.icon_help').tooltip();

    $("#popu_cuebox").hover(function(){
        $(this).show();
    },function(){
        $(this).hide();
    });
    $("#ostypeid").change(function(){
        var ostypeid = $(this).children('option:selected').val();
        if(ostypeid == 1)
        {
            $("#devicetypeid").hide();
        }
        else
        {
            $("#devicetypeid").show();
        }

    })
    $("#auditstatus").change(function(){
        $("#search").submit();
    })
    $("#ostypeid").change(function(){
        $("#search").submit();
    })
	$('#newAppCue .close').click(function(){
		$("#newAppCue").fadeOut();
	});


</script>

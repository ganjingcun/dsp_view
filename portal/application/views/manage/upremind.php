<link rel="stylesheet" href="/public/css/index.css" media="all" />
<link rel="stylesheet" href="/public/css/global_3.0.css" media="all" />
<script type="text/javascript" src="/public/js/jquery.min.js"></script>
<script type="text/javascript" src="/public/js/common.js"></script>
<style>
.warp{ width:800px; min-width:800px; }
.salutatory,.intent_select{
    padding-left:0;
    height:auto;
    text-align:left;
    
}
.intent_select{
	width:560px;
}
.warp .intent_select a{
	display:inline-block; margin-left:40px;
}
.salutatory{
	font-size:18px;
}
</style>
 	<div class="bread_crumbs">
        <span>您目前所在位置：</span>&nbsp;<a href="/"><b class="icon_home"></b></a> > <span>身份升级</span>
    </div>
</div>
	<!-- 主体内容 -->
<div class="warp">
	<div class="main_content">
	<h1 class="main_title">系统提示</h1>
	<hr class="dline">
	<br/>
	
	<p class="salutatory">系统检测到您本月收入首次超过10000元，建议您前往工商局注册个体工商户，并升级身份为个体工商户，以减少税负压力。<a href="/public/version1.0/customQuestionDev.html#15" target="_blank" class="ora">怎么升级为个体工商户？</a></p>
	<br />
	<div class="intent_select">
			<a class="button_a" href="/manage/userinfo/showupdateaccount">升级身份</a>
			<a class="button_b" href="/manage/appinfo/showlist">暂不升级身份</a>
		</div>
	</div>
</div>
<?php $this->load->view("manage/inc/footer");?>
<script type="text/javascript">

$(document).ready(function() {
	
})
</script>
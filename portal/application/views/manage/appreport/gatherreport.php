<form action="" method="post" name="frm">
<div class="inner_container" id="innerContainer">
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <?php include "gathertop.php";?>
                    <div class="jumbotron jumbotron_no_shadow tab_box">
                    
                        <div id="chart2" style="width: 100%; height: 400px; margin: 0 auto"></div>
                        <br>
                        <br>
                        <?php
                        //<div class="function_btn" style="float:right; margin-bottom:10px;"><a href="/manage/appreport/exportreport?type=exportreport" id="exportbtn" class="btn btn-primary btn-primary-noboder">下载报表</a></div>
                        ?>
                        <table class="table table-bordered table-bottom30 applist">
                            <thead>
                                <tr class="first_th">
                                    <th>系统类型</th>
                                    <th>banner广告</th>
                                    <th>插屏广告</th>
                                    <th>推荐墙广告</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr style="text-align: center">
                                    <td>iOS</td>
                                    <td><span style="font-family: 'arial'">&yen;</span> <?php echo isset($tablereport[2][0])?formatmoney($tablereport[2][0]):'0.00'?></td>
                                    <td><span style="font-family: 'arial'">&yen;</span> <?php echo isset($tablereport[2][1])?formatmoney($tablereport[2][1]):'0.00'?></td>
                                    <td><span style="font-family: 'arial'">&yen;</span> <?php echo isset($tablereport[2][2])?formatmoney($tablereport[2][2]):'0.00'?></td>
                                </tr>
                                <tr style="text-align: center">
                                    <td>Android</td>
                                    <td><span style="font-family: 'arial'">&yen;</span> <?php echo isset($tablereport[1][0])?formatmoney($tablereport[1][0]):'0.00'?></td>
                                    <td><span style="font-family: 'arial'">&yen;</span> <?php echo isset($tablereport[1][1])?formatmoney($tablereport[1][1]):'0.00'?></td>
                                    <td><span style="font-family: 'arial'">&yen;</span> <?php echo isset($tablereport[1][2])?formatmoney($tablereport[1][2]):'0.00'?></td>
                                </tr>
                                <tr >
                                    <td colspan="4">
                                        <div class="fr">总收入：<span style="font-family: 'arial'">&yen;</span> <?php echo formatmoney($sumincome);?></div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div><!--  闭合inner_tab -->
            </div>
        </div>
    </section>
</div>
</form>
<?php $this->load->view("manage/inc/footer");?>
<script type="text/javascript">
var chart2;
$(document).ready(function() {
	chart2 = new Highcharts.Chart({
		chart: {
			renderTo: 'chart2',//对应的模块ID
			type:'line',
			plotBackgroundColor: null,//图表背景颜色,默认透明
			plotBorderWidth: null,//图表背景边框
			plotShadow: false//阴影，默认2像素
		},
		title: {//title: 标题
			text: '收入趋势'
		},
		xAxis: {
			categories: [<?php foreach ($datelist as $v){ echo "'".$v."',";}?>]
		},
		yAxis: {
			min: 0
		},
		//tooltip: 浮层内容
		tooltip: {
			formatter: function() {
				return '<b>'+ this.series.name +'</b>: '+ this.y;
			}
		},
		legend: {
			layout: 'vertical',
			align: 'left',
			verticalAlign: 'top',
			x: 100,
			y: 50
		},
		series: [
		 <?php
		     foreach ($graphreport as $k=>$v){
		         if(empty($k))
		         {
		             continue;
		         }
		 ?>
		 	{	name:'<?php echo $k?>',
    			data:[<?php foreach ($v as $vt){echo formatmoney($vt, 'get', 2, '.').',';}?>],
    			//showInLegend:false,//显示/隐藏 线条开关
    			marker: {symbol:'circle'} //节点样式 "circle"圆点, "square"方, "diamond"钻型, "triangle"三角 and "triangle-down"向下的三角
			},
		<?php }?>
			]
	});

    /**
     * 日期选择器
     */
    $('#inputDate ,.inset_icon').click(function(){
        $('#dateLightBox').show();
        $('#dateLightBox').css('visibility','visible');
    })
    $('#date3').DatePickerClear();
    $('#date3').DatePicker({
        flat: true,
        format:'Y-m-d',
        date: $('#inputDate').val(),
        current: $('#inputDate').val(),
        mode:'range',
        starts: 1,
        calendars: 2,
        onBeforeShow: function(){
            $(this).DatePickerSetDate($('#inputDate').val(), true);
        },
        onChange: function(formated,dates){
            $('#inputDate').val(formated[0]+"~"+formated[1]);
        }
    });

    $("#dateLightBox .right a.time_sel").click(function(e){
        var data = $(this).attr("data");
        if(data){
            var time = SetDate(data);
            $('#inputDate').val(time[0]+"~"+time[1]);
        }else{
            return;
        }
    });

    $('#clear_button').click(function(){
        $('#dateLightBox').fadeOut();
    })

    $('#submission').click(function(){
        $('[name=frm]').submit();
    })

    $("table.applist tbody tr").hover(function(e){
        $(this).css("background-color","#F4F5F8");
    },function(e){
        $(this).css("background-color","");
    });

});
//-->
</script>


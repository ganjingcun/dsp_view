<link rel="stylesheet" href="/public/css/reportforms.css" media="all" />
<link rel="stylesheet" href="/public/css/popup.css" media="all" />
<link rel="stylesheet" href="/public/css/datepicker.css" media="all" />
<script type="text/javascript" src="/public/js/common.js"></script>
<script type="text/javascript" src="/public/js/datepicker.js"></script>
<script type="text/javascript" src="/public/js/highcharts.js"></script>
<script type="text/javascript" src="/public/js/chartscolor_2.js"></script>
<style type="text/css">
	.sug_box {
		border: 1px solid #d9d9d9;
		background: #fff;
	}

	.sug_box li {
		cursor: pointer;
	}

	.sug_box li:hover {
		background: #d9d9d9;
	}

	.sug_box .sug_tip {
		font-size: 12px;
		color: #999;
	}
	.subform {
    border: 1px solid #E7E9EE;
    border-radius: 4px;
    max-width: 100%;
}
</style>
<div class="bread_crumbs">
	<span>您目前所在位置：</span>&nbsp;<a href="/"><b class="icon_home"></b></a> > <a class="bla" href="/manage/adreport/index">统计分析</a> > <span class="blu">流量分析</span>
</div>
</div>
<!-- 主体内容 -->
<div class="warp">
	<div class="main_content main_content_leftside">
		<?php include "left.php"; ?>
		<!-- 内容部分 -->
		<div class="content content_leftside">
			<div class="inner_con">
				<form action="" method="post" name="frm">
                    <?php include "common.php"; ?>
					<div class="tab_menu">
						<a class="tab_item curr" href="javascript:;">广告位分布</a>
					</div>
					<hr class="dline" />
<?php if (!isset($table['ask'])): ?>
						<div class="chart_datanull">
							<p class="ac">没有查询到相关数据，请重新选择查询条件。</p>
						</div>
<?php else: ?>
						<div id="hours-tab">
							<!-- 报表 曲线图 -->
							<div id="chart2" style="width:100%; height:400px; margin:0 auto"></div>
						</div>
<?php endif; ?>
					<div id="day-tab" style="display:none;">每天</div>
					<div id="week-tab" style="display:none;">每周</div>
					<div id="month-tab" style="display:none;">每月</div><h3 class="column_title_blu"><b class="icon_list" style="margin-right:5px"></b>明细数据 <a class="fr" href="javascript:;" id="export">导出CSV</a></h3>
			<div class="div_sec">
				&nbsp;&nbsp;&nbsp;&nbsp;
				<table class="applist thead_poputop" width="100%">
    				<colgroup>
    					<col width="80" />
    					<col width="70" />
    					<col width="70" />
    					<?php if($adform != 3){?>
    					<col width="70" />
    					<col width="80" />
        				<col width="80" />
    					<?php }?>
    					<col width="60" />
    					<col width="60" />
    					<col width="60" />
    				</colgroup>
					<thead>
						<tr class="ac">
							<th>应用名称</th>
							<th>广告形式</th>
							<th>广告位</th>
							<th>请求量</th>
							<?php if($adform != 3){?><th>展示量</th><?php }?>
							<th>点击量</th>
    						<?php if($adform != 3){?><th>填充率</th><?php }?>
    						<?php if($adform != 3){?><th>点击率</th><?php }?>
							<th>请求占比</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td align="center">汇总</td>
							<td align="center">--</td>
							<td align="center">--</td>
							<td align="right"><?php echo $reportlist['sumreport']['sumask']?></td>
							<?php if($adform != 3){?>
							<td align="right"><?php echo $reportlist['sumreport']['sumimp']?></td>
							<?php }?>
							<td align="right"><?php  echo $reportlist['sumreport']['sumclick']?></td>
							<?php if($adform != 3){?>
							<td align="right">
							<?php
							    echo $reportlist['sumreport']['sumimprate'];
							?>
							</td>
							<td align="right">
							<?php
							    echo $reportlist['sumreport']['sumclickrate'];
							?>
							</td>
							<?php }?>
							<td align="right">--</td>
						</tr>
					<?php 
					if(!empty($reportlist['report']))
					foreach ($reportlist['report'] as $v) {
					    foreach($v as $value)
					    {
					?>
						<tr>
							<td align="center"><?php echo $value['appname']?></td>
							<td align="center"><?php
                                switch ($adform)
                                {
                                    case 1:
                                        echo 'Banner广告';
                                        break;
                                    case 2:
                                        echo '插屏广告';
                                        break;
                                    case 3:
                                        echo '推荐墙广告';
                                        break;
                                    case 4:
                                        echo '积分墙广告';
                                        break;
                                }
							?></td>
							<td><?php echo $value['positionname'] ?></td>
							<td align="right"><?php echo $value['ask']?></td>
							<?php if($adform != 3){?>
							<td align="right"><?php echo $value['imp']?></td>
							<?php }?>
							<td align="right"><?php  echo $value['click']?></td>
							<?php if($adform != 3){?>
							<td align="right">
							<?php
							    echo $value['imprate'];
							?>
							</td>
							<td align="right">
							<?php
							    echo $value['clickrate'];
							?>
							</td>
							<?php }?>
							<td align="right"><?php echo $value['askrate'];?></td>
						</tr>
					<?php 
					    }
					}
					?>
					</tbody>
				</table>
			</div>
			</div>

			</form>

			

		</div>
	</div>

</div>


</div>

<div id="popup_2" class="popup_body" style="visibility: hidden;position:absolute;">
    <div class="bg">
        <div class="popup_box" style="width: 420px; left: 80%; top:0px; border: none;margin-top:30px;">
			<div class="con calendar_box" style="overflow: hidden;">
				<div class="left">
					<div id="date3" class="calendar"> </div>
					<hr class="dline" />
					<div class="db10"></div>
					<div class="ac"><a id="submission" class="button_ss" href="javascript:;" >确定</a> <a id="clear_button" class="button_sx" href="javascript:;" >关闭</a></div>
				</div>
				<div class="right">
					<h4>预设范围:</h4>
					<hr class="dline" />
					<a href="javascript:;" class="time_sel" data="0">今天</a> <a href="javascript:;" class="time_sel" data="-1">昨天</a> <a href="javascript:;" class="time_sel" data="-7">最近7天</a> <a href="javascript:;" class="time_sel" data="-30">最近30天</a> <a href="javascript:" class="time_sel" data="-90">最近90天</a> </div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view("manage/inc/footer");?>
<script type="text/javascript">
	var chart2;
	$(document).ready(function() {
<?php if (!empty($table['ask'])): ?>
				chart2 = new Highcharts.Chart({
					chart: {
						renderTo: 'chart2',//对应的模块ID
						type:'line',
						plotBackgroundColor: null,//图表背景颜色,默认透明
						plotBorderWidth: null,//图表背景边框
						plotShadow: false//阴影，默认2像素
					},
					plotOptions:{
						line:{
							marker:{
								enabled: true
							}
						}
					},
					//title: 标题
					title: {
						text: ''
					},
					yAxis: {
						title: {
							text: ''
						}
					},
					//tooltip: 浮层内容
					tooltip: {
						formatter: function() {
							return '<b>'+ this.x +'</b>: '+this.y;
						}
					},
					legend: {
						enabled:false,
						layout: 'vertical',
						align: 'left',
						verticalAlign: 'top',
						x: 100,
						y: 50
					},
					series: [<?php echo $data1; ?>],
					xAxis:{
						categories:<?php echo $categories; ?>,
					}
				});
<?php endif; ?>

			//周期选择
			$('#inputDate').click(function(){
				$('#popup_2').show();
				$('#popup_2').css('visibility','visible');
			})
			$('#date3').DatePickerClear();
			$('#date3').DatePicker({
				flat: true,
				format:'Y-m-d',
				date: $('#inputDate').val(),
				current: $('#inputDate').val(),
				mode:'range',
				starts: 1,
				calendars: 2,
				onBeforeShow: function(){
					$(this).DatePickerSetDate($('#inputDate').val(), true);
				},
				onChange: function(formated,dates){
					//alert(thisdate)
					$('#inputDate').val(formated[0]+"~"+formated[1]);
					//obj.DatePickerHide();
				}
			});

			$("#popup_2 .right a.time_sel").click(function(e){
				var data = $(this).attr("data");
				if(data){
					var time = SetDate(data);
					$('#inputDate').val(time[0]+"~"+time[1]);
				}else{
					return
				}
			});


			$('#clear_button').click(function(){
				$('#popup_2').fadeOut();
			})

			$('#submission').click(function(){
				$('#daochu').val('');
				$('[name=wd]').val('');
				$('[name=pid]').val('');
				$('[name=tid]').val('');
				$('[name=frm]').submit();
			})

			$('#wd_opt').change(function(){
				$('#daochu').val('');
				$('[name=wd]').val($(this).val());
				$('[name=frm]').submit();
			})

			$('#sp_opt').change(function(){
				$('#daochu').val('');
				$('[name=tid]').val($(this).val());
				$('[name=wd]').val('');
				$('[name=frm]').submit();
			})

			$('#pl_opt').change(function(){
				$('#daochu').val('');
				$('[name=pid]').val($(this).val());
				$('[name=wd]').val('');
				$('[name=frm]').submit();
			})

			$('#export').click(function(){
				$('#daochu').val('export');
				$('[name=frm]').submit();
			})

			//弹出框2 坐标定位
			popupOrien("#popup_2",".time_input_box");
			$(window).resize(function(){
				popupOrien("#popup_2",".time_input_box");
			});
			function popupOrien(popup,timebox){
				var inputObj = $(timebox).eq(0).offset();
				console.log(inputObj.left+";"+inputObj.top);
				$(popup+" .popup_box").css({ "left":(inputObj.left-160),"top":inputObj.top})
			};

			//初始化tab
			tabInit($("#tab_menu_1"));
			$("table.applist tbody tr").hover(function(e){
				$(this).css("background-color","#F4F5F8");
			},function(e){
				$(this).css("background-color","");
			});

			//列表表头固定顶部函数
			theadPopuTop($(".applist").eq(0),"div_sec");
		});
</script>
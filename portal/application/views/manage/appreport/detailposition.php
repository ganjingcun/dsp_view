<div class="inner_container" id="innerContainer">
    <?php include "left.php"; ?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <form action="" name="frm" id="frm" method="post">
                    <div class="app_detail_top jumbotron jumbotron_no_shadow">
                        <h3>
                            <strong class="square_box"></strong>
                            <span><?php echo $appinfo['appname'];?></span>
                        </h3>
                        <br>
                        <!-- div><label>应用ID：</label><?php echo $appinfo['appid'];?></div> -->
                        <br>
                        <div>
                            <label>广告形式：</label>
                            <select name="adform" id="adform">
                                <option value="1" <?php if($adform == 1){echo 'selected';}?>>Banner广告</option>
                                <option value="2" <?php if($adform == 2){echo 'selected';}?>>插屏广告</option>
                                <option value="3" <?php if($adform == 3){echo 'selected';}?>>推荐墙广告</option>
                                <option value="4" <?php if($adform == 4){echo 'selected';}?>>积分墙广告</option>
                            </select>
                            <input type="hidden" id="appid" name="appid" value=<?php echo $appid;?>>
                        </div>
                    </div>
                    <div class="inner_tab">
                        <ul class="tab_item">
                            <li><a href="javascript:;" onclick="tabsubmit(1);">总体概况</a></li>
                            <li><a href="javascript:;" onclick="tabsubmit(2);">渠道分布</a></li>
<!--                            <li class="active"><a href="javascript:;" onclick="tabsubmit(3);">广告位分布</a></li>-->
                            <div class="inputDate_box fr">
                                <span class="inset_icon"><i class="icon icon_date"></i></span>
                                <input type="text" class="fr form-control form-control-small" id="inputDate" size="30" name="inputDate" readonly="readonly" value="<?php echo $sdate.'~'.$edate; ?>" style="width:200px;">
                            </div>
                            <!-------------定义弹出时间层的位置----------------->
                            <!-------------定义弹出时间层的位置----------------->
                            <div id="dateLightBox" class="popup" style="display: none;">
                                <i class="icon icon_white_jiao"></i>
                                <div class="calendar_box" >
                                    <div class="fl left">
                                        <div id="date3" class="calendar"></div>
                                    </div>
                                    <div class="fr right">
                                        <h4>预设范围:</h4>
                                        <a href="javascript:;" class="time_sel" data="0">今天</a>
                                        <a href="javascript:;" class="time_sel" data="-1">昨天</a>
                                        <a href="javascript:;" class="time_sel" data="-7">最近7天</a>
                                        <a href="javascript:;" class="time_sel" data="-30">最近30天</a>
                                        <a href="javascript:" class="time_sel" data="-90">最近90天</a>
                                    </div>
                                    <div class="popup_btn">
                                        <a href="javascript:;" id="submission" class="btn btn-primary btn-primary-noboder">确定</a>
                                        <a href="javascript:;" id="clear_button" class="btn btn-default btn-default-noboder">取消</a>
                                    </div>
                                </div>
                            </div>
                        </ul>
                    </div>
                </form>
                <div class="jumbotron jumbotron_no_shadow tab_box">
                    <div class="chart_datanull" style="display: none;">
                        <p class="ac">没有查询到相关数据，请重新选择时间周期。</p>
                    </div>
                    <div class="charttab_channel">
                        <!-- 报表 曲线图 -->
                        <div id="charttab" class="charttab fl">
                            <?php if($adform != 4){?><a id="0" href="javascript:" class="curr" >请求量</a><?php }?>
                            <?php if($adform == 1 || $adform == 2){?><a id="1" href="javascript:">展示量</a><?php }?>
                            <?php if($adform != 4){?><a id="2" href="javascript:">点击量</a><?php }?>
                            <?php if($adform == 1 || $adform == 2){?><a id="3" href="javascript:">填充率</a><?php }?>
                            <?php if($adform == 1 || $adform == 2){?><a id="4" href="javascript:">点击率</a><?php }?>
                            <a id="5" href="javascript:">收入</a>
                        </div>
                        <!-- 渠道选择 -->
                        <div id="channel_select" class="channel_select fr">
                            <div class="hd">广告位选择</div>
                            <div class="search"><input type="text" class="input_text" value="" /></div>
                            <div class="list">
                                <?php
                                if(isset($positionlist) && !empty($positionlist))
                                    foreach($positionlist as $v){?>
                                        <a id="<?php echo $v['positionid'];?>" <?php if ($v['positionid'] == $positionid){echo 'class="hover"';};?>  href="javascript:;"><?php echo $v['positionname'];?></a>
                                    <?php }?>
                            </div>
                        </div>
                        <!-- /渠道选择 -->
                    </div>
                    <div id="chart2" style="width: 100%; height: 400px; margin: 0 auto"></div>
                    <br>
                    <br>
                    <table class="applist thead_poputop table table-bordered table-bottom30" width="100%">
                        <thead class="double">
                            <tr class="first_th">
                                <th>日期</th>
                                <?php if($adform != 4){?><th>请求量</th><?php }?>
                                <?php if($adform == 1 || $adform == 2){?><th>展示量</th><?php }?>
                                <?php if($adform != 4){?><th>点击量</th><?php }?>
                                <?php if($adform == 1 || $adform == 2){?><th>填充率</th><?php }?>
                                <?php if($adform == 1 || $adform == 2){?><th>点击率</th><?php }?>
                                <th>收入</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr style="text-align: center;">
                                <td><div class="cm">合计</div></td>
                                <?php if($adform != 4){?><td><div class="cm"><?php echo $graphreport['sum']['ask']?></div></td><?php }?>
                                <?php if($adform == 1 || $adform == 2){?><td><div class="cm"><?php echo $graphreport['sum']['imp']?></div></td><?php }?>
                                <?php if($adform != 4){?><td><div class="cm"><?php echo $graphreport['sum']['click']?></div></td><?php }?>
                                <?php if($adform == 1 || $adform == 2){?><td><div class="cm"><?php echo $graphreport['sum']['imprate']?>%</div></td><?php }?>
                                <?php if($adform == 1 || $adform == 2){?><td><div class="cm"><?php echo $graphreport['sum']['clickrate']?>%</div></td><?php }?>
                                <td><div class="cm"><span style="font-family: 'arial'">&yen;</span> <?php echo $graphreport['sum']['income']?></div></td>
                            </tr>
                            <?php
                            if(isset($graphreport['list']) && !empty($graphreport['list']))
                                foreach (array_reverse($graphreport['list'],true) as $k=>$v){?>
                                <tr style="text-align: center;">
                                    <td><div class="cm"><?php echo $k?></div></td>
                                    <?php if($adform != 4){?><td><div class="cm"><?php echo $v['ask']?></div></td><?php }?>
                                    <?php if($adform == 1 || $adform == 2){?><td><div class="cm"><?php echo $v['imp']?></div></td><?php }?>
                                    <?php if($adform != 4){?><td><div class="cm"><?php echo $v['click']?></div></td><?php }?>
                                    <?php if($adform == 1 || $adform == 2){?><td><div class="cm"><?php echo $v['imprate']?>%</div></td><?php }?>
                                    <?php if($adform == 1 || $adform == 2){?><td><div class="cm"><?php echo $v['clickrate']?>%</div></td><?php }?>
                                    <td><div class="cm"><span style="font-family: 'arial'">&yen;</span> <?php echo $v['income']?></div></td>
                                </tr>
                            <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>
<?php $this->load->view("manage/inc/footer");?>
<script type="text/javascript">
<!--
//var chart,chart2,chart3;
var chart2;
$(document).ready(function() {

	chart2 = new Highcharts.Chart({
		chart: {
			renderTo: 'chart2',//对应的模块ID
			type:'line',
			plotBackgroundColor: null,//图表背景颜色,默认透明
			plotBorderWidth: null,//图表背景边框
			plotShadow: false//阴影，默认2像素
		},
		//title: 标题
		title: {
			text: '请求量'
		},
		xAxis: {
			categories: [<?php if(isset($graphreport['list']) && !empty($graphreport['list'])) foreach ($graphreport['list'] as $k=>$v){ echo "'".$k."',";}?>]
		},
		yAxis: {
			min: 0
		},
		//tooltip: 浮层内容
		tooltip: {
			formatter: function() {
				return '<b>'+ this.series.name +'</b>: '+ this.y;
			}
		},
		legend: {
			layout: 'vertical',
			enabled:false,
			align: 'left',
			verticalAlign: 'top',
			x: 100,
			y: 50
		},
		series: [		 
		 	{	
			 	name:'请求量', 
    			data:[<?php if(isset($graphreport['list']) && !empty($graphreport['list'])) foreach ($graphreport['list'] as $k=>$v){echo $v['ask'].',';}?>],
    			//showInLegend:false,//显示/隐藏 线条开关
    			marker: {symbol:'circle'} //节点样式 "circle"圆点, "square"方, "diamond"钻型, "triangle"三角 and "triangle-down"向下的三角
			},
			]
	});
});
function reChart(n) {
	var dataArr = {
			"0":[<?php if(isset($graphreport['list']) && !empty($graphreport['list']))  foreach ($graphreport['list'] as $k=>$v){echo $v['ask'].',';}?>],
			"1":[<?php if(isset($graphreport['list']) && !empty($graphreport['list']))  foreach ($graphreport['list'] as $k=>$v){echo $v['imp'].',';}?>],
			"2":[<?php if(isset($graphreport['list']) && !empty($graphreport['list']))  foreach ($graphreport['list'] as $k=>$v){echo $v['click'].',';}?>],
			"3":[<?php if(isset($graphreport['list']) && !empty($graphreport['list']))  foreach ($graphreport['list'] as $k=>$v){echo $v['imprate'].',';}?>],
			"4":[<?php if(isset($graphreport['list']) && !empty($graphreport['list']))  foreach ($graphreport['list'] as $k=>$v){echo $v['clickrate'].',';}?>],
			"5":[<?php if(isset($graphreport['list']) && !empty($graphreport['list'])) foreach ($graphreport['list'] as $k=>$v){echo $v['income'].',';}?>]	
		}

	// chart4 为图表对象；
	chart2.series[0].setData(dataArr[n]);  
}
//-->
</script> 
<script type="text/javascript">
	var chart2;
	$(document).ready(function() {
        //周期选择
        $('#inputDate ,.inset_icon').click(function(){
            $('#dateLightBox').show();
            $('#dateLightBox').css('visibility','visible');
        })
        $('#date3').DatePickerClear();

        $('#date3').DatePicker({
            flat: true,
            format:'Y-m-d',
            date: $('#inputDate').val(),
            current: $('#inputDate').val(),
            mode:'range',
            starts: 1,
            calendars: 2,
            onBeforeShow: function(){
                $(this).DatePickerSetDate($('#inputDate').val(), true);
            },
            onChange: function(formated,dates){
                //alert(thisdate)
                $('#inputDate').val(formated[0]+"~"+formated[1]);
                //obj.DatePickerHide();
            }
        });

        $("#dateLightBox .right a.time_sel").click(function(e){
            var data = $(this).attr("data");
            if(data){
                var time = SetDate(data);
                $('#inputDate').val(time[0]+"~"+time[1]);
            }else{
                return
            }
        });

        $('#clear_button').click(function(){
            $('#dateLightBox').fadeOut();
        })
        $('#submission').click(function(){
            $('[name=frm]').submit();
        })
        $('#adform').change(function(){
            $('[name=frm]').submit();
        })


        $("table.applist tbody tr").hover(function(e){
            $(this).css("background-color","#F4F5F8");
        },function(e){
            $(this).css("background-color","");
        });

        $("#charttab a").click(function(){
            $(this).addClass("curr").siblings().removeClass("curr");
        })
			<?php if($adform==4){echo '$("#charttab a").eq(0).click();';}?>
		});
	
	function tabsubmit(val)
	{
		if(val == 1)
		{
			$('[name=frm]').attr('action','/manage/detailreport/detailgather/?appid=<?php echo $appid;?>');
		}
		else if(val == 2)
		{
			$('[name=frm]').attr('action','/manage/detailreport/detailchannel/?appid=<?php echo $appid;?>');
		}
		else if(val == 3)
		{
			$('[name=frm]').attr('action','/manage/detailreport/detailposition/?appid=<?php echo $appid;?>');
			}
		$('[name=frm]').submit();
	}



	// 广告位切换ajax数据处理
	function channelChartSelect(por) {
		var SelectList,
			Chart,
			TableBody,
			InputBar,
			getChangeListUrl,
			onTabChannel;
		var containernew = Chart;

		function fullChart(data) {
			new Highcharts.Chart(data);	
		};

		function fullTable(data){
			var _leng = data.length,
				text = "";
			for (var i in data) {
				text+="<tr>";
				var l = data[i].length;
				for (var n = 0; n < l; n++) {
					text += '<td><div class="cm">'+data[i][n]+'</div></td>';
				};
				text+="</tr>";
			};
			TableBody.find("tbody").html(text);
		};

		function fullSelectList(data){
			var _leng = data.length,
				text = ""
			for (var i = 0; i < _leng; i++) {
				text += '<a id="'+data[i].id+'" href="javascript:;">'+data[i].name+'</a>';
			};
			SelectList.find(".list").html(text);
		}

		function init(por){
			SelectList 		 = por.SelectList;
			Chart 			 = por.ChartId;
			TableBody 		 = por.TableBody;
			InputBar 		 = por.InputBar;
			getChangeListUrl = por.getChangeListUrl;
			onTabChannel	 = por.onTabChannel;
			

			SelectList.find("a").bind('click',function(){
				$(this).addClass('hover').siblings().removeClass('hover');
				typeof onTabChannel === "function" ? onTabChannel($(this).attr("id")) : onTabChannel ;
			});


            SelectList.hover(function () {
                SelectList.css("height","auto");
            },function () {
                SelectList.css({"height":'29px',"overflow":"hidden"});
            });

			InputBar.bind("input",function(){
				keys = $(this).val();
				adform = $('#adform').val();
				$.ajax({url: '/manage/detailreport/ajaxpositionlist/', type: 'GET', dataType: 'json',
					data: {appid:'<?php echo $appid?>', keys:keys, adform:adform},
					success:function(data){
						fullSelectList(data.data)
					}
				});	
			

			});
		};

		//初始化
		init(por);

		return {
			fullChart:fullChart,
			fullTable:fullTable
		}
	};

	var chartdata = {
			"0":[<?php if(isset($graphreport['list']) && !empty($graphreport['list'])) foreach ($graphreport['list'] as $k=>$v){echo $v['ask'].',';}?>],
			"1":[<?php if(isset($graphreport['list']) && !empty($graphreport['list'])) foreach ($graphreport['list'] as $k=>$v){echo $v['imp'].',';}?>],
			"2":[<?php if(isset($graphreport['list']) && !empty($graphreport['list'])) foreach ($graphreport['list'] as $k=>$v){echo $v['click'].',';}?>],
			"3":[<?php if(isset($graphreport['list']) && !empty($graphreport['list'])) foreach ($graphreport['list'] as $k=>$v){echo $v['imprate'].',';}?>],
			"4":[<?php if(isset($graphreport['list']) && !empty($graphreport['list'])) foreach ($graphreport['list'] as $k=>$v){echo $v['clickrate'].',';}?>],
			"5":[<?php if(isset($graphreport['list']) && !empty($graphreport['list'])) foreach ($graphreport['list'] as $k=>$v){echo $v['income'].',';}?>]	
		}
	// 
	var chartselect = new channelChartSelect({
		"SelectList"	: $("#channel_select"),
		"ChartId"		: $("#chart2"),
		"TableBody"		: $("#datatable"),
		"InputBar"		: $("#channel_select input").eq(0),
		"getChangeListUrl": "/manage/detailreport/ajaxpositionlist/",
		onTabChannel:function(val) {
			inputDate = $("#inputDate").val();
			adform = $("#adform").val();
			publisherid = val;
			appid = '<?php echo $appid;?>';
			$.ajax({url: '/manage/detailreport/ajaxpositiondata/', type: 'post', dataType: 'json',
				data: {adform:adform, publisherid:publisherid, appid:appid, inputDate:inputDate},
				success:function(data){
					chartdata = data.data["ajaxgraph"];
					chartselect.fullTable(data.data["ajaxlist"]);	
					$("#charttab a").eq(0).click();			
				}
			});
		}
	});

	$("#charttab a").bind("click",function(){
		var id = $(this).attr("id");
		var unit = id=="2" ? "%" : "";
		var name = $(this).html()
		var charrObj = { chart:{}, title:{}, xAxis:{}, legend:{}, tooltip:{}, series:{}};
		charrObj.chart.renderTo = "chart2";
		charrObj.chart.defaultSeriesType = 'line';
		charrObj.title.text = name;
		charrObj.legend.enabled = false;
		charrObj.xAxis.categories = [<?php if(isset($graphreport['list']) && !empty($graphreport['list'])) foreach ($graphreport['list'] as $k=>$v){ echo "'".$k."',";}?>];
		charrObj.tooltip.formatter = function(){ 
			if(id==3 || id ==4)
			{
				return ''+ this.series.name +': '+ this.y + '%'
				}
			if(id ==5)
			{
				return ''+ this.series.name +':￥ '+ this.y
				}
			return ''+ this.series.name +': '+ this.y
			}
		for(var i in chartdata[id]){
			if(typeof chartdata[id][i]=="string"){ chartdata[id][i] = Number(chartdata[id][i]) };
		}
		charrObj.series = [{name:name, data:chartdata[id]}]
		chartselect.fullChart(charrObj);
		return false
	});







	
</script>
					<div class="subform">
					<div class="form_title"><b class="icon_chart"></b> <strong>基本流量分析</strong>
						<span class="time_input_box fr">
							<input type="text" class="inputdate" id="inputDate" size="30" name="inputDate" readonly="readonly" value="<?php echo $sdate.'~'.$edate; ?>"  style="background-position:97% -76px"/>
						</span>
					</div><br>
					<div class="">&nbsp;&nbsp;&nbsp;&nbsp;
						<label> 平台类型：</label>
						<select id='ostype' name="ostype">
							<option value='2' <?php if ($ostype == '2') { ?>selected<?php } ?>>iOS</option>
							<option value='1' <?php if ($ostype == '1') { ?>selected<?php } ?>>Android</option>
						</select>&nbsp;&nbsp;&nbsp;&nbsp;
						<label>应用名称：</label>
						<select id="appid" name="appid">
						<option <?php if($appid=='') echo 'selected';?> value="">全部</option>
						<?php foreach($applist as $v){?>
							<option value='<?php echo $v['appid']?>' <?php if($appid == $v['appid'] ) echo 'selected'?>><?php echo $v['appname']?></option>
						<?php }?>
						</select>&nbsp;&nbsp;&nbsp;&nbsp;
						<label>广告形式：</label>
						<select id="adform" name="adform">
							<option value='1' <?php if($adform == 1 ) echo 'selected'?>>Banner广告</option>
							<option value='2' <?php if($adform == 2 ) echo 'selected'?>>插屏广告</option>
							<option value='3' <?php if($adform == 3 ) echo 'selected'?>>推荐墙广告</option>
							<option value='3' <?php if($adform == 4 ) echo 'selected'?>>积分墙广告</option>
						</select>
						<button type="submit" id="searchbutton" class="button_sx button_submit">筛选</button>
					</div><br>
					</div>
					<table width="100%" class="applist">
							<colgroup>
							<col width="100" bgcolor="#00FF00">
							<col width="100">
							<?php if($adform != 3){?>
							<col width="100">
							<col width="100">
							<col width="100">
							<col width="100">
							<?php }?>
							<col width="100">
							<col width="100">
							<col width="100">
							</colgroup><thead>
								<tr>
									<th>日期</th>
									<th>请求量</th>
									<?php if($adform != 3){?>
									<th>展现量</th>
									<?php }?>
									<th>点击量</th>
									<?php if($adform != 3){?>
									<th>填充率</th>
									<th>点击率</th>
									<?php }?>
									<th>总收入</th>
									<?php if($adform != 3){?>
									<th>eCPM单价</th>
									<?php }?>
									<th>eCPC单价</th>
								</tr>
							</thead>
							<tbody>
							<?php
							if(!empty($dateinfolist))
                            foreach ($dateinfolist as $value) {
        					    if(!isset($commonreport[$value]))
        					    {
        					        continue;
        					    }
        					    ?>
								<tr>
									<td><?php
                                    if(strlen($value) > 2)
                                    {
                                        echo $value;
                                    }
                                    else
                                    {
        							    echo str_pad($value-1,2,"0",STR_PAD_LEFT) . ' -- ' . $value;
                                    }
							        ?></td>
    								<td>
    									<?php
                                            echo $commonreport[$value]['ask'];
    									?>
    								</td>
									<?php if($adform != 3){?>
    								<td>
    									<?php
                                            echo $commonreport[$value]['imp'];
    									?>
    								</td>
									<?php }?>
									<td>
									<?php
                                        echo $commonreport[$value]['click'];
									?>
									</td>
									<?php if($adform != 3){?>
										<td>
    									<?php
                                           echo $commonreport[$value]['imprate'];
    									?>
    									</td>
										<td>
    									<?php
                                           echo $commonreport[$value]['clickrate'];
    									?>
    									</td>
									<td>
									<?php
                                        echo $commonreport[$value]['cost'];
									?>
									</td>
									<?php if($adform != 3){?>
									<td>
									<?php
                                        echo $commonreport[$value]['cpm'];
									?>
									</td>
									<?php }?>
									<td>
									<?php
                                        echo $commonreport[$value]['cpc'];
									?>
									</td>
								</tr>
									<?php }}?>
							</tbody>
						</table>
					<br>
<script>
$(document).ready(function(){
	$("#ostype").change(function(){
		var ostype = $(this).children('option:selected').val();
		$.get("/manage/adreport/ajaxgetapplist", 
				{ 
					ostype:ostype,
	    			r:Math.random()
				}, 
				function(data)
				{
	    			if(data && data['status'] == 0){
	        			alert("未知错误！");
	    				return false;
	    			}else{
	    				$("#appid").empty();
	    				$("#appid").append("<option value=''>全部</option>");
						$(data.data).each(function(i,val){
							$("#appid").append("<option value='"+val.appid+"'>"+val.appname+"</option>");
						})
	    			}
				},
				'json');







		
		})
})

</script>
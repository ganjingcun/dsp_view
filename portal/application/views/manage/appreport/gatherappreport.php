<form action="" method="post" name="frm">
    <div class="inner_container" id="innerContainer">
        <?php include "left.php"; ?>
        <section>
            <div id="content" class="content">
                <div class="right_content" id="rightContent">
                    <?php include "gathertop.php";?>
                        <div class="jumbotron jumbotron_no_shadow tab_box applist" style="min-height: 300px;">
                        <div class="function_btn" style="float:right; margin-bottom:10px"><a href="/manage/appreport/exportappreport?type=exportreport" id="exportbtn" class="btn btn-primary btn-primary-noboder">下载报表</a></div>
                            <table class="table table-bordered table-bottom30">
                                <thead>
                                <tr class="first_th">
                                    <th>应用名称</th>
                                    <th>总收入</th>
                                    <th>收入占比</th>
                                    <th>Banner收入</th>
                                    <th>推荐墙收入</th>
                                    <th>插屏广告收入</th>
                                    <th>积分墙收入</th>
                                    <th>信息流收入</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if(isset($tablereport['iOS']) && !empty($tablereport['iOS']))
                                    foreach($tablereport['iOS'] as $k=>$v)
                                    {
                                        ?>
                                        <tr style="text-align: center">
                                            <td><?php echo $list[$k];?></td>
                                            <td><span style="font-family: 'arial'">&yen;</span> <?php echo isset($v['sum'])?formatmoney($v['sum']):'0.00'?></td>
                                            <td><?php echo isset($v['ratio'])?number_format($v['ratio'],2):'0.00'?>%</td>
                                            <td><span style="font-family: 'arial'">&yen;</span> <?php echo isset($v[1])?formatmoney($v[1]):'0.00'?></td>
                                            <td><span style="font-family: 'arial'">&yen;</span> <?php echo isset($v[3])?formatmoney($v[3]):'0.00'?></td>
                                            <td><span style="font-family: 'arial'">&yen;</span> <?php echo isset($v[2])?formatmoney($v[2]):'0.00'?></td>
                                            <td><span style="font-family: 'arial'">&yen;</span> <?php echo isset($v[6])?formatmoney($v[6]):'0.00'?></td>
                                            <td><span style="font-family: 'arial'">&yen;</span> <?php echo isset($v[20])?formatmoney($v[20]):'0.00'?></td>
                                        </tr>
                                    <?php
                                    }
                                ?>
                                </tbody>
                            </table>
                            <table class="table table-bordered table-bottom30 applist">
                                <thead>
                                <tr class="first_th">
                                    <th>应用名称</th>
                                    <th>总收入</th>
                                    <th>收入占比</th>
                                    <th>Banner收入</th>
                                    <th>推荐墙收入</th>
                                    <th>插屏广告收入</th>
                                    <th>积分墙收入</th>
                                    <th>信息流收入</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if(isset($tablereport['Android']) && !empty($tablereport['Android']))
                                    foreach($tablereport['Android'] as $k=>$v)
                                    {
                                        ?>
                                        <tr style="text-align: center">
                                            <td><?php echo $list[$k];?></td>
                                            <td><span style="font-family: 'arial'">&yen;</span> <?php echo isset($v['sum'])?formatmoney($v['sum']):'0.00'?></td>
                                            <td><?php echo isset($v['ratio'])?number_format($v['ratio'],2):'0.00'?>%</td>
                                            <td><span style="font-family: 'arial'">&yen;</span> <?php echo isset($v[1])?formatmoney($v[1]):'0.00'?></td>
                                            <td><span style="font-family: 'arial'">&yen;</span> <?php echo isset($v[3])?formatmoney($v[3]):'0.00'?></td>
                                            <td><span style="font-family: 'arial'">&yen;</span> <?php echo isset($v[2])?formatmoney($v[2]):'0.00'?></td>
                                            <td><span style="font-family: 'arial'">&yen;</span> <?php echo isset($v[6])?formatmoney($v[6]):'0.00'?></td>
                                            <td><span style="font-family: 'arial'">&yen;</span> <?php echo isset($v[20])?formatmoney($v[20]):'0.00'?></td>
                                        </tr>
                                    <?php
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</form>
<?php $this->load->view("manage/inc/footer");?>
<script type="text/javascript">
	$(document).ready(function() {
        //周期选择
        $('#inputDate ,.inset_icon').click(function(){
            $('#dateLightBox').show();
            $('#dateLightBox').css('visibility','visible');
        })
        $('#date3').DatePickerClear();
        $('#date3').DatePicker({
            flat: true,
            format:'Y-m-d',
            date: $('#inputDate').val(),
            current: $('#inputDate').val(),
            mode:'range',
            starts: 1,
            calendars: 2,
            onBeforeShow: function(){
                $(this).DatePickerSetDate($('#inputDate').val(), true);
            },
            onChange: function(formated,dates){
                $('#inputDate').val(formated[0]+"~"+formated[1]);
            }
        });

        $("#dateLightBox .right a.time_sel").click(function(e){
            var data = $(this).attr("data");
            if(data){
                var time = SetDate(data);
                $('#inputDate').val(time[0]+"~"+time[1]);
            }else{
                return
            }
        });

        $('#clear_button').click(function(){
            $('#dateLightBox').fadeOut();
        })

        $('#submission').click(function(){
            $('[name=frm]').submit();
        })

        $("table.applist tbody tr").hover(function(e){
            $(this).css("background-color","#F4F5F8");
        },function(e){
            $(this).css("background-color","");
        });

    });
</script>

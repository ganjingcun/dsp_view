<div class="app_detail_top jumbotron jumbotron_no_shadow">
    <h3>
        <strong class="square_box"></strong>
        <strong><?php echo $appname?></strong>
        <?php
        switch ($auditstatus)
        {
            case 0:
                $tmpstr = '未提交审核';
                $tmpcolor = 'default';
                break;
            case 1:
                $tmpstr = "审核中";
                $tmpcolor = 'warning';
                break;
            case 2:
                $tmpstr = "审核通过";
                $tmpcolor = 'success';
                break;
            case 3:
                $tmpstr = "应用被驳回";
                $tmpcolor = 'danger';
                break;
        }
        ?>
        <span class="label label-<?php echo $tmpcolor;?>"><?php echo $tmpstr;?></span>
        <?php if($auditstatus==3){?><i  data-toggle="tooltip" data-placement="right" title="" data-original-title="<?php echo $auditmemo?>" class="icon icon_help"></i><?php }?>


	    <?php
        if($auditstatus == 0 || $auditstatus == 3 )
        {
            echo '<a id="upLoadApp" class="btn btn-primary btn-primary-noboder" href="#"><i class="icon icon_turn"></i>上传应用并审核</a>';
        }
        ?>

    </h3>

    <div class="ad_on_off">
        <label>广告开关：</label>
        <?php if($switch == 0){?>
        <div class="icon on_off off" name="switch">
            <i class="icon icon_switch"></i>
        </div>
        <?php }else{ ?>
        <div class="icon on_off on" name="switch">
            <i class="icon icon_switch"></i>
        </div>
        <?php }?>
        <input type="hidden" name="appid" id="appid" value="<?php echo $appid;?>">
    </div>


    <?php if($isintegral == 1){?>
    <div class="credit">
        <label>积分兑换比例：</label>
        <span class="coin_box">
            <span class="text" style="font-family:'arial'">&yen; 1=</span>
            <span id="showintegration" >
                <?php echo $integration.' '.$unit;?><i adtype="2" ostype="1" id="editIcon" class="icon icon_edit_con"></i>
            </span>
            <span id="ratioinput" class="ratioinput" style="display: none;">
                <input type="text" value="<?php echo $integration?>" name="eintegration" id="eintegration" class="input_text" size="5" maxlength="8">
                <input type="text" value="<?php echo $unit?>" name="eunit" id="eunit" class="input_text" size="5" maxlength="8">
                <a href="javascript:;" class="coin_btn ok">确认</a>
                <a href="javascript:;" class="coin_btn cancel">取消</a>
            </span>
            <span class="alert alert-small alert-warning" id="eintegrationerr" style="display: none; position: relative; top:-8px;"><i class="icon error_icon"></i><span id="eintegrationmsg" style="display: inline; float:none;">请填写正确的兑换比例和单位。</span></span>
        </span>
    </div>
    <?php
    }
    else
    {
    ?>
    <a id="openCoinWall" class="open_coin btn btn-warning btn-warning-noboder"><i class="icon icon_coin_wall"></i>启用积分墙</a>
    <?php
    }
    if($isfeeds == 0)
    {
    ?>
    <a id="openFeeds" class="open_feeds btn btn-warning btn-warning-noboder"><i class="icon icon_coin_wall"></i>启用信息流</a>
    <?php 
    }
    ?>
</div>


<!-- 上传应用 -->
<div  id="upLoadAppLightBox" style="display: none;">
    <div class="light_box_content">
        <ul class="up_tab_item" id="upTabItem">
            <li class="active" data-tab="upload_app"><a href="javascript:;">上传应用</a></li>
            <li data-tab="offer_url"><a href="javascript:;">提供地址</a></li>
        </ul>

        <div class="tab_box up_tab_box upload_app" style="display: block;">
            <div class="up_box">
                <input type="file" name="uploadata"  id="uploadfile1" onchange='ajaxFileUpload(this)'>
                <input type="hidden" id="url" name="url">
                <span id ='submitid' class="loading" style="display: none;"><img src="/public/version1.0/images/loading.gif">文件上传中，请稍候.....</span>
            </div>
            <div class="up_success" style="display: none">
                <div class="reset"><a id="resubmitid" href="javascript:void(0);">重新选择文件</a></div>
                <div class="text_center" id="up_success_node">
                    <i class="icon icon_success"></i>
                    <div class="success_font">上传成功！</div>
                </div>
            </div>
            <div class="up_warning" style="display: none">
                <div class="text_center">
                    <i class="icon icon_warning"></i>
                    <div id="up_err" class="up_err" ></div>
                </div>
            </div>
            <div class="info" id="description">
                <div class="fl"><label>注</label></div>
                <div class="fl">
                    <p>1.请上传后缀为<?php if($ostypeid == 1){echo ".apk";}else{echo ".ipa";}?>的文件，文件大小不能超过50M。</p>
                    <p>2.应用提交后，无法撤回或修改，请确保待上传文件无错误。</p>
                </div>
            </div>
            <div class="text_center" id="descriptionsuccess" style="padding-top: 15px; display: none">
                <p>文件已上传成功，请点击"提交审核"后等待审核。</p>
                <p>24小时内会有答复，如有疑问，请联系客服</p>
            </div>
        </div>
        <div class="tab_box up_tab_box offer_url" style="display: none;">
            <div id="fixedUrl" class="fixed_url">
                <input type="text" id="marketurl" class="form-control form-control-small" style="width:100%;" placeholder="http://">
            </div>
<!--            <div id="errorTitle" class="text_center error_title" style="display: none">-->
<!--                <i class="icon icon_lose"></i>-->
<!--                <span class="cue" id="save_err"></span>-->
<!--            </div>-->
        </div>
        <input type="hidden" id="auditype" name="auditype">
        <div class="errorEmpty" id="subauditerr"  style="display: none">
            <i class="icon icon_lose"></i>
            <span id="subauditmsg" class="sub_audit_msg">请先上传指定格式文件或填写下载地址。</span>
        </div>
    </div>

</div>

<!-- 启用积分墙 -->
<div id="openCoinWallLightBox" style="display: none;">
    <div id="light_box_content" class="light_box_content">
        <div class="dashed_box">
            <h4><span>步骤①</span></h4>
            <strong class="title">设置积分兑换比例</strong>
            <div class="con"><span style="font-family: 'arial'">&yen;</span> 1=&nbsp;&nbsp;&nbsp;<span>
                    <input class="input_text" type="text" name="sintegration" id="sintegration" value="" size="6">
                    <input class="input_text" type="text" name="sunit" id="sunit" value="金币" size="6">
                    <div class="form_info_cue_errer" id="sintegrationerr" style="display: none;">
                        <i class="icon icon_lose"></i>
                        <em id="sintegrationmsg">请填写正确的兑换比例和单位。</em>
                    </div>
                </span>
            </div>
        </div>
        <div class="dashed_box">
            <h4><span>步骤②</span></h4>
            <strong class="title">新建积分墙广告位</strong>
            <div class="con">请于广告位管理页新建积分墙广告位，并在嵌入SDK时配置好积分墙广告位。</div>
        </div>
    </div>
</div>

<script type="text/javascript" src="/public/js/ajaxfileupload.js"></script>
<script>
//打开feeds的弹出层
$('#openFeeds').bind('click',function(e){
    art.dialog({
        title:'开启信息流广告<span class="bl">是否确认开启信息流广告：</span>',
        lock: true,
        fixed:true,
        ok:function(){
            var isfeeds = 1;
            var appid = <?php echo $appid?>;

            $.post("/manage/appinfo/dofeeds", {
            		isfeeds:isfeeds,
                    appid:appid
                },
                function(data)
                {
                    if(data.status){
                        location.reload();
                        return true;
                    }else{
                        alert("未知错误！");
                        location.reload();
                    }
                },
                'json');
            return false;
        },
        cancel:true,
        background: '#fff', // 背景色
        opacity: 0.6,	// 透明度
        content:'',
        id: 'upLoadAppFeedsLightBox',
        okVal:'确认',
        cancelVal:'取消'
    });
    e.preventDefault();
})

    var _upload_this_button;
    function ajaxFileUpload(fileObj){
        var obj       = $(fileObj);
        var fileId 	  = obj.attr('id');
        var str       = '';
        $("#up_err").html('');
        $("#submitid").show();
        $.ajaxFileUpload({
            url:'/manage/appinfo/ajaxVerifyApp?appid='+'<?php echo $appid?>',
            secureuri:false,
            fileElementId:fileId,
            dataType: 'json',
            success: function(data){
                $("#submitid").hide();
                $('#subauditerr').hide();
                if(data.status)
                {
                    $(".up_warning").hide();
                    $("#url").val(data.data);
                    $("#uploadfile1").hide();
                    $("#description").hide();
                    $("#descriptionsuccess").show();
                    $(".up_success").show();

                    if(_upload_this_button){
                        _upload_this_button.button({
                            name:'提交审核',
                            disabled:false
                        });
                    }
                }
                else{
                    $("#up_err").html(data.data);
                    $(".up_warning").show();
                }
            }
        });
    }

    $(document).on('click','#upTabItem li',function(){
        var box_class = $(this).data('tab');
        $(this).addClass('active').siblings('li').removeClass('active');
        $('.'+box_class).parent().find('.tab_box').hide();
        $('.'+box_class).show();
        $("#errorTitle").hide();
        $(".up_warning").hide();
        $("#subauditerr").hide();
        $('#resubmitid').click();
        $('#marketurl').val('');
        if(_upload_this_button){
            _upload_this_button.button({
                name:'提交审核',
                disabled:true
            });
        }
    })
    //重新选择文件
    $(document).on('click','#resubmitid',function(){
        $("#uploadfile1").show();
        $(".up_warning").hide();
        $(".up_success").hide();
        $("#description").show("");
        $("#descriptionsuccess").hide();
        if(_upload_this_button){
            _upload_this_button.button({
                name:'提交审核',
                disabled:true
            });
        }
    });
    //提供地址
    $(document).on('blur keyup','#marketurl',function(e){
        var marketurl = $("#marketurl").val();
        var appid = <?php echo $appid?>;

        if(_upload_this_button){
            if(marketurl == ''){
                _upload_this_button.button({
                    name:'提交审核',
                    disabled:true
                });
            }else{
                _upload_this_button.button({
                    name:'提交审核',
                    disabled:false
                });
            }
        }
    });
    //重新填写提供地址url
    $(document).on('focus ','#marketurl',function(e){
        $("#auditypemsg").html('');
        $("#subauditerr").hide();
    });


    //打开上传应用的弹出层
    $('#upLoadApp').bind('click',function(e){
        $('.up_tab_box').hide();
        $('.up_tab_box').eq(0).show();
        $('.up_warning').hide();
        $("#subauditerr").hide();
        $('.up_success').hide();
        $('#errorTitle').hide();
        var alertBox = art.dialog({
            title:'提交审核<span>（通过审核后才能获得收入！）</span>',
            lock: true,
            fixed:true,
            background: '#fff', // 背景色
            opacity: 0.6,	// 透明度
            content: $('#upLoadAppLightBox').html(),
            id: 'upLoadAppLightBox',
            button: [
                {
                    name: '提交审核',
                    callback: function () {
                        $("#marketurl").blur();
                        var marketurl = $("#marketurl").val();
                        var url = $("#url").val();
                        var appid = <?php echo $appid?>;
                        var auditype;
                        var _this = this;
                        $(".up_warning").hide();
                        if($('#subauditerr').css('display')=='block'){
                            return false;
                        }
                        _this.content('<div id="loading" style="text-align: center; padding-top: 15px;"><img src="/public/version1.0/images/loading.gif">文件正在提交中，请耐心等待......</div>')
                            .button({name:'提交审核',disabled:true});

                        if($('.upload_app').css("display")=='none'){
                            $('#auditype').val(2);
                            $.post("/manage/appinfo/doaudit", {
                                    url:marketurl,
                                    appid:appid
                                },
                                function(data)
                                {
                                    if(data.status){
                                        $("#errorTitle").hide();
                                        $("#auditypemsg").html(marketurl);
                                        $("#save_err").html('');
                                        $('#subauditerr').hide();

                                        $.post("/manage/appinfo/dostatus", {
                                                url:url,
                                                marketurl:marketurl,
                                                auditype:$('#auditype').val(),
                                                appid:appid
                                            },
                                            function(data)
                                            {
                                                if(data.status){
                                                    location.reload();
                                                }else{
                                                    $("#subauditmsg").html(data.data);
                                                    $("#subauditerr").show();
                                                    _this.content($('#upLoadAppLightBox').html());

                                                    return false;
                                                }
                                            },
                                            'json');
                                    }else{
                                        $("#subauditmsg").html(data.data);
                                        $("#subauditerr").show();
                                        _this.content($('#upLoadAppLightBox').html());
                                        if(_upload_this_button){
                                            _upload_this_button.button({
                                                name:'提交审核',
                                                disabled:true
                                            });
                                        }
                                    }
                                },
                                'json');
                        }else{
                            $('#auditype').val(1);
                            $.post("/manage/appinfo/dostatus", {
                                    url:url,
                                    marketurl:marketurl,
                                    autitype:$('#auditype').val(),
                                    appid:appid
                                },
                                function(data)
                                {
                                    if(data.status){
                                        location.reload();
                                    }else{
                                        $("#subauditmsg").html(data.data);
                                        $("#subauditerr").show();
                                        _this.content($('#upLoadAppLightBox').html());

                                        return false;
                                    }
                                },
                                'json');
                        }
                        return false;
                    },
                    focus: true
                },
                {
                    name: '关闭',
                    callback: function () {
                        return true;
                    }
                }
            ]
        });
        _upload_this_button = alertBox.button({
            name:'提交审核',
            disabled:true
        })
        e.preventDefault();
    });
    //打开积分墙的弹出层
    $('#openCoinWall').bind('click',function(e){
        art.dialog({
            title:'开启积分墙<span class="bl">开启积分墙，你还需要完成以下步骤：</span>',
            lock: true,
            fixed:true,
            ok:function(){
                var integration = $("#sintegration").val();
                var unit = $.trim($("#sunit").val());
                var isintegral = 1;
                var trusteeshiptype = 2;
                var appid = <?php echo $appid?>;
                $("#sintegrationerr").hide();

                if(integration == '' || unit == '')
                {
                    $("#sintegrationerr").show();
                    return false;
                }
                $.post("/manage/appinfo/dointegral", {
                        integration:integration,
                        unit:unit,
                        isintegral:isintegral,
                        appid:appid
                    },
                    function(data)
                    {
                        if(data.status){
                            location.reload();
                            return true;
                        }else{
                            alert("未知错误！");
                            location.reload();
                        }
                    },
                    'json');
                return false;
            },
            cancel:true,
            background: '#fff', // 背景色
            opacity: 0.6,	// 透明度
            content: $('#openCoinWallLightBox').html(),
            id: 'upLoadAppLightBox',
            okVal:'确认',
            cancelVal:'取消'
        });
        e.preventDefault();
    })

    $(function(){
        var r = /^\+?[0-9][0-9]*$/;
        $('.icon_help').tooltip();

        $(document).delegate('#openCoinWall','click',function(){
            $('.light_box_content').niceScroll({
                cursorcolor:"#d8d9dc",
                cursoropacitymax:1,
                touchbehavior:false,
                horizrailenabled:false,
                cursorwidth:"5px",
                cursorborder:"0",
                cursorborderradius:"5px"
            });
        })

        //编辑金币
        //判断输入框必须为数字
        $('#eintegration').bind('keyup',function(){
            var val = $(this).val();
            if(!(r.test(val)) || val <= 0){
                alert('请输入大于0的正整数！');
                $(this).val('');
                $(this).focus();
                return false;
            }
        })

        $('#ratioinput .ok').click(function(){
            var integration = $("#eintegration").val();
            var unit = $("#eunit").val();
            var appid = $("#appid").val();

            if(integration == '' || unit == '' || !(r.test(integration)))
            {
                $("#eintegrationerr").show();//错误提示显示
                return ;
            }

            $.post("/manage/appinfo/doeditintegration", { integration:integration, unit:unit, appid:appid}, function(data){
                if(data && data['status'] ==1){
                    $(".ratioinput").hide();
                    $("#showintegration").html(integration+' '+unit+'<i adtype="2" ostype="1" id="editIcon" class="icon icon_edit_con"></i>');
                    $("#showintegration").show();
                    $("#eintegrationerr").hide();
                }
                else{
                    $("#eintegrationerr").show();
                }

            }, 'json');

        })
        $("#showintegration").on('click','#editIcon',function(){
            $("#eintegrationerr").hide();//错误提示隐藏
            $("#ratioinput").show();
            $("#showintegration").hide();
        });
        $("#ratioinput .cancel").bind("click",function(){
            $(this).parent().hide();
            $("#showintegration").show();
            $("#eintegrationerr").hide();
        });

        $(document).on('keyup','#sintegration',function(){
            $("#sintegrationerr").hide();
            var val = $(this).val();
            if(!(r.test(val)) || val <= 0){
                alert('请输入大于0的正整数！');
                $(this).val('');
                $(this).focus();
                return false;
            }
        })
        //设置广告开关
        $(".on_off").click(function(e){
            var appid = $("#appid").val();
            var obj = this;

            if($(obj).hasClass("off")){
                art.dialog({
                    title:'<i class="icon icon_node"></i>广告开关提示',
                    lock: true,
                    fixed:true,
                    ok:function(){
                        $.post("/manage/appinfo/ajaxSwitch/", {appid:appid, 'switch':1}, function(data){
                            if(data && data['status']==1){
                                $(obj).addClass("on");
                                $(obj).removeClass("off");
                                return true;
                            }else{
                                if(data && data['info']){
                                    alert(data['info']);
                                }
                            }
                        }, 'json');
                    },
                    cancel:true,
                    background: '#fff', // 背景色
                    opacity: 0.6,	// 透明度
                    content: '<div style="padding:10px 25px; font-size:14px; text-align: center;">你确定要开启广告吗？</div>',
                    id: 'editLightBox',
                    okVal:'确认',
                    cancelVal:'取消'
                });
            }else{
                art.dialog({
                    title:'<i class="icon icon_node"></i>广告开关提示',
                    lock: true,
                    fixed:true,
                    ok:function(){
                        $.post("/manage/appinfo/ajaxSwitch/", { appid:appid, 'switch':0}, function(data){
                            if(data && data['status']==1){
                                $(obj).addClass("off");
                                $(obj).removeClass("on");
                                return true;
                            }else{
                                if(data && data['info']){
                                    alert(data['info']);
                                }
                            }
                        }, 'json');
                    },
                    cancel:true,
                    background: '#fff', // 背景色
                    opacity: 0.6,	// 透明度
                    content: '<div style="padding:10px 25px; font-size:14px; text-align: center;">你确定要关闭广告吗？</div>',
                    id: 'editLightBox',
                    okVal:'确认',
                    cancelVal:'取消'
                });
            }
        });

    })
</script>




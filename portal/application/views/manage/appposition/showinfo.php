<!--  主体部分 [ -->
<div class="inner_container" id="innerContainer">
    <?php $this->load->view('manage/inc/left')?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <?php $this->load->view('manage/inc/righttop')?>
                <div class="inner_tab">
                    <ul class="tab_item">
                        <li><a href="/manage/appchannel/showinfo/?appid=<?php echo $appid?>">渠道管理</a></li>
                        <li class="active"><a href="javascript:;">广告位管理</a></li>
                        <li><a href="/manage/apptag/showinfo/?appid=<?php echo $appid?>">标签管理</a></li>
                        <li><a href="/manage/appinfo/showinfo/?appid=<?php echo $appid?>">基本信息</a></li>
                        <?php if($isintegral == 1){  ?><li ><a href="/manage/appinfo/backconfig/?appid=<?php echo $appid?>">积分回调方式</a></li><?php } ?>
                    </ul>
                    <div class="jumbotron jumbotron_no_shadow tab_box ad_manager">
                        <div class="note">
                            <p>* 为方便您的产品接入SDK，允许广告位ID为空，并定义默认广告位</p>
                            <p>* 建议您创建多个广告位，分别管理不同的广告触发时机</p>
                        </div>
                        <table class="table table-bordered no-margin-bottom">
                            <thead>
                                <tr>
                                    <th>广告位ID(PlacementID)</th>
                                    <th>广告位名称</th>
                                    <th>广告位形式</th>
                                    <th>广告位描述</th>
                                    <th>操作</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            //标识判断广告位ID是否有不为空的广告位
                            $tmp = 0;
                            if(!empty($positionList))
                            {
                                foreach ($positionList as $v)
                                {
                                    if(empty($v['positionid']))
                                    {
                                        continue;
                                    }
                                    ?>
                                    <tr class="" style="word-wrap: break-word;word-break: normal;">
                                        <td><?php echo $v['positionid']?></td>
                                        <td width="20%"><?php echo $v['positionname']?></td>
                                        <td><?php
                                            $str = 'Banner广告';
                                            switch ($v['adform'])
                                            {
                                                case 1:
                                                    $str = 'Banner广告';
                                                    break;
                                                case 2:
                                                    $str = '插屏广告';
                                                    break;
                                                case 3:
                                                    $str = '推荐墙广告';
                                                    break;
                                                case 4:
                                                    $str = '积分墙广告';
                                                    break;
                                                case 20:
                                                    $str = '信息流广告';
                                                    break;
                                            }
                                            echo $str;
                                            ?></td>
                                        <td width="30%" style="word-wrap:break-word;word-break: break-all;"><?php echo $v['positiondesc']?></td>
                                        <td>
                                            <?php if(!empty($v['positionid'])){?><a class="edit" href="/manage/appposition/showinfo/?appid=<?php echo $appid?>&positionid=<?php echo $v['positionid']?>">编辑</a>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <a class="delete" href="javascript:;" onclick="return ConfirmDel('/manage/appposition/dodel/?appid=<?php echo $appid?>&positionid=<?php echo $v['positionid']?>&bordergroupid=<?php echo $v['bordergroupid']?>')">删除</a><?php }?>
                                        </td>
                                    </tr>
                                    <?php
                                    $tmp++;
                                }
                            }
                            if($tmp == 0)
                            {
                                echo '<tr id="emptyPosition" class="empty_container"><td colspan="5"><span class="non_empty"><i class="icon node_icon"></i>点击可创建新的广告位</span>默认广告位不展示在广告位栏，如需灵活控制广告触发时机，建议您创建新广告位！</td></tr>';
                            }
                            ?>
                            </tbody>
                        </table>
                        <?php if (empty($positionInfo)){?>
                        <div class="function_btn">
                            <a id="addPosition" class="btn btn-primary btn-primary-noboder"><i class="icon add_icon"></i>新增广告位</a>
                        </div>
                        <?php }?>
                        <br>
                        <?php if (!empty($positionInfo)){?>
                        <div id="commonPosition">
                            <form id="frm" name="frm" action="" method="post">
                                <div  class="new_position jumbotron_no_shadow">
                                    <h3 class="fixed_title">编辑广告位</h3>
                                    <div style="padding-left: 100px;">
                                        <div class="form-group">
                                            <span class="fl form-label">广告位ID</span>
                                            <span class="fl values"><?php echo $positionInfo['positionid']?></span>
                                            <span class="form_info_cue_errer hidden" id="positioniderr"><b class="icon_errer"></b><em id="positionidmsg"></em></span>
                                            <input type="hidden" value="<?PHP echo $positionInfo['positionid']?>" id="positionid" name="positionid">
                                            <input type="hidden" value="<?PHP echo $positionInfo['appid']?>" id="appid" name="appid">
                                        </div>
                                        <div class="form-group">
                                            <span class="fl form-label"><i class="require_item">*</i>广告位名称</span>
                                            <span class="fl">
                                                <input type="text" name="positionname" id="positionname"  class="fl form-control form-control-small" placeholder="广告位名称" value="<?PHP echo $positionInfo['positionname']?>">
                                                <span class="fl alert alert-small alert-warning" id="positionnameerr"  style="display: none; margin-left:10px;"><b class="icon error_icon"></b><em id="positionnamemsg"></em></span>
                                            </span>
                                        </div>
                                        <div class="form-group">
                                            <span class="fl form-label">广告位描述</span>
                                            <span class="fl">
                                                <textarea  name="positiondesc" id="positiondesc" maxlength="500" class="fl form-control form-control-small" placeholder="广告位描述"><?PHP echo $positionInfo['positiondesc']?></textarea>
                                            </span>
                                        </div>
                                        <div class="form-group">
                                            <span class="fl form-label"><i class="require_item">*</i>广告形式</span>
                                            <span id="adformdiv" class="fl radio_label">
                                                <label>
                                                    <?PHP if($positionInfo['adform'] == 1) echo "Banner广告"?>
                                                    <?PHP if($positionInfo['adform'] == 2) echo "插屏广告"?>
                                                    <?PHP if($positionInfo['adform'] == 3) echo "推荐墙广告"?>
                                                    <?PHP if($positionInfo['adform'] == 4) echo "积分墙广告"?>
                                                    <?PHP if($positionInfo['adform'] == 20) echo "信息流广告"?>
                                                </label>
                                                <span style="display: none" class="form_info_cue_errer" id="adformerr"><b class="icon_errer"></b><em id="adformmsg">请选择广告形式。</em></span>
                                            </span>
                                        </div>

                                        <div id="integrationdiv" class="integral" style="display: <?php if($isintegral != 1 || ($positionInfo['adform'] != 4)){echo "none";}?>">
                                            <i class="icon icon_jiao"></i>
                                            <div class="box">
                                                <label class="fl">兑换比例：</label>
                                                <div class="fl">
                                                    1人民币 = <?php echo $integration;?> <?php echo $unit;?>
                                                </div>
                                            </div>
                                            <div class="box">
                                                <label class="fl">选择托管方式：</label>
                                                <div class="fl entrust_form">
<!--                                                    <label><input name="trusteeshiptype"  value="1" type="radio" disabled>SDK积分托管</label>-->
                                                    <label><input  name="trusteeshiptype" class="osclass" value="2" type="radio" checked>自定义积分账户</label>
<!--                                                    <label><input name="trusteeshiptype" class="osclass" value="3" type="radio" disabled>服务器积分订单回调</label>-->
                                                    <span style="display: none" class="form_info_cue_errer" id="trusteeshiptypeerr"><b class="icon_errer"></b><em id="trusteeshiptypemsg">请选择托管方式。</em></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="display: none">
                                            <span class="fl form-label">奖励返还提示语:</span>
                                            <span class="fl">
                                                <textarea name="rewardesc" id="rewardesc" class="fl form-control form-control-small" maxlength="500" placeholder="奖励返还提示语"><?PHP echo $positionInfo['rewardesc']?></textarea>
                                            </span>
                                        </div>
                                        <div id="popupdiv" class="form-group" style="display: <?PHP if($positionInfo['adform'] != 2) echo "none"?>;">
                                                <div class="form-group">
                                                    <span class="fl form-label"><i class="require_item">*</i>广告形态</span>
                                                    <span id="adstylech" class="fl radio_label">
                                                    	<label><input name="adstyle" id="adstyle" value="0" type="radio" <?PHP if($positionInfo['adstyle'] == 0) echo "checked"?>>全屏</label>
                                                    	<label><input name="adstyle" id="adstyle" value="1" type="radio" <?PHP if($positionInfo['adstyle'] == 1) echo "checked"?>>自定义</label>
                                                    </span>
                                                </div>
                                                <div class="form-group" id="adstylediv" style="display: <?PHP if($positionInfo['adstyle'] == 0) echo "none"?>;">
                                                    <div class="form-group">
                                                        <span class="fl form-label"><i class="require_item">*</i>图片尺寸</span>
                                                        <span class="fl radio_label">
                                                        	<label><input name="adstyleimgsize" id="adstyleimgsize" value="1" type="radio" <?PHP if($positionInfo['adstyleimgsize'] == 1 || empty($positionInfo['adstyleimgsize'])) echo "checked"?>> 3:2</label>
                                                        	<label><input name="adstyleimgsize" id="adstyleimgsize" value="2" type="radio" <?PHP if($positionInfo['adstyleimgsize'] == 2) echo "checked"?>> 6:5</label>
                                                        </span>
                                                    </div>
                                                    <div class="form-group" >
                                                        <span class="fl form-label"><i class="require_item">*</i>插屏尺寸</span>
                                                        <span class="fl radio_label">
                                                        	<select name="adstylesize" id="adstylesize">
                                                            	<option value="90" <?PHP if($positionInfo['adstylesize'] == 90 || empty($positionInfo['adstylesize'])) echo "selected"?>>90%</option>
                                                            	<option value="75" <?PHP if($positionInfo['adstylesize'] == 75) echo "selected"?>>75%</option>
                                                            	<option value="60" <?PHP if($positionInfo['adstylesize'] == 60) echo "selected"?>>60%</option>
                                                            	<option value="50" <?PHP if($positionInfo['adstylesize'] == 50) echo "selected"?>>50%</option>
                                                        	</select>
                                                        </span>
                                                    </div>
                                                    <div class="form-group" style="display:none">
                                                        <span class="fl form-label"><i class="require_item">*</i>边框颜色</span>
                                                        <span id="bgcolordiv" class="fl radio_label">
                                                        	<select id="adstylecolor" name="adstylecolor" style="width: 100px;background-color: #<?php echo $positionInfo['adstylecolor'];?>">
                                                            	<option <?PHP if($positionInfo['adstylecolor'] == "") echo "selected"?> style="background-color:#FFFFFF" val="#FFFFFF" value="">无</option>
                                                            	<option <?PHP if($positionInfo['adstylecolor'] == "FFF68F") echo "selected"?> style="background-color:#FFF68F" val="#FFF68F" value="FFF68F"></option>
                                                            	<option <?PHP if($positionInfo['adstylecolor'] == "FF8C00") echo "selected"?> style="background-color:#FF8C00" val="#FF8C00" value="FF8C00"></option>
                                                            	<option <?PHP if($positionInfo['adstylecolor'] == "FF82AB") echo "selected"?> style="background-color:#FF82AB" val="#FF82AB" value="FF82AB"></option>
                                                            	<option <?PHP if($positionInfo['adstylecolor'] == "87CEFA") echo "selected"?> style="background-color:#87CEFA" val="#87CEFA" value="87CEFA"></option>
                                                            	<option <?PHP if($positionInfo['adstylecolor'] == "5CACEE") echo "selected"?> style="background-color:#5CACEE" val="#5CACEE" value="5CACEE"></option>
                                                            	<option <?PHP if($positionInfo['adstylecolor'] == "7FFFD4") echo "selected"?> style="background-color:#7FFFD4" val="#7FFFD4" value="7FFFD4"></option>
                                                            	<option <?PHP if($positionInfo['adstylecolor'] == "66CDAA") echo "selected"?> style="background-color:#66CDAA" val="#66CDAA" value="66CDAA"></option>
                                                            	<option <?PHP if($positionInfo['adstylecolor'] == "458B74") echo "selected"?> style="background-color:#458B74" val="#458B74" value="458B74"></option>
                                                            	<option <?PHP if($positionInfo['adstylecolor'] == "EEEEEE") echo "selected"?> style="background-color:#EEEEEE" val="#EEEEEE" value="EEEEEE"></option>
                                                            	<option <?PHP if($positionInfo['adstylecolor'] == "AEAEAE") echo "selected"?> style="background-color:#AEAEAE" val="#AEAEAE" value="AEAEAE"></option>
    	                                                       	<option <?PHP if($positionInfo['adstylecolor'] == "767676") echo "selected"?> style="background-color:#767676" val="#767676" value="767676"></option>
                                                            	<option <?PHP if($positionInfo['adstylecolor'] == "454545") echo "selected"?> style="background-color:#454545" val="#454545" value="454545"></option>
                                                        	</select>
                                                        </span>
                                                    </div>
                                                    
                                                     <div id="borderdiv" class="form-group">
                                                        <span class="fl form-label"><i class="require_item">*</i>边框设置</span>
                                                        <span class="fl radio_label">
                                                        	<label><input name="bordertype" id="bordertype" value="0" type="radio" <?PHP if($positionInfo['bordertype'] == "0") echo "checked"?>>无边框</label>
                                                        	<label><input name="bordertype" id="bordertype" value="1" type="radio" <?PHP if($positionInfo['bordertype'] == "1") echo "checked"?>>系统边框</label>
                                                        	<label><input name="bordertype" id="bordertype" value="2" type="radio" <?PHP if($positionInfo['bordertype'] == "2") echo "checked"?>>自定义边框</label>
                                                        	<input id="oldBorderType" type="hidden" value="<?php echo $positionInfo['bordertype'];?>"/>
                                                        	<input type="hidden" id="oldBorderGroupId" value="<?php echo $positionInfo['bordergroupid'];?>"/>
                                                        </span>
                                                    </div>
                                                    <div id="sysBorder" class="form-group" style="height: 250px; display:<?PHP if($positionInfo['bordertype'] != "1") echo "none"?>">
						                                <span class="fl form-label"><i class="require_item">*</i>系统边框</span>
						                                <div class="subform">
						                                <span class="fl radio_label check_labels">
							                                <label><input name="borderpolicy" id="borderpolicy" value="0" type="radio" style="display:none" <?PHP if($positionInfo['borderpolicy'] == "0") echo "checked"?>></label>
							                                <label><input name="borderpolicy" id="borderpolicy" value="1" type="radio" <?PHP if($positionInfo['borderpolicy'] == "1") echo "checked"?>>使用单个边框</label>
							                        		<?php if(!$sysBorderList){?>
							                                	<span class="fl alert alert-small alert-warning"><em>暂无系统边框</em></span><br/><br/>
							                                <?php }
							                                else{
							                                	if($sysBorDelFlag){?>
							                                	<div>
							                                		<label class="fl">原有系统边框已失效:</label>
								                                	<img class="border checkedborder"  id="<?php echo $myBorderList[0]['bordergroupid']?>" disabled="disabled" src="http://res.cocounion.com<?php echo $myBorderList[0]['path']?>"  alt="已失效边框 cannot shown"/>
								                                	
							                                	</div>
							                                	<br/>
							                                	<?php }?>
							                                	<div>
								                                	<label class="fl">可选系统边框：&nbsp;&nbsp;&nbsp;&nbsp;</label>
								                                	<?php foreach($sysBorderList as $v){?>
								                                	<img class="border <?PHP if($positionInfo['bordergroupid'] == $v['bordergroupid']) echo "checkedborder"?>" id="<?php echo $v['bordergroupid']?>" src="http://res.cocounion.com<?php echo $v['path']?>"  alt="可用边框 cannot shown"/>
								                                <?php 
								                                	}?>
							                                	</div><br/>
						                               		<?php }?>
							                                <input type="hidden" id="sysBorDelFlag" value="<?PHP if($sysBorDelFlag){ echo 0;} else echo 1;?>">
							                                <input type="hidden" id="sysBorderGroupId" value="<?PHP if($positionInfo['bordertype'] == '1' && $sysBorDelFlag==FALSE) echo $positionInfo['bordergroupid']; else echo 0;?>">
	                                                        <label><input name="borderpolicy" id="borderpolicy" value="2" type="radio" <?PHP if($positionInfo['borderpolicy'] == "2") echo "checked"?>>所有边框随机</label>
	                                                        <span style="display: none;" class="fl alert alert-small alert-warning" id="sysBordererr"><i class="icon error_icon"></i><b id="sysBordermsg"></b></span>
                                                        </span>
						                            	</div>
						                            </div>
						                           	 
						                            <div id="cusBorder" class="form-group" style="display:<?PHP if($positionInfo['bordertype'] != "2") echo "none"?>;">
						                            
						                            	<span class="fl form-label"><i class="require_item">*</i>上传边框</span>
						                            	<div style="margin:13px;">
							                            	<div class="form_input"><label class="cueinfo">图片格式支持JPG、PNG；文件大小不超过100KB。</label></div>
															<span class="form_info_cue_errer hidden" id="imgerr"><b class="icon_errer"></b><em></em></span>
								
								                            <div class="uploadBorder">
								                            
								                            
								                            <div class="form_label"></div>
					    									<div class="borderImg">
					    										<input type="file" name='Filedata'  id="border1" class='uploadify' onchange='ajaxUploder(this,"600*500")'/>&nbsp;&nbsp; <br/>
					    										<div class="form_input"><label class="cueinfo">图片尺寸：600×500 图片要求：外部窗口600*500，内部窗口566*468</label>
					    										<span id="bordererr1" style="display: none; margin-left:10px;"><i class="icon error_icon"></i><span id="bordermsg1"></span></span>
					    										</div>
					    										<div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
					    										<?php if(isset($borderImgList['12'])){?><div class="border1_image_show_box"><img src="/public/upload/<?php echo $borderImgList['12']['path'];?>" width="100" hight="100">
					    										<input type="hidden" id="img_border1" name="<?php if(isset($borderImgList['12'])){echo $borderImgList['12']['borderid']; }?>" value="<?php echo $borderImgList['12']['path'];?>"/></div><?php }?>
					    									</div><br>
					    									
							                            	
							                            	<div class="form_label"></div>
					    									<div class="borderImg">
					    										<input type="file" name='Filedata'  id="border2" class='uploadify'  onchange='ajaxUploder(this,"500*600")'/>&nbsp;&nbsp; <br/>
					    										<div class="form_input"><label class="cueinfo">图片尺寸：500*600 图片要求：外部窗口500*600，内部窗口466*568</label>
					    										<span id="bordererr2" style="display: none; margin-left:10px;"><i class="icon error_icon"></i><span id="bordermsg2"></span></span>
					    										</div>
					    										<div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
					    										<?php if(isset($borderImgList['13'])){?><div class="border2_image_show_box"><img src="/public/upload/<?php echo $borderImgList['13']['path'];?>" width="100" hight="100">
					    										<input type="hidden" id="img_border2" name="<?php if(isset($borderImgList['13'])){echo $borderImgList['13']['borderid']; }?>" value="<?php echo $borderImgList['13']['path'];?>"/></div><?php }?>
					    									</div><br>
							                            
							                            	<div class="form_label"></div>
					    									<div class="form_input">
					    										<input type="file" name='Filedata'  id="border3" class='uploadify'  onchange='ajaxUploder(this,"900*600")'/>&nbsp;&nbsp; <br/>
					    										<div class="form_input"><label class="cueinfo">图片尺寸：900×600 图片要求：外部窗口900*600，内部窗口866*568</label>
					    										<span id="bordererr3" style="display: none; margin-left:10px;"><i class="icon error_icon"></i><span id="bordermsg3"></span></span>
					    										</div>
					    										<div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
					    										<?php if(isset($borderImgList['8'])){?><div class="border3_image_show_box"><img src="/public/upload/<?php echo $borderImgList['8']['path'];?>" width="100" hight="100">
					    										<input type="hidden" id="img_border3" name="<?php if(isset($borderImgList['8'])){echo $borderImgList['8']['borderid']; }?>" value="<?php echo $borderImgList['8']['path'];?>"/></div><?php }?>
					    									</div><br>
					    									<em class="red" ><b class="icon_error" id='1692_default_imgs_error'></b></em>
							                           
							                            	<div class="form_label"></div>
					    									<div class="form_input">
					    										<input type="file" name='Filedata'  id="border4" class='uploadify'  onchange='ajaxUploder(this,"600*900")'/>&nbsp;&nbsp; <br/>
					    										<div class="form_input"><label class="cueinfo">图片尺寸：600*900 图片要求：外部窗口600*900，内部窗口566*868</label>
					    										<span id="bordererr4" style="display: none; margin-left:10px;"><i class="icon error_icon"></i><span id="bordermsg4"></span></span>
					    										</div>
					    										<div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
					    										<?php if(isset($borderImgList['9'])){?><div class="border4_image_show_box"><img src="/public/upload/<?php echo $borderImgList['9']['path'];?>" width="100" hight="100">
					    										<input type="hidden" id="img_border4" name="<?php if(isset($borderImgList['9'])){echo $borderImgList['9']['borderid']; }?>" value="<?php echo $borderImgList['9']['path'];?>"/></div><?php }?>
					    									</div><br>
					    									<em class="red" ><b class="icon_error" id='1692_default_imgs_error'></b></em>
							                            
							                            	<div class="form_label"></div>
					    									<div class="form_input">
					    										<input type="file" name='Filedata'  id="border5" class='uploadify'  onchange='ajaxUploder(this,"1280*720")'/>&nbsp;&nbsp; <br/>
					    										<div class="form_input"><label class="cueinfo">图片尺寸：1280*720 图片要求：外部窗口1280*720，内部窗口1246*688</label>
					    										<span id="bordererr5" style="display: none; margin-left:10px;"><i class="icon error_icon"></i><span id="bordermsg5"></span></span>
					    										</div>
					    										<div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
					    										<?php if(isset($borderImgList['10'])){?><div class="border5_image_show_box"><img src="/public/upload/<?php echo $borderImgList['10']['path'];?>" width="100" hight="100">
					    										<input type="hidden" id="img_border5" name="<?php if(isset($borderImgList['10'])){echo $borderImgList['10']['borderid']; }?>" value="<?php echo $borderImgList['10']['path'];?>"/></div><?php }?>
					    									</div><br>
					    									<em class="red" ><b class="icon_error" id='1692_default_imgs_error'></b></em>
							                          
							                            	<div class="form_label"></div>
					    									<div class="form_input">
					    										<input type="file" name='Filedata'  id="border6" class='uploadify'  onchange='ajaxUploder(this,"720*1280")'/>&nbsp;&nbsp; <br/>
					    										<div class="form_input"><label class="cueinfo">图片尺寸：720*1280 图片要求：外部窗口720*1280，内部窗口686*1248</label>
					    										<span id="bordererr6" style="display: none; margin-left:10px;"><i class="icon error_icon"></i><span id="bordermsg6"></span></span>
					    										</div>
					    										<div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
					    										<?php if(isset($borderImgList['11'])){?><div class="border6_image_show_box"><img src="/public/upload/<?php echo $borderImgList['11']['path'];?>" width="100" hight="100">
					    										<input type="hidden" id="img_border6" name="<?php if(isset($borderImgList['11'])){echo $borderImgList['11']['borderid']; }?>" value="<?php echo $borderImgList['11']['path'];?>"/></div><?php }?>
					    									</div><br>
					    									<em class="red" ><b class="icon_error" id='1692_default_imgs_error'></b></em>
							                            	</div>
				    									</div>
						                            </div>
						                            <div id="cusClose" class="form-group" style="display:<?PHP if($positionInfo['bordertype'] != "2") echo "none"?>;">
						                            	<span class="fl form-label">上传关闭按钮</span>
						                            	<div style="margin:13px;">
							                            	<div class="form_input"><label class="cueinfo">图片格式支持JPG、PNG；文件大小不超过100KB。</label></div>
															<span class="form_info_cue_errer hidden" id="imgerr"><b class="icon_errer"></b><em></em></span>
															<div class="uploadBorder">
																<div class="form_label"></div>
						    									<div class="form_input" id="editCloseIcon">
						    										<input type="file" name='Filedata'  id="border7" class='uploadify'  onchange='ajaxUploder(this,"60*60")'/> 
						    										<a id="delCloseIcon" style="margin-left:10px;" class="btn" href="javascript:;">删除</a>  
						    										<br/>
						    										<div class="form_input"><label class="cueinfo">图片尺寸：60*60</label>
						    											<span id="bordererr7" style="display: none; margin-left:10px;"><i class="icon error_icon"></i><span id="bordermsg7"></span></span>
						    										</div>
						    										<div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
						    										<?php if(isset($borderImgList['icon'])){?><div class="border7_image_show_box"><img  src="/public/upload/<?php echo $borderImgList['icon']['path'];?>" width="100" hight="100">
						    										<input type="hidden" id="img_border7" name="<?php if(isset($borderImgList['icon'])){echo $borderImgList['icon']['borderid']; }?>" value="<?php echo $borderImgList['icon']['path'];?>"/></div><?php }?>
						    										
						    										<div id='1692_default_imgs_error' style="display: none; margin-left:10px;">
							    										<b class="icon error_icon"></b>
																		<em id="1692_default_imgs_msg"></em>
																	</div>
						    									</div><br>
						    									
						    								</div>
					    								</div>
				    								</div>

                                            </div>
                                    </div>
                                </div>
                                <div class="new_position_btn_group">
                                    <a href="javascript:;"id="editBtnOk" class="btn btn-success btn-success-noboder">确&nbsp;&nbsp;&nbsp;&nbsp;认</a>
                                    <a href="javascript:;" id="editBtnCancel" onclick="location.href='/manage/appposition/showinfo/?appid=<?php echo $appid?>';" class="close btn btn-default btn-default-noboder">取&nbsp;&nbsp;&nbsp;&nbsp;消</a>
                                </div>
                            </form>
                        </div>
                        <?php
                        }
                        else
                        {
                        ?>
                        <div id="newPosition" style="display: none;">
                            <form id="frm" name="frm" action="" method="post">
                                <div  class="new_position jumbotron_no_shadow" >
                                    <h3 class="fixed_title">新增广告位</h3>
                                    <div style="padding-left: 100px;">
                                        <div class="form-group">
                                            <span class="fl form-label">广告位ID</span>
                                            <span class="fl values"><?php echo $positionid;?></span>
                                            <span class="form_info_cue_errer hidden" id="positioniderr"><b class="icon_errer"></b><em id="positionidmsg"></em></span>
                                            <input type="hidden" value="<?php echo $positionid;?>" id="positionid" name="positionid">
                                            <input type="hidden" value="<?php echo $appid;?>" id="appid" name="appid">
                                        </div>
                                        <div class="form-group">
                                            <span class="fl form-label"><i class="require_item">*</i>广告位名称</span>
                                            <span class="fl">
                                                <input type="text" name="positionname" id="positionname"  class="fl form-control form-control-small" placeholder="广告位名称" value="">
                                            </span>
                                            <span class="fl alert alert-small alert-warning" id="positionnameerr" style="display: none; margin-left:10px;"><i class="icon error_icon"></i><span id="positionnamemsg"></span></span>
                                        </div>
                                        <div class="form-group">
                                            <span class="fl form-label">广告位描述</span>
                                            <span class="fl">
                                                <textarea  name="positiondesc" id="positiondesc" maxlength="500" class="fl form-control form-control-small" placeholder="广告位描述"></textarea>
                                            </span>
                                        </div>
                                        <div class="form-group">
                                            <span class="fl form-label"><i class="require_item">*</i>广告形式</span>
                                            <span id="adformdiv" class="fl radio_label">
                                                <label><input name="adform" id="adform"  value="1" type="radio" />Banner广告</label>
                                                <label><input name="adform" id="adform"  value="2" type="radio" />插屏广告</label>
                                                <label><input name="adform" id="adform"  value="3" type="radio" />推荐墙广告</label>
                                                <?php if($isintegral == 1)
                                                {
                                                    ?>
                                                    <label><input name="adform" id="adform"  value="4" type="radio" />积分墙广告</label>
                                                <?php
                                                }
                                                ?>
                                                <?php if($isfeeds == 1)
                                                {
                                                    ?>
                                                    <label><input name="adform" id="adform"  value="20" type="radio" />信息流广告</label>
                                                <?php
                                                }
                                                ?>
                                                <div class="alert alert-small alert-warning" style="margin-top:20px; display: none;" id="adformerr"><i class="icon error_icon"></i><span id="adformmsg">请选择广告形式。</span></div>
                                            </span>
                                        </div>

                                        <div id="integrationdiv" class="integral" style="display: none;">
                                            <i class="icon icon_jiao"></i>
                                            <div class="box">
                                                <label class="fl">兑换比例：</label>
                                                <div class="fl">
                                                    1人民币 = <?php echo $integration;?> <?php echo $unit;?>
                                                </div>
                                            </div>
                                            <div class="box">
                                                <label class="fl">选择托管方式：</label>
                                                <div class="fl entrust_form">
                                                    <label><input  name="trusteeshiptype" class="osclass" value="2" type="radio" checked>自定义积分账户</label>
<!--                                                    <label><input name="trusteeshiptype"  value="1" type="radio" disabled>SDK积分托管</label>-->
<!--                                                    <label><input name="trusteeshiptype" class="osclass" value="3" type="radio" disabled>服务器积分订单回调</label>-->
                                                    <span style="display: none;" class="fl alert alert-small alert-warning" id="trusteeshiptypeerr"><b class="icon_errer"></b><em id="trusteeshiptypemsg">请选择托管方式。</em></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="display: none">
                                            <span class="fl form-label">奖励返还提示语:</span>
                                            <span class="fl">
                                                <textarea name="rewardesc" id="rewardesc" class="fl form-control form-control-small" maxlength="500" placeholder="奖励返还提示语"></textarea>
                                            </span>
                                        </div>
                                        <div id="popupdiv" class="form-group" style="display: none;">
                                                <div class="form-group">
                                                    <span class="fl form-label"><i class="require_item">*</i>广告形态</span>
                                                    <span id="adstylech" class="fl radio_label">
                                                    	<label><input name="adstyle" id="adstyle" value="0" type="radio" checked>全屏</label>
                                                    	<label><input name="adstyle" id="adstyle" value="1" type="radio">自定义</label>
                                                    </span>
                                                </div>
                                                <div class="form-group" id="adstylediv" style="display:none;">
                                                    <div class="form-group">
                                                        <span class="fl form-label"><i class="require_item">*</i>图片尺寸</span>
                                                        <span class="fl radio_label">
                                                        	<label><input name="adstyleimgsize" id="adstyleimgsize" value="1" type="radio" checked> 3:2</label>
                                                        	<label><input name="adstyleimgsize" id="adstyleimgsize" value="2" type="radio"> 6:5</label>
                                                        </span>
                                                    </div>
                                                    <div class="form-group" >
                                                        <span class="fl form-label"><i class="require_item">*</i>插屏尺寸</span>
                                                        <span class="fl radio_label">
                                                        	<select name="adstylesize" id="adstylesize">
                                                            	<option value="90">90%</option>
                                                            	<option value="75">75%</option>
                                                            	<option value="60">60%</option>
                                                            	<option value="50">50%</option>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div class="form-group" style="display:none">
                                                        <span class="fl form-label"><i class="require_item">*</i>边框颜色</span>
                                                        <span id="bgcolordiv" class="fl radio_label">
                                                        	<select id="adstylecolor" name="adstylecolor" style="width: 100px;">
                                                            	<option style="background-color:#FFFFFF" val="#FFFFFF" value="">无</option>
                                                            	<option style="background-color:#FFF68F" val="#FFF68F" value="FFF68F"></option>
                                                            	<option style="background-color:#FF8C00" val="#FF8C00" value="FF8C00"></option>
                                                            	<option style="background-color:#FF82AB" val="#FF82AB" value="FF82AB"></option>
                                                            	<option style="background-color:#87CEFA" val="#87CEFA" value="87CEFA"></option>
                                                            	<option style="background-color:#5CACEE" val="#5CACEE" value="5CACEE"></option>
                                                            	<option style="background-color:#7FFFD4" val="#7FFFD4" value="7FFFD4"></option>
                                                            	<option style="background-color:#66CDAA" val="#66CDAA" value="66CDAA"></option>
                                                            	<option style="background-color:#458B74" val="#458B74" value="458B74"></option>
                                                            	<option style="background-color:#EEEEEE" val="#EEEEEE" value="EEEEEE"></option>
                                                            	<option style="background-color:#AEAEAE" val="#AEAEAE" value="AEAEAE"></option>
    	                                                       	<option style="background-color:#767676" val="#767676" value="767676"></option>
                                                            	<option style="background-color:#454545" val="#454545" value="454545"></option>
                                                        	</select>
                                                        </span>
                                                    </div>
                                                    
                                                    <div id="borderdiv" class="form-group">
                                                        <span class="fl form-label"><i class="require_item">*</i>边框设置</span>
                                                        <span class="fl radio_label">
                                                        	<label><input name="bordertype" id="bordertype" value="0" type="radio">无边框</label>
                                                        	<label><input name="bordertype" id="bordertype" value="1" type="radio" checked>系统边框</label>
                                                        	<label><input name="bordertype" id="bordertype" value="2" type="radio">自定义边框</label>
                                                        </span>
                                                    </div>
                                                    <div id="sysBorder" class="form-group">
						                                <span class="fl form-label"><i class="require_item">*</i>系统边框</span>
						                                <div class="subform">
						                                <span class="fl radio_label check_labels">
							                                <label><input name="borderpolicy" id="borderpolicy" value="1" type="radio" checked>使用单个边框</label>
							                                <?php $sysBorderGroupId=0;
							                                	if(!$sysBorderList){?>
							                                	<span class="fl alert alert-small alert-warning"><em>暂无系统边框</em></span><br/><br/>
							                                <?php }else foreach($sysBorderList as $v){
							                                	if($v['defaultborder']==1) $sysBorderGroupId=$v['bordergroupid'];
						                                	?>
							                                	<img class="border <?php if($v['defaultborder']==1) echo 'checkedborder';?>" id="<?php echo $v['bordergroupid']?>" src="http://res.cocounion.com<?php echo $v['path']?>" alt="cannot show"/>
							                                <?php }?>
							                                <input type="hidden" id="sysBorderGroupId" value="<?php echo $sysBorderGroupId?>">
	                                                        <label><input name="borderpolicy" id="borderpolicy" value="2" type="radio">所有边框随机</label>
	                                                        <span style="display: none;" class="fl alert alert-small alert-warning" id="sysBordererr"><i class="icon error_icon"></i><b id="sysBordermsg"></b></span>
                                                        </span>
                                                        </div>
						                            </div>
						                            
						                            <div id="cusBorder" class="form-group" style="display:none;">
						                            	<span class="fl form-label"><i class="require_item">*</i>上传边框</span>
						                            	<div style="margin:13px;">
							                            	<div class="form_input"><label class="cueinfo">图片格式支持JPG、PNG；文件大小不超过100KB。</label></div>
															<span class="form_info_cue_errer hidden" id="imgerr"><b class="icon_errer"></b><em></em></span>
															<div class="uploadBorder">
								                            	<div class="form_label"></div>
						    									<div class="borderImg">
						    										<input type="file" name='Filedata'  id="border1" class='uploadify' onchange='ajaxUploder(this,"600*500",12)'/>&nbsp;&nbsp; <br/>
						    										<div class="form_input"><label class="cueinfo">图片尺寸：600×500 图片要求：外部窗口600*500，内部窗口566*468</label>
						    										<span id="bordererr1" style="display: none; margin-left:10px;"><i class="icon error_icon"></i><span id="bordermsg1"></span></span>
						    										</div>
						    										<div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
						    									</div><br>
						    									
						    									
						    									<div class="form_label"></div>
						    									<div class="borderImg">
						    										<input type="file" name='Filedata'  id="border2" class='uploadify'  onchange='ajaxUploder(this,"500*600",13)'/>&nbsp;&nbsp; <br/>
						    										<div class="form_input"><label class="cueinfo">图片尺寸：500*600 图片要求：外部窗口500*600，内部窗口466*568</label>
						    										<span id="bordererr2" style="display: none; margin-left:10px;"><i class="icon error_icon"></i><span id="bordermsg2"></span></span>
						    										</div>
						    										<div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
						    									</div><br>
						    									
						    									
						    									<div class="form_label"></div>
						    									<div class="form_input">
						    										<input type="file" name='Filedata'  id="border3" class='uploadify'  onchange='ajaxUploder(this,"900*600",8)'/>&nbsp;&nbsp; <br/>
						    										<div class="form_input"><label class="cueinfo">图片尺寸：900×600 图片要求：外部窗口900*600，内部窗口866*568</label>
						    										<span id="bordererr3" style="display: none; margin-left:10px;"><i class="icon error_icon"></i><span id="bordermsg3"></span></span>
						    										</div>
						    										<div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
						    									</div><br>
						    									<em class="red" ><b class="icon_error" id='1692_default_imgs_error'></b></em>
						    									
						    									<div class="form_label"></div>
						    									<div class="form_input">
						    										<input type="file" name='Filedata'  id="border4" class='uploadify'  onchange='ajaxUploder(this,"600*900",9)'/>&nbsp;&nbsp; <br/>
						    										<div class="form_input"><label class="cueinfo">图片尺寸：600*900 图片要求：外部窗口600*900，内部窗口566*868</label>
						    										<span id="bordererr4" style="display: none; margin-left:10px;"><i class="icon error_icon"></i><span id="bordermsg4"></span></span>
						    										</div>
						    										<div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
						    									</div><br>
						    									<em class="red" ><b class="icon_error" id='1692_default_imgs_error'></b></em>
						    									
						    									<div class="form_label"></div>
						    									<div class="form_input">
						    										<input type="file" name='Filedata'  id="border5" class='uploadify'  onchange='ajaxUploder(this,"1280*720",10)'/>&nbsp;&nbsp; <br/>
						    										<div class="form_input"><label class="cueinfo">图片尺寸：1280*720 图片要求：外部窗口1280*720，内部窗口1246*688</label>
						    										<span id="bordererr5" style="display: none; margin-left:10px;"><i class="icon error_icon"></i><span id="bordermsg5"></span></span>
						    										</div>
						    										<div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
						    									</div><br>
						    									<em class="red" ><b class="icon_error" id='1692_default_imgs_error'></b></em>
						    									
						    									<div class="form_label"></div>
						    									<div class="form_input">
						    										<input type="file" name='Filedata'  id="border6" class='uploadify'  onchange='ajaxUploder(this,"720*1280",11)'/>&nbsp;&nbsp; <br/>
						    										<div class="form_input"><label class="cueinfo">图片尺寸：720*1280 图片要求：外部窗口720*1280，内部窗口686*1248</label>
						    										<span id="bordererr6" style="display: none; margin-left:10px;"><i class="icon error_icon"></i><span id="bordermsg6"></span></span>
						    										</div>
						    										<div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
						    									</div><br>
						    									<em class="red" ><b class="icon_error" id='1692_default_imgs_error'></b></em>
					    									</div>
							                            </div>
						                            </div>
						                           <div id="cusClose" class="form-group" style="display:none;">
						                            	<span class="fl form-label">上传关闭按钮</span>
						                            	<div style="margin:13px;">
							                            	<div class="form_input"><label class="cueinfo">图片格式支持JPG、PNG；文件大小不超过100KB。</label></div>
															<span class="form_info_cue_errer hidden" id="imgerr"><b class="icon_errer"></b><em></em></span>
															<div class="uploadBorder">
																<div class="form_label"></div>
						    									<div class="form_input">
						    										<input type="file" name='Filedata'  id="border7" class='uploadify'  onchange='ajaxUploder(this,"60*60",15)'/>&nbsp;&nbsp;<br/>
						    										<div class="form_input"><label class="cueinfo">图片尺寸：60*60</label>
						    										<span id="bordererr7" style="display: none; margin-left:10px;"><i class="icon error_icon"></i><span id="bordermsg7"></span></span>
						    										</div>
						    										<div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
						    									</div><br>
						    									<em class="red" ><b class="icon_error" id='1692_default_imgs_error'></b></em>
						    								</div>
					    								</div>
				    								</div>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                                <div class="new_position_btn_group">
                                    <a href="#"id="addBtnOk" class="btn btn-success btn-success-noboder">确&nbsp;&nbsp;&nbsp;&nbsp;认</a>
                                    <a href="#" id="addBtnCancel" class="close btn btn-default btn-default-noboder">取&nbsp;&nbsp;&nbsp;&nbsp;消</a>
                                </div>
                            </form>
                        </div>
                        <?php }?>
                        <div style="height : 50px;"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- 删除广告位 -->
<div id="deleteLightBox" style="display: none;">
    <div class="light_box_content">
        <p class="one_info">
            <?php
            if($auditstatus == 1)
            {
                $delstr = "<div style='text-align: center; font-size:14px;'>您的应用正在审核，删除广告位将影响当前应用的审核内容，是否确定删除广告位信息？</div>";
            }
            else
            {
                $delstr = "<div style='text-align: center; font-size:14px;'>是否确定删除该广告位？</div><br>已经发布的应用中该广告位仍会继续产生广告收入，删除后该广告位的数据仍会显示在统计中。";
            }
            echo $delstr;
            ?>
        </p>
    </div>
</div>

<!-- ] 主体部分 -->
<?php $this->load->view("manage/inc/footer");?>
<script>
var updateBorder = new Array();
    $(document).ready(function(){

    	$('.check_labels').on('click','.border',function(){
    		if($(this).attr("disabled")!="disabled"){
    			$('.check_labels').find('.border').removeClass('checkedborder');
	            $(this).toggleClass('checkedborder');
	            $('#sysBorderGroupId').val($(this).attr('id'));
	    		if($('input:radio[name="borderpolicy"]:checked').val() == 2)
	    		{
	    			$('.check_labels').find('.border').removeClass('checkedborder');
	    			$('#sysBorderGroupId').val($(this).attr('0'));
	    			$('#sysBorDelFlag').val("1");
	    		}
    		}
    		else{
        		alert("您选择的系统边框已经失效，请换一个试试。");
        	}
    	});
        $("#sysBorder :radio").click(function(){
    		if($('input:radio[name="borderpolicy"]:checked').val() == 2){
    			$('.check_labels .border').removeClass('checkedborder');
    			$('#sysBorderGroupId').val('0');
    			$('#sysBorDelFlag').val('1');
    		}
        });
    	$("#borderdiv :radio").click(function(){
    		if($('input:radio[name="bordertype"]:checked').val() == 1){
    			$('#sysBorder').show();
    		}else{
    			$('#sysBorder').hide();
    		}

    		if($('input:radio[name="bordertype"]:checked').val() == 2){
    			$('#cusBorder').show();
    			$('#cusClose').show();
    		}else{
    			$('#cusBorder').hide();
    			$('#cusClose').hide();
    		}
        });
        
        $("#editBtnOk").click(function(){

			var sysBorDelFlag=$('#sysBorDelFlag').val();
        	var bordertype = $('input:radio[name="bordertype"]:checked').val();
            var borderpolicy = $('input:radio[name="borderpolicy"]:checked').val();
            var sysBorderGroupId = $('#sysBorderGroupId').val();
            $("#positionnameerr").hide();
            $("#adformerr").hide();
            $("#positioniderr").hide();
            $("#trusteeshiptypeerr").hide();
            for(var i=1; i<=7; i++)
            {
            	$("#bordererr"+i).hide();
            }
        	if(bordertype == 1)
            {
            	if(borderpolicy == 0)
            	{
                	$("#sysBordermsg").html("请选择边框设置。");
                	$("#sysBordererr").show();
                	return false;
            	}
            	else if (borderpolicy == 1 && sysBorderGroupId == 0 && sysBorDelFlag==1)
            	{
            		$("#sysBordermsg").empty().html("请选择系统边框。");
                	$("#sysBordererr").show();
                	return false;
            	}
            	else if(borderpolicy == 1 && sysBorderGroupId == 0 && sysBorDelFlag==0){
            		$("#sysBordermsg").empty().html("您使用的系统边框已经失效，请换一个试试。");
                	$("#sysBordererr").show();
                	return false;
            	}
            }
            if(bordertype == 2)
            {
            	for(var i=1; i<=6; i++){
            		if($("#img_border"+i).val()==null){
            			$("#bordermsg"+i).html("请上传边框。");
                    	$("#bordererr"+i).show();
                    	return false;
                	}
            	}
            }
            
            <?php
                if($auditstatus == 1)
                {
                    $editstr = '是否确定修改广告位信息？\n修改广告位将影响当前应用的审核内容，是否确定修改广告位？';
                }
                else
                {
                    $editstr = '是否确定修改广告位信息？\n修改该广告位信息，该广告位已产生的数据将以修改后的广告位信息展示。';
                }
            ?>

            var editstr = "<?php echo $editstr; ?>";
            art.dialog({
                title:'<i class="icon icon_node"></i>提示',
                lock: true,
                fixed:true,
                ok:function(){
                    var positionid = $("#positionid").val();
                    var positionname = $("#positionname").val();
                    var positiondesc = $("#positiondesc").val();
                    var appid = $("#appid").val();
                    var trusteeshiptype = $('input:radio[name="trusteeshiptype"]:checked').val();
                    var rewardesc = $("#rewardesc").val();
                    var adstyle = $('input:radio[name="adstyle"]:checked').val();
                    var adstyleimgsize = $('input:radio[name="adstyleimgsize"]:checked').val();
                    var adstylesize = $("#adstylesize").val();
                    var adstylecolor = $("#adstylecolor").val();
                    var img_border1 = $("#img_border1").val()==null?"":$("#img_border1").val();
                    var img_border2 = $("#img_border2").val()==null?"":$("#img_border2").val();
                    var img_border3 = $("#img_border3").val()==null?"":$("#img_border3").val();
                    var img_border4 = $("#img_border4").val()==null?"":$("#img_border4").val();
                    var img_border5 = $("#img_border5").val()==null?"":$("#img_border5").val();
                    var img_border6 = $("#img_border6").val()==null?"":$("#img_border6").val();
                    var img_border7 = $("#img_border7").val()==null?"":$("#img_border7").val();
                    var oldBorderType = $('#oldBorderType').val();
                    var oldBorderGroupId = $('#oldBorderGroupId').val();
                    $.post("/manage/appposition/doedit",
                        {
                            positionid:positionid,
                            positionname:positionname,
                            positiondesc:positiondesc,
                            appid:appid,
                            trusteeshiptype:trusteeshiptype,
                            rewardesc:rewardesc,
                            adstyle:adstyle,
                            adstyleimgsize:adstyleimgsize,
                            adstylesize:adstylesize,
                            adstylecolor:adstylecolor,
                            bordertype:bordertype,
                            borderpolicy:borderpolicy,
                            img_border1:img_border1,
                            img_border2:img_border2,
                            img_border3:img_border3,
                            img_border4:img_border4,
                            img_border5:img_border5,
                            img_border6:img_border6,
                            img_border7:img_border7,
                            sysBorderGroupId:sysBorderGroupId,
                            oldBorderType:oldBorderType,
                            oldBorderGroupId:oldBorderGroupId,
                            updateBorderJson : JSON.stringify(updateBorder),
                            r:Math.random()
                        },
                        function(data)
                        {
                            if(data && data['status'] == 0){
                                if(data['data'] != '')
                                {
                                    $("#"+data['data'][0]).html(data['data'][1]);
                                }
                                $("#"+data['info']).show();
                                return false;
                            }else{
                                location.href = '/manage/appposition/showinfo/?appid='+appid;
                            }
                        },
                        'json');
                    return true;
                },
                cancel:true,
                background: '#fff', // 背景色
                opacity: 0.6,	// 透明度
                content: '<div style="padding:10px 25px; font-size:14px;">'+editstr+'</div>',
                id: 'editLightBox',
                okVal:'确认',
                cancelVal:'取消'
            });

            return false;
        });

        
        
        $("#addBtnOk").click(function(){
            var positionid = $("#positionid").val();
            var positionname = $("#positionname").val();
            var positiondesc = $("#positiondesc").val();
            var adform = $('input:radio[name="adform"]:checked').val();
            var appid = $("#appid").val();
            var trusteeshiptype = $('input:radio[name="trusteeshiptype"]:checked').val();
            var rewardesc = $("#rewardesc").val();
            var adstyle = $('input:radio[name="adstyle"]:checked').val();
            var adstyleimgsize = $('input:radio[name="adstyleimgsize"]:checked').val();
            var adstylesize = $("#adstylesize").val();
            var adstylecolor = $("#adstylecolor").val();
            var bordertype = $('input:radio[name="bordertype"]:checked').val();
            var borderpolicy = $('input:radio[name="borderpolicy"]:checked').val();
            var img_border1 = $("#img_border1").val()==null?"":$("#img_border1").val();
            var img_border2 = $("#img_border2").val()==null?"":$("#img_border2").val();
            var img_border3 = $("#img_border3").val()==null?"":$("#img_border3").val();
            var img_border4 = $("#img_border4").val()==null?"":$("#img_border4").val();
            var img_border5 = $("#img_border5").val()==null?"":$("#img_border5").val();
            var img_border6 = $("#img_border6").val()==null?"":$("#img_border6").val();
            var img_border7 = $("#img_border7").val()==null?"":$("#img_border7").val();
            var sysBorderGroupId = $('#sysBorderGroupId').val();
            $("#positionnameerr").hide();
            $("#adformerr").hide();
            $("#positioniderr").hide();
            $("#trusteeshiptypeerr").hide();
            for(var i=1; i<=7; i++)
            {
            	$("#bordererr"+i).hide();
            }
            $.post("/manage/appposition/doadd",
                {
                    positionid:positionid,
                    positionname:positionname,
                    positiondesc:positiondesc,
                    adform:adform,
                    appid:appid,
                    trusteeshiptype:trusteeshiptype,
                    rewardesc:rewardesc,
                    adstyle:adstyle,
                    adstyleimgsize:adstyleimgsize,
                    adstylesize:adstylesize,
                    adstylecolor:adstylecolor,bordertype:bordertype,
                    borderpolicy:borderpolicy,
                    img_border1:img_border1,
                    img_border2:img_border2,
                    img_border3:img_border3,
                    img_border4:img_border4,
                    img_border5:img_border5,
                    img_border6:img_border6,
                    img_border7:img_border7,
                    sysBorderGroupId:sysBorderGroupId,
                    r:Math.random()
                },
                function(data)
                {
                    if(data && data['status'] == 0){
                        if(data['data'] != '')
                        {
                            $("#"+data['data'][0]).html(data['data'][1]);
                        }
                        $("#"+data['info']).show();
                        return false;
                    }else{
                        location.href = '/manage/appposition/showinfo/?appid='+appid;
                    }
                },
                'json');
            return false;
        });
        $('#addBtnCancel').click(function(){
            $('#newPosition').hide();
            $('#addPosition').show();
        });
        $('#addPosition').click(function(){
            $('#newPosition').show();
            $('#addPosition').hide();
        });

	$("#bgcolordiv select").change(function(){
		var index = $(this).val();
		var bgcolor = $(this).find("option[value="+index+"]").attr("val");
		$(this).css("background-color",bgcolor);
	});

	$("#delCloseIcon").click(function(){
		var path=$("#delCloseIcon").nextAll('.border7_image_show_box').find('input').val();
		if("undefined".indexOf(path) != -1){
			$("#delCloseIcon").parent().find("em").empty().append("您未添加关闭按钮。");
			$("#delCloseIcon").parent().find("#1692_default_imgs_error").removeProp("style");
		}
		else{
			$("#delCloseIcon").nextAll('.border7_image_show_box').find('img').remove();
			$("#delCloseIcon").nextAll('.border7_image_show_box').find('a').remove();
			$("#delCloseIcon").nextAll('.border7_image_show_box').find('input').val("");
			updateBorder.push('undefined'+"*"+path);
		}
	});
    	
 });
// ready end
    function ajaxUploder(fileObj, imgtype) {
        	var obj 			= $(fileObj);
        	var fileId 			= obj.attr('id');
        	var image_show_box 	= obj.parents('div:eq(0)').find('.image_show_box');
        	var maindiv 		= obj.parents('div:eq(0)');
        	var borderId = obj.parents('div:eq(0)').find('#img_'+fileId).attr('name');
        	var oldBorderType = $('#oldBorderType').val();

        	var loading = obj.siblings('.onload').show();
        	var err_msg = obj.parents('div:eq(0)').siblings('span:eq(0)');

        	$.ajaxFileUpload( {
        				url : '/manage/appposition/ajaxDoUploadStuff/?type=' + imgtype,
        				secureuri : true,
        				fileElementId : fileId,
        				dataType : 'json',// 服务器返回的格式，可以是json
        				success : function(data, status) {
        					err_msg.hide();
        					var msgObj = data[0]
        					if (msgObj.isReceived == true) {
        						var str = '<div class="'+fileId+'_image_show_box">';
        						
        						str+='<img src="/public/upload'+msgObj.imgInfo['newImg']+'" class="border" /><a href="/public/upload'+msgObj.imgInfo['newImg']+'" target="_blank">查看实际效果</a>';	
        						str+='<input type="hidden" id="img_'+fileId+'" value="'+msgObj.imgInfo['newImg']+'"/>';
        		
        						str += '&nbsp;&nbsp;</div>';
        						if(oldBorderType == 2)
        						{
            						
            						updateBorder.push(borderId+"*"+msgObj.imgInfo['newImg']);
            					}
        						maindiv.find('.'+fileId+'_image_show_box').remove();
        						maindiv.append(str);
        						maindiv.find('.'+fileId+'_image_show_box').fadeIn("slow");

        					} else {
        						var msg = msgObj.errorMessage;
        						alert('    ' + msg);
        					}
        					loading.hide();
        					return true;
        				},
        				error : function(data, status, e) {
        					loading.hide();
        				}
        			})
        }
    
    function ConfirmDel(href) {
        var href = href;
        art.dialog({
            title:'<i class="icon icon_node"></i>提示',
            lock: true,
            fixed:true,
            ok:function(){
                location.href = href;
                return true;
            },
            cancel:true,
            background: '#fff', // 背景色
            opacity: 0.6,	// 透明度
            content: $('#deleteLightBox').html(),
            id: 'deleteLightBox',
            okVal:'确认',
            cancelVal:'取消'
        });
        return false;
    }
    
    $("#adformdiv :radio").click(function(){
    	var adform = $('input:radio[name="adform"]:checked').val();
    	$("#integrationdiv").hide();
    	$("#popupdiv").hide();
    	if(adform == 4)
    	{
    		$("#integrationdiv").show();
    		}
    	else if(adform == 2)
    	{
    		$("#popupdiv").show();
    		}
    	})

    $("#adstylech :radio").click(function(){
    	var adstyle = $('input:radio[name="adstyle"]:checked').val();
    	$("#adstylediv").hide();
    	if(adstyle == 1)
    	{
    		$("#adstylediv").show();
    		}
    	})
</script>


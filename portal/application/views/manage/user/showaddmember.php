<!--  主体部分 [ -->
<div class="inner_container" id="innerContainer">
    <?php $this->load->view('manage/user/left')?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <div class="jumbotron jumbotron_no_shadow edit_info">
                    <header class="title">
                        <h3><strong  class="square_box"></strong><span>身份验证</span></h3>
                    </header>
                    <form action="" id="creator" method="post" name="creator">
                        <div class="form-group">
                            <div class="fl form-label">注册邮箱：</div>
                            <div class="fl font"><?php echo $userinfo['username'];?></div>
                        </div>
                        <!-- 个人团体 -->
                        <div id="accounttype1" >
                            <div class="form-group">
                                <span class="fl form-label"><i class="require_item">*</i>证件类型：</span>
                                <span class="fl radio-group" id="certtype_wrap">
                                    <label class="radio_style"><i class="icon radio_icon active"></i><input style="display: none;" type="radio" checked="checked" name="certtype" value="1" >中国大陆身份证</label>
                                    <label class="radio_style"><i class="icon radio_icon "></i><input style="display: none;" type="radio" name="certtype" value="2" >中国港澳台身份证</label>
                                    <label class="radio_style"><i class="icon radio_icon "></i><input style="display: none;" type="radio" name="certtype" value="3" >海外证件</label>
                                </span>
                                <span id="certtypeerr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="certtypemsg"></span></span>
                            </div>
                            <div class="form-group">
                                <div class="fl form-label"><i class="require_item">*</i>证件姓名：</div>
                                <div class="fl">
                                    <input type="text" size="50" id="realname" class="form-control form-control-small" name="realname" placeholder="请如实填写，同时也是您银行账户的开户人" >
                                </div>
                                <span id="realnameerr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="realnamemsg"></span></span>
                            </div>
                            <div class="form-group">
                                <div class="fl form-label"><i class="require_item">*</i>证件号码：</div>
                                <div class="fl">
                                    <input type="text" size="50" id="idnumber" class="form-control form-control-small" name="idnumber" placeholder="保存后将不可修改" >
                                </div>
                                <span id="idnumbererr" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="idnumbermsg"></span></span>
                            </div>
                            <div class="form-group">
                                <div class="fl form-label"><i class="require_item">*</i>证件正面：</div>
                                <div class="fl file_box">
                                    <input type="file" name='Filedata'  id="uploadify10" class='uploadify fl'  onchange='ajaxUploder(this,100)'/>
                                    <div class="fl"><label class="cueinfo">图片格式必须为png或jpg，文件大小不超过2M。</label></div>
                                    <br style="clear:left">
                                    <div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
                                </div>
                                <span id="uploadify100err" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="uploadify100msg"></span></span>
                            </div>
                            <div class="form-group">
                                <div class="fl form-label"><i class="require_item">*</i>证件背面：</div>
                                <div class="fl file_box">
                                    <input type="file" name='Filedata'  id="uploadify20" class='uploadify fl'  onchange='ajaxUploder(this,200)'/>
                                    <div class="fl"><label class="cueinfo">图片格式必须为png或jpg，文件大小不超过2M。</label></div>
                                    <br style="clear:left">
                                    <div class="onload" style='text-align:left;display:none;'><b class="icon"></b>正在上传，请稍侯…</div>
                                </div>
                                <span id="uploadify200err" class="fl alert alert-small alert-warning " style="display:none;"><i class="icon error_icon"></i><span id="uploadify200msg"></span></span>
                            </div>
                            <div class="form-group">
                                <div class="fl form-label" style="height: 40px;"></div>
                                <div class="fl">
                                    <button class="btn btn-primary btn-primary-noboder" id="formsubmit" name="formsubmit" type="button">提交审核</button>
                                    <button class="btn btn-default btn-default-noboder" type="button" id="cancel" name="cancel" onclick="history.back();">取消</button>
                                </div>
                            </div>
                        </div>
                        <!-- 提交按钮 -->
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
<?php $this->load->view("manage/inc/footer");?>
<script type="text/javascript">

//单选按钮
$('#certtype_wrap .radio_style').bind('click',function(e){
    $('#certtype_wrap .radio_style').find('.icon').removeClass('active');
    $(this).find('.icon').addClass('active');
    $(this).find('input').attr('checked',true);
});
$(document).ready(function() {

	//初始化区域信息
	$("#formsubmit").click(function (e){
		var certtype = $('input:radio[name="certtype"]:checked').val();
		var realname = $("#realname").val()
		var idnumber = $("#idnumber").val()
		var uploadify100 = $("#uploadify100").val()
		var uploadify200 = $("#uploadify200").val()
		msgInit();
		$("#formsubmit").attr("disabled", "disabled");
		$.post("/manage/userinfo/doaddmember", 
				{
    			certtype : certtype,
    			realname : realname,
    			idnumber : idnumber,
    			uploadify100 : uploadify100,
    			uploadify200 : uploadify200,
	   			r:Math.random()
				}, 
				function(data)
				{
	    			if(data && data['status'] == 0){
	        			if(data['data'] != '')
	        			{
	        				$("#"+data['data'][0]).html(data['data'][1]);
	            		}
	    				$("#"+data['info']).show();
	    		        $("#formsubmit").removeAttr("disabled");
	    				return false;
	    			}else if(data && data['status'] == 1){
		    			alert('提交成功，请等待审核！');
	    				location.href = '/manage/userinfo/showaccountinfo';
	    			}
	    			else
	    			{
						alert('未知错误！');
		    		}
				},
				'json');
			return false;
			return;
		})
});

function msgInit()
{
	$("#certtypeerr").hide()
	$("#realnameerr").hide()
	$("#idnumbererr").hide()
	$("#uploadify100err").hide()
	$("#uploadify200err").hide()
}
function ajaxUploder(fileObj, tpl) {
	var obj 			= $(fileObj);
	var fileId 			= obj.attr('id');
	var image_show_box 	= obj.parents('div:eq(0)').find('.image_show_box');
	var maindiv 		= obj.parents('div:eq(0)');
	var loading = obj.siblings('.onload').show();
	$.ajaxFileUpload( {
				url : '/manage/indexreg/ajaxDoUpload/',
				secureuri : true,
				fileElementId : fileId,
				dataType : 'json',// 服务器返回的格式，可以是json
				success : function(data, status) {
        			$("#iconimgerr").hide();
					if(data['status'])
					{
						var str = '<div class="'+tpl+'image_show_box">';
						str+='<img src="/public/upload'+data['data']+'" style="width:300px;height:200px;" /><a href="/public/upload'+data['data']+'" target="_blank">查看实际效果</a>';
						str+='<input type="hidden" id="uploadify'+tpl+'" name="uploadify'+tpl+'" value="'+data['data']+'"/>';
						str += '&nbsp;&nbsp;</div>';
						maindiv.find('.'+tpl+'image_show_box').remove();// 将刚才上传的干掉
						maindiv.append(str);
						maindiv.find('.'+tpl+'_image_show_box').fadeIn("slow");
						}
					else
					{
						alert(data['data']);
						}
					loading.hide();
					return true;
				},
				error : function(data, status, e) {
					loading.hide();
				}
			})
}

</script>

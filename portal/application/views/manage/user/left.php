

<aside style="display:none">
    <div id="leftBar" class="left_bar">
        <div class="app_title">
            <ul class="first">
                <br>
                <li class="open <?php echo (strpos($_SERVER['PHP_SELF'],'userinfo/index') > 0 || strpos($_SERVER['PHP_SELF'],'showuserinfo') > 0)  ? 'active' : '';?>"><a class="a_status" href="/manage/userinfo/index"><i class="icon user_info_icon"></i>基本信息管理</a></li>
                <li class="open <?php echo strpos($_SERVER['PHP_SELF'],'showaccountinfo') > 0 ? 'active' : '';?>"><a class="a_status" href="/manage/userinfo/showaccountinfo"><i class="icon identify_icon"></i>身份验证</a></li>
                <li class="open <?php echo (strpos($_SERVER['PHP_SELF'],'showfinanceinfo') > 0 || strpos($_SERVER['PHP_SELF'],'showfinancedetail') > 0)? 'active' : '';?>"><a class="a_status" href="/manage/userinfo/showfinanceinfo"><i class="icon reciver_icon"></i>收款信息管理</a></li>
                <li class="open <?php echo strpos($_SERVER['PHP_SELF'],'showeditpwd') > 0 ? 'active' : '';?>"><a class="a_status" href="/manage/userinfo/showeditpwd"><i class="icon repair_pwd_icon"></i>修改密码</a></li>
            </ul>
        </div>
    </div>
</aside>
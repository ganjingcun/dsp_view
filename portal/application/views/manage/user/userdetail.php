<!--  主体部分 [ -->
<div class="inner_container" id="innerContainer">
    <?php $this->load->view('manage/user/left')?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <div class="jumbotron jumbotron_no_shadow">
                    <header class="title">
                        <h3><strong  class="square_box"></strong><span>基本信息管理</span></h3>
                    </header>
                    <div style="padding-left: 100px;" class="common_font">
                        <div class="form-group">
                            <span class="form-label fl">联系人姓名:</span>
                            <span class="fl font-con"><?php echo $contact;?></span>
                        </div>
                        <div class="form-group">
                            <span class="form-label fl">注册邮箱:</span>
                            <span class="fl font-con"><?php echo $userinfo['username']?></span>
                        </div>
                        <div class="form-group">
                            <span class="form-label fl">手机号码:</span>
                            <span class="fl font-con"><?php echo $mobile;?></span>
                        </div>
                        <div class="form-group">
                            <span class="form-label fl">联系地址:</span>
                            <span class="fl font-con"><?php echo $address;?></span>
                        </div>
                        <div class="form-group">
                            <span class="form-label fl">固定电话:</span>
                            <span class="fl font-con"><?php echo $telephone;?></span>
                        </div>
                        <div class="form-group">
                            <span class="form-label fl">QQ:</span>
                            <span class="fl font-con"><?php echo $qqnumber;?></span>
                        </div>
                        <div class="form-group">
                            <span class="form-label fl">密钥:</span>
                            <span class="fl font-con"><?php echo $secretKey;?></span>
                        </div>
                        <div class="form-group">
                            <span class="form-label fl" style="height: 40px;"></span>
                            <span class="fl font-con">
<!--                            	<a href="/manage/userinfo/showuserinfo" class="btn btn-primary btn-primary-noboder"><i class="icon icon_edit_white_con"></i>编 辑</a>-->
							<?php
				        		if($auditstatus == 1)
				        		{ ?>
				        			<a href="/manage/userinfo/showuserinfo" class="btn btn-primary btn-primary-noboder"><i class="icon icon_edit_white_con"></i>编 辑</a>
				        		<?php
				        		}
				        		else{
				        		?>
				        			<a href="/manage/userinfo/showuserinfo" class="btn btn-primary btn-primary-noboder" disabled="disabled"><i class="icon icon_edit_white_con"></i>编 辑</a>
				        		<?php }?>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php $this->load->view("manage/inc/footer");?>

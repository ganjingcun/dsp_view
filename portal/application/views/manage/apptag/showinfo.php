<!--  主体部分 [ -->
<div class="inner_container" id="innerContainer">
    <?php $this->load->view('manage/inc/left')?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <?php $this->load->view('manage/inc/righttop')?>
                <div class="inner_tab">
                    <ul class="tab_item">
                        <li><a href="/manage/appchannel/showinfo/?appid=<?php echo $appid?>">渠道管理</a></li>
                        <li><a href="/manage/appposition/showinfo/?appid=<?php echo $appid?>">广告位管理</a></li>
                        <li class="active"><a href="javascript:;">标签管理</a></li>
                        <li><a href="/manage/appinfo/showinfo/?appid=<?php echo $appid?>">基本信息</a></li>
                         <?php if($isintegral == 1){  ?><li ><a href="/manage/appinfo/backconfig/?appid=<?php echo $appid?>">积分回调方式</a></li><?php } ?>
                    </ul>
                    <div class="jumbotron jumbotron_no_shadow tab_box label_mananer">
                        <div class="jumbotron jumbotron_no_shadow">
                            <label class="fl selected_labels_label">已选标签:</label>
                            <div class="fl selected_labels">
                                <?php
                                if(!empty($apptaglist))
                                {
                                    foreach ($apptaglist as $value) {
                                        echo '<span data-v="'.$value['tagid'].'" class="item">'.$value['tagname'].'<em class="close">x</em></span>';
                                    }
                                }
                                else
                                {
                                    echo '<em style="color:red; font-style: normal;">暂未选择。</em>';
                                }
                                ?>
                            </div>
                        </div>
                        <div class="label_mananer_box jumbotron_no_shadow">
                            <h3 class="fixed_title">新增标签</h3>
                            <div class="add_labels_box">
                                <div class="inner">
                                    <div class="add_labels_fonts"><span><b>没有您需要的标签? </b>欢迎来帮助我们完善标签库!</span></div>
                                    <div class="add_label_fixed">
                                        <input type="text"  id="tagname" name="tagname" class="form-control form-control-small add_label">
                                        <input type="hidden" id="appid" name="appid" value="<?php echo $appid?>">
                                        <a href="javascript:;" id="addtag" class="btn btn-success btn-success-noboder">添加</a>
                                        <span id="tagnameerr" class="alert alert-small alert-warning" style="display: none"><i class="icon error_icon"></i><span id="tagnamemsg">请填写标签名称。</span></span>
                                        <span id="tagnamesess" class="alert alert-small alert-success" style="display: none"><span id="tagnamemsg">添加成功，请等待审核。</span></span>
                                    </div>
                                </div>
                            </div>
                            <?php
                            if(!empty($alltaglist))
                            {
                                echo '<div id="newAddLabels" class="check_labels">';
                                foreach ($alltaglist as $k => $v)
                                {?>
                                    <span class="track" data-tagid="<?php echo $k?>"><i class="icon icon_track"></i><?php echo $v['tagname']?></span>
                                <?php
                                }
                                echo '</div>';
                            }
                            else
                            {
                            }
                            ?>
                            <span id="tagiderr" class="alert alert-small alert-warning in_block" style="display: none"><i class="icon error_icon"></i><span id="tagidmsg">请选择标签。</span></span>
                        </div>
                        <div class="btn-group">
                            <a href="javascript;;" id="addapptag" class="btn btn-primary btn-primary-noboder">保存</a>
                            <a href="/manage/apptag/showinfo/?appid=<?php echo $appid; ?>" id="canceladdapptag" class="btn btn-default btn-default-noboder">取消</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<?php $this->load->view("manage/inc/footer");?>
<script>

    $(document).ready(function(){
        $('#newAddLabels').on('click','.track',function(){
            $(this).toggleClass('checked');
        })

        $("#addapptag").click(function(){
            var appid = $("#appid").val();
            var tagid = '';
            $("#newAddLabels .track").each(function(){
                if($(this).hasClass("checked") == true)
                {
                    tagid+=$(this).data('tagid')+",";
                }
            })
            if(tagid=='')
            {
                $("#tagiderr").show();
            }
            $("#tagiderr").hide();

            $.post("/manage/apptag/doAddAppTag",
                {
                    appid:appid,
                    tagid:tagid,
                    r:Math.random()
                },
                function(data)
                {
                    if(data && data['status'] == 0){
                        if(data['data'] != '')
                        {
                            $("#"+data['data'][0]).html(data['data'][1]);
                        }
                        $("#"+data['info']).show();
                        return false;
                    }else{
                        location.href = '/manage/apptag/showinfo/?appid='+appid;
                    }
                },
                'json');
            return false;
        });

        $('#tagname').focus(function(){
            $('#tagnameerr').hide();
            $('#tagnamesess').hide();
        });
        $('#addtag').bind('click',function(){
            var tagname = $('#tagname').val();
            var appid = $("#appid").val();

            $.post("/manage/apptag/doadd",
                {
                    tagname:tagname,
                    appid:appid,
                    r:Math.random()
                },
                function(data)
                {
                    if(data && data['status'] == 0){
                        if(data['data'] != '')
                        {
                            $("#"+data['data'][0]).html(data['data'][1]);
                        }
                        $("#"+data['info']).show();
                        return false;
                    }else{
                        $('#tagnamesess').show();
                    }
                },
                'json');
            return false;
        })

        $(document).on('click','.item .close',function(){
            var $this=$(this)
            var tagid = $(this).parent().attr("data-v");
            var appid = $("#appid").val();
            if( tagid == undefined || tagid == '')
            {
                return false;
            }
            art.dialog({
                title:'<i class="icon icon_node"></i>提示',
                lock: true,
                fixed:true,
                ok:function(){
                    $.post("/manage/apptag/dodelapptag",
                        {
                            tagid:tagid,
                            appid:appid,
                            r:Math.random()
                        },
                        function(data)
                        {
                            if(data && data['status'] == 0){
                                if(data['data'] != '')
                                {
                                    $("#"+data['data'][0]).html(data['data'][1]);
                                }
                                $("#"+data['info']).show();
                                return false;
                            }else{
                                $this.parent().remove();
                                location.reload();
                            }
                        },
                        'json'
                    );
                },
                cancel:true,
                background: '#fff', // 背景色
                opacity: 0.6,	// 透明度
                content:'<div style="text-align: center;">确定要删除此条标签信息吗？</div>',
                id: 'deleteSelectedTag',
                okVal:'确认',
                cancelVal:'取消'
            });

        });


    });

</script>
<!--  主体部分 [ -->
<div class="inner_container" id="innerContainer">
    <?php $this->load->view('manage/income/left')?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <div class="jumbotron jumbotron_no_shadow">
                    <header class="title">
                        <h3><strong class="square_box"></strong><span>结算详情</span></h3>
                    </header>
                    <br>
                    <table class="table table-borderd table-bordered-rlborder">
                        <thead>
                            <tr>
                                <th>月份</th>
                                <th>应用id</th>
                                <th>应用名称</th>
                                <th>计费形式</th>
                                <th>月均单价</th>
                                <th>点击量</th>
                                <th>收入</th>
                                <th>校准收入</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if(is_array($income_list)){ foreach ($income_list as $key=>$val) { ?>
                            <tr class="ac">
                                <td><?php echo $val['pkMonth'];?></td>
                                <td><?php echo $val['pkCanalId'];?></td>
                                <td><?php echo $val['appname'];?></td>
                                <td><?php echo $val['acctypename'];?></td>
                                <td><?php echo formatmoney($val['avgincome']);?></td>
                                <td><?php echo $val['uidClick'];?></td>
                                <td><?php echo formatmoney($val['income']);?></td>
                                <td><?php echo formatmoney($val['cincome']);?></td>
                            </tr>
                        <?php } } ?>
                        </tbody>
                    </table>
                    <br>

                    <?php if($accounttype == '1'){ ?>
                        <header class="title">
                            <h3><strong class="square_box"></strong><span>团队成员分成详情</span></h3>
                        </header>
                        <br>
                        <table class="table table-borderd table-bordered-rlborder">
                            <thead>
                                <tr>
                                    <th>成员名称</th>
                                    <th>税前收入</th>
                                    <th>税负</th>
                                    <th>税后收入</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if(!empty($userratio)){ foreach ($userratio as $key=>$val) { ?>
                                <tr class="ac">
                                    <td><?php echo $val->realname;?></td>
                                    <td><?php echo formatmoney($val->cincome);?></td>
                                    <td><?php echo formatmoney($val->taxmoney);?></td>
                                    <td><?php echo formatmoney($val->aftertaxincome);?></td>
                                </tr>
                            <?php } } ?>
                            </tbody>
                        </table><br><br>
                    <?php }  ?>
                </div>
            </div>
        </div>
    </section>
</div>
<?php $this->load->view("manage/inc/footer");?>

<script>
$(function(){
	setTimeout(function(){
		$("#side_menu .menu_item .hd a").trigger('click');
	},1000);
})
</script>

<!--  主体部分 [ -->
<div class="inner_container" id="innerContainer">
    <?php $this->load->view('manage/income/left')?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <div class="jumbotron jumbotron_no_shadow">
                    <header class="title">
                        <h3><strong class="square_box"></strong><span>收入总览</span></h3>
                    </header>
                    <br>
                    <table class="table table-borderd table-bordered-rlborder">
                        <tr>
                            <td>
                                <p class="lineH40">您当前账户已结算到 <?php echo $last_month;?> <span class="buff_blue"><?php echo $userinfo['accounttypename']; ?></span></p>
                                <p class="lineH40">可提现余额(税前) <span class=""><?php if(empty($income_list)){ echo '<span style="color:#ffce54;">结算中...</span>';}else{ echo '￥';echo '<span class="cash">';echo $sum_cincome?formatmoney($sum_cincome):'0.00';echo "</span>元";}?> </span>
                                    <?php if($userinfo['accounttype'] !=1 && $today > 10 && $today <= 15) { ?>
                                        <?php if($sum_cincome == 0){ ?>
                                            <a class="btn btn-default btn-default-noboder" style="color: #fff; position: relative; top:-5px; cursor: not-allowed;"  href="javascript:void(0)"  title="前往提现"><i class="icon icon_crash"></i>前往提现</a>
                                        <?php }else{ ?>
                                            <a class="btn btn-success btn-success-noboder" style="color: #fff; position: relative; top:-5px;"  href="/manage/income/withdraw" target="_blank" title="前往提现"><i class="icon icon_crash"></i>前往提现</a>
                                        <?php } ?>
                                    <?php } ?>
                                </p>
                                <?php if($userinfo['accounttype'] ==1){ ?>
                                    <p class="red lineH40">* 厦门雅基软件有限公司将代扣税务(查看税法)，并于25日-30日期间完成支付，该期间可提现余额将清零。</p>
                                <?php }else{ ?>
                                    <p class="red lineH40">*请于20日前邮寄发票及提款订单，厦门雅基软件有限公司将于25日-30日期间进行支付</p>
                                <?php } ?>
                            </td>
                            <td>
                                <p class="lineH40" style="color:#999">每个自然月1日-10日为上月账户结算审核日</p>
                                <p class="lineH40">上月结算税前收入<strong class="fr ora"><?php if(empty($income_list)){ echo '结算中';}else{ echo '￥'; echo $income_list['cincome']?formatmoney($income_list['cincome']):'0.00';}?></strong></p>
                                <p class="lineH40">上上月结算税前收入<strong class="fr"><span style="font-family: 'arial'">&yen;</span> <?php echo $income_list2['cincome']?formatmoney($income_list2['cincome']):'0.00';?>元</strong></p>
                            </td>
                        </tr>
                    </table>
                    <br>

                    <!-- 上月结算详情 -->
                    <header class="title">
                        <h3><strong class="square_box"></strong><span>上月结算详情</span></h3>
                    </header>
                    <br>
                    <table class="table table-borderd table-bordered-rlborder">
                        <thead>
                        <tr>
                            <th>应用名称</th>
                            <th>计费形式</th>
                            <th>月均单价</th>
                            <th>点击量</th>
                            <th>收入</th>
                            <th>校准收入</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(!empty($income_detail)){ foreach ($income_detail as $key=>$val) { ?>
                            <tr class="ac">
                                <td><?php echo $val['appname'];?></td>
                                <td><?php echo $val['acctypename'];?></td>
                                <td><?php echo formatmoney($val['avgincome']);?></td>
                                <td><?php echo $val['uidClick'];?></td>
                                <td><?php echo formatmoney($val['income']);?></td>
                                <td><?php echo formatmoney($val['cincome']);?></td>
                            </tr>
                        <?php } }else{ ?>
                            <tr class="ac"><td colspan="6">未结算</td></tr>
                        <?php } ?>
                        </tbody>
                    </table><br>


                    <?php if($userinfo['accounttype'] == '1'){ ?>
                    <!-- 团队成员分成详情 -->
                    <header class="title">
                        <h3><strong class="square_box"></strong><span>团队成员分成详情</span></h3>
                    </header>
                    <br>
                    <table class="table table-borderd table-bordered-rlborder">
                        <thead>
                        <tr>
                            <th>成员名称</th>
                            <th>税前收入</th>
                            <th>税负</th>
                            <th>税后收入</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(!empty($userratio)){ foreach ($userratio as $key=>$val) { ?>
                            <tr class="ac">
                                <td><?php echo $val->realname;?></td>
                                <td><?php echo formatmoney($val->cincome);?></td>
                                <td><?php echo formatmoney($val->taxmoney);?></td>
                                <td><?php echo formatmoney($val->aftertaxincome);?></td>
                            </tr>
                        <?php } } ?>
                        </tbody>
                    </table>
                    <?php } ?>

                    <div style="line-height:24px;">
                        请企业用户将提款单与发票一起邮寄至厦门雅基软件有限公司<span style="color: #3089d9; "> (邮寄地址:厦门市思明区观音山商务区7号楼1302室, 361000)</span>.<br>
                        厦门雅基软件有限公司统一打款期为每月25日～31日。如果此期间厦门雅基软件有限公司未收到提款单, 打款程序将顺延至下个自然月。
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<?php $this->load->view("manage/inc/footer");?>
<script>
//问号 icon 弹出提示框
$(".icon_help").hover(
	function(e){
		var xy = $(this).offset();
		console.log(xy);
		$("#popu_cuebox .con").html($(this).attr("data-title"));
		$("#popu_cuebox").css({
			"top":xy.top,
			"left":xy.left
		});
		$("#popu_cuebox").show();
	},function(){
			$("#popu_cuebox").hide();
});

$("#popu_cuebox").hover(function(){
		$(this).show();
	},function(){
		$(this).hide();
})



$(function(){
	setTimeout(function(){
		$("#side_menu .menu_item .hd a").trigger('click');
	},1000);
})

</script>
 
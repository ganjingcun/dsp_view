<aside style="display:none">
    <div id="leftBar" class="left_bar">
        <div class="app_title">
            <ul class="first">
                <br>
                <li class="open <?php echo (strpos($_SERVER['PHP_SELF'],'income/index') > 0 || strpos($_SERVER['PHP_SELF'],'showuserinfo') > 0 || strpos($_SERVER['PHP_SELF'],'withdraw') > 0)  ? 'active' : '';?>"><a class="a_status" href="/manage/income/index"><i class="icon income_icon"></i>收入总览</a></li>
                <li class="open <?php echo strpos($_SERVER['PHP_SELF'],'income_list') > 0 || strpos($_SERVER['PHP_SELF'],'income_detail') > 0 ? 'active' : '';?>"><a class="a_status" href="/manage/income/income_list"><i class="icon settle_accounts_icon"></i>结算记录</a></li>
                <li class="open <?php echo (strpos($_SERVER['PHP_SELF'],'paid_list') > 0 || strpos($_SERVER['PHP_SELF'],'paid_detail') > 0)? 'active' : '';?>"><a class="a_status" href="/manage/income/paid_list"><i class="icon deposit_icon"></i>提现记录</a></li>
            </ul>
        </div>
    </div>
</aside>
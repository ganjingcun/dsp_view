<!--  主体部分 [ -->
<div class="inner_container" id="innerContainer">
    <?php $this->load->view('manage/income/left')?>
    <section>
        <div id="content" class="content">
            <div class="right_content" id="rightContent">
                <!-- 内容部分 -->
                <div class="jumbotron jumbotron_no_shadow" >
                    <header class="title">
                        <h3><strong class="square_box"></strong><span>结算记录</span></h3>
                    </header>
                    <br>
                    <form name='form1' action="/manage/income/income_list">
                        <div class="form-group">

                            <div class="fr">
                                <label class="form-label" style="font-size: 14px; font-weight:normal; color:#999;">按照年份筛选:</label>
                                <select  name="search_year" id="search_year"  style="height:40px;">
                                    <?php foreach($years as $key=>$val){ ?>
                                        <option <?php if($search_year == $val){ ?> selected="selected" <?php } ?>><?php echo $val;?></option>
                                    <?php } ?>
                                </select>
                                <input class="btn btn-primary btn-primary-noboder" type="submit" name="sub" value="搜索">
                            </div>
                            <div class="fl">
                                <a class="btn btn-primary btn-primary-noboder" type="button"  href="/manage/appreport/gatherreport"><i class="icon look_table_icon"></i>查看报表</a>
                            </div>
                        </div>
                    </form>



                    <table class="table table-borderd table-bordered-rlborder">
                        <thead>
                            <tr>
                                <th>月份</th>
                                <th>结算时间</th>
                                <th>税前收入(元)</th>
                                <?php if ($accounttype == '1'){ ?>
                                    <th>税负</th>
                                    <th>税后收入</th>
                                <?php } ?>
                                <th>查看详细</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if(is_array($income_list)){ foreach ($income_list as $key=>$val) { ?>
                            <tr class="ac">
                                <td><?php echo $val['month'];?></td>
                                <td><?php echo date("Y-m-d H:i",$val['addtime']);?></td>
                                <td><span style="font-family: 'arial'">&yen;</span> <?php echo formatmoney($val['cincome']);?></td>
                                <?php if ($accounttype == '1'){ ?>
                                    <td><span style="font-family: 'arial'">&yen;</span> <?php echo formatmoney($val['taxmoney']);?></td>
                                    <td><span style="font-family: 'arial'">&yen;</span> <?php echo formatmoney($val['aftertaxmoney']);?></td>
                                <?php } ?>
                                <td><a href="/manage/income/income_detail?incomeid=<?php echo $val['id'];?>">查看</a></td>
                            </tr>
                        <?php }} ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </section>
</div>
<?php $this->load->view("manage/inc/footer");?>
	
	<div id="popu_cuebox" class="popu_cuebox">
		<div class="con"></div>
	</div>
	
<script>
//问号 icon 弹出提示框
$(".icon_help").hover(
	function(e){
		var xy = $(this).offset();
		console.log(xy);
		$("#popu_cuebox .con").html($(this).attr("data-title"));
		$("#popu_cuebox").css({
			"top":xy.top,
			"left":xy.left
		});
		$("#popu_cuebox").show();
	},function(){
			$("#popu_cuebox").hide();
});

$("#popu_cuebox").hover(function(){
		$(this).show();
	},function(){
		$(this).hide();
})



$(function(){
	setTimeout(function(){
		$("#side_menu .menu_item .hd a").trigger('click');
	},1000);
})

</script>
 
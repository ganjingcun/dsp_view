<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>打印提款单</title>
<link rel="stylesheet" href="/public/css/global_3.0.css?022815" media="all" />
<link rel="stylesheet" href="/public/css/help.css?0522" media="all" />
<link rel="stylesheet" href="/public/css/from_related.css?022815" media="all" />
<!-- ie6,7下png透明 -->
<!--[if IE 6]>
	<script type="text/javascript" src="../js/DD_belatedPNG.js" ></script>	
	<script type="text/javascript">
	DD_belatedPNG.fix('.bg_png');
	</script>
<![endif]-->
</head>
<style>
        .main_content{border:none;}
        .tikuandan{margin:0;width:960px;}
</style>        
<body>
	<!-- 头部 -->
	 
	<!-- 主体内容 -->
	<div class="warp">
		<div class="main_content">
			<!-- 左侧内容 -->
			 
			<!-- 内容部分 -->
			<div class="content">
					<div class="inner_con">
 						<div class="db10"></div>
						 
						<div class="db10"></div>
						<!-- 提款单 -->
						<div class="tikuandan">
							<br/>
							<h1 class="main_title ac">上海拓畅信息技术有限公司提款单</h1>
							<br/>
							<br/>
							<!--  -->
							<div class="set_budget">
								<table width="100%" cellspacing="0" cellpadding="0" border="0" style="width:800px;">
									<colgroup>
										<col width="100">
										<col width="100">
										<col width="200">
										<col width="150">
										<col width="100">
									</colgroup>
									<tbody>
										<tr>
											<th>单号</th>
											<th>项目</th>
											<th>账号</th>
											<th>提款时间</th>
											<th>提款金额</th>
										</tr>
										<tr class="ac">
											<td><?php echo $paid_info['incomekey'];?></td>
											<td>流量收入</td>
											<td><?php echo $paid_info['username'];?></td>
											<td><?php echo date("Y-m-d H:i",$paid_info['addtime']);?></td>
											<td><span style="font-family: 'arial'">&yen;</span> <?php echo formatmoney($paid_info['aftertaxmoney']);?></td>
										</tr>
									</tbody>
								</table>
							</div>
							<br/>
							<!--  -->
							<h1 class="main_title">金额明细</h1>
							<hr class="dline"/>
							<div class="set_budget">
								<table width="100%" cellspacing="0" cellpadding="0" border="0" style="width:800px;">
									<colgroup width="100">
										<col width="100">
										<col width="100">
										<col width="100">
										<col width="100">
										<col width="100">
									</colgroup>
									<tbody>
										<tr>
											<th>月份</th>
											<th>税前收入(元)</th>
                                                                                        <?php if ($accounttype == '1'){ ?>
                                                                                                <th> 税负</th>
                                                                                                <th>税后金额(元) </th>
                                                                                       <?php } ?>
 										</tr>
                                                                                <?php foreach($income_info as $key=>$val){ ?>
										<tr class="ac">
 											<td><?php echo $val['month'];?></td>
											<td><span style="font-family: 'arial'">&yen;</span> <?php echo formatmoney($val['cincome']);?></td>
                                        <?php if ($accounttype == '1'){ ?>
                                                <td><span style="font-family: 'arial'">&yen;</span> <?php echo formatmoney($val['taxmoney']);?></td>
                                                <td><span style="font-family: 'arial'">&yen;</span> <?php echo formatmoney($val['aftertaxmoney']);?></td>
                                        <?php } ?>
										</tr>
                                        <?php } ?>
									</tbody>
								</table>
							</div>

							<!--  -->
							<br/>
							<hr class="dline"/>
							<h1 class="main_title">收款账户</h1>
							<div class="form_item">
								 
                                                                <?php if ($accounttype == '1'){ ?>
                                                                         <div class="form_label">真实姓名:</div>
                                                                        <div class="form_input"><?php echo $paid_info['realname'];?></div>
                                                                <?php }elseif($accounttype == '2') { ?>
                                                                        <div class="form_label">公司名称:</div>
                                                                        <div class="form_input"><?php echo $paid_info['companyname'];?></div>
                                                                <?php }elseif($accounttype == '3') { ?>
                                                                        <div class="form_label">个体工商户名称:</div>
                                                                        <div class="form_input"><?php echo $paid_info['individualname'];?></div>
                                                                <?php } ?>
							</div> 
							<div class="form_item">
								<div class="form_label">收款人名:</div>
								<div class="form_input"><?php echo $paid_info['bankusername'];?></div>
							</div>
							<div class="form_item">
								<div class="form_label">开户银行:</div>
								<div class="form_input"><?php echo $paid_info['bank'];?></div>
							</div>
							<div class="form_item">
								<div class="form_label">银行账号:</div>
								<div class="form_input"><?php echo $paid_info['bankaccount'];?></div>
							</div>
                                                        <div class="ar"><span style="margin-right:200px;">盖章：</span></div>
							<br/>
							<br/>
							 
							<br/>
							<br/>
							<br/>
						</div>
						<br/>
						 
					</div>


			</div>
			
		</div>


	</div>
 
	
<script>
//问号 icon 弹出提示框
$(".icon_help").hover(
	function(e){
		var xy = $(this).offset();
		console.log(xy);
		$("#popu_cuebox .con").html($(this).attr("data-title"));
		$("#popu_cuebox").css({
			"top":xy.top,
			"left":xy.left
		});
		$("#popu_cuebox").show();
	},function(){
			$("#popu_cuebox").hide();
});

$("#popu_cuebox").hover(function(){
		$(this).show();
	},function(){
		$(this).hide();
})



$(function(){
	setTimeout(function(){
		$("#side_menu .menu_item .hd a").trigger('click');
	},1000);
})

</script>
</body>
</html>
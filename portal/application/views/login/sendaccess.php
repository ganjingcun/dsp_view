<?php 
require_once "header.php";
?>
    <!--  主体部分 [ -->
    <div class="container">
        <div class="user_relative">
            <div  class="send_email_success">
                <h3><i class="icon send_email_success_icon"></i>已发送邮件至您的邮箱！</h3>
                <div class="details">
                    我们已经向您的邮箱<a href="http://<?php echo 'mail.'.substr($email, stripos($email, '@')+1, strlen($email)-stripos($email, '@'));?>" class="btn-link"><?php echo $email;?></a>发送了密码重置邮件，请点击邮件中链接进行密码重置！
                </div>
                <div>
                    <a href="http://<?php echo 'mail.'.substr($email, stripos($email, '@')+1, strlen($email)-stripos($email, '@'));?>" target="_blank" class="long-btn-nb long-btn-nb-primary">立即进入邮箱</a>
                </div>
            </div>
        </div>
    </div>
    <!-- ] 主体部分 -->
<?php 
require_once "footer.html";
?>

<?php 
require_once "header.php";
?>
<?php if(!empty($msg)){?>
    <!--  主体部分 [ -->
    <div class="container">
        <div class="user_relative">
            <div  class="send_email_success">
                <div class="details">
                    <?php echo $msg;?>
                </div>
                <div>
                    <a href="/webindex/forgotpassword" class="long-btn-nb long-btn-nb-primary">重新发送邮件。</a>
                </div>
            </div>
        </div>
    </div>
    <!-- ] 主体部分 -->
<?php 
}
else 
{?>

    <!--  主体部分 [ -->
    <div class="container">
        <div class="user_relative">
            <h2>重置密码</h2>
            <div class="box">

                <div class="form_box jumbotron" style="margin:0 auto;">
                    <form action="webindex/doResetPwd" method="post">
                        <div class="form-group">
            		        <input type='hidden' id='username' name='username' value='<?php echo $username;?>' />
            		        <input type='hidden' id='key' name='key' value='<?php echo $key;?>' />
                            <h3>输入新密码</h3>
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="password" id="password" name="password" placeholder="请输入新密码">
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="password" id="repassword" name="repassword" placeholder="确认新密码">

                            <span class="alert alert-small alert-warning" style="display: none; position: relative; top: 20px; margin-bottom: 10px" id="repassworderr"><i class="icon  error_icon"></i><span id="repasswordmsg"></span></span>
                        </div>
						<div class="form-group">
							<span class="alert alert-small alert-warning" style="display: none; position: relative; top: 5px;" id="msgerr"><i class="icon  error_icon"></i><span id="msgmsg"></span></span>
						</div>
                        <br>
                        <div class="form-group">
                            <input type="button" id="resetPassword" value="重置密码" class="long-btn-nb long-btn-nb-primary">
                        </div>
                        <br>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- ] 主体部分 -->
<?php }?>
<script type="text/javascript">

$(function(){

	$('#resetPassword').click(function(e){
		var username = $('#username').val();
		var key = $("#key").val();
		var password = $("#password").val();
		var repassword = $("#repassword").val();
	    $("#msgerr").hide();
        $("#repassworderr").hide();
		$.post('/webindex/doResetPwd', 
			{ 
				username:username,
				key:key,
				password:password,
				repassword:repassword,
    			r:Math.random()
			}, 
			function(data)
			{
    			if(data && data['status'] == 0){
        			if(data['data'] != '')
        			{
        				$("#"+data['data'][0]).html(data['data'][1]);
            		}
    				$("#"+data['info']).show();
    				return false;
    			}else{
    				setTimeout(function(){
                        splash({
                            str:'重置成功！<span class="setSec">5</span>秒后将跳转到首页',
                            url:'/',
                            setSec:5
                        });
                    },300);
    			}
    			return false;
			},
			'json');
	});
})

</script>
<?php 
require_once "footer.html";
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>CocosAds</title>
    <link href="/public/version1.0/css/punch.css" rel="stylesheet" media="all">
    <script type="text/javascript" src="/public/version1.0/js/html5.js"></script>
    <script type="text/javascript" src="/public/version1.0/js/jquery.js"></script>
</head>

<body>
<!-- header [ -->
<header id="register_top">
    <section>
        <div class="inner_top">
            <ul>
                <li><a href="http://old.punchbox.org">前往旧平台</a></li>
                <li><a href="/public/version1.0/about.html">关于我们</a> </li>
                <li><a href="/public/version1.0/contact.html">联系我们</a> </li>
                <li><a href="/public/version1.0/customQuestion.html">帮助中心</a> </li>

            </ul>
        </div>
        <div class="inner">
            <a href="/" class="fl logo">logo</a>
            <ul class="fr register_nav">
                <li style="color: white; font-size: 14px;">已有账号？&nbsp;&nbsp;&nbsp;&nbsp;</li>
                <li><a href="/" class="btn btn-warning">登 录</a></li>
            </ul>
        </div>
    </section>
</header>
<!-- ] header -->

<!--  主体部分 [ -->
<div class="container">
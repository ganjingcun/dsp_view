<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 网站登陆注册入口
 *
 * @author wd
 *
 */
class WebIndex extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->helper("tools");
        $this->load->helper("email");
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');
        $this->load->helper('cookie');
        $this->load->model('/user/NoticeBoardClass');
        $this->load->model('manage/AppInfoClass');
        $this->load->model('manage/AppPositionClass');
        $this->load->model('user/AuthClass');
        $this->load->model('report/NewAppReportClass');
        header("Content-type: text/html; charset=utf-8");
    }

    /**
     * 首页.
     *
     * @author wd
     */
    public function index()
    {
        $data['client_id'] = 2;
        $data['redirect_url'] = 'http://'.$_SERVER['HTTP_HOST'].'/webindex/oauthlogin/';
        $data['sign'] = cocoSignData($data);
        $data['redirect_url'] = urlencode($data['redirect_url']);
        $data['username'] = $this->session->userdata('username');
        $data['autologin'] = get_cookie('autologin');
        $data['autousername'] = get_cookie('autousername');
        $data['autopwd'] = get_cookie('autopwd');
        $noticeboard=$this->NoticeBoardClass->getAvailableNotices();
        $data['noticeboard']=$noticeboard;
        $this->load->view('indexnew', $data);
    }

    /**
     * 首页.
     *
     * @author wd
     */
    public function indexold()
    {
        $data['client_id'] = 2;
        $data['redirect_url'] = 'http://'.$_SERVER['HTTP_HOST'].'/webindex/oauthlogin/';
        $data['sign'] = cocoSignData($data);
        $data['redirect_url'] = urlencode($data['redirect_url']);
        $data['username'] = $this->session->userdata('username');
        $data['autologin'] = get_cookie('autologin');
        $data['autousername'] = get_cookie('autousername');
        $data['autopwd'] = get_cookie('autopwd');
        $noticeboard=$this->NoticeBoardClass->getAvailableNotices();
        $data['noticeboard']=$noticeboard;
        $this->load->view('index', $data);
    }

    public function ssoStValidate()
    {
        $post_data = array();
        $post_data['st'] = $this->input->get('st');
        $post_data['client_id'] = $this->config->item('sso_client_id');
        $result = curlPost('https://passport.cocos.com/sso/st_validate', $post_data);
        $validinfo = json_decode($result, true);

        if($validinfo['status'] != 'success')
        {
            echo $result;
            return ;
        }

        // var_dump($validinfo);
        // return;

        $error = array('status' => false);

        $data['username'] = $validinfo['username'];
        $data['password'] = $validinfo['username'].$validinfo['uid'];
        $data['email'] = $validinfo['email'];
        $data['ip'] = $this->input->ip_address();

        $this->load->model("user/UserInfoClass");
        $error = $this->UserInfoClass->oauthLogin($data);
        if($error['status'] == 2)
        {
            $this->UserInfoClass->oauthRegister($data);
            $error = $this->UserInfoClass->oauthLogin($data);
            if($error['status'])
            {
                echo 'result = ' . json_encode($error);
                return;
            }
        }
        elseif($error['status'] == 4) {
            echo '<script>alert("用户名不能为空,请设置一个用户名然后重新登录.");location.href="https://open.cocos.com/user/user_edit?type=1"</script>';
            return;
        }
        elseif($error['status'])
        {
            echo 'result = ' . json_encode($error);
            return;
        }

        // $applistdata = array('user_id'=>$userInfo['user_id'],'from'=>'ads');
        // $sign = cocoSignData($applistdata);
        // $getAppListUrl = "{$this->config->item('openurl')}/api/user_apps/ads/{$userInfo['user_id']}/{$sign}";
        // $resu = curlGet($getAppListUrl);
        // $appList = json_decode($resu, true);
        // //{$this->config->item('openurl')}/api/app_info/ads/651404131/sign
        // //      $arrinfo = array('app_id'=>651404131,'from'=>'ads');
        // //
        // //      $sign = cocoSignData($arrinfo);
        // //      $geturl = "{$this->config->item('openurl')}/api/app_info/ads/651404131/{$sign}";
        // //      $resu = curlGet($geturl);
        // //      echo $resu;
        // $ccappids = null;
        // foreach($appList as $v)
        // {
        //     $ccappids[] = $v['id'];
        // }
        // $this->load->model('manage/AppInfoClass');
        // $chkappids = $this->AppInfoClass->getCocoAppList($ccappids, $this->session->userdata('userid'));
        // if(empty($chkappids))
        // {
        //     $appids = array();
        // }
        // else
        // {
        //     foreach($chkappids as $v)
        //     {
        //         $appids[] = $v['appid'];
        //     }
        // }
        // $result = array_diff($ccappids, $appids);
        // foreach ($appList as $k=>$v)
        // {
        //     if(array_search($v['id'], $result) === FALSE)
        //     {
        //         unset($appList[$k]);
        //     }
        // }
        // $this->AppInfoClass->ccAdd($appList,  $this->session->userdata('userid'));
        if ($this->input->get('mode') == 'json') {
            $result = array();
            $result['status'] = 'success';
            $result['username'] = $validinfo['username'];
            $result['commonreport'] = $this->NewAppReportClass->getDayAppIncomeReport2($this->session->userdata('userid'));
            echo 'result = ' . json_encode($result);
        } else {
            redirect('/manage/appinfo/index');            
        }
    }

    /**
     * 登陆页面.
     *
     * @author wd
     */
    public function cancelAutoLogin()
    {
        set_cookie('autologin', 0, -1);
        set_cookie('autopwd', '', -1);
        set_cookie('autousername', '', -1);
        ajaxReturn(1, 1, 1);
    }

    /**
     * 取消记住密码.
     *
     * @author wd
     */
    public function showLogin()
    {
        $data['client_id'] = 2;
        $data['redirect_url'] = 'http://'.$_SERVER['HTTP_HOST'].'/webindex/oauthlogin/';
        $data['sign'] = cocoSignData($data);
        $data['redirect_url'] = urlencode($data['redirect_url']);
        $data['header_title'] = '用户登录';
        $this->load->view('login/showlogin', $data);
    }

    /**
     * 登陆页面.
     *
     * @author wd
     */
    public function oauthLogin()
    {
        $authcode = $this->input->get('authcode');
        $getTokenJson = curlGet("{$this->config->item('openurl')}/login_oauth/gettoken?client_id=2&auth_code={$authcode}");
        $getToken = json_decode($getTokenJson, true);
        if(!isset($getToken['accesstoken']) || empty($getToken['accesstoken']))
        {
            echo "登录有误，请刷新重试！";
            return ;
        }
        $getInfoJson = curlGet("{$this->config->item('openurl')}/login_oauth/getdata?accesstoken={$getToken['accesstoken']}");
        $info = json_decode($getInfoJson, true);
        if(!empty($info['error']))
        {
            echo '登录有误，请<a href="javascript:void();" onclick="history.back();">返回</a>重试！';
            return ;
        }


        $userInfo = $info['data'];
        $error = array('status' => false);
        $data['username'] = $userInfo['user_name'];
        $data['password'] = $userInfo['user_name'].$userInfo['user_id'];
        $data['email'] = $userInfo['email'];
        $data['ip'] = $this->input->ip_address();
        $this->load->model("user/UserInfoClass");
        $error = $this->UserInfoClass->oauthLogin($data);
        if($error['status'] == 2)
        {
            $this->UserInfoClass->oauthRegister($data);
            $error = $this->UserInfoClass->oauthLogin($data);
            if($error['status'])
            {
                echo $error['usernamemsg'];
                return;
            }
        }
        elseif($error['status'])
        {
            echo $error['usernamemsg'];
            return;
        }

        $applistdata = array('user_id'=>$userInfo['user_id'],'from'=>'ads');
        $sign = cocoSignData($applistdata);
        $getAppListUrl = "{$this->config->item('openurl')}/api/user_apps/ads/{$userInfo['user_id']}/{$sign}";
        $resu = curlGet($getAppListUrl);
        $appList = json_decode($resu, true);
        //{$this->config->item('openurl')}/api/app_info/ads/651404131/sign
        //		$arrinfo = array('app_id'=>651404131,'from'=>'ads');
        //
        //		$sign = cocoSignData($arrinfo);
        //		$geturl = "{$this->config->item('openurl')}/api/app_info/ads/651404131/{$sign}";
        //		$resu = curlGet($geturl);
        //		echo $resu;
        $ccappids = null;
        foreach($appList as $v)
        {
            $ccappids[] = $v['id'];
        }
        $this->load->model('manage/AppInfoClass');
        $chkappids = $this->AppInfoClass->getCocoAppList($ccappids, $this->session->userdata('userid'));
        if(empty($chkappids))
        {
            $appids = array();
        }
        else
        {
            foreach($chkappids as $v)
            {
                $appids[] = $v['appid'];
            }
        }
        $result = array_diff($ccappids, $appids);
        foreach ($appList as $k=>$v)
        {
            if(array_search($v['id'], $result) === FALSE)
            {
                unset($appList[$k]);
            }
        }
        $this->AppInfoClass->ccAdd($appList,  $this->session->userdata('userid'));
        redirect('/manage/appinfo/showlist');
    }

    /**
     * 登陆.
     *
     * @author wd
     */
    public function doLogin()
    {
        $data['header_title'] = '用户登录';
        $error = array('status' => false);
        $data['username'] = spost('username');
        $data['password'] = spost('password');
        $data['autologin'] = spost('autologin');
        $data['ip'] = $this->input->ip_address();
        $data['captcha'] = spost('captcha');
        $data['validatecode'] = $this->session->userdata('validate_code');
        $this->load->model("user/UserInfoClass");
        $error = $this->UserInfoClass->doLogin($data);
        $data = array_merge($data, $error);
        ajaxReturn($data['info'], $data['status'], $data['data']);
    }
    
   

    /**
     * 修改密码.
     *
     * @author wd
     */
    public function ajaxforgotpwd()
    {
        $email = trim($this->input->post('email'));
        $captcha = trim($this->input->post('captcha'));
        $this->load->helper("email");
        if(empty($email))
        {
            $error['status'] = 0;
            $error['data'] = array('emailmsg', '邮箱不能为空！');
            $error['info'] = 'emailerr';
            ajaxReturn($error['info'], $error['status'], $error['data']);
        }
        if(!valid_email($email))
        {
            $error['status'] = 0;
            $error['data'] = array('emailmsg', '请输入正确的邮箱地址！');
            $error['info'] = 'emailerr';
            ajaxReturn($error['info'], $error['status'], $error['data']);
        }
        $data['validatecode'] = $this->session->userdata('validate_code');
        if(strtoupper($captcha) != $data['validatecode'])
        {
            $error['status'] = 0;
            $error['data'] = array('captchamsg', '验证码错误！');
            $error['info'] = 'captchaerr';
            ajaxReturn($error['info'], $error['status'], $error['data']);

        }
        $this->load->model("user/UserInfoClass");
        $userInfo = $this->UserInfoClass->getUserInfo($email);
        if(empty($userInfo))
        {
            $error['status'] = 0;
            $error['data'] = array('emailmsg', '该用户不存在!');
            $error['info'] = 'emailerr';
            ajaxReturn($error['info'], $error['status'], $error['data']);
        }
        if($userInfo['userstatus'] != 1)
        {
            $error['status'] = 0;
            $error['data'] = array('emailmsg', '该用户没有激活或已被锁定，请先激活或联系客服!');
            $error['info'] = 'emailerr';
            ajaxReturn($error['info'], $error['status'], $error['data']);
        }
        //发重置邮件
        $this->UserInfoClass->sendMail($userInfo['userid'], $userInfo['username'], $userInfo['password'], 3);
        ajaxReturn('', 1, '');
    }

    /**
     * 注册页面.
     *
     * @author wd
     */
    public function showRegister()
    {
        $data['header_title'] = '新用户注册';
        $this->load->view('login/showregister', $data);
    }

    /**
     * 忘记密码页面.
     *
     * @author wd
     */
    public function forgotpassword()
    {
        $data['header_title'] = '找回密码';
        $this->load->view('login/forgotpassword');
    }

    /**
     * 忘记密码邮件发送成功页面.
     *
     * @author wd
     */
    public function sendaccess()
    {
        $data['header_title'] = '找回密码';
        $email = $this->input->get('email');
        if(!valid_email($email))
        {
            $error['status'] = true;
            $error['usernamemsg'] = '请输入正确的邮箱地址！';
            echo json_encode($error);
            return ;
        }
        $data['email'] =htmlspecialchars($email);
        $this->load->view('login/sendaccess', $data);
    }

    /**
     * 重置密码
     *
     * @access public
     * @param $key
     * @return page
     */
    function showresetpwd($userId='',$key=''){

        $msg = '';
        if(empty($userId)||empty($key))
        {
            $msg = "验证信息错误，请打开正确的验证链接！";
        }
        $this->load->model("user/UserInfoClass");
        $userInfo = $this->UserInfoClass->getRow($userId);
        if($userInfo==false)
        {
            $msg = "验证信息错误，请打开正确的验证链接！";
        }

        if($userInfo['userstatus']!=1)
        {
            $msg = "此用户未激活或被锁定！";
        }

        //加密类
        $this->load->library('Security');

        if($key != Security::cryptPassword($userInfo['password'],$userInfo['username']))
        {
            $msg = "验证信息错误，请打开正确的验证链接！";
        }
        $data['key'] = Security::cryptPassword($userInfo['password'],$userInfo['userid']);
        $data['userid'] = $userInfo['userid'];
        $data['username'] = $userInfo['username'];
        $data['header_title'] = '密码重置';
        $data['msg'] = $msg;
        $this->load->view('login/resetpwd', $data);
    }



    /**
     * 更新用户密码
     *
     * @access public
     * @return page
     */
    function doResetPwd()
    {
        $error = array();
        $username 	= trim($this->input->post('username'));
        $key 		= $this->input->post('key');
        $password 	= $this->input->post('password');
        $repassword = $this->input->post('repassword');
        $this->load->helper('email');
        if(empty($username) || empty($key))
        {
            $error['status'] = 0;
            $error['data'] = array('msgmsg', '验证信息错误，请打开正确的验证链接！');
            $error['info'] = 'msgerr';
            ajaxReturn($error['info'], $error['status'], $error['data']);
        }
        if(!valid_email($username))
        {
            $error['status'] = 0;
            $error['data'] = array('msgmsg', '验证信息错误，请打开正确的验证链接！');
            $error['info'] = 'msgerr';
            ajaxReturn($error['info'], $error['status'], $error['data']);
        }

        if(empty($password))
        {
            $error['status'] = 0;
            $error['data'] = array('repasswordmsg', '密码不能为空！');
            $error['info'] = 'repassworderr';
            ajaxReturn($error['info'], $error['status'], $error['data']);
        }

        if($password != $repassword)
        {
            $error['status'] = 0;
            $error['data'] = array('repasswordmsg', '两次输入的密码不一致！');
            $error['info'] = 'repassworderr';
            ajaxReturn($error['info'], $error['status'], $error['data']);
        }

        $this->load->model("user/UserInfoClass");
        $userInfo = $this->UserInfoClass->getUserInfo($username);
        if(empty($userInfo))
        {
            $error['data'] = array('msgmsg', '验证信息错误，请打开正确的验证链接！');
            $error['info'] = 'msgerr';
            $error['status'] = 0;
            ajaxReturn($error['info'], $error['status'], $error['data']);
        }
        $this->load->library('Security');
        if(!$key==Security::cryptPassword($userInfo['password'], $userInfo['userid']))
        {
            $error['status'] = 0;
            $error['data'] = array('msgmsg', '验证信息错误，请打开正确的验证链接！');
            $error['info'] = 'msgerr';
            ajaxReturn($error['info'], $error['status'], $error['data']);
        }
        $status = $this->UserInfoClass->resetUserPassword($password, $userInfo);
        if($status['status'] != 1)
        {
            ajaxReturn($status['info'], $status['status'], $status['data']);
        }
        ajaxReturn('', $status['status'], '');
    }

    /**
     * 注册.
     *
     * @author wd
     */
    public function doRegister()
    {
        $data['header_title'] = '新用户注册';
        $error = array('status' => false);
        $data['username'] = spost('username');
        $data['password'] = spost('password');
        $data['passconf'] = spost('passconf');
        $data['ip'] = $this->input->ip_address();
        $data['captcha'] = spost('captcha');
        $data['validatecode'] = $this->session->userdata('validate_code');
        $this->load->model("user/UserInfoClass");
        $error = $this->UserInfoClass->doRegister($data);
        $data['username'] = htmlspecialchars($data['username']);
        $data = array_merge($data, $error);
        if($data['status'])
        {
            $this->load->view('login/doregister', $data);
            return;
        }
        redirect('/webindex/showaccess?type=0&email='.$data['username']);
    }

    /**
     * 注册、激活页面
     */
    public function showaccess()
    {
        $type = $this->input->get('type');
        $email = $this->input->get('email');
        $title = $this->input->get('title');
        $this->load->view('login/showaccess', array('type'=>$type,'email'=>$email, 'title'=>$title, 'header_title'=>'欢迎加入CocosAds!'));
    }

    /**
     * 退出
     *
     * @access public
     * @return page
     */
    function logOut(){
        $this->session->sess_destroy();
        set_cookie('auto','0');
        //导航搜索条件清除
        set_cookie('navName', '', time()-7200);
        set_cookie('navAppName', '', time()-7200);
        // redirect('webindex');

        $client_id = $this->config->item('sso_client_id');
        $sso_signout_url = 'https://passport.cocos.com/sso/signout?client_id=' . $client_id . '&url=' . $this->config->item('base_url');
        redirect($sso_signout_url);
        return;
    }

    /**
     * 退出
     *
     * @access public
     * @return page
     */
    function oauthLogOut(){
        $this->session->sess_destroy();
        set_cookie('auto','0');
        //导航搜索条件清除
        set_cookie('navName', '', time()-7200);
        set_cookie('navAppName', '', time()-7200);
        $callback = $this->input->get("callback");
        $callback = urlencode($callback);
        $str = $callback."(".json_encode(array('status'=>'success','error'=>0)).")";
        echo $str;
    }

    /**
     * 激活
     *
     * @access public
     * @return page
     */
    function activate($userId, $key){
        if(empty($userId)||empty($key))
        {
            redirect('/webindex/showaccess?type=9&title=激活链接错误!');
            return false;
        }
        $this->load->model("user/UserInfoClass");
        $userInfo = $this->UserInfoClass->getRow($userId);
        if($userInfo==false)
        {
            redirect('/webindex/showaccess?type=9&title=激活链接错误!');
            return false;
        }

        if($userInfo['userstatus']!=0)
        {
            redirect('/webindex/showaccess?type=9&title=账号已经激活或被锁定!');
            return false;
        }

        //验证用户是否过期
        if(($userInfo['regtime']+60*24*3600)<mktime())
        {
            redirect('/webindex/showaccess?type=9&title=激活链接已过期!');
            return false;
        }

        //加密类
        $this->load->library('Security');

        if($key != Security::cryptPassword($userInfo['password'],$userInfo['username']))
        {
            redirect('/webindex/showaccess?type=9&title=激活链接错误!');
            return false;
        }
        $userInfo = $this->UserInfoClass->activate($userId);
        redirect('/webindex/showaccess?type=5');
        return false;
    }
    
    public function appReportAPI(){
		$getData=$this->input->get();
		$reData = array('status' => 1, 'data' => array(), 'error'=>'');
    	if(!isset($getData['username']) || empty($getData['username'])){
    		$reData['status'] = 0;
    		$reData['error'] = urlencode('缺少用户名信息');
    		exit(urldecode(json_encode($reData)));
    	}
    	if(!isset($getData['key']) || empty($getData['key'])){
    		$reData['status'] = 0;
    		$reData['error'] = urlencode('缺少密钥');
    		exit(urldecode(json_encode($reData)));
    	}
        $data['username'] = $getData['username'];
        $data['key'] = $getData['key'];
        
        $this->load->model("user/UserInfoClass");
        $error = $this->UserInfoClass->reportAPIAuthentication($data);
        
        if(!$error['status']){
    		$reData['status'] = 0;
    		$reData['error'] = urlencode($error['data']);
    		exit(urldecode(json_encode($reData)));
        }
        $sdate = isset($getData['sdate'])?$getData['sdate']:'';
        $edate = isset($getData['edate'])?$getData['edate']:'';
        $date = $sdate.'~'.$edate;
        require_once 'manage/detailreport.php';
        $this->userInfo = $this->UserInfoClass->getUserInfo($data['username']);
        $this->userid =  $this->userInfo['userid'];
        $report = DetailReport::generateAppDetail($date);
  		foreach($report as $appid=>$appData){
  			foreach($appData as $adform=>$adformData){
  				$report[$appid][$adform]['appname'] = urlencode($report[$appid][$adform]['appname']);
  				$report[$appid][$adform]['adform'] = urlencode($report[$appid][$adform]['adform']);
  			}
  		}
        $reData['data'] = $report;
		exit(urldecode(json_encode($reData)));
    }
    
    public function addSecKeyToMember(){
    	$this->load->model('user/UserInfoClass');
    	$this->UserInfoClass->generateSecKeyToUsers();
    	print_r("完成");
    }
    
    public function setdefaultborder(){
    	$time_start = $this->microtime_float();
    	$query = $this->db->get_where('ad_border', array('defaultborder'=>1, 'status'=>1));
    	$rst = $query->result_array();
    	if($rst){
    		$bordergroupid = $rst[0]['bordergroupid'];
    		echo 'default bordergroupid: '.$bordergroupid.'<br/>';
    		if($bordergroupid){
				$set['bordertype'] = 1;
				$set['borderpolicy'] = 1;
				$set['bordergroupid'] = $bordergroupid;
				$where['adform'] = 2;
				$where['adstyle'] = 1;
				$where['bordertype'] = 0;
				$this->db->where($where);
				$rows = $this->db->count_all_results('app_position');
				echo '无边框广告位共计： '.$rows.'<br/>';
				$this->db->update('app_position', $set, $where);
        		$upNum = $this->db->affected_rows();
        		echo '修改广告位共计: '.$upNum.'<br/><br/>';
        		
        		$set['adstyle'] = 1;
        		$wherescd['adform'] = 2;
        		$wherescd['bordertype'] = 0;
        		$wherescd['positionname'] = '默认广告位';
				$this->db->where($wherescd);
				$rowsscd = $this->db->count_all_results('app_position');
				echo '无边框默认广告位共计： '.$rowsscd.'<br/>';
				$this->db->update('app_position', $set, $wherescd);
        		$upNumscd = $this->db->affected_rows();
        		echo '修改默认广告位共计: '.$upNumscd.'<br/><br/>';
    		}
    	}
    	$time_end = $this->microtime_float();
    	$time = $time_end - $time_start;
        echo "<br>共计使用时间：$time <br>";
    }
    
	function microtime_float()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }
    

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
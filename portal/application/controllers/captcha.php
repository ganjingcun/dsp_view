<?php
class Captcha extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('cookie');
	}

	function index($width,$height,$code_num)
	{		
		$this->cache();
		$this->load->library('validate_code',array('width'=>$width,'height'=>$height,'code_num'=>$code_num));
		$this->session->set_userdata(array('validate_code'=>strtoupper($this->validate_code->get_validate_code())));
		$this->validate_code->out_img();
	}

	private function cache()
	{
		$this->output->cache(10);
	}
}
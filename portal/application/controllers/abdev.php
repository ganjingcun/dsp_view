<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 网站登陆注册入口
 *
 * @author wd
 *
 */
class AbDev extends CI_Controller {

    private $secret = "046da2af2bb263b4c82b83b9fbb80533";
    function __construct()
    {
        parent::__construct();
        $this->load->helper("tools");
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');
        $this->load->helper('cookie');
        header("Content-type: text/html; charset=utf-8");
    }

    /**
     * 首页.
     *
     * @author wd
     */
    public function index()
    {
        $this->load->model('manage/SdkVersionLogClass');
        $data['iossdkinfo'] = $this->SdkVersionLogClass->getRow(1);
        $data['androidsdkinfo'] = $this->SdkVersionLogClass->getRow(2);
        $this->load->view('abdev', $data);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
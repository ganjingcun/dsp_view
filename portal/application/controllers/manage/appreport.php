<?php
/**
 * 应用管理类
 * @filename	: appinfo.php
 * @author		: niejianhui
 * @datetime	: 2013-10-21
 * @Description  : 前台广告主收入理页面。
 */

class AppReport extends MY_Controller{
    private  $userid = null;
    public function __construct()
    {
        parent::__construct();
        header("Cache-control:no-cache,no-store,must-revalidate");
        header("Pragma:no-cache");
        header("Expires:0");
        $this->load->model('manage/AuthClass');
        $this->load->model('manage/AppInfoClass');
        $this->load->model('report/NewAppReportClass');
        $this->load->model('manage/AppChannelClass');
        $this->load->model('manage/AppPositionClass');
        $this->load->helper('tools');
        $this->userid =  AuthClass::getUserId();
        $param = $this->input->get();
       	if(!isset($param['type']) || $param['type'] != 'exportreport'){
        	$this->loadHeader('统计分析', 3, 31);
       	}
    }

    /**
     * 收入概况
     * Enter description here ...
     */
    function gatherReport(){
        $data = $this->getCondition();
        $data = array_merge_recursive($data,$this->formatTop());

        $this->load->model('user/userinfoclass');
        $user=$this->userinfoclass->getRow($this->userid);

//        $data['graphreport'] = $this->getAllGraphReport($data['applist'], $data['sdate'], $data['edate']);
//        $data['tablereport'] = $this->getAllTableReport($data['applist'], $data['sdate'], $data['edate']);
        $data['graphreport'] = $this->NewAppReportClass->getAllGraphReport2($user['userid'], $data['sdate'], $data['edate']);
        $data['tablereport'] =  $this->NewAppReportClass->getAllTableReport2($user['userid'], $data['sdate'], $data['edate']);

        $data['totalmoney']=(int)$user['totalmoney'];
        $data['sumincome'] = $this->getSumIncomeReport($data['graphreport']);
        $data['datelist'] = mktimes($data['sdate'], $data['edate'], true, true);
        $this->load->view('manage/appreport/gatherreport', $data);
    }
    
    function exportReport(){
        $data = $this->getCondition();
        $data = array_merge_recursive($data,$this->formatTop());
        $data['graphreport'] = $this->getAllGraphReport($data['applist'], $data['sdate'], $data['edate']);
        $tablereport = $this->getAllTableReport($data['applist'], $data['sdate'], $data['edate']);
        $sumincome = $this->getSumIncomeReport($data['graphreport']);
        
        $reportdata = array();
        
        $reportdata[] = array(
        	'systype'=>'IOS',
        	'banner'=>isset($tablereport['iOS'][2][1])?formatmoney($tablereport['iOS'][2][1]):'0.00',
        	'popup'=>isset($tablereport['iOS'][2][2])?formatmoney($tablereport['iOS'][2][2]):'0.00',
        	'feeds'=>isset($tablereport['iOS'][2][20])?formatmoney($tablereport['iOS'][2][20]):'0.00',
        	'banner11'=>isset($tablereport['iOS'][1][1])?formatmoney($tablereport['iOS'][1][1]):'0.00',
        	'popup12'=>isset($tablereport['iOS'][1][2])?formatmoney($tablereport['iOS'][1][2]):'0.00',
        	'wall'=>isset($tablereport['iOS'][1][3])?formatmoney($tablereport['iOS'][1][3]):'0.00',
        	'feeds120'=>isset($tablereport['iOS'][1][20])?formatmoney($tablereport['iOS'][1][20]):'0.00',
        	'wall36'=>isset($tablereport['iOS'][3][6])?formatmoney($tablereport['iOS'][3][6]):'0.00'
        	);
        $reportdata[] = array(
        	'systype'=>'Android',
        	'banner'=>isset($tablereport['Android'][2][1])?formatmoney($tablereport['Android'][2][1]):'0.00',
        	'popup'=>isset($tablereport['Android'][2][2])?formatmoney($tablereport['Android'][2][2]):'0.00',
        	'feeds'=>isset($tablereport['Android'][2][20])?formatmoney($tablereport['Android'][2][20]):'0.00',
        	'banner11'=>isset($tablereport['Android'][1][1])?formatmoney($tablereport['Android'][1][1]):'0.00',
        	'popup12'=>isset($tablereport['Android'][1][2])?formatmoney($tablereport['Android'][1][2]):'0.00',
        	'wall'=>isset($tablereport['Android'][1][3])?formatmoney($tablereport['Android'][1][3]):'0.00',
        	'feeds120'=>isset($tablereport['Android'][1][20])?formatmoney($tablereport['Android'][1][20]):'0.00',
        	'wall36'=>isset($tablereport['Android'][3][6])?formatmoney($tablereport['Android'][3][6]):'0.00'
        	);
        $reportdata[] = array('systype'=>'总收入','banner'=>formatmoney($sumincome),'popup'=>'','feeds'=>'','banner11'=>'','popup12'=>'','wall'=>'','feeds120'=>'','wall36'=>'');
        
		$filename=urlencode('总体概况统计').'_'.date('Y-m-dHis');
	    header("Content-Disposition:attachment;filename={$filename}.csv");
	    $table = iconv("UTF-8","GBK","系统,,eCPM收入,,,eCPC收入,,,eCPA收入").PHP_EOL;
	    $table .= iconv("UTF-8","GBK","系统类型,banner广告,插屏广告,信息流广告,banner广告,插屏广告,推荐墙广告,信息流广告,积分墙广告").PHP_EOL;
	    
	    if(!empty($reportdata)){
			foreach($reportdata as $k=>$v){
				$table.=iconv("UTF-8","GBK",$v['systype'].",".$v['banner'].",".$v['popup'].",".$v['feeds'].",".$v['banner11'].",".$v['popup12'].",".$v['wall'].",".$v['feeds120'].",".$v['wall36']).PHP_EOL;
			}
	    }
		echo $table;
    }

    /**
     * 平台类型分布
     * Enter description here ...
     */
    function gatherDeviceReport(){
        $data = $this->getCondition();
        $data = array_merge_recursive($data,$this->formatTop());
        $data['graphreport'] = $this->getAllDeviceGraphReport($data['applist'], $data['sdate'], $data['edate']);
        $data['tablereport'] = $this->getAllDeviceTableReport($data['applist'], $data['sdate'], $data['edate']);
        $data['sumincome'] = $this->getDeviceIncomeReport($data['graphreport']);
        $data['datelist'] = mktimes($data['sdate'], $data['edate'], true, true);
        $this->load->model('user/userinfoclass');
        $user=$this->userinfoclass->getRow($this->userid);
        $data['totalmoney']=(int)$user['totalmoney'];
        $this->load->view('manage/appreport/gatherdevicereport', $data);
    }

    function exportDeviceReport(){
        $data = $this->getCondition();
        $data = array_merge_recursive($data,$this->formatTop());
        $data['graphreport'] = $this->getAllDeviceGraphReport($data['applist'], $data['sdate'], $data['edate']);
        $tablereport = $this->getAllDeviceTableReport($data['applist'], $data['sdate'], $data['edate']);
        $sumincome= $this->getDeviceIncomeReport($data['graphreport']);
        
        $reportdata = array();
        if(!empty($tablereport)){
        	foreach($tablereport as $k=>$v)
            {
            	$reportdata[] = array(
            		'k'=>$k,
            		'ecpm'=>isset($v[2])?formatmoney($v[2]):'0.00',
            		'ecpc'=>isset($v[1])?formatmoney($v[1]):'0.00',
            		'ecpa'=>isset($v[3])?formatmoney($v[3]):'0.00',
            		'sum'=>isset($v['sum'])?formatmoney($v['sum']):'0.00',
            		'ratio'=>isset($v['ratio'])?number_format($v['ratio'],2).'%':'0.00%'
            	);
            }
        }
        $reportdata[] = array('k'=>'总收入','ecpm'=>formatmoney($sumincome),'ecpc'=>'','ecpa'=>'','sum'=>'','ratio'=>'');
        
		$filename=urlencode('平台类型分布统计').'_'.date('Y-m-dHis');
	    header("Content-Disposition:attachment;filename={$filename}.csv");
	    $table = iconv("UTF-8","GBK",",eCPM收入,eCPC收入,eCPA收入,总计,占比").PHP_EOL;
	    if(!empty($reportdata)){
			foreach($reportdata as $k=>$v){
				$table.=iconv("UTF-8","GBK",$v['k'].",".$v['ecpm'].",".$v['ecpc'].",".$v['ecpa'].",".$v['sum'].",".$v['ratio']).PHP_EOL;
			}
	    }
	    echo $table;
    }
    
    /**
     * 广告形式分布
     * Enter description here ...
     */
    function gatherAdTypeReport(){
        $data = $this->getCondition();
        $data = array_merge_recursive($data,$this->formatTop());
        $data['graphreport'] = $this->getAllAdTypeGraphReport($data['applist'], $data['sdate'], $data['edate']);
        $data['tablereport'] = $this->getAllAdTypeTableReport($data['applist'], $data['sdate'], $data['edate']);
        $data['sumincome'] = $this->getDeviceIncomeReport($data['graphreport']);
        $data['datelist'] = mktimes($data['sdate'], $data['edate'], true, true);
        $this->load->model('user/userinfoclass');
        $user=$this->userinfoclass->getRow($this->userid);
        $data['totalmoney']=(int)$user['totalmoney'];
        $this->load->view('manage/appreport/gatheradtypereport', $data);
    }
    
    function exportAdtypeReport(){
	    $data = $this->getCondition();
        $data = array_merge_recursive($data,$this->formatTop());
        $data['graphreport'] = $this->getAllAdTypeGraphReport($data['applist'], $data['sdate'], $data['edate']);
        $data['tablereport'] = $this->getAllAdTypeTableReport($data['applist'], $data['sdate'], $data['edate']);
        $data['sumincome'] = $this->getDeviceIncomeReport($data['graphreport']);
        
        $reportdata = array();
        if(!empty($data['tablereport'])){
        	foreach($data['tablereport'] as $k=>$v)
            {
            	$ecpm = isset($v[2])?formatmoney($v[2]):'0.00';
            	$ecpc = isset($v[1])?formatmoney($v[1]):'0.00';
            	$ecpa = isset($v[3])?formatmoney($v[3]):'0.00';
            	$sum = isset($v['sum'])?formatmoney($v['sum']):'0.00';
            	$ratio = isset($v['ratio'])?number_format($v['ratio'],2):'0.00';
            	$reportdata[] = array('k'=>$k,'ecpm'=>$ecpm,'ecpc'=>$ecpc,'ecpa'=>$ecpa,'sum'=>$sum,'ratio'=>$ratio);
            }
        }
        $reportdata[] = array('k'=>'总收入','ecpm'=>formatmoney($data['sumincome']),'ecpc'=>'','ecpa'=>'','sum'=>'','ratio'=>'');
        
        $filename=urlencode('广告形式分布统计').'_'.date('Y-m-dHis');
	    header("Content-Disposition:attachment;filename={$filename}.csv");
	    $table = iconv("UTF-8","GBK",",eCPM收入,eCPC收入,eCPA收入,总计,占比").PHP_EOL;
	    if(!empty($reportdata)){
			foreach($reportdata as $k=>$v){
				$table.=iconv("UTF-8","GBK",$v['k'].",".$v['ecpm'].",".$v['ecpc'].",".$v['ecpa'].",".$v['sum'].",".$v['ratio']).PHP_EOL;
			}
	    }
	    echo $table;
   	}

    /**
     * 广告形式分布
     * Enter description here ...
     */
    function gatherAppReport(){
        $data = $this->getCondition();
        $data = array_merge_recursive($data,$this->formatTop());
        $data['tablereport'] = $this->getAllAppTableReport($data['applist'], $data['sdate'], $data['edate']);
        $data['datelist'] = mktimes($data['sdate'], $data['edate'], true, true);
        $this->load->model('user/userinfoclass');
        $user=$this->userinfoclass->getRow($this->userid);
        $data['totalmoney']=(int)$user['totalmoney'];
        if(!empty($data['applist']) && count($data['applist']) > 0)
        {
            foreach($data['applist'] as $v)
            {
                $data['list'][$v['appid']] = $v['appname'];
            }
        }
        
        $this->load->view('manage/appreport/gatherappreport', $data);
    }
    
    function exportAppReport(){
        $data = $this->getCondition();
        $data = array_merge_recursive($data,$this->formatTop());
        $data['tablereport'] = $this->getAllAppTableReport($data['applist'], $data['sdate'], $data['edate']);
        if(!empty($data['applist']) && count($data['applist']) > 0)
        {
            foreach($data['applist'] as $v)
            {
                $data['list'][$v['appid']] = $v['appname'];
            }
        }
        
    	$reportios = array();
        if(!empty($data['tablereport']['iOS'])){
        	foreach($data['tablereport']['iOS'] as $k=>$v)
            {
            	$reportdios[] = array(
            		'appname'=>$data['list'][$k],
            		'sum'=>isset($v['sum'])?formatmoney($v['sum']):'0.00',
            		'ratio'=>isset($v['ratio'])?number_format($v['ratio'],2).'%':'0.00%',
            		'banner'=>isset($v[1])?formatmoney($v[1]):'0.00',
            		'wall'=>isset($v[3])?formatmoney($v[3]):'0.00',
            		'popup'=>isset($v[2])?formatmoney($v[2]):'0.00',
            		'wall2'=>isset($v[6])?formatmoney($v[6]):'0.00',
            		'feeds'=>isset($v[20])?formatmoney($v[20]):'0.00'
            	);
            }
        }
        $reportandroid = array();
    	if(!empty($data['tablereport']['Android'])){
        	foreach($data['tablereport']['Android'] as $k=>$v)
            {
            	$reportdandroid[] = array(
            		'appname'=>$data['list'][$k],
            		'sum'=>isset($v['sum'])?formatmoney($v['sum']):'0.00',
            		'ratio'=>isset($v['ratio'])?number_format($v['ratio'],2).'%':'0.00%',
            		'banner'=>isset($v[1])?formatmoney($v[1]):'0.00',
            		'wall'=>isset($v[3])?formatmoney($v[3]):'0.00',
            		'popup'=>isset($v[2])?formatmoney($v[2]):'0.00',
            		'wall2'=>isset($v[6])?formatmoney($v[6]):'0.00',
            		'feeds'=>isset($v[20])?formatmoney($v[20]):'0.00'
            	);
            }
        }
        
        $filename=urlencode('应用分布统计').'_'.date('Y-m-dHis');
	    header("Content-Disposition:attachment;filename={$filename}.csv");
	    $table = iconv("UTF-8","GBK","应用名称,总收入,收入占比,Banner收入,推荐墙收入,插屏广告收入,积分墙收入,信息流收入").PHP_EOL;
	    if(!empty($reportios)){
			foreach($reportios as $k=>$v){
				$table.=iconv("UTF-8","GBK",$v['appname'].",".$v['sum'].",".$v['ratio'].",".$v['banner'].",".$v['wall'].",".$v['popup'],$v['wall2'],$v['feeds']).PHP_EOL;
			}
	    }
	    $table .= iconv("UTF-8","GBK","应用名称,总收入,收入占比,Banner收入,推荐墙收入,插屏广告收入,积分墙收入,信息流收入").PHP_EOL;
    	if(!empty($reportandroid)){
			foreach($reportandroid as $k=>$v){
				$table.=iconv("UTF-8","GBK",$v['appname'].",".$v['sum'].",".$v['ratio'].",".$v['banner'].",".$v['wall'].",".$v['popup'],$v['wall2'],$v['feeds']).PHP_EOL;
			}
	    }
	    echo $table;
    }

    private function formatTop()
    {
        $data['applist'] =  $this->AppInfoClass->getAppInfoList('',$this->userInfo);
//        $data['commonreport'] = $this->getAllCommonReport($data['applist']);
        $data['commonreport'] = $this->NewAppReportClass->getDayAppIncomeReport2($this->userid);
        return $data;
    }

    /**
     * 通用数据
     * Enter description here ...
     * @param unknown_type $campaignId
     * @param unknown_type $adform
     */
    private function getAllCommonReport($appList)
    {
        $appIds = $this->getAllAppId($appList);
        $data = $this->NewAppReportClass->getDayAppIncomeReport($appIds);
        return $data;
    }

    /**
     * 合计数据
     * Enter description here ...
     * @param unknown_type $campaignId
     * @param unknown_type $adform
     */
    private function getSumIncomeReport($data)
    {
        if(empty($data))
        {
            return '0.00';
        }
        $sumIncome = 0;
        foreach ($data as $v)
        {
            foreach ($v as $vt)
            {
                $sumIncome += $vt;
            }
        }
        return $sumIncome;
    }

    /**
     * 合计数据
     * Enter description here ...
     * @param unknown_type $campaignId
     * @param unknown_type $adform
     */
    private function getDeviceIncomeReport($data)
    {
        if(empty($data))
        {
            return '0.00';
        }
        $sumIncome = 0;
        foreach ($data as $v)
        {
            $sumIncome += $v;
        }
        return $sumIncome;
    }


    /**
     * 通用数据
     * Enter description here ...
     * @param unknown_type $campaignId
     * @param unknown_type $adform
     */
    private function getAllGraphReport($appList, $sd, $ed)
    {
        $appIds = $this->getAllAppId($appList);
        $data = $this->NewAppReportClass->getAllGraphReport($appIds, $sd, $ed);
        return $data;
    }


    /**
     * 平台类型分布图表
     * Enter description here ...
     * @param unknown_type $campaignId
     * @param unknown_type $adform
     */
    private function getAllDeviceGraphReport($appList, $sd, $ed)
    {
        $appIds = $this->getAllAppId($appList);
        $data = $this->NewAppReportClass->getAllDeviceGraphReport($appIds, $sd, $ed);
        return $data;
    }


    /**
     * 广告形式分布图表
     * Enter description here ...
     * @param unknown_type $campaignId
     * @param unknown_type $adform
     */
    private function getAllAdTypeGraphReport($appList, $sd, $ed)
    {
        $appIds = $this->getAllAppId($appList);
        $data = $this->NewAppReportClass->getAllAdTypeGraphReport($appIds, $sd, $ed);
        return $data;
    }

    /**
     * 通用数据
     * Enter description here ...
     * @param unknown_type $campaignId
     * @param unknown_type $adform
     */
    private function getAllTableReport($appList, $sd, $ed)
    {
        $appIds = $this->getAllAppId($appList);
        $data = $this->NewAppReportClass->getAllTableReport($appIds, $sd, $ed);
        return $data;
    }

    /**
     * 通用数据
     * Enter description here ...
     * @param unknown_type $campaignId
     * @param unknown_type $adform
     */
    private function getAllDeviceTableReport($appList, $sd, $ed)
    {
        $appIds = $this->getAllAppId($appList);
        $data = $this->NewAppReportClass->getAllDeviceTableReport($appIds, $sd, $ed);
        return $data;
    }

    /**
     * 通用数据
     * Enter description here ...
     * @param unknown_type $campaignId
     * @param unknown_type $adform
     */
    private function getAllAdTypeTableReport($appList, $sd, $ed)
    {
        $appIds = $this->getAllAppId($appList);
        $data = $this->NewAppReportClass->getAllAdTypeTableReport($appIds, $sd, $ed);
        return $data;
    }

    /**
     * 通用数据
     * Enter description here ...
     * @param unknown_type $campaignId
     * @param unknown_type $adform
     */
    private function getAllAppTableReport($appList, $sd, $ed)
    {
        $appIds = $this->getAllAppId($appList);
        $data = $this->NewAppReportClass->getAllAppTableReport($appIds, $sd, $ed);
        return $data;
    }


    /**
     * 获取所有AppId
     */
    private function getAllAppId($appList)
    {
        $tmpAppIds = '';
        if(!empty($appList))
        {
            foreach ($appList as $v)
            {
                $tmpAppIds[] = $v['appid'];
            }
        }
        if(!empty($tmpAppIds))
        {
            $appIds = implode(',', $tmpAppIds);
        }
        else
        {
            $appIds = '';
        }
        return $appIds;
    }




    /**
     * 处理页面请求数据
     * Enter description here ...
     */
    function getCondition(){
        $condition = array();
        $osType = $this->input->post("ostype");
        $appid = $this->input->post("appid");
        $adform = $this->input->post("adform");
        $inputDate = $this->input->post("inputDate");
        $timeDate = explode('~', $inputDate);

        if(count($timeDate) == 2)
        {
            $sdate = $timeDate[0];
            $edate = $timeDate[1];
        }
        if(empty($sdate))
        {
            $sdate = date('Y-m-d',time()-6*86400);
        }
        if(empty($edate))
        {
            $edate = date('Y-m-d');
        }
        if(empty($adform))
        {
            $adform = 1;
        }
        if(empty($osType))
        {
            $osType = 1;
        }
        $condition['ostype'] = $osType;
        $condition['appid'] = $appid;
        $condition['adform'] = $adform;
        $condition['sdate'] = $sdate;
        $condition['edate'] = $edate;
        $condition['leftlist'] = $this->AppInfoClass->getAppInfoList('', $this->userInfo);
        return $condition;
    }
}
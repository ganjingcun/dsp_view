<?php
/**
 * 应用管理类
 * @filename	: appinfo.php
 * @author		: wangdong
 * @datetime	: 2013-10-15
 * @Description  : 前台App管理页面。
 */
define('UPLOAD_CSV_PATH',UPLOAD_DIR.'/csv/');
class AppInfo extends MY_Controller{
    private  $userid = null;
    private $urlmatch='/^(http:\/\/)?(https:\/\/)?([\w\d-]+\.)+[\w-]+(\/[\d\w-.\/\!\+?%&=]*)?$/Ui';
    public function __construct()
    {
        parent::__construct();
        $this->output->set_header("Cache-control:private;");
        $this->load->model('manage/AuthClass');
        $this->load->model('manage/AppInfoClass');
        $this->load->model('manage/AppAuditInfoClass');
        $this->load->model('manage/AppPositionClass');
        $this->load->model('manage/AppChannelClass');
        $this->load->model('manage/AppTagRelationClass');
        $this->userid =  AuthClass::getUserId();
    }

    function index($subpage=0){
        $data['userinfo'] = $this->userInfo;
        $data['subpage']=$subpage;
        $this->load->view('managev2/index', $data);
    }

    /**
     * 添加App应用页。
     * @author wangdong
     */
    function showAdd(){
        $data = array();
        $data['apptype'] = $this->AppInfoClass->getAppType();
        $this->loadHeader('创建应用', 2, 21);
        $this->load->view('manage/appinfo/showadd', $data);
    }

    /**
     * 保存添加App。
     * @author wangdong
     */
    function doAdd(){
        $postData = $this->input->post();
        $appInfo = $this->AppInfoClass->doAdd($postData, $this->userInfo);
        if($appInfo['status'] == 0)
        {
            ajaxReturn($appInfo['info'], 0, $appInfo['data']);
        }
        ajaxReturn('', 1, array($appInfo['appid'], ''));
    }

    /**
     * 保存编辑App。
     * @author wangdong
     */
    function doEdit(){
        $postData = $this->input->post();
        $appInfo = $this->AppInfoClass->doEdit($postData, $this->userInfo);
        if($appInfo['status'] == 0)
        {
            ajaxReturn($appInfo['info'], 0, $appInfo['data']);
        }
        ajaxReturn('', 1, array($appInfo['appid']));
    }

    /**
     * 保存编辑App。
     * @author wangdong
     */
    function doEditIntegration(){
        $postData = $this->input->post();
        $appInfo = $this->AppInfoClass->doEditIntegration($postData, $this->userInfo);
        if($appInfo['status'] == 0)
        {
            ajaxReturn($appInfo['info'], 0, $appInfo['data']);
        }
        ajaxReturn('', 1, array($appInfo['appid']));
    }

    /**
     * 开启积分墙广告。
     * @author wangdong
     */
    function doIntegral(){
        $postData = $this->input->post();
        $appInfo = $this->AppInfoClass->doIntegral($postData, $this->userInfo);

        if($appInfo['status'] == 0)
        {
            ajaxReturn($appInfo['info'], 0, $appInfo['data']);
        }
        //$appPositionInfo = $this->AppPositionClass->doAdd($postData, $this->userInfo);
        ajaxReturn('', 1, array($appInfo['appid']));
    }
    
	/**
     * 开启feeds广告。
     */
    function doFeeds(){
        $postData = $this->input->post();
        $appInfo = $this->AppInfoClass->doFeeds($postData, $this->userInfo);

        if($appInfo['status'] == 0)
        {
            ajaxReturn($appInfo['info'], 0, $appInfo['data']);
        }
        ajaxReturn('', 1, array($appInfo['appid']));
    }

    /**
     * 开关状态。
     * @author wangdong
     */
    function ajaxSwitch(){
        $postData = $this->input->post();
        $appInfo = $this->AppInfoClass->doSwitch($postData, $this->userInfo);
        if($appInfo['status'] == 0)
        {
            ajaxReturn($appInfo['info'], 0, $appInfo['data']);
        }
        ajaxReturn('', 1, '');
    }

    /**
     * 编辑App应用页。
     * @author wangdong
     */
    function showEdit(){
        $appId = $this->input->get('appid');
        $data = $this->AppInfoClass->getDetailInfo($this->userInfo, $appId);
        $data['apptype'] = $this->AppInfoClass->getAppType();
        $data['positionList'] = $this->AppPositionClass->getList(array('appid'=>$appId, 'userid' => $this->userInfo['userid'], 'status' => 0));
        $data['appid'] = $appId;
        $data['leftlist'] = $this->AppInfoClass->getAppInfoList('', $this->userInfo);
        $this->loadHeader('应用管理', 2, 22);
        $this->load->view('manage/appinfo/showedit', $data);
    }

    /**
     * App应用列表页。
     * @author wangdong
     */
    function showList(){
        $data['auditstatus'] = $this->input->get_post('auditstatus');
        $data['ostypeid'] = $this->input->get_post('ostypeid');
        $data['keys'] = $this->input->get_post('keys');
        if(empty($data['auditstatus']) && $data['auditstatus'] === false)
        {
            $data['auditstatus'] = 9;
        }
        $this->loadHeader('应用管理', 2, 22);
        $data['data'] = $this->AppInfoClass->getAppList($data, $this->userInfo);
        $data['leftlist'] = $this->AppInfoClass->getAppInfoList('', $this->userInfo);
        $this->load->view('manage/appinfo/showlist', $data);
    }

    /**
     * 展现App信息。
     * @author wangdong
     */
    function showDetail(){
        $appId = $this->input->get('appid');
        $data = $this->AppInfoClass->getDetailInfo($this->userInfo, $appId);
        $data['positionList'] = $this->AppPositionClass->getList(array('appid'=>$appId, 'userid' => $this->userInfo['userid'], 'status' => 0));
        $data['appid'] = $appId;
        $data['leftlist'] = $this->AppInfoClass->getAppInfoList('', $this->userInfo);
        $data['channellist'] = $this->AppChannelClass->getList($appId, $this->userInfo['userid']);
        $this->loadHeader('应用管理', 2, 22);
        $this->load->view('manage/appinfo/showdetail', $data);
    }

    /**
     * 展现App信息。
     * @author wangdong
     */
    function showAudit(){
        $appId = $this->input->get('appid');
        $data = $this->AppInfoClass->getDetailInfo($this->userInfo, $appId);
        $data['positionList'] = $this->AppPositionClass->getList(array('appid'=>$appId, 'userid' => $this->userInfo['userid'], 'status' => 0));
        $this->loadHeader('应用管理', 2, 22);
        $this->load->view('manage/appinfo/showaudit', $data);
    }

    /**
     * 展现App信息。
     * @author wangdong
     */
    function showInfo(){
        $appId = $this->input->get('appid');
        $data = $this->AppInfoClass->getDetailInfo($this->userInfo, $appId);
        $data['appid'] = $appId;
        $data['leftlist'] = $this->AppInfoClass->getAppInfoList('', $this->userInfo);
        $this->loadHeader('应用管理', 2, 22);
        $this->load->model("report/AppReportClass");
        $this->load->view('manage/appinfo/showinfo', $data);
    }

    /**
     * 展现开关信息。
     * @author wangdong
     */
    function showSwitchInfo(){
        $appId = $this->input->get('appid');
        $data = $this->AppInfoClass->getDetailInfo($this->userInfo, $appId);
        $data['appid'] = $appId;
        $data['leftlist'] = $this->AppInfoClass->getAppInfoList('', $this->userInfo);
        $this->loadHeader('应用管理', 2, 22);
        $this->load->model("report/AppReportClass");
        $this->load->view('manage/appinfo/showswitchinfo', $data);
    }

    //上传文件
    function ajaxVerifyApp()
    {
        $appid = (int) $this->input->get("appid");
        $appInfo = $this->AppInfoClass->getAppInfoRow($appid, $this->userInfo['userid']);
        if(empty($appInfo))
        {
            ajaxReturn('您要操作的应用不存在！', false);
        }
        $data = $this->AppInfoClass->upload($appInfo['ostypeid']);

        if($data['status'])
        {
            $appData = $this->AppInfoClass->doAutit($appid, $data['data'], 1, $this->userInfo);
            if($appData['status'] == 0)
            {
                ajaxReturn('', $appData['status'], $appData['data']);
            }
        }
        ajaxReturn('', $data['status'], $data['data']);
    }

    //上传文件
    function doAudit()
    {
        $appid = (int) $this->input->post("appid");
        $url = $this->input->post("url");
        $appInfo = $this->AppInfoClass->getAppInfoRow($appid, $this->userInfo['userid']);
        if(empty($appInfo))
        {
            ajaxReturn('您要操作的应用不存在！', false);
        }

        $appData = $this->AppInfoClass->doAutit($appid, $url, 2, $this->userInfo);
        ajaxReturn('', $appData['status'], $appData['data']);
    }

    //提交审核
    function doStatus()
    {
        $appid = (int) $this->input->post("appid");
        $url = $this->input->post("url");
        $marketurl = $this->input->post("marketurl");
        $auditype = $this->input->post("auditype");
        if(empty($url) && empty($marketurl))
        {
            ajaxReturn('', false,'请先上传指定格式文件或填写App URL。');
        }
        $appInfo = $this->AppInfoClass->getAppInfoRow($appid, $this->userInfo['userid']);
        if(empty($appInfo))
        {
            ajaxReturn('', false, '您要操作的应用不存在！');
        }

        /*
        //提交到畅思DSP
        $postdata = array();
        $postdata['appid'] = $appInfo['chance_appid'];
        $postdata['auditurl'] = $url;
        $postdata['auditmarketurl'] = $marketurl;
        $postdata['audittype'] = $auditype;
        $postdata = array('params' => $postdata, 'userinfo' => $this->config->item('dsp_api_userinfo'));
        $result = curlPost($this->config->item('dsp_api_server') . 'doAuditAPI', $postdata, 'POST', true);
        $result = json_decode($result, true);
        if($result['status'] != 1)
        {
            ajaxReturn('', false, '远程服务器错误！');
            error_log('json:'.json_encode($result));
        }
        */

        $this->load->library('sendemail');
        $this->sendemail->appvierfy($appInfo['appname']);
        //保存当前提交审核的app信息
        $data = $this->AppInfoClass->getDetailInfo($this->userInfo, $appid);
        $data['positionList'] = $this->AppPositionClass->getList(array('appid'=>$appid, 'userid' => $this->userInfo['userid'], 'status' => 0));
        $data['auditurl'] = $url;
        $data['auditmarketurl'] = $marketurl;
        $data['channellist'] = $this->AppChannelClass->getList($appid, $this->userInfo['userid']);
        $data['apptaglist'] = $this->AppTagRelationClass->getList($appid, $this->userInfo['userid']);
        if($auditype == '2') $this->AppInfoClass->doAutit($appid, $marketurl, 2, $this->userInfo);
        $auditid = $this->AppAuditInfoClass->doAdd($data, $appid);
        $appData = $this->AppInfoClass->doStatus($appid, $auditid, $this->userInfo);
        ajaxReturn('', $appData['status'], $appData['data']);
    }

    /**
     * 展现SDK信息。
     * @author wangdong
     */
    function showSDKInfo(){
        $this->load->model('manage/SdkVersionLogClass');
        $os=array('ios'=>1,'android'=>2,'cocos'=>3);
        $sdktype=array('common'=>1,'video'=>2);
        $data['iossdkinfo1'] = $this->SdkVersionLogClass->getRow($os['ios'],$sdktype['common']);
        $data['iossdkinfo2'] = $this->SdkVersionLogClass->getRow($os['ios'],$sdktype['video']);
        $data['androidsdkinfo'] = $this->SdkVersionLogClass->getRow($os['android'],$sdktype['common']);
        $data['cocossdkinfo'] = $this->SdkVersionLogClass->getRow($os['cocos'],$sdktype['common']);
        $data['leftlist'] = $this->AppInfoClass->getAppInfoList('', $this->userInfo);
        $this->loadHeader('SDK下载', 4, 23);
        $this->load->view('manage/appinfo/showsdkinfo', $data);
    }
    
    /**
     * 展现API信息。
     * @author wf
     */
    function showAPIInfo(){
        $this->load->model('manage/ApiVersionLogClass');
        $data['apiinfo'] = $this->ApiVersionLogClass->getRow(1);
        $data['leftlist'] = $this->AppInfoClass->getAppInfoList('', $this->userInfo);
        $this->loadHeader('API下载', 5, 23);
        $this->load->view('manage/appinfo/showapiinfo',$data);
    }

    /**
     * 展现SDK信息。
     * @author wangdong
     */
    function showSDKInfoList(){
        $ostype = $this->input->get("ostype");
        if($ostype != 1 && $ostype != 2 && $ostype != 3){
        	$ostype = 1;
        }
        $sdktype=$this->input->get("sdktype")?$this->input->get("sdktype"):1;
        $this->load->model('manage/SdkVersionLogClass');
        $os=array('ios'=>1,'android'=>2,'cocos'=>3);
        $sdk=array('common'=>1,'video'=>2);
        $data['ioscommon']= $this->SdkVersionLogClass->getList($os['ios'],$sdk['common']);
        $data['iosvideo']= $this->SdkVersionLogClass->getList($os['ios'],$sdk['video']);
        $data['androidcommon']= $this->SdkVersionLogClass->getList($os['android'],$sdk['common']);
        $data['cocoscommon']= $this->SdkVersionLogClass->getList($os['cocos'],$sdk['common']);
        $data['leftlist'] = $this->AppInfoClass->getAppInfoList('', $this->userInfo);
        $data['ostype'] = $ostype;
        $data['sdktype'] = $sdktype;
        $this->loadHeader('SDK下载', 4, 23);
        $this->load->view('manage/appinfo/showsdkinfolist', $data);
    }
    
    /**
     * 展现API信息。
     * @author wf
     */
    function showApiInfoList(){
        $this->load->model('manage/ApiVersionLogClass');
        $data['listinfo'] = $this->ApiVersionLogClass->getList();
        $data['leftlist'] = $this->AppInfoClass->getAppInfoList('', $this->userInfo);
        $this->loadHeader('API下载', 5, 23);
        $this->load->view('manage/appinfo/showapiinfolist', $data);
    }
    /**
     *
     * 展现FAQ及文档,
     */
    function faqDoc(){
    	$from= $this->input->get("from");
    	if ($from == 2) {
        	$this->loadHeader('API下载', 5, 23);
    	}
    	else{
    		$this->loadHeader('SDK下载', 4, 23);
    	}
    	$data['from']=$from;
        $this->load->view('manage/appinfo/faqdoc',$data);
    }
    /**
     * 积分墙回调方式查看
     */
    function editWallConfig(){
        $appId = $this->input->get('appid');
        $data = $this->AppInfoClass->getDetailInfo($this->userInfo, $appId);
        $data['appid'] = $appId;
        if($data['appcampaignid']){
            $coco_url = "http://stats.cocounion.com/";
            $ret =  curlGet($coco_url.'app/'.$data['appcampaignid'].'/notify');
            $ret = json_decode($ret,true);
            $data['coco_url'] =  $this->jointUrl($ret['notifyServer']['url'],$ret['dictionary']);
            $data['secretKey'] = $ret['notifyServer']['secretKey'];
        }
        $this->loadHeader('应用管理', 3, 22);
        $this->load->view('manage/appinfo/editbackconfig', $data);
    }
    /**
     * 编辑积分回调方式
     */
    function backconfig(){
        $appId = $this->input->get('appid');
        $data = $this->AppInfoClass->getDetailInfo($this->userInfo, $appId);
        $data['appid'] = $appId;
        $this->loadHeader('应用管理', 3, 22);
        if($data['appcampaignid']){
            $coco_url = "http://stats.cocounion.com/";
            $ret =  curlGet($coco_url.'app/'.$data['appcampaignid'].'/notify');
            $ret = json_decode($ret,true);
            $data['coco_url'] =  $this->jointUrl($ret['notifyServer']['url'],$ret['dictionary']);
            $data['secretKey'] = $ret['notifyServer']['secretKey'];
            $this->load->view('manage/appinfo/backconfig', $data);
        }else{
            $this->load->view('manage/appinfo/editbackconfig', $data);
        }
    }
    /**
     * 拼接url
     */
    function jointUrl($url='',$dictionary=array()){
        if(!empty($dictionary)){
            $str = "";
            foreach($dictionary as $key=>$val){
                $str  = "&".$key."=".$val.$str;
            }
            $str = ltrim($str, '&');
            $url = $url."?".$str;
        }
        return $url;
    }
    /**
     * 验证积分回调设置信息
     */
    function checkWallData(){
        $secretKey = $this->input->post('secretKey');
        $url = $this->input->post('url');
        if(empty($url))
        {
            $error['data'] = array('back_urlmsg', '请填写服务器端地址');
            $error['info'] = 'back_urlerr';
            ajaxReturn($error['info'], 0, $error['data']);
        }
        if( strpos($url,"http") !== 0){
            $error['data'] = array('back_urlmsg', 'url地址格式不正确，请以http开头');
            $error['info'] = 'back_urlerr';
            ajaxReturn($error['info'], 0, $error['data']);
        }
    }
    /**
     * 保存积分设置
     *
     */

    function saveconfig(){
        $appId = $this->input->get('appid');
        $app_info = $this->AppInfoClass->getDetailInfo($this->userInfo, $appId);
        $old_appKey = $this->input->post("old_secretKey");
        $coco_url = "http://stats.cocounion.com/";
        $wallrechargetype = $this->input->post('wallrechargetype');
        if($wallrechargetype == '0') {
            $this->AppInfoClass->editAppwall(array('appcampaignid'=>$old_appKey,'wallrechargetype'=>$wallrechargetype,'appid'=>$appId), $this->userInfo);
            ajaxReturn('1', 1, '1');
        }
        $this->checkWallData();
        if(empty($old_appKey)){
            //新建app
            $url = $this->input->post('url');
            $post_data = array();
            if($app_info['ostypeid'] == 1) $platform = "Android";
            else $platform ='iOS';
            $post_data['userId'] = "dev";
            $post_data['platform'] = $platform;
            $post_data['appName'] = $app_info['appname'];
            $post_data['appIdentifier'] = $app_info['packagename'];
            $create_ret =  curlPost($coco_url.'app',  $post_data);
            $create_ret = json_decode($create_ret,true);
            if(!is_array($create_ret))
            {
                ajaxReturn('1', 0, 'error');
            }
            $appKey = $create_ret['appId'];
            if(empty($appKey)) ajaxReturn('1',0,'error');
            //入库app_info 表 appcampaignid 和 wallrechargetype
            $this->AppInfoClass->editAppwall(array('appcampaignid'=>$appKey,'wallrechargetype'=>$wallrechargetype,'appid'=>$appId), $this->userInfo);
             
            // 保存url 和 密钥
            $secretKey = $this->input->post('secretKey');
            $notify_data =  $this->getUrlInfo($appKey,$url,$secretKey);
            $ret =  curlPost($coco_url.'notify',  $notify_data);
            $ret = json_decode($ret,true);
            if(!is_array($ret))
            {
                ajaxReturn('1', 0, 'error');
            }
            if(!empty($ret['appId'])){
                ajaxReturn('2', 1, '1');
            }else{
                ajaxReturn('2', 0, 'error');
            }
        }else{
            //入库app_info 表 appcampaignid 和 wallrechargetype
            $this->AppInfoClass->editAppwall(array('appcampaignid'=>$old_appKey,'wallrechargetype'=>$wallrechargetype,'appid'=>$appId), $this->userInfo);
            //编辑 url 和密钥
            $secretKey = $this->input->post('secretKey');
            $url = $this->input->post('url');
            $notify_data =  $this->getUrlInfo($old_appKey,$url,$secretKey); //var_dump($notify_data);exit;
            $ret =  curlPost($coco_url.'notify',$notify_data,'PUT');
            $ret = json_decode($ret,true);
            if(!is_array($ret))
            {
                ajaxReturn('1', 0, 'error');
            }
            if(!empty($ret['result']['appId'])){
                ajaxReturn('3', 1, '1');
            }else{
                ajaxReturn('3', 0, 'error');
            }
        }
         
    }
    /**
     * 取得 保存url 和密钥的参数
     */
    function getUrlInfo($appId='',$url='',$secretKey=''){
        $url_info = $this->formatUrl($url);
        $url_info['dictionary'] = empty($url_info['dictionary'])?'':$url_info['dictionary'];
        $notify_data = array(
                 'appId' => $appId, 
                 'mapping' => Array
        ( 'transactionid' =>'',
                          'device' => 
        Array ( 'mac' =>'', 'os' =>'', 'ip' => '','os_version' =>'', 'idfa' =>'', 'imei' =>'' ) ,
                          'params' => 
        Array ('coins' =>'', 'app' => Array ('token' =>'' ) ,'ad' => Array ('adid' =>'','adtitle' =>'' ) ,'taskname' => '','taskcontent' =>'' )
        ),
                'dictionary' => $url_info['dictionary'],
                'notifyServer' => Array ('url' =>$url_info['base_url'] ,'method'=> 'GET','secretKey'=>$secretKey ) 
        );
        return $notify_data;
    }
    /**
     * 格式化url 参数
     */
    function formatUrl($url=''){
        $info = array();
        $urlinfo = explode("?",$url);
        $info['base_url'] = $urlinfo[0];
        $info['dictionary'] = '';
        if(!empty($urlinfo[1])){
            $queryParts = explode("&", $urlinfo[1]);

            $params = array();
            foreach ($queryParts as $param)
            {
                $item = explode('=', $param);
                $params[$item[0]] = $item[1];
            }
            $info['dictionary'] = $params;
        }
        return $info;
    }
    
    /**
     * 批量添加App应用页。
     * @author wf
     */
    function showBatchAdd(){
        $this->loadHeader('批量创建应用', 2, 21);
        $this->load->view('manage/appinfo/showbatchadd');
    }
    
    /**
     * 批量添加App:上传文档
     * @author wf
     */
    function doUpload(){
    	$this->load->library('MyUpload');
	    if ($_FILES["file"]["error"] > 0)
		{
			ajaxReturn('CSV文件上传失败,错误代码为：', 0, $_FILES["file"]["error"]);
		}
		else
		{
            if(empty($_FILES['file']['name'])){
            	ajaxReturn('请上传CSV文件', 0,"");
            }else{
                $upload =$this->upload($_FILES['file']);
                if($upload['error']){
                	ajaxReturn('CSV文件上传到服务器失败：'.$upload['error'], 0, array($upload['error']));
                }
                else{
                	$csvpath=UPLOAD_DIR.$upload['path'];
                	ajaxReturn('CSV文件上传到服务器成功', 1, array($csvpath)); 
                }
            }
		}
    }
    
    /**
     * 批量添加App。
     * wf
     */
    function doBatchAdd(){
		$file=$this->input->post('file');
	    $row = 1;
	    $data = array();
	    if(!file_exists($file)){
	    	ajaxReturn('CSV文件上传到服务器失败:服务器不存在此文件', 0, array()); 
	    }
		else {
			if(($handle = fopen($file, "r")) !== FALSE) {
			    while (($curline = fgetcsv($handle,',')) !== FALSE) {
			        $num = count($curline);
			        $tmp = array();
			        $row++;
			        for ($c=0; $c < $num; $c++) {
						$encode = mb_detect_encoding($curline[$c], array("ASCII","UTF-8","GB2312","GBK","BIG5"));
//						array_push($tmp, iconv($encode,'UTF-8//IGNORE',$curline[$c]));
						array_push($tmp, mb_convert_encoding($curline[$c],"UTF-8",$encode));
			        }
			        array_push($data, $tmp);
			    }
			    fclose($handle);
			    $insert_batch = $this->AppInfoClass->doBatchAdd($data,$this->userInfo);
 				if(count($insert_batch)==1){
 					unlink($file);
 					ajaxReturn('请上传有内容文件。', 0, array()); 
 				}
 				else{
 					$success=0;$error=0;$cursor="";
 					for ($i = 0; $i < count($insert_batch); $i++) {
	 					if ($insert_batch[$i]['status']==0){
 							$error++;
 							$cursor.=($i+2).",";
 						}
 						else{
 							$success++;
 						}
 					}
 					$msg='共'.$success.'个应用创建成功，'.$error.'个应用创建失败。';
 					if($error>0){
 						$msg.='创建失败的应用对应文档以下几行：'.$cursor;
 					}
 					unlink($file);
 					ajaxReturn($msg, 1, array()); 
 				}
			}
			else{
				unlink($file);
				ajaxReturn('读取CSV文件失败，请重试。', 0, array()); 
			}
		} 
    }
    
    /**
     * 上传用户文件
     * @param $name   $_FILES['img'] 页面file名字
     * @return string 返回被保存的文件的数据库格式
     */

    private function upload($name,$path=UPLOAD_CSV_PATH)
    {
        $return = array('path'=>'','error'=>array());
        $MyUpload = new MyUpload($name);
        $MyUpload->setUploadPath($path);
        $fileExt = 'csv';
    	$MyUpload->setFileExt(array($fileExt));
        $MyUpload->setMaxsize(300*1024);//300k
        $strError = '';
        if( !$MyUpload->isAllowedTypes()){
            $strError = '请上传后缀为'.$fileExt.'的文件 !';
        }
        elseif( $MyUpload->isBigerThanMaxSize()){
            $strError = '此处的文件最大不能超过 '.intval($MyUpload->getMaxsize()/1024) .'KB';
        }
        if(empty($strError) && $MyUpload->upload()){
            $return['path'] = $MyUpload->getUplodedFilePath();
	        if(($handle = fopen(UPLOAD_DIR.$return['path'], "r")) !== FALSE) {
			    if(($curline = fread($handle,50)) !== FALSE) {
			    	$encoding=mb_detect_encoding($curline, array("ASCII","UTF-8","GB2312","GBK","BIG5"));
			    	if($encoding!="UTF-8" && $encoding!="utf-8"){
			    		$strError = '请上传格式为UTF-8的文件 !';
			    		$return['error'] = $strError;
			    	}
			    }
			    fclose($handle);
	        }
        }else{
            $return['error'] = $strError;
        }
        return $return;
    }
}
<?php
/**
 * 应用管理类
 * @filename	: appinfo.php
 * @author		: wangdong
 * @datetime	: 2013-10-15
 * @Description  : 前台App管理页面。
 */

class Paidprint extends MY_Controller{
    private  $userid = null;
    public function __construct()
    {
        parent::__construct();
        header("Cache-control:no-cache,no-store,must-revalidate");
        header("Pragma:no-cache");
        header("Expires:0");
        $this->load->helper('tools');
        $this->accounttypearr = array('0'=>"个人/团体",'1'=>"个人/团体",'2'=>"企业",'3'=>"个体工商");
        $this->acctypearr = array('1'=>"CPC",'2'=>"CPM",'3'=>"CPA");
        $this->load->model('user/AccountInfoClass');
        $this->load->model('user/TeamMembersClass');
        $this->load->model('user/UserInfoClass');
        $this->load->model('user/IncomeClass');
    }
 
        /**
         * 获取提款单详情
         */
        function paid_detail(){ 
                //$this->loadHeader('收入管理', 0, 0);
                $id = $this->input->get('id');
                $query = $this->db->get_where('ad_income_paid',array('id'=>$id));
                if ($query->num_rows() > 0)
                {
                    $paid_info = $query->row_array();
                } 
                $userid = $paid_info['userid'];
                $account_info = $this->get_user_account($userid);
                $paid_info['bankusername'] = $account_info['bankusername'];
                $paid_info['realname']  = $account_info['realname'];
                $paid_info['bankaccount'] = $account_info['bankaccount'];
                $paid_info['companyname'] = $account_info['companyname'];
                $paid_info['bank'] = $account_info['bank'];
                $paid_info['individualname'] = $account_info['individualname'];
                $data['paid_info'] = $paid_info;
                $income_info = array();
                $query = $this->db->get_where('ad_month_income',array('paidid'=>$paid_info['id']));
                if ($query->num_rows() > 0){
                        $income_info = $query->result_array();
                }
                $data['income_info'] = $income_info;
                $data['accounttype'] =$account_info['accounttype'];
                $this->load->view('manage/income/paid_print',$data);
        }      
          /**
         * 用户账号认证信息
         */
        function get_user_account($userid=''){
                $account_where = array('userid'=>$userid,'status'=>1,'auditstatus'=>1,'accounttype'=>$this->userInfo['istype']);  //结算单
                $query = $this->db->get_where('account_info' ,$account_where);
                if ($query->num_rows() > 0)
                {
                    $accountinfo = $query->row_array();
                }
                $account_type = $accountinfo['accounttype'];
                $accountinfo['accounttypename'] = $this->accounttypearr[$account_type] ;
                return $accountinfo;
        }
}

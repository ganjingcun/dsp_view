<?php
/**
 * 应用管理类
 * @filename	: appinfo.php
 * @author		: wangdong
 * @datetime	: 2013-10-15
 * @Description  : 前台App管理页面。
 */
define('UPLOAD_APP_PATH',UPLOAD_DIR.'/account/'.date('Ym'));
class IndexReg extends CI_Controller{
    private  $userInfo = null;
    private $urlmatch='/^(http:\/\/)?(https:\/\/)?([\w\d-]+\.)+[\w-]+(\/[\d\w-.\/\!\+?%&=]*)?$/Ui';
    public function __construct()
    {
        parent::__construct();
        header("Cache-control:no-cache,no-store,must-revalidate");
        header("Pragma:no-cache");
        header("Expires:0");
        $this->load->model('user/AccountInfoClass');
        $this->load->model('user/AuthClass');
        $this->load->library('session');
        $this->userInfo = AuthClass::getCurrentUser();
        $this->load->helper('tools');
        $this->load->helper('url');
        if(!isset($this->userInfo['userid']))
        {
            redirect('/webindex');
            exit;
        }
    }

    function doSave()
    {
        $postData = $this->input->post();
        if(isset($postData['id']) && $postData['id'] > 0)
        {
            $this->doEdit();
        }
        else
        {
            $this->doAdd();
        }
    }

    private function doAdd()
    {
        $postData = $this->input->post();
        $info = $this->AccountInfoClass->doAdd($postData, $this->userInfo);
        if($info['status'] == 0)
        {
            ajaxReturn($info['info'], 0, $info['data']);
        }
        ajaxReturn('', 1, '');
    }

    private function doEdit()
    {
        $postData = $this->input->post();
        $info = $this->AccountInfoClass->doEdit($postData, $this->userInfo);
        if($info['status'] == 0)
        {
            ajaxReturn($info['info'], 0, $info['data']);
        }
        ajaxReturn('', 1, '');
    }
    
    function upRemind()
    {
        $this->loadHeader('开发者身份升级提示', 0, 0);
        $this->load->view('/manage/upremind');
    }
    
    /**
     * 开发者首页。
     * @author wangdong
     */
    function index(){
        $this->loadHeader('开发者管理', 0, 0);
        $where['status'] = 1;
        $where['accounttype'] = isset($this->userInfo['istype'])?($this->userInfo['istype']):0;
        $where['auditstatus'] = 2;
        $where['userid'] = $this->userInfo['userid'];
        if( $this->userInfo['isauth'] < 2 )
        {
            redirect('/manage/appinfo/index');
        }
        $accountInfo = $this->AccountInfoClass->getAccountByStatus($where);
        if(empty($accountInfo))
        {
            $accountInfo['isempty'] = false;
        }
        else
        {
            $accountInfo['isempty'] = true;
        }
        $accountInfo['userinfo'] = $this->userInfo;
        $this->load->view('/manage/indexreg', $accountInfo);
    }

    //通用头部
    protected function loadHeader($pageTile = '广告活动管理', $slab = 0, $smenu = 0)
    {
        $headinfo['pageTitle'] = $pageTile;
        $headinfo['smenu'] = $smenu;
        $headinfo['slab'] = $slab;
        $headinfo['userinfo'] = $this->userInfo;
        $this->load->view("manage/inc/header", $headinfo);
    }

    /**
     * 上传身份图片
     */
    public function ajaxDoUpload()
    {
        $data = $this->upLoad();
        ajaxReturn('', $data['status'], $data['data']);
    }

    /**
     * 上传身份图片
     * @return unknown_type
     */
    private function upLoad()
    {
        $this->load->library('MyUpload',$_FILES['Filedata']);
        $upload = new MyUpload($_FILES['Filedata']);
        $upload->setUploadPath(UPLOAD_APP_PATH);
        $fileExt = array('jpg','png');
        $upload->setFileExt($fileExt);
        $upload->setMaxsize(2*1024*1024);

        $strError = '';

        if( !$upload->isAllowedTypes() )
        {
            $strError = '请上传后缀为png或jpg的文件 !';
        }
        elseif( $upload->isBigerThanMaxSize() )
        {
            $strError = '此处的文件最大不能超过2M!';
        }

        if(empty($strError) and $upload->upload())
        {
            $imgPath = $upload->getUplodedFilePath();
            $data['data'] = $imgPath;
            $data['status'] = 1;
        }
        else
        {
            $data['data'] = $strError;
            $data['status'] = 0;
        }
        return $data;
    }
}
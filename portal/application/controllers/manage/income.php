<?php
/**
 * 应用管理类
 * @filename	: appinfo.php
 * @author		: wangdong
 * @datetime	: 2013-10-15
 * @Description  : 前台App管理页面。
 */

class Income extends MY_Controller{
    private  $userid = null;
    public function __construct()
    {
        parent::__construct();
        header("Cache-control:no-cache,no-store,must-revalidate");
        header("Pragma:no-cache");
        header("Expires:0");
        $this->accounttypearr = array('0'=>"个人/团体",'1'=>"个人/团体",'2'=>"企业",'3'=>"个体工商");
        $this->acctypearr = array('1'=>"CPC",'2'=>"CPM",'3'=>"CPA");
        $this->load->model('user/AccountInfoClass');
        $this->load->model('user/TeamMembersClass');
        $this->load->model('user/UserInfoClass');
        $this->load->model('user/IncomeClass');
    }

    /**
     * 开发者首页。
     * @author wangdong
     */
        function index(){
                $this->loadHeader('收入总览', 0, 0);
                $userid = $this->userInfo['userid'];
                $accountInfo['accounttype'] = $this->userInfo['istype'];
                $accountInfo['userid'] = $userid; 
                $accountInfo['accounttypename'] = $this->accounttypearr[$this->userInfo['istype']];
                $where['userid'] = $userid;

                $where['month'] = $this->get_last_month();
                $last_where = $where; 
                //上个月结算信息
                $last_where['month'] = $this->get_last_month();
                $income_list = $this->IncomeClass->getIncomeInfo($last_where);
                //上上月结算信息
                $last_where2 = $where;
                $last_where2['month'] = $this->get_last_month(2);
                $income_list2 = $this->IncomeClass->getIncomeInfo($last_where2);
                
                $data['sum_cincome'] = $this->IncomeClass->getSumIncome($userid,$this->userInfo['istype']);
                $data['userinfo'] =  $accountInfo;
                $data['income_list'] = $income_list[0];
                $data['userratio'] = json_decode($income_list[0]['userratio']);
                $data['income_list2'] = $income_list2[0];  
                $data['last_month'] = $this->get_last_month();
                $incomeid = $income_list[0]['id'];
                $data['income_detail'] = $this->get_income_detail($incomeid);
                $data['today'] = date("d");
                $this->load->view('manage/income/index',$data);
        }
        
         /**
         * 获取月份
         */
        function get_last_month($month_num=1){
                $last_month = date("Y-m",mktime(0, 0 , 0,date("m")-$month_num,1,date("Y")));
                return $last_month;
        }
        /**
         * 结算记录
         */
        function income_list(){
                $this->loadHeader('结算记录', 0, 0);
                $userid = $this->userInfo['userid'];
                $search_year = $this->input->get('search_year');
                $start_month = $search_year?$search_year."-01":date("Y-01");
                $end_month = $search_year?$search_year."-12":date("Y-12");                
                $this->db->where('userid',$userid);
                $this->db->where('month <=',$end_month);
                $this->db->where('month >=',$start_month);
                $this->db->order_by('id',"DESC");
                $query = $this->db->get('ad_month_income');
                $income_list = array();
                 if ($query->num_rows() > 0)
                {
                    $income_list = $query->result_array();
                } 
                $data['income_list'] = $income_list;
                $now_year = date("Y");
                for($i=0;$i<5;$i++){
                        $year = $now_year-$i;
                        $years_arr[] = $year;
                }
                $data['years'] = $years_arr;
                $data['search_year'] = $search_year;
                $data['accounttype'] = $this->userInfo['istype'];
                 $this->load->view('manage/income/income_list',$data);
               
        }
        /**
         * 提款记录
         */
        function paid_list(){
                 $this->loadHeader('提款记录', 0, 0);
                $userid = $this->userInfo['userid'];
                $search_year = $this->input->get('search_year');
                $start_month = $search_year?$search_year."-01":date("Y-01");
                $end_month = $search_year?$search_year."-12":date("Y-12");
                 //列表
                $this->db->where('userid',$userid);
                $this->db->where('paidmonth <=',$end_month);
                $this->db->where('paidmonth >=',$start_month);
                 $this->db->order_by("id","DESC");
                 
                $query = $this->db->get('ad_income_paid');
                $income_list = array();
                 if ($query->num_rows() > 0)
                {
                    $income_list = $query->result_array();
                } 
               $data['income_list'] = $income_list; 
               $now_year = date("Y");
                for($i=0;$i<5;$i++){
                        $year = $now_year-$i;
                        $years_arr[] = $year;
                }
                $data['years'] = $years_arr;
                $data['search_year'] = $search_year;
                
               $this->load->view('manage/income/paid_list',$data);
               
        }
           /**
         * 用户账号认证信息
         */
        function get_user_account($userid=''){
                $account_where = array('userid'=>$userid,'status'=>1,'auditstatus'=>1,'accounttype'=>$this->userInfo['istype']);  //结算单
                $query = $this->db->get_where('account_info' ,$account_where);
                if ($query->num_rows() > 0)
                {
                    $accountinfo = $query->row_array();
                }
                $account_type = $accountinfo['accounttype'];
                if(!empty($accountinfo)) $accountinfo['accounttypename'] = $this->accounttypearr[$account_type] ;
                return $accountinfo;
        }
        /**
         * 获取提款单详情
         */
        function paid_detail(){
                $this->loadHeader('提款单详情', 0, 0);
                $id = $this->input->get('id');
               
                $query = $this->db->get_where('ad_income_paid',array('id'=>$id));
                if ($query->num_rows() > 0)
                {
                    $paid_info = $query->row_array();
                } 
                $userid = $this->userInfo['userid'];
                $account_info = $this->get_user_account($userid);
                $paid_info['bankusername'] = $account_info['bankusername'];
                $paid_info['realname']  = $account_info['realname'];
                $paid_info['bankaccount'] = $account_info['bankaccount'];
                $paid_info['companyname'] = $account_info['companyname'];
                $paid_info['bank'] = $account_info['bank'];
                $paid_info['individualname'] = $account_info['individualname'];
                $data['paid_info'] = $paid_info;
                $income_info = array();
                $query = $this->db->get_where('ad_month_income',array('paidid'=>$paid_info['id']));
                if ($query->num_rows() > 0){
                        $income_info = $query->result_array();
                }
                $data['accounttype'] = $this->userInfo['istype']; 
                $data['income_info'] = $income_info;
                $this->load->view('manage/income/paid_detail',$data);
        }      
        /**
         * 查看结算详细
         */
        function income_detail(){
                $this->loadHeader('结算详情', 0, 0);
                $incomeid = $this->input->get('incomeid');
                
                $data['income_list'] = $this->get_income_detail($incomeid);
                $query = $this->db->get_where('ad_month_income' ,array('id'=>$incomeid));
                if ($query->num_rows() > 0){
                    $income_info = $query->row_array();
                }
                $data['userratio'] = json_decode($income_info['userratio']); 
                $data['accounttype'] = $this->userInfo['istype'];
                $this->load->view('manage/income/income_detail',$data);
         }
         /**
          * 结算单详细
          */
         function get_income_detail($incomeid=0){
                $income_list = array();
                $query = $this->db->get_where('ad_month_income',array('id'=>$incomeid));
                $income_info = $query->row_array();
                if(!empty($income_info)){
                        $incomeid_arr = explode(',',$income_info['incomeid']);

                        $this->db->where_in('id',$incomeid_arr);
                        $query = $this->db->get('ad_month_app_income');

                         if ($query->num_rows() > 0)
                        {
                            $income_list = $query->result_array();
                        } 
                        foreach($income_list as $key=>$val){
                                $appid = $val['pkCanalId'];
                                $query = $this->db->get_where('app_info',array('appid'=>$appid));
                                $appinfo = $query->row_array();
                                $income_list[$key]['appname'] = $appinfo['appname'];
                                $acctype = $val['accType']; 
                                $income_list[$key]['acctypename'] = $this->acctypearr[$acctype];
                                $income_list[$key]['appid'] = $appid;
                                $uidClick = $val['uidClick']?$val['uidClick']:1;
                                $income_list[$key]['avgincome'] = $val['cincome']/$uidClick;
                        }
                }        
                return $income_list;
         }
         /**
          * 企业 提现 页面
          */
         function withdraw(){
                $this->loadHeader('企业提款申请', 0, 0);
                $userid = $this->userInfo['userid'];
                $data['userid'] = $userid;
                $data['username'] = $this->userInfo['username'];
                $data['incomekey'] =  substr(md5($data['username']),0,2).time().rand(10000,99999);
                $data['sum_income'] = $this->IncomeClass->getSumIncome($userid,$this->userInfo['istype']);      //税前收入
                if($data['sum_income'] == 0){
                        // echo "<script > alert('结算余额为零，暂不能提现');</script>";
                        // echo "<script >  history.back();</script>";
                        redirect('/manage/income/index', 'refresh');
                }
                //结算明细
                $where['status'] = '0';
                $where['userid'] = $userid;
                $query = $this->db->get_where('ad_month_income' ,$where);
                if ($query->num_rows() > 0)
                {
                    $income_list = $query->result_array();
                } 
                $data['income_list'] = $income_list;
                $account_where = array('userid'=>$userid,'status'=>1,'auditstatus'=>1,'accounttype'=>$this->userInfo['istype']);  //结算单
                $query = $this->db->get_where('account_info' ,$account_where);
                if ($query->num_rows() > 0)
                {
                    $account_info = $query->row_array();
                }
                $data['accounttype'] = $this->userInfo['istype'];
                $data['account_info'] = $account_info;
                $this->load->view('manage/income/withdraw',$data);
           }
         /**
          * 生成结算单
          */
         function create_paid(){
                $userid = $this->userInfo['userid'];
                $data['userid'] = $userid;
                $data['username'] =  $this->userInfo['username'];   //账号
                $data['incomekey'] =  substr(md5($data['username']),0,2).time().rand(10000,99999);
                $data['sumcincome'] = $this->IncomeClass->getSumIncome($userid,$this->userInfo['istype']);
                $data['aftertaxmoney'] =  $data['sumcincome'];

               $data['cashmonth'] =implode(",", $this->get_income_month());              
               $data['addtime'] = time();
               $data['paidmonth'] =  date("Y-m");   
               $data['status'] = 3;
               $this->db->insert('ad_income_paid',$data); 
               $id = $this->db->insert_id();
               if($id){  
                        $this->db->where('userid', $userid);
                        $this->db->where('status', 0);
                        $this->db->update('ad_month_income', array('status'=>1,'paidid'=>$id)); 
                        redirect('/manage/paidprint/paid_detail?id='.$id);
               }             
         }
          
          /**
         * 取得结算记录的月份、对应的 结算单的id
         */
        function get_income_month(){
                 $userid = $this->userInfo['userid'];
                $where = array('userid'=>$userid,'status'=>'0');
                $this->db->select('month');
                $query = $this->db->get_where('ad_month_income' ,$where);
                if ($query->num_rows() > 0)
                {
                    $income_list = $query->result_array();
                } 
                foreach($income_list as $val){
                        $cashmonth[] = $val['month'];
                }
                return $cashmonth;
        }
}

<?php
/**
 * 应用管理类
 * @filename	: appinfo.php
 * @author		: wangdong
 * @datetime	: 2013-10-15
 * @Description  : 前台App管理页面。
 */

class UserInfo extends MY_Controller{
    private  $userid = null;
    private $urlmatch='/^(http:\/\/)?(https:\/\/)?([\w\d-]+\.)+[\w-]+(\/[\d\w-.\/\!\+?%&=]*)?$/Ui';
    private $currencyList = array(1=>"人民币", 2=>"美元");
    public function __construct()
    {
        parent::__construct();
        header("Cache-control:no-cache,no-store,must-revalidate");
        header("Pragma:no-cache");
        header("Expires:0");
        $this->load->model('user/AccountInfoClass');
        $this->load->model('user/TeamMembersClass');
        $this->load->model('user/UserInfoClass');
        $this->load->library('session');
        $this->load->helper('cookie');
    }

    /**
     * 开发者首页。
     * @author wangdong
     */
    function index(){
        $this->loadHeader('基本信息管理', 0, 0);
        $where['status'] = 1;
        $where['accounttype'] = $this->userInfo['istype'];
        $where['auditstatus'] = array(0, 1, 2);
        $where['userid'] = $this->userInfo['userid'];
        $accountInfo = $this->AccountInfoClass->getAccountByStatus($where);
        $accountInfo['secretKey'] = $this->UserInfoClass->getSecretKey($this->userInfo['userid']);
        $this->load->view('manage/user/userdetail', $accountInfo);
    }

    /**
     * 开发者首页。
     * @author wangdong
     */
    function showUserInfo(){
        $this->loadHeader('基本信息管理', 0, 0);
        $where['status'] = 1;
        $where['accounttype'] = $this->userInfo['istype'];
        $where['auditstatus'] = array(0, 1, 2);
        $where['userid'] = $this->userInfo['userid'];
        $accountInfo = $this->AccountInfoClass->getAccountByStatus($where);
        $accountInfo['secretKey'] = $this->UserInfoClass->getSecretKey($this->userInfo['userid']);
        $this->load->view('manage/user/showuserinfo', $accountInfo);
    }

    /**
     * 开发者首页。
     * @author wangdong
     */
    function showFinanceInfo(){
        $this->loadHeader('收款信息管理', 0, 0);
        $where['status'] = 1;
        $where['accounttype'] = $this->userInfo['istype'];
        $where['auditstatus'] = array(0, 1, 2);
        $where['userid'] = $this->userInfo['userid'];
        $accountInfo = $this->AccountInfoClass->getAccountByStatus($where);
        $accountInfo['currency'] = $this->currencyList[$accountInfo['currency']];
        $this->load->view('manage/user/showfinanceinfo', $accountInfo);
    }

    /**
     * 开发者首页。
     * @author wangdong
     */
    function showFinanceDetail(){
        $this->loadHeader('收款信息管理', 0, 0);
        $where['status'] = 1;
        $where['accounttype'] = $this->userInfo['istype'];
        $where['auditstatus'] = array(0, 1, 2);
        $where['userid'] = $this->userInfo['userid'];
        $accountInfo = $this->AccountInfoClass->getAccountByStatus($where);
        $accountInfo['currency'] = $this->currencyList[$accountInfo['currency']];
        $this->load->view('manage/user/showfinancedetail', $accountInfo);
    }

    /**
     * 开发者信息。
     * @author wangdong
     */
    function showAccountInfo(){
        $this->loadHeader('身份验证', 0, 0);
        $where['status'] = 1;
        $where['accounttype'] = $this->userInfo['istype'];
        $where['userid'] = $this->userInfo['userid'];
        $accountInfo['userinfo'] = $this->AccountInfoClass->getAccountByStatus($where);
    	$accounthistory = $this->grouphistory();
        if($accounthistory != false && count($accounthistory) > 1 && $accounthistory[0]['tag'] != 3 && $accounthistory[0]['createtime'] > $accountInfo['userinfo']['createtime'] && $accounthistory[0]['auditstatus'] != 1){
            $accountInfo['updatauserinfo']=$accounthistory[0];
        }
        if($accountInfo['userinfo']['accounttype'] == 1)
        {
            //个人开发者
            $accountInfo['memberslist'] = $this->TeamMembersClass->getMemberList($this->userInfo['userid']);
            $this->load->view('manage/user/groupinfo', $accountInfo);
        }
        elseif($accountInfo['userinfo']['accounttype'] == 2)
        {
            //企业公司
            $this->load->view('manage/user/companyinfo', $accountInfo);
        }
        elseif($accountInfo['userinfo']['accounttype'] == 3)
        {
            //个体工商户
            $this->load->view('manage/user/ownerinfo', $accountInfo);
        }
    }

    /**
     * 个体升级
     */

    function showUpdateAccount()
    {
        $this->loadHeader('身份变更', 0, 0);
        $where['status'] = 1;
        $where['accounttype'] = $this->userInfo['istype'];
        $where['userid'] = $this->userInfo['userid'];
        $accountInfo['userinfo'] = $this->AccountInfoClass->getAccountByStatus($where);
        if(empty($accountInfo['userinfo'])||$accountInfo['userinfo']['auditstatus'] != 1)
        {
            redirect('/manage/userinfo/showaccountinfo');
            return;
        }
		$accounthistory = $this->AccountInfoClass->getAccountHistory($this->userInfo['userid']);
		if($accounthistory != false && count($accounthistory) > 1 && ($accounthistory[0]['tag'] == 2 || $accounthistory[0]['tag'] == 4)&& $accounthistory[0]['createtime'] > $accountInfo['userinfo']['createtime'] && $accounthistory[0]['auditstatus'] == 0)
		{
            redirect('/manage/userinfo/showaccountinfo');
            return;
        }
        $this->load->view('manage/user/showupdateaccount',$where);
    }

    /**
     * 团体成员添加。
     * @author wangdong
     */
    function showAddMember(){
        $this->loadHeader('添加团队成员', 0, 0);
        $this->load->view('manage/user/showaddmember');
    }

    /**
     * 团体成员修改。
     * @author wangdong
     */
    function showEditMember(){
        $this->loadHeader('修改团队成员信息', 0, 0);
        $id = $this->input->get('memberid');
        $info['memberinfo'] = $this->TeamMembersClass->getMemberInfo($id, $this->userInfo['userid']);
        $this->load->view('manage/user/showeditmember', $info);
    }
    
	/**
     * 用户身份信息修改。
     * @author wangfei
     */
    function showEditGroup(){
        $this->loadHeader('修改用户身份信息', 0, 0);
        $userid = $this->input->get('userid');
        
        $id=$this->input->get('id');
        $where['id'] =  $id;
        $info['accountinfo'] = $this->AccountInfoClass->getAccountByWhere($where);
        $accounttype= $info['accountinfo']['accounttype'];
        switch ($accounttype) {
        	case 1:
        		$this->load->view('manage/user/showeditgroup', $info);
        		break;	
        	case 2:
        		$this->load->view('manage/user/showeditcompany', $info);
        		break;	
        	case 3:
	        	$this->load->view('manage/user/showeditowner', $info);
	        	break;	
        	default:
        		$this->load->view('manage/user/showeditgroup', $info);;
        	break;
        }
//        $this->load->view('manage/user/showeditgroup', $info);
    }

    /**
     * 显示密码管理页。
     * @author wangdong
     */
    function showEditpwd(){
        $this->loadHeader('密码管理', 0, 0);
        $this->load->view('manage/user/showeditpwd');
    }

    /**
     * 编辑密码。
     * @author wangdong
     */
    function doEditPWD(){
        $postData = $this->input->post();
        $info = $this->UserInfoClass->doEditPWD($postData, $this->userInfo);
        if($info['status'] == 0)
        {
            ajaxReturn($info['info'], 0, $info['data']);
        }
        ajaxReturn('', 1, '');
    }

    /**
     * 删除。
     * @author wangdong
     */
    function doDelete(){
        $postData = $this->input->post();
        $info = $this->TeamMembersClass->doDelete($postData, $this->userInfo);
        if($info['status'] == 0)
        {
            ajaxReturn($info['info'], 0, $info['data']);
        }
        ajaxReturn('', 1, '');
    }

    /**
     * 调整分成比例。
     * @author wangdong
     */
    function doEditRatio(){
        $postData = $this->input->post();
        unset($postData['r']);
        $info = $this->TeamMembersClass->doEditRatio($postData, $this->userInfo);
        if($info['status'] == 0)
        {
            ajaxReturn($info['info'], 0, $info['data']);
        }
        ajaxReturn('', 1, '');
    }

    /**
     * 编辑基本信息。
     * @author wangdong
     */
    function doEditContact(){
        $postData = $this->input->post();
        $info = $this->AccountInfoClass->doEditContact($postData, $this->userInfo);
        if($info['status'] == 0)
        {
            ajaxReturn($info['info'], 0, $info['data']);
        }
        ajaxReturn('', 1, '');
    }

    /**
     * 账户身份变更。
     * @author wangdong
     */
    function doUpdateAccount(){        
        $postData = $this->input->post();
        $where['status'] = 1;
        $where['accounttype'] = $this->userInfo['istype'];
        $where['userid'] = $this->userInfo['userid'];
        $accountInfo = $this->AccountInfoClass->getAccountByStatus($where);
        $postData['currency']=$accountInfo['currency'];
        if($where['accounttype'] == 1 || ($where['accounttype'] == 3 && isset($postData['accounttype']) && $postData['accounttype'] == 2)){
        	 $info = $this->AccountInfoClass->doUpdateAccount($postData, $this->userInfo);
        }
        elseif($where['accounttype'] == 2 || ($where['accounttype'] == 3 && isset($postData['accounttype']) && $postData['accounttype'] == 1)){
        	$info = $this->AccountInfoClass->doDownGradeAccount($postData, $this->userInfo);
        }      
        if($info['status'] == 0)
        {
            ajaxReturn($info['info'], 0, $info['data']);
        }
        ajaxReturn('', 1, '');
    }

    /**
     * 用户身份变更内容编辑接口
     * @author wangdong
     */
    function doEditUpAccount(){
        $postData = $this->input->post();  
        $info = $this->AccountInfoClass->doEditUpAccount($postData, $this->userInfo);
        if($info['status'] == 0)
        {
            ajaxReturn($info['info'], 0, $info['data']);
        }
        ajaxReturn('', 1, '');
    }

    
    /**
     * 取消身份升级。
     * @author wangdong
     */
    function doUpdateCancel(){
        $postData = $this->input->post();
        $info = $this->AccountInfoClass->doUpdateCancel($this->userInfo);
        if($info['status'] == 0)
        {
            ajaxReturn($info['info'], 0, $info['data']);
        }
        ajaxReturn('', 1, '');
    }

    /**
     * 编辑收款账号信息。
     * @author wangdong
     */
    function doEditFinance(){
        $postData = $this->input->post();
        $info = $this->AccountInfoClass->doEditFinance($postData, $this->userInfo);
        if($info['status'] == 0)
        {
            ajaxReturn($info['info'], 0, $info['data']);
        }
        ajaxReturn('', 1, '');
    }

    /**
     * 添加团队成员。
     * @author wangdong
     */
    function doAddMember(){
        $postData = $this->input->post();
        $info = $this->TeamMembersClass->doAddMember($postData, $this->userInfo);
        if($info['status'] == 0)
        {
            ajaxReturn($info['info'], 0, $info['data']);
        }
        ajaxReturn('', 1, '');
    }

    /**
     * 添加团队成员。
     * @author wangdong
     */
    function doEditMember(){
        $postData = $this->input->post();
        $info = $this->TeamMembersClass->doEditMember($postData, $this->userInfo);

        if(!$info['status'])
        {
            ajaxReturn($info['info'], 0, $info['data']);
        }
        ajaxReturn('', 1, '');
    }
    
 	/**
     * 修改个人用户身份认证信息
     * @author wangfei
     * 2015-03-20
     */
    function doEditGroup(){
    	$postData = $this->input->post();
        $info=$this->AccountInfoClass->doEditGroup($postData,$this->userInfo);
        
        if($info['status']==0){
            ajaxReturn($info['info'], 0, $info['data']);
        }
        ajaxReturn('', 1, '');
    }
    
 	/**
     * 修改企业身份认证信息
     * @author wangfei
     * 2015-04-17
     */   
    function doEidtCompany(){
    	$postData = $this->input->post();
        $info=$this->AccountInfoClass->doEditCompany($postData,$this->userInfo);
        
        if($info['status']==0){
            ajaxReturn($info['info'], 0, $info['data']);
        }
        ajaxReturn('', 1, '');
    }
    
 	/**
     * 修改个人工商身份认证信息
     * @author wangfei
     * 2015-04-17
     */   
    function doEidtOwner(){
    	$postData = $this->input->post();
        $info=$this->AccountInfoClass->doEditOwner($postData,$this->userInfo);
        
        if($info['status']==0){
            ajaxReturn($info['info'], 0, $info['data']);
        }
        ajaxReturn('', 1, '');
    }
    
    
     /**
     * 身份降级
     */

    function showDownGradeAccount()
    {
        $this->loadHeader('身份降级', 0, 0);
        $where['status'] = 1;
        $where['accounttype'] = $this->userInfo['istype'];
        $where['userid'] = $this->userInfo['userid'];
        $accountInfo['userinfo'] = $this->AccountInfoClass->getAccountByStatus($where);
        $currencys=$accountInfo['userinfo']['currency']; 
        if(empty($accountInfo['userinfo'])||$accountInfo['userinfo']['auditstatus'] != 1)
        {
            redirect('/manage/userinfo/showaccountinfo');
            return;
        }
        $accounthistory = $this->AccountInfoClass->getAccountHistory($this->userInfo['userid']);
		if($accounthistory != false && count($accounthistory) > 1 && $accounthistory[0]['tag'] == 4 && $accounthistory[0]['createtime'] > $accountInfo['userinfo']['createtime'] && $accounthistory[0]['auditstatus'] == 0)
		{
            redirect('/manage/userinfo/showaccountinfo');
            return;
        }
        $this->load->view('manage/user/showdowngradeaccount',$currencys);
    }
    
    /**
     *  身份降级。
     * @author wf
     */
    function doDownGradeAccount(){
        $postData = $this->input->post();
        $where['status'] = 1;
        $where['accounttype'] = $this->userInfo['istype'];
        $where['userid'] = $this->userInfo['userid'];
        $accountInfo = $this->AccountInfoClass->getAccountByStatus($where);
        $postData['currency']=$accountInfo['currency'];
        $info = $this->AccountInfoClass->doDownGradeAccount($postData, $this->userInfo);
        if($info['status'] == 0)
        {
            ajaxReturn($info['info'], 0, $info['data']);
        }
        ajaxReturn('', 1, '');
    }
    
    function grouphistory(){
        $userid = $this->userInfo['userid'];
        $accountlist = $this->AccountInfoClass->getAccountHistory($userid);
        return $accountlist;
    }    
}
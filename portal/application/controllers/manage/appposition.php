<?php
/**
 * 应用管理类
 * @filename	: appinfo.php
 * @author		: wangdong
 * @datetime	: 2013-10-15
 * @Description  : 前台App管理页面。
 */

class AppPosition extends MY_Controller{
    private  $userid = null;
    public function __construct()
    {
        parent::__construct();
        header("Cache-control:no-cache,no-store,must-revalidate");
        header("Pragma:no-cache");
        header("Expires:0");
        $this->load->model('manage/AppPositionClass');
        $this->load->model('manage/AppInfoClass');
        $this->load->model('manage/AdBorderClass');
    }

    /**
     * 添加应用广告位。
     * @author wangdong
     */
    function showAdd(){
        $appId = $this->input->get('appid');
        $positionId = $this->input->get('positionid');
        if(empty($appId))
        {
            redirect("/manage/appinfo/showlist");
            exit;
        }
        $data = $this->AppInfoClass->getDetailInfo($this->userInfo, $appId);
        $data['positionList'] = $this->AppPositionClass->getList(array('appid'=>$appId, 'userid' => $this->userInfo['userid'], 'status' => 0));
        $data['positionid'] = $this->AppPositionClass->createPositionId($appId);
        $data['positionInfo'] = '';
        if($data['positionList'])
        {
            foreach ($data['positionList'] as $v)
            {
                if($v['positionid'] == $positionId && !empty($v['positionid']))
                {
                    $data['positionInfo'] = $v;
                    break;
                }
            }
        }
        $data['appid'] = $appId;
        $data['sysBorderList'] = $this->AdBorderClass->getSysBorderList();
        $this->loadHeader('申请广告位', 2, 21);
        $this->load->view('manage/appposition/showadd', $data);
    }


    /**
     * 添加应用广告位。
     * @author wangdong
     */
    function showInfo(){
        $appId = $this->input->get('appid');
        $positionId = $this->input->get('positionid');
        if(empty($appId))
        {
            redirect("/manage/appinfo/showlist");
            exit;
        }
        $data = $this->AppInfoClass->getDetailInfo($this->userInfo, $appId);
        $data['positionList'] = $this->AppPositionClass->getList(array('appid'=>$appId,  'status' => 0));//'userid' => $this->userInfo['userid'],
        $data['positionInfo'] = '';
        if($data['positionList'])
        {
            foreach ($data['positionList'] as $v)
            {
                if($v['positionid'] == $positionId && !empty($v['positionid']))
                {
                    $data['positionInfo'] = $v;
        			$borderImg = $this->AdBorderClass->getBorderList(array('bordergroupid' => $v['bordergroupid'], 'type' => 2, 'status' => 1));
        			$borderImgList=array();
        			foreach($borderImg as $v)
        			{
        				if(!empty($v['sizeid'])){
        					$borderImgList[$v['sizeid']]=$v;
        				}
        				else{
        					$borderImgList['icon']=$v;
        				}
        			}
        			$data['borderImgList']=$borderImgList;
                    break;
                }
            }
        }
        if($data['positionInfo']!=''){
        	$data['myBorderList']=$this->AdBorderClass->getBorderList(array('bordergroupid' => $data['positionInfo']['bordergroupid']));
	        $data['sysBorDelFlag']=false;
	        if($data['myBorderList']!='' && $data['myBorderList']!=null){
	        	if($data['myBorderList'][0]['status']==2){
	        		$data['sysBorDelFlag']=true;
	        	}
	        }
        }    
        $data['appid'] = $appId;
        $data['leftlist'] = $this->AppInfoClass->getAppInfoList('', $this->userInfo);
        $data['positionid'] = $this->AppPositionClass->createPositionId($appId);
        $data['sysBorderList'] = $this->AdBorderClass->getSysBorderList();
        $this->loadHeader('申请广告位', 2, 21);
        $this->load->view('manage/appposition/showinfo', $data);
    }

    /**
     *  添加应用广告位。
     * @author wangdong
     */
    function doAdd(){
        $postData = $this->input->post();
        $positionInfo = $this->AppPositionClass->doAdd($postData, $this->userInfo);
        if($positionInfo['status'] == 0)
        {
            ajaxReturn($positionInfo['info'], 0, $positionInfo['data']);
        }
        ajaxReturn('', 1, '');
    }

    /**
     *  删除应用广告位。
     * @author wangdong
     */
    function doDel(){
        $appId = $this->input->get('appid');
        $bordergroupid = $this->input->get('bordergroupid');
        $positionId = sget('positionid');
        if(empty($appId) || empty($positionId))
        {
            redirect("/manage/appinfo/showlist");
            exit;
        }
        $positionInfo = $this->AppPositionClass->doDel($this->input->get(), $this->userInfo);
        redirect("/manage/appposition/showinfo/?appid=".$appId);
    }

    /**
     *  删除应用广告位。
     * @author wangdong
     */
    function ajaxDoDel(){
        $appId = $this->input->get('appid');
        $positionId = sget('positionid');
        if(empty($appId) || empty($positionId))
        {
            ajaxReturn('', 0, '');
        }
        $positionInfo = $this->AppPositionClass->doDel($this->input->get(), $this->userInfo);
        ajaxReturn('', 1, '');
    }

    /**
     *  编辑应用广告位。
     * @author wangdong
     */
    function doEdit(){
        $postData = $this->input->post();
        $positionInfo = $this->AppPositionClass->doEdit($postData, $this->userInfo);
        if($positionInfo['status'] == 0)
        {
            ajaxReturn($positionInfo['info'], 0, $positionInfo['data']);
        }
        ajaxReturn('', 1, '');
    }
    

    
	/**
     * 上传边框
     */
    public function ajaxDoUploadStuff()
    {
    	$returnArray = array('isReceived' => false, 'errorMessage' => '', 'imgInfo' => '', 'imgs_id' => null);
        if ($_FILES['Filedata']['size'] > 120 * 1024) {
            $returnArray['errorMessage'] = '图片大小超过120kb';
            echo json_encode(array($returnArray));
            return;
        }
        $type = $this->input->get_post('type');
        
        $returnArray = $this->AppPositionClass->upload($type);

        if ($returnArray['isReceived']) {
            $returnArray['imgs_id'] = $type;
        }

        echo json_encode(array($returnArray));
        return;
    }
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: chukong
 * Date: 3/1/16
 * Time: 5:03 PM
 */

/**
 * 网站登陆注册入口
 *
 * @author wd
 *
 */
class Debug extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->helper("tools");
        $this->load->helper(array('form', 'url'));
        $this->load->model('/user/UserInfoClass');
        header("Content-type: text/html; charset=utf-8");
    }

    function index()
    {
        $postData = $this->input->post();
        if (!isset($postData['url']))
        {
            echo '<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>CocosAds Debug</title>
<style>
html,body{
	height:100%;
}
</style>
</head>

<body>
<form action="" method="post" target="bodyframe">
<label for="url">Url:</label>
<input name="url" type="url" id="url" size="100"><input type="submit">
</form>
<br>
<iframe name="bodyframe" width="100%" height="90%"></iframe>
</body>
</html>';
        }
        else
        {
            $resultstr = curlGet($postData['url']);
            if ($resultstr=='')
            {
                echo '无法连接服务器';
                return;
            }
            $result = json_decode($resultstr);
            if (isset($result) && $result->result)
            {
                echo $result->datas[0]->body;
            }
            else
            {
                echo $resultstr;
            }
        }

    }
}
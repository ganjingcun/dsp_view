<?php
/**
 * 推广计划管理类
 * @filename	: ads.php
 * @author		: funbox
 * @datetime	: 2016-10-31
 * @Description  : dsp系统临时前台广告管理
 */

class Campaign extends MY_Controller{
    private  $userid = null;
    public function __construct()
    {
        parent::__construct();
        $this->output->set_header("Cache-control:private;");
        $this->load->model('manage/AuthClass');
		$this->load->model('dsp_manage/AdCampaignClass');
        $this->userid =  AuthClass::getUserId();
    }

	function showList(){
		$this->loadHeader('推广计划管理', 2, 22);
		$where['status_in'] = array(1,2,3,4,5);
		$data['data'] = $this->AdCampaignClass->getList($where, $this->userInfo);

		$this->load->view('dsp_manage/campaign/showlist', $data);
	}

	function showAdd(){
		$this->loadHeader('创建推广计划', 2, 21);
		$this->load->view('dsp_manage/campaign/showadd');

	}

	function doAdd() {

		if ($this->is_post() === TRUE) {

			$data = $_POST;
			$data['status'] = 1;//待投放
			$data['selfappset'] = 0;//正常广告
			$data['daybudget'] = formatmoney($data['daybudget'],'set');
			$data['userid'] = $this->userid;
			$data['starttime'] = strtotime($data['starttime']);
			$data['endtime'] = strtotime($data['endtime']);
			$data['createtime'] = time();
			$data['lastupdatetime'] = time();
			$res = $this->AdCampaignClass->add($data);

			if($res)
			{
				ajaxReturn('', 1, array($res, ''));
			}
			ajaxReturn($info, 0, $data);

		}
	}

	function showEdit(){
		$this->loadHeader('修改推广计划', 2, 21);

		$campaignid = intval($_GET['campaignid']);
		$where['campaignid'] = $campaignid;
		$data['campaign'] = $this->AdCampaignClass->getInfo($where, $this->userInfo);
		$this->load->view('dsp_manage/campaign/showedit',$data);

	}

	function doEdit() {

		if ($this->is_post() === TRUE) {

			$data = $_POST;

			$where['campaignid'] = $data['campaignid'];
			$where['userid'] = $this->userid;

			$data['daybudget'] = formatmoney($data['daybudget'],'set');
			$data['starttime'] = strtotime($data['starttime']);
			$data['endtime'] = strtotime($data['endtime']);
			$data['lastupdatetime'] = time();
			unset($data['campaignid']);
			unset($data['userid']);

			$res = $this->AdCampaignClass->save($data, $where);

			if($res)
			{
				$this->load->driver('cache', array('adapter' => 'redis'));
				$this->cache->save('ADINFODATEMARK', date('YmdH',time()-3600), 0);
				ajaxReturn('', 1, array($res, ''));
			}
			ajaxReturn($info, 0, $data);

		}
	}
}
<?php
/**
 * 广告管理类
 * @filename	: ads.php
 * @author		: funbox
 * @datetime	: 2016-10-31
 * @Description  : dsp系统临时前台广告管理
 */

class Ads extends MY_Controller{
    private  $userid = null;
    public function __construct()
    {
        parent::__construct();
        $this->output->set_header("Cache-control:private;");
        $this->load->model('manage/AuthClass');
		$this->load->model('dsp_manage/AdGroupClass');
		$this->load->model('dsp_manage/AdCampaignClass');
        $this->userid =  AuthClass::getUserId();
    }

	/**
	 * 广告列表
	 * @author funbox
	 */
	function showList(){

		$this->loadHeader('广告管理', 2, 22);
		$where['status_in'] = array(0,1,2);
		$data['data'] = $this->AdGroupClass->getAdsList($where, $this->userInfo);

		$this->load->view('dsp_manage/ads/showlist', $data);
	}

	/**
	 * 添加广告
	 * @author funbox
	 */
	function showAdd(){
		$where['status_in'] = array(1,2,3,4,5);
		$data['campaign'] = $this->AdCampaignClass->getList($where, $this->userInfo);

		$this->loadHeader('创建广告', 2, 21);
		$this->load->view('dsp_manage/ads/showadd',$data);

	}
	/**
	 * 提交广告
	 * @author funbox
	 */
	function doAdd() {

		if ($this->is_post() === TRUE) {

			$data = $_POST;
			$data['status'] = 1;//有效
			$data['price'] = formatmoney($data['price'],'set');
			$data['daybudget'] = formatmoney($data['daybudget'],'set');
			$data['defaultbudget'] = @formatmoney($data['defaultbudget'],'set');
			$data['userid'] = $this->userid;
			$data['starttime'] = strtotime($data['starttime']);
			$data['endtime'] = strtotime($data['endtime']);
			$data['createtime'] = time();
			$data['lastupdatetime'] = time();
			$res = $this->AdGroupClass->add($data);

			if($res)
			{
				ajaxReturn('', 1, array($res, ''));
			}
			ajaxReturn($info, 0, $data);

		}
	}

	/**
	 * 修改广告
	 * @author funbox
	 */
	function showEdit(){
		$this->loadHeader('修改广告', 2, 21);

		$where['status_in'] = array(1,2,3,4,5);
		$data['campaign'] = $this->AdCampaignClass->getList($where, $this->userInfo);

		$adgroupid = intval($_GET['adgroupid']);
		$data['ads'] = $this->AdGroupClass->getAdsInfo($adgroupid, $this->userInfo);

		$this->load->view('dsp_manage/ads/showedit',$data);

	}
	/**
	 * 修改提交广告
	 * @author funbox
	 */
	function doEdit() {

		if ($this->is_post() === TRUE) {

			$data = $_POST;

			$where['adgroupid'] = $data['adgroupid'];
			$where['userid'] = $this->userid;

			$data['daybudget'] = formatmoney($data['daybudget'],'set');
			$data['price'] = formatmoney($data['price'],'set');
			$data['starttime'] = strtotime($data['starttime']);
			$data['endtime'] = strtotime($data['endtime']);
			$data['lastupdatetime'] = time();
			unset($data['adgroupid']);
			unset($data['userid']);

			$res = $this->AdGroupClass->save($data, $where);

			if($res)
			{
				$this->load->driver('cache', array('adapter' => 'redis'));
				$this->cache->save('ADINFODATEMARK', date('YmdH',time()-3600), 0);
				ajaxReturn('', 1, array($res, ''));
			}
			ajaxReturn($info, 0, $data);

		}
	}

	/**
	 * 广告创意列表
	 * @author funbox
	 */
	function stuffList(){
		$adgroupid = intval($_GET['adgroupid']);

		$this->loadHeader('广告创意', 2, 22);
		$data['ads'] = $this->AdGroupClass->getAdsInfo($adgroupid, $this->userInfo);

		$where['adgroupid'] = $adgroupid;
		$where['status_in'] = array(0,1,2);
		$this->load->model('dsp_manage/AdStuffClass');
		$data['stuff'] = $this->AdStuffClass->getList($where, $this->userInfo);
		if (!empty($data['stuff'])) {
			foreach ($data['stuff'] as $item) {
				$stuffids[] = $item['stuffid'];
			}
		}

		if (!empty($stuffids)) {
			$where = array();
			$where['stuffid_in'] = $stuffids;
			$where['status_in'] = array(1,2);
			$this->load->model('dsp_manage/AdStuffImgClass');
			$stuff_img = $this->AdStuffImgClass->getList($where, $this->userInfo);
			if (!empty($stuff_img)) {
				foreach ($stuff_img as $item) {
					$data['stuff_imgs'][$item['stuffid']][] = $item;
				}
			}
		}

		$this->load->view('dsp_manage/ads/stufflist', $data);
	}

	/**
	 * 添加广告创意
	 * @author funbox
	 */
	function addStuff(){

		$data['adgroupid'] = intval($_GET['adgroupid']);

		$this->loadHeader('创建广告创意', 2, 21);
		$this->load->view('dsp_manage/ads/addstuff',$data);

	}
	/**
	 * 提交广告创意
	 * @author funbox
	 */
	function doAddStuff() {

		if ($this->is_post() === TRUE) {
			$this->load->model('dsp_manage/AdStuffClass');

			$adgroupid = intval($_POST['adgroupid']);
			$ads = $this->AdGroupClass->getAdsInfo($adgroupid, $this->userInfo);

			$data = $_POST;
			$data['status'] = 1;//审核通过
			$data['campaignid'] = $ads['campaignid'];
			$data['userid'] = $this->userid;
			$data['createtime'] = time();
			$data['lastupdatetime'] = time();

			$img_array = $data['img'];
			$width_array = $data['width'];
			$height_array = $data['height'];
			unset($data['img']);
			unset($data['width']);
			unset($data['height']);

			$res = $this->AdStuffClass->add($data);

			if($res)
			{
				//图片
				$this->load->model('dsp_manage/AdStuffImgClass');
				$this->load->model('dsp_manage/AdStuffImgSizeClass');

				$data = array();

				for ($i=0;$i<count($img_array);$i++) {
					if (empty($img_array[$i])) continue;
					$data_tmp['width'] = $width_array[$i];
					$data_tmp['height'] = $height_array[$i];
					$data_tmp['version'] = 1;
					$tmp['sizeid'] = $this->AdStuffImgSizeClass->add($data_tmp);
					$tmp['stuffid'] = $res;
					$tmp['path'] = str_replace($this->config->item('pic_url'),'',$img_array[$i]);
					$data[] = $tmp;
				}
				$this->AdStuffImgClass->addBatch($data);

				$this->load->driver('cache', array('adapter' => 'redis'));
				$this->cache->save('ADINFODATEMARK', date('YmdH',time()-3600), 0);

				ajaxReturn('', 1, array($res, ''));
			}
			ajaxReturn($info, 0, $data);

		}
	}
}
<?php
/**
 * 应用管理类
 * @filename	: index.php
 * @author		: funbox
 * @datetime	: 2016-10-31
 * @Description  : dsp系统临时前台管理
 */

class Index extends MY_Controller{
    private  $userid = null;
    public function __construct()
    {
        parent::__construct();
        $this->output->set_header("Cache-control:private;");
        $this->userid =  AuthClass::getUserId();
    }

    function index(){
        $data['userinfo'] = $this->userInfo;
        $this->load->view('dsp_manage/index', $data);
    }
}
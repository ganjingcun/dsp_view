<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 联想认证回调
 */
class Auth extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    public function web(){
    	$getdata = $this->input->get();
    //	$post_data['lpsust'] = $getdata['lenovoid_wust'];
    	$post_data['lpsust'] = 'ZAgAAAAAAAGE9MTAwNjI2MTQwODMmYj0xJmM9MSZkPTEwNjAyJmU9OEI3MDYzMzA5RTI3QzMwRDEzMjI0QTJEN0Y5QjY0OTMxJmg9MTQ1MDY4NDg5MjMwMSZpPTQzMjAwJmo9MCZpbD1jbiZ1c2VybmFtZT0xMjd4JTQwcXEuY29tm3LGQvWy1QQtapAzo15IZA';
    	//$post_data['realm'] = 'ad.lenovogame.com';
    	$post_data['realm'] = 'app.lenovo.com';
    	//$userInfoPost = $this->getUserInfo('https://ad.lenovogame.com/interserver/authen/1.2/getaccountid', $post_data);
    	$userInfoPost = $this->getUserInfo('http://www.lenovomm.com/', $post_data);
    	error_log('json: '.json_encode($userInfoPost));
    	foreach($userInfoPost as $k=>$v){
    		error_log($k.": ".$v);
    	}
    //	$userInfoPost = $this->getUserInfo('http://hj.dev.cocounion.com:8080/auth/rcv', $post_data);
    	if($userInfoPost['http_code'] == 200){
	    	$userInfo = simplexml_load_string($userInfoPost['data']);
			$data['accountid'] = (string) $userInfo->AccountID;
			$data['username'] = (string)$userInfo->Username;
			$data['devicdid'] = (string)$userInfo->DevicdID;
			$data['verified'] = (int)$userInfo->verified;
			$data['thirdname'] = (string)$userInfo->Thirdname;
    		$this->db->insert('account_lenovo',$data); 
            $id = $this->db->insert_id();
    	}
    }
    
    private function getUserInfo($c_url, $c_url_data,$method='POST'){
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $c_url);
	    curl_setopt($ch,CURLOPT_USERAGENT,"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)");
	    curl_setopt($ch, CURLOPT_POST, 1);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method); //设置请求方式
	    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($c_url_data));
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    $result = curl_exec($ch);
	    
	    $http_retcode=curl_getinfo($ch, CURLINFO_HTTP_CODE);
	    $errno = curl_errno( $ch );
	 	$info  = curl_getinfo( $ch );
	 	$info['errno'] = $errno;
		error_log('info: '.json_encode($info));
		error_log('curl_multi_getcontent: '.curl_multi_getcontent($ch));
	    curl_setopt($ch, CURLOPT_POST, 0); //当前面设置set ($ch, curlopt_post, 1)时，用curl_exec  post数据后，需要重新设置CURLOPT_POST为0，否则，会有500或者403错误。
	    
	    curl_close($ch);
	    unset($ch);
	    $data['data'] = $result;
	    $data['http_code'] = $http_retcode;
	    return $data;
    }
    
    public function rcv(){
    	$postdata = $this->input->post();
    	$xmldata = '<?xml version="1.0" encoding="UTF-8"?>
    					<IdentityInfo>
						<AccountID>123456</AccountID>
						<Username>用户名</Username>
						<DevicdID>登陆所有设备ID（可选）</DevicdID>
						<verified>1</verified>
						<Thirdname>第三方IDP的名称， 可选</Thirdname>
					</IdentityInfo>';
		header("Content-Type: text/xml");
		header("Content-Length: ".strlen($xmldata));
		echo $xmldata;
		exit();
    }
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * api图片处理接口
 *
 * @author wd
 *
 */
class Img extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->helper("tools");
        $this->load->helper(array('form', 'url'));
    }

    /**
     * 图片上传.
     *
     * @author wd
     */
    public function upload()
    {
		//图片
		$this->load->model('dsp_manage/AdStuffImgClass');
		$this->load->model('dsp_manage/AdStuffImgSizeClass');

		switch ($_POST['img_type']) {
			case 1:
			default:
				$path = 'stuff';
				break;

		}
		$this->load->library('MyNewUpload',$_FILES['img']);
		$handle = $this->mynewupload;
		if ($handle->uploaded) {
			$handle->allowed = array('image/*');
			$handle->file_new_name_body = md5($_FILES['img']['name'] . uniqid());
			$handle->image_convert = 'png';
			$handle->process('../demand/public/upload/' . $path . '/img/' . date('Ym'));
			if ($handle->processed) {

				$path = str_replace('../demand/public/upload', '', $handle->file_dst_pathname);
				$img_url = $this->config->item('pic_url').$path;
				$info = array('status' => 0, 'data'=> array('img_url' => $img_url));
				echo json_encode($info);
				exit;
			}
		}
		$info = array('status' => 1, 'data'=> array(), 'error'=>$handle->error);
		echo json_encode($info);
		exit;
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Validate_code
{
	private $width;				//验证码图片宽度
	private $height;			//验证码图片高度
	private $code;				//验证码字符
	private $code_num;			//验证码字符个数
	private $image;				//验证码资源
	
	function __construct($config=array('width'=>'80','height'=>'20','code_num'=>'4'))
	{
		$this->set_config($config);
		$this->code = $this->get_code();
	}
	
	function get_validate_code()
	{
		return $this->code;
	}
	
	private function get_code()
	{
		$pool = '23456789abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';
		$code = '';
		for($i = 0;$i<$this->code_num;$i++)
		{
			$code .= substr($pool, mt_rand(0, strlen($pool) -1), 1);
		}
		return $code;
	}
	function out_img()
	{	
		$this->get_create_image();		//创建画布
		$this->set_disturb_color();		//设置干扰
		$this->out_put_text();			//向图像中输入随机字符串
		$this->out_put_image();			//生成相应格式的图片并输出
	}
	
	function set_config($config)
	{
		foreach($config as $key=>$value)
		{
			$this->$key = $value;
		}
		
	}

	private function get_create_image()
	{
		$this->image = imagecreatetruecolor($this->width, $this->height);
		$back_color	 = imagecolorallocate($this->image, mt_rand(230,255), mt_rand(230,255), mt_rand(230,255));
		@imagefill($this->image, 0, 0, $back_color);
		$border=imagecolorallocate($this->image, 2, 57, 91);
		imageRectangle($this->image,0,0,$this->width-1,$this->height-1,$border);
	}
	
	private function set_disturb_color()
	{
		//设置干扰像素，向图像中输出不同颜色的100个点
		for($i=0; $i<10; $i++)
		{
			$color = imagecolorallocate($this->image, mt_rand(0,255), mt_rand(200,255), mt_rand(180,255));
			imageline($this->image, mt_rand(2,$this->width-2), mt_rand(2,$this->height-2), mt_rand(2,$this->width-2),mt_rand(2,$this->height-2), $color);
		}
	}
	
	private function out_put_text()
	{
		//随机颜色、随机摆放、随机字符串向图像中输出
		for ($i=0;$i<$this->code_num;$i++)
		{	
			$fontcolor = imagecolorallocate($this->image, rand(0,128), rand(0,128), rand(0,128));
			$fontSize=5;
			$x = floor($this->width/$this->code_num)*$i+3;
			$y = mt_rand(0,$this->height-imagefontheight($fontSize));
			imagechar($this->image, $fontSize, $x, $y, $this->code{$i}, $fontcolor);
		}
	}
	
	private function out_put_image()
	{
		//自动检测GD支持的图像类型，并输出图像
		if(imagetypes() & IMG_GIF){          //判断生成GIF格式图像的函数是否存在
			header("Content-type: image/gif");  //发送标头信息设置MIME类型为image/gif
			imagegif($this->image);           //以GIF格式将图像输出到浏览器
		}elseif(imagetypes() & IMG_JPG){      //判断生成JPG格式图像的函数是否存在
			header("Content-type: image/jpeg"); //发送标头信息设置MIME类型为image/jpeg
			imagejpeg($this->image, "", 0.5);   //以JPEN格式将图像输出到浏览器
		}elseif(imagetypes() & IMG_PNG){     //判断生成PNG格式图像的函数是否存在
			header("Content-type: image/png");  //发送标头信息设置MIME类型为image/png
			imagepng($this->image);          //以PNG格式将图像输出到浏览器
		}elseif(imagetypes() & IMG_WBMP){   //判断生成WBMP格式图像的函数是否存在
			header("Content-type: image/vnd.wap.wbmp");   //发送标头为image/wbmp
			imagewbmp($this->image);       //以WBMP格式将图像输出到浏览器
		}else{                              //如果没有支持的图像类型
			die("PHP不支持图像创建！");    //不输出图像，输出一错误消息，并退出程序
		}
	}
	
	function __destruct(){                      //当对象结束之前销毁图像资源释放内存
 		imagedestroy($this->image);            //调用GD库中的方法销毁图像资源
	}		
}
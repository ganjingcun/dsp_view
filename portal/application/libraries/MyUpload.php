<?php


class MyUpload
{

	private $allowedTypes = array('image/jpg','image/bmp', 'image/jpe', 'image/jpeg', 'image/pjpeg', 'image/x-png','image/png','image/gif');

	private $fileExt = array('jpg','png');

	private $uploadPath = '';

	private $maxSize = 407200;//300k

	public function __construct(array $file=array())
	{
		$this->file = $file;
	}

	/**
	 * 设置文件的上传目录，绝对地址
	 * @param string $uploadPath
	 * @return null
	 */
	public function setUploadPath($uploadPath=UPLOAD_DIR)
	{
		$this->uploadPath = $uploadPath;
	}

	/**
	 * 设置文件的大小，byte单位
	 * @param integer $maxSize
	 * @return null
	 */
	public function setMaxsize($maxSize)
	{
		$this->maxSize = $maxSize;
	}

	/**
	 * 设置允许上传的文件mime
	 * @param array $allowedTypes 允许上传的文件类型数组 例如：array('image/jpg','image/bmp')
	 * @return null
	 */
	public function setAllowedTypes(array $allowedTypes)
	{
		$this->allowedTypes = $allowedTypes;
	}

	/**
	 * 设置允许上传的文件的后缀名 
	 * @param array $fileExt 例如：array('jpg','png')
	 * @return null
	 */
	public function setFileExt(array $fileExt)
	{
		$this->fileExt = $fileExt;
	}

	/**
	 * 获取文件的大小
	 * @return integer
	 */
	public function getMaxsize()
	{
		return $this->maxSize;
	}

	/**
	 * 获取允许上传的图片后缀名
	 * @param unknown_type $val
	 * @param array $array
	 * @return unknown_type
	 */
	public function getaAllowedExt()
	{
		return $this->allowedTypes;
	}


	/**
	 * 检查文件大小，是否超过了文件的大小限制
	 * @return bool
	 */
	public function isBigerThanMaxSize()
	{
		return $this->file["size"] >  $this->maxSize;
	}


	/**
	 * 检查文件后缀名是否正确
	 * @return unknown_type
	 */
	public function isAllowedTypes()
	{
		//		$fileExt = array('gif', 'jpg', 'jpeg', 'png', 'jpe');
		//		$fileExt = array('jpg');
		$ext = $this->getFileExt();

		if (in_array($ext, $this->fileExt))
		{
			if ($this->file['tmp_name'] === FALSE)
			{
				return false;
			}

		}
		else
		{
			return false;
		}

		return true;
	}

	/**
	 * 验证文件的内容类型
	 * @return bool
	 */
	public function isAllowedMime()
	{
		if(  in_array($this->file["type"],$this->allowedTypes) )
		{
			return true;
		}
	}

	/**
	 * 获取文件后缀名
	 * @return string|null
	 */
	public function getFileExt()
	{
		$x = explode('.', $this->file['name']);
		return strtolower(end($x));
	}

	/**
	 * 检查图片大小是否正确
	 * @param integer $width 被允许的宽度
	 * @param integer $height 被允许的高度
	 * @return bool
	 */
	public function isAllowedSize($width,$height)
	{
		//对于上传的文件类型，大小，尺寸等做验证。。此处暂时省略
		$fileSize 	= getimagesize($this->file['tmp_name']);
		$fileWidth 	= $fileSize[0];
		$fileHeight = $fileSize[1];
		if($fileWidth != $width or $fileHeight != $height)
		{
			return false;
		}
		return true;
	}

	/**
	 * 上传图片
	 * @param bool $isReplace 如果存在同名图片是否覆盖
	 * @return bool
	 */
	public function upload($isReplace=true)
	{
		$filename = $this->file["name"];

		if (file_exists($this->uploadPath. "/" . $filename) and $isReplace)
		{
			@unlink($this->uploadPath. "/" . $filename);
		}

		self::createDir($this->uploadPath);

		$filename = self::createKey().'.'.$this->getFileExt();

		if ( ! @copy($this->file["tmp_name"], $this->uploadPath. "/" . $filename))
		{
			if ( ! move_uploaded_file($this->file["tmp_name"], $this->uploadPath. "/" . $filename))
			{

				return false;
			}

		}
		$this->filePath = str_replace(UPLOAD_DIR,'',$this->uploadPath. "/" . $filename);
		return true;
	}
	
	/**
	 * 获取文件的真实名称
	 * @return string
	 */
	public function getRealName()
	{
		return $this->file['name'];
	}

	/**
	 * 生成一串随机数字
	 * @return string
	 */
	public static function createKey()
	{
		$randpwd = '';
		for ($i = 0; $i < 10; $i++)
		{
			$randpwd .= mt_rand(33, 500);
		}

		return md5($randpwd);
	}

	/**
	 * 返回被保存的带路径的文件名
	 * @return string
	 */
	public function getUplodedFilePath()
	{
		return @$this->filePath;
	}

	/**
	 * 循环创建目录
	 * @param string $path
	 * @return null
	 */
	public static function createDir($path){
		if(!is_readable($path)){
			self::createDir( dirname($path) );
			if(!is_file($path)) mkdir($path,0777);
		}
	}


}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 邮件发送类
 *
 * @brief 辅助类
 * @author 王栋
 * @date 2012.03.09
 * @note 详细说明及修改日志
 */
class Sendemail
{
    /**
     * 管理用户发邮件
     *
     * @param $userId
     * @param $key
     * @access public
     * @return page
     */
    function sendRegEmail($userId,$userName,$key){
        if(empty($userId)||empty($userName)||empty($key))
        {
            return false;
        }
        $url = site_url().'/webindex/activate/'.$userId.'/'.$key;
        $static_url = 'http://'.$_SERVER['HTTP_HOST'].'/public/version1.0/contact.html';

        $title = '欢迎加入CocosAds!';
        $msg = '<table border="0" cellspacing="0" cellpadding="0" width="100%" style="background:#f5f5f5; padding: 20px 0;">
        <tbody>
            <tr>
                <td>
                    <table border="0" cellspacing="0" cellpadding="0" width="750" style="margin:0 auto; font-size:14px; margin:0 auto; padding-bottom: 20px; border:1px solid #ccc; background: #fff;">
                        <tbody>
                            <tr>
                                <td>
                                    <br>
                                    <strong style="padding:0 25px; font-size:16px;">CocosAds开发者注册激活邮件</strong>
                                    <p style="padding:0 25px; line-height:24px; color:#48cfad; font-size: 24px;">尊敬的用户,感谢您注册CocosAds</p>
                                    <p style="padding:0 25px; line-height:24px;">CocosAds基于对大数据的分析和不断优化的投放策略，为应用开发者提供可观的广告收益和推广效果，帮助各类广告主实现市场活动。CocosAds，您最得力的助手。</p>

                                    <p style="padding:0 25px; line-height:24px;">请点击下方链接激活账户：</p>

                                    <p style="padding:0 25px; line-height:16px;"><a href="'.$url.'">'.$url.'</a></p>

                                    <p style="padding:0 25px; line-height:24px;">请您阅读以下说明：</p>

                                    <p style="padding:0 25px; line-height:18px;">
                                        · 如果链接无法点击，请粘贴到浏览器窗口地址栏中打开；<br>
                                        · 链接仅一次有效，成功激活后，链接失效；<br>
                                        · 激活后，该电子邮件将作为登录账号使用；<br>
                                        · 本邮件由CocosAds的邮件系统发送，请勿直接回复。<br>
                                    </p>

                                    <p style="padding:0 25px; line-height:24px;">如果您还有其他问题,我们的工作人员一直在此恭候您的到来：</p>
                                    <p style="padding:0 25px; line-height:24px;"><a href="'.$static_url.'" style="display:block; width:100px; border-radius: 3px; text-decoration: none; padding: 6px 0px; text-align: center; color: #ffffff; background: #2b99f8;">联系我们</a></p>
                                    <p style="padding:0 25px; line-height:24px; text-align: right;">CocosAds&nbsp;&nbsp;敬上</p>
                                    <div style="width:700px; border-top:1px solid #ccc; font-size:12px; color: #999; margin:0 auto;">
                                        Copyright © 2012-2013 www.cocounion.com.
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>';
			$this->mailSend($userName,$title,$msg);
    }

    /**
     * 重置密碼发邮件
     *
     * @param $userId
     * @param $key
     * @access public
     * @return page
     *
     */
    function sendResetEmail($userId,$userName,$key){
        if(empty($userId)||empty($userName)||empty($key))
        {
            return false;
        }
        $url = site_url().'/webindex/showresetpwd/'.$userId.'/'.$key;
    
        $static_url = 'http://'.$_SERVER['HTTP_HOST'].'/public/version1.0/contact.html';
        $title = 'CocosAds密码找回!';
        $msg = '
		<table border="0" cellspacing="0" cellpadding="0" width="100%" style="background:#f5f5f5; padding: 20px 0;">
        <tbody>
            <tr>
                <td>
                    <table border="0" cellspacing="0" cellpadding="0" width="750" style="margin:0 auto; font-size:14px; margin:0 auto; padding-bottom: 20px; border:1px solid #ccc; background: #fff;">
                        <tbody>
                            <tr>
                                <td>
                                    <br>
                                    <strong style="padding:0 25px; font-size:16px;">尊敬的开发者'.$userName.',感谢您对CocosAds的信任</strong>
                                    
                                    <p style="padding:0 25px; line-height:24px;">CocosAds基于对大数据的分析和不断优化的投放策略，为应用开发者提供可观的广告收益和推广效果，帮助各类广告主实现市场活动。CocosAds,您最得力的助手。</p>


                                    <p style="padding:0 25px; line-height:16px;">点击此处进行密码重置：<a href="'.$url.'" target="_blank">'.$url.'</a></p>

     

                                    <p style="padding:0 25px; line-height:24px;">如果您还有其他问题,我们的工作人员一直在此恭候您的到来：</p>
                                    <p style="padding:0 25px; line-height:24px;"><a href="'.$static_url.'" style="display:block; width:100px; border-radius: 3px; text-decoration: none; padding: 6px 0px; text-align: center; color: #ffffff; background: #2b99f8;">联系我们</a></p>
                                    <p style="padding:0 25px; line-height:24px; text-align: right;">CocosAds&nbsp;&nbsp;敬上</p>
                                    <div style="width:700px; border-top:1px solid #ccc; font-size:12px; color: #999; margin:0 auto;">
                                        Copyright © 2012-2013 www.cocounion.com.
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>';
        $this->mailSend($userName,$title,$msg);
    }
    /**
     * 邀请用户发邮件
     *
     * @param $userId
     * @param $userName
     * @param $passWord
     * @param $key
     * @access public
     * @return page
     */
    function sendInviteEmail($userId,$userName,$passWord,$key){
        if(empty($userName))
        {
            return false;
        }
        $url = site_url().'/webindex/activate/'.$userId.'/'.$key;
        $title = '欢迎加入CocosAds!';
        $msg = "您好，很荣幸您能加入CocosAds，成为我们的会员。<br>
			（您的用户ID是：$userName ，初始密码是：$passWord ，账户激活后可进行修改设定。）
			点击下面的链接即可激活您的账号。<br>
			<a href='$url' target='_blank'>$url</a><br>
			如果您点击激活过程中出现意外，请复制下面的链接并粘贴到浏览器中也可以完成激活。<br>
			$url
			<br><br><br>
			该激活链接的有效期为60天，请在有效期内完成激活。
			<br>
			感谢您选择CocosAds，我们期待为您提供最好的移动广告相关解决方案。
			<br>
			<br>
			<br>
			此致！
			CocosAds团队";
			$this->mailSend($userName,$title,$msg);
    }

    /**
     * 新注册用户发邮件
     *
     * @param $userName
     * @param $title
     * @param $msg
     * @access public
     * @return page
     */
    function mailSend($userName='',$title,$msg=''){
        if(empty($userName))
        {
            return false;
        }
        //		$title = "=?UTF-8?B?".base64_encode($title)."?=";
        //
        //		$cmd = "/usr/local/bin/sendEmail -f 'service@punchobx.org' -t '$userName' -u '$title' -o message-content-type=html -m '".$msg."' -o message-charset=UTF-8 >/dev/null";
        //if ( TRUE == $ERROR)
        //system($cmd);
        //$title = "=?UTF-8?B?".base64_encode($title)."?=";
        $CI =& get_instance();
        $CI->load->library('email');
        $CI->email->initialize();
        $CI->email->from('message@chance-ad.com', 'CocosAds');
        $CI->email->to($userName);
        $CI->email->reply_to('message@chance-ad.com', 'CocosAds');
        $CI->email->subject($title);
        $CI->email->message($msg);
        if ( ! $CI->email->send())
        {
//            print_r($CI->email->print_debugger());
//            exit;
            //            $CI->load->model('adminv2/EmailLogClass');
            //            $data = array(
            //				'objtype' => 2,
            //				'add_time' => time(),
            //				'status' => 0,
            //				'title' => $title,
            //				'contents' => $msg,
            //				'email' => $userName,
            //				'status' => 1,
            //            );
            //            print_r($data);
            //            $CI->EmailLogClass->add($data);
            //            return false;
        }
        return true;
    }

    /**
     * app应用审核通知邮件。
     */
    function appvierfy($appname = '', $email='2241567135@qq.com'){
        if(empty($email))
        {
            return false;
        }
        $title = '【应用审核处理】接收到新的SDK审核包【'.$appname.'】,请处理.';
        $msg = "系统于".date('Y-m-d H:i:s')."，接收到新SDK审核包【{$appname}】，请及时处理!";
        $this->mailSend($email,$title,$msg);
        return 1;
    }


    //提交身份验证 邮件
    // 友情提醒，账号xxxxxx@xxx.com提交了身份验证信息。请及时前往http://www.xxxx.com进行审核操作。
    function apply_varify($email=''){
        if(empty($email))
        {
            return false;
        }
        $title = '身份验证信息审核';
        $msg = "友情提醒，账号".$email."提交了身份验证信息。请及时前往<a href='http://unionadmin.punchbox.org/admin/financial/showverify' target='_blank'>http://unionadmin.punchbox.org/admin/financial/showverify</a>进行审核操作。";
        $email = "363764133@qq.com";
        $flg = $this->mailSend($email,$title,$msg);
    }

    //内部发送邮件
    function send_mail($userName='',$title='',$msg='',$cc=''){
        if(empty($userName))
        {
            return false;
        }
        $CI =& get_instance();
        $CI->load->library('email');
        $CI->email->from('addept-test@appget.cn', 'punchbox 广告平台');
        $CI->email->to($userName);
        $CI->email->reply_to('service@punchbox.org', 'punchbox 广告平台');
        $CI->email->subject($title);
        $CI->email->message($msg);
        if($cc) $CI->email->cc($cc);
        if ( ! $CI->email->send())
        {
            /*$data = array(
             'objtype' => 2,
             'add_time' => time(),
             'status' => 0,
             'title' => $title,
             'contents' => $msg,
             'email' => $userName,
             'status' => 1,
             );
             print_r($data);*/
            return false;
        }
        return true;
    }

}
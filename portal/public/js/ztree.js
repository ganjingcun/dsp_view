// JavaScript Document
var ztree = new Object();
ztree.Switch = function(id){
	var li_obj = id.parent()
	var li_ul_obj = li_obj.find("ul").eq(0)
	li_ul_obj.slideToggle("fast")
	if(li_obj.find("ul").is("ul")){		
		if(id.hasClass("curr")){
			id.removeClass("curr")
			li_obj.find("ul").removeClass("show")
		}else{
			id.addClass("curr")	
			li_obj.find("ul").addClass("show")
			}
	}
}

ztree.Choice = function(id){
	var inputObj = id.parent().find("input")
	if(id.attr("checked")){
		inputObj.attr("checked","checked");
		inputObj.removeClass('void')
	}else{
		inputObj.removeAttr("checked");
		inputObj.parent().parentsUntil($('ul.data_tree'),'li').each(function(){
			$(this).find('input').eq(0).attr("checked",false).addClass('void')
		})
		inputObj.removeClass('void')
		}
	}

ztree.Refur = function(id){
	$(id).find("ul").css("display","none");
	$(id).find("b.curr").siblings("ul").show("fast");
	var s = $(id).find("input")
	for(var i = 0; i<s.length; i++){
		if($(s[i]).attr("checked")){
			$(s[i]).parent().find("input").attr("checked","checked");
			}
		}
	
	}

ztree.Init = function(obj){
	obj.find('b').bind("click", function(e){	
		ztree.Switch($(this));
		return false
	})
	obj.find('input').bind("click", function(e){
		ztree.Choice($(this));
	})
	obj.find('a').bind("click", function(e){
		$(this).parentsUntil('.data_tree').find('a').removeClass("hover");
		$(this).toggleClass("hover");
		return false
	})	
	ztree.Refur(obj)
}
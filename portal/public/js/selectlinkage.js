
	var selectLinkage = (function(){
		var id = $("#select_linkage");
		var ajaxurl = "/manage/userinfo/get_areas";
		function asse(data){
			if(!data){ return "" };
			var str = "";
			if(typeof data=="string"){ data= JSON.parse(data); }
			for (var i = 0; i <= data.length - 1; i++) {
				str = str + "<option value='"+data[i].areaid+"' >"+data[i].areaname+"</option>";
			};
			return str
		}

		function selectobj(n){
			var sid = id.find("#area_"+n);
			if(sid.html()){
				$('<select id="area_'+n+'" name="area_'+n+'"></select>').insertAfter("#area_"+n);
				sid.remove();
			}else{
 				id.append('<select id="area_'+n+'" name="area_'+n+'"></select>');
			}
			return $("#area_"+n)
		};

		function getdata(por,callback){
 			$.get(ajaxurl+"?areaid="+por.areaid,function(data){
 				callback(data)
			});
			
			//callback(data["type_"+1]);
		}

		function setNextSelect(n,data){
			if(data){
				var select = selectobj(n+1);
				select.html(asse(data));
				select.css({"position":"relative"});
				select.on("change",function(e){
					getdata({"areaid":this.value},function(data){
						setNextSelect(n+1,data);
					})
				})
			}else{
				return
			}
		}

		function setSelect(n){
			var select = $("#area_"+n);
			select.on("change",function(e){
				getdata({"areaid":this.value},function(data){
					setNextSelect(n,data);
				})
			});
		}
		function init(data){
			var select1 = selectobj(0);
			var str = asse(data)
			select1.html(str);
			setSelect(0);
		};

		function add(areaid,n,callback){
			if(areaid <= 1){ return }
			var pid = 1;
			if(n>=0){ pid = n };
			getdata({"areaid":areaid},function(data){
				setNextSelect(pid,data);
				if(typeof callback ==="function"){
					callback();
				}
			})
		}
		
		function selectitem(aid,n){
			if(!n){ return }
			$("#area_"+aid).find("option[value="+n+"]").attr("selected","selected");
		}

		return {
			init:init,
			add:add,
			selectitem:selectitem
		}
	})()

 
var adboard = (function(){
	function repairNum(){
		$("#conlist tr").each(function(index, element) {
			var n = index+1;
			if(index<9){ n = (index+1); }
			 $(this).find("td").eq(0).html(n);
		});
		var y =	$("#conlist .curr").each(function() {
			$("#conlist").scrollTop(this.offsetTop-200);
		});
	};
	
	function moveup(){
		if($("#conlist tr.curr").hasClass("curr") != true){
			alert("请从列表中选择素材。");return false;
		}
		var moveObj = $("#conlist tr.curr").prev();
		$("#conlist tr.curr").after(moveObj);
		$("#conlist").attr("curr-index",$("#conlist tr.curr").index());
		$("#conlist tr.curr td:eq(0)").attr('class','1');
		repairNum()
	};
	
	function addnew(){
		$("#url").val("");
		$("#popup_1").toggle();
	}
	
	function movedown(){
		if($("#conlist tr.curr").hasClass("curr") != true){
			alert("请从列表中选择素材。");return false;
		}
		var moveObj = $("#conlist tr.curr").next();
		moveObj.after($("#conlist tr.curr"));
		$("#conlist").attr("curr-index",$("#conlist tr.curr").index());
		$("#conlist tr.curr td:eq(0)").attr('class','1');
		repairNum()
	};
	
	function edit(){
		if($("#conlist tr.curr").hasClass("curr") != true){
			alert("请从列表中选择素材。");
			return false;
		}
		$("input[name=appname]").val($("#conlist tr.curr p:eq(0)").html());
		$("textarea[name=descs]").val($("#conlist tr.curr p:eq(1)").html());
		$("#njh_appimg").attr("src",$("#conlist tr.curr img:eq(0)").attr("src"));
		$("#popup_3").toggle();
	};
	
	function dele(){
		if($("#conlist tr.curr").hasClass("curr") != true){
			alert("请从列表中选择素材。");return false;
		}
		if(!confirm("确定需要删除该选定的App？")){
			return false;
		}
		var name = $("#conlist tr.curr td:eq(0)").attr('name');
		var pid = $("#pid").val();
		if(!(name && pid)){
			return false;
		}
		$.post("/manage/admore/ajaxdel",{ pid:pid,id:name},function(data){
			if(data && data['info']){
				alert(data['info']);
			}
			if(data && data['status'] && data['status'] ==1){
				$("#conlist tr.curr").remove();
				repairNum();
			}
		},'json');
	};

	function switchSection(ele){
		if(!$(ele).attr("data-target")){ return; }
		var cmdArr =$(ele).attr("data-target").split(":");
		switch(cmdArr[0]){
			case 'pop':
				$("#"+cmdArr[1]).toggle();
				break;
			case 'com':
				eval(cmdArr[1])();
				break;
			default:break;
		}
	};
	
	return {
		repairNum:repairNum,
		moveup:moveup,
		movedown:movedown,
		dele:dele,
		edit:edit,
		addnew:addnew,
		switchSection:switchSection
	}	
})();


function onCloseButton(){
	var event = arguments.callee.caller.arguments[0]||window.event;//消除浏览器差异
	var e = event.srcElement || event.target;//IE支持srcElement,FF支持target 获取当前触发事件的元素
	$(e).parentsUntil().find('.popup_body').fadeOut();
};


function tableTdOnClick(){
	$("#conlist").on("click",function(e){
		var e = e ? e : window.event;
		var $this = $(e.target).parents("tr");
		$("#conlist tr").removeClass("curr");
		$this.addClass("curr");
		$("#conlist").attr("curr-index",$this.index());
	});
};
function inputDateOnClick(obj){
	var obj = $(obj)
	obj.DatePicker({
		format:'Y-m-d',
		date: obj.val(),
		current: obj.val(),
		starts: 1,
		position: 'bottom',
		enablePast:true,
		onBeforeShow: function(){
			obj.DatePickerSetDate(obj.val(), true);
		},
		onChange: function(formated,dates){
				obj.val(formated);
				obj.DatePickerHide();
		}
	});
}

(function($){

    $.fn.extend({ 

        hoverZoom: function(settings) {
 
            var defaults = {
                zoom: 10,
                speed: 300
            };
             
            var settings = $.extend(defaults, settings);
         
            return this.each(function() {
            
                var s = settings;
                var hz = $(this);
                var image = $('img', hz);
                var height = image.height();

                hz.hover(function() {
                    $('img', this).stop().animate({
                        height: height + s.zoom,
                        marginLeft: -(s.zoom),
                        marginTop: -(s.zoom)/2
                    }, s.speed);
                    $('.title_bg',this).animate({'bottom':'0px'},200);
                    $('.title',this).animate({'bottom':'-40px'},400);

                }, function() {
                    $('img', this).stop().animate({
                        height: height,
                        marginLeft: 0,
                        marginTop: 0
                    }, 300);
                    $('.title_bg',this).stop(true,false).animate({'bottom':'-40px'},200);
                    $('.title',this).stop(true,false).animate({'bottom':'-80px'},400);

                });
            });
        }
    });
})(jQuery);


$(document).ready(function(){
		//前后复制内容
		var fst_html=$(".banner .banner_ul li").first().clone();
		var lst_html=$(".banner .banner_ul li").last().clone();
		$(".banner .banner_ul").prepend(lst_html);
		$(".banner .banner_ul").append(fst_html);
	
	
		$(window).resize(function(){
			li_width=$(".banner .ban_li").width();
		})
	
		var li_length=$(".banner .ban_li").length;
		/*一张图片*/
		if(li_length<=3){
			$(".ban_bt").remove();
			$(".ban_icon_main").remove();
			$(".banner .banner_ul li:gt(0)").remove();
			return false;
		}				//一张图，不滚动。
		
		var li_width=$(".banner .ban_li").width();
		$(".banner .banner_ul").css("left",-li_width);
		
		
		var index = 1;
		var speed=8000;
		var pictime;

		
		/*banner*/
		$(".banner").hover(function() {
			clearInterval(pictime);
				
		},function() {
			pictime=setInterval(show_left,speed); 
		})
		
		/*自动滚动*/
		pictime=setInterval(show_left,speed); 
		
		
		/*左侧按钮*/
		$(".ban_lf").click(function(){
			clearInterval(pictime);
			show_right();
			})
			
		/*右侧按钮*/
		$(".ban_rg").click(function(){
			clearInterval(pictime);
			show_left();
			})

		/*底部按钮移动*/
		$(".ban_bt span").hover(function() {
			clearInterval(pictime);
			index=$(".ban_bt span").index($(this))+1;
			moveshow(index);
				
		},function() {
			pictime=setInterval(moveshow(index),speed); 
		})
		
		/*左右箭头*/
		$(".ban_icon").hover(function(){
			clearInterval(pictime);
		},function(){
			pictime=setInterval(moveshow(index),speed); 
		})
		
		/*移动图片*/
		function moveshow(index){
			
			var now_left=-index*li_width;
			$(".banner .banner_ul").stop().animate({"left":now_left},500);
				
			if(index<li_length-1)
			{
				$(".ban_bt span").not(index-1).removeClass("now").eq(index-1).addClass("now");
			}
			else
			{
				$(".ban_bt span").not(0).removeClass("now").eq(0).addClass("now");
			}
			
			
			}
			
		/*左移*/	
		function show_left(){
			index+=1;
			if(index == li_length) {
				index = 2;
				$(".banner .banner_ul").css("left",-li_width);
			}
			moveshow(index);
			}
			
		/*右移*/
		function show_right(){
				index-=1;
				if(index == -1) {
					index = li_length-3;
					var li_in=-li_width*(li_length-2);
					$(".banner .banner_ul").css("left",li_in);
				}
				moveshow(index);
			
			}
		
	})
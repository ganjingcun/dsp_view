'use strict';

var curItem = document.getElementsByClassName('active')[0];

if (curItem) {
	curItem.scrollIntoViewIfNeeded();
}